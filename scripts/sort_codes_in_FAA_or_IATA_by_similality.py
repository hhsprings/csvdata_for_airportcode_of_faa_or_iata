# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
import os
import csv
import math
from itertools import chain
from common import matcher
_CSVDIR = os.path.join(os.path.dirname(__file__), "../input/FAA_and_table_of_oneworld/")
_CSVFILE = os.path.join(_CSVDIR, "codes_in_FAA_or_IATA.csv")

result = []
for r in csv.reader(open(_CSVFILE)):
    code, icao_found_in_faa, found_as_faa_name, found_as_faa_loc, \
        found_as_iatatab_name, found_as_iatatab_loc, _ = r
    if code == 'code':
        continue
    #
    ratio = matcher.match(
        found_as_faa_name, found_as_faa_loc, found_as_iatatab_name, found_as_iatatab_loc)
    #
    if math.isnan(ratio):
        result.append((["_0_", "     "], r))
        continue
    else:
        if ratio < 0.5:
            result.append((["_1_", "{:.3f}".format(ratio)], r))
        elif ratio < 0.7:
            result.append((["_2_", "{:.3f}".format(ratio)], r))
        else:
            result.append((["_3_", "{:.3f}".format(ratio)], r))

writer = csv.writer(open("codes_in_FAA_or_IATA_sorted_by_similality.csv", "wb"))
writer.writerow("SIMILAR_KIND,SIMILAR_RATIO,code,icao_found_in_faa,found_as_faa_name,found_as_faa_loc,found_as_iatatab_name,found_as_iatatab_loc,iata_source".split(","))
for r in sorted(result):
    writer.writerow(list(chain(*r)))

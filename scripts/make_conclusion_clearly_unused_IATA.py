# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
import os
import csv
import re
from StringIO import StringIO

_dir = os.path.join(os.path.dirname(__file__), "../input/FAA_and_table_of_oneworld/")
_csvfile_faaiata = os.path.join(_dir, "codes_in_FAA_or_IATA.csv")
_dir = os.path.join(os.path.dirname(__file__), "../input/great_circle_mapper/")
_csvfile_gcm = os.path.join(_dir, "codes_in_great_circle_mapper.csv")
_csvfile_gcm_otr = os.path.join(_dir, "codes_in_great_circle_mapper_as_other.csv")
_GCM_IATA = {
    iata: (faa, icao, name, city)
    for faa, icao, iata, name, city in csv.reader(open(_csvfile_gcm))
    if faa != "faa" and iata != "////"
    }
_GCM_OTHER = {
    code: (type_, name, city, info)
    for type_, code, name, city, info in csv.reader(open(_csvfile_gcm_otr))
    if code != "code" and \
        ((type_ == "Obsolete" and (
                # following are checked manually
                # ------------------------------
                # see http://www.gcmap.com/airport/KBXM.
                code == "NHZ"
                # ------------------------------
                #
                or (".FAA)" not in info))) or \
             (type_ == "Alias") or ("closed" in info))
    }


def _unconditional_norm(s):
    s = re.sub(r"\s*[,]\s*", r", ", s.strip())
    s = re.sub(r"\s+", r" ", s)
    s = s.decode("utf-8").replace(u"–", "-").encode("utf-8")
    return s


all_iata = {}

#
for r in sorted(csv.reader(open(_csvfile_faaiata))):
    code, icao_found_in_faa, found_as_faa_name, found_as_faa_loc, \
        found_as_iatatab_name, found_as_iatatab_loc, _ = map(_unconditional_norm, r)
    if code == 'code':
        continue
    #
    if found_as_iatatab_name != '////':
        rgx = re.compile(r"\b(closed|ceased|replaced by)\b")
        if rgx.search(found_as_iatatab_name) or \
                code in _GCM_OTHER:
            otr = _GCM_OTHER.get(code, ("", "", "", ""))
            type_gcm, name_gcm, city_gcm, info_gcm = otr
            all_iata[code] = (found_as_iatatab_name, found_as_iatatab_loc, type_gcm, name_gcm, city_gcm, info_gcm)

rst = open(os.path.join(os.path.dirname(__file__), "../conclusions/IATA_codes_of_clearly_unused.rst"), "wb")
rst.write('''\
Unused IATA codes
=================
IATA codes listed below are the codes that OWNO ( [1]_ ) or GCM ( [2]_ ) says it had been closed or replaced, etc.

''')

csvmem = StringIO()
writer1 = csv.writer(csvmem)
writer2 = csv.writer(open(os.path.join(os.path.dirname(__file__), "../conclusions/IATA_codes_of_clearly_unused.csv"), "wb"))
writer2.writerow("IATA code, name (OWNO), city (OWNO), name (GCM), city (GCM), note (GCM)".split(","))
for iata in sorted(all_iata):
    found_as_iatatab_name, found_as_iatatab_loc, type_gcm, name_gcm, city_gcm, info_gcm = all_iata[iata]
    writer1.writerow((iata, found_as_iatatab_name, found_as_iatatab_loc, name_gcm, city_gcm, info_gcm))
    writer2.writerow((iata, found_as_iatatab_name, found_as_iatatab_loc, name_gcm, city_gcm, info_gcm))
rst.write('''
.. csv-table::
    :header: "IATA code", "name (OWNO)", "city (OWNO)", "name (GCM)", "city (GCM)", "note (GCM)"
    :widths: 5,19,19,19,19,19

''')
rst.write("    " + "\n    ".join(csvmem.getvalue().replace("\r\n", "\n").split("\n")))
rst.write('''
... [1] `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

... [2] `Great Circle Mapper <http://www.gcmap.com/>`_
''')

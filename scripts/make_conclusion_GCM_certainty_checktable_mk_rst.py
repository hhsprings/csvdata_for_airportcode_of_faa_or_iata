# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
import os
import csv
#
_dir = os.path.join(os.path.dirname(__file__), "../conclusions/GCM_certainty/")
tmp_data_per_page = {}
with open(os.path.join(_dir, "GCM_certainty_checktable.csv")) as fi:
    header = fi.readline().strip().replace("Degrees of Coincidence of names", "ratio").replace(" mismatch", "").replace(" claims", "")
    for line in fi.readlines():
        line = line.strip()
        if not line:
            continue
        page = line[0]
        if page not in tmp_data_per_page:
            tmp_data_per_page[page] = []
        tmp_data_per_page[page].append(line)
#
pp = 90
data_per_page = {}
for page in tmp_data_per_page:
    d = tmp_data_per_page[page]
    #xx = []
    pp = 120
    while True:
        if (len(d) - list(range(0, len(d), pp))[-1]) > 60:
            break
        pp -= 10
    for j, i in enumerate(range(0, len(d), pp)):
        cp = d[i:i+pp]
        from_to = [cp[0].split(",")[0], cp[-1].split(",")[0]]
        if from_to[0] == "////":
            from_to[0] = "FAA LID " + cp[0].split(",")[1]
        if "////" in from_to[0]:
            from_to[0] = "ICAO " + cp[0].split(",")[3]
        if from_to[1] == "////":
            from_to[1] = "FAA LID " + cp[-1].split(",")[1]
        data_per_page[(page, j + 1)] = (from_to, d[i:i+pp])
#
for page, pagenum in sorted(data_per_page):
    from_to, data = data_per_page[(page, pagenum)]
    page_ind = "no_IATA" if page == "/" else page
    page_title = "({} - {})".format(*from_to)
    with open(os.path.join(_dir, "GCM_certainty_checktable_{}{}.rst".format(page_ind, pagenum)), "wb") as fo:
        fo.write("""
List for checking certainty of Great Circle Mapper {}
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

""".format(page_title))
        fo.write(".. csv-table::\n")
        fo.write("    :header: " + header + "\n")
        fo.write("    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2\n\n")
        fo.write("    " + "\n    ".join(data) + "\n")

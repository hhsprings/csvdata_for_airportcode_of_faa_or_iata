# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
import os
import math
import csv
import re
from fuzzy import nysiis, DMetaphone, Soundex  # 3-rd party module. please install yourself.
_dmetaphone = DMetaphone()
dmetaphone = lambda s: _dmetaphone(s)
from common import matcher
from common.norm import loc_norm
_dir = os.path.join(os.path.dirname(__file__), "../input/FAA_and_table_of_oneworld/")
_csvfile_faaiata = os.path.join(_dir, "codes_in_FAA_or_IATA.csv")
_dir = os.path.join(os.path.dirname(__file__), "../input/great_circle_mapper/")
_csvfile_gcm = os.path.join(_dir, "codes_in_great_circle_mapper.csv")
_csvfile_gcm_otr = os.path.join(_dir, "codes_in_great_circle_mapper_as_other.csv")
_dir = os.path.join(os.path.dirname(__file__), "../conclusions/")
_knwon_closed_iata = [
    row[0] for row in csv.reader(open(os.path.join(_dir, "IATA_codes_of_clearly_unused.csv")))
    if row[0] != "IATA code"
    ]
_GCM_MAPPING = {
    (faa, iata): (icao, name, city)
    for faa, icao, iata, name, city in csv.reader(open(_csvfile_gcm))
    if faa != "faa"
    }
_GCM_IATA = {
    iata: (faa, icao, name, city)
    for faa, icao, iata, name, city in csv.reader(open(_csvfile_gcm))
    if faa != "faa" and iata != "////"
    }
_GCM_OTHER = {
    code: (type_, name, city, info)
    for type_, code, name, city, info in csv.reader(open(_csvfile_gcm_otr))
    if code != "code"
    }


def _unconditional_norm(s):
    import re
    s = re.sub(r"\s*[,]\s*", r", ", s.strip())
    s = re.sub(r"\s+", r" ", s)
    s = s.decode("utf-8").replace(u"–", "-").encode("utf-8")
    return s


all_faa = {"////": ("////", "////", "////")}
all_no_faacode_but_in_faa = {}
all_iata = {"////": ("////", "////", "////")}

#
for r in sorted(csv.reader(open(_csvfile_faaiata))):
    code, icao_found_in_faa, found_as_faa_name, found_as_faa_loc, \
        found_as_iatatab_name, found_as_iatatab_loc, iata_source = map(_unconditional_norm, r)
    if code == 'code':
        continue
    #
    if found_as_faa_name != '////':
        if code == "////" and icao_found_in_faa != "////":
            all_no_faacode_but_in_faa[icao_found_in_faa] = (icao_found_in_faa, found_as_faa_name, found_as_faa_loc)
        else:
            all_faa[code] = (icao_found_in_faa, found_as_faa_name, found_as_faa_loc)
    #
    if found_as_iatatab_name != '////':
        if code not in _knwon_closed_iata:
            all_iata[code] = (found_as_iatatab_name, found_as_iatatab_loc, iata_source)
#
def _country_states_mismatch_check_inner_inner(lhs, rhs, _split, _relax, _thr, is_iatasource_iata):
    # 1: OK
    # 0: TODO CHECK (NEUTRAL)
    # -1: NG
    lhss = set([_relax(s) for s in _split(lhs[0]) if s.strip()])
    rhss = set([_relax(s) for s in _split(rhs[0]) if s.strip()])
    #
    if not is_iatasource_iata:
        if lhs[1] == rhs[1]:  # match country
            if (not lhss and not rhss) or (len(lhss & rhss) >= _thr):
                return 1  # OK
            return 0  # TODO CHECK
        return -1  # NG
    else:
        c1 = _relax(lhs[1]).strip()
        if c1:
            lhss.add(c1)
        c2 = _relax(rhs[1]).strip()
        if c2:
            rhss.add(c2)
        if (len(lhss & rhss) >= _thr):
            return 1  # OK, MAYBE...
        return 0  # TODO CHECK

#TODO:
#  Green Island,"Green Island, Taiwan",////,////,????,"Lyudao, Tao Island, Taiwan"
#  is this "MAYBE"? -> strange.

def _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split, _relax, is_iatasource_iata):
    r = _country_states_mismatch_check_inner_inner(lhs, rhs, _split, _relax, 1, is_iatasource_iata)
    if r < 1:
        _IGN = [
            "international", "intl", "municipal", "muni",
            "regional", "rgnl", "regnl", "rgnl", "rgn", "rgnl",
            "aircraft", "acft", "airport", "aerodrome", "arpt", "aeroport",
            "heliport",
            ]
        _IGN_RGX = re.compile(r"\b(" + "|".join(_IGN) + r")\b", re.I)
        new_lhs_name = _IGN_RGX.sub("", lhs_name).strip().replace("????", "")
        new_rhs_name = _IGN_RGX.sub("", rhs_name).strip().replace("????", "")
        if new_lhs_name or new_rhs_name:
            new_lhs = list(lhs)
            if new_lhs_name:
                new_lhs[0] = new_lhs_name + ", " + new_lhs[0]
            new_rhs = list(rhs)
            if new_rhs_name:
                new_rhs[0] = new_rhs_name + ", " + new_rhs[0]
            r = max(r, _country_states_mismatch_check_inner_inner(new_lhs, new_rhs, _split, _relax, 2, is_iatasource_iata))
    return r

def _country_states_mismatch_check(lhs, lhs_name, rhs, rhs_name, is_iatasource_iata):
    # 2: OK
    # 1: MAYBE
    # 0: TODO CHECK (NEUTRAL)
    # -1: NG
    def _split1(s):
        return [ss for ss in re.split(r"(,\s|\s*/\s*)", s) if ss.strip()]
    def _split2(s):
        return [ss for ss in re.split(r"(,\s|\s|\s*/\s*)", s) if ss.strip()]
    def _relax1(s):
        return re.sub(r"\b([^,/\s]+)\b", lambda m: nysiis(m.group(1)), re.sub(r"(-|\.\s)", " ", s))
    def _relax2(s):
        return re.sub(r"\b([^,/\s]+)\b", lambda m: Soundex(6)(m.group(1)), re.sub(r"(-|\.\s)", " ", s))
    def _relax3(s):
        return re.sub(r"\b([^,/]+)\b", lambda m: nysiis(m.group(1)), re.sub(r"(-|\.\s)", " ", s))
    #
    r = _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split1, _relax1, is_iatasource_iata)
    if r < 1:
        r = max(r, _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split1, _relax2, is_iatasource_iata))
        if r < 1:
            r = max(r, _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split1, _relax3, is_iatasource_iata))
    if r == 0:
        if _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split2, _relax1, is_iatasource_iata) == 1:
            return 1
        if _country_states_mismatch_check_inner(lhs, lhs_name, rhs, rhs_name, _split2, _relax3, is_iatasource_iata) == 1:
            return 1
    if not is_iatasource_iata:
        return 2 if r == 1 else r
    else:
        return r

#
result = []
#
for (faa_code, iata_code) in _GCM_MAPPING:
    icao_gcm, name_gcm, city_gcm = _GCM_MAPPING[(faa_code, iata_code)]
    if icao_gcm in all_no_faacode_but_in_faa:
        icao_found_in_faa, found_as_faa_name, found_as_faa_loc = all_no_faacode_but_in_faa[icao_gcm]
    else:
        icao_found_in_faa, found_as_faa_name, found_as_faa_loc = all_faa.get(faa_code, ("////", "////", "////"))
    found_as_iatatab_name, found_as_iatatab_loc, iata_source = all_iata.get(iata_code, ("////", "////", "////"))

    # TODO: relax checking when iata_source is "IATA"

    l0 = loc_norm(found_as_faa_loc, True)
    l1 = loc_norm(found_as_iatatab_loc, False)
    l2 = loc_norm(city_gcm, False)
    ratio0 = matcher.match(
        found_as_faa_name, found_as_faa_loc,
        found_as_iatatab_name, found_as_iatatab_loc)
    ratio1 = matcher.match(
        found_as_faa_name, found_as_faa_loc,
        name_gcm, city_gcm)
    ratio2 = matcher.match(
        found_as_iatatab_name, found_as_iatatab_loc,
        name_gcm, city_gcm)

    #
    if found_as_faa_name == "////":
        ICAO_code_mismatch_check = "UNK"
    elif icao_found_in_faa == icao_gcm:
        ICAO_code_mismatch_check = "OK"
    else:
        ICAO_code_mismatch_check = "NG"
    #
    if found_as_faa_loc == "////" and found_as_iatatab_loc == "////":
        country_states_mismatch_check = "UNK"
    else:
        country_states_mismatch_check = "TO DO CHECK"
        if found_as_faa_loc != "////" and found_as_iatatab_loc != "////":
            ch = _country_states_mismatch_check(l0, found_as_faa_name, l1, found_as_iatatab_name, iata_source == "IATA")
            ch = min(ch, _country_states_mismatch_check(l1, found_as_iatatab_name, l2, name_gcm, iata_source == "IATA"))
            ch = min(ch, _country_states_mismatch_check(l0, found_as_faa_name, l2, name_gcm, False))
        elif found_as_faa_loc == "////":
            ch = _country_states_mismatch_check(l1, found_as_iatatab_name, l2, name_gcm, iata_source == "IATA")
        else:
            ch = _country_states_mismatch_check(l0, found_as_faa_name, l2, name_gcm, False)
        if ch == 2:
            country_states_mismatch_check = "OK"
        elif ch == 1:
            country_states_mismatch_check = "MAYBE"
        elif ch == -1:
            country_states_mismatch_check = "NG"
    #
    if math.isnan(ratio0) and math.isnan(ratio1) and math.isnan(ratio2):
        deg_of_coincidence = float('nan')
    else:
        deg_of_coincidence = 1.0
        deg_of_coincidence *= (ratio0 if not math.isnan(ratio0) else 1.0)
        deg_of_coincidence *= (ratio1 if not math.isnan(ratio1) else 1.0)
        deg_of_coincidence *= (ratio2 if not math.isnan(ratio2) else 1.0)
        deg_of_coincidence = round(deg_of_coincidence, 3)
    #
    result.append((
        iata_code,
        faa_code,
        icao_found_in_faa,
        icao_gcm,
        ICAO_code_mismatch_check,  # ICAO code mismatch check

        deg_of_coincidence,

        found_as_iatatab_name,
        found_as_iatatab_loc,
        iata_source,

        found_as_faa_name,
        found_as_faa_loc,

        name_gcm,
        city_gcm,

        country_states_mismatch_check,  # country, states mismatch check
        ))
#
writer = csv.writer(open(os.path.join(os.path.dirname(__file__), "../conclusions/GCM_certainty/GCM_certainty_checktable.csv"), "wb"))
writer.writerow("IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), ICAO code mismatch check, Degrees of Coincidence of names, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country mismatch check".split(","))
writer.writerows(sorted(result))

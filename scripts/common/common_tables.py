# -*- coding: utf-8 -*-
# this script is for Python 2.7.
import re
#
_RAA_OBSTACLE_DATA_RAW = [
    # Obstacle Data - Regional Areas of Responsibility
    # <http://www.faa.gov/air_traffic/flight_info/aeronav/obst_data/regionalaor>
    #
    # East [POC: Kevin Diggs]
    ("United States", "Alabama", "AL"),
    ("United States", "Connecticut", "CT"),
    ("United States", "Delaware", "DE"),
    ("United States", "Washington, DC", "DC"),
    ("United States", "Florida", "FL"),
    ("United States", "Georgia", "GA"),
    ("United States", "Kentucky", "KY"),
    ("United States", "Maine", "ME"),
    ("United States", "Maryland", "MD"),
    ("United States", "Massachusetts", "MA"),
    ("United States", "Mississippi", "MS"),
    ("United States", "New Hampshire", "NH"),
    ("United States", "New Jersey", "NJ"),
    ("United States", "New York", "NY"),
    ("United States", "North Carolina", "NC"),
    ("United States", "Pennsylvania", "PA"),
    ("United States", "Rhode Island", "RI"),
    ("United States", "South Carolina", "SC"),
    ("United States", "Tennessee", "TN"),
    ("United States", "Vermont", "VT"),
    ("United States", "Virginia", "VA"),
    ("United States", "West Virginia", "WV"),
    ("Puerto Rico", "Puerto Rico", "PR"),
    ("Bahamas", "Bahamas", "BS"),
    ("Caribbean", "Antigua & Barbuda", "AG"),
    ("Caribbean", "Netherland Antilles", "AN"),
    ("Caribbean", "Aruba", "AW"),
    ("Caribbean", "Cuba", "CU"),
    ("Caribbean", "Dominican Republic", "DO"),
    ("Caribbean", "Guadeloupe", "GP"),
    ("Caribbean", "Honduras", "HN"),
    ("Caribbean", "Haiti", "HT"),
    ("Caribbean", "Jamaica", "JM"),
    ("Caribbean", "St. Kitts & Nevis", "KN"),
    ("Caribbean", "Cayman Islands", "KY"),
    ("Caribbean", "Montserrat", "MS"),
    ("Caribbean", "Turks & Caicos Islands", "TC"),
    ("Caribbean", "British Virgin Islands", "VG"),
    ("Caribbean", "U.S. Virgin Islands", "VI"),  # original: Virgin Islands
    #
    # Central [POC: Nate Hersh]
    #   NOTE: Texas Longitude Divisions:
    #     TX (E): East = 93°30'30" - 95°59'59"
    #     TX (C): Central = 96°00'00" - 98°59'59"
    #     TX (W): West = 99°00'00" - 106°38'00"
    #
    ("United States", "Arkansas", "AR"),
    ("United States", "Illinois", "IL"),
    ("United States", "Indiana", "IN"),
    ("United States", "Iowa", "IA"),
    ("United States", "Kansas", "KS"),
    ("United States", "Louisiana", "LA"),
    ("United States", "Michigan", "MI"),
    ("United States", "Minnesota", "MN"),
    ("United States", "Missouri", "MO"),
    ("United States", "Nebraska", "NE"),
    ("United States", "New Mexico", "NM"),
    ("United States", "North Dakota", "ND"),
    ("United States", "Ohio", "OH"),
    ("United States", "Oklahoma", "OK"),
    ("United States", "South Dakota", "SD"),
    ("United States", "Texas", "TX"),
    ("United States", "Wisconsin", "WI"),
    #
    # West [POC: Harry Pavulaan]
    #
    ("United States", "Alaska", "AK"),
    ("United States", "Arizona", "AZ"),
    ("United States", "California", "CA"),
    ("United States", "Colorado", "CO"),
    ("United States", "Hawaii", "HI"),
    ("United States", "Idaho", "ID"),
    ("United States", "Montana", "MT"),
    ("United States", "Nevada", "NV"),
    ("United States", "Oregon", "OR"),
    ("United States", "Utah", "UT"),
    ("United States", "Washington", "WA"),
    ("United States", "Wyoming", "WY"),
    ("Canada", "Canada", "CA"),
    ("Mexico", "Mexico", "MX"),
    ("Pacific", "American Samoa", "AS"),
    ("Pacific", "Fed States of Micronesia", "FM"),
    ("Pacific", "Micronesia, Fed States of", "FM"),  #
    ("Pacific", "Guam", "GU"),
    ("Pacific", "Kiribati", "KI"),
    ("Pacific", "Marshall Islands", "MH"),
    ("Pacific", "Midway Islands", "MI"),
    ("Pacific", "Northern Mariana Isl", "MP"),
    ("Pacific", "Palau", "PW"),
    ("Pacific", "Russia", "RU"),
    ("Pacific", "Tokelau", "TK"),
    ("Pacific", "Wake Island", "WQ"),
    ("Pacific", "Samoa", "WS"),

    # followings are not in <http://www.faa.gov/air_traffic/flight_info/aeronav/obst_data/regionalaor>,
    # but maybe.
    ("???", "Northern Mariana Islands", "CQ"),
    ("???", "US Minor Outlying Islands", "MQ"),
    ]

_KNOWN_STATE_IF_BLANK = {
    'ANDERSON, - UNITED STATES': 'Anderson, South Carolina, United States',
    'CHANDALAR LAKE, - UNITED STATES': 'Chandalar, Alaska, United States',
    'CHICAGO/WAUKEGAN, - UNITED STATES': 'Waukegan, Illinois, United States',
    'COLUMBUS, - UNITED STATES': 'Columbus, Ohio, United States',
    'ENID, - UNITED STATES': 'Enid, Oklahoma, United States',
    'IMMOKALEE, - UNITED STATES': 'Immokalee, Florida, United States',
    'IOWA CITY, - UNITED STATES': 'Iowa City, Iowa, United States',
    'JACKSONVILLE, - UNITED STATES': 'Jacksonville, Illinois, United States',
    'KILLEEN, - UNITED STATES': 'Killeen, Texas, United States',
    'KINSTON, - UNITED STATES': 'Kinston, North Carolina, United States',
    'LAKELAND, - UNITED STATES': 'Lakeland, Florida, United States',
    'LOS ANGELES, - UNITED STATES': 'Los Angeles, California, United States',
    'MANLEY HOT SPRINGS, - UNITED STATES': 'Manley Hot Springs, Alaska, United States',
    'OKEECHOBEE, - UNITED STATES': 'Okeechobee, Florida, United States',
    'STATE COLLEGE, - UNITED STATES': 'State College, Pennsylvania, United States',
    'TANANA, - UNITED STATES': 'Tanana, Alaska, United States',
    'UVALDE, - UNITED STATES': 'Uvalde, Texas, United States',
    'WATSONVILLE, - UNITED STATES': 'Watsonville, California, United States',
    'WEATHERFORD, - UNITED STATES': 'Weatherford, Texas, United States',
    'WESTERLY, - UNITED STATES': 'Westerly, Rhode Island, United States',
    'WINNEMUCCA, - UNITED STATES': 'Winnemucca, Nevada, United States',
    'PINEDALE, - UNITED STATES': 'Pinedale, Wyoming, United States',

    "NEW BIGHT, - BAHAMAS": "Cat Island, Bahamas",
    }

_LOC_SIMPLY_REPLACES_0 = {
    'Aksu, PR China': 'Aksu, Xinjiang, China',
    'Altay, PR China': 'Altay, Xinjiang, China',
    'Ankang, PR China': 'Ankang, Shaanxi, China',
    'Anqing, PR China': 'Anqing, Anhui, China',
    'Anshan, PR China': 'Anshan, Liaoning, China',
    'Bangda, PR China': 'Bangda, Qamdo (Chamdo), Tibet, China',
    'Baoshan, PR China': 'Baoshan, Yunnan, China',
    'Baotou, PR China': 'Baotou, Inner Mongolia, China',
    'Beihai, PR China': 'Beihai, Guangxi, China',
    'Beijing, PR China': 'Beijing, Beijing, China',
    'Beijing, PR China': 'Beijing, Beijing, China',
    'Beijing, PR China': 'Beijing, Beijing, China',
    'Bengbu, PR China': 'Bengbu, Anhui, China',
    'Changchun, PR China': 'Changchun, Jilin, China',
    'Changde, PR China': 'Changde, Hunan, China',
    'Changhai, PR China': 'Changhai, Liaoning, China',
    'Changsha, PR China': 'Changsha, Hunan, China',
    'Changzhi, PR China': 'Changzhi, Shaanxi, China',
    'Changzhou, PR China': 'Changzhou, Jiangsu, China',
    'Chaoyang, PR China': 'Chaoyang, Liaoning, China',
    'Chengdu, PR China': 'Chengdu, Sichuan, China',
    'Chifeng, PR China': 'Chifeng, Inner Mongolia, China',
    'Chongqing, PR China': 'Chongqing, Chongqing, China',
    'Dali City, PR China': 'Xiaguan, Yunnan, China',
    'Dalian, PR China': 'Dalian, Liaoning, China',
    'Dandong, PR China': 'Dandong, Liaoning, China',
    'Datong, PR China': 'Datong, Shaanxi, China',
    'Daxian, PR China': 'Dazhou, Sichuan, China',
    'Dayong, PR China': 'Zhangjiajie, Hunan, China',
    'Dazu, PR China': 'Dazu, Chongqing, China',
    'Diqing, PR China': 'Shangri-La, Yunnan, China',
    'Dunhuang, PR China': 'Dunhuang, Gansu, China',
    'Enshi, PR China': 'Enshi, Hubei, China',
    'Fuoshan, PR China': 'Foshan, Guangdong, China',
    'Fuyang, PR China': 'Fuyang, Anhui, China',
    'Fuyun, PR China': 'Fuyun, Xinjiang, China',
    'Fuzhou, PR China': 'Fuzhou, Fujian, China',
    'Ganzhou, PR China': 'Ganzhou, Jiangxi, China',
    'Golmud, PR China': 'Golmud, Qinghai, China',
    'Guang Yuan, PR China': 'Guangyuan, Sichuan, China',
    'Guanghan, PR China': 'Guanghan, Sichuan, China',
    'Guanghua, PR China': 'Guanghua, Hubei, China',
    'Guangzhou, PR China': 'Guangzhou, Guangdong, China',
    'Guilin, PR China': 'Guilin, Guangxi, China',
    'Guiyang, PR China': 'Guiyang, Guizhou, China',
    'Haikou, PR China': 'Haikou, Hainan, China',
    'Hailar, PR China': 'Hailar, Inner Mongolia, China',
    'Hami, PR China': 'Hami, Xinjiang, China',
    'Hangzhou, PR China': 'Hangzhou, Zhejiang, China',
    'Harbin, PR China': 'Harbin, Heilongjiang, China',
    'Hefei, PR China': 'Hefei, Anhui, China',
    'Heihe, PR China': 'Heihe, Heilongjiang, China',
    'Hengyang, PR China': 'Hengyang, Hunan, China',
    'Hohhot, PR China': 'Hohhot, Inner Mongolia, China',
    'Hotan, PR China': 'Hotan, Xinjiang, China',
    'Huanghua, PR China': 'Changsha, Hunan, China',
    'Huizhou, PR China': 'Huizhou, Guangdong, China',
    "Ji'an, PR China": "Ji'an, Jiangxi, China",
    'Jiamusi, PR China': 'Jiamusi, Heilongjiang, China',
    'Jilin, PR China': 'Jilin, Jilin, China',
    'Jinan, PR China': 'Jinan, Shandong, China',
    'Jingdezhen, PR China': 'Jingdezhen, Jiangxi, China',
    'Jinghong, PR China': 'Jinghong, Yunnan, China',
    'Jining, PR China': 'Jining, Shandong, China',
    'Jinjiang, PR China': 'Quanzhou, Fujian, China',
    'Jinzhou, PR China': 'Jinzhou, Liaoning, China',
    'Jiujiang, PR China': 'Jiujiang, Jiangxi, China',
    'Juzhou, PR China': 'Quzhou, Zhejiang, China',
    'Karamay, PR China': 'Karamay, Xinjiang, China',
    'Kashi, PR China': 'Kashgar, Xinjiang, China',
    'Korla, PR China': 'Korla, Xinjiang, China',
    'Lanzhou, PR China': 'Lanzhou, Gansu, China',
    'Lhasa, PR China': 'Lhasa, Tibet, China',
    'Lianyungang, PR China': 'Lianyungang, Jiangsu, China',
    'Lijiang City, PR China': 'Lijiang, Yunnan, China',
    'Linyi, PR China': 'Linyi, Shandong, China',
    'Liuzhou, PR China': 'Liuzhou, Guangxi, China',
    'Luoyang, PR China': 'Luoyang, Henan, China',
    'Luxi, PR China': 'Mang City, Dehong, Yunnan, China',
    'Luzhou, PR China': 'Luzhou, Sichuan, China',
    'Meixian, PR China': 'Meizhou, Guangdong, China',
    'Mian Yang, PR China': 'Mianyang, Sichuan, China',
    'Mudanjiang, PR China': 'Mudanjiang, Heilongjiang, China',
    'Nanchang, PR China': 'Nanchang, Jiangxi, China',
    'Nanchong, PR China': 'Nanchong, Sichuan, China',
    'Nanking/Nanjing, PR China': 'Nanjing, Jiangsu, China',
    'Nanning, PR China': 'Nanning, Guangxi, China',
    'Nantong, PR China': 'Nantong, Jiangsu, China',
    'Nanyang, PR China': 'Nanyang, Henan, China',
    'Ningbo, PR China': 'Ningbo, Zhejiang, China',
    'Qiemo, PR China': 'Qiemo, Xinjiang, China',
    'Qingdao, PR China': 'Qingdao, Shandong, China',
    'Qingyang, PR China': 'Qingyang, Gansu, China',
    'Qinhuangdao, PR China': 'Qinhuangdao, Hubei, China',
    'Qiqihar, PR China': 'Qiqihar, Heilongjiang, China',
    'Rugao, PR China': 'Rugao, Jiangsu, China',
    'Sanya, PR China': 'Sanya, Hainan, China',
    'Shanghai, PR China': 'Shanghai, Shanghai, China',
    'Shanghai, PR China': 'Shanghai, Shanghai, China',
    'Shanhaiguan, PR China': 'Shikezi, Xinjiang, China',
    'Shantou, PR China': 'Jieyang, Guangdong, China',
    'Shaoguan, PR China': 'Shaoguan, Guangdong, China',
    'Shashi, PR China': 'Shashi, Hubei, China',
    'Shenyang, PR China': 'Shenyang, Liaoning, China',
    'Shenzhen, PR China': 'Shenzhen, Guangdong, China',
    'Shijiazhuang, PR China': 'Shijiazhuang, Hubei, China',
    'Simao, PR China': "Simao, Pu'er City, Yunnan, China",
    'Tacheng, PR China': 'Tacheng, Xinjiang, China',
    'Taiyuan, PR China': 'Taiyuan, Shaanxi, China',
    'Taizhou, Zhejiang, PR China': 'Huangyan, Zhejiang, China',
    'Tianjin, PR China': 'Tianjin, Tianjin, China',
    'Tonghua, PR China': 'Tonghua, Jilin, China',
    'Tongliao, PR China': 'Tongliao, Inner Mongolia, China',
    'Tongren, PR China': 'Tongren, Guizhou, China',
    'Tunxi, PR China': 'Huangshan, Anhui, China',
    'Ulanhot, PR China': 'Ulanhot, Inner Mongolia, China',
    'Urumqi, PR China': '\xc3\x9cr\xc3\xbcmqi, Xinjiang, China',
    'Wanxian, PR China': 'Wanxian, Sichuan, China',
    'Weifang, PR China': 'Weifang, Shandong, China',
    'Weihai, PR China': 'Weihai, Shandong, China',
    'Wenzhou, PR China': 'Wenzhou, Zhejiang, China',
    'Wuhan, PR China': 'Wuhan, Hubei, China',
    'Wuhu, PR China': 'Wuhu, Anhui, China',
    'Wuxi, PR China': 'Wuxi, Jiangsu, China',
    'Wuyishan, PR China': 'Wuyishan, Fujian, China',
    'Wuzhou, PR China': 'Wuzhou, Guangxi, China',
    'Xi An, PR China': "Xi'an, Shaanxi, China",
    'Xi An, PR China': "Xi'an, Shaanxi, China",
    'Xiamen, PR China': 'Xiamen, Fujian, China',
    'Xiangfan, PR China': 'Xiangyang, Hubei, China',
    'Xichang, PR China': 'Xichang, Sichuan, China',
    'Xilinhot, PR China': 'Xilinhot, Inner Mongolia, China',
    'Xingcheng, PR China': 'Xingcheng, Liaoning, China',
    'Xingning, PR China': 'Xingning, Guangdong, China',
    'Xingtai, PR China': 'Xingtai, Hubei, China',
    'Xining, PR China': 'Xining, Qinghai, China',
    'Xuzhou, PR China': 'Xuzhou, Jiangsu, China',
    "Yan'an, PR China": "Yan'an, Shaanxi, China",
    'Yancheng, PR China': 'Yancheng, Jiangsu, China',
    'Yanji, PR China': 'Yanji, Jilin, China',
    'Yantai, PR China': 'Yantai, Shandong, China',
    'Yichang, PR China': 'Yichang, Hubei, China',
    'Yilan, PR China': 'Yilan, Heilongjiang, China',
    'Yinchuan, PR China': 'Yinchuan, Ningxia, China',
    'Yining, PR China': 'Yining, Xinjiang, China',
    'Yiwu, PR China': 'Yiwu, Zhejiang, China',
    'Yuanmou, PR China': 'Yuanmou, Yunnan, China',
    'Zhanjiang, PR China': 'Zhanjiang, Guangdong, China',
    'Zhaotong, PR China': 'Zhaotong, Yunnan, China',
    'Zhengzhou, PR China': 'Zhengzhou, Henan, China',
    'Zhoushan, PR China': 'Zhoushan, Zhejiang, China',
    'Zhuhai, PR China': 'Zhuhai, Guangdong, China',
    'Zunyi, PR China': 'Zunyi, Guizhou, China',

    # hmmm...now Hong Kong is of China...
    'Hong Kong, Hong Kong, PR China': 'Hong Kong, Hong Kong',

    # hmm...
    'Taipa Island, Macau': 'Macau (SAR), PR China',
    }

_LOC_SIMPLY_REPLACES_1 = {
    'Hawai, Hawai': 'Hawai, United States',
    'Abilene, USA': 'Abilene, Texas, United States',
    'Alice, USA': 'Alice, Texas, United States',
    'Papua-New Guinea': 'Papua New Guinea',
    'Libyan Arab Jamahiriya (Libya)': 'Libya',

    'Congo (ROC)': 'Congo',
    'Congo (Republic of)': 'Congo',

    'Congo (DRC)': 'Zaire',
    'Democratic Republic of Congo (Zaire)': 'Zaire',

    "Lao People's Democratic Republic (Laos)": "Laos",
    "Lao PDR": "Laos",
    "Sri Lanka (Ceylon)": "Sri Lanka",

    "Viet Nam": "Vietnam",
    "Republic of Korea (South Korea)": "South Korea",
    "Korea, DPR": "North Korea",
    "Democratic People's Republic of Korea (North Korea)": "North Korea",

    "Myanmar (Burma)": "Myanmar",
    "Syrian Arab Republic (Syria)": "Syria",

    "Macedonia (Republic of)": "Macedonia",

    "Virgin Islands (British)": "British Virgin Islands",

    'Russian Federation (Russia)': 'Russia',

    "Cote d'Ivoire (Ivory Coast)": "Cote d'Ivoire",
    'Isle of Man, Isle of Man': 'Isle Of Man, United Kingdom',

    # Don't confuse with American Samoa (AS)
    "Samoa (Western Samoa)": "Samoa",

    "Cocos (Keeling) Islands": "Keeling Cocos",

    "Wallis and Futuna Islands": "Wallis and Futuna",

    'Gaza, Palestine': 'Gaza City, Palestinian Territory',

    "Turks and Caicos": "Turks and Caicos Islands",
    'Islands Islands': 'Islands',

    'Scotland, UK': 'Scotland, United Kingdom',

    ', Montenegro': ', Yugoslavia (Serbia and Montenegro)',

    'Wau, Sudan': 'Wau, Western Bahr el Ghazal, South Sudan',

    "St. Kitts And Nevis": "Saint Kitts and Nevis",

    "Svalbard (Norway)": "Svalbard, Norway",

    "Crimea, Ukraine, Russia": "Crimea, Ukraine",

    # East Timor became independent from Indonesia, but OWNO table is still saying it is in Indonesia.
    "East Timor (Timor-Leste)": "Indonesia",
    "Timor-Leste (East Timor)": "Indonesia",

    "St. Peter, Jersey": "Saint Peter, Jersey, United Kingdom",
    "Jersey, United Kingdom": "Saint Peter, Jersey, United Kingdom",

    'Malakal, Sudan': 'Malakal, Upper Nile, South Sudan',

    # see https://en.wikipedia.org/wiki/Pristina
    'Pristina, Serbia': 'Pristina, Kosovo',

    # EuroAirport is served by:
    #     Basel, Switzerland
    #     Mulhouse, France
    #     Freiburg, Germany
    'Mulhouse/Basel, France/Switzerland': 'Basel/Mulhouse/Freiburg, Alsace, Switzerland/France/Germany',
    'Basel/Mulhouse/Freiburg, Alsace, France': 'Basel/Mulhouse/Freiburg, Alsace, Switzerland/France/Germany',

    # These doesn't have the same meaning, but we can say these are the same in the context of
    # checking pointer to the same airport.
    'Saba Island, Netherlands Antilles': 'The Bottom, Saba, Bonaire/Sint Eustatius/Saba',
    'St Eustatius, Netherlands Antilles': 'Oranjestad, Sint Eustatius, Bonaire/Sint Eustatius/Saba',
    "Bonaire, Netherlands Antilles": "Kralendijk, Bonaire, Bonaire/Sint Eustatius/Saba",

    #
    'Teniente R. Marsh, Antarctica': 'Base Antarctica, King George Island, Magallanes y de la Antartica Chilena, Chile',

    #
    'Guernsey, Channel Island (UK)': 'Guernsey, Channel Island, United Kingdom',
    'Forest, Guernsey': 'Guernsey, Channel Island, United Kingdom',
    "Alderney (one of the Channel Islands), United Kingdom": "Alderney, Channel Island, United Kingdom",
    "Alderney, Guernsey": "Alderney, Channel Island, United Kingdom",

    # see https://en.wikipedia.org/wiki/Laayoune
    # Please be calm, I'm neutral. My interest is only which multiple names are the same or not.
    "Laayoune, Morocco": "El Aaiun, Western Sahara",
    "Smara, Morocco": "Smara, Western Sahara",
    "Dakhla, Morocco": "Villa Cisneros, Western Sahara",

    "St. Martin, Guadeloupe": "Grand Case, St-Martin, Guadeloupe",
    }
# TODO:
#    maybe "Kuressaare, Ethiopia" in OWNO's table is wrong. (Kuressaare, Estonia)


_LOC_SIMPLY_REPLACES_2 = {
    'WASHINGTON, DISTRICT OF COLUMBIA, UNITED STATES': 'Washington, Washington DC, United States',
    'WASHINGTON, DC, UNITED STATES': 'Washington DC, United States',
    'MICRONESIA (FEDERATED STATES OF)': 'Micronesia',
    'MICRONESIA, FED STATES OF': 'Micronesia',
    'WAKE ISLAND, US MINOR OUTLYING IS': 'Wake Island, Wake Island',
    'WAKE ISLAND MINOR OUTLYING ISLANDS': 'Wake Island, Wake Island',
    'UNITED STATES MINOR OUTLYING ISLANDS': 'US Minor Outlying Islands',
    ', VIRGIN ISLANDS, UNITED STATES': ', U.S. Virgin Islands',

    "ORANJESTAD, - NETHERLANDS ANTILLES": 'Oranjestad, Sint Eustatius, Bonaire/Sint Eustatius/Saba',

    "GRAND CASE, - SAINT MARTIN": "Grand Case, St-Martin, Guadeloupe",
    "VIRGIN ISLANDS (BRITISH)": "British Virgin Islands",
    "VIRGIN ISLANDS, BRITISH": "British Virgin Islands",
    }

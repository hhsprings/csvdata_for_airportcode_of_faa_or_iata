# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
import re
from unidecode import unidecode  # pip install unidecode
from fuzzy import nysiis, DMetaphone, Soundex  # 3-rd party module. please install yourself.
from .common_tables import (
    _RAA_OBSTACLE_DATA_RAW, _KNOWN_STATE_IF_BLANK,
    _LOC_SIMPLY_REPLACES_0, _LOC_SIMPLY_REPLACES_1, _LOC_SIMPLY_REPLACES_2
    )


#
_ABR = {
    "international": "intl",
    "municipal": "muni",
    "regional": "rgnl",
    "regnl": "rgnl",
    "rgn": "rgnl",
    "aircraft": "acft",
    "airport": "",
    "aerodrome": "",
    "arpt": "",
    "fields": "field",
    "fld": "field",
    "mount": "mt",
    }
_ABR_RGX = re.compile(r"\b(" + "|".join(_ABR.keys()) + r")\b")
_ABR_SUB_FUN = lambda m: _ABR[m.group()]

_KNOWN_CITY_MAPPING = {
    # key=IATA style, value=FAA style
    "Lemars": "Le Mars",
}

def loc_norm(input_loc, is_faa):
    if input_loc in ("////", "????"):
        return input_loc, input_loc, input_loc
    #
    if not is_faa:
        for k in _LOC_SIMPLY_REPLACES_0:
            input_loc = input_loc.replace(k, _LOC_SIMPLY_REPLACES_0[k])
    #
    s = unidecode(input_loc.decode("utf-8")).strip()
    if not is_faa:
        for k in _LOC_SIMPLY_REPLACES_1:
            s = s.replace(k, _LOC_SIMPLY_REPLACES_1[k])
    #
    if is_faa:
        if s in _KNOWN_STATE_IF_BLANK:
            s = _KNOWN_STATE_IF_BLANK[s]
        else:
            m = re.match(r"^(.*),\s*(.*)-\s(.*)$", s)
            if m:
                ci, st, ct = map(lambda x: x.strip(), m.group(1, 2, 3))
                for reg, name, code in _RAA_OBSTACLE_DATA_RAW:
                    if st == code:
                        if ct == reg.upper():  # U.S.
                            s = "{}, {}, {}".format(ci.title(), name, reg)
                        else:  # Puerto Rico, etc. (normal)
                            s = "{}, {}".format(ci.title(), name)
                        break
                    elif ct == name.upper():  # Marshall Islands, etc.
                        s = "{}, {}".format(ci.title(), name)
                        break
                #else:
                #    s = "{} [4]".format(s.title())
    else:
        m = re.match(r"(.*)\s\((..)\),\s*USA$", s)
        if not m:
            m = re.match(r"(.*)\s\((..)\),\s*,\s*USA$", s)
            if not m:
                m = re.match(r"(.*)\s(..),\s*USA$", s)
                if not m:
                    m = re.match(r"(.*),\s*USA(..)$", s)
                    if not m:
                        m = re.match(r"(.*),\s*USA \((..)\)$", s)
        if m:
            ci, st = map(lambda x: x.strip(), m.group(1, 2))
            for reg, name, code in _RAA_OBSTACLE_DATA_RAW:
                if st == code:
                    s = "{}, {}, {}".format(ci, name, reg)
                    break

    # GCM style: Puerto Rico, United States
    for reg, name, code in _RAA_OBSTACLE_DATA_RAW:
        if reg != "United States" and (name in s and "United States" in s):
            s = s.replace(", United States", "")
            break

    #
    for k in _LOC_SIMPLY_REPLACES_2:
        s = s.upper().replace(k, _LOC_SIMPLY_REPLACES_2[k])
    #
    res = filter(lambda x: x.strip() and x != ", ", map(lambda ss: re.sub(r"^- ", "", ss), s.lower().rpartition(", ")))
    if len(res) == 1:
        res.insert(0, "")
    return map(lambda x: x.title(), res)


_SPL_PUNC_RGX = re.compile(r'\s+|\s*-\s*|\s*\.\s*')


_KNOWN_NAME_MAPPING = {
    # why first large S is always missing?
    r"t Clair County": "St Clair County",
    r'tate': 'State',
    r'tevens Field': 'Stevens Field',

    # maybe...
    r"Bobby L. Chain Mun": "Bobby L. Chain Muni",
    r'Mettle Field': 'Mettel Field',  # really?
    r'Jo Foss Fld': 'Joe Foss Fld',
    r'Bob Barker Memorial': 'Bob Baker Memorial',
    r'James Cox Dayton International': 'James M Cox Dayton International',
    r'Green County': 'Greene County',
    r'Wyoming Valle': 'Wyoming Valley',
    }
_NNM_RGX = re.compile(r"\b(" + "|".join(_KNOWN_NAME_MAPPING.keys()) + r")\b")
_NNM_SUB_FUN = lambda m: _KNOWN_NAME_MAPPING[m.group()]


def _name_norm(city, found_as_iatatab_name):
    found_as_iatatab_name = _NNM_RGX.sub(_NNM_SUB_FUN, found_as_iatatab_name.strip())
    found_as_iatatab_name = found_as_iatatab_name.replace(".", " ")

    it = [s.lower()
          for s in _SPL_PUNC_RGX.split(found_as_iatatab_name) if s.strip()]
    if city:
        it.extend([s.lower() for s in _SPL_PUNC_RGX.split(city) if s.strip()])

    def _f(s):
        return _ABR_RGX.sub(_ABR_SUB_FUN, s)

    return " ".join(
        sorted(
            list(set(map(_f, it))))).strip()


def name_split_and_norm(name, loc):
    name = re.sub(r"\s\(closed[^()]*\)", "", name)
    name = re.sub(r"\s\(under construction\)", "", name)
    name = re.sub(r"\(replaced by the\s*", "\(", name)
    name = re.sub(r"\(former\s*", "\(", name)
    name = unidecode(name.decode("utf-8"))
    if name == "????" and loc[0] and loc[0] != "????":  # GCM
        # assume airport name equals to city name
        name = ""
    #
    result = []
    #
    cities = []
    for c in loc[0].lower().split(", "):
        cities.extend(re.split(r"\s*/\s*", c))
    if len(cities) > 1:
        for i in range(1, len(cities)):
            cities.append(" ".join(cities[:i+1]))
    cities.insert(0, "")
    #
    for n in re.split(r"\s*/\s*", name):
        for city in cities:
            rgx = re.compile(r"([^()]*)\s*\((.*)\)\s*(.*)\s*")
            m = rgx.match(n)
            if m:
                n1 = rgx.sub(r"\1 \3", n).strip()
                result.append(_name_norm(city, n1))
                n2 = rgx.sub(r"\2 \3", n).strip()
                result.append(_name_norm(city, n2))
            else:
                rgx = re.compile(r"(.*)\s\((.*)\)")
                m = rgx.match(name)
                if m:
                    n1, n2 = m.group(1).strip(), m.group(2).strip()
                    result.append(_name_norm(city, n1))
                    result.append(_name_norm(city, n2))
                else:
                    result.append(_name_norm(city, n))
    #
    result.extend(map(nysiis, result))
    return result

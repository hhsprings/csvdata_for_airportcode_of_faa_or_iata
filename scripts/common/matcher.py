# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import re
from difflib import SequenceMatcher
from .norm import (loc_norm, name_split_and_norm)


def match(lhs_name, lhs_loc, rhs_name, rhs_loc):
    """
    return similarity_ratio_if_it_is_pair_candidate otherwise nan.
    """
    #
    if lhs_name == '////' or rhs_name == '////':
        return float('nan')
    #
    normed_lhs_loc = loc_norm(lhs_loc, not re.search(r"[a-z]", lhs_name + lhs_loc))
    normed_rhs_loc = loc_norm(rhs_loc, not re.search(r"[a-z]", rhs_name + rhs_loc))
    ratio = 0.0
    nmfs = name_split_and_norm(lhs_name, normed_lhs_loc)
    nmis = name_split_and_norm(rhs_name, normed_rhs_loc)
    _at_max_for_debug = ()
    for nmf in filter(lambda s: s, nmfs):
        for nmi in filter(lambda s: s, nmis):
            sm = SequenceMatcher(None, nmf, nmi)
            new_ratio = round(sm.ratio(), 3)
            if new_ratio >= ratio:
                ratio = new_ratio
                _at_max_for_debug = (nmf, nmi, sm.ratio(), lhs_name, normed_rhs_loc, rhs_name)
    #if ratio > 0.95:
    #    print(_at_max_for_debug)
    return ratio

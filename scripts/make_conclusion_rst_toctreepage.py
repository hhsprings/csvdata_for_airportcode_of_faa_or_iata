# -*- coding: utf-8 -*-
# this script is for Python 2.7.
from __future__ import absolute_import
import sys
from os import path
from glob import glob
#
_dir = path.join(path.dirname(__file__), "../conclusions/")
ofn = "README.rst"
pages = []
for page in sorted([fn
                    for fn in (glob(path.join(_dir, "*.rst")) + \
                                   glob(path.join(_dir, "GCM_certainty/*")))
                    if ofn not in fn]):
    with open(page) as fi:
        last = ""
        for line in fi.readlines():
            line = line.strip()
            sline = set(line)
            if "=" in sline and len(sline) == 1:
                pages.append((path.relpath(page, _dir).replace("\\", "/"), last))
            last = line

with open(path.join(_dir, ofn), "wb") as fo:
    for fn, title in pages:
        fo.write("* `{} <{}>`_\n".format(title, fn))

# -*- coding: utf-8 -*-
import csv
from StringIO import StringIO

# LID,ICAO,NAME,LOC
_fn_faa = "../input/FAA_and_table_of_oneworld/origs/from_FAA.csv"
dict_nfdc_faa2icao = {
    lid: (lid, icao, name, loc)
    for lid, icao, name, loc in csv.reader(open(_fn_faa))
    if lid != "LID" and lid != "////"
    }
dict_nfdc_icao2faa = {
    icao: (lid, icao, name, loc)
    for lid, icao, name, loc in csv.reader(open(_fn_faa))
    if lid != "LID" and icao != "////"
    }

# faa, icao, iata, name, city
_fn_gcm = "../input/great_circle_mapper/codes_in_great_circle_mapper.csv"
dict_gcm_faa2icao = {
    faa: (faa, icao, name, city)
    for faa, icao, _, name, city in csv.reader(open(_fn_gcm))
    if faa != "faa" and faa != "////"
    }
dict_gcm_icao2faa = {
    icao: (faa, icao, name, city)
    for faa, icao, _, name, city in csv.reader(open(_fn_gcm))
    if faa != "faa" and icao != "////"
    }
#
results = []
# 1.
for nfdc_faa in dict_nfdc_faa2icao:
    nfdc_faa, nfdc_icao, nfdc_name, nfdc_loc = dict_nfdc_faa2icao[nfdc_faa]
    gcm_faa, gcm_icao, gcm_name, gcm_loc = dict_gcm_faa2icao.get(nfdc_faa, ("////", "////", "////", "////"))
    if (gcm_icao, gcm_name, gcm_loc) == ("////", "////", "////"):
        # TODO
        pass
    elif nfdc_icao != gcm_icao:
        results.append((nfdc_faa, nfdc_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc))
        pass

# 2.
for gcm_faa in dict_gcm_faa2icao:
    nfdc_faa, nfdc_icao, nfdc_name, nfdc_loc = dict_nfdc_faa2icao.get(gcm_faa, ("////", "////", "////", "////"))
    gcm_faa, gcm_icao, gcm_name, gcm_loc = dict_gcm_faa2icao[gcm_faa]
    if (nfdc_icao, nfdc_name, nfdc_loc) == ("////", "////", "////"):
        results.append((nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc))
        pass
    elif nfdc_icao != gcm_icao:
        results.append((nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc))
        pass

# 3.
for nfdc_icao in dict_nfdc_icao2faa:
    nfdc_faa, nfdc_icao, nfdc_name, nfdc_loc = dict_nfdc_icao2faa[nfdc_icao]
    gcm_faa, gcm_icao, gcm_name, gcm_loc = dict_gcm_icao2faa.get(nfdc_icao, ("////", "////", "////", "////"))
    if (gcm_faa, gcm_name, gcm_loc) == ("////", "////", "////"):
        # TODO
        pass
    elif nfdc_faa != gcm_faa:
        results.append((nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc))
        pass

# 4.
for gcm_icao in dict_gcm_icao2faa:
    nfdc_faa, nfdc_icao, nfdc_name, nfdc_loc = dict_nfdc_icao2faa.get(gcm_icao, ("////", "////", "////", "////"))
    gcm_faa, gcm_icao, gcm_name, gcm_loc = dict_gcm_icao2faa[gcm_icao]
    if (nfdc_faa, nfdc_name, nfdc_loc) == ("////", "////", "////"):
        pass
    elif nfdc_faa != gcm_faa:
        results.append((nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc))
        pass
#
results = sorted(list(set(results)))
faa_only_gcm = [(nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc)
                for nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc in results
                if nfdc_faa == "////" and gcm_faa != "////"]
faa_only_nfdc = [(nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc)
                for nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc in results
                if gcm_faa == "////" and nfdc_faa != "////"]
other_mismatch = [(nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc)
                  for nfdc_faa, gcm_faa, nfdc_icao, gcm_icao, nfdc_name, nfdc_loc, gcm_name, gcm_loc in results
                  if gcm_faa == nfdc_faa]
#
fo = open("../conclusions/GCM_NFDC_code_mismatches.rst", "wb")
#
fo.write("""\
Found code mismatches (GCMap and NFDC)
======================================
NFDC: Aeronautical Information Services - National Flight Data Center (NFDC)

""")
#
for res, title in (
    (faa_only_gcm, "FAA LIDs only in GCMap"),
    (faa_only_nfdc, "FAA LIDs only in NFDC"),
    (other_mismatch, "ICAO code mismatch")):
    memcsv = StringIO()
    writer = csv.writer(memcsv)
    writer.writerows(res)
    fo.write("""
{}
------------------------------

.. csv-table::
    :header: FAA LID (NFDC), FAA LID (GCM), ICAO (NFDC), ICAO (GCM), NAME (NFDC), LOC (NFDC), NAME (GCM), LOC (GCM)

""".format(title))
    fo.write("    " + "\n    ".join(memcsv.getvalue().split("\r\n")) + "\n")

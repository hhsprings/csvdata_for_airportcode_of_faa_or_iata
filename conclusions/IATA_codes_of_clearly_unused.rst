Unused IATA codes
=================
IATA codes listed below are the codes that OWNO ( [1]_ ) or GCM ( [2]_ ) says it had been closed or replaced, etc.


.. csv-table::
    :header: "IATA code", "name (OWNO)", "city (OWNO)", "name (GCM)", "city (GCM)", "note (GCM)"
    :widths: 5,19,19,19,19,19

    AAP,Andrau Airpark (closed 1998),"Houston (TX), USA",Andrau Airpark,"Houston, Texas, United States",Type: Obsolete (see AAP.OLD)
    AIY,Bader Field (closed 2006),"Atlantic City (NJ), USA",Atlantic City Municipal/Bader Field,"Atlantic City, New Jersey, United States",Type: Obsolete (see AIY.OLD)
    AMI,Selaparang,"Mataram, Indonesia",Selaparang Airport,"Mataram, Lombok Island, Nusa Tenggara Barat, Indonesia",Type: Obsolete (see AMI.OLD) / Old (Alt.): Bandar Udara International Airport
    ARX,Asbury Park (closed 1977),"Asbury Park (NJ), USA",,,
    BAR,Baker AAF,"Baker Island (AK), USA",????,"Baker Island, United States Minor Outlying Islands",Type: Obsolete (see BAR.OLD)
    BSL,EuroAirport Swiss,"Basel/Mulhouse, Switzerland",EuroAirport,"Basel/Mulhouse/Freiburg, Alsace, France",Type: Alias (see MLH)
    CGX,Merrill C Meigs,"Chicago (IL), USA",Merrill C. Meigs Airport,"Chicago, Illinois, United States",Type: Obsolete (see CGX.OLD)
    CNN,Chulman,"Chulman, Russia",Chulman Neryungri Airport,"Cul'man, Sakha (Yakutiya), Russian Federation (Russia)",Type: Obsolete (see NER)
    CQM,Ciudad Real Central Airport (closed),"Ciudad Real, Spain",,,
    DOK,Donetsk Airport (closed since 26 May 2014),"Donetsk, Ukraine",,,
    FRJ,Frejus,"Frejus, France",????,"Fréjus/St-Raphaël, Provence-Alpes-Côte d'Azur, France",Type: Obsolete (see FRJ.OLD)
    FZO,Filton,"Filton, United Kingdom",Bristol Filton Airport,"Bristol, Gloucestershire, England, United Kingdom",Type: Obsolete (see FZO.OLD)
    GHK,Gush Katif,"Gush Katif, Israel",????,"Gush Katif, Israel",Type: Obsolete (see GHK.OLD)
    GOY,Amparai,"Gal Oya, Sri Lanka",Gal Oya Airport,"Ampara, Eastern Province, Sri Lanka (Ceylon)",Type: Obsolete (see ADP)
    GWW,RAF Gatow (former British Royal Air Force airfield),"Berlin, Germany",RAF Gatow,"Berlin, Berlin, Germany",Type: Obsolete (see GWW.OLD) / Old (Alt.): AB Berlin Gatow
    HEN,Hendon,"Hendon, United Kingdom",????,"Hendon, Middlesex, England, United Kingdom",Type: Obsolete (see HEN.OLD)
    HEW,Hellinikon Airport,"Athens, Greece",Hellenikon,"Athens, Attikí (Attica), Greece",Type: Obsolete (see HEW.OLD)
    HEX,Herrera International Airport (closed),"Santo Domingo, Dominican Republic",Herrera International Airport,"Santo Domingo, Distrito Nacional, Dominican Republic",Type: Obsolete (see HEX.OLD)
    HID,Horn Island,"Horn Island, Australia",Horn Island Airport,"Thursday Island, Queensland, Australia",Type: Alias (see TIS)
    HLY,Holyhead,"Holyhead, United Kingdom",Anglesey Airport/RAF Valley,"Anglesey, Anglesey, Wales, United Kingdom",Type: Obsolete (see VLY) / Old (Alt.): Maes Awyr Môn
    HTF,Hatfield,"Hatfield, United Kingdom",????,"Hatfield, Hertfordshire, England, United Kingdom",Type: Obsolete (see HTF.OLD)
    ILL,Willmar,"Willmar (MN), USA",Willmar Municipal-John L Rice Field,"Willmar, Minnesota, United States",Type: Obsolete (see ILL.OLD)
    INR,Kincheloe AFB,"Sault Ste Marie (MI), USA",Kincheloe AFB,"CIU: Sault Ste. Marie, Michigan, United States",Type: Obsolete (see INR.OLD)
    JGN,Jiayuguan Airport,"Jiayuguan, PR China",Jiayuguan,"Jiuquan, Gansu, China",Type: Alias (see ZLJQ)
    JNJ,Jaaluni,Duqm,Duqm International Airport,"Duqm, Oman",Type: Alias (see DQM) / Old (Alt.): Duqm Jaaluni Airport
    JON,Johnston Island,"Johnston Island, US Minor Outlying Islands",Johnston Atoll Airport,"Johnston Island, United States Minor Outlying Islands",Type: Obsolete (see JON.OLD)
    LID,Valkenburg Naval Air Base (closed),"Leiden, Netherlands",Valkenburg Naval AB,"Leiden, Zuid-Holland (South Holland), Netherlands",Type: Obsolete (see EHVB)
    MFW,Magaruque,"Magaruque, Mozambique",????,"Magaruque Island, Bazaruto Archipelago, Inhambane, Mozambique","Note: Airport closed / Type: Airport (Aerodrome, Airfield)"
    MPE,Griswold Airport (closed since the 2007),"Madison (CT), USA",,,
    NAT,Augusto Severo International Airport,"Natal, Brazil",Augusto Severo International Airport,"Natal, Rio Grande do Norte, Brazil","Note: Airport closed / Type: Airport (Aerodrome, Airfield) / Old (Alt.): Parnamirim Airport"
    NGZ,NAS,"Alameda (CA), USA",NAS Alameda,"Alameda, California, United States","Type: Obsolete (see NGZ.OLD) / Old (Alt.): Nimitz Field, Benton Field"
    NHA,Nha Trang,"Nha Trang, Viet Nam",????,"Nha Trang, Vietnam",Type: Obsolete (see NHA.OLD)
    NHZ,NAS,"Brunswick (ME), USA",Brunswick Executive Airport,"Brunswick, Maine, United States",Type: Obsolete (see BXM.FAA) / Old (Alt.): NAS Brunswick
    NIC,Nicosia,"Nicosia, Cyprus",Nicosia (Lefkosia)) International Airport,"Nicosia (Lefkosia)), Cyprus",Type: Obsolete (see NIC.OLD) / Old (Alt.): Athalassa
    NLP,Nelspruit Airport (replaced by the Kruger Mpumalanga International),"Nelspruit (Mbombela), South Africa",,,
    NRR,Roosevelt NAS,"Roosevelt Roads, Puerto Rico",Roosevelt Roads Naval Station - Ofstie Field,"Ceiba, Puerto Rico, United States",Type: Obsolete (see NRR.OLD)
    NXX,Willow Grove NAS,"Willow Grove (PA), USA",Willow Grove NAS JRB Airport,"Willow Grove, Pennsylvania, United States",Type: Obsolete (see NXX.OLD)
    PFN,Bay County,"Panama City (FL), USA",Panama City-Bay Co. International Airport,"Panama City, Florida, United States",Type: Obsolete (see PFN.OLD)
    SRF,Hamilton Field,"San Rafael (CA), USA",Hamilton AFB,"Novato, California, United States",Type: Obsolete (see SRF.OLD) / Old (Alt.): Hamilton AAF
    SSX,Samsun,"Samsun, Turkey",????,"Samsun, Samsun, Turkey",Type: Obsolete (see SSX.OLD)
    THF,Tempelhof (ceased operating in 2008),"Berlin, Germany",Tempelhof,"Berlin, Berlin, Germany",Type: Obsolete (see THF.OLD)
    TZN,South Andros,"South Andros, Bahamas",????,"South Andros, Andros Island, Bahamas",Type: Alias (see COX)
    UCA,Oneida County,"Utica (NY), USA",Oneida County Airport,"Utica, New York, United States",Type: Obsolete (see UCA.OLD)
    UTC,Soesterberg,"Utrecht, Netherlands",Soesterberg AB,"Utrecht, Utrecht, Netherlands",Type: Obsolete (see UTC.OLD)
    WOE,Woensdrecht AB,"Woensdrecht AB, Netherlands",Woensdrecht AB,"Bergen op Zoom, Noord-Brabant (North Brabant), Netherlands",Type: Alias (see BZM)
    YFJ,Snare Lake,"Snare Lake, Canada",La Macaza - Mont-Tremblant International Airport,"Mont-Tremblant, Québec, Canada",Type: Obsolete (see YTM) / Old (Alt.): Rivière Rouge - Mont-Tremblant International Airport
    YTU,Tasu,"Tasu, Canada",Tasu Water Aerodrome,"Tasu, British Columbia, Canada",Type: Obsolete (see YTU.OLD)
    YTX,Telegraph Creek,"Telegraph Creek, Canada",Telegraph Creek Airport,"Telegraph Creek, British Columbia, Canada",Type: Obsolete (see YTX.OLD)
    YXD,Municipal,"Edmonton, Canada",Edmonton City Centre Airport,"Edmonton, Alberta, Canada",Type: Obsolete (see YXD.OLD) / Old (Alt.): Blatchford Field
    YXI,Killaloe/Bonnechere Airport (ceased operations),"Killaloe, Canada",,,
    ZGC,Zhongchuan,"Lanzhou, PR China",Lanzhou Zhongchuan International Airport,"Lanzhou, Gansu, China",Type: Obsolete (see LHW) / Old (Alt.): Lanzhou West
    
... [1] `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

... [2] `Great Circle Mapper <http://www.gcmap.com/>`_

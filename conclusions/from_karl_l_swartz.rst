From: "Karl L. Swartz" <karl@kls2.com> (Date: Fri, 9 Sep 2016 12:59:33 -0700)
=============================================================================

    Thank you for the note!  I'm glad you like the Great Circle Mapper.
    
    The IATA Code Search ("IATA" for short) seems like it ought to be
    authoritative but it seems to have rather old and occasionally
    incomplete data.  I also check http://flighttools.oag.com/demo/ and its
    "Where Can I Fly Direct" feature and have found cases where IATA is
    missing a code but the OAG page has it, and various other sources
    strongly hint that it is real.
    
    Also note that a three-letter code may be an IATA code or an FAA LID
    (Location IDentifier is the term the FAA uses rather than code) and the
    same "code" in the two namespaces may refer to entirely different
    locations.  See http://www.gcmap.com/faq/locations#codes and the
    following two Q&A pairs.
    
    Moving on ...


CSL / MMSL:
---------------
    
    OWNO is obviously treating CSL as the FAA LID.  GCmap has that data as
    well, but you have to use CSL.FAA (or KCSL) to get it.
    
    Neither IATA nor OAG has CSL as a valid code, though some cached data
    from IATA from years ago has it as O'Sullivan AAF in San Luis Obispo,
    i.e., the same as CSL.FAA. 
    https://en.wikipedia.org/wiki/Cabo_San_Lucas_International_Airport says
    MMSL has no IATA code, but
    https://en.wikipedia.org/wiki/List_of_airports_in_Mexico has CSL as a
    code for that airport.  (Perhaps that is what you are trying to clean
    up.)
    
    For now, I've marked CSL as being an obsolete code for MMSL.
    
FAQ / NSFQ:
---------------

    This one is a bit of a mess.  FTI is the IATA code for Fitiuta per IATA
    (but not listed in OAG).  GCmap has that code, too, but also incorrectly
    listed FAQ as an IATA code for the airport (FAQ is the FAA LID) and that
    ended up being preferred.
    
    IATA says FAQ is the code for Freida River; OAG doesn't have that code
    either.  Several other sources, including Wikipedia as you cited, have
    Frieda River as the proper spelling.  I'm inclined to believe that IATA
    got the spelling wrong.  I've added FAQ / AYFR to the GCmap database
    with Frieda River as the preferred spelling but the IATA spelling as an
    alternate.
    
LIA / ZULP:
---------------

    At one point, IATA appears to have had LIA with Liangping as the airport
    name and Lima, Ohio as the city!  Another ever older cache of IATA data
    has LIA as Lima, Ohio.  My best guess is that LIA may once have been
    assigned to Lima, Ohio but IATA reassigned it to the airport in China. 
    With the explosion of new airports in China such reassignments have been
    rather common.
    
    I believe what GCmap currently has reflects the correct current
    assignment of LIA.
    
LPS (LPF / ZUPS):
---------------
    
    GCmap had the same airport under LPS and, in a new record, under LPF /
    ZUPS.  I've deleted the LPS record, which appears to have been a typo,
    perhaps based on sketchy data as it was created just a few months after
    the airport opened.
    
    Meanwhile, Wikipedia lists LPS as pointing to Lopez Island (IATA agrees,
    OAG doesn't know the code), as you note, and
    https://en.wikipedia.org/wiki/Fishermans_Bay/LPS_Seaplane_Base lists the
    FAA LID for that facility as WA81.  However, current FAA data lists WA81
    as being an airport (not a seaplane base) in Vancouver, Washington, over
    322 km away.  A search on the lat/lon that Wikipedia lists
    (http://www.gcmap.com/search?Q=3D48.516389N+122.918056W) suggests that
    the FAA LID is now 81W and AirNav (http://www.airnav.com/airport/81W)
    says 81W used to be WA81.  Kenmore Air flies there so it might have an
    IATA code, but there are two land airports nearby (4WA4 and S31) which
    could also be what IATA means.  4WA4 is private and thus is unlikely to
    have an IATA code but S31 is public.
    
    I'm going to guess that Wikipedia was correct in matching LPS to WA81
    and thus LPS should now be matched to 81W, and GCmap now has it that
    way.
    
MBM  / 6B6:
---------------

    Corrected the IATA code associated with 6B6 (FAA) to be MMN.
    
    As for MBM, Mkambati is a town in the Eastern Cape province of South
    Africa but there's no sign of an airport near there.  I'm checking with
    a contact in South Africa to see if I can learn more about it.  For now,
    GCmap has a placeholder where OpenAIP puts it.
    
MFW:
---------------

    MFW appears to have been reassigned; some old sources do list it as the
    city code that was in GCmap.
    
    Meanwhile, I've updated it to point to Magaruque Island, though the
    airport there appears to be closed.
    
MQJ / UEMA:
---------------

    Honuu, Khonu, and Khonuu are the result of different transliterations of
    Cyrillic to Roman alphabets.  There are a number of different mappings
    which result in multiple spellings on the same name.  See AIP Russia
    (http://www.caiga.ru/common/AirClassGDE/validaip4/aip/ad/ad2/uema/4-ad2-uema-txt.pdf)
    for the original form if you really want it!
    
NSA / SBNT:
---------------

    NSA came from an unreliable source, possibly a guess when NAT moved from
    the old airport (SBNT) to the new one (SBSG) in 2014.  I've removed it,
    and changed things so that NAT.OLD refers to the old international
    airport and SBNT refers to Natal AFB which remains at that location.
    
    GCmap has NSV / YNSH as referring to Noosaville, also just Noosa, in
    Australia.
    
OLI / POLI: 
---------------
   
    OLI is (was?) the FAA LID associated with POLI but never the IATA code
    for that location so far as I can tell.  Fixed.
    
    OLI (the IATA code) is properly associated with BIRF, Rif Airport in
    Ólafsvik, Iceland.  Fixed.
    
SFJ / BGSF:
---------------
    
    Name cleaned up.
    
SSD / SCSF:
---------------
    
    Cached old IATA data has SSD as San Felipe, Colombia (and SFH as San
    Felipe, México).  I believe the GCmap has both SSD and SFH in the
    correct countries.
    
    Thank you for all of your work!  All of the updates noted above should
    be live on the site.

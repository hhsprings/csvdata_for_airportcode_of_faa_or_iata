Found Errors of Great Circle Mapper or One World Nation's Online
================================================================

ABB,////,////,DNAS
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    ABB,////,////,DNAS,0.588,RAF Station,"Abingdon, United Kingdom",////,////,Asaba International Airport,"Asaba, Delta, Nigeria"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says ABB is of "Asaba International".

ARG,ARG,KARG,KARG
*****************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    ARG,ARG,KARG,KARG,0.326,Argyle International Airport,"Argyle, St. Vincent and the Grenadines",WALNUT RIDGE RGNL,"WALNUT RIDGE, AR - UNITED STATES",Walnut Ridge Regional,"Walnut Ridge, Arkansas, United States"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says ARG is of "Walnut Ridge Regional".

BBL,////,////,YLLE
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BBL,////,////,YLLE,0.769,Babolsar,"Babolsar, Iran",////,////,Ballera Airport,"Ballera, Queensland, Australia"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BBL is of "Ballera".

BEM,////,////,GMMD
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BEM,////,////,GMMD,0.5,Bossembele,"Bossembele, Central African Republic",////,////,Beni Mellal National Airport,"Beni Mellal, Morocco"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BENI is of "Beni Mellal National".

BFJ,////,////,ZUBJ
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BFJ,////,////,ZUBJ,0.182,Ba,"Ba, Fiji",////,////,Bijie Feixiong Airport,"Bijie, Guizhou, China"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BFJ is of "Bijie".

BGG,////,////,LTCU
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BGG,////,////,LTCU,0.833,Bongouanou,"Bongouanou, Cote d'Ivoire",////,////,Bingöl Airport,"Bingöl, Turkey"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BGG is of "Bingol" (Bingöl?).

BGN,////,////,UESG
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BGN,////,////,UESG,0.588,R.A.F.,"Brueggen, Germany",////,////,Belaya Gora Airport,"Belaya Gora, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BGN is of "Belaya Gora".

BKN,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BKN,////,////,////,0.526,Birni Nkoni,"Birni Nkoni, Niger",////,////,Balkanabat Airport,"Balkanabat, Balkan, Turkmenistan"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BKN is of "Balkanabat".

BPE,////,////,ZBDH
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    BPE,////,////,ZBDH,0.625,Bagan,"Bagan, Myanmar",////,////,Qinhuangdao Beidaihe Airport,"Qinhuangdao, Hubei, China"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says BPE is of "Qinhuangdao Beidaihe".

CSL,////,////,MMSL
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    CSL,////,////,MMSL,0.571,O'Sullivan AAF,"San Luis Obispo (CA), USA",////,////,Aeropuerto Cabo San Lucas,"Los Cabos, Baja California Sur, México"

Maybe both are wrong, or has problem. CSL is not found at `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.

`IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says:

.. csv-table::
    :header: CITY NAME, 3-LETTER CITY CODE, AIRPORT NAME, 3-LETTER AIRPORT CODE

    San Jose del Cabo, SJD, Los Cabos Intl, SJD
    San Luis Obispo, SBP, San Luis County Rgnl, SBP
    Jaen, JAE, Aeropuerto de Shumba, JAE
    Cabo Frio, CFB, International, CFB
    Pedernales, CBJ, Cabo Rojo, CBJ
    Puerto Aisen, WPA, Cabo Juan Roman, WPA
    San Jose del Cabo, SJD, Los Cabos Intl, SJD

`WIKIPEDIA page of Cabo San Lucas International Airport <https://en.wikipedia.org/wiki/Cabo_San_Lucas_International_Airport>`_ says it has no IATA code.

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).



FAQ,FAQ,NSFQ,NSFQ
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    FAQ,FAQ,NSFQ,NSFQ,0.326,Freida River,"Freida River, Papua New Guinea",FITIUTA,"FITIUTA VILLAGE, AS - UNITED STATES",????,"Fitiuta Village, American Samoa"

GCM's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says FAQ is of "Freida River". (According to WIKIPEDIA: Frieda River Airport (IATA: FAQ, ICAO: AYFR)). Maybe FITIUTA has no IATA.

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

GBB,////,////,UBBQ
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    GBB,////,////,UBBQ,0.6,Gara Djebilet,"Gara Djebilet, Algeria",////,////,Gabala International Airport,"Gabala, Qebele (Ibala), Azerbaijan"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says GBB is of "Gabala International".

GXH,////,////,ZLXH
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    GXH,////,////,ZLXH,0.471,NAF,"Mildenhall, United Kingdom",////,////,Gannan Xiahe Airport,"Xiahe, Gansu, China"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says GXH is of "Gannan Xiahe".

HBT,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    HBT,////,////,////,0.444,Hafr Albatin,"Hafr Albatin, Saudi Arabia",////,////,Hambantota Waterdrome,"Hambantota, Southern Province, Sri Lanka (Ceylon)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says HBT is of "Hambantota SPB".

JAE,////,////,SPJE
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    JAE,////,////,SPJE,0.486,Technology Park,"Atlanta (GA), USA",////,////,Shumba Airport,"Jaén, Cajamarca, Perú"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says JAE is of "Aeropuerto de Shumba, Jaen".

KCK,////,////,UIKK
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    KCK,////,////,UIKK,0.514,Fairfax Municipal,"Kansas City (KS), USA",////,////,Kirensk Airport,"Kirensk, Irkutskaya, Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says KCK is of "Kirensk".

KYS,////,////,GAKY
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    KYS,////,////,GAKY,0.462,Kars Airport,"Kars, Turkey",////,////,Dag Dag,"Kayes, Kayes, Mali"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says KYS is of "Dag-Dag, Kayes".

LIA,////,////,ZULP
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    LIA,////,////,ZULP,1.0,Liangping,"Lima (OH), USA",////,////,????,"Liangping, Sichuan, China"

I can't make a definitive statement which is right. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ just says LIA is of "Liangping", but it is clear for me that "Liangping" is Chinese, so maybe OWNO's error (just wrong location?)

- On Sat Sep 10, 2016: See `Karl's mail <from_karl_l_swartz.rst>`_.

LPS,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    LPS,////,////,////,0.529,Lopez Island,"Lopez Island (WA), USA",////,////,Liupanshui Yuezhao Airport,"Liupanshui, Guizhou, China"

GCM's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says LPS is of "Lopez Island". (IATA code of "Liupanshui Yue Zhao" is LPF.)

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

MBM,6B6,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    MBM,6B6,////,////,0.16,Mkambati,"Mkambati, South Africa",MINUTE MAN AIR FIELD,"STOW, MA - UNITED STATES",Minute Man Air Field,"Stow, Massachusetts, United States"

GCM's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says MBM is of "Mkambati". (IATA code of "Minute Man Airfield, Stow" is MMN.)

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

MFW,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    MFW,////,////,////,0.273,Magaruque,"Magaruque, Mozambique",////,////,Metropolitan Area,"Miami/Fort Lauderdale/West Palm Beach, Florida, United States"

GCM's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says MFW is of "Magaruque Island". (IATA code of "Metropolitan Area, Miami" is MIA.)

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

MHC,////,////,SCPQ
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    MHC,////,////,SCPQ,0.545,Macmahon Camp 4,"Macmahon Camp 4, Australia",////,////,Mocopulli Airport,"Dalcahue, Chiloé Island, Los Lagos, Chile"

Maybe OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says MHC is of "Mocopulli, Castro".

MNH,////,////,OORQ
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    MNH,////,////,OORQ,0.556,Minneriya,"Minneriya, Sri Lanka",////,////,Rustaq Airport,"Musanaa, Oman"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says MNH is of "Musanaa".

MQJ,////,////,UEMA
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    MQJ,////,////,UEMA,0.571,Merkez,"Balikesir, Turkey",////,////,Moma Airport,"Honuu, Sakha (Yakutiya), Russian Federation (Russia)"

At least, OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says MQJ is of "Moma, Khonu". "Khonu" is "Honuu"? I don't know, yet.

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

NOP,////,////,LTCM
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    NOP,////,////,LTCM,0.5,Nab,"Mactan Island, Philippines",////,////,Sinop Airport,"Sinop, Turkey"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says NOP is of "Sinop". (See also "SIC,////,////,////".)

NSA,////,////,SBNT
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    NSA,////,////,SBNT,0.273,Noosa,"Noosa, Australia",////,////,Augusto Severo International Airport,"Natal, Rio Grande do Norte, Brazil"

Maybe both are wrong, or has problem. NSA is not found at `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.

`IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says:

.. csv-table::
    :header: CITY NAME, 3-LETTER CITY CODE, AIRPORT NAME, 3-LETTER AIRPORT CODE

    Noosaville, NSV, Noosa, NSV
    Managua, MGA, Augusto C. Sandino, MGA
    Pedro Juan Caballero, PJC, Augusto Roberto Fuster, PJC
    Evensk, SWV, Severo-Evensk, SWV
    Severodonetsk, SEV, Severodonetsk, SEV
    Natal, NAT, International, NAT
    Puerto Natales, PNT, Teniente J. Gallardo, PNT
    Rio Grande, RGA, Hermes Quijada, RGA
    Rio Grande, RIG, Regional, RIG
    Juazeiro do Norte, JDO, O. Bezerra de Menezes, JDO
    Ourilandia do Norte, OIA, Ourilandia do Norte, OIA
    Porto Alegre do Norte, PBX, Fazenda Piraguassu, PBX

`WIKIPEDIA page of Augusto Severo International Airport <https://en.wikipedia.org/wiki/Augusto_Severo_International_Airport>`_ says it has (now?) no IATA code.

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

NZC,////,////,SPZA
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    NZC,////,////,SPZA,0.4,Cecil Field NAS,"Jacksonville (FL), USA",////,////,Nasca - María Reiche Neuman Airport,"Nazca, Ica, Perú"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says NZC is of "Maria Reiche Neuman, Nazca".

OLI,OLI,////,POLI
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    OLI,OLI,////,POLI,0.0,Rif,"Olafsvik, Iceland",////,////,????,????

GCM's error, maybe. At least, OWNO is right. Accoding to WIKIPEDIA, it is likely POLI is of "Oliktok Long Range Radar Site" (Facility?), and he says it has no IATA code.

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).

RIZ,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    RIZ,////,////,////,0.533,Rio Alzucar,"Rio Alzucar, Panama",////,////,Rizhao Shanzihe Airport,"Rizhao, Shandong, China"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says RIZ is of "Shanzihe, Rizhao".

SEK,////,////,UESK
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SEK,////,////,UESK,0.326,Ksar Es Souk,"Ksar Es Souk, Morocco",////,////,Srednekoly'msk Airport,"Srednekoly'msk, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SEK is of "Srednekolymsk".

SFJ,////,////,BGSF
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SFJ,////,////,BGSF,0.462,Kangerlussuaq,"St John Island, U.S. Virgin Islands",////,////,????,"Sondre Strømfjord, Qeqqata, Greenland"

Missing airport name is of GCM's problem, but location is right, and according to `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_, SFJ is actually of "Kangerlussuaq". Maybe OWNO's error (wrong location?).

- On Sat Sep 10, 2016: GCM's error was fixed (see `Karl's mail <from_karl_l_swartz.rst>`_).


SHO,////,////,FDSK
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SHO,////,////,FDSK,0.435,Solak,"Sokcho, South Korea",////,////,King Mswati III International Airport,"Manzini, Lubombo, Swaziland"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SHO is of "King Mswati III Intl, Manzini".

SIC,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SIC,////,////,////,0.333,Sinop Airport,"Sinop, Turkey",////,////,San José Island Airport,"San José Island, Panamá, Panamá"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SIC is of "San Jose Island". (IATA code of Sinop Airport is NOP. See also "NOP,////,////,LTCM".)

SLJ,////,////,YSOL
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SLJ,////,////,YSOL,0.65,Stellar Air Park,"Chandler (AZ), USA",////,////,Solomon Airport,"Solomon, Western Australia, Australia"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SLJ is of "Solomon".

SQJ,////,////,////
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SQJ,////,////,////,0.286,Shehdi,"Shehdi, Ethiopia",////,////,Sanming Shaxian Airport,"Sanming, Fujian, China"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SQJ is of "Shaxian, Sanming".

SSD,////,////,SCSF
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SSD,////,////,SCSF,0.621,San Felipe,"San Felipe, Colombia",////,////,Victor Lafón,"San Felipe, Valparaíso, Chile"

`IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ just says SSD is of "San Felipe", so he don't say which is right. "Google Maps" finds "San Felipe" at Mexico (Baja California), but it's IATA is SFH. "Google Maps" finds "San Felipe, Chile", its ICAO code is SCSF, and WIKIPADIA says its IATA code is SSD. Maybe GCM is right, and OWNO is wrong. (Incidentally, at least three places having name "San Felipe" are found at Colombia.)

- On Sat Sep 10, 2016: See `Karl's mail <from_karl_l_swartz.rst>`_.

SUK,////,////,UEBS
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SUK,////,////,UEBS,0.615,Samchok,"Samchok, South Korea",////,////,Sakkyryr Airport,"Batagay-Alyta, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SUK is of "Sakkyryr".

SUY,////,////,UENS
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SUY,////,////,UENS,0.615,Sudureyri,"Sudureyri, Iceland",////,////,Suntar Airport,"Suntar, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SUY is of "Suntar".

SYS,////,////,UERS
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    SYS,////,////,UERS,0.588,Yosu,"Sunchon, South Korea",////,////,????,"Saskylakh, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says SYS is of "Saskylakh".

TLK,////,////,UECT
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    TLK,////,////,UECT,0.5,Talknafjordur,"Talknafjordur, Iceland",////,////,Talakan Airport,"Talakan, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says TLK is of "Talakan".

URE,////,////,EEKE
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    URE,////,////,EEKE,1.0,Kuressaare,"Kuressaare, Ethiopia",////,////,????,"Kuressaare, Estonia"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ just says URE is of "Kuressaare". "Google Maps" and WIKIPEDIA says Kuressaare is in Estonia, not Ethiopia, so I think OWNO's error.

ZKP,////,////,UESU
******************
.. csv-table::
    :header: IATA code (GCM claims), FAA code, ICAO code (FAA claims), ICAO code (GCM claims), Degrees of Coincidence of names, name (OWNO), loc (OWNO), name (FAA), loc (FAA), name (GCM), city (GCM)

    ZKP,////,////,UESU,0.5,Kasompe,"Kasompe, Zambia",////,////,Zyryanka Airport,"Zyryanka, Sakha (Yakutiya), Russian Federation (Russia)"

OWNO's error. `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_ says ZKP is of "Zyryanka".

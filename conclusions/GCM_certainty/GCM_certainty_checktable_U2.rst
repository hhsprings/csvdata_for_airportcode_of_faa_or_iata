
List for checking certainty of Great Circle Mapper (UPB - UZU)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    UPB,////,////,MUPB,UNK,nan,////,////,////,////,////,Playa Baracoa,"Caimito, La Habana, Cuba",UNK
    UPG,////,////,WAAA,UNK,0.808,Hasanudin,"Ujung Pandang, Indonesia",OWNO,////,////,Sultan Hasanuddin International Airport,"Ujung Pandang, Sulawesi Selatan, Indonesia",OK
    UPL,////,////,MRUP,UNK,1.0,Upala,"Upala, Costa Rica",OWNO,////,////,????,"Upala, Alajuela, Costa Rica",OK
    UPN,////,////,MMPN,UNK,0.263,Uruapan,"Uruapan, Mexico",OWNO,////,////,Licenciado y General Ignacio López Rayón,"Uruapan, Michoacán de Ocampo, México",OK
    UPP,UPP,PHUP,PHUP,OK,0.64,Upolu Point,"Upolu Point (HI), USA",OWNO,UPOLU,"HAWI, HI - UNITED STATES",Upolu Airport,"Hawi, Hawaii, Hawaii, United States",OK
    UPR,////,////,////,UNK,1.0,Upiara,"Upiara, Papua New Guinea",OWNO,////,////,????,"Upiara, Western, Papua-New Guinea",OK
    URA,////,////,UARR,UNK,0.533,Uralsk,"Uralsk, Kazakhstan",OWNO,////,////,Oral Ak Zhol Airport,"Oral, Batys Qazaqstan, Kazakhstan",TO DO CHECK
    URB,////,////,SBUP,UNK,1.0,Ernesto Pochler,"Urubupunga, Brazil",OWNO,////,////,Ernesto Pochler,"Urubupunga, São Paulo, Brazil",OK
    URC,////,////,ZWWW,UNK,0.727,Urumqi,"Urumqi, PR China",OWNO,////,////,Ürümqi Diwopu International Airport,"Ürümqi, Xinjiang, China",OK
    URE,////,////,EEKE,UNK,1.0,Kuressaare,Kuressaare,IATA,////,////,????,"Kuressaare, Estonia",MAYBE
    URG,////,////,SBUG,UNK,1.0,Ruben Berta,"Uruguaiana, Brazil",OWNO,////,////,Rubem Berta,"Uruguaiana, Rio Grande do Sul, Brazil",OK
    URI,////,////,////,UNK,0.769,Uribe,"Uribe, Colombia",OWNO,////,////,????,"La Uribe, Meta, Colombia",MAYBE
    URJ,////,////,USHU,UNK,0.75,Uraj,"Uraj, Russia",OWNO,////,////,Uray Airport,"Uray, Khanty-Mansiyskiy, Russian Federation (Russia)",TO DO CHECK
    URM,////,////,SVUM,UNK,1.0,Uriman,"Uriman, Venezuela",OWNO,////,////,Uriman Airport,"Uriman, Bolívar, Venezuela",OK
    URN,////,////,OAOG,UNK,1.0,Urgoon,"Urgoon, Afghanistan",OWNO,////,////,????,"Urgoon, Paktika, Afghanistan",OK
    URO,////,////,LFOP,UNK,0.5,Boos,"Rouen, France",OWNO,////,////,Vallée de Seine,"Rouen, Haute-Normandie (Upper Normandy), France",OK
    URR,////,////,SKUR,UNK,1.0,Urrao,"Urrao, Colombia",OWNO,////,////,Urrao Airport,"Urrao, Antioquia, Colombia",OK
    URS,////,////,UUOK,UNK,0.526,Kursk,"Kursk, Russia",OWNO,////,////,Vostochny Airport,"Kursk, Kurskaya, Russian Federation (Russia)",OK
    URT,////,////,VTSB,UNK,1.0,Surat Thani,"Surat Thani, Thailand",OWNO,////,////,????,"Surat Thani, Surat Thani, Thailand",OK
    URY,////,////,OEGT,UNK,0.909,Gurayat,"Gurayat, Saudi Arabia",OWNO,////,////,Guriat Airport,"Guriat, Saudi Arabia",OK
    URZ,////,////,OARG,UNK,1.0,Uruzgan,"Uruzgan, Afghanistan",OWNO,////,////,????,"Uruzgan, Urozgan, Afghanistan",OK
    USA,JQF,KJQF,KJQF,OK,1.0,////,////,////,CONCORD RGNL,"CONCORD, NC - UNITED STATES",Concord Regional,"Concord, North Carolina, United States",OK
    USH,////,////,SAWH,UNK,0.733,Islas Malvinas,"Ushuaia, Argentina",OWNO,////,////,Islas Malvinas Argentinas International Airport,"Ushuaia, Tierra del Fuego, Argentina",OK
    USI,////,////,SYMB,UNK,1.0,Mabaruma,"Mabaruma, Guyana",OWNO,////,////,????,"Mabaruma, Barima-Waini, Guyana",OK
    USK,////,////,UUYS,UNK,1.0,Usinsk,"Usinsk, Russia",OWNO,////,////,Usinsk Airport,"Usinsk, Komi, Russian Federation (Russia)",OK
    USL,////,////,YUSL,UNK,1.0,Useless Loop,"Useless Loop, Australia",OWNO,////,////,????,"Useless Loop, Western Australia, Australia",OK
    USM,////,////,VTSM,UNK,1.0,Koh Samui,"Koh Samui, Thailand",OWNO,////,////,????,"Koh Samui, Nakhon Si Thammarat, Thailand",OK
    USN,////,////,RKPU,UNK,1.0,Ulsan,"Ulsan, South Korea",OWNO,////,////,????,"Ulsan, Republic of Korea (South Korea)",OK
    USO,////,////,////,UNK,1.0,Usino,"Usino, Papua New Guinea",OWNO,////,////,????,"Usino, Madang, Papua-New Guinea",OK
    USQ,////,////,LTBO,UNK,1.0,Usak,"Usak, Turkey",OWNO,////,////,????,"Usak, Usak, Turkey",OK
    USR,////,////,UEMT,UNK,nan,////,////,////,////,////,????,"Ust Nera, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    USS,////,////,MUSS,UNK,0.8,Sancti Spiritus,"Sancti Spiritus, Cuba",OWNO,////,////,Sancti Spíritus Airoprt,"Sancti Spíritus, Sancti Spíritus, Cuba",OK
    UST,SGJ,KSGJ,KSGJ,OK,0.572,St Augustine,"St Augustine (FL), USA",OWNO,NORTHEAST FLORIDA RGNL,"ST AUGUSTINE, FL - UNITED STATES",Northeast Florida Regional,"St. Augustine, Florida, United States",OK
    USU,////,////,RPVV,UNK,1.0,Busuanga,"Busuanga, Philippines",OWNO,////,////,????,"Busuanga, Philippines",OK
    UTA,////,////,FVMU,UNK,1.0,Mutare,"Mutare, Zimbabwe",OWNO,////,////,????,"Mutare, Zimbabwe",OK
    UTB,////,////,YMTB,UNK,1.0,Muttaburra,"Muttaburra, Australia",OWNO,////,////,????,"Muttaburra, Queensland, Australia",OK
    UTD,////,////,YNUT,UNK,1.0,Nutwood Downs,"Nutwood Downs, Australia",OWNO,////,////,????,"Nutwood Downs, Northern Territory, Australia",OK
    UTG,////,////,FXQG,UNK,1.0,Quthing,"Quthing, Lesotho",OWNO,////,////,????,"Quthing, Lesotho",OK
    UTH,////,////,VTUD,UNK,0.8,Udon Thani,"Udon Thani, Thailand",OWNO,////,////,Udon Thani International Airport,"Udon Thani, Udon Thani, Thailand",OK
    UTI,////,////,EFUT,UNK,1.0,Utti,"Kouvola, Finland",OWNO,////,////,Utti Airport,"Kouvola, Kymenlaakso (Kymmenedalen (Kymenlaakso)), Finland",OK
    UTK,03N,////,////,OK,1.0,Utirik Island,"Utirik Island, Marshall Islands",OWNO,UTIRIK,"UTIRIK ISLAND, - MARSHALL ISLANDS",Utirik Airport,"Utirik Island, Marshall Islands",OK
    UTM,UTA,KUTA,KUTA,OK,1.0,////,////,////,TUNICA MUNI,"TUNICA, MS - UNITED STATES",Tunica Municipal Airport,"Tunica, Mississippi, United States",OK
    UTN,////,////,FAUP,UNK,0.762,Upington,"Upington, South Africa",OWNO,////,////,Upington International Airport,"Upington, Northern Cape, South Africa",OK
    UTO,UTO,PAIM,PAIM,OK,0.785,Indian Mountain AFS,"Utopia Creek (AK), USA",OWNO,INDIAN MOUNTAIN LRRS,"UTOPIA CREEK, AK - UNITED STATES",Indian Mountain LRRS Airport,"Utopia Creek, Alaska, United States",OK
    UTP,////,////,VTBU,UNK,0.526,Utapao,"Utapao, Thailand",OWNO,////,////,U-Taphao International Airport,"Rayong, Rayong, Thailand",TO DO CHECK
    UTR,////,////,VTPU,UNK,1.0,Uttaradit,"Uttaradit, Thailand",OWNO,////,////,????,"Uttaradit, Uttaradit, Thailand",OK
    UTS,////,////,UUYX,UNK,nan,////,////,////,////,////,Ust'-Cil'ma Airport,"Ust'-Cil'ma, Komi, Russian Federation (Russia)",UNK
    UTT,////,////,FAUT,UNK,0.769,Umtata,"Umtata, South Africa",OWNO,////,////,Mthatha Airport,"Mthatha, Eastern Cape, South Africa",TO DO CHECK
    UTU,////,////,////,UNK,1.0,Ustupo,"Ustupo, Panama",OWNO,////,////,????,"Ustupo, Kuna Yala (Guna Yala), Panamá",OK
    UTW,////,////,FAQT,UNK,1.0,Queenstown,"Queenstown, South Africa",OWNO,////,////,Queenstown Airport,"Queenstown, Eastern Cape, South Africa",OK
    UUA,////,////,UWKB,UNK,1.0,Bugulma,"Bugulma, Russia",OWNO,////,////,Bugul'ma Airport,"Bugul'ma, Tatarstan, Russian Federation (Russia)",OK
    UUD,////,////,UIUU,UNK,0.571,Ulan-Ude,"Ulan-Ude, Russia",OWNO,////,////,Baikal International Airport,"Ulan-Ude, Buryatiya, Russian Federation (Russia)",OK
    UUK,UBW,PAKU,PAKU,OK,0.805,Kuparuk,"Kuparuk (AK), USA",OWNO,UGNU-KUPARUK,"KUPARUK, AK - UNITED STATES",Ugnu-Kuparuk Airport,"Kuparuk, Alaska, United States",OK
    UUN,////,////,ZMBU,UNK,1.0,Baruun-Urt,"Baruun-Urt, Mongolia",OWNO,////,////,????,"Baruun-Urt, Mongolia",OK
    UUS,////,////,UHSS,UNK,0.773,Yuzhno-Sakhalinsk,"Yuzhno-Sakhalinsk, Russia",OWNO,////,////,Khomutovo Airport,"Yuzhno-Sakhalinsk, Sakhalinskaya, Russian Federation (Russia)",OK
    UUU,////,////,////,UNK,1.0,Manumu,"Manumu, Papua New Guinea",OWNO,////,////,????,"Manumu, Central, Papua-New Guinea",OK
    UVA,UVA,KUVA,KUVA,OK,1.0,Garner Fld,"Uvalde (TX), USA",OWNO,GARNER FIELD,"UVALDE, - UNITED STATES",Garner Field,"Uvalde, Texas, United States",OK
    UVE,////,////,NWWV,UNK,0.6,Ouvea,"Ouvea, New Caledonia",OWNO,////,////,Ouloup,"Ouvéa, Fayaue Island, New Caledonia",OK
    UVF,////,////,TLPL,UNK,0.818,Hewanorra,"Saint Lucia, Saint Lucia",OWNO,////,////,Hewanorra International Airport,"St. Lucia, Saint Lucia",OK
    UVL,////,////,HEKG,UNK,0.471,Kharga,"Kharga, Egypt",OWNO,////,////,New Valley Airport,"El-Kharga, Al Wadi al Jadid (New Valley), Egypt",TO DO CHECK
    UVO,////,////,////,UNK,1.0,Uvol,"Uvol, Papua New Guinea",OWNO,////,////,????,"Uvol, East New Britain, Papua-New Guinea",OK
    UWA,MA53,////,////,OK,1.0,Ware,"Ware (MA), USA",OWNO,WARE,"WARE, MA - UNITED STATES",????,"Ware, Massachusetts, United States",OK
    UYL,////,////,HSNN,UNK,1.0,Nyala,"Nyala, Sudan",OWNO,////,////,????,"Nyala, South Darfur, Sudan",OK
    UYN,////,////,ZLYL,UNK,1.0,Yulin Yuyang Airport,"Yulin, Shaanxi, China",OWNO,////,////,Yulin Yuyang Airport,"Yulin, Shaanxi, China",OK
    UYU,////,////,SLUY,UNK,nan,////,////,////,////,////,Joya Andina Airport,"Uyuni, Quijarro, Potosí, Bolivia",UNK
    UZC,////,////,LYUZ,UNK,nan,////,////,////,////,////,Uzice-Ponikve,"Uzice, Serbia",UNK
    UZH,////,////,////,UNK,1.0,Unayzah,"Unayzah, Saudi Arabia",OWNO,////,////,????,"Unayzah, Saudi Arabia",OK
    UZU,////,////,SATU,UNK,1.0,Curuzu Cuatia,"Curuzu Cuatia, Argentina",OWNO,////,////,????,"Curuzú Cuatiá, Corrientes, Argentina",OK

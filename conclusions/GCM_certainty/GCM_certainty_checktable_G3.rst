
List for checking certainty of Great Circle Mapper (GRK - GZW)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    GRK,GRK,KGRK,KGRK,OK,0.674,Gray AAF,"Killeen (TX), USA",OWNO,ROBERT GRAY AAF,"FORT HOOD/KILLEEN, TX - UNITED STATES",Robert Gray AAF Airport,"Fort Hood/Killeen, Texas, United States",OK
    GRL,////,////,////,UNK,1.0,Garasa,"Garasa, Papua New Guinea",OWNO,////,////,????,"Garasa, Morobe, Papua-New Guinea",OK
    GRM,CKC,KCKC,KCKC,OK,1.0,Grand Marais/Cook County Airport,"Grand Marais (MN), USA",OWNO,GRAND MARAIS/COOK COUNTY,"GRAND MARAIS, MN - UNITED STATES",Grand Marais/Cook County Airport,"Grand Marais, Minnesota, United States",OK
    GRN,GRN,KGRN,KGRN,OK,0.734,Gordon,"Gordon (NE), USA",OWNO,GORDON MUNI,"GORDON, NE - UNITED STATES",Gordon Municipal Airport,"Gordon, Nebraska, United States",OK
    GRO,////,////,LEGE,UNK,1.0,Costa Brava,"Gerona, Spain",OWNO,////,////,Girona-Costa Brava,"Girona, Catalonia, Spain",OK
    GRP,////,////,SWGI,UNK,1.0,Gurupi,"Gurupi, Brazil",OWNO,////,////,????,"Gurupi, Goiás, Brazil",OK
    GRQ,////,////,EHGG,UNK,1.0,Eelde,"Groningen, Netherlands",OWNO,////,////,Eelde,"Groningen, Drenthe, Netherlands",OK
    GRR,GRR,KGRR,KGRR,OK,0.608,Kent County International,"Grand Rapids (MI), USA",OWNO,GERALD R FORD INTL,"GRAND RAPIDS, MI - UNITED STATES",Gerald R Ford International Airport,"Grand Rapids, Michigan, United States",OK
    GRS,////,////,LIRS,UNK,0.615,Baccarini,"Grosseto, Italy",OWNO,////,////,????,"Grosseto, Tuscany, Italy",OK
    GRT,////,////,////,UNK,1.0,Gujrat,"Gujrat, Pakistan",OWNO,////,////,????,"Gujrat, Punjab, Pakistan",OK
    GRU,////,////,SBGR,UNK,1.0,Guarulhos International,"Sao Paulo, Brazil",OWNO,////,////,Guarulhos International Airport,"São Paulo, São Paulo, Brazil",OK
    GRV,////,////,URMG,UNK,0.6,Groznyj,"Groznyj, Russia",OWNO,////,////,Severny Airport,"Grozny, Chechenskaya (Chechnya), Russian Federation (Russia)",TO DO CHECK
    GRW,////,////,LPGR,UNK,1.0,Graciosa Island,"Graciosa Island, Portugal",OWNO,////,////,????,"Graciosa Island, Região Autónoma dos Açores (Azores), Portugal",OK
    GRX,////,////,LEGR,UNK,1.0,Granada,"Granada, Spain",OWNO,////,////,????,"Granada, Andalusia, Spain",OK
    GRY,////,////,BIGR,UNK,1.0,Grimsey,"Grimsey, Iceland",OWNO,////,////,????,"Grímsey, Iceland",OK
    GRZ,////,////,LOWG,UNK,0.632,Thalerhof,"Graz, Austria",OWNO,////,////,Graz International Airport,"Graz, Steiermark (Styria), Austria",OK
    GSA,////,////,WBKN,UNK,1.0,Long Pasia,"Long Pasia, Malaysia",OWNO,////,////,????,"Long Pasia, Sabah, Malaysia",OK
    GSB,GSB,KGSB,KGSB,OK,1.0,Seymour Johnson AFB,"Goldsboro (NC), USA",OWNO,SEYMOUR JOHNSON AFB,"GOLDSBORO, NC - UNITED STATES",Seymour Johnson AFB,"Goldsboro, North Carolina, United States",OK
    GSC,////,////,YGSC,UNK,1.0,Gascoyne Junction,"Gascoyne Junction, Australia",OWNO,////,////,????,"Gascoyne Junction, Western Australia, Australia",OK
    GSE,////,////,ESGP,UNK,0.8,Saeve,"Gothenburg, Sweden",OWNO,////,////,Säve AB,"Göteborg, Västra Götalands län, Sweden",TO DO CHECK
    GSH,GSH,KGSH,KGSH,OK,0.719,Goshen,"Goshen (IN), USA",OWNO,GOSHEN MUNI,"GOSHEN, IN - UNITED STATES",Goshen Municipal Airport,"Goshen, Indiana, United States",OK
    GSI,////,////,////,UNK,1.0,Guadalcanal,"Guadalcanal, Solomon Islands",OWNO,////,////,????,"Guadalcanal, Solomon Islands",OK
    GSM,////,////,OIKQ,UNK,0.5,Gheshm,"Gheshm, Iran",OWNO,////,////,Dayrestan Airport,"Dayrestan, Queshm Island, Hormozgan, Iran",TO DO CHECK
    GSN,////,////,YMGN,UNK,1.0,Mount Gunson,"Mount Gunson, Australia",OWNO,////,////,????,"Mount Gunson, South Australia, Australia",OK
    GSO,GSO,KGSO,KGSO,OK,1.0,Piedmont Triad International Airport,"Greensboro/Winston Salem (NC), USA",OWNO,PIEDMONT TRIAD INTL,"GREENSBORO, NC - UNITED STATES",Piedmont Triad International Airport,"Greensboro, North Carolina, United States",OK
    GSP,GSP,KGSP,KGSP,OK,0.88,Greenville-Spartanbur Airport,"Greer (SC), USA",OWNO,GREENVILLE SPARTANBURG INTL,"GREER, SC - UNITED STATES",Greenville Spartanburg International Airport,"Greer, South Carolina, United States",OK
    GSQ,////,////,HEOW,UNK,0.722,Shark Elowainat,"Shark Elowainat, Egypt",OWNO,////,////,Sharq Al-Owainat International Airport,"Sharq Al-Owainat, Al Wadi al Jadid (New Valley), Egypt",TO DO CHECK
    GSR,////,////,HCMG,UNK,1.0,Gardo,"Gardo, Somalia",OWNO,////,////,????,"Gardo, Bari, Somalia",OK
    GSS,////,////,////,UNK,1.0,Sabi Sabi,"Sabi Sabi, South Africa",OWNO,////,////,????,"Sabi Sabi, Mpumalanga, South Africa",OK
    GST,GST,PAGS,PAGS,OK,1.0,Gustavus Airport,"Gustavus (AK), USA",OWNO,GUSTAVUS,"GUSTAVUS, AK - UNITED STATES",????,"Gustavus, Alaska, United States",OK
    GSU,////,////,HSGF,UNK,0.737,Gedaref,"Gedaref, Sudan",OWNO,////,////,Azaza,"Gedaref, Gedaref, Sudan",OK
    GTA,////,////,////,UNK,0.609,Gatokae Aerodrom,"Gatokae, Solomon Islands",OWNO,////,////,????,"Gatokae, Solomon Islands",OK
    GTE,////,////,YGTE,UNK,0.737,Alyangula,"Groote Eylandt, Australia",OWNO,////,////,????,"Groote Eylandt, Northern Territory, Australia",OK
    GTF,GTF,KGTF,KGTF,OK,1.0,International,"Great Falls (MT), USA",OWNO,GREAT FALLS INTL,"GREAT FALLS, MT - UNITED STATES",Great Falls International Airport,"Great Falls, Montana, United States",OK
    GTG,GTG,KGTG,KGTG,OK,1.0,Municipal,"Grantsburg (WI), USA",OWNO,GRANTSBURG MUNI,"GRANTSBURG, WI - UNITED STATES",Grantsburg Municipal Airport,"Grantsburg, Wisconsin, United States",OK
    GTI,////,////,EDCG,UNK,0.667,Guettin,"Guettin, Germany",OWNO,////,////,Rügen,"Güttin, Mecklenburg-Vorpommern, Germany",OK
    GTN,////,////,NZGT,UNK,1.0,Glentanner,"Mount Cook, New Zealand",OWNO,////,////,Glentanner,"Aoraki/Mount Cook, New Zealand",OK
    GTO,////,////,WAMG,UNK,0.75,Tolotio,"Gorontalo, Indonesia",OWNO,////,////,Jalaluddin,"Gorontalo, Gorontalo, Indonesia",OK
    GTP,3S8,////,////,OK,1.0,Grants Pass,Grants Pass,IATA,GRANTS PASS,"GRANTS PASS, OR - UNITED STATES",????,"Grants Pass, Oregon, United States",MAYBE
    GTR,GTR,KGTR,KGTR,OK,0.906,Golden Triangle Reg.,"Columbus (MS), USA",OWNO,GOLDEN TRIANGLE RGNL,"COLUMBUS/W POINT/STARKVILLE, MS - UNITED STATES",Golden Triangle Regional,"Columbus/West Point/Starkville, Mississippi, United States",OK
    GTS,////,////,YGDW,UNK,0.75,Granites,"Granites, Australia",OWNO,////,////,????,"Granite Downs, South Australia, Australia",MAYBE
    GTT,////,////,YGTN,UNK,1.0,Georgetown,"Georgetown, Australia",OWNO,////,////,????,"Georgetown, Queensland, Australia",OK
    GTW,////,////,LKHO,UNK,1.0,Holesov,"Zlin, Czech Republic",OWNO,////,////,????,"Holesov, Zlín, Czech Republic",OK
    GTY,W05,////,////,OK,0.814,Gettysburg,"Gettysburg (PA), USA",OWNO,GETTYSBURG RGNL,"GETTYSBURG, PA - UNITED STATES",Gettysburg Regional,"Gettysburg, Pennsylvania, United States",OK
    GUA,////,////,MGGT,UNK,0.947,La Aurora,"Guatemala City, Guatemala",OWNO,////,////,La Aurora International Airport,"Guatemala City, Guatemala, Guatemala",OK
    GUB,////,////,MMGR,UNK,1.0,Guerrero Negro,"Guerrero Negro, Mexico",OWNO,////,////,????,"Guerrero Negro, Baja California Sur, México",OK
    GUC,GUC,KGUC,KGUC,OK,0.465,Gunnison,"Gunnison (CO), USA",OWNO,GUNNISON-CRESTED BUTTE RGNL,"GUNNISON, CO - UNITED STATES",Gunnison-Crested Butte Regional,"Gunnison, Colorado, United States",OK
    GUD,////,////,GAGM,UNK,1.0,Goundam,"Goundam, Mali",OWNO,////,////,????,"Goundam, Gao, Mali",OK
    GUE,////,////,////,UNK,1.0,Guriaso,"Guriaso, Papua New Guinea",OWNO,////,////,????,"Guriaso, Sandaun, Papua-New Guinea",OK
    GUF,JKA,KJKA,KJKA,OK,0.865,Edwards,"Gulf Shores (AL), USA",OWNO,JACK EDWARDS,"GULF SHORES, AL - UNITED STATES",Jack Edwards Airport,"Gulf Shores, Alabama, United States",OK
    GUG,////,////,////,UNK,1.0,Guari,"Guari, Papua New Guinea",OWNO,////,////,????,"Guari, Central, Papua-New Guinea",OK
    GUH,////,////,YGDH,UNK,1.0,Gunnedah,"Gunnedah, Australia",OWNO,////,////,????,"Gunnedah, New South Wales, Australia",OK
    GUI,////,////,SVGI,UNK,1.0,Guiria,"Guiria, Venezuela",OWNO,////,////,????,"Guiria, Sucre, Venezuela",OK
    GUJ,////,////,SBGW,UNK,1.0,Guaratingueta,"Guaratingueta, Brazil",OWNO,////,////,????,"Guaratingueta, São Paulo, Brazil",OK
    GUL,////,////,YGLB,UNK,1.0,Goulburn,"Goulburn, Australia",OWNO,////,////,????,"Goulburn, New South Wales, Australia",OK
    GUM,GUM,PGUM,PGUM,OK,0.404,A.B. Won Pat International,"Guam, Guam",OWNO,GUAM INTL,"GUAM, GU - UNITED STATES",Guam International Airport,"Agana, Guam, United States",OK
    GUP,GUP,KGUP,KGUP,OK,0.314,Senator Clark,"Gallup (NM), USA",OWNO,GALLUP MUNI,"GALLUP, NM - UNITED STATES",Gallup Municipal Airport,"Gallup, New Mexico, United States",OK
    GUQ,////,////,SVGU,UNK,1.0,Guanare,"Guanare, Venezuela",OWNO,////,////,????,"Guanare, Portuguesa, Venezuela",OK
    GUR,////,////,AYGN,UNK,1.0,Gurney,"Alotau, Papua New Guinea",OWNO,////,////,Gurney,"Alotau, Milne Bay, Papua-New Guinea",OK
    GUS,GUS,KGUS,KGUS,OK,0.918,Grissom AFB,"Peru (IN), USA",OWNO,GRISSOM ARB,"PERU, IN - UNITED STATES",Grissom ARB,"Peru, Indiana, United States",OK
    GUT,////,////,ETUO,UNK,0.783,Guetersloh,"Guetersloh, Germany",OWNO,////,////,RAF,"Gütersloh, North Rhine-Westphalia, Germany",OK
    GUU,////,////,BIGF,UNK,1.0,Grundarfjordur,"Grundarfjordur, Iceland",OWNO,////,////,????,"Grundarfjörður, Iceland",OK
    GUV,////,////,////,UNK,1.0,Mougulu,"Mougulu, Papua New Guinea",OWNO,////,////,????,"Mougulu, Western, Papua-New Guinea",OK
    GUW,////,////,UATG,UNK,1.0,Atyrau,"Atyrau, Kazakhstan",OWNO,////,////,Atyrau Airport,"Atyrau, Atyraü, Kazakhstan",OK
    GUX,////,////,VAGN,UNK,1.0,Guna,"Guna, India",OWNO,////,////,????,"Guna, Madhya Pradesh, India",OK
    GUY,GUY,KGUY,KGUY,OK,0.826,Guymon,"Guymon (OK), USA",OWNO,GUYMON MUNI,"GUYMON, OK - UNITED STATES",Guymon Municipal Airport,"Guymon, Oklahoma, United States",OK
    GVA,////,////,LSGG,UNK,1.0,Geneve-Cointrin,"Geneva, Switzerland",OWNO,////,////,Geneve-Cointrin,"Geneva, Genève, Switzerland",OK
    GVE,GVE,KGVE,KGVE,OK,1.0,Municipal,"Gordonsville (VA), USA",OWNO,GORDONSVILLE MUNI,"GORDONSVILLE, VA - UNITED STATES",Gordonsville Municipal Airport,"Gordonsville, Virginia, United States",OK
    GVI,////,////,////,UNK,1.0,Green River,"Green River, Papua New Guinea",OWNO,////,////,????,"Green River, Sandaun, Papua-New Guinea",OK
    GVL,GVL,KGVL,KGVL,OK,1.0,Lee Gilmer Memorial,"Gainesville (GA), USA",OWNO,LEE GILMER MEMORIAL,"GAINESVILLE, GA - UNITED STATES",Lee Gilmer Memorial Airport,"Gainesville, Georgia, United States",OK
    GVP,////,////,YGNV,UNK,1.0,Greenvale,"Greenvale, Australia",OWNO,////,////,????,"Greenvale, Queensland, Australia",OK
    GVR,////,////,SBGV,UNK,1.0,Governador Valadares,"Governador Valadares, Brazil",OWNO,////,////,????,"Governador Valadares, Minas Gerais, Brazil",OK
    GVT,GVT,KGVT,KGVT,OK,0.845,Majors Field,"Greenville (TX), USA",OWNO,MAJORS,"GREENVILLE, TX - UNITED STATES",Majors Airport,"Greenville, Texas, United States",OK
    GVX,////,////,ESSK,UNK,1.0,Sandviken,"Gavle, Sweden",OWNO,////,////,????,"Gävle-Sandviken, Gävleborgs län, Sweden",TO DO CHECK
    GWA,////,////,VYGW,UNK,1.0,Gwa,"Gwa, Myanmar",OWNO,////,////,????,"Gwa, Rakhine, Myanmar (Burma)",OK
    GWD,////,////,OPGD,UNK,1.0,Gwadar,"Gwadar, Pakistan",OWNO,////,////,????,"Gwadar, Balochistan, Pakistan",OK
    GWE,////,////,FVTL,UNK,0.471,Gweru,"Gweru, Zimbabwe",OWNO,////,////,Thornhill AB,"Gweru, Zimbabwe",OK
    GWL,////,////,VIGR,UNK,1.0,Gwalior,"Gwalior, India",OWNO,////,////,????,"Gwalior, Madhya Pradesh, India",OK
    GWN,////,////,////,UNK,1.0,Gnarowein,"Gnarowein, Papua New Guinea",OWNO,////,////,????,"Gnarowein, Morobe, Papua-New Guinea",OK
    GWO,GWO,KGWO,KGWO,OK,1.0,Leflore,"Greenwood (MS), USA",OWNO,GREENWOOD-LEFLORE,"GREENWOOD, MS - UNITED STATES",Greenwood-Leflore Airport,"Greenwood, Mississippi, United States",OK
    GWS,GWS,KGWS,KGWS,OK,0.834,Glenwood Springs,"Glenwood Springs (CO), USA",OWNO,GLENWOOD SPRINGS MUNI,"GLENWOOD SPRINGS, CO - UNITED STATES",Glenwood Springs Municipal Airport,"Glenwood Springs, Colorado, United States",OK
    GWT,////,////,EDXW,UNK,1.0,Sylt Airport,"Westerland, Germany",OWNO,////,////,Sylt Airport,"Sylt, Sylt Island, Schleswig-Holstein, Germany",OK
    GWV,WV66,////,////,OK,0.626,Glendale,"Glendale (WV), USA",OWNO,GLENDALE FOKKER FIELD,"GLENDALE, WV - UNITED STATES",Glendale Fokker Field,"Glendale, West Virginia, United States",OK
    GWY,////,////,EICM,UNK,0.818,Carnmore,"Galway, Ireland",OWNO,////,////,????,"Galway, County Galway, Connacht, Ireland",OK
    GXF,////,////,OYSY,UNK,0.615,Seiyun,"Seiyun, Yemen",OWNO,////,////,Sayun International Airport,"Sayun, Yemen",OK
    GXG,////,////,FNNG,UNK,1.0,Negage,"Negage, Angola",OWNO,////,////,????,"Negage, Angola",OK
    GXH,////,////,ZLXH,UNK,1.0,Gannan Xiahe,Xiahe,IATA,////,////,Gannan Xiahe Airport,"Xiahe, Gansu, China",MAYBE
    GXQ,////,////,SCCY,UNK,0.884,Ten. Vidal,"Coyhaique, Chile",OWNO,////,////,Teniente Vidal Airport,"Coyhaique, Aysén del General Carlos Ibáñez del Campo, Chile",OK
    GXX,////,////,FKKJ,UNK,1.0,Yagoua,"Yagoua, Cameroon",OWNO,////,////,Yagoua Airport,"Yagoua, Far North (Extrême-Nord), Cameroon",OK
    GXY,GXY,KGXY,KGXY,OK,1.0,Weld County,"Greeley (CO), USA",OWNO,GREELEY-WELD COUNTY,"GREELEY, CO - UNITED STATES",Greeley-Weld County Airport,"Greeley, Colorado, United States",OK
    GYA,////,////,SLGY,UNK,1.0,Guayaramerin,"Guayaramerin, Bolivia",OWNO,////,////,Guayaramerín Airport,"Guayaramerín, Vaca Díez, El Beni, Bolivia",OK
    GYD,////,////,UBBB,UNK,nan,////,////,////,////,////,Heydar Aliyev International Airport,"Baku, Baki (Baku), Azerbaijan",UNK
    GYE,////,////,SEGU,UNK,0.531,Simon Bolivar,"Guayaquil, Ecuador",OWNO,////,////,José Joaquín de Olmdeo International Airport,"Guayaquil, Guayas, Ecuador",OK
    GYG,////,////,UEMM,UNK,nan,////,////,////,////,////,Magan Airport,"Magan, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    GYI,////,////,HRYG,UNK,1.0,Gisenyi,"Gisenyi, Rwanda",OWNO,////,////,????,"Gisenyi, Rwanda",OK
    GYL,////,////,YARG,UNK,1.0,Argyle,"Argyle, Australia",OWNO,////,////,????,"Argyle, Western Australia, Australia",OK
    GYM,////,////,MMGM,UNK,0.787,Gen Jose M Yanez,"Guaymas, Mexico",OWNO,////,////,General José María Yáñez International Airport,"Guaymas, Sonora, México",OK
    GYN,////,////,SBGO,UNK,1.0,Santa Genoveva,"Goiania, Santa Genoveva Airport, Brazil",OWNO,////,////,Santa Genoveva,"Goiania, Goiás, Brazil",OK
    GYP,////,////,YGYM,UNK,1.0,Gympie,"Gympie, Australia",OWNO,////,////,????,"Gympie, Queensland, Australia",OK
    GYR,GYR,KGYR,KGYR,OK,0.549,Litchfield,"Goodyear (AZ), USA",OWNO,PHOENIX GOODYEAR,"GOODYEAR, AZ - UNITED STATES",Phoenix Goodyear Airport,"Goodyear, Arizona, United States",OK
    GYS,////,////,ZUGU,UNK,1.0,Guang Yuan,"Guang Yuan, PR China",OWNO,////,////,????,"Guangyuan, Sichuan, China",OK
    GYU,////,////,ZLGY,UNK,nan,////,////,////,////,////,Guyuan Liupanshan Airport,"Guyuan, Ningxia, China",UNK
    GYY,GYY,KGYY,KGYY,OK,0.686,Regional,"Gary (IN), USA",OWNO,GARY/CHICAGO INTL,"GARY, IN - UNITED STATES",Gary/Chicago International Airport,"Gary, Indiana, United States",OK
    GZA,////,////,LVGZ,UNK,0.688,Gaza International Airport,"Gaza City, Palestinian Territory",OWNO,////,////,Yaser Arafat International Airport,"Gaza, Palestine",OK
    GZI,////,////,OAGN,UNK,1.0,Ghazni,"Ghazni, Afghanistan",OWNO,////,////,????,"Ghazni, Ghazni, Afghanistan",OK
    GZM,////,////,LMMG,UNK,0.333,Gozo,"Gozo, Malta",OWNO,////,////,Xewkija Heliport,"Gozo, Malta",OK
    GZO,////,////,AGGN,UNK,0.471,Gizo,"Gizo, Solomon Islands",OWNO,////,////,Nusatupe,"Gizo, Nusatupe Island, Solomon Islands",OK
    GZP,////,////,LTFG,UNK,nan,////,////,////,////,////,????,"Gazipasa, Antalya, Turkey",UNK
    GZT,////,////,LTAJ,UNK,0.727,Gaziantep,"Gaziantep, Turkey",OWNO,////,////,Oguzeli,"Gaziantep, Gaziantep, Turkey",OK
    GZW,////,////,OIIK,UNK,nan,////,////,////,////,////,????,"Qazvin, Zanjan, Iran",UNK

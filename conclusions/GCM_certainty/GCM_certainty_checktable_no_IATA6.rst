
List for checking certainty of Great Circle Mapper (FAA LID P13 - FAA LID TAN)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,P13,////,////,OK,1.0,////,////,////,SAN CARLOS APACHE,"GLOBE, AZ - UNITED STATES",San Carlos Apache Airport,"Globe, Arizona, United States",OK
    ////,P14,////,////,OK,1.0,////,////,////,HOLBROOK MUNI,"HOLBROOK, AZ - UNITED STATES",Holbrook Municipal Airport,"Holbrook, Arizona, United States",OK
    ////,PBH,KPBH,KPBH,OK,1.0,////,////,////,PRICE COUNTY,"PHILLIPS, WI - UNITED STATES",Price County Airport,"Phillips, Wisconsin, United States",OK
    ////,PBX,KPBX,KPBX,OK,1.0,////,////,////,PIKE COUNTY-HATCHER FIELD,"PIKEVILLE, KY - UNITED STATES",Pike County-Hatcher Field,"Pikeville, Kentucky, United States",OK
    ////,PCM,KPCM,KPCM,OK,1.0,////,////,////,PLANT CITY,"PLANT CITY, FL - UNITED STATES",????,"Plant City, Florida, United States",OK
    ////,PCW,KPCW,KPCW,OK,1.0,////,////,////,ERIE-OTTAWA INTL,"PORT CLINTON, OH - UNITED STATES",Erie-Ottawa International Airport,"Port Clinton, Ohio, United States",OK
    ////,PCZ,KPCZ,KPCZ,OK,1.0,////,////,////,WAUPACA MUNI,"WAUPACA, WI - UNITED STATES",Waupaca Municipal Airport,"Waupaca, Wisconsin, United States",OK
    ////,PEA,KPEA,KPEA,OK,1.0,////,////,////,PELLA MUNI,"PELLA, IA - UNITED STATES",Pella Municipal Airport,"Pella, Iowa, United States",OK
    ////,PEO,KPEO,KPEO,OK,1.0,////,////,////,PENN YAN,"PENN YAN, NY - UNITED STATES",????,"Penn Yan, New York, United States",OK
    ////,PEX,KPEX,KPEX,OK,1.0,////,////,////,PAYNESVILLE MUNI,"PAYNESVILLE, MN - UNITED STATES",Paynesville Municipal Airport,"Paynesville, Minnesota, United States",OK
    ////,PEZ,KPEZ,KPEZ,OK,1.0,////,////,////,PLEASANTON MUNI,"PLEASANTON, TX - UNITED STATES",Pleasanton Municipal Airport,"Pleasanton, Texas, United States",OK
    ////,PHG,KPHG,KPHG,OK,1.0,////,////,////,PHILLIPSBURG MUNI,"PHILLIPSBURG, KS - UNITED STATES",Phillipsburg Municipal Airport,"Phillipsburg, Kansas, United States",OK
    ////,PIL,KPIL,KPIL,OK,1.0,////,////,////,PORT ISABEL-CAMERON COUNTY,"PORT ISABEL, TX - UNITED STATES",Port Isabel-Cameron County Airport,"Port Isabel, Texas, United States",OK
    ////,PJC,KPJC,KPJC,OK,1.0,////,////,////,ZELIENOPLE MUNI,"ZELIENOPLE, PA - UNITED STATES",Zelienople Municipal Airport,"Zelienople, Pennsylvania, United States",OK
    ////,PKV,KPKV,KPKV,OK,1.0,////,////,////,CALHOUN COUNTY,"PORT LAVACA, TX - UNITED STATES",Calhoun County Airport,"Port Lavaca, Texas, United States",OK
    ////,PLU,KPLU,KPLU,OK,1.0,////,////,////,PIERCE COUNTY - THUN FIELD,"PUYALLUP, WA - UNITED STATES",Pierce County - Thun Field,"Puyallup, Washington, United States",OK
    ////,PMU,KPMU,KPMU,OK,1.0,////,////,////,PANOLA COUNTY,"BATESVILLE, MS - UNITED STATES",Panola County Airport,"Batesville, Mississippi, United States",OK
    ////,PMV,KPMV,KPMV,OK,1.0,////,////,////,PLATTSMOUTH MUNI,"PLATTSMOUTH, NE - UNITED STATES",Plattsmouth Municipal Airport,"Plattsmouth, Nebraska, United States",OK
    ////,PNA,KPNA,KPNA,OK,1.0,////,////,////,RALPH WENZ FIELD,"PINEDALE, - UNITED STATES",Ralph Wenz Field,"Pinedale, Wyoming, United States",OK
    ////,PNM,KPNM,KPNM,OK,1.0,////,////,////,PRINCETON MUNI,"PRINCETON, MN - UNITED STATES",Princeton Municipal Airport,"Princeton, Minnesota, United States",OK
    ////,PNT,KPNT,KPNT,OK,1.0,////,////,////,PONTIAC MUNI,"PONTIAC, IL - UNITED STATES",Pontiac Municipal Airport,"Pontiac, Illinois, United States",OK
    ////,POV,KPOV,KPOV,OK,1.0,////,////,////,PORTAGE COUNTY,"RAVENNA, OH - UNITED STATES",Portage County Airport,"Ravenna, Ohio, United States",OK
    ////,PPQ,KPPQ,KPPQ,OK,1.0,////,////,////,PITTSFIELD PENSTONE MUNI,"PITTSFIELD, IL - UNITED STATES",Pittsfield Penstone Municipal Airport,"Pittsfield, Illinois, United States",OK
    ////,PQN,KPQN,KPQN,OK,1.0,////,////,////,PIPESTONE MUNI,"PIPESTONE, MN - UNITED STATES",Pipestone Municipal Airport,"Pipestone, Minnesota, United States",OK
    ////,PRG,KPRG,KPRG,OK,1.0,////,////,////,EDGAR COUNTY,"PARIS, IL - UNITED STATES",Edgar County Airport,"Paris, Illinois, United States",OK
    ////,PRN,KPRN,KPRN,OK,1.0,////,////,////,MAC CRENSHAW MEMORIAL,"GREENVILLE, AL - UNITED STATES",Mac Crenshaw Memorial Airport,"Greenville, Alabama, United States",OK
    ////,PRS,KPRS,KPRS,OK,1.0,////,////,////,PRESIDIO LELY INTL,"PRESIDIO, TX - UNITED STATES",Presidio Lely International Airport,"Presidio, Texas, United States",OK
    ////,PRZ,KPRZ,KPRZ,OK,1.0,////,////,////,PORTALES MUNI,"PORTALES, NM - UNITED STATES",Portales Municipal Airport,"Portales, New Mexico, United States",OK
    ////,PUJ,KPUJ,KPUJ,OK,0.778,////,////,////,PAULDING NORTHWEST ATLANTA,"ATLANTA, GA - UNITED STATES",Silver Comet Field at Paulding Northwest Atlanta Airport,"Atlanta, Georgia, United States",OK
    ////,PVB,KPVB,KPVB,OK,1.0,////,////,////,PLATTEVILLE MUNI,"PLATTEVILLE, WI - UNITED STATES",Platteville Municipal Airport,"Platteville, Wisconsin, United States",OK
    ////,PVE,KPVE,KPVE,OK,1.0,////,////,////,BEECH RIVER RGNL,"LEXINGTON-PARSONS, TN - UNITED STATES",Beech River Regional,"Lexington-Parsons, Tennessee, United States",OK
    ////,PVG,KPVG,KPVG,OK,1.0,////,////,////,HAMPTON ROADS EXECUTIVE,"NORFOLK, VA - UNITED STATES",Hampton Roads Executive Airport,"Norfolk, Virginia, United States",OK
    ////,PVJ,KPVJ,KPVJ,OK,1.0,////,////,////,PAULS VALLEY MUNI,"PAULS VALLEY, OK - UNITED STATES",Pauls Valley Municipal Airport,"Pauls Valley, Oklahoma, United States",OK
    ////,PWC,KPWC,KPWC,OK,1.0,////,////,////,PINE RIVER RGNL,"PINE RIVER, MN - UNITED STATES",Pine River Regional,"Pine River, Minnesota, United States",OK
    ////,PWG,KPWG,KPWG,OK,1.0,////,////,////,MC GREGOR EXECUTIVE,"WACO, TX - UNITED STATES",Mc Gregor Executive Airport,"Waco, Texas, United States",OK
    ////,PXE,KPXE,KPXE,OK,1.0,////,////,////,PERRY-HOUSTON COUNTY,"PERRY, GA - UNITED STATES",Perry-Houston County Airport,"Perry, Georgia, United States",OK
    ////,PYN,KPYN,KPYN,OK,1.0,////,////,////,PIEDMONT MUNI,"PIEDMONT, MO - UNITED STATES",Piedmont Municipal Airport,"Piedmont, Missouri, United States",OK
    ////,PYP,KPYP,KPYP,OK,1.0,////,////,////,CENTRE-PIEDMONT-CHEROKEE COUNTY RGNL,"CENTRE, AL - UNITED STATES",Centre-Piedmont-Cherokee County Regional,"Centre, Alabama, United States",OK
    ////,PYX,KPYX,KPYX,OK,1.0,////,////,////,PERRYTON OCHILTREE COUNTY,"PERRYTON, TX - UNITED STATES",Perryton Ochiltree County Airport,"Perryton, Texas, United States",OK
    ////,PZQ,KPZQ,KPZQ,OK,1.0,////,////,////,PRESQUE ISLE COUNTY,"ROGERS CITY, MI - UNITED STATES",Presque Isle County Airport,"Rogers City, Michigan, United States",OK
    ////,R49,////,////,OK,1.0,////,////,////,FERRY COUNTY,"REPUBLIC, WA - UNITED STATES",Ferry County Airport,"Republic, Washington, United States",OK
    ////,RAS,KRAS,KRAS,OK,1.0,////,////,////,MUSTANG BEACH,"PORT ARANSAS, TX - UNITED STATES",Mustang Beach Airport,"Port Aransas, Texas, United States",OK
    ////,RAW,KRAW,KRAW,OK,1.0,////,////,////,WARSAW MUNI,"WARSAW, MO - UNITED STATES",Warsaw Municipal Airport,"Warsaw, Missouri, United States",OK
    ////,RBE,KRBE,KRBE,OK,1.0,////,////,////,ROCK COUNTY,"BASSETT, NE - UNITED STATES",Rock County Airport,"Bassett, Nebraska, United States",OK
    ////,RBM,KRBM,KRBM,OK,1.0,////,////,////,ROBINSON AAF/NG,"CAMP ROBINSON/LITTLE ROCK, AR - UNITED STATES",Robinson AAF/NG Airport,"Camp Robinson/Little Rock, Arkansas, United States",OK
    ////,RBO,KRBO,KRBO,OK,1.0,////,////,////,NUECES COUNTY,"ROBSTOWN, TX - UNITED STATES",Nueces County Airport,"Robstown, Texas, United States",OK
    ////,RCE,KRCE,KRCE,OK,1.0,////,////,////,CLARENCE E PAGE MUNI,"OKLAHOMA CITY, OK - UNITED STATES",Clarence E Page Municipal Airport,"Oklahoma City, Oklahoma, United States",OK
    ////,RCM,KRCM,KRCM,OK,1.0,////,////,////,SKYHAVEN,"WARRENSBURG, MO - UNITED STATES",Skyhaven Airport,"Warrensburg, Missouri, United States",OK
    ////,RCV,KRCV,KRCV,OK,1.0,////,////,////,ASTRONAUT KENT ROMINGER,"DEL NORTE, CO - UNITED STATES",Astronaut Kent Rominger Airport,"Del Norte, Colorado, United States",OK
    ////,RCX,KRCX,KRCX,OK,1.0,////,////,////,RUSK COUNTY,"LADYSMITH, WI - UNITED STATES",Rusk County Airport,"Ladysmith, Wisconsin, United States",OK
    ////,RCZ,KRCZ,KRCZ,OK,1.0,////,////,////,RICHMOND COUNTY,"ROCKINGHAM, NC - UNITED STATES",Richmond County Airport,"Rockingham, North Carolina, United States",OK
    ////,RDK,KRDK,KRDK,OK,1.0,////,////,////,RED OAK MUNI,"RED OAK, IA - UNITED STATES",Red Oak Municipal Airport,"Red Oak, Iowa, United States",OK
    ////,RDR,KRDR,KRDR,OK,1.0,////,////,////,GRAND FORKS AFB,"GRAND FORKS, ND - UNITED STATES",Grand Forks AFB,"Grand Forks, North Dakota, United States",OK
    ////,REI,KREI,KREI,OK,1.0,////,////,////,REDLANDS MUNI,"REDLANDS, CA - UNITED STATES",Redlands Municipal Airport,"Redlands, California, United States",OK
    ////,RFI,KRFI,KRFI,OK,1.0,////,////,////,RUSK COUNTY,"HENDERSON, TX - UNITED STATES",Rusk County Airport,"Henderson, Texas, United States",OK
    ////,RGK,KRGK,KRGK,OK,1.0,////,////,////,RED WING RGNL,"RED WING, MN - UNITED STATES",Red Wing Regional,"Red Wing, Minnesota, United States",OK
    ////,RHP,KRHP,KRHP,OK,1.0,////,////,////,WESTERN CAROLINA RGNL,"ANDREWS, NC - UNITED STATES",Western Carolina Regional,"Andrews, North Carolina, United States",OK
    ////,RIU,KRIU,KRIU,OK,1.0,////,////,////,RANCHO MURIETA,"RANCHO MURIETA, CA - UNITED STATES",????,"Rancho Murieta, California, United States",OK
    ////,RMN,KRMN,KRMN,OK,1.0,////,////,////,STAFFORD RGNL,"STAFFORD, VA - UNITED STATES",Stafford Regional,"Stafford, Virginia, United States",OK
    ////,RMY,KRMY,KRMY,OK,1.0,////,////,////,BROOKS FIELD,"MARSHALL, MI - UNITED STATES",Brooks Field,"Marshall, Michigan, United States",OK
    ////,RNM,KRNM,KRNM,OK,1.0,////,////,////,RAMONA,"RAMONA, CA - UNITED STATES",????,"Ramona, California, United States",OK
    ////,RNP,KRNP,KRNP,OK,1.0,////,////,////,OWOSSO COMMUNITY,"OWOSSO, MI - UNITED STATES",Owosso Community Airport,"Owosso, Michigan, United States",OK
    ////,RNV,KRNV,KRNV,OK,1.0,////,////,////,CLEVELAND MUNI,"CLEVELAND, MS - UNITED STATES",Cleveland Municipal Airport,"Cleveland, Mississippi, United States",OK
    ////,ROI,PKRO,PKRO,OK,0.593,////,////,////,DYESS AAF,"ROI-NAMUR, - MARSHALL ISLANDS",Freeflight International Airport,"Roi-Namur, Kwajalein Atoll, Marshall Islands",OK
    ////,ROS,KROS,KROS,OK,1.0,////,////,////,RUSH CITY RGNL,"RUSH CITY, MN - UNITED STATES",Rush City Regional,"Rush City, Minnesota, United States",OK
    ////,RPH,KRPH,KRPH,OK,1.0,////,////,////,GRAHAM MUNI,"GRAHAM, TX - UNITED STATES",Graham Municipal Airport,"Graham, Texas, United States",OK
    ////,RPJ,KRPJ,KRPJ,OK,1.0,////,////,////,ROCHELLE MUNI AIRPORT-KORITZ FIELD,"ROCHELLE, IL - UNITED STATES",Rochelle Municipal-Koritz Field,"Rochelle, Illinois, United States",OK
    ////,RQE,KRQE,KRQE,OK,1.0,////,////,////,WINDOW ROCK,"WINDOW ROCK, AZ - UNITED STATES",????,"Window Rock, Arizona, United States",OK
    ////,RSV,KRSV,KRSV,OK,0.939,////,////,////,CRAWFORD CO,"ROBINSON, IL - UNITED STATES",Crawford County Airport,"Robinson, Illinois, United States",OK
    ////,RTS,KRTS,KRTS,OK,1.0,////,////,////,RENO/STEAD,"RENO, NV - UNITED STATES",Reno/Stead Airport,"Reno, Nevada, United States",OK
    ////,RUE,KRUE,KRUE,OK,1.0,////,////,////,RUSSELLVILLE RGNL,"RUSSELLVILLE, AR - UNITED STATES",Russellville Regional,"Russellville, Arkansas, United States",OK
    ////,RUG,KRUG,KRUG,OK,1.0,////,////,////,RUGBY MUNI,"RUGBY, ND - UNITED STATES",Rugby Municipal Airport,"Rugby, North Dakota, United States",OK
    ////,RVR,TJRV,TJRV,OK,1.0,////,////,////,JOSE APONTE DE LA TORRE,"CEIBA, PR - UNITED STATES",José Aponte de la Torre,"Ceiba, Puerto Rico, United States",OK
    ////,RWV,KRWV,KRWV,OK,1.0,////,////,////,CALDWELL MUNI,"CALDWELL, TX - UNITED STATES",Caldwell Municipal Airport,"Caldwell, Texas, United States",OK
    ////,RXE,KRXE,KRXE,OK,1.0,////,////,////,REXBURG-MADISON COUNTY,"REXBURG, ID - UNITED STATES",Rexburg-Madison County Airport,"Rexburg, Idaho, United States",OK
    ////,RYM,KRYM,KRYM,OK,1.0,////,////,////,RAY S MILLER AAF,"CAMP RIPLEY, MN - UNITED STATES",Ray S Miller AAF Airport,"Camp Ripley, Minnesota, United States",OK
    ////,RYN,KRYN,KRYN,OK,1.0,////,////,////,RYAN FIELD,"TUCSON, AZ - UNITED STATES",Ryan Field,"Tucson, Arizona, United States",OK
    ////,RYV,KRYV,KRYV,OK,1.0,////,////,////,WATERTOWN MUNI,"WATERTOWN, WI - UNITED STATES",Watertown Municipal Airport,"Watertown, Wisconsin, United States",OK
    ////,RYW,KRYW,KRYW,OK,1.0,////,////,////,LAGO VISTA TX - RUSTY ALLEN,"LAGO VISTA, TX - UNITED STATES",Lago Vista TX - Rusty Allen Airport,"Lago Vista, Texas, United States",OK
    ////,RYY,KRYY,KRYY,OK,1.0,////,////,////,COBB COUNTY INTL-MCCOLLUM FIELD,"ATLANTA, GA - UNITED STATES",Cobb County International-McCollum Field,"Atlanta, Georgia, United States",OK
    ////,RZN,KRZN,KRZN,OK,1.0,////,////,////,BURNETT COUNTY,"SIREN, WI - UNITED STATES",Burnett County Airport,"Siren, Wisconsin, United States",OK
    ////,RZR,KRZR,KRZR,OK,1.0,////,////,////,CLEVELAND RGNL JETPORT,"CLEVELAND, TN - UNITED STATES",Cleveland Regional Jetport,"Cleveland, Tennessee, United States",OK
    ////,RZT,KRZT,KRZT,OK,1.0,////,////,////,ROSS COUNTY,"CHILLICOTHE, OH - UNITED STATES",Ross County Airport,"Chillicothe, Ohio, United States",OK
    ////,S30,////,////,OK,1.0,////,////,////,LEBANON STATE,"LEBANON, OR - UNITED STATES",Lebanon State Airport,"Lebanon, Oregon, United States",OK
    ////,S31,////,////,OK,1.0,////,////,////,LOPEZ ISLAND,"LOPEZ, WA - UNITED STATES",????,"Lopez Island, Washington, United States",OK
    ////,S40,////,////,OK,1.0,////,////,////,PROSSER,"PROSSER, WA - UNITED STATES",????,"Prosser, Washington, United States",OK
    ////,SAZ,KSAZ,KSAZ,OK,1.0,////,////,////,STAPLES MUNI,"STAPLES, MN - UNITED STATES",Staples Municipal Airport,"Staples, Minnesota, United States",OK
    ////,SBO,KSBO,KSBO,OK,1.0,////,////,////,EAST GEORGIA REGIONAL,"SWAINSBORO, GA - UNITED STATES",East Georgia Regional,"Swainsboro, Georgia, United States",OK
    ////,SCA,KSCA,KSCA,OK,1.0,////,////,////,SIDNEY MUNI,"SIDNEY, OH - UNITED STATES",Sidney Municipal Airport,"Sidney, Ohio, United States",OK
    ////,SCD,KSCD,KSCD,OK,1.0,////,////,////,MERKEL FIELD SYLACAUGA MUNI,"SYLACAUGA, AL - UNITED STATES",Merkel Field Sylacauga Municipal Airport,"Sylacauga, Alabama, United States",OK
    ////,SCR,KSCR,KSCR,OK,1.0,////,////,////,SILER CITY MUNI,"SILER CITY, NC - UNITED STATES",Siler City Municipal Airport,"Siler City, North Carolina, United States",OK
    ////,SDA,KSDA,KSDA,OK,1.0,////,////,////,SHENANDOAH MUNI,"SHENANDOAH, IA - UNITED STATES",Shenandoah Municipal Airport,"Shenandoah, Iowa, United States",OK
    ////,SDC,KSDC,KSDC,OK,1.0,////,////,////,WILLIAMSON-SODUS,"WILLIAMSON/SODUS, NY - UNITED STATES",Williamson-Sodus Airport,"Williamson/Sodus, New York, United States",OK
    ////,SET,KSET,KSET,OK,1.0,////,////,////,ST CHARLES COUNTY SMARTT,"ST CHARLES, MO - UNITED STATES",St. Charles County Smartt Airport,"St. Charles, Missouri, United States",OK
    ////,SFQ,KSFQ,KSFQ,OK,1.0,////,////,////,SUFFOLK EXECUTIVE,"SUFFOLK, VA - UNITED STATES",Suffolk Executive Airport,"Suffolk, Virginia, United States",OK
    ////,SFY,KSFY,KSFY,OK,1.0,////,////,////,TRI-TOWNSHIP,"SAVANNA, IL - UNITED STATES",Tri-Township Airport,"Savanna, Illinois, United States",OK
    ////,SGS,KSGS,KSGS,OK,1.0,////,////,////,SOUTH ST PAUL MUNI-RICHARD E FLEMING FLD,"SOUTH ST PAUL, MN - UNITED STATES",South St. Paul Municipal-Richard E Fleming Field,"South St. Paul, Minnesota, United States",OK
    ////,SHL,KSHL,KSHL,OK,1.0,////,////,////,SHELDON MUNI,"SHELDON, IA - UNITED STATES",Sheldon Municipal Airport,"Sheldon, Iowa, United States",OK
    ////,SIF,KSIF,KSIF,OK,1.0,////,////,////,ROCKINGHAM COUNTY NC SHILOH,"REIDSVILLE, NC - UNITED STATES",Rockingham County NC Shiloh Airport,"Reidsville, North Carolina, United States",OK
    ////,SJS,KSJS,KSJS,OK,1.0,////,////,////,BIG SANDY RGNL,"PRESTONSBURG, KY - UNITED STATES",Big Sandy Regional,"Prestonsburg, Kentucky, United States",OK
    ////,SJX,KSJX,KSJX,OK,1.0,////,////,////,BEAVER ISLAND,"BEAVER ISLAND, MI - UNITED STATES",????,"Beaver Island, Michigan, United States",OK
    ////,SLH,KSLH,KSLH,OK,1.0,////,////,////,CHEBOYGAN COUNTY,"CHEBOYGAN, MI - UNITED STATES",Cheboygan County Airport,"Cheboygan, Michigan, United States",OK
    ////,SLI,KSLI,KSLI,OK,1.0,////,////,////,LOS ALAMITOS AAF,"LOS ALAMITOS, CA - UNITED STATES",Los Alamitos AAF Airport,"Los Alamitos, California, United States",OK
    ////,SMQ,KSMQ,KSMQ,OK,1.0,////,////,////,SOMERSET,"SOMERVILLE, NJ - UNITED STATES",Somerset Airport,"Somerville, New Jersey, United States",OK
    ////,SNC,KSNC,KSNC,OK,1.0,////,////,////,CHESTER,"CHESTER, CT - UNITED STATES",????,"Chester, Connecticut, United States",OK
    ////,SNH,KSNH,KSNH,OK,1.0,////,////,////,SAVANNAH-HARDIN COUNTY,"SAVANNAH, TN - UNITED STATES",Savannah-Hardin County Airport,"Savannah, Tennessee, United States",OK
    ////,SOA,KSOA,KSOA,OK,1.0,////,////,////,SONORA MUNI,"SONORA, TX - UNITED STATES",Sonora Municipal Airport,"Sonora, Texas, United States",OK
    ////,SPB,KSPB,KSPB,OK,1.0,////,////,////,SCAPPOOSE INDUSTRIAL AIRPARK,"SCAPPOOSE, OR - UNITED STATES",Scappoose Industrial Airpark,"Scappoose, Oregon, United States",OK
    ////,SPH,KSPH,KSPH,OK,1.0,////,////,////,SPRINGHILL,"SPRINGHILL, LA - UNITED STATES",????,"Springhill, Louisiana, United States",OK
    ////,SPZ,KSPZ,KSPZ,OK,1.0,////,////,////,SILVER SPRINGS,"SILVER SPRINGS, NV - UNITED STATES",????,"Silver Springs, Nevada, United States",OK
    ////,SRB,KSRB,KSRB,OK,1.0,////,////,////,UPPER CUMBERLAND RGNL,"SPARTA, TN - UNITED STATES",Upper Cumberland Regional,"Sparta, Tennessee, United States",OK
    ////,SSQ,KSSQ,KSSQ,OK,1.0,////,////,////,SHELL LAKE MUNI,"SHELL LAKE, WI - UNITED STATES",Shell Lake Municipal Airport,"Shell Lake, Wisconsin, United States",OK
    ////,STF,KSTF,KSTF,OK,1.0,////,////,////,GEORGE M BRYAN,"STARKVILLE, MS - UNITED STATES",George M Bryan Airport,"Starkville, Mississippi, United States",OK
    ////,SUO,KSUO,KSUO,OK,1.0,////,////,////,ROSEBUD SIOUX TRIBAL,"ROSEBUD, SD - UNITED STATES",Rosebud Sioux Tribal Airport,"Rosebud, South Dakota, United States",OK
    ////,SUT,KSUT,KSUT,OK,1.0,////,////,////,CAPE FEAR RGNL JETPORT/HOWIE FRANKLIN FLD,"OAK ISLAND, NC - UNITED STATES",Cape Fear Regional Jetport/Howie Franklin Field,"Oak Island, North Carolina, United States",OK
    ////,SUZ,KSUZ,KSUZ,OK,1.0,////,////,////,SALINE COUNTY RGNL,"BENTON, AR - UNITED STATES",Saline County Regional,"Benton, Arkansas, United States",OK
    ////,SYF,KSYF,KSYF,OK,1.0,////,////,////,CHEYENNE COUNTY MUNI,"ST FRANCIS, KS - UNITED STATES",Cheyenne County Municipal Airport,"St. Francis, Kansas, United States",OK
    ////,SYM,KSYM,KSYM,OK,1.0,////,////,////,MOREHEAD-ROWAN COUNTY CLYDE A THOMAS RGNL,"MOREHEAD, KY - UNITED STATES",Morehead-Rowan County Clyde A Thomas Regional,"Morehead, Kentucky, United States",OK
    ////,SZT,KSZT,KSZT,OK,1.0,////,////,////,SANDPOINT,"SANDPOINT, ID - UNITED STATES",????,"Sandpoint, Idaho, United States",OK
    ////,TAN,KTAN,KTAN,OK,1.0,////,////,////,TAUNTON MUNI - KING FIELD,"TAUNTON, MA - UNITED STATES",Taunton Municipal - King Field,"Taunton, Massachusetts, United States",OK

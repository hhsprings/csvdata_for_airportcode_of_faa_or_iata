
List for checking certainty of Great Circle Mapper (MLD - MOQ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MLD,MLD,KMLD,KMLD,OK,1.0,Malad City,"Malad City (ID), USA",OWNO,MALAD CITY,"MALAD CITY, ID - UNITED STATES",????,"Malad City, Idaho, United States",OK
    MLE,////,////,VRMM,UNK,0.571,International,"Male, Maldives",OWNO,////,////,Ibrahim Nasir International Airport,"Malé, Hulhulé Island, Malé (Maale), Maldives",OK
    MLF,MLF,KMLF,KMLF,OK,0.734,Milford,"Milford (UT), USA",OWNO,MILFORD MUNI/BEN AND JUDY BRISCOE FIELD,"MILFORD, UT - UNITED STATES",Milford Municipal/Ben and Judy Briscoe Field,"Milford, Utah, United States",OK
    MLG,////,////,WARA,UNK,1.0,Abdul Rachman Saleh,"Malang, Indonesia",OWNO,////,////,Abdul Rachman Saleh,"Malang, Jawa Timur, Indonesia",OK
    MLH,////,////,LFSB,UNK,1.0,EuroAirport,"Mulhouse/Basel, France/Switzerland",OWNO,////,////,EuroAirport,"Basel/Mulhouse/Freiburg, Alsace, France",OK
    MLI,MLI,KMLI,KMLI,OK,0.826,Quad-City,"Moline (IL), USA",OWNO,QUAD CITY INTL,"MOLINE, IL - UNITED STATES",Quad City International Airport,"Moline, Illinois, United States",OK
    MLJ,MLJ,KMLJ,KMLJ,OK,1.0,Baldwin County,"Milledgeville (GA), USA",OWNO,BALDWIN COUNTY,"MILLEDGEVILLE, GA - UNITED STATES",Baldwin County Airport,"Milledgeville, Georgia, United States",OK
    MLK,M75,////,////,OK,1.0,Malta,"Malta (MT), USA",OWNO,MALTA,"MALTA, MT - UNITED STATES",????,"Malta, Montana, United States",OK
    MLL,MDM,PADM,PADM,OK,0.498,Marshall,"Marshall (AK), USA",OWNO,MARSHALL DON HUNTER SR,"MARSHALL, AK - UNITED STATES",Marshall Don Hunter Sr. Airport,"Marshall, Alaska, United States",OK
    MLM,////,////,MMMM,UNK,0.304,Morelia,"Morelia, Mexico",OWNO,////,////,General Francisco J. Mujica International Airport,"Morelia, Michoacán de Ocampo, México",OK
    MLN,////,////,GEML,UNK,1.0,Melilla,"Melilla, Spain",OWNO,////,////,????,"Melilla, Melilla, Spain",OK
    MLO,////,////,LGML,UNK,1.0,Milos,"Milos, Greece",OWNO,////,////,????,"Milos, Notío Aigaío (Southern Aegean), Greece",OK
    MLP,////,////,RPMM,UNK,1.0,Malabang,"Malabang, Philippines",OWNO,////,////,????,"Malabang, Mindanao, Philippines",OK
    MLQ,////,////,////,UNK,1.0,Malalaua,"Malalaua, Papua New Guinea",OWNO,////,////,????,"Malalaua, Gulf, Papua-New Guinea",OK
    MLR,////,////,YMCT,UNK,1.0,Millicent,"Millicent, Australia",OWNO,////,////,????,"Millicent, South Australia, Australia",OK
    MLS,MLS,KMLS,KMLS,OK,0.484,Municipal,"Miles City (MT), USA",OWNO,FRANK WILEY FIELD,"MILES CITY, MT - UNITED STATES",Frank Wiley Field,"Miles City, Montana, United States",OK
    MLT,MLT,KMLT,KMLT,OK,0.805,Millinocket,"Millinocket (ME), USA",OWNO,MILLINOCKET MUNI,"MILLINOCKET, ME - UNITED STATES",Millinocket Municipal Airport,"Millinocket, Maine, United States",OK
    MLU,MLU,KMLU,KMLU,OK,0.734,Municipal,"Monroe (LA), USA",OWNO,MONROE RGNL,"MONROE, LA - UNITED STATES",Monroe Regional,"Monroe, Louisiana, United States",OK
    MLV,////,////,YMEU,UNK,1.0,Merluna,"Merluna, Australia",OWNO,////,////,????,"Merluna, Queensland, Australia",OK
    MLW,////,////,GLMR,UNK,1.0,Sprigg Payne,"Monrovia, Liberia",OWNO,////,////,Spriggs Payne,"Monrovia, Liberia",OK
    MLX,////,////,LTAT,UNK,0.7,Malatya,"Malatya, Turkey",OWNO,////,////,Erhac,"Malatya, Malatya, Turkey",OK
    MLY,MLY,PAML,PAML,OK,1.0,Manley Hot Springs,"Manley Hot Springs (AK), USA",OWNO,MANLEY HOT SPRINGS,"MANLEY HOT SPRINGS, - UNITED STATES",????,"Manley Hot Springs, Alaska, United States",OK
    MLZ,////,////,SUMO,UNK,0.4,Melo,"Melo, Uruguay",OWNO,////,////,Cerro Largo,"Melo, Cerro Largo, Uruguay",OK
    MMA,////,////,////,UNK,1.0,Metropolitan Area,"Malmo, Sweden",OWNO,////,////,Metropolitan Area,"Malmö, Sweden",OK
    MMB,////,////,RJCM,UNK,1.0,Memanbetsu,"Memanbetsu, Japan",OWNO,////,////,Memanbetsu Airport,"Ozora, Hokkaido, Japan",OK
    MMC,////,////,MMDM,UNK,1.0,Ciudad Mante,"Ciudad Mante, Mexico",OWNO,////,////,????,"Ciudad Mante, Tamaulipas, México",OK
    MMD,////,////,ROMD,UNK,0.759,Maridor,"Minami Daito, Japan",OWNO,////,////,Minami Daito Airport,"Minamidaito, Minamidaito Jima, Okinawa, Japan",OK
    MME,////,////,EGNV,UNK,0.519,Teesside International,"Teesside, United Kingdom",OWNO,////,////,Durham Tees Valley Airport,"Teesside, Durham, England, United Kingdom",OK
    MMF,////,////,FKKF,UNK,1.0,Mamfe,"Mamfe, Cameroon",OWNO,////,////,Mamfe Airport,"Mamfe, South-West (Sud-Ouest), Cameroon",OK
    MMG,////,////,YMOG,UNK,1.0,Mount Magnet,"Mount Magnet, Australia",OWNO,////,////,????,"Mount Magnet, Western Australia, Australia",OK
    MMH,MMH,KMMH,KMMH,OK,0.757,Mammoth Lakes,"Mammoth Lakes (CA), USA",OWNO,MAMMOTH YOSEMITE,"MAMMOTH LAKES, CA - UNITED STATES",Mammoth Yosemite Airport,"Mammoth Lakes, California, United States",OK
    MMI,MMI,KMMI,KMMI,OK,1.0,McMinn County,"Athens (TN), USA",OWNO,MCMINN COUNTY,"ATHENS, TN - UNITED STATES",McMinn County Airport,"Athens, Tennessee, United States",OK
    MMJ,////,////,RJAF,UNK,1.0,Matsumoto,"Matsumoto, Japan",OWNO,////,////,Matsumoto Airport,"Matsumoto, Nagano, Japan",OK
    MMK,////,////,ULMM,UNK,1.0,Murmansk,"Murmansk, Russia",OWNO,////,////,Murmansk Airport,"Murmansk, Murmanskaya, Russian Federation (Russia)",OK
    MML,MML,KMML,KMML,OK,0.848,Municipal-Ryan Field,"Marshall (MN), USA",OWNO,SOUTHWEST MINNESOTA RGNL MARSHALL/RYAN FLD,"MARSHALL, MN - UNITED STATES",Southwest Minnesota Regional Marshall/Ryan Field,"Marshall, Minnesota, United States",OK
    MMM,////,////,YMMU,UNK,1.0,Middlemount,"Middlemount, Australia",OWNO,////,////,????,"Middlemount, Queensland, Australia",OK
    MMN,6B6,////,////,OK,1.0,Minute Man Airfield,"Stow (MA), USA",OWNO,MINUTE MAN AIR FIELD,"STOW, MA - UNITED STATES",Minute Man Air Field,"Stow, Massachusetts, United States",OK
    MMO,////,////,GVMA,UNK,1.0,Vila Do Maio,"Maio, Cape Verde",OWNO,////,////,Maio,"Vila do Maio, Maio, Cape Verde",OK
    MMP,////,////,SKMP,UNK,0.48,Mompos,"Mompos, Colombia",OWNO,////,////,San Bernardo Airport,"Mompós, Bolívar, Colombia",OK
    MMQ,////,////,FLBA,UNK,1.0,Mbala,"Mbala, Zambia",OWNO,////,////,????,"Mbala, Zambia",OK
    MMR,TX26,////,KATT,NG,0.615,Camp Maybry AHP,"Austin (TX), USA",OWNO,MABRY ARNG,"AUSTIN, TX - UNITED STATES",Camp Maybry AHP,"Austin, Texas, United States",OK
    MMS,MMS,KMMS,KMMS,OK,1.0,Selfs,"Marks (MS), USA",OWNO,SELFS,"MARKS, MS - UNITED STATES",Selfs Airport,"Marks, Mississippi, United States",OK
    MMT,MMT,KMMT,KMMT,OK,0.602,Mc Entire ANG Base,"Columbia (SC), USA",OWNO,MC ENTIRE JNGB,"EASTOVER, SC - UNITED STATES",Mc Entire JNGB Airport,"Eastover, South Carolina, United States",OK
    MMU,MMU,KMMU,KMMU,OK,1.0,Municipal,"Morristown (NJ), USA",OWNO,MORRISTOWN MUNI,"MORRISTOWN, NJ - UNITED STATES",Morristown Municipal Airport,"Morristown, New Jersey, United States",OK
    MMW,////,////,////,UNK,1.0,Moma,"Moma, Mozambique",OWNO,////,////,????,"Moma, Mozambique",OK
    MMX,////,////,ESMS,UNK,0.588,Malmö Airport,"Malmö, Sweden",OWNO,////,////,Malmö-Sturup Airport,"Malmö, Skåne län, Sweden",OK
    MMY,////,////,ROMY,UNK,0.514,Hirara,"Miyako Jima, Japan",OWNO,////,////,Miyako Airport,"Miyakojima, Miyakojima Island, Okinawa, Japan",OK
    MMZ,////,////,OAMN,UNK,1.0,Maimana,"Maimana, Afghanistan",OWNO,////,////,????,"Maimana, Faryab, Afghanistan",OK
    MNA,////,////,WAMN,UNK,1.0,Melangguane,"Melangguane, Indonesia",OWNO,////,////,Melangguane Airport,"Melangguane, Sulawesi Utara, Indonesia",OK
    MNB,////,////,FZAG,UNK,1.0,Moanda,"Moanda, Congo (DRC)",OWNO,////,////,Muanda Airport,"Muanda, Bas-Congo, Democratic Republic of Congo (Zaire)",OK
    MNC,////,////,FQNC,UNK,1.0,Nacala,"Nacala, Mozambique",OWNO,////,////,????,"Nacala, Mozambique",OK
    MND,////,////,////,UNK,1.0,Medina,"Medina, Colombia",OWNO,////,////,????,"Medina, Cundinamarca, Colombia",OK
    MNE,////,////,YMUG,UNK,1.0,Mungeranie,"Mungeranie, Australia",OWNO,////,////,????,"Mungeranie, South Australia, Australia",OK
    MNF,////,////,NFMA,UNK,0.71,Mana Island Airstrip,"Mana Island, Fiji",OWNO,////,////,????,"Mana Island, Fiji",OK
    MNG,////,////,YMGD,UNK,1.0,Maningrida,"Maningrida, Australia",OWNO,////,////,????,"Maningrida, Northern Territory, Australia",OK
    MNH,////,////,OORQ,UNK,0.667,Musanaa,Musanaa,IATA,////,////,Rustaq Airport,"Musanaa, Oman",MAYBE
    MNI,////,////,TRPG,UNK,0.538,Bramble,"Montserrat, Montserrat",OWNO,////,////,John A. Osborne Airport,"Gerald's Park, Montserrat",TO DO CHECK
    MNJ,////,////,FMSM,UNK,1.0,Mananjary,"Mananjary, Madagascar",OWNO,////,////,????,"Mananjary, Madagascar",OK
    MNK,////,////,NGMA,UNK,1.0,Maiana,"Maiana, Kiribati",OWNO,////,////,????,"Maiana, Kiribati",OK
    MNL,////,////,RPLL,UNK,1.0,Ninoy Aquino International,"Manila, Philippines",OWNO,////,////,Ninoy Aquino International Airport,"Manila, Philippines",OK
    MNM,MNM,KMNM,KMNM,OK,0.386,Menominee,"Menominee (MI), USA",OWNO,MENOMINEE-MARINETTE TWIN COUNTY,"MENOMINEE, MI - UNITED STATES",Menominee-Marinette Twin County Airport,"Menominee, Michigan, United States",OK
    MNN,MNN,KMNN,KMNN,OK,1.0,Municipal,"Marion (OH), USA",OWNO,MARION MUNI,"MARION, OH - UNITED STATES",Marion Municipal Airport,"Marion, Ohio, United States",OK
    MNO,////,////,FZRA,UNK,1.0,Manono,"Manono, Congo (DRC)",OWNO,////,////,Manono Airport,"Manono, Katanga, Democratic Republic of Congo (Zaire)",OK
    MNP,////,////,////,UNK,1.0,Maron,"Maron, Papua New Guinea",OWNO,////,////,Maron Airport,"Maron Island, Manus, Papua-New Guinea",OK
    MNQ,////,////,YMTO,UNK,1.0,Monto,"Monto, Australia",OWNO,////,////,????,"Monto, Queensland, Australia",OK
    MNR,////,////,FLMG,UNK,1.0,Mongu,"Mongu, Zambia",OWNO,////,////,????,"Mongu, Western, Zambia",OK
    MNS,////,////,FLMA,UNK,1.0,Mansa,"Mansa, Zambia",OWNO,////,////,????,"Mansa, Zambia",OK
    MNT,51Z,////,////,OK,0.549,Minto,"Minto (AK), USA",OWNO,MINTO AL WRIGHT,"MINTO, AK - UNITED STATES",Minto Al Wright Airport,"Minto, Alaska, United States",OK
    MNU,////,////,VYMM,UNK,1.0,Maulmyine,"Maulmyine, Myanmar",OWNO,////,////,????,"Maulmyine, Mon, Myanmar (Burma)",OK
    MNV,////,////,////,UNK,1.0,Mountain Valley,"Mountain Valley, Australia",OWNO,////,////,????,"Mountain Valley, Northern Territory, Australia",OK
    MNW,////,////,YMDS,UNK,1.0,Macdonald Downs,"Macdonald Downs, Australia",OWNO,////,////,????,"Macdonald Downs, Northern Territory, Australia",OK
    MNX,////,////,SBMY,UNK,1.0,Manicore,"Manicore, Brazil",OWNO,////,////,????,"Manicoré, Amazonas, Brazil",OK
    MNY,////,////,AGGO,UNK,1.0,Mono,"Mono, Solomon Islands",OWNO,////,////,Mono,"Stirling Island, Treasury Islands, Solomon Islands",OK
    MNZ,HEF,KHEF,KHEF,OK,0.694,Manassas,"Manassas (VA), USA",OWNO,MANASSAS RGNL/HARRY P DAVIS FIELD,"WASHINGTON, DC - UNITED STATES",Manassas Regional/Harry P Davis Field,"Washington, District of Columbia, United States",OK
    MOA,////,////,MUMO,UNK,1.0,Orestes Acosta,"Moa, Cuba",OWNO,////,////,Orestes Acosta,"Moa, Holguín, Cuba",OK
    MOB,MOB,KMOB,KMOB,OK,0.719,Mobile Municipal Airport,"Mobile (AL), USA",OWNO,MOBILE RGNL,"MOBILE, AL - UNITED STATES",Mobile Regional,"Mobile, Alabama, United States",OK
    MOC,////,////,SBMK,UNK,0.65,Montes Claros,"Montes Claros, Brazil",OWNO,////,////,Mário Ribeiro Airport,"Montes Claros, Minas Gerais, Brazil",OK
    MOD,MOD,KMOD,KMOD,OK,0.581,Municipal,"Modesto (CA), USA",OWNO,MODESTO CITY-CO-HARRY SHAM FLD,"MODESTO, CA - UNITED STATES",Modesto City-Co-Harry Sham Field,"Modesto, California, United States",OK
    MOE,////,////,VYMO,UNK,1.0,Momeik,"Momeik, Myanmar",OWNO,////,////,????,"Momeik, Shan, Myanmar (Burma)",OK
    MOF,////,////,WATC,UNK,1.0,Waioti,"Maumere, Indonesia",OWNO,////,////,Wai Oti Airport,"Maumere, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    MOG,////,////,VYMS,UNK,1.0,Mong Hsat,"Mong Hsat, Myanmar",OWNO,////,////,????,"Mong Hsat, Shan, Myanmar (Burma)",OK
    MOH,////,////,////,UNK,1.0,Mohanbari,"Mohanbari, India",OWNO,////,////,????,"Mohanbari, Assam, India",OK
    MOI,////,////,NCMR,UNK,1.0,Mitiaro Island,"Mitiaro Island, Cook Islands",OWNO,////,////,????,"Mitiaro Island, Cook Islands",OK
    MOJ,////,////,SMMO,UNK,1.0,Moengo,"Moengo, Suriname",OWNO,////,////,????,"Moengo, Marowijne, Suriname",OK
    MOL,////,////,ENML,UNK,1.0,Aro,"Molde, Norway",OWNO,////,////,Årø,"Molde, Møre og Romsdal, Norway",OK
    MOM,////,////,GQNL,UNK,0.667,Moudjeria,"Moudjeria, Mauritania",OWNO,////,////,Letfotar Airport,"Moudjeria, Mauritania",OK
    MON,////,////,NZMC,UNK,1.0,Mount Cook Airport,"Mount Cook, New Zealand",OWNO,////,////,????,"Aoraki/Mount Cook, New Zealand",OK
    MOO,////,////,YOOM,UNK,1.0,Moomba,"Moomba, Australia",OWNO,////,////,????,"Moomba, South Australia, Australia",OK
    MOP,MOP,KMOP,KMOP,OK,1.0,Municipal,"Mount Pleasant (MI), USA",OWNO,MOUNT PLEASANT MUNI,"MOUNT PLEASANT, MI - UNITED STATES",Mount Pleasant Municipal Airport,"Mount Pleasant, Michigan, United States",OK
    MOQ,////,////,FMMV,UNK,1.0,Morondava,"Morondava, Madagascar",OWNO,////,////,Morondava Airport,"Morondava, Madagascar",OK

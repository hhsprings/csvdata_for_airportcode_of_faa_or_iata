
List for checking certainty of Great Circle Mapper (MAA - MDO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MAA,////,////,VOMM,UNK,0.811,Chennai International Airport (Madras Airport),"Chennai, Tamil Nadu, India",OWNO,////,////,Meenambakkam,"Chennai/Madras, Tamil Nadu, India",OK
    MAB,////,////,SBMA,UNK,0.375,Maraba,"Maraba, Brazil",OWNO,////,////,João Correa da Rocha Airport,"Marabá, Pará, Brazil",OK
    MAC,MAC,KMAC,KMAC,OK,0.445,Smart,"Macon (GA), USA",OWNO,MACON DOWNTOWN,"MACON, GA - UNITED STATES",Macon Downtown Airport,"Macon, Georgia, United States",OK
    MAD,////,////,LEMD,UNK,0.918,Adolfo Suárez Madrid-Barajas Airport,"Madrid, Spain",OWNO,////,////,Adolfo Suárez Madrid-Barajas International Airport,"Madrid, Madrid, Spain",OK
    MAE,MAE,KMAE,KMAE,OK,0.766,Madera,"Madera (CA), USA",OWNO,MADERA MUNI,"MADERA, CA - UNITED STATES",Madera Municipal Airport,"Madera, California, United States",OK
    MAF,MAF,KMAF,KMAF,OK,0.366,Odessa Regional,"Midland (TX), USA",OWNO,MIDLAND INTL,"MIDLAND, TX - UNITED STATES",Midland International Airport,"Midland, Texas, United States",OK
    MAG,////,////,AYMD,UNK,1.0,Madang,"Madang, Papua New Guinea",OWNO,////,////,????,"Madang, Madang, Papua-New Guinea",OK
    MAH,////,////,LEMH,UNK,1.0,Menorca,"Menorca, Balearic Islands, Spain",OWNO,////,////,????,"Mahón, Menorca, Balearic Islands, Spain",OK
    MAI,////,////,FWMG,UNK,1.0,Mangochi,"Mangochi, Malawi",OWNO,////,////,????,"Mangochi, Malawi",OK
    MAJ,MAJ,PKMJ,PKMJ,OK,0.369,Amata Kabua International,"Majuro, Marshall Islands",OWNO,MARSHALL ISLANDS INTL,"MAJURO ATOLL, - MARSHALL ISLANDS",Amata Kabua International Airport,"Majuro Atoll, Marshall Islands",MAYBE
    MAK,////,////,HSSM,UNK,0.878,Malakal,"Malakal, Sudan",OWNO,////,////,Malakal International Airport,"Malakal, Upper Nile, South Sudan",OK
    MAL,////,////,WAPE,UNK,0.519,Mangole,"Mangole, Indonesia",OWNO,////,////,Falabisahaya Airport,"Mangole, Mangole Island, Maluku, Indonesia",OK
    MAM,////,////,MMMA,UNK,0.375,Matamoros,"Matamoros, Mexico",OWNO,////,////,General Servando Canales International Airport,"Matamoros, Tamaulipas, México",OK
    MAN,////,////,EGCC,UNK,0.606,Manchester Airport,"Manchester, United Kingdom",OWNO,////,////,Ringway International Airport,"Manchester, England, United Kingdom",OK
    MAO,////,////,SBEG,UNK,1.0,Eduardo Gomes International Airport,"Manaus, Brazil",OWNO,////,////,Eduardo Gomes International Airport,"Manaus, Amazonas, Brazil",OK
    MAP,////,////,////,UNK,1.0,Mamai,"Mamai, Papua New Guinea",OWNO,////,////,????,"Mamai, Central, Papua-New Guinea",OK
    MAQ,////,////,VTPM,UNK,1.0,Mae Sot,"Mae Sot, Thailand",OWNO,////,////,????,"Mae Sot, Tak, Thailand",OK
    MAR,////,////,SVMC,UNK,1.0,La Chinita,"Maracaibo, Venezuela",OWNO,////,////,La Chinita,"Maracaibo, Zulia, Venezuela",OK
    MAS,////,////,AYMO,UNK,1.0,Momote,"Manus Island, Papua New Guinea",OWNO,////,////,Momote Airport,"Lorengau, Manus Island, Manus, Papua-New Guinea",OK
    MAT,////,////,FZAM,UNK,1.0,Matadi,"Matadi, Congo (DRC)",OWNO,////,////,????,"Matadi, Bas-Congo, Democratic Republic of Congo (Zaire)",OK
    MAU,////,////,NTTP,UNK,1.0,Maupiti Airport,"Maupiti, French Polynesia",OWNO,////,////,????,"Maupiti, Society Islands, French Polynesia",OK
    MAV,3N1,////,////,OK,0.445,Maloelap airstrip,"Maloelap Island, Marshall Islands",OWNO,MALOELAP,"TAORA IS MALOELAP ATOLL, - MARSHALL ISLANDS",Maloelap Airport,"Taora Island, Marshall Islands",MAYBE
    MAW,MAW,KMAW,KMAW,OK,0.734,Malden,"Malden (MO), USA",OWNO,MALDEN RGNL,"MALDEN, MO - UNITED STATES",Malden Regional,"Malden, Missouri, United States",OK
    MAX,////,////,GOSM,UNK,0.78,Ouro Sogui airfield,"Matam, Senegal",OWNO,////,////,Ouro Sogui Airport,"Matam, Matam, Senegal",OK
    MAY,////,MYAB,MYAB,OK,0.36,Mangrove Cay,"Mangrove Cay, Bahamas",OWNO,CLARENCE A BAIN,"MANGROVE CAY, - BAHAMAS",Clarence A. Bain,"Mangrove Cay, Andros Island, Bahamas",OK
    MAZ,MAZ,TJMZ,TJMZ,OK,1.0,Eugenio María de Hostos Airport,"Mayaguez, Puerto Rico",OWNO,EUGENIO MARIA DE HOSTOS,"MAYAGUEZ, PR - UNITED STATES",Eugenio Maria De Hostos Airport,"Mayagüez, Puerto Rico, United States",OK
    MBA,////,////,HKMO,UNK,1.0,Moi International Airport,"Mombasa, Kenya",OWNO,////,////,Moi International Airport,"Mombasa, Kenya",OK
    MBB,////,////,YMBL,UNK,1.0,Marble Bar,"Marble Bar, Australia",OWNO,////,////,????,"Marble Bar, Western Australia, Australia",OK
    MBC,////,////,FOGG,UNK,1.0,Mbigou,"Mbigou, Gabon",OWNO,////,////,M'Bigou Airport,"M'Bigou, Ngounié, Gabon",OK
    MBD,////,////,FAMM,UNK,0.737,International,"Mmabatho, South Africa",OWNO,////,////,Mahikeng International Airport,"Mahikeng, North West, South Africa",TO DO CHECK
    MBE,////,////,RJEB,UNK,1.0,Monbetsu,"Monbetsu, Japan",OWNO,////,////,Monbetsu Airport,"Monbetsu, Hokkaido, Japan",OK
    MBF,////,////,////,UNK,1.0,Mount Buffalo,"Mount Buffalo, Australia",OWNO,////,////,????,"Mount Buffalo, Victoria, Australia",OK
    MBG,MBG,KMBG,KMBG,OK,0.801,Mobridge,"Mobridge (SD), USA",OWNO,MOBRIDGE MUNI,"MOBRIDGE, SD - UNITED STATES",Mobridge Municipal Airport,"Mobridge, South Dakota, United States",OK
    MBH,////,////,YMYB,UNK,1.0,Maryborough,"Maryborough, Australia",OWNO,////,////,????,"Maryborough, Queensland, Australia",OK
    MBI,////,////,HTMB,UNK,1.0,Mbeya,"Mbeya, Tanzania",OWNO,////,////,Mbeya Airport,"Mbeya, Mbeya, Tanzania",OK
    MBJ,////,////,MKJS,UNK,1.0,Sangster International,"Montego Bay, Jamaica",OWNO,////,////,Sangster International Airport,"Montego Bay, Jamaica",OK
    MBK,////,////,SWXM,UNK,0.324,Matupa,"Matupa, Brazil",OWNO,////,////,Regional Orlando Villas Boas,"Matupá, Mato Grosso, Brazil",OK
    MBL,MBL,KMBL,KMBL,OK,0.906,Blacker,"Manistee (MI), USA",OWNO,MANISTEE CO-BLACKER,"MANISTEE, MI - UNITED STATES",Manistee Co-Blacker Airport,"Manistee, Michigan, United States",OK
    MBM,////,////,////,UNK,1.0,Mkambati,"Mkambati, South Africa",OWNO,////,////,????,"Mkambati, Eastern Cape, South Africa",OK
    MBN,////,////,////,UNK,1.0,Mt Barnett,"Mt Barnett, Australia",OWNO,////,////,????,"Mt. Barnett, Western Australia, Australia",OK
    MBO,////,////,RPUM,UNK,1.0,Mamburao,"Mamburao, Philippines",OWNO,////,////,????,"Mamburao, Philippines",OK
    MBP,////,////,SPBB,UNK,1.0,Moyobamba,"Moyobamba, Peru",OWNO,////,////,Moyobamba Airport,"Moyobamba, San Martín, Perú",OK
    MBQ,////,////,HUMA,UNK,1.0,Mbarara,"Mbarara, Uganda",OWNO,////,////,????,"Mbarara, Uganda",OK
    MBR,////,////,GQNU,UNK,1.0,Mbout,"Mbout, Mauritania",OWNO,////,////,????,"M'bout, Mauritania",OK
    MBS,MBS,KMBS,KMBS,OK,0.578,Tri City Airport,"Saginaw (MI), USA",OWNO,MBS INTL,"SAGINAW, MI - UNITED STATES",MBS International Airport,"Saginaw, Michigan, United States",OK
    MBT,////,////,RPVJ,UNK,0.438,Masbate,"Masbate, Philippines",OWNO,////,////,Moises R. Espinosa Airport,"Masbate, Philippines",OK
    MBU,////,////,AGGD,UNK,1.0,Mbambanakira,"Mbambanakira, Solomon Islands",OWNO,////,////,????,"Mbambanakira, Solomon Islands",OK
    MBV,////,////,////,UNK,1.0,Masa,"Masa, Papua New Guinea",OWNO,////,////,????,"Masa, Morobe, Papua-New Guinea",OK
    MBW,////,////,YMMB,UNK,1.0,Moorabbin,"Moorabbin, Australia",OWNO,////,////,????,"Moorabbin, Victoria, Australia",OK
    MBX,////,////,LJMB,UNK,1.0,Maribor,"Maribor, Slovenia",OWNO,////,////,????,"Maribor, Slovenia",OK
    MBY,MBY,KMBY,KMBY,OK,0.464,Moberly,"Moberly (MO), USA",OWNO,OMAR N BRADLEY,"MOBERLY, MO - UNITED STATES",Omar N Bradley Airport,"Moberly, Missouri, United States",OK
    MBZ,////,////,SWMW,UNK,1.0,Maues,"Maues, Brazil",OWNO,////,////,????,"Maues, Amazonas, Brazil",OK
    MCA,////,////,GUMA,UNK,1.0,Macenta,"Macenta, Guinea",OWNO,////,////,????,"Macenta, Guinea",OK
    MCB,MCB,KMCB,KMCB,OK,1.0,Pike County,"Mccomb (MS), USA",OWNO,MC COMB/PIKE COUNTY/JOHN E LEWIS FIELD,"MC COMB, MS - UNITED STATES",Mc Comb/Pike County/John E Lewis Field,"Mc Comb, Mississippi, United States",OK
    MCC,MCC,KMCC,KMCC,OK,0.776,Mcclellan AFB,"Sacramento (CA), USA",OWNO,MC CLELLAN AIRFIELD,"SACRAMENTO, CA - UNITED STATES",Mc Clellan Airfield,"Sacramento, California, United States",OK
    MCD,MCD,KMCD,KMCD,OK,1.0,Mackinac Island,"Mackinac Island (MI), USA",OWNO,MACKINAC ISLAND,"MACKINAC ISLAND, MI - UNITED STATES",????,"Mackinac Island, Michigan, United States",OK
    MCE,MCE,KMCE,KMCE,OK,0.748,Merced Municipal Airport,"Merced (CA), USA",OWNO,MERCED RGNL/MACREADY FIELD,"MERCED, CA - UNITED STATES",Merced Regional/Macready Field,"Merced, California, United States",OK
    MCF,MCF,KMCF,KMCF,OK,0.722,Mac Dill AFB,"Tampa (FL), USA",OWNO,MACDILL AFB,"TAMPA, FL - UNITED STATES",Macdill AFB,"Tampa, Florida, United States",OK
    MCG,MCG,PAMC,PAMC,OK,0.716,Mcgrath,"Mcgrath (AK), USA",OWNO,MC GRATH,"MCGRATH, AK - UNITED STATES",Mc Grath Airport,"McGrath, Alaska, United States",OK
    MCH,////,////,SEMH,UNK,0.378,Machala,"Machala, Ecuador",OWNO,////,////,General Manuel Serrano,"Machala, El Oro, Ecuador",OK
    MCI,MCI,KMCI,KMCI,OK,1.0,International,"Kansas City (MO), USA",OWNO,KANSAS CITY INTL,"KANSAS CITY, MO - UNITED STATES",Kansas City International Airport,"Kansas City, Missouri, United States",OK
    MCJ,////,////,SKLM,UNK,0.48,Maicao,"Maicao, Colombia",OWNO,////,////,Jorge Isaacs Airport,"Maicao, La Guajira, Colombia",OK
    MCK,MCK,KMCK,KMCK,OK,0.306,Mccook,"Mccook (NE), USA",OWNO,MC COOK BEN NELSON RGNL,"MC COOK, NE - UNITED STATES",Mc Cook Ben Nelson Regional,"Mc Cook, Nebraska, United States",OK
    MCL,INR,PAIN,PAIN,OK,0.468,Mt Mckinley,"Mt Mckinley (AK), USA",OWNO,MC KINLEY NATIONAL PARK,"MCKINLEY PARK, AK - UNITED STATES",Mc Kinley National Park Airport,"McKinley Park, Alaska, United States",OK
    MCM,////,////,LNMC,UNK,0.6,Heliport,"Monaco, Monaco",OWNO,////,////,Fontvieille Heliport,"Monte Carlo, Monaco",TO DO CHECK
    MCN,MCN,KMCN,KMCN,OK,0.543,Lewis B Wilson,"Macon (GA), USA",OWNO,MIDDLE GEORGIA RGNL,"MACON, GA - UNITED STATES",Middle Georgia Regional,"Macon, Georgia, United States",OK
    MCO,MCO,KMCO,KMCO,OK,1.0,International,"Orlando (FL), USA",OWNO,ORLANDO INTL,"ORLANDO, FL - UNITED STATES",Orlando International Airport,"Orlando, Florida, United States",OK
    MCP,////,////,SBMQ,UNK,0.333,Alberto Alcolumbre International Airport,"Macapa, Brazil",OWNO,////,////,????,"Macapá, Pará, Brazil",OK
    MCQ,////,////,LHMC,UNK,1.0,Miskolc,"Miskolc, Hungary",OWNO,////,////,Miskolc Airport,"Miskolc, Borsod-Abaúj-Zemplén, Hungary",OK
    MCS,////,////,SARM,UNK,1.0,Monte Caseros,"Monte Caseros, Argentina",OWNO,////,////,????,"Montes Caseros, Corrientes, Argentina",OK
    MCT,////,////,OOMS,UNK,0.545,Seeb,"Muscat, Oman",OWNO,////,////,Muscat International Airport,"Muscat, Oman",OK
    MCU,////,////,LFLT,UNK,0.867,Gueret (Lepaud),"Montlucon, France",OWNO,////,////,Domérat,"Montluçon, Auvergne, France",OK
    MCV,////,////,YMHU,UNK,1.0,Mcarthur River,"Mcarthur River, Australia",OWNO,////,////,????,"McArthur River, Northern Territory, Australia",OK
    MCW,MCW,KMCW,KMCW,OK,0.81,Mason City,"Mason City IA, USA",OWNO,MASON CITY MUNI,"MASON CITY, IA - UNITED STATES",Mason City Municipal Airport,"Mason City, Iowa, United States",OK
    MCX,////,////,URML,UNK,0.783,Makhachkala,"Makhachkala, Russia",OWNO,////,////,Uytash Airport,"Makhachkala, Dagestan, Russian Federation (Russia)",OK
    MCY,////,////,YBSU,UNK,1.0,Maroochydore,"Sunshine Coast, Australia",OWNO,////,////,Sunshine Coast Airport,"Maroochydore, Queensland, Australia",OK
    MCZ,////,////,SBMO,UNK,0.909,Maceió/Zumbi dos Palmares International Airport,"Maceio, Brazil",OWNO,////,////,Zumbi dos Palmares,"Maceió, Alagoas, Brazil",OK
    MDB,////,////,////,UNK,1.0,Melinda,"Melinda, Belize",OWNO,////,////,Melinda Airport,"Melinda, Stann Creek, Belize",OK
    MDC,////,////,WAMM,UNK,0.821,Samratulangi,"Manado, Indonesia",OWNO,////,////,Sam Ratulangi,"Manado, Sulawesi Utara, Indonesia",OK
    MDD,MDD,KMDD,KMDD,OK,1.0,Airpark,"Midland (TX), USA",OWNO,MIDLAND AIRPARK,"MIDLAND, TX - UNITED STATES",Midland Airpark,"Midland, Texas, United States",OK
    MDE,////,////,SKRG,UNK,0.936,Jose Marie Cordova,"Medellin, Colombia",OWNO,////,////,José María Córdova International Airport,"Medellín, Antioquia, Colombia",OK
    MDF,MDZ,KMDZ,KMDZ,OK,0.51,Medford,"Medford (WI), USA",OWNO,TAYLOR COUNTY,"MEDFORD, WI - UNITED STATES",Taylor County Airport,"Medford, Wisconsin, United States",OK
    MDG,////,////,ZYMD,UNK,0.884,Mudanjiang,"Mudanjiang, PR China",OWNO,////,////,Hailang Airport,"Mudanjiang, Heilongjiang, China",OK
    MDH,MDH,KMDH,KMDH,OK,1.0,Southern Illinois,"Carbondale (IL), USA",OWNO,SOUTHERN ILLINOIS,"CARBONDALE/MURPHYSBORO, IL - UNITED STATES",Southern Illinois Airport,"Carbondale/Murphysboro, Illinois, United States",OK
    MDI,////,////,DNMK,UNK,1.0,Makurdi,"Makurdi, Nigeria",OWNO,////,////,Makurdi Airport,"Makurdi, Benue, Nigeria",OK
    MDJ,S33,////,////,OK,0.476,City-county,"Madras (OR), USA",OWNO,MADRAS MUNICIPAL,"MADRAS, OR - UNITED STATES",Madras Municipal Airport,"Madras, Oregon, United States",OK
    MDK,////,////,FZEA,UNK,1.0,Mbandaka,"Mbandaka, Congo (DRC)",OWNO,////,////,Mbandaka Airport,"Mbandaka, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    MDL,////,////,VYMD,UNK,0.72,Annisaton,"Mandalay, Myanmar",OWNO,////,////,Mandalay International Airport,"Mandalay, Mandalay, Myanmar (Burma)",OK
    MDN,IMS,KIMS,KIMS,OK,0.387,Jefferson Proving Grd,"Madison (IN), USA",OWNO,MADISON MUNI,"MADISON, IN - UNITED STATES",Madison Municipal Airport,"Madison, Indiana, United States",OK
    MDO,MDO,PAMD,PAMD,OK,0.627,Intermediate,"Middleton Island (AK), USA",OWNO,MIDDLETON ISLAND,"MIDDLETON ISLAND, AK - UNITED STATES",????,"Middleton Island, Alaska, United States",OK

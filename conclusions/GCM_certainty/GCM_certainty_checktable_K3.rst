
List for checking certainty of Great Circle Mapper (KMA - KSD)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    KMA,////,////,AYKM,UNK,1.0,Kerema,"Kerema, Papua New Guinea",OWNO,////,////,????,"Kerema, Gulf, Papua-New Guinea",OK
    KMB,////,////,////,UNK,1.0,Koinambe,"Koinambe, Papua New Guinea",OWNO,////,////,????,"Koinambe, Western Highlands, Papua-New Guinea",OK
    KMC,////,////,OEKK,UNK,0.933,King Khalid Military,"King Khalid Mil. City, Saudi Arabia",OWNO,////,////,King Khaled Military City Airport,"King Khaled Military City, Saudi Arabia",MAYBE
    KME,////,////,HRZA,UNK,0.667,Kamembe,"Kamembe, Rwanda",OWNO,////,////,Cyangugu,"Kamembe, Rwanda",OK
    KMF,////,////,////,UNK,1.0,Kamina,"Kamina, Papua New Guinea",OWNO,////,////,????,"Kamina, Gulf, Papua-New Guinea",OK
    KMG,////,////,ZPPP,UNK,1.0,Kunming Changshui International Airport,"Kunming, Yunnan, China",OWNO,////,////,Kunming Changshui International Airport,"Kunming, Yunnan, China",OK
    KMH,////,////,FAKU,UNK,0.636,Kuruman,"Kuruman, South Africa",OWNO,////,////,Johan Pienaar Airport,"Kuruman, Northern Cape, South Africa",OK
    KMI,////,////,RJFM,UNK,1.0,Miyazaki,"Miyazaki, Japan",OWNO,////,////,Miyazaki Airport,"Miyazaki, Miyazaki, Japan",OK
    KMJ,////,////,RJFT,UNK,1.0,Kumamoto,"Kumamoto, Japan",OWNO,////,////,Kumamoto Airport,"Kumamoto, Kumamoto, Japan",OK
    KMK,////,////,FCPA,UNK,1.0,Makabana,"Makabana, Congo (ROC)",OWNO,////,////,Makabana Airport,"Makabana, Congo (Republic of)",OK
    KML,////,////,YKML,UNK,1.0,Kamileroi,"Kamileroi, Australia",OWNO,////,////,????,"Kamileroi, Queensland, Australia",OK
    KMM,////,////,////,UNK,1.0,Kimam,"Kimam, Indonesia",OWNO,////,////,????,"Kimam, Papua, Indonesia",OK
    KMN,////,////,FZSA,UNK,0.833,Kamina,"Kamina, Congo (DRC)",OWNO,////,////,Kamina AB,"Kamina, Katanga, Democratic Republic of Congo (Zaire)",OK
    KMO,MBA,PAMB,PAMB,OK,0.826,Manokotak SPB,"Manokotak (AK), USA",OWNO,MANOKOTAK,"MANOKOTAK, AK - UNITED STATES",????,"Manokotak, Alaska, United States",OK
    KMP,////,////,FYKT,UNK,0.581,J.G.H. Van Der Wath,"Keetmanshoop, Namibia",OWNO,////,////,????,"Keetmanshoop, Namibia",OK
    KMQ,////,////,RJNK,UNK,1.0,Komatsu,"Komatsu, Japan",OWNO,////,////,Komatsu Airport,"Komatsu, Ishikawa, Japan",OK
    KMR,////,////,////,UNK,1.0,Karimui,"Karimui, Papua New Guinea",OWNO,////,////,????,"Karimui, Chimbu, Papua-New Guinea",OK
    KMS,////,////,DGSI,UNK,1.0,Kumasi,"Kumasi, Ghana",OWNO,////,////,????,"Kumasi, Ghana",OK
    KMT,////,////,////,UNK,1.0,Kampot,"Kampot, Cambodia",OWNO,////,////,????,"Kampot, Cambodia",OK
    KMU,////,////,HCMK,UNK,0.933,Kismayu,"Kismayu, Somalia",OWNO,////,////,Kisimayu Airport,"Kisimayu, Jubbada Hoose (Lower Juba), Somalia",OK
    KMV,////,////,VYKL,UNK,1.0,Kalemyo,"Kalemyo, Myanmar",OWNO,////,////,????,"Kalemyo, Sagaing, Myanmar (Burma)",OK
    KMW,////,////,UUBA,UNK,1.0,Kostroma,"Kostroma, Russia",OWNO,////,////,Kostroma Airport,"Kostroma, Kostromskaya, Russian Federation (Russia)",OK
    KMX,////,////,OEKM,UNK,0.651,Khamis Mushait,"Khamis Mushait, Saudi Arabia",OWNO,////,////,King Khaled AB,"Khamis Mushait, Saudi Arabia",OK
    KMY,KMY,////,////,OK,0.805,Moser Bay,"Moser Bay (AK), USA",OWNO,MOSER BAY,"MOSER BAY, AK - UNITED STATES",Moser Bay SPB,"Moser Bay, Alaska, United States",OK
    KMZ,////,////,FLKO,UNK,1.0,Kaoma,"Kaoma, Zambia",OWNO,////,////,????,"Kaoma, Zambia",OK
    KNA,////,////,SCVM,UNK,1.0,Vina del Mar,"Vina del Mar, Chile",OWNO,////,////,Viña del Mar Airport,"Viña del Mar, Valparaíso, Chile",OK
    KNB,KNB,KKNB,KKNB,OK,0.679,Kanab,"Kanab (UT), USA",OWNO,KANAB MUNI,"KANAB, UT - UNITED STATES",Kanab Municipal Airport,"Kanab, Utah, United States",OK
    KND,////,////,FZOA,UNK,1.0,Kindu,"Kindu, Congo (DRC)",OWNO,////,////,Kindu Airport,"Kindu, Maniema, Democratic Republic of Congo (Zaire)",OK
    KNE,////,////,////,UNK,1.0,Kanainj,"Kanainj, Papua New Guinea",OWNO,////,////,????,"Kanainj, Madang, Papua-New Guinea",OK
    KNG,////,////,WASK,UNK,1.0,Kaimana,"Kaimana, Indonesia",OWNO,////,////,????,"Kaimana, Papua Barat, Indonesia",OK
    KNH,////,////,RCBS,UNK,0.714,Shang-Yi,"Kinmen, Taiwan",OWNO,////,////,????,"Kinmen, Taiwan",OK
    KNI,////,////,YKNG,UNK,1.0,Katanning,"Katanning, Australia",OWNO,////,////,????,"Katanning, Western Australia, Australia",OK
    KNJ,////,////,FCBK,UNK,1.0,Kindamba,"Kindamba, Congo (ROC)",OWNO,////,////,Kindamba Airport,"Kindamba, Congo (Republic of)",OK
    KNK,9K2,PFKK,////,NG,1.0,Kakhonak,"Kakhonak (AK), USA",OWNO,KOKHANOK,"KOKHANOK, AK - UNITED STATES",????,"Kokhanok, Alaska, United States",OK
    KNL,////,////,////,UNK,1.0,Kelanoa,"Kelanoa, Papua New Guinea",OWNO,////,////,????,"Kelanoa, Morobe, Papua-New Guinea",OK
    KNM,////,////,FZTK,UNK,1.0,Kaniama,"Kaniama, Congo (DRC)",OWNO,////,////,????,"Kaniama, Katanga, Democratic Republic of Congo (Zaire)",OK
    KNN,////,////,GUXD,UNK,0.833,Kankan,"Kankan, Guinea",OWNO,////,////,Diankana,"Kankan, Guinea",OK
    KNO,////,////,WIMM,UNK,1.0,Kualanamu International Airport,"Medan, Indonesia",OWNO,////,////,Kualanamu International Airport,"Medan, Sumatera Utara, Indonesia",OK
    KNP,////,////,FNCP,UNK,1.0,Capanda,"Capanda, Angola",OWNO,////,////,????,"Capanda, Angola",OK
    KNQ,////,////,NWWD,UNK,1.0,Kone,"Kone, New Caledonia",OWNO,////,////,????,"Koné, New Caledonia",OK
    KNR,////,////,OIBJ,UNK,1.0,Jam,"Kangan, Iran",OWNO,////,////,Jam,"Kangan, Bushehr, Iran",OK
    KNS,////,////,YKII,UNK,1.0,King Island,"King Island, Australia",OWNO,////,////,????,"King Island, Tasmania, Australia",OK
    KNT,TKX,KTKX,KTKX,OK,0.64,Municipal,"Kennett (MO), USA",OWNO,KENNETT MEMORIAL,"KENNETT, MO - UNITED STATES",Kennett Memorial Airport,"Kennett, Missouri, United States",OK
    KNU,////,////,VIKA,UNK,1.0,Kanpur Airport,"Kanpur, Uttar Pradesh, India",OWNO,////,////,????,"Kanpur, Uttar Pradesh, India",OK
    KNW,KNW,PANW,PANW,OK,1.0,New Stuyahok,"New Stuyahok (AK), USA",OWNO,NEW STUYAHOK,"NEW STUYAHOK, AK - UNITED STATES",????,"New Stuyahok, Alaska, United States",OK
    KNX,////,////,YPKU,UNK,1.0,Kununurra,"Kununurra, Australia",OWNO,////,////,????,"Kununurra, Western Australia, Australia",OK
    KNZ,////,////,GAKA,UNK,1.0,Kenieba,"Kenieba, Mali",OWNO,////,////,????,"Kéniéba, Kayes, Mali",OK
    KOA,KOA,PHKO,PHKO,OK,0.682,Keahole,"Kona (HI), USA",OWNO,KONA INTL AT KEAHOLE,"KAILUA/KONA, HI - UNITED STATES",Kona International at Keahole,"Kailua/Kona, Hawaii, Hawaii, United States",OK
    KOC,////,////,NWWK,UNK,1.0,Koumac,"Koumac, New Caledonia",OWNO,////,////,????,"Koumac, New Caledonia",OK
    KOD,////,////,////,UNK,1.0,Kotabangun,"Kotabangun, Indonesia",OWNO,////,////,????,"Kotabangun, Kalimantan Timur (East Borneo), Indonesia",OK
    KOE,////,////,WATT,UNK,1.0,Eltari,"Kupang, Indonesia",OWNO,////,////,Eltari Airport,"Kupang, Nusa Tenggara Timur, Indonesia",OK
    KOF,////,////,FAKP,UNK,1.0,Komatipoort,"Komatipoort, South Africa",OWNO,////,////,????,"Komatipoort, Mpumalanga, South Africa",OK
    KOG,////,////,VLKG,UNK,1.0,Khong,"Khong, Lao PDR",OWNO,////,////,????,"Khong, Lao People's Democratic Republic (Laos)",OK
    KOH,////,////,YKLA,UNK,1.0,Koolatah,"Koolatah, Australia",OWNO,////,////,????,"Koolatah, Queensland, Australia",OK
    KOI,////,////,EGPA,UNK,1.0,Kirkwall Airport,"Kirkwall (Orkney Is), United Kingdom",OWNO,////,////,Kirkwall Airport,"Kirkwall, Orkney Isles, Scotland, United Kingdom",OK
    KOJ,////,////,RJFK,UNK,1.0,Kagoshima Airport,"Kagoshima, Japan",OWNO,////,////,Kagoshima Airport,"Kagoshima, Kagoshima, Japan",OK
    KOK,////,////,EFKK,UNK,1.0,Kruunupyy,"Kokkola/Pietarsaari, Finland",OWNO,////,////,Kruunupyy,"Kokkola/Pietarsaari, Pohjanmaa (Österbotten (Ostrobothnia)), Finland",OK
    KOL,////,////,////,UNK,1.0,Koumala,"Koumala, Central African Republic",OWNO,////,////,????,"Koumala, Bamingui-Bangoran (Bamïngï-Bangoran), Central African Republic",OK
    KOM,////,////,////,UNK,1.0,Komo-Manda,"Komo-Manda, Papua New Guinea",OWNO,////,////,????,"Komo-Manda, Southern Highlands, Papua-New Guinea",OK
    KON,////,////,////,UNK,1.0,Kontum,"Kontum, Viet Nam",OWNO,////,////,????,"Kontum, Vietnam",OK
    KOO,////,////,FZRQ,UNK,1.0,Kongolo,"Kongolo, Congo (DRC)",OWNO,////,////,Kongolo Airport,"Kongolo, Katanga, Democratic Republic of Congo (Zaire)",OK
    KOP,////,////,VTUW,UNK,0.839,Nakhon Phanom,"Nakhon Phanom, Thailand",OWNO,////,////,Nakhon Phanom West,"Nakhon Phanom, Nakhon Phanom, Thailand",OK
    KOR,////,////,////,UNK,1.0,Kokoro,"Kokoro, Papua New Guinea",OWNO,////,////,????,"Kokoro, Gulf, Papua-New Guinea",OK
    KOS,////,////,VDSV,UNK,0.839,Sihanoukville,"Sihanoukville, Cambodia",OWNO,////,////,Sihanoukville International Airport,"Sihanoukville, Cambodia",OK
    KOT,2A9,PFKO,PFKO,OK,1.0,Kotlik,"Kotlik (AK), USA",OWNO,KOTLIK,"KOTLIK, AK - UNITED STATES",????,"Kotlik, Alaska, United States",OK
    KOU,////,////,FOGK,UNK,1.0,Koulamoutou,"Koulamoutou, Gabon",OWNO,////,////,????,"Koulamoutou, Ogooué-Lolo, Gabon",OK
    KOV,////,////,UACK,UNK,1.0,Kokshetau,"Kokshetau, Kazakhstan",OWNO,////,////,Kokshetau Airport,"Kokshetau, Aqmola, Kazakhstan",OK
    KOW,////,////,ZSGZ,UNK,0.8,Ganzhou,"Ganzhou, PR China",OWNO,////,////,Ganzhou Huangjin Airport,"Ganzhou, Jiangxi, China",OK
    KOX,////,////,WABN,UNK,1.0,Kokonao,"Kokonao, Indonesia",OWNO,////,////,????,"Kokonao, Papua, Indonesia",OK
    KOY,KOY,////,////,OK,0.778,Olga Bay SPB,"Olga Bay (AK), USA",OWNO,OLGA BAY,"OLGA BAY, AK - UNITED STATES",Olga Bay SPB,"Olga Bay, Alaska, United States",OK
    KOZ,4K5,////,////,OK,0.778,Ouzinkie SPB,"Ouzinkie (AK), USA",OWNO,OUZINKIE,"OUZINKIE, AK - UNITED STATES",????,"Ouzinkie, Alaska, United States",OK
    KPA,////,////,////,UNK,1.0,Kopiago,"Kopiago, Papua New Guinea",OWNO,////,////,????,"Kopiago, Southern Highlands, Papua-New Guinea",OK
    KPB,KPB,////,////,OK,0.81,Point Baker SPB,"Point Baker (AK), USA",OWNO,POINT BAKER,"POINT BAKER, AK - UNITED STATES",Point Baker SPB,"Point Baker, Alaska, United States",OK
    KPC,KPC,PAPC,PAPC,OK,0.845,Port Clarence,"Port Clarence (AK), USA",OWNO,PORT CLARENCE CGS,"PORT CLARENCE, AK - UNITED STATES",Port Clarence CGS Airport,"Port Clarence, Alaska, United States",OK
    KPE,////,////,////,UNK,1.0,Yapsiei,"Yapsiei, Papua New Guinea",OWNO,////,////,????,"Yapsiei, Sandaun, Papua-New Guinea",OK
    KPG,////,////,////,UNK,1.0,Kurupung,"Kurupung, Guyana",OWNO,////,////,????,"Kurupung, Cuyuni-Mazaruni, Guyana",OK
    KPI,////,////,WBGP,UNK,1.0,Kapit,"Kapit, Malaysia",OWNO,////,////,????,"Kapit, Sarawak, Malaysia",OK
    KPM,////,////,////,UNK,1.0,Kompiam,"Kompiam, Papua New Guinea",OWNO,////,////,????,"Kompiam, Enga, Papua-New Guinea",OK
    KPN,IIK,PAKI,PAKI,OK,0.79,Kipnuk SPB,"Kipnuk (AK), USA",OWNO,KIPNUK,"KIPNUK, AK - UNITED STATES",????,"Kipnuk, Alaska, United States",OK
    KPO,////,////,RKTH,UNK,1.0,Pohang,"Pohang, South Korea",OWNO,////,////,????,"Pohang, Republic of Korea (South Korea)",OK
    KPP,////,////,YKPR,UNK,1.0,Kalpowar,"Kalpowar, Australia",OWNO,////,////,????,"Kalpowar, Queensland, Australia",OK
    KPR,KPR,////,////,OK,0.826,Port Williams SPB,"Port Williams (AK), USA",OWNO,PORT WILLIAMS,"PORT WILLIAMS, AK - UNITED STATES",Port Williams SPB,"Port Williams, Alaska, United States",OK
    KPS,////,////,YKMP,UNK,1.0,Kempsey,"Kempsey, Australia",OWNO,////,////,????,"Kempsey, New South Wales, Australia",OK
    KPT,06U,////,////,OK,1.0,Jackpot,"Jackpot (NV), USA",OWNO,JACKPOT/HAYDEN FIELD,"JACKPOT, NV - UNITED STATES",Jackpot/Hayden Field,"Jackpot, Nevada, United States",OK
    KPV,PEV,PAPE,PAPE,OK,0.801,Perryville SPB,"Perryville (AK), USA",OWNO,PERRYVILLE,"PERRYVILLE, AK - UNITED STATES",????,"Perryville, Alaska, United States",OK
    KPY,KPY,////,////,OK,0.81,Port Bailey SPB,"Port Bailey (AK), USA",OWNO,PORT BAILEY,"PORT BAILEY, AK - UNITED STATES",Port Bailey SPB,"Port Bailey, Alaska, United States",OK
    KQA,KQA,////,////,OK,0.752,Akutan,"Akutan (AK), USA",OWNO,AKUTAN,"AKUTAN, AK - UNITED STATES",Akutan SPB,"Akutan, Alaska, United States",OK
    KQL,////,////,////,UNK,1.0,Kol,"Kol, Papua New Guinea",OWNO,////,////,????,"Kol, Western Highlands, Papua-New Guinea",OK
    KQR,////,////,YKAR,UNK,nan,////,////,////,////,////,Karara Airport,"Rothsay, Western Australia, Australia",UNK
    KQT,////,////,UTDT,UNK,nan,////,////,////,////,////,Qurghonteppa International Airport,"Qurghonteppa, Khatlon, Tajikistan",UNK
    KRA,////,////,YKER,UNK,1.0,Kerang,"Kerang, Australia",OWNO,////,////,????,"Kerang, Victoria, Australia",OK
    KRB,////,////,YKMB,UNK,1.0,Karumba,"Karumba, Australia",OWNO,////,////,????,"Karumba, Queensland, Australia",OK
    KRC,////,////,WIPH,UNK,1.0,Kerinci,"Kerinci, Indonesia",OWNO,////,////,????,"Kerinci, Sumatera Barat, Indonesia",OK
    KRD,////,////,////,UNK,1.0,Kurundi,"Kurundi, Australia",OWNO,////,////,????,"Kurundi, Northern Territory, Australia",OK
    KRE,////,////,HBBO,UNK,1.0,Kirundo,"Kirundo, Burundi",OWNO,////,////,????,"Kirundo, Burundi",OK
    KRF,////,////,ESNK,UNK,0.7,Kramfors,"Kramfors, Sweden",OWNO,////,////,????,"Kramfors-Sollefteå, Västernorrlands län, Sweden",TO DO CHECK
    KRG,////,////,SYKS,UNK,1.0,Karasabai,"Karasabai, Guyana",OWNO,////,////,????,"Karasabai, Upper Takutu-Upper Essequibo, Guyana",OK
    KRH,////,////,EGKR,UNK,nan,////,////,////,////,////,????,"Redhill, Surrey, England, United Kingdom",UNK
    KRI,////,////,AYKK,UNK,1.0,Kikori,"Kikori, Papua New Guinea",OWNO,////,////,????,"Kikori, Gulf, Papua-New Guinea",OK
    KRJ,////,////,////,UNK,1.0,Karawari,"Karawari, Papua New Guinea",OWNO,////,////,????,"Karawari, East Sepik, Papua-New Guinea",OK
    KRK,////,////,EPKK,UNK,0.808,J. Paul II Balice International,"Krakow, Poland",OWNO,////,////,John Paul II International Airport,"Kraków, Malopolskie, Poland",OK
    KRL,////,////,ZWKL,UNK,1.0,Korla,"Korla, PR China",OWNO,////,////,????,"Korla, Xinjiang, China",OK
    KRM,////,////,SYKR,UNK,1.0,Karanambo,"Karanambo, Guyana",OWNO,////,////,????,"Karanambo, Upper Takutu-Upper Essequibo, Guyana",OK
    KRN,////,////,ESNQ,UNK,1.0,Kiruna,"Kiruna, Sweden",OWNO,////,////,????,"Kiruna, Norrbottens län, Sweden",OK
    KRO,////,////,USUU,UNK,1.0,Kurgan,"Kurgan, Russia",OWNO,////,////,Kurgan Airport,"Kurgan, Kurganskaya, Russian Federation (Russia)",OK
    KRP,////,////,EKKA,UNK,1.0,Karup,"Karup, Denmark",OWNO,////,////,????,"Karup, Denmark",OK
    KRQ,////,////,UKCK,UNK,1.0,Kramatorsk,"Kramatorsk, Ukraine",OWNO,////,////,????,"Kramatorsk, Donetsk, Ukraine",OK
    KRR,////,////,URKK,UNK,0.667,Krasnodar,"Krasnodar, Russia",OWNO,////,////,Pashkovskiy Airport,"Krasnodar, Krasnodarskiy, Russian Federation (Russia)",OK
    KRS,////,////,ENCN,UNK,1.0,Kjevik,"Kristiansand, Norway",OWNO,////,////,Kjevik,"Kristiansand, Vest-Agder, Norway",OK
    KRT,////,////,HSSS,UNK,0.727,Civil,"Khartoum, Sudan",OWNO,////,////,????,"Khartoum, Khartoum, Sudan",OK
    KRU,////,////,////,UNK,1.0,Kerau,"Kerau, Papua New Guinea",OWNO,////,////,????,"Kerau, Central, Papua-New Guinea",OK
    KRV,////,////,////,UNK,1.0,Kerio Valley,"Kerio Valley, Kenya",OWNO,////,////,Kerio Valley Airport,"Kimwarer, Kenya",OK
    KRW,////,////,UTAK,UNK,0.833,Turkmanbashi,"Turkmenbashi, Turkmenistan",OWNO,////,////,Türkmenbashi International Airport,"Türkmenbashi, Balkan, Turkmenistan",OK
    KRX,////,////,////,UNK,1.0,Kar Kar,"Kar Kar, Papua New Guinea",OWNO,////,////,????,"Kar Kar, Madang, Papua-New Guinea",OK
    KRY,////,////,ZWKM,UNK,1.0,Karamay,"Karamay, PR China",OWNO,////,////,????,"Karamay, Xinjiang, China",OK
    KRZ,////,////,FZBT,UNK,0.32,Kiri,"Kiri, Congo (DRC)",OWNO,////,////,Basango Mboliasa Airport,"Kiri, Bandundu, Democratic Republic of Congo (Zaire)",OK
    KSA,TTK,PTSA,PTSA,OK,1.0,Kosrae,"Kosrae, Micronesia",OWNO,KOSRAE,"KOSRAE, - MICRONESIA, FED STATES OF",Kosrae Airport,"Kosrae Island, Kosrae, Micronesia (Federated States of)",OK
    KSB,////,////,////,UNK,1.0,Kasanombe,"Kasanombe, Papua New Guinea",OWNO,////,////,????,"Kasanombe, Morobe, Papua-New Guinea",OK
    KSC,////,////,LZKZ,UNK,1.0,Barca,"Kosice, Slovakia",OWNO,////,////,Barca,"Kosice, Slovakia",OK
    KSD,////,////,ESOK,UNK,1.0,Karlstad,"Karlstad, Sweden",OWNO,////,////,????,"Karlstad, Värmlands län, Sweden",OK

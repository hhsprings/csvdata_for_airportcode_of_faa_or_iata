
List for checking certainty of Great Circle Mapper (PGR - PMX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    PGR,PGR,KPGR,KPGR,OK,0.669,Municipal,"Paragould (AR), USA",OWNO,KIRK FIELD,"PARAGOULD, AR - UNITED STATES",Kirk Field,"Paragould, Arkansas, United States",OK
    PGS,L37,////,////,OK,0.445,Peach Springs,"Peach Springs (AZ), USA",OWNO,GRAND CANYON CAVERNS,"PEACH SPRINGS, AZ - UNITED STATES",Grand Canyon Caverns Airport,"Peach Springs, Arizona, United States",OK
    PGU,////,////,OIBP,UNK,nan,////,////,////,////,////,Persian Gulf Airport,"Asalouyeh, Bushehr, Iran",UNK
    PGV,PGV,KPGV,KPGV,OK,1.0,Pitt-Greenville,"Greenville (NC), USA",OWNO,PITT-GREENVILLE,"GREENVILLE, NC - UNITED STATES",Pitt-Greenville Airport,"Greenville, North Carolina, United States",OK
    PGX,////,////,LFBX,UNK,0.667,Perigueux,"Perigueux, France",OWNO,////,////,Bassillac,"Périgueux, Aquitaine, France",OK
    PGZ,////,////,SSZW,UNK,0.774,Sant'Ana,"Ponta Grossa, Brazil",OWNO,////,////,????,"Ponta Grossa, Paraná, Brazil",OK
    PHA,////,////,VVPR,UNK,0.875,Phan Rang,"Phan Rang, Viet Nam",OWNO,////,////,Phan Rang AB,"Phan Rang, Vietnam",OK
    PHB,////,////,SBPB,UNK,1.0,Santos Dumont,"Parnaiba, Brazil",OWNO,////,////,Santos Dumont,"Parnaíba, Piauí, Brazil",OK
    PHC,////,////,DNPO,UNK,0.839,Port Harcourt,"Port Harcourt, Nigeria",OWNO,////,////,Port Harcourt International Airport,"Port Harcourt, Rivers, Nigeria",OK
    PHD,PHD,KPHD,KPHD,OK,0.845,Harry Clever,"New Philadelphia (OH), USA",OWNO,HARRY CLEVER FIELD,"NEW PHILADELPHIA, OH - UNITED STATES",Harry Clever Field,"New Philadelphia, Ohio, United States",OK
    PHE,////,////,YPPD,UNK,1.0,Port Hedland,"Port Hedland, Australia",OWNO,////,////,????,"Port Hedland, Western Australia, Australia",OK
    PHF,PHF,KPHF,KPHF,OK,1.0,Newport News Williamsburg International,"Newport News (VA), USA",OWNO,NEWPORT NEWS/WILLIAMSBURG INTL,"NEWPORT NEWS, VA - UNITED STATES",Newport News/Williamsburg International Airport,"Newport News, Virginia, United States",OK
    PHG,////,////,////,UNK,nan,////,////,////,////,////,Port Harcourt NAF Base,"Port Harcourt, Rivers, Nigeria",UNK
    PHH,////,////,VVPT,UNK,1.0,Phan Thiet,"Phan Thiet, Viet Nam",OWNO,////,////,????,"Phan Thiet, Vietnam",OK
    PHI,////,////,SNYE,UNK,1.0,Pinheiro,"Pinheiro, Brazil",OWNO,////,////,????,"Pinheiro, Maranhão, Brazil",OK
    PHK,PHK,KPHK,KPHK,OK,1.0,Palm Beach Co Glades,"Pahokee (FL), USA",OWNO,PALM BEACH CO GLADES,"PAHOKEE, FL - UNITED STATES",Palm Beach Co. Glades Airport,"Pahokee, Florida, United States",OK
    PHL,PHL,KPHL,KPHL,OK,1.0,International,"Philadelphia (PA), USA",OWNO,PHILADELPHIA INTL,"PHILADELPHIA, PA - UNITED STATES",Philadelphia International Airport,"Philadelphia, Pennsylvania, United States",OK
    PHN,PHN,KPHN,KPHN,OK,1.0,t Clair County International,"Port Huron (MI), USA",OWNO,ST CLAIR COUNTY INTL,"PORT HURON, MI - UNITED STATES",St. Clair County International Airport,"Port Huron, Michigan, United States",OK
    PHO,PHO,PAPO,PAPO,OK,1.0,Point Hope,"Point Hope (AK), USA",OWNO,POINT HOPE,"POINT HOPE, AK - UNITED STATES",????,"Point Hope, Alaska, United States",OK
    PHP,PHP,KPHP,KPHP,OK,1.0,Philip,"Philip (SD), USA",OWNO,PHILIP,"PHILIP, SD - UNITED STATES",????,"Philip, South Dakota, United States",OK
    PHQ,////,////,YTMO,UNK,nan,////,////,////,////,////,The Monument Airport,"Phosphate Hill, Queensland, Australia",UNK
    PHR,////,////,NFND,UNK,0.897,Pacific Harbor,"Pacific Harbor, Fiji",OWNO,////,////,SPB,"Pacific Harbour / Deuba, Viti Levu, Fiji",OK
    PHS,////,////,VTPP,UNK,1.0,Phitsanulok,"Phitsanulok, Thailand",OWNO,////,////,????,"Phitsanulok, Phitsanulok, Thailand",OK
    PHT,PHT,KPHT,KPHT,OK,1.0,Henry County,"Paris (TN), USA",OWNO,HENRY COUNTY,"PARIS, TN - UNITED STATES",Henry County Airport,"Paris, Tennessee, United States",OK
    PHW,////,////,FAPH,UNK,0.571,Phalaborwa,"Phalaborwa, South Africa",OWNO,////,////,Hendrik Van Eck Airport,"Phalaborwa, Limpopo, South Africa",OK
    PHX,PHX,KPHX,KPHX,OK,0.79,ky Harbor International,"Phoenix (AZ), USA",OWNO,PHOENIX SKY HARBOR INTL,"PHOENIX, AZ - UNITED STATES",Phoenix Sky Harbor International Airport,"Phoenix, Arizona, United States",OK
    PHY,////,////,VTPB,UNK,1.0,Phetchabun,"Phetchabun, Thailand",OWNO,////,////,????,"Phetchabun, Phetchabun, Thailand",OK
    PIA,PIA,KPIA,KPIA,OK,0.46,Greater Peoria,"Peoria (IL), USA",OWNO,GENERAL DOWNING - PEORIA INTL,"PEORIA, IL - UNITED STATES",General Downing - Peoria International Airport,"Peoria, Illinois, United States",OK
    PIB,PIB,KPIB,KPIB,OK,0.916,Hattiesburg-Laurel Reg,"Laurel (MS), USA",OWNO,HATTIESBURG-LAUREL RGNL,"HATTIESBURG-LAUREL, MS - UNITED STATES",Hattiesburg-Laurel Regional,"Hattiesburg-Laurel, Mississippi, United States",OK
    PIC,////,////,MBPI,UNK,1.0,Pine Cay,"Pine Cay, Turks and Caicos Islands",OWNO,////,////,????,"Pine Cay, Turks and Caicos Islands",OK
    PID,////,////,MYPI,UNK,1.0,Paradise Island,"Nassau, Bahamas",OWNO,////,////,Paradise Island,"Nassau, Bahamas",OK
    PIE,PIE,KPIE,KPIE,OK,0.882,St. Petersburg-Clearwater International,"St. Petersburg (FL), USA",OWNO,ST PETE-CLEARWATER INTL,"ST PETERSBURG-CLEARWATER, FL - UNITED STATES",St. Pete-Clearwater International Airport,"St. Petersburg-Clearwater, Florida, United States",OK
    PIF,////,////,RCSQ,UNK,0.762,Pingtung,"Pingtung, Taiwan",OWNO,////,////,North,"Pingtung, Taiwan",OK
    PIH,PIH,KPIH,KPIH,OK,0.734,Pocatello,"Pocatello (ID), USA",OWNO,POCATELLO RGNL,"POCATELLO, ID - UNITED STATES",Pocatello Regional,"Pocatello, Idaho, United States",OK
    PIK,////,////,EGPK,UNK,1.0,Prestwick,"Glasgow, United Kingdom",OWNO,////,////,Prestwick,"Glasgow, Ayr, Scotland, United Kingdom",OK
    PIL,////,////,SGPI,UNK,0.357,Pilar,"Pilar, Paraguay",OWNO,////,////,Carlos Miguel Jiménez Airport,"Pilar, Ñeembucú, Paraguay",OK
    PIM,PIM,KPIM,KPIM,OK,0.834,Garden Harris County,"Pine Mountain (GA), USA",OWNO,HARRIS COUNTY,"PINE MOUNTAIN, GA - UNITED STATES",Harris County Airport,"Pine Mountain, Georgia, United States",OK
    PIN,////,////,SWPI,UNK,1.0,Parintins,"Parintins, Brazil",OWNO,////,////,????,"Parintins, Amazonas, Brazil",OK
    PIO,////,////,SPSO,UNK,0.25,Pisco,"Pisco, Peru",OWNO,////,////,Capitán FAP Renán Elías Olivera Airport,"Pisco, Ica, Perú",OK
    PIP,PNP,PAPN,PAPN,OK,1.0,Pilot Point Airport,"Pilot Point (AK), USA",OWNO,PILOT POINT,"PILOT POINT, AK - UNITED STATES",????,"Pilot Point, Alaska, United States",OK
    PIQ,////,////,////,UNK,1.0,Pipillipai,"Pipillipai, Guyana",OWNO,////,////,????,"Pipillipai, Cuyuni-Mazaruni, Guyana",OK
    PIR,PIR,KPIR,KPIR,OK,0.781,Pierre,"Pierre (SD), USA",OWNO,PIERRE RGNL,"PIERRE, SD - UNITED STATES",Pierre Regional,"Pierre, South Dakota, United States",OK
    PIS,////,////,LFBI,UNK,1.0,Biard,"Poitiers, France",OWNO,////,////,Biard,"Poitiers, Poitou-Charentes, France",OK
    PIT,PIT,KPIT,KPIT,OK,1.0,Pittsburgh International Airport,"Pittsburgh (PA), USA",OWNO,PITTSBURGH INTL,"PITTSBURGH, PA - UNITED STATES",Pittsburgh International Airport,"Pittsburgh, Pennsylvania, United States",OK
    PIU,////,////,SPUR,UNK,0.192,Piura,"Piura, Peru",OWNO,////,////,Capitán FAP Guillermo Concha Iberico International Airport,"Piura, Piura, Perú",OK
    PIV,////,////,SNPX,UNK,1.0,Pirapora,"Pirapora, Brazil",OWNO,////,////,????,"Pirapora, Minas Gerais, Brazil",OK
    PIW,////,////,CZMN,UNK,1.0,Pikwitonei,"Pikwitonei, Canada",OWNO,////,////,????,"Pikwitonei, Manitoba, Canada",OK
    PIX,////,////,LPPI,UNK,1.0,Pico Island,"Pico Island, Portugal",OWNO,////,////,????,"Pico Island, Região Autónoma dos Açores (Azores), Portugal",OK
    PIZ,PIZ,PPIZ,PPIZ,OK,0.426,Dew Station,"Point Lay (AK), USA",OWNO,POINT LAY LRRS,"POINT LAY, AK - UNITED STATES",Point Lay LRRS Airport,"Point Lay, Alaska, United States",OK
    PJA,////,////,ESUP,UNK,1.0,Pajala,"Pajala, Sweden",OWNO,////,////,????,"Pajala, Norrbottens län, Sweden",OK
    PJB,PAN,KPAN,KPAN,OK,1.0,Payson,"Payson (AZ), USA",OWNO,PAYSON,"PAYSON, AZ - UNITED STATES",????,"Payson, Arizona, United States",OK
    PJC,////,////,SGPJ,UNK,0.563,Pedro Juan Caballero,"Pedro Juan Caballero, Paraguay",OWNO,////,////,Dr. Augusto Roberto Fuster International Airport,"Pedro Juan Caballero, Amambay, Paraguay",OK
    PJG,////,////,OPPG,UNK,1.0,Panjgur,"Panjgur, Pakistan",OWNO,////,////,????,"Panjgur, Balochistan, Pakistan",OK
    PJM,////,////,MRPJ,UNK,1.0,Puerto Jimenez,"Puerto Jimenez, Costa Rica",OWNO,////,////,????,"Puerto Jiménez, Puntarenas, Costa Rica",OK
    PKA,PKA,PAPK,PAPK,OK,0.87,PB,"Napaskiak (AK), USA",OWNO,NAPASKIAK,"NAPASKIAK, AK - UNITED STATES",????,"Napaskiak, Alaska, United States",OK
    PKB,PKB,KPKB,KPKB,OK,0.537,Wood County Airport,"Parkersburg (WV), USA",OWNO,MID-OHIO VALLEY RGNL,"PARKERSBURG, WV - UNITED STATES",Mid-Ohio Valley Regional,"Parkersburg, West Virginia, United States",OK
    PKC,////,////,UHPP,UNK,0.833,Petropavlovsk-Kamchats,"Petropavlovsk-Kamchats, Russia",OWNO,////,////,Yelizovo Airport,"Petropavlovsk-Kamchatsky, Kamchatskiy, Russian Federation (Russia)",OK
    PKD,PKD,KPKD,KPKD,OK,0.49,Park Rapids,"Park Rapids (MN), USA",OWNO,PARK RAPIDS MUNI-KONSHOK FIELD,"PARK RAPIDS, MN - UNITED STATES",Park Rapids Municipal-Konshok Field,"Park Rapids, Minnesota, United States",OK
    PKE,////,////,YPKS,UNK,1.0,Parkes,"Parkes, Australia",OWNO,////,////,????,"Parkes, New South Wales, Australia",OK
    PKF,PKF,KPKF,KPKF,OK,0.801,Park Falls,"Park Falls (WI), USA",OWNO,PARK FALLS MUNI,"PARK FALLS, WI - UNITED STATES",Park Falls Municipal Airport,"Park Falls, Wisconsin, United States",OK
    PKG,////,////,WMPA,UNK,0.824,Pangkor Airport,"Pangkor, Malaysia",OWNO,////,////,????,"Pulau Pangkor, Perak, Malaysia",MAYBE
    PKH,////,////,LGHL,UNK,1.0,Alexion,"Porto Kheli, Greece",OWNO,////,////,Alexion,"Porto Cheli, Peloponnísos (Peloponnese), Greece",OK
    PKJ,////,////,////,UNK,1.0,Playa Grande,"Playa Grande, Guatemala",OWNO,////,////,Playa Grande,"Playa Grande, Ixcán, Quiché, Guatemala",OK
    PKK,////,////,VYPU,UNK,1.0,Pakokku,"Pakokku, Myanmar",OWNO,////,////,????,"Pakokku, Magway, Myanmar (Burma)",OK
    PKM,////,////,////,UNK,1.0,Port Kaituma,"Port Kaituma, Guyana",OWNO,////,////,????,"Port Kaituma, Barima-Waini, Guyana",OK
    PKN,////,////,WAOI,UNK,0.75,Pangkalanbuun,"Pangkalanbuun, Indonesia",OWNO,////,////,Iskandar Airport,"Pangkalanbuun, Kalimantan Tengah (Central Borneo), Indonesia",OK
    PKO,////,////,DBBP,UNK,1.0,Parakou,"Parakou, Benin",OWNO,////,////,Parakou Airport,"Parakou, Borgou, Benin",OK
    PKP,////,////,NTGP,UNK,1.0,Puka Puka,"Puka Puka, French Polynesia",OWNO,////,////,????,"Puka Puka, French Polynesia",OK
    PKR,////,////,VNPK,UNK,1.0,Pokhara,"Pokhara, Nepal",OWNO,////,////,????,"Pokhara, Nepal",OK
    PKT,////,////,YPKT,UNK,1.0,Port Keats,"Port Keats, Australia",OWNO,////,////,????,"Port Keats, Northern Territory, Australia",OK
    PKU,////,////,WIBB,UNK,0.571,Simpang Tiga,"Pekanbaru, Indonesia",OWNO,////,////,Sultan Syarif Kasim II International Airport,"Pekanbaru, Riau, Indonesia",OK
    PKV,////,////,ULOO,UNK,1.0,Pskov,"Pskov, Russia",OWNO,////,////,Pskov Airport,"Pskov, Pskovskaya, Russian Federation (Russia)",OK
    PKW,////,////,FBSP,UNK,1.0,Selebi-Phikwe,"Selebi-Phikwe, Botswana",OWNO,////,////,Selebi-Phikwe Airport,"Selebi-Phikwe, Central, Botswana",OK
    PKY,////,////,WAOP,UNK,0.69,Palangkaraya,"Palangkaraya, Indonesia",OWNO,////,////,Tjilik Riwut Airport,"Palangkaraya, Kalimantan Tengah (Central Borneo), Indonesia",OK
    PKZ,////,////,VLPS,UNK,0.667,Pakse,"Pakse, Lao PDR",OWNO,////,////,Pakse International Airport,"Pakse, Lao People's Democratic Republic (Laos)",OK
    PLA,////,////,////,UNK,1.0,Planadas,"Planadas, Colombia",OWNO,////,////,????,"Planadas, Tolima, Colombia",OK
    PLC,////,////,////,UNK,1.0,Planeta Rica,"Planeta Rica, Colombia",OWNO,////,////,????,"Planeta Rica, Córdoba, Colombia",OK
    PLD,////,////,////,UNK,1.0,Playa Samara,"Playa Samara, Costa Rica",OWNO,////,////,????,"Playa Sámara, Guanacaste, Costa Rica",OK
    PLE,////,////,////,UNK,1.0,Paiela,"Paiela, Papua New Guinea",OWNO,////,////,????,"Paiela, Enga, Papua-New Guinea",OK
    PLF,////,////,FTTP,UNK,1.0,Pala,"Pala, Chad",OWNO,////,////,????,"Pala, Mayo-Kébbi-Est, Chad",OK
    PLH,////,////,EGHD,UNK,0.636,Plymouth,"Plymouth, United Kingdom",OWNO,////,////,Roborough Airport,"Plymouth, Devonshire, England, United Kingdom",OK
    PLI,////,////,////,UNK,1.0,Palm Island,"Palm Island, Saint Vincent and the Grenadines",OWNO,////,////,????,"Palm Island, Saint Vincent and The Grenadines",OK
    PLJ,////,////,////,UNK,1.0,Placencia,"Placencia, Belize",OWNO,////,////,Placencia Airport,"Placencia, Stann Creek, Belize",OK
    PLK,PLK,KPLK,KPLK,OK,0.709,M Graham Clark,"Branson/Point Lookout (MO), USA",OWNO,M GRAHAM CLARK DOWNTOWN,"BRANSON, MO - UNITED STATES",M Graham Clark Downtown Airport,"Branson, Missouri, United States",OK
    PLL,////,////,SBMN,UNK,0.375,Ponta Pelada,"Ponta Pelada, Brazil",OWNO,////,////,Manaus AFB,"Manaus, Amazonas, Brazil",TO DO CHECK
    PLM,////,////,WIPP,UNK,0.842,Mahmud Badaruddin Ii,"Palembang, Indonesia",OWNO,////,////,Sultan Mahmud Badaruddin II International Airport,"Palembang, Sumatera Selatan, Indonesia",OK
    PLN,PLN,KPLN,KPLN,OK,0.778,Emmet County,"Pellston (MI), USA",OWNO,PELLSTON RGNL AIRPORT OF EMMET COUNTY,"PELLSTON, MI - UNITED STATES",Pellston Regional Airport of Emmet County,"Pellston, Michigan, United States",OK
    PLO,////,////,YPLC,UNK,1.0,Port Lincoln,"Port Lincoln, Australia",OWNO,////,////,????,"Port Lincoln, South Australia, Australia",OK
    PLP,////,////,MPLP,UNK,0.421,La Palma,"La Palma, Panama",OWNO,////,////,Captain Ramon Xatruch,"La Palma, Darién, Panamá",OK
    PLQ,////,////,EYPA,UNK,0.75,Palanga,"Palanga, Lithuania",OWNO,////,////,Palanga International Airport,"Palanga, Lithuania",OK
    PLR,PLR,KPLR,KPLR,OK,1.0,t Clair County,"Pell City (AL), USA",OWNO,ST CLAIR COUNTY,"PELL CITY, AL - UNITED STATES",St. Clair County Airport,"Pell City, Alabama, United States",OK
    PLS,////,MBPV,MBPV,OK,1.0,International,"Providenciales, Turks and Caicos Islands",OWNO,PROVIDENCIALES INTL,"PROVIDENCIALES, - TURKS AND CAICOS ISLANDS",Providenciales International Airport,"Providenciales, Caicos Island, Turks and Caicos Islands",OK
    PLT,////,////,SKPL,UNK,1.0,Plato,"Plato, Colombia",OWNO,////,////,Plato Airport,"Plato, Magdalena, Colombia",OK
    PLU,////,////,SBBH,UNK,0.63,Pampulha Airport,"Belo Horizonte, Brazil",OWNO,////,////,Pampulha - Carlos Drummond de Andrade Airport,"Belo Horizonte, Minas Gerais, Brazil",OK
    PLV,////,////,UKHP,UNK,0.56,Poltava,"Poltava, Ukraine",OWNO,////,////,Suprunovka,"Poltava, Poltava, Ukraine",OK
    PLW,////,////,WAML,UNK,1.0,Mutiara,"Palu, Indonesia",OWNO,////,////,Mutiara,"Palu, Sulawesi Tengah, Indonesia",OK
    PLX,////,////,UASS,UNK,0.387,Semipalatinsk,"Semipalatinsk, Kazakhstan",OWNO,////,////,Semey Airport,"Semey, Shyghys Qazaqstan, Kazakhstan",TO DO CHECK
    PLY,C65,////,////,OK,0.752,Plymouth,"Plymouth (IN), USA",OWNO,PLYMOUTH MUNI,"PLYMOUTH, IN - UNITED STATES",Plymouth Municipal Airport,"Plymouth, Indiana, United States",OK
    PLZ,////,////,FAPE,UNK,0.848,Port Elizabeth,"Port Elizabeth, South Africa",OWNO,////,////,Port Elizabeth International Airport,"Port Elizabeth, Eastern Cape, South Africa",OK
    PMA,////,////,HTPE,UNK,1.0,Wawi,"Pemba, Tanzania",OWNO,////,////,Pemba Airport,"Chake-Chake, Pemba South (Kusini Pemba), Tanzania",OK
    PMB,PMB,KPMB,KPMB,OK,0.766,Intermediate,"Pembina (ND), USA",OWNO,PEMBINA MUNI,"PEMBINA, ND - UNITED STATES",Pembina Municipal Airport,"Pembina, North Dakota, United States",OK
    PMC,////,////,SCTE,UNK,0.826,Tepual,"Puerto Montt, Chile",OWNO,////,////,El Tepual International Airport,"Puerto Montt, Los Lagos, Chile",OK
    PMD,PMD,KPMD,KPMD,OK,0.458,Air Force 42,"Palmdale (CA), USA",OWNO,PALMDALE USAF PLANT 42,"PALMDALE, CA - UNITED STATES",Palmdale USAF Plant 42 Airport,"Palmdale, California, United States",OK
    PME,////,////,////,UNK,1.0,Portsmouth,"Portsmouth, United Kingdom",OWNO,////,////,????,"Portsmouth, Hampshire, England, United Kingdom",OK
    PMF,////,////,LIMP,UNK,0.526,Parma,"Milan, Italy",OWNO,////,////,Giuseppe Verdi,"Parma, Emilia-Romagna, Italy",OK
    PMG,////,////,SBPP,UNK,1.0,International,"Ponta Pora, Brazil",OWNO,////,////,Ponta Porã International Airport,"Ponta Porã, Mato Grosso do Sul, Brazil",OK
    PMH,PMH,KPMH,KPMH,OK,0.694,Regional,"Portsmouth (OH), USA",OWNO,GREATER PORTSMOUTH RGNL,"PORTSMOUTH, OH - UNITED STATES",Greater Portsmouth Regional,"Portsmouth, Ohio, United States",OK
    PMI,////,////,LEPA,UNK,0.82,Palma Mallorca,"Palma Mallorca, Balearic Islands, Spain",OWNO,////,////,Son Sant Joan Airport,"Palma de Mallorca, Mallorca, Balearic Islands, Spain",OK
    PMK,////,////,YPAM,UNK,1.0,Palm Island,"Palm Island, Australia",OWNO,////,////,????,"Palm Island, Queensland, Australia",OK
    PML,1AK3,PAAL,PAAL,OK,0.815,AFS,"Port Moller (AK), USA",OWNO,PORT MOLLER,"COLD BAY, AK - UNITED STATES",Port Moller Airport,"Cold Bay, Alaska, United States",OK
    PMM,////,////,////,UNK,1.0,Phanom Sarakham,"Phanom Sarakham, Thailand",OWNO,////,////,????,"Phanom Sarakham, Chachoengsao, Thailand",OK
    PMN,////,////,////,UNK,1.0,Pumani,"Pumani, Papua New Guinea",OWNO,////,////,????,"Pumani, Milne Bay, Papua-New Guinea",OK
    PMO,////,////,LICJ,UNK,0.5,Punta Raisi,"Palermo, Italy",OWNO,////,////,Falcone e Borsellino International Airport,"Palermo, Sicily, Italy",OK
    PMQ,////,////,SAWP,UNK,1.0,Perito Moreno,"Perito Moreno, Argentina",OWNO,////,////,????,"Perito Moreno, Santa Cruz, Argentina",OK
    PMR,////,////,NZPM,UNK,1.0,Palmerston North,"Palmerston North, New Zealand",OWNO,////,////,????,"Palmerston North, New Zealand",OK
    PMS,////,////,OSPR,UNK,1.0,Palmyra,"Palmyra, Syria",OWNO,////,////,????,"Palmyra, Syrian Arab Republic (Syria)",OK
    PMT,////,////,////,UNK,1.0,Paramakotoi,"Paramakotoi, Guyana",OWNO,////,////,????,"Paramakotoi, Potaro-Siparuni, Guyana",OK
    PMV,////,////,SVMG,UNK,0.716,DelCaribe Gen S Marino,"Porlamar, Venezuela",OWNO,////,////,"Del Caribe ""General Santiago Mariño"" International Airport","Porlamar, Isla Margarita, Nueva Esparta, Venezuela",OK
    PMW,////,////,SBPJ,UNK,0.545,Palmas,"Palmas, Brazil",OWNO,////,////,Tocantins,"Palmas, Paraná, Brazil",OK
    PMX,13MA,////,////,OK,1.0,Metropolitan,"Palmer (MA), USA",OWNO,METROPOLITAN,"PALMER, MA - UNITED STATES",Metropolitan Airport,"Palmer, Massachusetts, United States",OK

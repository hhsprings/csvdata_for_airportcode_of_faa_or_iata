
List for checking certainty of Great Circle Mapper (DSC - DZU)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    DSC,////,////,FKKS,UNK,1.0,Dschang,"Dschang, Cameroon",OWNO,////,////,Dschang Airport,"Dschang, West (Ouest), Cameroon",OK
    DSD,////,////,TFFA,UNK,1.0,La Desirade,"La Desirade, Guadeloupe",OWNO,////,////,????,"Grande Anse, La Desirade, Guadeloupe",OK
    DSE,////,////,HADC,UNK,1.0,Combolcha,"Dessie, Ethiopia",OWNO,////,////,Combolcha Airport,"Combolcha, Amara, Ethiopia",OK
    DSI,DTS,KDTS,KDTS,OK,0.543,Destin,"Destin (FL), USA",OWNO,DESTIN EXECUTIVE,"DESTIN, FL - UNITED STATES",Destin Executive Airport,"Destin, Florida, United States",OK
    DSK,////,////,OPDI,UNK,1.0,Dera Ismail Khan Airport,"Dera Ismail Khan, Pakistan",OWNO,////,////,Dera Ismail Khan Airport,"Dera Ismail Khan, Khyber Pakhtunkhwa, Pakistan",OK
    DSM,DSM,KDSM,KDSM,OK,0.734,Des Moines,"Des Moines IA, USA",OWNO,DES MOINES INTL,"DES MOINES, IA - UNITED STATES",Des Moines International Airport,"Des Moines, Iowa, United States",OK
    DSN,////,////,ZBDS,UNK,1.0,Ordos Ejin Horo Airport,"Ordos, Inner Mongolia, China",OWNO,////,////,Ordos Ejin Horo Airport,"Ordos, Inner Mongolia, China",OK
    DSO,////,////,////,UNK,1.0,Sondok,Sondok,IATA,////,////,Sondok Airport,"Sondok, South Hamgyong, Democratic People's Republic of Korea (North Korea)",MAYBE
    DSV,DSV,KDSV,KDSV,OK,0.771,Dansville,"Dansville (NY), USA",OWNO,DANSVILLE MUNI,"DANSVILLE, NY - UNITED STATES",Dansville Municipal Airport,"Dansville, New York, United States",OK
    DTA,DTA,KDTA,KDTA,OK,1.0,Delta Municipal Airport,"Delta (UT), USA",OWNO,DELTA MUNI,"DELTA, UT - UNITED STATES",Delta Municipal Airport,"Delta, Utah, United States",OK
    DTB,////,////,WIMN,UNK,nan,////,////,////,////,////,Silangit Airport,"Siborong-Borong, Sumatera Utara, Indonesia",UNK
    DTD,////,////,WALJ,UNK,1.0,Datadawai Airport,"Datadawai, Long Lunuk, Indonesia",OWNO,////,////,Datah Dawai Airport,"Long Lunuk, Kalimantan Timur (East Borneo), Indonesia",OK
    DTE,////,////,RPUD,UNK,0.471,Bagasbas Airport,"Daet, Camarines Norte province, Philippines",OWNO,////,////,????,"Daet, Philippines",OK
    DTL,DTL,KDTL,KDTL,OK,0.734,Municipal,"Detroit Lakes (MN), USA",OWNO,DETROIT LAKES-WETHING FIELD,"DETROIT LAKES, MN - UNITED STATES",Detroit Lakes-Wething Field,"Detroit Lakes, Minnesota, United States",OK
    DTM,////,////,EDLW,UNK,0.7,Dortmund,"Dortmund, Germany",OWNO,////,////,Wickede,"Dortmund, North Rhine-Westphalia, Germany",OK
    DTN,DTN,KDTN,KDTN,OK,1.0,Downtown,"Shreveport (LA), USA",OWNO,SHREVEPORT DOWNTOWN,"SHREVEPORT, LA - UNITED STATES",Shreveport Downtown Airport,"Shreveport, Louisiana, United States",OK
    DTT,////,////,////,UNK,0.737,Detroit City,"Detroit City (MI), USA",OWNO,////,////,Metropolitan Area,"Detroit, Michigan, United States",OK
    DTW,DTW,KDTW,KDTW,OK,0.667,Wayne County,"Detroit (MI), USA",OWNO,DETROIT METROPOLITAN WAYNE COUNTY,"DETROIT, MI - UNITED STATES",Detroit Metropolitan Wayne County Airport,"Detroit, Michigan, United States",OK
    DUA,DUA,KDUA,KDUA,OK,0.627,Eaker,"Durant (OK), USA",OWNO,DURANT RGNL - EAKER FIELD,"DURANT, OK - UNITED STATES",Durant Regional - Eaker Field,"Durant, Oklahoma, United States",OK
    DUB,////,////,EIDW,UNK,0.75,Dublin,"Dublin, Ireland",OWNO,////,////,Dublin International Airport,"Dublin, County Dublin, Leinster, Ireland",OK
    DUC,DUC,KDUC,KDUC,OK,0.826,Halliburton,"Duncan (OK), USA",OWNO,HALLIBURTON FIELD,"DUNCAN, OK - UNITED STATES",Halliburton Field,"Duncan, Oklahoma, United States",OK
    DUD,////,////,NZDN,UNK,0.778,Dunedin,"Dunedin, New Zealand",OWNO,////,////,Dunedin International Airport,"Dunedin, New Zealand",OK
    DUE,////,////,FNDU,UNK,1.0,Dundo,"Dundo, Angola",OWNO,////,////,????,"Dundo, Angola",OK
    DUF,7NC2,////,////,OK,1.0,Pine Island Airport,"Duck (NC), USA",OWNO,PINE ISLAND,"COROLLA, NC - UNITED STATES",Pine Island Airport,"Corolla, North Carolina, United States",OK
    DUG,DUG,KDUG,KDUG,OK,1.0,Bisbee-Douglas International,"Douglas (AZ), USA",OWNO,BISBEE DOUGLAS INTL,"DOUGLAS BISBEE, AZ - UNITED STATES",Bisbee Douglas International Airport,"Douglas, Arizona, United States",OK
    DUJ,DUJ,KDUJ,KDUJ,OK,0.401,Jefferson County,"Dubois (PA), USA",OWNO,DUBOIS RGNL,"DUBOIS, PA - UNITED STATES",Dubois Regional,"Dubois, Pennsylvania, United States",OK
    DUK,////,////,FADK,UNK,1.0,Dukuduk,"Dukuduk, South Africa",OWNO,////,////,????,"Dukuduk, KwaZulu-Natal, South Africa",OK
    DUM,////,////,WIBD,UNK,1.0,Pinang Kampai,"Dumai, Indonesia",OWNO,////,////,Pinang Kampai Airport,"Dumai, Riau, Indonesia",OK
    DUQ,////,////,////,UNK,1.0,Duncan/Quam,"Duncan/Quam, Canada",OWNO,////,////,????,"Duncan, British Columbia, Canada",OK
    DUR,////,////,FALE,UNK,0.741,Durban International,"Durban, South Africa",OWNO,////,////,King Shaka International Airport,"Durban, KwaZulu-Natal, South Africa",OK
    DUS,////,////,EDDL,UNK,0.686,Flughafen Düsseldorf International,"Duesseldorf, Germany",OWNO,////,////,Rhein-Ruhr,"Düsseldorf, North Rhine-Westphalia, Germany",OK
    DUT,DUT,PADU,PADU,OK,0.4,Emergency Field,"Dutch Harbor (AK), USA",OWNO,UNALASKA,"UNALASKA, AK - UNITED STATES",????,"Unalaska, Alaska, United States",OK
    DVA,////,////,LRDV,UNK,1.0,Deva,"Deva, Romania",OWNO,////,////,????,"Deva, Romania",OK
    DVL,DVL,KDVL,KDVL,OK,0.834,Devils Lake,"Devils Lake (ND), USA",OWNO,DEVILS LAKE RGNL,"DEVILS LAKE, ND - UNITED STATES",Devils Lake Regional,"Devils Lake, North Dakota, United States",OK
    DVN,DVN,KDVN,KDVN,OK,0.734,Davenport,"Davenport IA, USA",OWNO,DAVENPORT MUNI,"DAVENPORT, IA - UNITED STATES",Davenport Municipal Airport,"Davenport, Iowa, United States",OK
    DVO,////,////,RPMD,UNK,0.438,Mati,"Davao, Philippines",OWNO,////,////,Francisco Bangoy International Airport,"Davao City, Philippines",MAYBE
    DVP,////,////,YDPD,UNK,1.0,Davenport Downs,"Davenport Downs, Australia",OWNO,////,////,????,"Davenport Downs, Queensland, Australia",OK
    DVR,////,////,YDMN,UNK,0.783,Daly River,"Daly River, Australia",OWNO,////,////,Daly River Mission Airport,"Daly River Mission, Northern Territory, Australia",MAYBE
    DVT,DVT,KDVT,KDVT,OK,1.0,Phoenix-Deer Valley,"Phoenix (AZ), USA",OWNO,PHOENIX DEER VALLEY,"PHOENIX, AZ - UNITED STATES",Phoenix Deer Valley Airport,"Phoenix, Arizona, United States",OK
    DWA,////,////,FWDW,UNK,1.0,Dwangwa,"Dwangwa, Malawi",OWNO,////,////,????,"Dwangwa, Malawi",OK
    DWB,////,////,FMNO,UNK,1.0,Soalala,"Soalala, Madagascar",OWNO,////,////,????,"Soalala, Madagascar",OK
    DWC,////,////,OMDW,UNK,nan,////,////,////,////,////,Al Maktoum International Airport,"Jebel Ali, Dubai, United Arab Emirates",UNK
    DWD,////,////,OEDM,UNK,nan,////,////,////,////,////,Prince Salman bin Abdulaziz Airport,"Al-Dawadmi, Saudi Arabia",UNK
    DWH,DWH,KDWH,KDWH,OK,0.766,David Wayne Hooks,"Houston (TX), USA",OWNO,DAVID WAYNE HOOKS MEMORIAL,"HOUSTON, TX - UNITED STATES",David Wayne Hooks Memorial Airport,"Houston, Texas, United States",OK
    DWO,////,////,////,UNK,0.703,Diyawanna Oya,Kotte,IATA,////,////,Diyawanna Oya Waterdrome,"Sri Jayawardenepura Kotte, Western Province, Sri Lanka (Ceylon)",MAYBE
    DWS,////,////,////,UNK,1.0,Walt Disney World,"Orlando (FL), USA",OWNO,////,////,Walt Disney World,"Orlando, Florida, United States",OK
    DXB,////,////,OMDB,UNK,0.667,Dubai,"Dubai, United Arab Emirates",OWNO,////,////,Dubai International Airport,"Dubai, Dubai, United Arab Emirates",OK
    DXD,////,////,YDIX,UNK,1.0,Dixie,"Dixie, Australia",OWNO,////,////,????,"Dixie, Queensland, Australia",OK
    DXR,DXR,KDXR,KDXR,OK,0.781,Danbury,"Danbury (CT), USA",OWNO,DANBURY MUNI,"DANBURY, CT - UNITED STATES",Danbury Municipal Airport,"Danbury, Connecticut, United States",OK
    DYA,////,////,YDYS,UNK,1.0,Dysart,"Dysart, Australia",OWNO,////,////,????,"Dysart, Queensland, Australia",OK
    DYG,////,////,ZGDY,UNK,0.809,Dayong,"Dayong, PR China",OWNO,////,////,Zhangjiajie Hehua Airport,"Zhangjiajie, Hunan, China",OK
    DYL,DYL,KDYL,KDYL,OK,1.0,Doylestown,"Doylestown (PA), USA",OWNO,DOYLESTOWN,"DOYLESTOWN, PA - UNITED STATES",????,"Doylestown, Pennsylvania, United States",OK
    DYM,////,////,////,UNK,1.0,Diamantina Lakes,"Diamantina Lakes, Australia",OWNO,////,////,????,"Diamantina Lakes, Queensland, Australia",OK
    DYR,////,////,UHMA,UNK,0.667,Anadyr,"Anadyr, Russia",OWNO,////,////,Ugolny Airport,"Anadyr, Chukotskiy, Russian Federation (Russia)",OK
    DYS,DYS,KDYS,KDYS,OK,1.0,Dyess AFB,"Abilene (TX), USA",OWNO,DYESS AFB,"ABILENE, TX - UNITED STATES",Dyess AFB,"Abilene, Texas, United States",OK
    DYU,////,////,UTDD,UNK,0.762,Dushanbe,"Dushanbe, Tajikistan",OWNO,////,////,Dushanbe International Airport,"Dushanbe, Dushanbe, Tajikistan",OK
    DYW,////,////,YDLW,UNK,1.0,Daly Waters,"Daly Waters, Australia",OWNO,////,////,????,"Daly Waters, Northern Territory, Australia",OK
    DZA,////,////,FMCZ,UNK,0.64,Dzaoudzi,"Dzaoudzi, Mayotte",OWNO,////,////,Dzaoudzi-Pamandzi,"Dzaoudzi, Pamandzi Island, Mayotte",OK
    DZI,////,////,////,UNK,1.0,Codazzi,"Codazzi, Colombia",OWNO,////,////,????,"Codazzi, Cesar, Colombia",OK
    DZN,////,////,UAKD,UNK,0.941,Zhezhazgan,"Zhezkazgan, Kazakhstan",OWNO,////,////,Zhezkazgan Airport,"Zhezkazgan, Qaraghandy, Kazakhstan",OK
    DZO,////,////,SUDU,UNK,0.435,Durazno,"Durazno, Uruguay",OWNO,////,////,Santa Bernardina International Airport,"Durazno, Durazno, Uruguay",OK
    DZU,////,////,ZUDZ,UNK,1.0,Dazu,"Dazu, PR China",OWNO,////,////,????,"Dazu, Chongqing, China",OK

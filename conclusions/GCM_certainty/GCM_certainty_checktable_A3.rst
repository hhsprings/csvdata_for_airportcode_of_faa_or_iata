
List for checking certainty of Great Circle Mapper (ALY - ARY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ALY,////,////,HEAX,UNK,1.0,El Nouzha Airport (Alexandria International Airport),"Alexandria, Egypt",OWNO,////,////,Alexandria International Airport,"Alexandria, Al Iskandariyah (Alexandria), Egypt",OK
    AMA,AMA,KAMA,KAMA,OK,1.0,Rick Husband Amarillo International Airport,"Amarillo (TX), USA",OWNO,RICK HUSBAND AMARILLO INTL,"AMARILLO, TX - UNITED STATES",Rick Husband Amarillo International Airport,"Amarillo, Texas, United States",OK
    AMB,////,////,FMNE,UNK,1.0,Ambilobe,"Ambilobe, Madagascar",OWNO,////,////,????,"Ambilobe, Madagascar",OK
    AMC,////,////,FTTN,UNK,1.0,Am Timan,"Am Timan, Chad",OWNO,////,////,????,"Am Timan, Salamat, Chad",OK
    AMD,////,////,VAAH,UNK,1.0,Sardar Vallabhbhai Patel International Airport,"Ahmedabad, India",OWNO,////,////,Sardar Vallabhbhai Patel International Airport,"Ahmedabad, Gujarat, India",OK
    AME,////,////,////,UNK,1.0,Alto Molocue,"Alto Molocue, Mozambique",OWNO,////,////,????,"Alto Molocue, Mozambique",OK
    AMF,////,////,////,UNK,1.0,Ama,"Ama, Papua New Guinea",OWNO,////,////,????,"Ama, East Sepik, Papua-New Guinea",OK
    AMG,////,////,////,UNK,1.0,Amboin,"Amboin, Papua New Guinea",OWNO,////,////,????,"Amboin, East Sepik, Papua-New Guinea",OK
    AMH,////,////,HAAM,UNK,0.952,Arba Mintch,"Arba Mintch, Ethiopia",OWNO,////,////,Arba Minch Airport,"Arba Minch, SNNPR, Ethiopia",MAYBE
    AMJ,////,////,SNAR,UNK,1.0,Almenara,"Almenara, Brazil",OWNO,////,////,????,"Almenara, Minas Gerais, Brazil",OK
    AMK,00C,////,////,OK,0.734,Animas Airpark,"Durango (CO), USA",OWNO,ANIMAS AIR PARK,"DURANGO, CO - UNITED STATES",Animas Air Park,"Durango, Colorado, United States",OK
    AML,////,////,////,UNK,1.0,Puerto Armuellas,"Puerto Armuellas, Panama",OWNO,////,////,????,"Puerto Armuellas, Chiriquí, Panamá",OK
    AMM,////,////,OJAI,UNK,1.0,Queen Alia International Airport,"Amman, Jordan",OWNO,////,////,Queen Alia International Airport,"Amman, Jordan",OK
    AMN,AMN,KAMN,KAMN,OK,1.0,Gratiot Community,"Alma (MI), USA",OWNO,GRATIOT COMMUNITY,"ALMA, MI - UNITED STATES",Gratiot Community Airport,"Alma, Michigan, United States",OK
    AMO,////,////,FTTU,UNK,1.0,Mao,"Mao, Chad",OWNO,////,////,????,"Mao, Kanem, Chad",OK
    AMP,////,////,FMSY,UNK,1.0,Ampanihy,"Ampanihy, Madagascar",OWNO,////,////,????,"Ampanihy, Madagascar",OK
    AMQ,////,////,WAPP,UNK,1.0,Pattimura,"Ambon, Indonesia",OWNO,////,////,Pattimura,"Ambon, Maluku, Indonesia",OK
    AMS,////,////,EHAM,UNK,1.0,Amsterdam-Schiphol,"Amsterdam, Netherlands",OWNO,////,////,Schiphol,"Amsterdam, Noord-Holland (North Holland), Netherlands",OK
    AMT,////,////,YAMT,UNK,1.0,Amata,"Amata, Australia",OWNO,////,////,????,"Amata, Northern Territory, Australia",OK
    AMU,////,////,////,UNK,1.0,Amanab,"Amanab, Papua New Guinea",OWNO,////,////,????,"Amanab, Sandaun, Papua-New Guinea",OK
    AMV,////,////,ULDD,UNK,1.0,Amderma,"Amderma, Russia",OWNO,////,////,????,"Amderma, Novaya Zemlya, Nenetskiy, Russian Federation (Russia)",OK
    AMW,AMW,KAMW,KAMW,OK,0.613,Ames,"Ames IA, USA",OWNO,AMES MUNI,"AMES, IA - UNITED STATES",Ames Municipal Airport,"Ames, Iowa, United States",OK
    AMX,////,////,YAMM,UNK,1.0,Ammaroo,"Ammaroo, Australia",OWNO,////,////,????,"Ammaroo, Northern Territory, Australia",OK
    AMY,////,////,FMMB,UNK,1.0,Ambatomainty,"Ambatomainty, Madagascar",OWNO,////,////,Ambatomainty Airport,"Ambatomainty, Madagascar",OK
    AMZ,////,////,NZAR,UNK,1.0,Ardmore,"Ardmore, New Zealand",OWNO,////,////,????,"Ardmore, New Zealand",OK
    ANB,ANB,KANB,KANB,OK,0.506,Metropolitan Area,"Anniston (AL), USA",OWNO,ANNISTON RGNL,"ANNISTON, AL - UNITED STATES",Anniston Regional,"Anniston, Alabama, United States",OK
    ANC,ANC,PANC,PANC,OK,1.0,Ted Stevens Anchorage International Airport,"Anchorage (AK), USA",OWNO,TED STEVENS ANCHORAGE INTL,"ANCHORAGE, AK - UNITED STATES",Ted Stevens Anchorage International Airport,"Anchorage, Alaska, United States",OK
    AND,AND,KAND,KAND,OK,0.826,Anderson,"Anderson (SC), USA",OWNO,ANDERSON RGNL,"ANDERSON, - UNITED STATES",Anderson Regional,"Anderson, South Carolina, United States",OK
    ANE,////,////,LFJR,UNK,1.0,Marce,"Angers, France",OWNO,////,////,Marce,"Angers, Pays de la Loire, France",OK
    ANF,////,////,SCFA,UNK,0.667,Cerro Moreno International Airport,"Antofagasta, Chile",OWNO,////,////,Andrés Sabella International Airport,"Antofagasta, Antofagasta, Chile",OK
    ANG,////,////,LFBU,UNK,1.0,Brie-Champniers,"Angouleme, France",OWNO,////,////,Brie-Champniers,"Angoulême, Poitou-Charentes, France",OK
    ANI,ANI,PANI,PANI,OK,1.0,Aniak,"Aniak (AK), USA",OWNO,ANIAK,"ANIAK, AK - UNITED STATES",????,"Aniak, Alaska, United States",OK
    ANJ,////,////,FCBZ,UNK,1.0,Zanaga,"Zanaga, Congo (ROC)",OWNO,////,////,Zanaga Airport,"Zanaga, Congo (Republic of)",OK
    ANK,////,////,LTAD,UNK,1.0,Etimesgut,"Ankara, Turkey",OWNO,////,////,Etimesgut,"Ankara, Turkey",OK
    ANL,////,////,////,UNK,1.0,Andulo,"Andulo, Angola",OWNO,////,////,????,"Andulo, Angola",OK
    ANM,////,////,FMNH,UNK,1.0,Antsirabato,"Antalaha, Madagascar",OWNO,////,////,Antsirabato,"Antalaha, Madagascar",OK
    ANN,ANN,PANT,PANT,OK,1.0,Annette Island,"Annette Island (AK), USA",OWNO,ANNETTE ISLAND,"ANNETTE, AK - UNITED STATES",Annette Island Airport,"Annette, Alaska, United States",OK
    ANO,////,////,FQAG,UNK,1.0,Angoche,"Angoche, Mozambique",OWNO,////,////,????,"Angoche, Mozambique",OK
    ANP,ANP,KANP,KANP,OK,1.0,Lee Airport,"Annapolis (MD), USA",OWNO,LEE,"ANNAPOLIS, MD - UNITED STATES",Lee Airport,"Annapolis, Maryland, United States",OK
    ANQ,ANQ,KANQ,KANQ,OK,0.964,Tri-State Steuben City,"Angola (IN), USA",OWNO,TRI-STATE STEUBEN COUNTY,"ANGOLA, IN - UNITED STATES",Tri-State Steuben County Airport,"Angola, Indiana, United States",OK
    ANR,////,////,EBAW,UNK,1.0,Antwerpen International Airport (Deurne),"Antwerp, Belgium",OWNO,////,////,Deurne,"Antwerp, Antwerpen, Belgium",OK
    ANS,////,////,SPHY,UNK,1.0,Andahuaylas,"Andahuaylas, Peru",OWNO,////,////,Andahuaylas Airport,"Andahuaylas, Apurímac, Perú",OK
    ANU,////,////,TAPA,UNK,1.0,V.C. Bird International Airport,"St. John's, Antigua and Barbuda",OWNO,////,////,V.C. Bird International Airport,"Antigua, Antigua Island, Antigua and Barbuda",OK
    ANV,ANV,PANV,PANV,OK,1.0,Anvik,"Anvik (AK), USA",OWNO,ANVIK,"ANVIK, AK - UNITED STATES",????,"Anvik, Alaska, United States",OK
    ANW,ANW,KANW,KANW,OK,0.771,Ainsworth,"Ainsworth (NE), USA",OWNO,AINSWORTH RGNL,"AINSWORTH, NE - UNITED STATES",Ainsworth Regional,"Ainsworth, Nebraska, United States",OK
    ANX,////,////,ENAN,UNK,1.0,Andenes,"Andenes, Norway",OWNO,////,////,Andoya/Andenes,"Andoya, Norway",OK
    ANY,ANY,KANY,KANY,OK,0.719,Anthony,"Anthony (KS), USA",OWNO,ANTHONY MUNI,"ANTHONY, KS - UNITED STATES",Anthony Municipal Airport,"Anthony, Kansas, United States",OK
    ANZ,////,////,////,UNK,1.0,Angus Downs,"Angus Downs, Australia",OWNO,////,////,????,"Angus Downs, Northern Territory, Australia",OK
    AOA,////,////,////,UNK,1.0,Aroa,"Aroa, Papua New Guinea",OWNO,////,////,????,"Aroa, Central, Papua-New Guinea",OK
    AOB,////,////,////,UNK,1.0,Annanberg,"Annanberg, Papua New Guinea",OWNO,////,////,????,"Annanberg, Madang, Papua-New Guinea",OK
    AOC,////,////,EDAC,UNK,1.0,Altenburg Nobitz,"Altenburg, Germany",OWNO,////,////,Nobitz,"Altenburg, Thüringen, Germany",OK
    AOD,////,////,////,UNK,1.0,Abou Deia,"Abou Deia, Chad",OWNO,////,////,Abou-Deïa Airport,"Abou-Deïa, Salamat, Chad",OK
    AOE,////,////,LTBY,UNK,0.756,Anadolu University,"Eskisehir, Turkey",OWNO,////,////,Eskisehir Anadolu Airport,"Eskisehir, Eskisehir, Turkey",OK
    AOG,////,////,ZYAS,UNK,0.917,Anshan,"Anshan, PR China",OWNO,////,////,Anshan AB,"Anshan, Liaoning, China",OK
    AOH,AOH,KAOH,KAOH,OK,1.0,Allen County,"Lima (OH), USA",OWNO,LIMA ALLEN COUNTY,"LIMA, OH - UNITED STATES",Lima Allen County Airport,"Lima, Ohio, United States",OK
    AOI,////,////,LIPY,UNK,1.0,Ancona Falconara Airport,"Ancona, Italy",OWNO,////,////,Falconara,"Ancona, Marche, Italy",OK
    AOJ,////,////,RJSA,UNK,1.0,Aomori,"Aomori, Japan",OWNO,////,////,????,"Aomori, Aomori, Japan",OK
    AOK,////,////,LGKP,UNK,1.0,Karpathos,"Karpathos, Greece",OWNO,////,////,????,"Karpathos, Notío Aigaío (Southern Aegean), Greece",OK
    AOL,////,////,SARL,UNK,1.0,Paso De Los Libres,"Paso De Los Libres, Argentina",OWNO,////,////,????,"Paso de los Libres, Corrientes, Argentina",OK
    AOM,////,////,OOAD,UNK,1.0,Adam,Adam,IATA,////,////,Adam Airport,"Adam, Oman",MAYBE
    AON,////,////,////,UNK,1.0,Arona,"Arona, Papua New Guinea",OWNO,////,////,????,"Arona, Eastern Highlands, Papua-New Guinea",OK
    AOO,AOO,KAOO,KAOO,OK,0.608,Martinsburg,"Altoona (PA), USA",OWNO,ALTOONA-BLAIR COUNTY,"ALTOONA, PA - UNITED STATES",Altoona-Blair County Airport,"Altoona, Pennsylvania, United States",OK
    AOR,////,////,WMKA,UNK,0.583,Alor Setar,"Alor Setar, Malaysia",OWNO,////,////,Sultan Abdul Halim,"Alor Setar, Kedah, Malaysia",OK
    AOS,AK81,////,////,OK,0.626,Amook,"Amook (AK), USA",OWNO,AMOOK BAY,"AMOOK BAY, AK - UNITED STATES",Amook Bay SPB,"Amook Bay, Alaska, United States",OK
    AOT,////,////,LIMW,UNK,1.0,Corrado Gex,"Aosta, Italy",OWNO,////,////,Corrado Gex,"Aosta, Aosta Valley, Italy",OK
    AOU,////,////,VLAP,UNK,1.0,Attopeu,"Attopeu, Lao PDR",OWNO,////,////,????,"Attopeu, Lao People's Democratic Republic (Laos)",OK
    APA,APA,KAPA,KAPA,OK,0.664,Arapahoe County - Centennial Airport,"Denver (CO), USA",OWNO,CENTENNIAL,"DENVER, CO - UNITED STATES",Centennial Airport,"Denver, Colorado, United States",OK
    APB,////,////,SLAP,UNK,1.0,Apolo,"Apolo, Bolivia",OWNO,////,////,Apolo Airport,"Apolo, Franz Tamayo, La Paz, Bolivia",OK
    APC,APC,KAPC,KAPC,OK,1.0,Napa County,"Napa (CA), USA",OWNO,NAPA COUNTY,"NAPA, CA - UNITED STATES",Napa County Airport,"Napa, California, United States",OK
    APF,APF,KAPF,KAPF,OK,0.719,Naples,"Naples (FL), USA",OWNO,NAPLES MUNI,"NAPLES, FL - UNITED STATES",Naples Municipal Airport,"Naples, Florida, United States",OK
    APG,APG,KAPG,KAPG,OK,1.0,Phillips AAF,"Aberdeen (MD), USA",OWNO,PHILLIPS AAF,"ABERDEEN PROVING GROUNDS(ABERDEEN), MD - UNITED STATES",Phillips AAF Airport,"Aberdeen Proving Grounds, Maryland, United States",OK
    APH,APH,KAPH,KAPH,OK,0.781,Camp A P Hill,"Bowling Green (VA), USA",OWNO,A P HILL AAF (FORT A P HILL),"FORT A. P. HILL, VA - UNITED STATES",A P Hill AAF Airport,"Fort A. P. Hill, Virginia, United States",OK
    API,////,////,SKAP,UNK,0.276,Apiay,"Apiay, Colombia",OWNO,////,////,Luis Fernando Gómez Niño AB,"Apiay, Meta, Colombia",OK
    APK,////,////,NTGD,UNK,1.0,Apataki,"Apataki, French Polynesia",OWNO,////,////,????,"Apataki, Tuamotu Islands, French Polynesia",OK
    APL,////,////,FQNP,UNK,1.0,Nampula,"Nampula, Mozambique",OWNO,////,////,????,"Nampula, Mozambique",OK
    APN,APN,KAPN,KAPN,OK,1.0,Alpena County Regional,"Alpena (MI), USA",OWNO,ALPENA COUNTY RGNL,"ALPENA, MI - UNITED STATES",Alpena County Regional,"Alpena, Michigan, United States",OK
    APO,////,////,SKLC,UNK,0.429,Apartado,"Apartado, Colombia",OWNO,////,////,Antonio Roldán Betancourt Airport,"Apartadó, Antioquia, Colombia",OK
    APP,////,////,////,UNK,1.0,Asapa,"Asapa, Papua New Guinea",OWNO,////,////,????,"Asapa, Northern, Papua-New Guinea",OK
    APQ,////,////,SNAL,UNK,1.0,Arapiraca,"Arapiraca, Brazil",OWNO,////,////,????,"Arapiraca, Alagoas, Brazil",OK
    APR,////,////,////,UNK,1.0,April River,"April River, Papua New Guinea",OWNO,////,////,????,"April River, East Sepik, Papua-New Guinea",OK
    APS,////,////,SWNS,UNK,1.0,Anapolis,"Anapolis, Brazil",OWNO,////,////,????,"Anapolis, Goiás, Brazil",OK
    APT,APT,KAPT,KAPT,OK,0.716,Marion County,"Jasper (TN), USA",OWNO,MARION COUNTY-BROWN FIELD,"JASPER, TN - UNITED STATES",Marion County-Brown Field,"Jasper, Tennessee, United States",OK
    APU,////,////,SSAP,UNK,1.0,Apucarana,"Apucarana, Brazil",OWNO,////,////,????,"Apucarana, Paraná, Brazil",OK
    APV,APV,KAPV,KAPV,OK,1.0,Apple Valley,"Apple Valley (CA), USA",OWNO,APPLE VALLEY,"APPLE VALLEY, CA - UNITED STATES",????,"Apple Valley, California, United States",OK
    APW,////,////,NSFA,UNK,1.0,Faleolo International Airport,"Apia, Samoa",OWNO,////,////,Faleolo International Airport,"Apia, Upolu Island, Samoa (Western Samoa)",OK
    APX,////,////,SSOG,UNK,1.0,Arapongas,"Arapongas, Brazil",OWNO,////,////,????,"Arapongas, Paraná, Brazil",OK
    APY,////,////,SNAI,UNK,1.0,Alto Parnaiba,"Alto Parnaiba, Brazil",OWNO,////,////,Alto Parnaíba,"Alto Parnaíba, Maranhão, Brazil",OK
    APZ,////,////,SAHZ,UNK,1.0,Zapala,"Zapala, Argentina",OWNO,////,////,????,"Zapala, Neuquén, Argentina",OK
    AQA,////,////,SBAQ,UNK,0.432,Araraquara,"Araraquara, Brazil",OWNO,////,////,Bartolomeu de Gusmão State Airport,"Araraquara, São Paulo, Brazil",OK
    AQB,////,////,MGQC,UNK,1.0,Quiche Airport,"Quiche, Guatemala",OWNO,////,////,????,"Santa Cruz del Quiché, Quiché, Guatemala",OK
    AQG,////,////,ZSAQ,UNK,1.0,Anqing,"Anqing, PR China",OWNO,////,////,????,"Anqing, Anhui, China",OK
    AQI,////,////,OEPA,UNK,0.533,Qaisumah,"Qaisumah, Saudi Arabia",OWNO,////,////,Hafr al-Batin Airport,"Al-Qaisumah, Saudi Arabia",TO DO CHECK
    AQJ,////,////,OJAQ,UNK,1.0,King Hussein International Airport,"Aqaba, Jordan",OWNO,////,////,King Hussein International Airport,"Aqaba, Jordan",OK
    AQM,////,////,SWNI,UNK,0.643,Ariquemes,"Ariquemes, Brazil",OWNO,////,////,Nova Vida,"Ariquemes, Rondônia, Brazil",OK
    AQP,////,////,SPQU,UNK,0.882,Rodriguez Ballon International Airport,"Arequipa, Peru",OWNO,////,////,Alfredo Rodríguez Ballón International Airport,"Arequipa, Arequipa, Perú",OK
    AQS,////,////,////,UNK,1.0,Saqani,"Saqani, Fiji",OWNO,////,////,????,"Saqani, Fiji",OK
    ARA,ARA,KARA,KARA,OK,1.0,Acadiana Regional,"New Iberia (LA), USA",OWNO,ACADIANA RGNL,"NEW IBERIA, LA - UNITED STATES",Acadiana Regional,"New Iberia, Louisiana, United States",OK
    ARB,ARB,KARB,KARB,OK,1.0,Municipal,"Ann Arbor (MI), USA",OWNO,ANN ARBOR MUNI,"ANN ARBOR, MI - UNITED STATES",Ann Arbor Municipal Airport,"Ann Arbor, Michigan, United States",OK
    ARC,ARC,PARC,PARC,OK,1.0,Arctic Village,"Arctic Village (AK), USA",OWNO,ARCTIC VILLAGE,"ARCTIC VILLAGE, AK - UNITED STATES",????,"Arctic Village, Alaska, United States",OK
    ARD,////,////,WATM,UNK,1.0,Alor Island,"Alor Island, Indonesia",OWNO,////,////,Alor Island Airport,"Kalabahi, Alor Island, Nusa Tenggara Timur, Indonesia",OK
    ARE,ABO,TJAB,TJAB,OK,0.778,Antonio/Nery/Juarbe Pol,"Arecibo, Puerto Rico",OWNO,ANTONIO/NERY/JUARBE POL,"ARECIBO, PR - UNITED STATES",Antonio (Nery) Juarbe Pol Airport,"Arecibo, Puerto Rico, United States",OK
    ARF,////,////,////,UNK,1.0,Acaricuara,"Acaricuara, Colombia",OWNO,////,////,????,"Acaricuara, Caquetá, Colombia",OK
    ARG,ARG,KARG,KARG,OK,0.25,Regional,Walnut Ridge,IATA,WALNUT RIDGE RGNL,"WALNUT RIDGE, AR - UNITED STATES",Walnut Ridge Regional,"Walnut Ridge, Arkansas, United States",MAYBE
    ARH,////,////,ULAA,UNK,1.0,Talagi Airport,"Arkhangelsk, Russia",OWNO,////,////,Talagi Airport,"Arkhangelsk, Arkhangel'skaya, Russian Federation (Russia)",OK
    ARI,////,////,SCAR,UNK,1.0,Chacalluta International Airport,"Arica, Chile",OWNO,////,////,Chacalluta International Airport,"Arica, Arica y Parinacota, Chile",OK
    ARJ,////,////,WAJA,UNK,1.0,Arso,"Arso, Indonesia",OWNO,////,////,????,"Arso, Papua, Indonesia",OK
    ARK,////,////,HTAR,UNK,1.0,Arusha,"Arusha, Tanzania",OWNO,////,////,Arusha Airport,"Arusha, Arusha, Tanzania",OK
    ARL,////,////,DFER,UNK,1.0,Arly,"Arly, Burkina Faso",OWNO,////,////,????,"Arly, Burkina Faso",OK
    ARM,////,////,YARM,UNK,1.0,Armidale,"Armidale, Australia",OWNO,////,////,????,"Armidale, New South Wales, Australia",OK
    ARN,////,////,ESSA,UNK,1.0,Arlanda,"Stockholm, Sweden",OWNO,////,////,Arlanda,"Stockholm, Stockholms län, Sweden",OK
    ARO,////,////,////,UNK,1.0,Arboletas,"Arboletas, Colombia",OWNO,////,////,????,"Arboletas, Antioquia, Colombia",OK
    ARP,////,////,////,UNK,1.0,Aragip,"Aragip, Papua New Guinea",OWNO,////,////,????,"Aragip, Milne Bay, Papua-New Guinea",OK
    ARQ,////,////,////,UNK,0.621,Arauquita,"Arauquita, Colombia",OWNO,////,////,El Troncal,"Arauquita, Arauca, Colombia",OK
    ARR,////,////,SAVR,UNK,0.618,Alto Rio Senguerr,"Alto Rio Senguerr, Argentina",OWNO,////,////,D. Casimiro Szlapelis,"Alto Río Senguerr, Chubut, Argentina",OK
    ARS,////,////,SWEC,UNK,1.0,Aragarcas,"Aragarcas, Brazil",OWNO,////,////,????,"Aragarcas, Goiás, Brazil",OK
    ART,ART,KART,KART,OK,0.771,Watertown,"Watertown (NY), USA",OWNO,WATERTOWN INTL,"WATERTOWN, NY - UNITED STATES",Watertown International Airport,"Watertown, New York, United States",OK
    ARU,////,////,SBAU,UNK,1.0,Aracatuba,"Aracatuba, Brazil",OWNO,////,////,????,"Aracatuba, São Paulo, Brazil",OK
    ARV,ARV,KARV,KARV,OK,0.724,Noble F. Lee,"Minocqua (WI), USA",OWNO,LAKELAND/NOBLE F LEE MEMORIAL FIELD,"MINOCQUA-WOODRUFF, WI - UNITED STATES",Lakeland/Noble F Lee Memorial Field,"Minocqua-Woodruff, Wisconsin, United States",OK
    ARW,////,////,LRAR,UNK,0.667,Arad,"Arad, Romania",OWNO,////,////,Arad International Airport,"Arad, Romania",OK
    ARY,////,////,YARA,UNK,1.0,Ararat,"Ararat, Australia",OWNO,////,////,????,"Ararat, Victoria, Australia",OK

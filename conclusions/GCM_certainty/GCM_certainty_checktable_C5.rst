
List for checking certainty of Great Circle Mapper (CSV - CZX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    CSV,CSV,KCSV,KCSV,OK,0.65,Memorial,"Crossville (TN), USA",OWNO,CROSSVILLE MEMORIAL-WHITSON FIELD,"CROSSVILLE, TN - UNITED STATES",Crossville Memorial-Whitson Field,"Crossville, Tennessee, United States",OK
    CSX,////,////,ZGCS,UNK,0.778,Changsha,"Changsha, PR China",OWNO,////,////,Datuopu,"Changsha, Hunan, China",OK
    CSY,////,////,UWKS,UNK,1.0,Cheboksary,"Cheboksary, Russia",OWNO,////,////,????,"Cheboksary, Chavashskaya, Russian Federation (Russia)",OK
    CTA,////,////,LICC,UNK,0.702,Fontanarossa,"Catania, Italy",OWNO,////,////,Catania-Fontanarossa Vincenzo Bellini,"Catania, Sicily, Italy",OK
    CTB,CTB,KCTB,KCTB,OK,0.748,Cut Bank,"Cut Bank (MT), USA",OWNO,CUT BANK INTL,"CUT BANK, MT - UNITED STATES",Cut Bank International Airport,"Cut Bank, Montana, United States",OK
    CTC,////,////,SANC,UNK,1.0,Catamarca,"Catamarca, Argentina",OWNO,////,////,????,"Catamarca, Catamarca, Argentina",OK
    CTD,////,////,MPCE,UNK,0.316,Chitre,"Chitre, Panama",OWNO,////,////,Herrera Alonso Valderrama Airport,"Chitré, Herrera, Panamá",OK
    CTE,////,////,////,UNK,1.0,Carti,"Carti, Panama",OWNO,////,////,Cartí Airport,"Cartí, Cartí Islands, Kuna Yala (Guna Yala), Panamá",OK
    CTF,////,////,MGCT,UNK,1.0,Coatepeque Airport,"Coatepeque, Guatemala",OWNO,////,////,????,"Coatepeque, Quetzaltenango, Guatemala",OK
    CTG,////,////,SKCG,UNK,1.0,Rafael Núñez International Airport,"Cartagena, Colombia",OWNO,////,////,Rafael Núñez International Airport,"Cartagena de Indias, Bolívar, Colombia",OK
    CTH,MQS,KMQS,KMQS,OK,0.929,Chestercounty Carlson,"Coatesville (PA), USA",OWNO,CHESTER COUNTY G O CARLSON,"COATESVILLE, PA - UNITED STATES",Chester County G O Carlson Airport,"Coatesville, Pennsylvania, United States",OK
    CTI,////,////,FNCV,UNK,1.0,Cuito Cuanavale,"Cuito Cuanavale, Angola",OWNO,////,////,????,"Cuito Cuanavale, Angola",OK
    CTK,7G9,////,////,OK,0.79,Canton,"Canton (SD), USA",OWNO,CANTON MUNI,"CANTON, SD - UNITED STATES",Canton Municipal Airport,"Canton, South Dakota, United States",OK
    CTL,////,////,YBCV,UNK,1.0,Charleville,"Charleville, Australia",OWNO,////,////,????,"Charleville, Queensland, Australia",OK
    CTM,////,////,MMCM,UNK,0.778,Chetumal,"Chetumal, Mexico",OWNO,////,////,Chetumal International Airport,"Chetumal, Quintana Roo, México",OK
    CTN,////,////,YCKN,UNK,1.0,Cooktown,"Cooktown, Australia",OWNO,////,////,????,"Cooktown, Queensland, Australia",OK
    CTO,3C8,////,////,OK,0.534,Peconic River,"Calverton (NY), USA",OWNO,CALVERTON EXECUTIVE AIRPARK,"CALVERTON, NY - UNITED STATES",Calverton Executive Airpark,"Calverton, New York, United States",OK
    CTP,////,////,SNCP,UNK,0.692,Carutapera,"Carutapera, Brazil",OWNO,////,////,Aeropuerto Carutapera,"Carutapera, Maranhão, Brazil",OK
    CTQ,////,////,SSVP,UNK,0.622,Do Palmar,"Santa Vitoria, Brazil",OWNO,////,////,Aeropuerto Fernando Ferrari,"Santa Vitória do Palmar, Rio Grande do Sul, Brazil",MAYBE
    CTR,////,////,YCAC,UNK,1.0,Cattle Creek,"Cattle Creek, Australia",OWNO,////,////,????,"Cattle Creek, Northern Territory, Australia",OK
    CTS,////,////,RJCC,UNK,0.816,New Chitose International Airport,"Sapporo, Japan",OWNO,////,////,New Sapporo International Airport,"Sapporo, Hokkaido, Japan",OK
    CTT,////,////,LFMQ,UNK,1.0,Le Castellet,"Le Castellet, France",OWNO,////,////,Le Castellet Airport,"Le Castellet, Provence-Alpes-Côte d'Azur, France",OK
    CTU,////,////,ZUUU,UNK,0.72,Chengdu,"Chengdu, PR China",OWNO,////,////,Shuangliu International Airport,"Chengdu, Sichuan, China",OK
    CTW,P52,////,////,OK,1.0,Cottonwood,"Cottonwood (AZ), USA",OWNO,COTTONWOOD,"COTTONWOOD, AZ - UNITED STATES",????,"Cottonwood, Arizona, United States",OK
    CTX,N03,////,////,OK,0.421,Cortland,"Cortland (NY), USA",OWNO,CORTLAND COUNTY-CHASE FIELD,"CORTLAND, NY - UNITED STATES",Cortland County-Chase Field,"Cortland, New York, United States",OK
    CTY,CTY,KCTY,KCTY,OK,1.0,Cross City,"Cross City (FL), USA",OWNO,CROSS CITY,"CROSS CITY, FL - UNITED STATES",????,"Cross City, Florida, United States",OK
    CTZ,CTZ,KCTZ,KCTZ,OK,1.0,Sampson County,"Clinton (NC), USA",OWNO,CLINTON-SAMPSON COUNTY,"CLINTON, NC - UNITED STATES",Clinton-Sampson County Airport,"Clinton, North Carolina, United States",OK
    CUA,////,////,MMDA,UNK,1.0,Ciudad Constitucion,"Ciudad Constitucion, Mexico",OWNO,////,////,????,"Ciudad Constitución, Baja California Sur, México",OK
    CUB,CUB,KCUB,KCUB,OK,0.651,Owens Field,"Columbia (SC), USA",OWNO,JIM HAMILTON L B OWENS,"COLUMBIA, SC - UNITED STATES",Jim Hamilton L B Owens Airport,"Columbia, South Carolina, United States",OK
    CUC,////,////,SKCC,UNK,0.882,Camilo Dazo,"Cucuta, Colombia",OWNO,////,////,Camilo Daza International Airport,"Cúcuta, Norte de Santander, Colombia",OK
    CUE,////,////,SECU,UNK,0.375,Cuenca,"Cuenca, Ecuador",OWNO,////,////,Mariscal Lamar International Airport,"Cuenca, Azuay, Ecuador",OK
    CUF,////,////,LIMZ,UNK,1.0,Levaldigi,"Cuneo, Italy",OWNO,////,////,Levaldigi,"Cuneo, Piedmont, Italy",OK
    CUH,CUH,KCUH,KCUH,OK,1.0,Municipal,"Cushing (OK), USA",OWNO,CUSHING MUNI,"CUSHING, OK - UNITED STATES",Cushing Municipal Airport,"Cushing, Oklahoma, United States",OK
    CUK,////,////,////,UNK,1.0,Caye Caulker,"Caye Caulker, Belize",OWNO,////,////,Caye Caulker Airport,"Caye Caulker, Belize, Belize",OK
    CUL,////,////,MMCL,UNK,0.879,Fedl De Bachigualato,"Culiacan, Mexico",OWNO,////,////,Federal de Bachigualato International Airport,"Culiacán, Sinaloa, México",OK
    CUM,////,////,SVCU,UNK,0.37,Cumana,"Cumana, Venezuela",OWNO,////,////,Antonio José de Sucre,"Cumaná, Sucre, Venezuela",OK
    CUN,////,////,MMUN,UNK,0.75,Cancun,"Cancun, Mexico",OWNO,////,////,Cancún International Airport,"Cancún, Quintana Roo, México",OK
    CUP,////,////,SVCP,UNK,0.333,Carupano,"Carupano, Venezuela",OWNO,////,////,General José Francisco Bermúdez,"Carúpano, Sucre, Venezuela",OK
    CUQ,////,////,YCOE,UNK,1.0,Coen,"Coen, Australia",OWNO,////,////,????,"Coen, Queensland, Australia",OK
    CUR,////,////,TNCC,UNK,1.0,Aeropuerto Hato,"Curacao, Netherlands Antilles",OWNO,////,////,Aeropuerto Hato,"Willemstad, Curaçao, Netherlands Antilles",OK
    CUT,////,////,SAZW,UNK,1.0,Cutral,"Cutral, Argentina",OWNO,////,////,????,"Cutral, Neuquén, Argentina",OK
    CUU,////,////,MMCU,UNK,0.785,Gen Fierro Villalobos,"Chihuahua, Mexico",OWNO,////,////,General Roberto Fierro Villalobos International Airport,"Chihuahua, Chihuahua, México",OK
    CUZ,////,////,SPZO,UNK,0.571,Velazco Astete,"Cuzco, Peru",OWNO,////,////,Teniente FAP Alejandro Velasco Astete International Airport,"Cusco, Cuzco, Perú",OK
    CVF,////,////,LFLJ,UNK,0.69,Courchevel,"Courchevel, France",OWNO,////,////,Courchevel Altiport,"Courchevel, Rhône-Alpes, France",OK
    CVG,CVG,KCVG,KCVG,OK,0.64,Cincinnati International,"Cincinnati (OH), USA",OWNO,CINCINNATI/NORTHERN KENTUCKY INTL,"COVINGTON, KY - UNITED STATES",Cincinnati/Northern Kentucky International Airport,"Covington, Kentucky, United States",OK
    CVI,////,////,////,UNK,1.0,Caleta Olivia,"Caleta Olivia, Argentina",OWNO,////,////,????,"Caleta Olivia, Santa Cruz, Argentina",OK
    CVJ,////,////,MMCB,UNK,0.435,Cuernavaca,"Cuernavaca, Mexico",OWNO,////,////,General Mariano Matamoros,"Cuernavaca, Morelos, México",OK
    CVM,////,////,MMCV,UNK,0.484,Ciudad Victoria,"Ciudad Victoria, Mexico",OWNO,////,////,General Pedro J. Méndez National,"Ciudad Victoria, Tamaulipas, México",OK
    CVN,CVN,KCVN,KCVN,OK,1.0,Municipal,"Clovis (NM), USA",OWNO,CLOVIS MUNI,"CLOVIS, NM - UNITED STATES",Clovis Municipal Airport,"Clovis, New Mexico, United States",OK
    CVO,CVO,KCVO,KCVO,OK,0.815,Albany/Corvallis Airport,"Corvallis (OR), USA",OWNO,CORVALLIS MUNI,"CORVALLIS, OR - UNITED STATES",Corvallis Municipal Airport,"Corvallis, Oregon, United States",OK
    CVQ,////,////,YCAR,UNK,1.0,Carnarvon,"Carnarvon, Australia",OWNO,////,////,????,"Carnarvon, Western Australia, Australia",OK
    CVS,CVS,KCVS,KCVS,OK,1.0,Cannon AFB,"Clovis, USA (NM)",OWNO,CANNON AFB,"CLOVIS, NM - UNITED STATES",Cannon AFB,"Clovis, New Mexico, United States",OK
    CVT,////,////,EGBE,UNK,0.429,Baginton,"Coventry, United Kingdom",OWNO,////,////,West Midlands Airport,"Coventry, Warwickshire, England, United Kingdom",OK
    CVU,////,////,LPCR,UNK,1.0,Corvo Island,"Corvo Island, Portugal",OWNO,////,////,????,"Corvo Island, Região Autónoma dos Açores (Azores), Portugal",OK
    CWA,CWA,KCWA,KCWA,OK,1.0,Central Wisconsin,"Wausau (WI), USA",OWNO,CENTRAL WISCONSIN,"MOSINEE, WI - UNITED STATES",Central Wisconsin Airport,"Mosinee, Wisconsin, United States",OK
    CWB,////,////,SBCT,UNK,1.0,Afonso Pena International Airport,"Curitiba, Brazil",OWNO,////,////,Afonso Pena International Airport,"Curitiba, Paraná, Brazil",OK
    CWC,////,////,UKLN,UNK,0.941,Chernovtsy,"Chernovtsy, Ukraine",OWNO,////,////,????,"Chernivtsi, Chernivtsi, Ukraine",OK
    CWE,////,////,HECW,UNK,nan,////,////,////,////,////,Cairo West AB,"Cairo, Al Jizah (Giza), Egypt",UNK
    CWI,CWI,KCWI,KCWI,OK,0.766,Clinton,"Clinton IA, USA",OWNO,CLINTON MUNI,"CLINTON, IA - UNITED STATES",Clinton Municipal Airport,"Clinton, Iowa, United States",OK
    CWL,////,////,EGFF,UNK,0.842,Cardiff-Wales Airport,"Cardiff, United Kingdom",OWNO,////,////,Cardiff International Airport,"Cardiff, Glamorgan, Wales, United Kingdom",OK
    CWT,////,////,YCWR,UNK,1.0,Cowra,"Cowra, Australia",OWNO,////,////,????,"Cowra, New South Wales, Australia",OK
    CXA,////,////,SVCD,UNK,0.875,Caicara De Oro,"Caicara De Oro, Venezuela",OWNO,////,////,????,"Caicara de Orinoco, Guárico, Venezuela",MAYBE
    CXB,////,////,VGCB,UNK,1.0,Cox's Bazar,"Cox's Bazar, Bangladesh",OWNO,////,////,????,"Cox's Bazar, Bangladesh",OK
    CXF,CXF,PACX,PACX,OK,1.0,Coldfoot,"Coldfoot (AK), USA",OWNO,COLDFOOT,"COLDFOOT, AK - UNITED STATES",????,"Coldfoot, Alaska, United States",OK
    CXH,////,////,CYHC,UNK,0.756,Coal Harbour,"Vancouver, Canada",OWNO,////,////,Vancouver Harbour Water Airport,"Vancouver, British Columbia, Canada",OK
    CXI,////,////,PLCH,UNK,1.0,Cassidy International Airport,"Kiritimati (island), Kiribati",OWNO,////,////,Cassidy International Airport,"Christmas Island, Kiribati",OK
    CXJ,////,////,SBCX,UNK,1.0,Campo Dos Bugres,"Caxias Do Sul, Brazil",OWNO,////,////,Campo dos Bugres,"Caxias do Sul, Rio Grande do Sul, Brazil",OK
    CXL,CXL,KCXL,KCXL,OK,1.0,International,"Calexico (CA), USA",OWNO,CALEXICO INTL,"CALEXICO, CA - UNITED STATES",Calexico International Airport,"Calexico, California, United States",OK
    CXO,CXO,KCXO,KCXO,OK,0.36,Montgomery Co,"Conroe (TX), USA",OWNO,LONE STAR EXECUTIVE,"HOUSTON, TX - UNITED STATES",Lone Star Executive Airport,"Houston, Texas, United States",OK
    CXP,////,////,WIHL,UNK,1.0,Tunggul Wulung,"Cilacap, Indonesia",OWNO,////,////,Tunggul Wulung,"Cilacap, Jawa Tengah, Indonesia",OK
    CXR,////,////,VVCR,UNK,nan,////,////,////,////,////,Cam Ranh International Airport,"Nha Trang, Vietnam",UNK
    CYA,////,////,MTCA,UNK,1.0,Les Cayes,"Les Cayes, Haiti",OWNO,////,////,????,"Les Cayes, Haiti",OK
    CYB,////,////,MWCB,UNK,0.928,Gerrard-Smith,"Cayman Brac Island, Cayman Islands",OWNO,////,////,Gerrard-Smith International Airport,"Cayman Brac Island, Cayman Islands",OK
    CYC,////,////,////,UNK,1.0,Caye Chapel,"Caye Chapel, Belize",OWNO,////,////,Caye Chapel Airport,"Caye Chapel, Belize, Belize",OK
    CYF,CFK,PACK,PACK,OK,0.815,Chefornak SPB,"Chefornak (AK), USA",OWNO,CHEFORNAK,"CHEFORNAK, AK - UNITED STATES",????,"Chefornak, Alaska, United States",OK
    CYI,////,////,RCKU,UNK,1.0,Chiayi,"Chiayi, Taiwan",OWNO,////,////,????,"Chiayi, Taiwan",OK
    CYM,CYM,////,////,OK,0.774,Chatham SPB,"Chatham (AK), USA",OWNO,CHATHAM,"CHATHAM, AK - UNITED STATES",Chatham SPB,"Chatham, Alaska, United States",OK
    CYO,////,////,MUCL,UNK,0.692,Cayo Largo Del Sur,"Cayo Largo Del Sur, Cuba",OWNO,////,////,Vilo Acuña International Airport,"Cayo Largo del Sur, Isla de la Juventud, Cuba",OK
    CYP,////,////,RPVC,UNK,1.0,Calbayog,"Calbayog, Philippines",OWNO,////,////,????,"Calbayog, Philippines",OK
    CYR,////,////,SUCA,UNK,0.37,Colonia,"Colonia, Uruguay",OWNO,////,////,Laguna de Los Patos International Airport,"Colonia, Colonia, Uruguay",OK
    CYS,CYS,KCYS,KCYS,OK,1.0,Cheyenne Regional,"Cheyenne (WY), USA",OWNO,CHEYENNE RGNL/JERRY OLSON FIELD,"CHEYENNE, WY - UNITED STATES",Cheyenne Regional/Jerry Olson Field,"Cheyenne, Wyoming, United States",OK
    CYT,0AA1,////,PACY,NG,0.498,Intermediate,"Yakataga (AK), USA",OWNO,YAKATAGA,"YAKATAGA, AK - UNITED STATES",????,"Yakataga, Alaska, United States",OK
    CYU,////,////,RPLO,UNK,1.0,Cuyo,"Cuyo, Philippines",OWNO,////,////,????,"Cuyo, Palawan, Philippines",OK
    CYW,////,////,MMCY,UNK,nan,////,////,////,////,////,Captain Rogelio Castillo National,"Celaya, Guanajuato, México",UNK
    CYX,////,////,UESS,UNK,0.875,Cherskiy,"Cherskiy, Russia",OWNO,////,////,Cherskij Airport,"Cherskij, Sakha (Yakutiya), Russian Federation (Russia)",OK
    CYZ,////,////,RPUY,UNK,1.0,Cauayan,"Cauayan, Philippines",OWNO,////,////,????,"Cauayan, Luzon Island, Philippines",OK
    CZA,////,////,MMCT,UNK,1.0,Chichen Itza,"Chichen Itza, Mexico",OWNO,////,////,????,"Chichen Itza, Yucatán, México",OK
    CZE,////,////,SVCR,UNK,0.308,Coro,"Coro, Venezuela",OWNO,////,////,José Leonardo Chirinos,"Coro, Falcón, Venezuela",OK
    CZF,CZF,PACZ,PACZ,OK,0.852,Cape Romanzof,"Cape Romanzof (AK), USA",OWNO,CAPE ROMANZOF LRRS,"CAPE ROMANZOF, AK - UNITED STATES",Cape Romanzof LRRS Airport,"Cape Romanzof, Alaska, United States",OK
    CZH,////,////,////,UNK,1.0,Corozal,"Corozal, Belize",OWNO,////,////,Corozal Airport,"Corozal, Corozal, Belize",OK
    CZL,////,////,DABC,UNK,0.65,Ain El Bey,"Constantine, Algeria",OWNO,////,////,Mohamed Boudiaf International Airport,"Constantine, Constantine, Algeria",OK
    CZM,////,////,MMCZ,UNK,0.778,Cozumel,"Cozumel, Mexico",OWNO,////,////,Cozumel International Airport,"Cozumel, Quintana Roo, México",OK
    CZN,CZN,////,////,OK,0.679,Chisana Field,"Chisana, USA (AK)",OWNO,CHISANA,"CHISANA, AK - UNITED STATES",????,"Chisana, Alaska, United States",OK
    CZS,////,////,SBCZ,UNK,0.636,Campo Internacional,"Cruzeiro Do Sul, Brazil",OWNO,////,////,????,"Cruzeiro do Sul, Acre, Brazil",OK
    CZT,CZT,KCZT,KCZT,OK,0.581,Carrizo Springs,"Carrizo Springs, USA (TX)",OWNO,DIMMIT COUNTY,"CARRIZO SPRINGS, TX - UNITED STATES",Dimmit County Airport,"Carrizo Springs, Texas, United States",OK
    CZU,////,////,SKCZ,UNK,0.7,Corozal,"Corozal, Colombia",OWNO,////,////,Las Brujas Airport,"Corozal, Sucre, Colombia",OK
    CZW,////,////,EPCH,UNK,0.733,Czestochowa,"Czestochowa, Poland",OWNO,////,////,Rudniki,"Czestochowa, Slaskie, Poland",OK
    CZX,////,////,ZSCG,UNK,1.0,Changzhou,"Changzhou, PR China",OWNO,////,////,????,"Changzhou, Jiangsu, China",OK


List for checking certainty of Great Circle Mapper (MOR - MSM)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MOR,MOR,KMOR,KMOR,OK,1.0,Moore-Murrell,"Morristown (TN), USA",OWNO,MOORE-MURRELL,"MORRISTOWN, TN - UNITED STATES",Moore-Murrell Airport,"Morristown, Tennessee, United States",OK
    MOT,MOT,KMOT,KMOT,OK,1.0,International,"Minot (ND), USA",OWNO,MINOT INTL,"MINOT, ND - UNITED STATES",Minot International Airport,"Minot, North Dakota, United States",OK
    MOU,MOU,PAMO,PAMO,OK,1.0,Mountain Village,"Mountain Village (AK), USA",OWNO,MOUNTAIN VILLAGE,"MOUNTAIN VILLAGE, AK - UNITED STATES",????,"Mountain Village, Alaska, United States",OK
    MOV,////,////,YMRB,UNK,1.0,Moranbah,"Moranbah, Australia",OWNO,////,////,????,"Moranbah, Queensland, Australia",OK
    MOW,////,////,////,UNK,1.0,Metropolitan Area,"Moscow, Russia",OWNO,////,////,Metropolitan Area,"Moscow, Moskovskaya, Russian Federation (Russia)",OK
    MOX,MOX,KMOX,KMOX,OK,0.43,Municipal,"Morris (MN), USA",OWNO,MORRIS MUNI - CHARLIE SCHMIDT FLD,"MORRIS, MN - UNITED STATES",Morris Municipal - Charlie Schmidt Field,"Morris, Minnesota, United States",OK
    MOY,////,////,////,UNK,1.0,Monterrey,"Monterrey, Colombia",OWNO,////,////,????,"Monterrey, Casanare, Colombia",OK
    MOZ,////,////,NTTM,UNK,1.0,Temae,"Moorea, French Polynesia",OWNO,////,////,Temae,"Moorea, Society Islands, French Polynesia",OK
    MPA,////,////,////,UNK,1.0,Mpacha,"Mpacha, Namibia",OWNO,////,////,????,"Mpacha, Namibia",OK
    MPB,X44,////,////,OK,0.254,Miami Seaplane Base,"Miami (FL), USA",OWNO,MIAMI,"MIAMI, FL - UNITED STATES",Watson Island International Airport,"Miami, Florida, United States",OK
    MPC,////,////,WIPU,UNK,0.414,"Muko-Muko Airport, Southwest Sumatra","Muko-Muko, Indonesia",OWNO,////,////,????,"Muko-Muko, Bengkulu, Indonesia",OK
    MPD,////,////,OPMP,UNK,0.37,"Sindhri Airport, Mirpur Khas District","Mirpur Khas, Pakistan",OWNO,////,////,????,"Sindhri Tharparkar, Sindh, Pakistan",MAYBE
    MPF,////,////,////,UNK,1.0,Mapoda,"Mapoda, Papua New Guinea",OWNO,////,////,????,"Mapoda, Western, Papua-New Guinea",OK
    MPG,////,////,////,UNK,1.0,Makini,"Makini, Papua New Guinea",OWNO,////,////,????,"Makini, Morobe, Papua-New Guinea",OK
    MPH,////,////,RPVE,UNK,0.431,"Godofredo P. Ramos Airport, Malay, Western Visayas","Caticlan, Philippines",OWNO,////,////,Malay,"Caticlan, Philippines",OK
    MPJ,MPJ,KMPJ,KMPJ,OK,1.0,Petit Jean Park,"Morrilton (AR), USA",OWNO,PETIT JEAN PARK,"MORRILTON, AR - UNITED STATES",Petit Jean Park Airport,"Morrilton, Arkansas, United States",OK
    MPK,////,////,RKJM,UNK,1.0,Mokpo,"Mokpo, South Korea",OWNO,////,////,????,"Mokpo, Republic of Korea (South Korea)",OK
    MPL,////,////,LFMT,UNK,1.0,Montpellier-Méditerranée Airport (Fréjorgues Airport),"Montpellier, France",OWNO,////,////,Frejorgues,"Montpellier Méditerranée, Languedoc-Roussillon, France",MAYBE
    MPM,////,////,FQMA,UNK,0.706,Maputo International,"Maputo, Mozambique",OWNO,////,////,????,"Maputo, Mozambique",OK
    MPN,////,////,EGYP,UNK,0.815,Mount Pleasant,"Mount Pleasant, Falkland Islands",OWNO,////,////,Mount Pleasant International Airport,"Mount Pleasant, Falkland Islands",OK
    MPO,MPO,KMPO,KMPO,OK,0.626,Mt Pocono,"Mt Pocono (PA), USA",OWNO,POCONO MOUNTAINS MUNI,"MOUNT POCONO, PA - UNITED STATES",Pocono Mountains Municipal Airport,"Mount Pocono, Pennsylvania, United States",OK
    MPP,////,////,////,UNK,1.0,Mulatupo,"Mulatupo, Panama",OWNO,////,////,????,"Mulatupo, Kuna Yala (Guna Yala), Panamá",OK
    MPQ,////,////,OJMN,UNK,1.0,Maan,"Maan, Jordan",OWNO,////,////,????,"Ma'An, Jordan",OK
    MPR,MPR,KMPR,KMPR,OK,1.0,Mcpherson,"Mcpherson (KS), USA",OWNO,MC PHERSON,"MC PHERSON, KS - UNITED STATES",????,"Mc Pherson, Kansas, United States",OK
    MPT,////,////,WPMN,UNK,1.0,Maliana,"Maliana, Indonesia",OWNO,////,////,????,"Maliana, Timor-Leste (East Timor)",OK
    MPU,////,////,////,UNK,1.0,Mapua,"Mapua, Papua New Guinea",OWNO,////,////,????,"Mapua, New Ireland, Papua-New Guinea",OK
    MPV,MPV,KMPV,KMPV,OK,1.0,Edward F Knapp State,"Montpelier (VT), USA",OWNO,EDWARD F KNAPP STATE,"BARRE/MONTPELIER, VT - UNITED STATES",Edward F Knapp State Airport,"Barre/Montpelier, Vermont, United States",OK
    MPW,////,////,UKCM,UNK,0.762,Mariupol,"Mariupol, Ukraine",OWNO,////,////,Mariupol International Airport,"Mariupol, Donetsk, Ukraine",OK
    MPX,////,////,////,UNK,1.0,Miyanmin,"Miyanmin, Papua New Guinea",OWNO,////,////,????,"Miyanmin, Sandaun, Papua-New Guinea",OK
    MPY,////,////,SOOA,UNK,1.0,Maripasoula,"Maripasoula, French Guiana",OWNO,////,////,????,"Maripasoula, French Guiana",OK
    MPZ,MPZ,KMPZ,KMPZ,OK,1.0,Municipal,"Mt Pleasant IA, USA",OWNO,MOUNT PLEASANT MUNI,"MOUNT PLEASANT, IA - UNITED STATES",Mount Pleasant Municipal Airport,"Mount Pleasant, Iowa, United States",OK
    MQA,////,////,YMDI,UNK,0.636,Mandora,"Mandora, Australia",OWNO,////,////,????,"Mandora Station, Western Australia, Australia",MAYBE
    MQB,MQB,KMQB,KMQB,OK,1.0,Municipal,"Macomb (IL), USA",OWNO,MACOMB MUNI,"MACOMB, IL - UNITED STATES",Macomb Municipal Airport,"Macomb, Illinois, United States",OK
    MQC,////,////,LFVM,UNK,0.762,Miquelon Aprt,"Miquelon, St. Pierre and Miquelon",OWNO,////,////,????,"Miquelon, St. Pierre and Miquelon",OK
    MQD,////,////,SAVQ,UNK,1.0,Maquinchao,"Maquinchao, Argentina",OWNO,////,////,????,"Maquinchao, Río Negro, Argentina",OK
    MQE,////,////,YMQA,UNK,1.0,Marqua,"Marqua, Australia",OWNO,////,////,????,"Marqua, Northern Territory, Australia",OK
    MQF,////,////,USCM,UNK,0.828,Magnitogorsk,"Magnitogorsk, Russia",OWNO,////,////,Magnitogorsk International Airport,"Magnitogorsk, Chelyabinskaya, Russian Federation (Russia)",OK
    MQH,////,////,SBMC,UNK,0.75,Municipal,"Minacu, Brazil",OWNO,////,////,????,"Minaçu, Goiás, Brazil",OK
    MQJ,////,////,UEMA,UNK,1.0,Moma,Khonu,IATA,////,////,Moma Airport,"Honuu, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    MQK,////,////,SLTI,UNK,1.0,San Matias,"San Matias, Bolivia",OWNO,////,////,San Matías Airport,"San Matías, Ángel Sandoval, Santa Cruz, Bolivia",OK
    MQL,////,////,YMIA,UNK,1.0,Mildura,"Mildura, Australia",OWNO,////,////,????,"Mildura, Victoria, Australia",OK
    MQM,////,////,LTCR,UNK,1.0,Mardin,"Mardin, Turkey",OWNO,////,////,????,"Mardin, Mardin, Turkey",OK
    MQN,////,////,ENRA,UNK,0.667,Mo I Rana,"Mo I Rana, Norway",OWNO,////,////,Røssvoll,"Mo i Rana, Nordland, Norway",OK
    MQP,////,////,FAKN,UNK,1.0,Kruger Mpumalanga International,"Nelspruit, South Africa",OWNO,////,////,Kruger Mpumalanga International Airport,"Nelspruit, Mpumalanga, South Africa",OK
    MQQ,////,////,FTTD,UNK,1.0,Moundou,"Moundou, Chad",OWNO,////,////,????,"Moundou, Logone-Occidental, Chad",OK
    MQR,////,////,////,UNK,1.0,Mosquera,"Mosquera, Colombia",OWNO,////,////,????,"Mosquera, Nariño, Colombia",OK
    MQS,////,////,TVSM,UNK,1.0,Mustique,"Mustique Island, Saint Vincent and the Grenadines",OWNO,////,////,????,"Mustique Island, Saint Vincent and The Grenadines",OK
    MQT,SAW,KSAW,KSAW,OK,1.0,Sawyer International,"Marquette (MI), USA",OWNO,SAWYER INTL,"MARQUETTE, MI - UNITED STATES",Sawyer International Airport,"Marquette, Michigan, United States",OK
    MQU,////,////,SKQU,UNK,1.0,Mariquita,"Mariquita, Colombia",OWNO,////,////,Mariquita Airport,"Mariquita, Tolima, Colombia",OK
    MQV,////,////,////,UNK,1.0,Mostaganem,Mostaganem,IATA,////,////,????,"Mostaganem, Mostaganem, Algeria",MAYBE
    MQW,MQW,KMQW,KMQW,OK,1.0,Telfair-Wheeler,"Mc Rae (GA), USA",OWNO,TELFAIR-WHEELER,"MC RAE, GA - UNITED STATES",Telfair-Wheeler Airport,"Mc Rae, Georgia, United States",OK
    MQX,////,////,HAMK,UNK,0.444,Makale,"Makale, Ethiopia",OWNO,////,////,Alula Aba Nega Airport,"Makale, Tigray, Ethiopia",OK
    MQY,MQY,KMQY,KMQY,OK,1.0,Smyrna,"Smyrna (TN), USA",OWNO,SMYRNA,"SMYRNA, TN - UNITED STATES",????,"Smyrna, Tennessee, United States",OK
    MQZ,////,////,YMGT,UNK,1.0,Margaret River,"Margaret River, Australia",OWNO,////,////,Margaret River Airport,"Margaret River, Western Australia, Australia",OK
    MRA,////,////,HLMS,UNK,0.7,Misurata,"Misurata, Libya",OWNO,////,////,Misrata International Airport,"Misrata, Libyan Arab Jamahiriya (Libya)",OK
    MRB,MRB,KMRB,KMRB,OK,0.591,Martinsburg,"Martinsburg (WV), USA",OWNO,EASTERN WV RGNL/SHEPHERD FLD,"MARTINSBURG, WV - UNITED STATES",Eastern WV Regional/Shepherd Field,"Martinsburg, West Virginia, United States",OK
    MRC,MRC,KMRC,KMRC,OK,1.0,Maury County,"Columbia (TN), USA",OWNO,MAURY COUNTY,"COLUMBIA/MOUNT PLEASANT, TN - UNITED STATES",Maury County Airport,"Columbia/Mount Pleasant, Tennessee, United States",OK
    MRD,////,////,SVMD,UNK,0.864,A Carnevalli,"Merida, Venezuela",OWNO,////,////,Alberto Carnevalli,"Mérida, Mérida, Venezuela",OK
    MRE,////,////,////,UNK,0.588,Mara Lodges,"Mara Lodges, Kenya",OWNO,////,////,Mara Serena,"Masai Mara Game Reserve, Kenya",MAYBE
    MRF,MRF,KMRF,KMRF,OK,1.0,Municipal,"Marfa (TX), USA",OWNO,MARFA MUNI,"MARFA, TX - UNITED STATES",Marfa Municipal Airport,"Marfa, Texas, United States",OK
    MRG,////,////,YMBA,UNK,1.0,Mareeba,"Mareeba, Australia",OWNO,////,////,????,"Mareeba, Queensland, Australia",OK
    MRH,////,////,////,UNK,1.0,May River,"May River, Papua New Guinea",OWNO,////,////,May River Airport,"May River, East Sepik, Papua-New Guinea",OK
    MRI,MRI,PAMR,PAMR,OK,1.0,Merrill Field,"Anchorage (AK), USA",OWNO,MERRILL FIELD,"ANCHORAGE, AK - UNITED STATES",Merrill Field,"Anchorage, Alaska, United States",OK
    MRJ,////,////,MHMA,UNK,1.0,Marcala,"Marcala, Honduras",OWNO,////,////,????,"Marcala, La Paz, Honduras",OK
    MRK,MKY,KMKY,KMKY,OK,1.0,Marco Island,"Marco Island (FL), USA",OWNO,MARCO ISLAND,"MARCO ISLAND, FL - UNITED STATES",????,"Marco Island, Florida, United States",OK
    MRL,////,////,////,UNK,1.0,Miners Lake,"Miners Lake, Australia",OWNO,////,////,????,"Miners Lake, Queensland, Australia",OK
    MRM,////,////,////,UNK,1.0,Manare,"Manare, Papua New Guinea",OWNO,////,////,????,"Manare, Central, Papua-New Guinea",OK
    MRN,MRN,KMRN,KMRN,OK,0.623,Lenoir,"Morganton (NC), USA",OWNO,FOOTHILLS REGIONAL,"MORGANTON, NC - UNITED STATES",Foothills Regional,"Morganton, North Carolina, United States",OK
    MRO,////,////,NZMS,UNK,1.0,Masterton,"Masterton, New Zealand",OWNO,////,////,????,"Masterton, New Zealand",OK
    MRP,////,////,YALA,UNK,1.0,Marla,"Marla, Australia",OWNO,////,////,????,"Marla, South Australia, Australia",OK
    MRQ,////,////,RPUW,UNK,0.769,Marinduque,"Marinduque, Philippines",OWNO,////,////,Gasan,"Marinduque, Marinduque Island, Philippines",OK
    MRR,////,////,SEMA,UNK,1.0,José María Velasco Ibarra Airport,"Macará, Ecuador",OWNO,////,////,José María Velasco Ibarra Airport,"Macara, Loja, Ecuador",OK
    MRS,////,////,LFML,UNK,1.0,Marseille Provence Airport,"Marseille, France",OWNO,////,////,????,"Marseille Provence, Provence-Alpes-Côte d'Azur, France",OK
    MRT,////,////,////,UNK,1.0,Moroak,"Moroak, Australia",OWNO,////,////,????,"Moroak, Northern Territory, Australia",OK
    MRU,////,////,FIMP,UNK,0.947,Seewoosagur Ramgoolam International,"Mauritius, Mauritius",OWNO,////,////,Sir Seewoosagur Ramgoolam International Airport,"Mauritius, Mauritius",OK
    MRV,////,////,URMM,UNK,1.0,Mineralnye Vody,"Mineralnye Vody, Russia",OWNO,////,////,Mineralnyye Vody Airport,"Mineralnyye Vody, Stavropol'skiy, Russian Federation (Russia)",OK
    MRW,////,////,EKMB,UNK,1.0,Maribo,"Maribo, Denmark",OWNO,////,////,Maribo,"Lolland Falster, Denmark",OK
    MRX,////,////,OIAM,UNK,1.0,Mahshahr,"Bandar Mahshahr, Iran",OWNO,////,////,Mahshahr,"Bandar Mahshahr, Khuzestan, Iran",OK
    MRY,MRY,KMRY,KMRY,OK,1.0,Monterey Regional Airport (Monterey Peninsula Airport),"Monterey/Carmel (CA), USA",OWNO,MONTEREY RGNL,"MONTEREY, CA - UNITED STATES",Monterey Regional,"Monterey, California, United States",OK
    MRZ,////,////,YMOR,UNK,1.0,Moree,"Moree, Australia",OWNO,////,////,????,"Moree, New South Wales, Australia",OK
    MSA,////,////,CZMD,UNK,1.0,Muskrat Dam,"Muskrat Dam, Canada",OWNO,////,////,????,"Muskrat Dam, Ontario, Canada",OK
    MSC,FFZ,KFFZ,KFFZ,OK,1.0,Falcon Field,"Mesa (AZ), USA",OWNO,FALCON FLD,"MESA, AZ - UNITED STATES",Falcon Field,"Mesa, Arizona, United States",OK
    MSD,43U,////,////,UNK,1.0,Mt Pleasant,"Mt Pleasant (UT), USA",OWNO,////,////,????,"Mount Pleasant, Utah, United States",OK
    MSE,////,////,EGMH,UNK,0.846,Kent International,"Manston, United Kingdom",OWNO,////,////,Manston,"London, Kent, England, United Kingdom",OK
    MSF,////,////,YMNS,UNK,1.0,Mount Swan,"Mount Swan, Australia",OWNO,////,////,????,"Mount Swan, Northern Territory, Australia",OK
    MSG,////,////,FXMA,UNK,1.0,Matsaile,"Matsaile, Lesotho",OWNO,////,////,????,"Matsaile, Lesotho",OK
    MSH,////,////,OOMA,UNK,0.667,Masirah,"Masirah, Oman",OWNO,////,////,????,"Masirah Island, Oman",MAYBE
    MSJ,////,////,RJSM,UNK,1.0,Misawa,"Misawa, Japan",OWNO,////,////,Misawa Airport / Misawa AB,"Misawa, Aomori, Japan",OK
    MSL,MSL,KMSL,KMSL,OK,0.445,Florence/Muscle Sh/Sheffield Airport,"Sheffield (AL), USA",OWNO,NORTHWEST ALABAMA RGNL,"MUSCLE SHOALS, AL - UNITED STATES",Northwest Alabama Regional,"Muscle Shoals, Alabama, United States",OK
    MSM,////,////,FZCV,UNK,1.0,Masi Manimba,"Masi Manimba, Congo (DRC)",OWNO,////,////,????,"Masi Manimba, Bandundu, Democratic Republic of Congo (Zaire)",OK

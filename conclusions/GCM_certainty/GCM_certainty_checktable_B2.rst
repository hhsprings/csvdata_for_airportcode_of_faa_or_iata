
List for checking certainty of Great Circle Mapper (BEM - BJB)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    BEM,////,////,GMMD,UNK,0.609,National,Beni Mellal,IATA,////,////,Beni Mellal National Airport,"Beni Mellal, Morocco",MAYBE
    BEN,////,////,HLLB,UNK,1.0,Benina International,"Benghazi, Libya",OWNO,////,////,Benina International Airport,"Benghazi, Libyan Arab Jamahiriya (Libya)",OK
    BEO,////,////,YPEC,UNK,0.684,Belmont,"Newcastle, Australia",OWNO,////,////,Aeropelican,"Newcastle, New South Wales, Australia",OK
    BEP,////,////,VOBI,UNK,1.0,Bellary,"Bellary, India",OWNO,////,////,????,"Bellary, Karnataka, India",OK
    BEQ,////,////,EGXH,UNK,1.0,Honington,"Bury St Edmunds, United Kingdom",OWNO,////,////,Honington,"Bury St. Edmunds, Suffolk, England, United Kingdom",OK
    BER,////,////,////,UNK,1.0,Metropolitan Area,"Berlin, Germany",OWNO,////,////,Metropolitan Area,"Berlin, Berlin, Germany",OK
    BES,////,////,LFRB,UNK,1.0,Guipavas,"Brest, France",OWNO,////,////,Guipavas,"Brest, Bretagne, France",OK
    BET,BET,PABE,PABE,OK,1.0,Bethel Airport,"Bethel (AK), USA",OWNO,BETHEL,"BETHEL, AK - UNITED STATES",????,"Bethel, Alaska, United States",OK
    BEU,////,////,YBIE,UNK,1.0,Bedourie,"Bedourie, Australia",OWNO,////,////,????,"Bedourie, Queensland, Australia",OK
    BEV,////,////,LLBS,UNK,0.741,Beer Sheba,"Beer Sheba, Israel",OWNO,////,////,Teyman,"Beer Sheba, Israel",OK
    BEW,////,////,FQBR,UNK,1.0,Beira,"Beira, Mozambique",OWNO,////,////,????,"Beira, Mozambique",OK
    BEX,////,////,EGUB,UNK,0.571,RAF Station,"Benson, United Kingdom",OWNO,////,////,????,"Benson, Oxfordshire, England, United Kingdom",OK
    BEY,////,////,OLBA,UNK,0.667,International,"Beirut, Lebanon",OWNO,////,////,Rafic Hariri International Airport,"Beirut, Lebanon",OK
    BEZ,////,////,NGBR,UNK,1.0,Beru,"Beru, Kiribati",OWNO,////,////,????,"Beru, Kiribati",OK
    BFC,////,////,YBMD,UNK,1.0,Bloomfield,"Bloomfield, Australia",OWNO,////,////,????,"Bloomfield, Queensland, Australia",OK
    BFD,BFD,KBFD,KBFD,OK,0.799,Bradford,"Bradford (PA), USA",OWNO,BRADFORD RGNL,"BRADFORD, PA - UNITED STATES",Bradford Regional,"Bradford, Pennsylvania, United States",OK
    BFE,////,////,EDLI,UNK,1.0,Bielefeld,"Bielefeld, Germany",OWNO,////,////,????,"Bielefeld, North Rhine-Westphalia, Germany",OK
    BFF,BFF,KBFF,KBFF,OK,0.43,Scotts Bluff County,"Scottsbluff (NE), USA",OWNO,WESTERN NEBRASKA RGNL/WILLIAM B HEILIG FIELD,"SCOTTSBLUFF, NE - UNITED STATES",Western Nebraska Regional/William B Heilig Field,"Scottsbluff, Nebraska, United States",OK
    BFG,U07,////,////,OK,1.0,Bullfrog Basin,"Bullfrog Basin (UT), USA",OWNO,BULLFROG BASIN,"GLEN CANYON NATL REC AREA, UT - UNITED STATES",Bullfrog Basin Airport,"Glen Canyon National Recreation Area, Utah, United States",OK
    BFI,BFI,KBFI,KBFI,OK,0.859,Boeing Fld International,"Seattle (WA), USA",OWNO,BOEING FIELD/KING COUNTY INTL,"SEATTLE, WA - UNITED STATES",Boeing Field/King County International Airport,"Seattle, Washington, United States",OK
    BFJ,////,////,ZUBJ,UNK,0.526,Bijie,Bijie,IATA,////,////,Bijie Feixiong Airport,"Bijie, Guizhou, China",MAYBE
    BFL,BFL,KBFL,KBFL,OK,1.0,Meadows Field,"Bakersfield (CA), USA",OWNO,MEADOWS FIELD,"BAKERSFIELD, CA - UNITED STATES",Meadows Field,"Bakersfield, California, United States",OK
    BFM,BFM,KBFM,KBFM,OK,0.479,Mob Aerospace,"Mobile (AL), USA",OWNO,MOBILE DOWNTOWN,"MOBILE, AL - UNITED STATES",Mobile Downtown Airport,"Mobile, Alabama, United States",OK
    BFN,////,////,FABL,UNK,0.757,Bloemfontein International,"Bloemfontein, South Africa",OWNO,////,////,Bram Fischer International Airport,"Bloemfontein, Free State, South Africa",OK
    BFO,////,////,FVCZ,UNK,1.0,Buffalo Range,"Buffalo Range, Zimbabwe",OWNO,////,////,Buffalo Range,"Chiredzi, Zimbabwe",OK
    BFP,BVI,KBVI,KBVI,OK,0.771,Beaver Falls,"Beaver Falls (PA), USA",OWNO,BEAVER COUNTY,"BEAVER FALLS, PA - UNITED STATES",Beaver County Airport,"Beaver Falls, Pennsylvania, United States",OK
    BFQ,////,////,MPPI,UNK,1.0,Bahia Pinas,"Bahia Pinas, Panama",OWNO,////,////,Bahía Piña Airport,"Puerto Piña, Darién, Panamá",MAYBE
    BFR,BFR,KBFR,KBFR,OK,1.0,Virgil I Grissom Muni,"Bedford (IN), USA",OWNO,VIRGIL I GRISSOM MUNI,"BEDFORD, IN - UNITED STATES",Virgil I Grissom Municipal Airport,"Bedford, Indiana, United States",OK
    BFS,////,////,EGAA,UNK,0.688,Belfast International Airport,"Belfast, United Kingdom",OWNO,////,////,Aldergrove International Airport,"Belfast, County Antrim, Northern Ireland, United Kingdom",OK
    BFT,ARW,KARW,KARW,OK,1.0,County,"Beaufort (SC), USA",OWNO,BEAUFORT COUNTY,"BEAUFORT, SC - UNITED STATES",Beaufort County Airport,"Beaufort, South Carolina, United States",OK
    BFU,////,////,ZSBB,UNK,1.0,Bengbu,"Bengbu, PR China",OWNO,////,////,????,"Bengbu, Anhui, China",OK
    BFV,////,////,VTUO,UNK,1.0,Buri Ram,"Buri Ram, Thailand",OWNO,////,////,????,"Buri Ram, Buri Ram, Thailand",OK
    BFW,////,////,DAOS,UNK,nan,////,////,////,////,////,Sidi Bel Abbès Airport,"Sidi Bel Abbès, Sidi Bel Abbès, Algeria",UNK
    BFX,////,////,FKKU,UNK,1.0,Bafoussam,"Bafoussam, Cameroon",OWNO,////,////,Bafoussem Airport,"Bafoussem, West (Ouest), Cameroon",OK
    BGA,////,////,SKBG,UNK,0.732,Palo Negro,"Bucaramanga, Colombia",OWNO,////,////,Palonegro International Airport,"Bucaramanga, Santander, Colombia",OK
    BGB,////,////,FOGB,UNK,1.0,Booue,"Booue, Gabon",OWNO,////,////,Booue Airport,"Booue, Ogooué-Ivindo, Gabon",OK
    BGC,////,////,LPBG,UNK,1.0,Braganca,"Braganca, Portugal",OWNO,////,////,????,"Bragança, Bragança, Portugal",OK
    BGD,BGD,KBGD,KBGD,OK,0.36,Borger,"Borger (TX), USA",OWNO,HUTCHINSON COUNTY,"BORGER, TX - UNITED STATES",Hutchinson County Airport,"Borger, Texas, United States",OK
    BGE,BGE,KBGE,KBGE,OK,0.588,Decatur County,"Bainbridge (GA), USA",OWNO,DECATUR COUNTY INDUSTRIAL AIR PARK,"BAINBRIDGE, GA - UNITED STATES",Decatur County Industrial Air Park,"Bainbridge, Georgia, United States",OK
    BGF,////,////,FEFF,UNK,0.632,Bangui,"Bangui, Central African Republic",OWNO,////,////,M'Poko,"Bangui, Ombella-Mpoko (Ömbëlä-Pökö), Central African Republic",OK
    BGG,////,////,LTCU,UNK,1.0,Bingol,Bingol,IATA,////,////,Bingöl Airport,"Bingöl, Turkey",MAYBE
    BGH,////,////,GQNE,UNK,1.0,Abbaye,"Boghe, Mauritania",OWNO,////,////,Abbaye,"Bogué, Mauritania",OK
    BGI,////,////,TBPB,UNK,1.0,Grantley Adams International,"Bridgetown, Barbados",OWNO,////,////,Grantley Adams International Airport,"Bridgetown, Barbados",OK
    BGJ,////,////,BIBF,UNK,1.0,Borgarfjordur Eystri,"Borgarfjordur Eystri, Iceland",OWNO,////,////,????,"Borgarfjörður eystri, Iceland",OK
    BGK,////,////,////,UNK,1.0,Big Creek,"Big Creek, Belize",OWNO,////,////,Big Creek Airport,"Big Creek, Stann Creek, Belize",OK
    BGL,////,////,VNBL,UNK,1.0,Baglung,"Baglung, Nepal",OWNO,////,////,????,"Baglung, Nepal",OK
    BGM,BGM,KBGM,KBGM,OK,1.0,Greater Binghamton Airport,"Johnson City (NY), USA",OWNO,GREATER BINGHAMTON/EDWIN A LINK FIELD,"BINGHAMTON, NY - UNITED STATES",Greater Binghamton/Edwin A Link Field,"Binghamton, New York, United States",OK
    BGN,////,////,UESG,UNK,1.0,Belaya Gora,Belaya Gora,IATA,////,////,Belaya Gora Airport,"Belaya Gora, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    BGO,////,////,ENBR,UNK,0.564,Metropolitan Area,"Bergen, Norway",OWNO,////,////,Flesland,"Bergen, Norway",OK
    BGP,////,////,////,UNK,1.0,Bongo,"Bongo, Gabon",OWNO,////,////,????,"Bongo, Ogooué-Maritime, Gabon",OK
    BGQ,BGQ,PAGQ,PAGQ,OK,1.0,Big Lake,"Big Lake (AK), USA",OWNO,BIG LAKE,"BIG LAKE, AK - UNITED STATES",????,"Big Lake, Alaska, United States",OK
    BGR,BGR,KBGR,KBGR,OK,1.0,International,"Bangor (ME), USA",OWNO,BANGOR INTL,"BANGOR, ME - UNITED STATES",Bangor International Airport,"Bangor, Maine, United States",OK
    BGS,////,////,////,UNK,1.0,Webb AFB,"Big Spring (TX), USA",OWNO,////,////,Webb AFB,"Big Spring, Texas, United States",OK
    BGT,E51,////,////,OK,1.0,Bagdad,"Bagdad (AZ), USA",OWNO,BAGDAD,"BAGDAD, AZ - UNITED STATES",????,"Bagdad, Arizona, United States",OK
    BGU,////,////,FEFG,UNK,1.0,Bangassou,"Bangassou, Central African Republic",OWNO,////,////,????,"Bangassou, Mbomou (Mbömü), Central African Republic",OK
    BGV,////,////,SSBG,UNK,0.706,Bento Goncalves,"Bento Goncalves, Brazil",OWNO,////,////,Aeroclube de Bento Gonçalves,"Bento Gonçalves, Rio Grande do Sul, Brazil",OK
    BGW,////,////,ORBI,UNK,1.0,Baghdad International Airport,"Baghdad, Iraq",OWNO,////,////,Baghdad International Airport,"Baghdad, Iraq",OK
    BGX,////,////,SBBG,UNK,0.229,Bage,"Bage, Brazil",OWNO,////,////,Comandante Gustavo Kraemer,"Bagé, Rio Grande do Sul, Brazil",OK
    BGY,////,////,LIME,UNK,1.0,Orio Al Serio,"Milan, Italy",OWNO,////,////,Orio al Serio,"Bergamo, Lombardy, Italy",OK
    BGZ,////,////,LPBR,UNK,1.0,Braga,"Braga, Portugal",OWNO,////,////,????,"Braga, Braga, Portugal",OK
    BHA,////,////,SESV,UNK,0.739,Bahia De Caraquez,"Bahia De Caraquez, Ecuador",OWNO,////,////,Los Perales,"Bahía de Caráquez, Manabí, Ecuador",OK
    BHB,BHB,KBHB,KBHB,OK,0.504,Bar Harbour,"Bar Harbour (ME), USA",OWNO,HANCOCK COUNTY-BAR HARBOR,"BAR HARBOR, ME - UNITED STATES",Hancock County-Bar Harbor Airport,"Bar Harbor, Maine, United States",OK
    BHD,////,////,EGAC,UNK,1.0,George Best Belfast City Airport,"Belfast, United Kingdom",OWNO,////,////,George Best Belfast City,"Belfast, County Down, Northern Ireland, United Kingdom",OK
    BHE,////,////,NZWB,UNK,0.444,Blenheim,"Blenheim, New Zealand",OWNO,////,////,Woodbourne Air Station,"Blenheim, New Zealand",OK
    BHF,////,////,SKCP,UNK,0.833,Bahia Cupica,"Bahia Cupica, Colombia",OWNO,////,////,Cupica,"Bahía Solano, Chocó, Colombia",MAYBE
    BHG,////,////,MHBL,UNK,1.0,Brus Laguna,"Brus Laguna, Honduras",OWNO,////,////,????,"Brus Laguna, Gracias a Dios, Honduras",OK
    BHH,////,////,OEBH,UNK,1.0,Bisha,"Bisha, Saudi Arabia",OWNO,////,////,Bisha Airport,"Bisha, Saudi Arabia",OK
    BHI,////,////,SAZB,UNK,0.872,Comandante,"Bahia Blanca, Argentina",OWNO,////,////,Comandante Espora,"Bahía Blanca, Argentina",OK
    BHJ,////,////,VABJ,UNK,0.667,Rudra Mata,"Bhuj, India",OWNO,////,////,????,"Bhuj, Gujarat, India",OK
    BHK,////,////,UTSB,UNK,0.737,Bukhara,"Bukhara, Uzbekistan",OWNO,////,////,Bukhara International Airport,"Bukhara, Buxoro, Uzbekistan",OK
    BHL,////,////,////,UNK,0.8,Bahia Angeles,"Bahia Angeles, Mexico",OWNO,////,////,????,"Bahia de los Angeles, Baja California, México",MAYBE
    BHM,BHM,KBHM,KBHM,OK,0.464,Birmingham,"Birmingham (AL), USA",OWNO,BIRMINGHAM-SHUTTLESWORTH INTL,"BIRMINGHAM, AL - UNITED STATES",Birmingham-Shuttlesworth International Airport,"Birmingham, Alabama, United States",OK
    BHN,////,////,OYBN,UNK,1.0,Beihan,"Beihan, Yemen",OWNO,////,////,????,"Beihan, Yemen",OK
    BHO,////,////,VABP,UNK,1.0,Bhopal,"Bhopal, India",OWNO,////,////,????,"Bhopal, Madhya Pradesh, India",OK
    BHP,////,////,VNBJ,UNK,1.0,Bhojpur,"Bhojpur, Nepal",OWNO,////,////,????,"Bhojpur, Nepal",OK
    BHQ,////,////,YBHI,UNK,1.0,Broken Hill,"Broken Hill, Australia",OWNO,////,////,????,"Broken Hill, New South Wales, Australia",OK
    BHR,////,////,VNBP,UNK,1.0,Bharatpur,"Bharatpur, Nepal",OWNO,////,////,Bharatpur,"Bharatpur, Nepal",OK
    BHS,////,////,YBTH,UNK,0.7,Raglan,"Bathurst, Australia",OWNO,////,////,????,"Bathurst, New South Wales, Australia",OK
    BHT,////,////,YBTD,UNK,1.0,Brighton Downs,"Brighton Downs, Australia",OWNO,////,////,????,"Brighton Downs, Queensland, Australia",OK
    BHU,////,////,VABV,UNK,1.0,Bhavnagar,"Bhavnagar, India",OWNO,////,////,????,"Bhavnagar, Gujarat, India",OK
    BHV,////,////,OPBW,UNK,1.0,Bahawalpur,"Bahawalpur, Pakistan",OWNO,////,////,????,"Bahawalpur, Punjab, Pakistan",OK
    BHX,////,////,EGBB,UNK,0.857,International,"Birmingham, United Kingdom",OWNO,////,////,Birmingham Airport,"Birmingham, Warwickshire, England, United Kingdom",OK
    BHY,////,////,ZGBH,UNK,1.0,Beihai,"Beihai, PR China",OWNO,////,////,????,"Beihai, Guangxi, China",OK
    BHZ,////,////,////,UNK,1.0,Metropolitan Area,"Belo Horizonte, Brazil",OWNO,////,////,Metropolitan Area,"Belo Horizonte, Minas Gerais, Brazil",OK
    BIA,////,////,LFKB,UNK,1.0,Poretta,"Bastia, France",OWNO,////,////,Poretta,"Bastia, Corse (Corsica), France",OK
    BIB,////,////,HCMB,UNK,1.0,Baidoa,"Baidoa, Somalia",OWNO,////,////,Baidoa Airport,"Baidoa, Bay, Somalia",OK
    BID,BID,KBID,KBID,OK,0.734,Block Island,"Block Island (RI), USA",OWNO,BLOCK ISLAND STATE,"BLOCK ISLAND, RI - UNITED STATES",Block Island State Airport,"Block Island, Rhode Island, United States",OK
    BIE,BIE,KBIE,KBIE,OK,0.766,Beatrice,"Beatrice (NE), USA",OWNO,BEATRICE MUNI,"BEATRICE, NE - UNITED STATES",Beatrice Municipal Airport,"Beatrice, Nebraska, United States",OK
    BIF,BIF,KBIF,KBIF,OK,1.0,Biggs AAF,"El Paso (TX), USA",OWNO,BIGGS AAF (FORT BLISS),"FORT BLISS/EL PASO/, TX - UNITED STATES",Biggs AAF Airport,"Fort Bliss, Texas, United States",OK
    BIH,BIH,KBIH,KBIH,OK,1.0,Bishop,"Bishop (CA), USA",OWNO,BISHOP,"BISHOP, CA - UNITED STATES",????,"Bishop, California, United States",OK
    BII,////,////,////,UNK,1.0,Enyu Airfield,"Bikini Atoll, Marshall Islands",OWNO,////,////,Enyu Airfield,"Bikini Atoll, Marshall Islands",OK
    BIJ,////,////,////,UNK,1.0,Biliau,"Biliau, Papua New Guinea",OWNO,////,////,????,"Biliau, Madang, Papua-New Guinea",OK
    BIK,////,////,WABB,UNK,0.545,Mokmer,"Biak, Indonesia",OWNO,////,////,Frans Kaisiepo,"Biak, Papua, Indonesia",OK
    BIL,BIL,KBIL,KBIL,OK,0.734,Billings,"Billings (MT), USA",OWNO,BILLINGS LOGAN INTL,"BILLINGS, MT - UNITED STATES",Billings Logan International Airport,"Billings, Montana, United States",OK
    BIM,////,MYBS,MYBS,OK,0.543,International,"Bimini, Bahamas",OWNO,SOUTH BIMINI,"NORTH BIMINI, - BAHAMAS",Bimini International Airport,"Bimini, Bimini Island, Bahamas",MAYBE
    BIN,////,////,OABN,UNK,0.923,Bamiyan,"Bamiyan, Afghanistan",OWNO,////,////,????,"Bamyan, Bamyan, Afghanistan",OK
    BIO,////,////,LEBB,UNK,0.6,Bilbao,"Bilbao, Spain",OWNO,////,////,Sondica,"Bilbao, Basque Country, Spain",OK
    BIP,////,////,YBWM,UNK,1.0,Bulimba,"Bulimba, Australia",OWNO,////,////,????,"Bulimba, Queensland, Australia",OK
    BIQ,////,////,LFBZ,UNK,0.5,Biarritz Parme,"Biarritz, France",OWNO,////,////,Bayonne-Anglet,"Biarritz, Aquitaine, France",OK
    BIR,////,////,VNVT,UNK,1.0,Biratnagar,"Biratnagar, Nepal",OWNO,////,////,????,"Biratnagar, Nepal",OK
    BIS,BIS,KBIS,KBIS,OK,0.81,Bismarck,"Bismarck (ND), USA",OWNO,BISMARCK MUNI,"BISMARCK, ND - UNITED STATES",Bismarck Municipal Airport,"Bismarck, North Dakota, United States",OK
    BIT,////,////,VNBT,UNK,1.0,Baitadi,"Baitadi, Nepal",OWNO,////,////,????,"Baitadi, Nepal",OK
    BIU,////,////,BIBD,UNK,1.0,Bildudalur,"Bildudalur, Iceland",OWNO,////,////,????,"Bíldudalur, Iceland",OK
    BIV,////,////,FEFR,UNK,1.0,Bria,"Bria, Central African Republic",OWNO,////,////,????,"Bria, Haute-Kotto (Tö-Kötö), Central African Republic",OK
    BIW,////,////,YBIL,UNK,1.0,Billiluna,"Billiluna, Australia",OWNO,////,////,????,"Billiluna, Western Australia, Australia",OK
    BIX,BIX,KBIX,KBIX,OK,1.0,Keesler AFB,"Biloxi (MS), USA",OWNO,KEESLER AFB,"BILOXI, MS - UNITED STATES",Keesler AFB,"Biloxi, Mississippi, United States",OK
    BIY,////,////,FABE,UNK,1.0,Bisho,"Bisho, South Africa",OWNO,////,////,????,"Bhisho, Eastern Cape, South Africa",OK
    BIZ,////,////,////,UNK,1.0,Bimin,"Bimin, Papua New Guinea",OWNO,////,////,????,"Bimin, Western, Papua-New Guinea",OK
    BJA,////,////,DAAE,UNK,0.353,Bejaia,"Bejaia, Algeria",OWNO,////,////,Soummam -Abane Ramdane Airport,"Bejaia, Béjaïa, Algeria",OK
    BJB,////,////,OIMN,UNK,1.0,Bojnord,"Bojnord, Iran",OWNO,////,////,????,"Bojnord, Khorasan-e Shemli, Iran",OK

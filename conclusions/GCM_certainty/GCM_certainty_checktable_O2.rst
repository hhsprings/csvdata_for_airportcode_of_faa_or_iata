
List for checking certainty of Great Circle Mapper (OKY - OSN)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    OKY,////,////,YBOK,UNK,1.0,Oakey,"Oakey, Australia",OWNO,////,////,????,"Oakey, Queensland, Australia",OK
    OLA,////,////,ENOL,UNK,0.8,Orland,"Orland, Norway",OWNO,////,////,Ørland AB,"Brekstad, Norway",TO DO CHECK
    OLB,////,////,LIEO,UNK,1.0,Costa Smeralda,"Olbia, Italy",OWNO,////,////,Costa Smeralda,"Olbia, Sardinia, Italy",OK
    OLC,////,////,SDCG,UNK,nan,////,////,////,////,////,Senadora Eunice Michiles,"São Paulo de Olivença, Amazonas, Brazil",UNK
    OLD,OLD,KOLD,KOLD,OK,0.41,Old Town,"Old Town (ME), USA",OWNO,"DEWITT FLD, OLD TOWN MUNI","OLD TOWN, ME - UNITED STATES","Dewitt Field, Old Town Municipal Airport","Old Town, Maine, United States",OK
    OLE,OLE,KOLE,KOLE,OK,0.428,Municipal,"Olean (NY), USA",OWNO,CATTARAUGUS COUNTY-OLEAN,"OLEAN, NY - UNITED STATES",Cattaraugus County-Olean Airport,"Olean, New York, United States",OK
    OLF,OLF,KOLF,KOLF,OK,0.687,International,"Wolf Point (MT), USA",OWNO,L M CLAYTON,"WOLF POINT, MT - UNITED STATES",L M Clayton Airport,"Wolf Point, Montana, United States",OK
    OLH,6R7,////,////,OK,0.661,Old Harbor Airport and SPB,"Old Harbor (AK), USA",OWNO,OLD HARBOR,"OLD HARBOR, AK - UNITED STATES",????,"Old Harbor, Alaska, United States",OK
    OLI,////,////,BIRF,UNK,1.0,Rif,"Olafsvik, Iceland",OWNO,////,////,Rif Airport,"Ólafsvik, Iceland",OK
    OLJ,////,////,NVSZ,UNK,0.37,Olpoi,"Olpoi, Vanuatu",OWNO,////,////,North West Santo,"Olpoi, Espíritu Santo Island, Sanma, Vanuatu",OK
    OLL,////,////,FCOD,UNK,0.609,Ollombo,Oyo,IATA,////,////,Oyo Ollombo International Airport,"Oyo, Congo (Republic of)",MAYBE
    OLM,OLM,KOLM,KOLM,OK,0.771,Olympia,"Olympia (WA), USA",OWNO,OLYMPIA RGNL,"OLYMPIA, WA - UNITED STATES",Olympia Regional,"Olympia, Washington, United States",OK
    OLN,////,////,////,UNK,1.0,Colonia Sarmiento,"Colonia Sarmiento, Argentina",OWNO,////,////,????,"Colonia Sarmiento, Chubut, Argentina",OK
    OLO,////,////,LKOL,UNK,1.0,Olomouc,"Olomouc, Czech Republic",OWNO,////,////,Olomouc Airport,"Olomouc, Olomouc, Czech Republic",OK
    OLP,////,////,YOLD,UNK,1.0,Olympic Dam,"Olympic Dam, Australia",OWNO,////,////,????,"Olympic Dam, South Australia, Australia",OK
    OLQ,////,////,////,UNK,1.0,Olsobip,"Olsobip, Papua New Guinea",OWNO,////,////,????,"Olsobip, Western, Papua-New Guinea",OK
    OLS,OLS,KOLS,KOLS,OK,1.0,International,"Nogales (AZ), USA",OWNO,NOGALES INTL,"NOGALES, AZ - UNITED STATES",Nogales International Airport,"Nogales, Arizona, United States",OK
    OLU,OLU,KOLU,KOLU,OK,0.778,Columbus,"Columbus (NE), USA",OWNO,COLUMBUS MUNI,"COLUMBUS, NE - UNITED STATES",Columbus Municipal Airport,"Columbus, Nebraska, United States",OK
    OLV,OLV,KOLV,KOLV,OK,1.0,Olive Branch Airport,"Olive Branch (MS), USA",OWNO,OLIVE BRANCH,"OLIVE BRANCH, MS - UNITED STATES",????,"Olive Branch, Mississippi, United States",OK
    OLY,OLY,KOLY,KOLY,OK,1.0,Olney-Noble,"Olney (IL), USA",OWNO,OLNEY-NOBLE,"OLNEY-NOBLE, IL - UNITED STATES",????,"Olney-Noble, Illinois, United States",OK
    OLZ,////,////,UEMO,UNK,nan,////,////,////,////,////,Olekminsk Airport,"Olekminsk, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    OMA,OMA,KOMA,KOMA,OK,1.0,Eppley Airfield,"Omaha (NE), USA",OWNO,EPPLEY AIRFIELD,"OMAHA, NE - UNITED STATES",Eppley Airfield,"Omaha, Nebraska, United States",OK
    OMB,////,////,FOOH,UNK,1.0,Omboue,"Omboue, Gabon",OWNO,////,////,????,"Omboue, Gabon",OK
    OMC,////,////,RPVO,UNK,1.0,Ormoc,"Ormoc, Philippines",OWNO,////,////,????,"Ormoc, Leyte Island, Philippines",OK
    OMD,////,////,FYOG,UNK,1.0,Oranjemund,"Oranjemund, Namibia",OWNO,////,////,????,"Oranjemund, Namibia",OK
    OME,OME,PAOM,PAOM,OK,1.0,Nome,"Nome (AK), USA",OWNO,NOME,"NOME, AK - UNITED STATES",????,"Nome, Alaska, United States",OK
    OMF,////,////,OJMF,UNK,1.0,King Hussein,"Mafraq, Jordan",OWNO,////,////,King Hussein,"Mafraq, Jordan",OK
    OMG,////,////,FYOE,UNK,1.0,Omega,"Omega, Namibia",OWNO,////,////,????,"Omega, Namibia",OK
    OMH,////,////,OITR,UNK,0.714,Urmieh,"Urmieh, Iran",OWNO,////,////,????,"Orumiyeh, Azarbayjan-e Gharbi, Iran",TO DO CHECK
    OMI,////,////,OIAJ,UNK,nan,////,////,////,////,////,Omidiyeh AB,"Omidiyeh, Khuzestan, Iran",UNK
    OMK,OMK,KOMK,KOMK,OK,0.734,Municipal,"Omak (WA), USA",OWNO,OMAK,"OMAK, WA - UNITED STATES",????,"Omak, Washington, United States",OK
    OML,////,////,////,UNK,1.0,Omkalai,"Omkalai, Papua New Guinea",OWNO,////,////,????,"Omkalai, Chimbu, Papua-New Guinea",OK
    OMM,////,////,OONR,UNK,1.0,Marmul,"Marmul, Oman",OWNO,////,////,????,"Marmul, Oman",OK
    OMN,////,////,////,UNK,1.0,Osmanabad,"Osmanabad, India",OWNO,////,////,????,"Osmanabad, Maharashtra, India",OK
    OMO,////,////,LQMO,UNK,0.706,Mostar,"Mostar, Bosnia and Herzegovina",OWNO,////,////,Mostar International Airport,"Mostar, Bosnia and Herzegovina",OK
    OMR,////,////,LROD,UNK,1.0,Oradea,"Oradea, Romania",OWNO,////,////,????,"Oradea, Romania",OK
    OMS,////,////,UNOO,UNK,1.0,Omsk Tsentralny Airport,"Omsk, Russia",OWNO,////,////,Tsentralny Airport,"Omsk, Omskaya, Russian Federation (Russia)",OK
    OMY,////,////,////,UNK,1.0,Oddor Meanche,"Oddor Meanche, Cambodia",OWNO,////,////,????,"Oddor Meanche, Cambodia",OK
    ONA,ONA,KONA,KONA,OK,0.507,Municipal,"Winona (MN), USA",OWNO,WINONA MUNI-MAX CONRAD FLD,"WINONA, MN - UNITED STATES",Winona Municipal-Max Conrad Field,"Winona, Minnesota, United States",OK
    ONB,////,////,////,UNK,1.0,Ononge,"Ononge, Papua New Guinea",OWNO,////,////,????,"Ononge, Central, Papua-New Guinea",OK
    OND,////,////,FYOA,UNK,1.0,Ondangwa,"Ondangwa, Namibia",OWNO,////,////,????,"Ondangwa, Namibia",OK
    ONE,////,////,////,UNK,1.0,Onepusu,"Onepusu, Solomon Islands",OWNO,////,////,????,"Onepusu, Solomon Islands",OK
    ONG,////,////,YMTI,UNK,0.741,Mornington,"Mornington, Australia",OWNO,////,////,????,"Mornington Island, Queensland, Australia",MAYBE
    ONH,N66,////,////,OK,1.0,Municipal,"Oneonta (NY), USA",OWNO,ONEONTA MUNI,"ONEONTA, NY - UNITED STATES",Oneonta Municipal Airport,"Oneonta, New York, United States",OK
    ONI,////,////,WABD,UNK,1.0,Moanamani,"Moanamani, Indonesia",OWNO,////,////,????,"Moanamani, Papua, Indonesia",OK
    ONJ,////,////,RJSR,UNK,1.0,Odate Noshiro,"Odate Noshiro, Japan",OWNO,////,////,Odate-Noshiro Airport,"Odate-Noshiro, Akita, Japan",OK
    ONK,////,////,UERO,UNK,nan,////,////,////,////,////,Olenek Airport,"Olenek, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    ONL,ONL,KONL,KONL,OK,0.404,Municipal,"Oneill (NE), USA",OWNO,THE O'NEILL MUNI-JOHN L BAKER FIELD,"O'NEILL, NE - UNITED STATES",The O'Neill Municipal-John L Baker Field,"O'Neill, Nebraska, United States",OK
    ONM,ONM,KONM,KONM,OK,0.543,Socorro,"Socorro (NM), USA",OWNO,SOCORRO MUNI,"SOCORRO, NM - UNITED STATES",Socorro Municipal Airport,"Socorro, New Mexico, United States",OK
    ONO,ONO,KONO,KONO,OK,0.719,Ontario,"Ontario (OR), USA",OWNO,ONTARIO MUNI,"ONTARIO, OR - UNITED STATES",Ontario Municipal Airport,"Ontario, Oregon, United States",OK
    ONP,ONP,KONP,KONP,OK,0.734,Newport,"Newport (OR), USA",OWNO,NEWPORT MUNI,"NEWPORT, OR - UNITED STATES",Newport Municipal Airport,"Newport, Oregon, United States",OK
    ONQ,////,////,LTAS,UNK,1.0,Zonguldak,"Zonguldak, Turkey",OWNO,////,////,Zonguldak Airport,"Zonguldak, Zonguldak, Turkey",OK
    ONR,////,////,YMNK,UNK,1.0,Monkira,"Monkira, Australia",OWNO,////,////,????,"Monkira, Queensland, Australia",OK
    ONS,////,////,YOLW,UNK,1.0,Onslow,"Onslow, Australia",OWNO,////,////,????,"Onslow, Western Australia, Australia",OK
    ONT,ONT,KONT,KONT,OK,1.0,International,"Ontario (CA), USA",OWNO,ONTARIO INTL,"ONTARIO, CA - UNITED STATES",Ontario International Airport,"Ontario, California, United States",OK
    ONU,////,////,NFOL,UNK,1.0,Ono I Lau,"Ono I Lau, Fiji",OWNO,////,////,????,"Ono-I-Lau, Fiji",OK
    ONX,////,////,MPEJ,UNK,0.381,Colon,"Colon, Panama",OWNO,////,////,Enrique Adolfo Jimenez,"Colón, Colón, Panamá",OK
    ONY,ONY,KONY,KONY,OK,0.664,Olney,"Olney (TX), USA",OWNO,OLNEY MUNI,"OLNEY, TX - UNITED STATES",Olney Municipal Airport,"Olney, Texas, United States",OK
    OOA,OOA,KOOA,KOOA,OK,1.0,Municipal,"Oskaloosa IA, USA",OWNO,OSKALOOSA MUNI,"OSKALOOSA, IA - UNITED STATES",Oskaloosa Municipal Airport,"Oskaloosa, Iowa, United States",OK
    OOK,OOK,PAOO,PAOO,OK,1.0,Toksook Bay,"Toksook Bay (AK), USA",OWNO,TOKSOOK BAY,"TOKSOOK BAY, AK - UNITED STATES",????,"Toksook Bay, Alaska, United States",OK
    OOL,////,////,YBCG,UNK,1.0,Coolangatta,"Gold Coast, Australia",OWNO,////,////,????,"Coolangatta/Gold Coast, Queensland, Australia",OK
    OOM,////,////,YCOM,UNK,0.385,Cooma,"Cooma, Australia",OWNO,////,////,Cooma-Snowy Mountains Airport,"Cooma, New South Wales, Australia",OK
    OOR,////,////,YMOO,UNK,1.0,Mooraberree,"Mooraberree, Australia",OWNO,////,////,????,"Mooraberree, Queensland, Australia",OK
    OOT,////,////,NGON,UNK,1.0,Onotoa,"Onotoa, Kiribati",OWNO,////,////,????,"Onotoa, Kiribati",OK
    OPA,////,////,BIKP,UNK,1.0,Kopasker,"Kopasker, Iceland",OWNO,////,////,????,"Kópasker, Iceland",OK
    OPB,////,////,////,UNK,1.0,Open Bay,"Open Bay, Papua New Guinea",OWNO,////,////,????,"Open Bay, East New Britain, Papua-New Guinea",OK
    OPF,OPF,KOPF,KOPF,OK,0.674,Opa Locka,"Miami (FL), USA",OWNO,OPA-LOCKA EXECUTIVE,"MIAMI, FL - UNITED STATES",Opa-Locka Executive Airport,"Miami, Florida, United States",OK
    OPI,////,////,YOEN,UNK,1.0,Oenpelli,"Oenpelli, Australia",OWNO,////,////,????,"Oenpelli, Northern Territory, Australia",OK
    OPL,OPL,KOPL,KOPL,OK,0.769,St Landry Parish,"Opelousas (LA), USA",OWNO,ST LANDRY PARISH-AHART FIELD,"OPELOUSAS, LA - UNITED STATES",St. Landry Parish-Ahart Field,"Opelousas, Louisiana, United States",OK
    OPO,////,////,LPPR,UNK,0.313,Porto,"Porto, Portugal",OWNO,////,////,Francisco Sá Carneiro,"Porto, Porto, Portugal",OK
    OPS,////,////,SWSI,UNK,1.0,Sinop,"Sinop, Brazil",OWNO,////,////,????,"Sinop, Mato Grosso, Brazil",OK
    OPU,////,////,AYBM,UNK,1.0,Balimo,"Balimo, Papua New Guinea",OWNO,////,////,????,"Balimo, Western, Papua-New Guinea",OK
    OPW,////,////,FYOP,UNK,1.0,Opuwa,"Opuwa, Namibia",OWNO,////,////,????,"Opuwa, Namibia",OK
    ORA,////,////,SASO,UNK,1.0,Oran,"Oran, Argentina",OWNO,////,////,????,"Orán, Salta, Argentina",OK
    ORB,////,////,ESOE,UNK,0.632,Orebro-Bofors,"Orebro, Sweden",OWNO,////,////,????,"Örebro, Örebro län, Sweden",OK
    ORC,////,////,SKOE,UNK,1.0,Orocue,"Orocue, Colombia",OWNO,////,////,Orocue Airport,"Orocue, Casanare, Colombia",OK
    ORD,ORD,KORD,KORD,OK,1.0,O'Hare International,"Chicago (IL), USA",OWNO,CHICAGO O'HARE INTL,"CHICAGO, IL - UNITED STATES",Chicago O'Hare International Airport,"Chicago, Illinois, United States",OK
    ORE,////,////,LFOJ,UNK,0.7,Orleans,"Orleans, France",OWNO,////,////,Bricy,"Orléans, Centre, France",OK
    ORF,ORF,KORF,KORF,OK,1.0,International,"Norfolk (VA), USA",OWNO,NORFOLK INTL,"NORFOLK, VA - UNITED STATES",Norfolk International Airport,"Norfolk, Virginia, United States",OK
    ORG,////,////,SMZO,UNK,1.0,Zorg en Hoop Airport,"Paramaribo, Suriname",OWNO,////,////,Zorg en Hoop,"Paramaribo, Paramaribo, Suriname",OK
    ORH,ORH,KORH,KORH,OK,0.819,Worcester,"Worcester (MA), USA",OWNO,WORCESTER RGNL,"WORCESTER, MA - UNITED STATES",Worcester Regional,"Worcester, Massachusetts, United States",OK
    ORI,ORI,////,////,OK,0.801,Port Lions SPB,"Port Lions (AK), USA",OWNO,PORT LIONS,"PORT LIONS, AK - UNITED STATES",????,"Port Lions, Alaska, United States",OK
    ORJ,////,////,SYOR,UNK,1.0,Orinduik,"Orinduik, Guyana",OWNO,////,////,????,"Orinduik, Potaro-Siparuni, Guyana",OK
    ORK,////,////,EICK,UNK,0.667,Cork,"Cork, Ireland",OWNO,////,////,Cork International Airport,"Cork, County Cork, Munster, Ireland",OK
    ORL,ORL,KORL,KORL,OK,0.391,Herndon,"Orlando (FL), USA",OWNO,EXECUTIVE,"ORLANDO, FL - UNITED STATES",Executive Airport,"Orlando, Florida, United States",OK
    ORM,////,////,EGBK,UNK,0.833,Northampton,"Northampton, United Kingdom",OWNO,////,////,Sywell,"Northampton, Northamptonshire, England, United Kingdom",OK
    ORN,////,////,DAOO,UNK,0.839,Es Senia,"Oran, Algeria",OWNO,////,////,Es Sénia International Airport,"Oran, Oran, Algeria",OK
    ORO,////,////,MHYR,UNK,1.0,Yoro,"Yoro, Honduras",OWNO,////,////,????,"Yoro, Yoro, Honduras",OK
    ORP,////,////,FBOR,UNK,1.0,Orapa,"Orapa, Botswana",OWNO,////,////,Orapa Airport,"Orapa, Central, Botswana",OK
    ORR,////,////,YYOR,UNK,1.0,Yorketown,"Yorketown, Australia",OWNO,////,////,????,"Yorketown, South Australia, Australia",OK
    ORT,ORT,PAOR,PAOR,OK,1.0,Northway Airport,"Northway (AK), USA",OWNO,NORTHWAY,"NORTHWAY, AK - UNITED STATES",????,"Northway, Alaska, United States",OK
    ORU,////,////,SLOR,UNK,0.435,Oruro,"Oruro, Bolivia",OWNO,////,////,Juan Mendoza Airport,"Oruro, Cercado, Oruro, Bolivia",OK
    ORV,D76,PFNO,////,NG,0.889,Curtis Memorial,"Noorvik (AK), USA",OWNO,ROBERT/BOB/CURTIS MEMORIAL,"NOORVIK, AK - UNITED STATES",Robert (Bob) Curtis Memorial,"Noorvik, Alaska, United States",OK
    ORW,////,////,OPOR,UNK,1.0,Ormara,"Ormara, Pakistan",OWNO,////,////,????,"Ormara, Balochistan, Pakistan",OK
    ORX,////,////,SNOX,UNK,1.0,Oriximina,"Oriximina, Brazil",OWNO,////,////,????,"Oriximina, Pará, Brazil",OK
    ORY,////,////,LFPO,UNK,1.0,Orly,"Paris, France",OWNO,////,////,Orly,"Paris, Île-de-France, France",OK
    ORZ,////,////,////,UNK,1.0,Orange Walk,"Orange Walk, Belize",OWNO,////,////,Orange Walk Airport,"Orange Walk, Orange Walk, Belize",OK
    OSA,////,////,////,UNK,0.357,Metropolitan Area,"Osaka, Japan",OWNO,////,////,????,"Osaka, Osaka, Japan",OK
    OSB,K15,////,////,OK,0.57,Osage Beach,"Osage Beach (MO), USA",OWNO,GRAND GLAIZE-OSAGE BEACH,"OSAGE BEACH, MO - UNITED STATES",Grand Glaize-Osage Beach Airport,"Osage Beach, Missouri, United States",OK
    OSC,OSC,KOSC,KOSC,OK,0.857,Wurtsmith AFB,"Oscoda (MI), USA",OWNO,OSCODA-WURTSMITH,"OSCODA, MI - UNITED STATES",Oscoda-Wurtsmith Airport,"Oscoda, Michigan, United States",OK
    OSD,////,////,ESNZ,UNK,0.857,Froesoe,"Ostersund, Sweden",OWNO,////,////,Frösön AB,"Östersund, Jämtlands län, Sweden",OK
    OSE,////,////,////,UNK,1.0,Omora,"Omora, Papua New Guinea",OWNO,////,////,????,"Omora, Morobe, Papua-New Guinea",OK
    OSF,////,////,UUMO,UNK,nan,////,////,////,////,////,Ostafyevo International Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",UNK
    OSG,////,////,////,UNK,1.0,Ossima,"Ossima, Papua New Guinea",OWNO,////,////,????,"Ossima, Sandaun, Papua-New Guinea",OK
    OSH,OSH,KOSH,KOSH,OK,0.712,Wittman Field,"Oshkosh (WI), USA",OWNO,WITTMAN RGNL,"OSHKOSH, WI - UNITED STATES",Wittman Regional,"Oshkosh, Wisconsin, United States",OK
    OSI,////,////,LDOS,UNK,1.0,Osijek,"Osijek, Croatia",OWNO,////,////,????,"Osijek, Croatia",OK
    OSK,////,////,ESMO,UNK,1.0,Oskarshamn,"Oskarshamn, Sweden",OWNO,////,////,????,"Oskarshamn, Kalmar län, Sweden",OK
    OSL,////,////,ENGM,UNK,1.0,"Oslo Airport, Gardermoen","Oslo, Norway",OWNO,////,////,Oslo Airport Gardermoen,"Oslo, Akershus, Norway",OK
    OSM,////,////,ORBM,UNK,0.667,Mosul,"Mosul, Iraq",OWNO,////,////,Mosul International Airport,"Mosul, Iraq",OK
    OSN,////,////,RKSO,UNK,1.0,Ab,"Osan, South Korea",OWNO,////,////,Osan AB,"Osan, Republic of Korea (South Korea)",OK

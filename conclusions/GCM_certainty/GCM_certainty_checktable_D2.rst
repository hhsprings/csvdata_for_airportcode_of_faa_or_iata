
List for checking certainty of Great Circle Mapper (DJH - DSA)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    DJH,////,////,////,UNK,1.0,Jebel Ali SPB,Dubai,IATA,////,////,Jebel Ali SPB,"Dubai, United Arab Emirates",MAYBE
    DJJ,////,////,////,UNK,1.0,Sentani,"Jayapura, Indonesia",OWNO,////,////,Sentani,"Djayapura, Papua, Indonesia",OK
    DJM,////,////,FCBD,UNK,1.0,Djambala,"Djambala, Congo (ROC)",OWNO,////,////,????,"Djambala, Congo (Republic of)",OK
    DJN,D66,////,////,OK,1.0,Delta Junction,"Delta Junction, USA (AK)",OWNO,DELTA JUNCTION,"DELTA JUNCTION, AK - UNITED STATES",????,"Delta Junction, Alaska, United States",OK
    DJO,////,////,DIDL,UNK,1.0,Daloa,"Daloa, Cote d'Ivoire",OWNO,////,////,????,"Daloa, Haut-Sassandra, Côte d'Ivoire (Ivory Coast)",OK
    DJU,////,////,BIDV,UNK,1.0,Djupivogur,"Djupivogur, Iceland",OWNO,////,////,????,"Djúpivogur, Iceland",OK
    DKA,////,////,DNKT,UNK,1.0,Katsina,Katsina,IATA,////,////,Katsina Airport,"Katsina, Katsina, Nigeria",MAYBE
    DKI,////,////,YDKI,UNK,1.0,Dunk Island,"Dunk Island, Australia",OWNO,////,////,????,"Dunk Island, Queensland, Australia",OK
    DKK,DKK,KDKK,KDKK,OK,1.0,Dunkirk,"Dunkirk (NY), USA",OWNO,CHAUTAUQUA COUNTY/DUNKIRK,"DUNKIRK, NY - UNITED STATES",Chautauqua County/Dunkirk Airport,"Dunkirk, New York, United States",OK
    DKR,////,////,GOOY,UNK,0.375,Yoff,"Dakar, Senegal",OWNO,////,////,Léopold Sédar Senghor International Airport,"Dakar, Dakar, Senegal",OK
    DKS,////,////,UODD,UNK,1.0,Dikson,"Dikson, Russia",OWNO,////,////,Dikson Airport,"Dikson, Krasnoyarskiy, Russian Federation (Russia)",OK
    DKV,////,////,YDVR,UNK,1.0,Docker River,"Docker River, Australia",OWNO,////,////,????,"Docker River, Northern Territory, Australia",OK
    DLA,////,////,FKKD,UNK,0.706,Douala,"Douala, Cameroon",OWNO,////,////,Douala International Airport,"Douala, Littoral, Cameroon",OK
    DLB,////,////,////,UNK,1.0,Dalbertis,"Dalbertis, Papua New Guinea",OWNO,////,////,????,"Dalbertis, Sandaun, Papua-New Guinea",OK
    DLC,////,////,ZYTL,UNK,0.733,Dalian,"Dalian, PR China",OWNO,////,////,Zhoushuizi International Airport,"Dalian, Liaoning, China",OK
    DLD,////,////,ENDI,UNK,1.0,Dagali Airport,"Geilo, Norway",OWNO,////,////,Geilo,"Dagali, Buskerud, Norway",OK
    DLE,////,////,LFGJ,UNK,1.0,Tavaux,"Dole, France",OWNO,////,////,Tavaux,"Dole, Franche-Comté, France",OK
    DLF,DLF,KDLF,KDLF,OK,1.0,Laughlin AFB,"Del Rio (TX), USA",OWNO,LAUGHLIN AFB,"DEL RIO, TX - UNITED STATES",Laughlin AFB,"Del Rio, Texas, United States",OK
    DLG,DLG,PADL,PADL,OK,0.87,Municipal,"Dillingham (AK), USA",OWNO,DILLINGHAM,"DILLINGHAM, AK - UNITED STATES",????,"Dillingham, Alaska, United States",OK
    DLH,DLH,KDLH,KDLH,OK,1.0,Duluth International,"Duluth (MN), USA",OWNO,DULUTH INTL,"DULUTH, MN - UNITED STATES",Duluth International Airport,"Duluth, Minnesota, United States",OK
    DLI,////,////,VVDL,UNK,0.593,Lienkhang,"Dalat, Viet Nam",OWNO,////,////,Lien Khuong International Airport,"Da Lat, Vietnam",OK
    DLK,////,////,YDLK,UNK,1.0,Dulkaninna,"Dulkaninna, Australia",OWNO,////,////,????,"Dulkaninna, South Australia, Australia",OK
    DLL,DLC,KDLC,KDLC,OK,0.76,Dillon,"Dillon (SC), USA",OWNO,DILLON COUNTY,"DILLON, SC - UNITED STATES",Dillon County Airport,"Dillon, South Carolina, United States",OK
    DLM,////,////,LTBS,UNK,1.0,Dalaman,"Dalaman, Turkey",OWNO,////,////,????,"Dalaman, Mugla, Turkey",OK
    DLN,DLN,KDLN,KDLN,OK,1.0,Dillon,"Dillon (MT), USA",OWNO,DILLON,"DILLON, MT - UNITED STATES",????,"Dillon, Montana, United States",OK
    DLS,DLS,KDLS,KDLS,OK,0.79,The Dalles,"The Dalles (OR), USA",OWNO,COLUMBIA GORGE RGNL/THE DALLES MUNI,"THE DALLES, OR - UNITED STATES",Columbia Gorge Regional/The Dalles Municipal Airport,"The Dalles, Oregon, United States",OK
    DLU,////,////,ZPDL,UNK,1.0,Dali,"Dali City, PR China",OWNO,////,////,Dali,"Xiaguan, Yunnan, China",OK
    DLV,////,////,YDLV,UNK,1.0,Delissaville,"Delissaville, Australia",OWNO,////,////,????,"Delissaville, Northern Territory, Australia",OK
    DLY,////,////,NVVD,UNK,1.0,Dillons Bay,"Dillons Bay, Vanuatu",OWNO,////,////,????,"Dillon's Bay, Erromango Island, Taféa, Vanuatu",OK
    DLZ,////,////,ZMDZ,UNK,0.769,Dalanzadgad,"Dalanzadgad, Mongolia",OWNO,////,////,Omnogobi,"Dalanzadgad, Mongolia",OK
    DMA,DMA,KDMA,KDMA,OK,1.0,Davis Monthan AFB,"Tucson (AZ), USA",OWNO,DAVIS MONTHAN AFB,"TUCSON, AZ - UNITED STATES",Davis Monthan AFB,"Tucson, Arizona, United States",OK
    DMB,////,////,UADD,UNK,0.7,Zhambyl,"Zhambyl, Kazakhstan",OWNO,////,////,Taraz Airport,"Taraz, Zhambyl, Kazakhstan",OK
    DMD,////,////,YDMG,UNK,0.692,Doomadgee,"Doomadgee, Australia",OWNO,////,////,????,"Doomadgee Mission, Queensland, Australia",MAYBE
    DME,////,////,UUDD,UNK,0.903,Domodedovo,"Moscow, Russia",OWNO,////,////,Domodedovo International Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",OK
    DMK,////,////,VTBD,UNK,0.85,Don Muang,"Bangkok, Thailand",OWNO,////,////,Don Mueang International Airport,"Bangkok, Krung Thep Maha Nakhon (Bangkok), Thailand",OK
    DMM,////,////,OEDF,UNK,1.0,King Fahad International,"Dammam, Saudi Arabia",OWNO,////,////,King Fahd International Airport,"Dammam, Saudi Arabia",OK
    DMN,DMN,KDMN,KDMN,OK,0.64,Deming,"Deming (NM), USA",OWNO,DEMING MUNI,"DEMING, NM - UNITED STATES",Deming Municipal Airport,"Deming, New Mexico, United States",OK
    DMO,DMO,KDMO,KDMO,OK,0.748,Sedalia,"Sedalia (MO), USA",OWNO,SEDALIA RGNL,"SEDALIA, MO - UNITED STATES",Sedalia Regional,"Sedalia, Missouri, United States",OK
    DMR,////,////,////,UNK,1.0,Dhamar,"Dhamar, Yemen",OWNO,////,////,????,"Dhamar, Yemen",OK
    DMT,////,////,SWDM,UNK,1.0,Diamantino,"Diamantino, Brazil",OWNO,////,////,????,"Diamantino, Mato Grosso, Brazil",OK
    DMU,////,////,VEMR,UNK,1.0,Dimapur,"Dimapur, India",OWNO,////,////,????,"Dimapur, Nagaland, India",OK
    DNA,////,////,RODN,UNK,0.971,Kadena AFB,"Okinawa, Japan",OWNO,////,////,Kadena AB,"Okinawa, Ryukyu Island, Okinawa, Japan",OK
    DNB,////,////,YDBR,UNK,1.0,Dunbar,"Dunbar, Australia",OWNO,////,////,????,"Dunbar, Queensland, Australia",OK
    DND,////,////,EGPN,UNK,1.0,Dundee,"Dundee, United Kingdom",OWNO,////,////,????,"Dundee, Angus, Scotland, United Kingdom",OK
    DNF,////,////,////,UNK,1.0,Martuba,"Derna, Libya",OWNO,////,////,Martubah,"Derna, Libyan Arab Jamahiriya (Libya)",OK
    DNH,////,////,ZLDH,UNK,1.0,Dunhuang,"Dunhuang, PR China",OWNO,////,////,????,"Dunhuang, Gansu, China",OK
    DNI,////,////,HSWD,UNK,1.0,Wad Medani,"Wad Medani, Sudan",OWNO,////,////,????,"Wad Medani, Gezira, Sudan",OK
    DNK,////,////,UKDD,UNK,0.875,Dnepropetrovsk,"Dnepropetrovsk, Ukraine",OWNO,////,////,Dnepropetrovsk International Airport,"Dnepropetrovsk, Dnipropetrovsk, Ukraine",OK
    DNM,////,////,////,UNK,1.0,Denham,"Denham, Australia",OWNO,////,////,????,"Denham, Western Australia, Australia",OK
    DNN,DNN,KDNN,KDNN,OK,1.0,Municipal,"Dalton (GA), USA",OWNO,DALTON MUNI,"DALTON, GA - UNITED STATES",Dalton Municipal Airport,"Dalton, Georgia, United States",OK
    DNO,////,////,SWDN,UNK,1.0,Dianopolis,"Dianopolis, Brazil",OWNO,////,////,????,"Dianopolis, Tocantins, Brazil",OK
    DNP,////,////,VNDG,UNK,0.5,Dang,"Dang, Nepal",OWNO,////,////,Tulsipur,"Dang, Nepal",OK
    DNQ,////,////,YDLQ,UNK,1.0,Deniliquin,"Deniliquin, Australia",OWNO,////,////,????,"Deniliquin, New South Wales, Australia",OK
    DNR,////,////,LFRD,UNK,1.0,Pleurtuit,"Dinard, France",OWNO,////,////,????,"Dinard/Pleurtuit/St-Malo, Bretagne, France",OK
    DNS,DNS,KDNS,KDNS,OK,1.0,Municipal,"Denison IA, USA",OWNO,DENISON MUNI,"DENISON, IA - UNITED STATES",Denison Municipal Airport,"Denison, Iowa, United States",OK
    DNU,////,////,////,UNK,1.0,Dinangat,"Dinangat, Papua New Guinea",OWNO,////,////,????,"Dinangat, Morobe, Papua-New Guinea",OK
    DNV,DNV,KDNV,KDNV,OK,0.669,Vermilion County,"Danville (IL), USA",OWNO,VERMILION REGIONAL,"DANVILLE, IL - UNITED STATES",Vermilion Regional,"Danville, Illinois, United States",OK
    DNX,////,////,HSGG,UNK,1.0,Galegu,"Dinder, Sudan",OWNO,////,////,Galegu,"Dinder, Sennar, Sudan",OK
    DNZ,////,////,LTAY,UNK,1.0,Cardak,"Denizli, Turkey",OWNO,////,////,Cardak,"Denizli, Denizli, Turkey",OK
    DOA,////,////,////,UNK,1.0,Doany,"Doany, Madagascar",OWNO,////,////,????,"Doany, Madagascar",OK
    DOB,////,////,WAPD,UNK,1.0,Dobo Airport,"Dobo, Indonesia",OWNO,////,////,????,"Dobo, Wamar Island, Maluku, Indonesia",OK
    DOC,////,////,////,UNK,1.0,Dornoch,"Dornoch, United Kingdom",OWNO,////,////,????,"Dornoch, Highlands, Scotland, United Kingdom",OK
    DOD,////,////,HTDO,UNK,1.0,Dodoma,"Dodoma, Tanzania",OWNO,////,////,Dodoma Airport,"Dodoma, Dodoma, Tanzania",OK
    DOE,////,////,SMDJ,UNK,0.609,Djoemoe,"Djoemoe, Suriname",OWNO,////,////,Djoemoe Airstrip,"Djoemoe, Sipaliwini, Suriname",OK
    DOG,////,////,HSDN,UNK,1.0,Dongola,"Dongola, Sudan",OWNO,////,////,????,"Dongola, Northern, Sudan",OK
    DOH,////,////,OTHH,UNK,0.421,Doha,"Doha, Qatar",OWNO,////,////,Hamad International Airport,"Doha, Qatar",OK
    DOI,////,////,////,UNK,1.0,Doini,"Doini, Papua New Guinea",OWNO,////,////,????,"Doini, Milne Bay, Papua-New Guinea",OK
    DOK,////,////,UKCC,UNK,nan,////,////,////,////,////,????,"Donetsk, Donetsk, Ukraine",UNK
    DOL,////,////,LFRG,UNK,1.0,St Gatien,"Deauville, France",OWNO,////,////,St-Gatien,"Deauville, Basse-Normandie (Lower Normandy), France",OK
    DOM,////,////,TDPD,UNK,1.0,Melville Hall,"Dominica, Dominica",OWNO,////,////,Melville Hall,"Marigot, Dominica",OK
    DON,////,////,////,UNK,1.0,Dos Lagunas,"Dos Lagunas, Guatemala",OWNO,////,////,????,"Dos Lagunas, Petén, Guatemala",OK
    DOO,////,////,////,UNK,1.0,Dorobisoro,"Dorobisoro, Papua New Guinea",OWNO,////,////,????,"Dorobisoro, Central, Papua-New Guinea",OK
    DOP,////,////,VNDP,UNK,1.0,Dolpa,"Dolpa, Nepal",OWNO,////,////,????,"Dolpa, Nepal",OK
    DOR,////,////,DFEE,UNK,1.0,Dori,"Dori, Burkina Faso",OWNO,////,////,????,"Dori, Burkina Faso",OK
    DOS,////,////,////,UNK,1.0,Dios,"Dios, Papua New Guinea",OWNO,////,////,????,"Dios, Bougainville, Papua-New Guinea",OK
    DOU,////,////,SSDO,UNK,1.0,Dourados,"Dourados, Brazil",OWNO,////,////,????,"Dourados, Mato Grosso do Sul, Brazil",OK
    DOV,DOV,KDOV,KDOV,OK,1.0,Dover AFB,"Dover (DE), USA",OWNO,DOVER AFB,"DOVER, DE - UNITED STATES",Dover AFB,"Dover, Delaware, United States",OK
    DOX,////,////,YDRA,UNK,1.0,Dongara,"Dongara, Australia",OWNO,////,////,????,"Dongara, Western Australia, Australia",OK
    DOY,////,////,ZSDY,UNK,nan,////,////,////,////,////,????,"Dongying, Shandong, China",UNK
    DPA,DPA,KDPA,KDPA,OK,0.664,Dupage County,"Chicago (IL), USA",OWNO,DUPAGE,"CHICAGO/WEST CHICAGO, IL - UNITED STATES",Dupage Airport,"Chicago/West Chicago, Illinois, United States",OK
    DPE,////,////,LFAB,UNK,0.571,Dieppe,"Dieppe, France",OWNO,////,////,St-Aubin,"Dieppe, Haute-Normandie (Upper Normandy), France",OK
    DPG,DPG,KDPG,KDPG,OK,1.0,Michael AAF,"Dugway (UT), USA",OWNO,MICHAEL AAF (DUGWAY PROVING GROUND),"DUGWAY PROVING GROUND, UT - UNITED STATES",Michael AAF Airport,"Dugway Proving Ground, Utah, United States",OK
    DPL,////,////,RPMG,UNK,1.0,Dipolog,"Dipolog, Philippines",OWNO,////,////,????,"Dipolog, Philippines",OK
    DPO,////,////,YDPO,UNK,1.0,Devonport,"Devonport, Australia",OWNO,////,////,????,"Devonport, Tasmania, Australia",OK
    DPS,////,////,WADD,UNK,0.906,Ngurah Rai,"Denpasar Bali, Indonesia",OWNO,////,////,Ngurah Rai - Bali International Airport,"Denpasar, Bali, Indonesia",MAYBE
    DPT,////,////,UEBD,UNK,0.857,Deputatsky,Deputatsky,IATA,////,////,Deputatskij Airport,"Deputatskij, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    DPU,////,////,////,UNK,1.0,Dumpu,"Dumpu, Papua New Guinea",OWNO,////,////,????,"Dumpu, Madang, Papua-New Guinea",OK
    DQA,////,////,ZYDQ,UNK,nan,////,////,////,////,////,Daqing Sartu Airport,"Daqing, Heilongjiang, China",UNK
    DQM,////,////,////,UNK,0.615,International,Duqm,IATA,////,////,Duqm International Airport,"Duqm, Oman",MAYBE
    DRA,NV65,////,KDRA,NG,1.0,Desert Rock,"Mercury (NV), USA",OWNO,DESERT ROCK,"MERCURY, NV - UNITED STATES",Desert Rock Airport,"Mercury, Nevada, United States",OK
    DRB,////,////,YDBY,UNK,1.0,Derby,"Derby, Australia",OWNO,////,////,????,"Derby, Western Australia, Australia",OK
    DRC,////,////,////,UNK,1.0,Dirico,"Dirico, Angola",OWNO,////,////,????,"Dirico, Angola",OK
    DRD,////,////,YDOR,UNK,0.667,Dorunda Station,"Dorunda Station, Australia",OWNO,////,////,????,"Dorunda, Queensland, Australia",MAYBE
    DRE,DRM,KDRM,KDRM,OK,1.0,Drummond Island,"Drummond Island (MI), USA",OWNO,DRUMMOND ISLAND,"DRUMMOND ISLAND, MI - UNITED STATES",????,"Drummond Island, Michigan, United States",OK
    DRG,DEE,PADE,PADE,OK,1.0,Deering,"Deering (AK), USA",OWNO,DEERING,"DEERING, AK - UNITED STATES",????,"Deering, Alaska, United States",OK
    DRH,////,////,////,UNK,1.0,Dabra,"Dabra, Indonesia",OWNO,////,////,????,"Dabra, Papua, Indonesia",OK
    DRI,DRI,KDRI,KDRI,OK,0.79,Beauregard Parish,"De Ridder (LA), USA",OWNO,BEAUREGARD RGNL,"DE RIDDER, LA - UNITED STATES",Beauregard Regional,"De Ridder, Louisiana, United States",OK
    DRJ,////,////,SMDA,UNK,1.0,Drietabbetje,"Drietabbetje, Suriname",OWNO,////,////,????,"Drietabbetje, Sipaliwini, Suriname",OK
    DRK,////,////,MRDK,UNK,nan,////,////,////,////,////,Puntarenas Airport,"Drake Bay, Puntarenas, Costa Rica",UNK
    DRM,////,////,////,UNK,1.0,Drama,"Drama, Greece",OWNO,////,////,????,"Drama, Anatolikí Makedonía kai Thráki (Eastern Macedonia and Thrace), Greece",OK
    DRN,////,////,YDBI,UNK,1.0,Dirranbandi,"Dirranbandi, Australia",OWNO,////,////,????,"Dirranbandi, Queensland, Australia",OK
    DRO,DRO,KDRO,KDRO,OK,0.799,La Plata,"Durango (CO), USA",OWNO,DURANGO-LA PLATA COUNTY,"DURANGO, CO - UNITED STATES",Durango-La Plata County Airport,"Durango, Colorado, United States",OK
    DRR,////,////,YDRI,UNK,1.0,Durrie,"Durrie, Australia",OWNO,////,////,????,"Durrie, Queensland, Australia",OK
    DRS,////,////,EDDC,UNK,1.0,Dresden Airport (Dresden-Klotzsche Airport),"Dresden, Germany",OWNO,////,////,Dresden Airport,"Dresden, Saxony, Germany",OK
    DRT,DRT,KDRT,KDRT,OK,1.0,International,"Del Rio (TX), USA",OWNO,DEL RIO INTL,"DEL RIO, TX - UNITED STATES",Del Rio International Airport,"Del Rio, Texas, United States",OK
    DRU,M26,////,////,OK,1.0,Drummond,"Drummond, USA (MT)",OWNO,DRUMMOND,"DRUMMOND, MT - UNITED STATES",????,"Drummond, Montana, United States",OK
    DRV,////,////,VRMD,UNK,nan,////,////,////,////,////,Dharavandhoo Airport,"Dharavandhoo Island, Baa Atoll, Maldives",UNK
    DRW,////,////,YPDN,UNK,0.75,Darwin,"Darwin, Australia",OWNO,////,////,Darwin International Airport,"Darwin, Northern Territory, Australia",OK
    DRY,////,////,YDRD,UNK,1.0,Drysdale River,"Drysdale River, Australia",OWNO,////,////,Drysdale River Airport,"Drysdale River Station, Western Australia, Australia",OK
    DSA,////,////,EGCN,UNK,1.0,Robin Hood Airport Doncaster Sheffield,"Doncaster, United Kingdom",OWNO,////,////,Robin Hood Airport,"Doncaster/Sheffield, Yorkshire, England, United Kingdom",OK

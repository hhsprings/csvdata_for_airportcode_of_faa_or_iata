
List for checking certainty of Great Circle Mapper (UAB - UOX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    UAB,////,////,LTAG,UNK,nan,////,////,////,////,////,Incirlik AB,"Adana, Adana, Turkey",UNK
    UAC,////,////,////,UNK,1.0,San Luis Rio Colorado,"San Luis Rio Colorado, Mexico",OWNO,////,////,????,"San Luis Río Colorado, Sonora, México",OK
    UAE,////,////,////,UNK,1.0,Mount Aue,"Mount Aue, Papua New Guinea",OWNO,////,////,????,"Mount Aue, Western Highlands, Papua-New Guinea",OK
    UAH,////,////,NTMU,UNK,1.0,Ua Huka,"Ua Huka, French Polynesia",OWNO,////,////,????,"Ua Huka, Marquesas Islands, French Polynesia",OK
    UAI,////,////,WPDB,UNK,1.0,Suai,"Suai, Indonesia",OWNO,////,////,Suai,"Suai, Timor-Leste (East Timor)",OK
    UAK,////,////,BGBW,UNK,1.0,Narsarsuaq,"Narsarsuaq, Greenland",OWNO,////,////,????,"Narsarsuaq, Kujalleq, Greenland",OK
    UAL,////,////,FNUA,UNK,1.0,Luau,"Luau, Angola",OWNO,////,////,????,"Luau, Angola",OK
    UAM,UAM,PGUA,PGUA,OK,1.0,Anderson AFB,"Guam, Guam",OWNO,ANDERSEN AFB,"ANDERSEN, MARIANA ISLAND, GU - UNITED STATES",Andersen AFB,"Andersen, Mariana Island, Guam, United States",OK
    UAP,////,////,NTMP,UNK,1.0,Ua Pou,"Ua Pou, French Polynesia",OWNO,////,////,????,"Ua Pou, Marquesas Islands, French Polynesia",OK
    UAQ,////,////,SANU,UNK,1.0,San Juan,"San Juan, Argentina",OWNO,////,////,????,"San Juan, San Juan, Argentina",OK
    UAS,////,////,HKSB,UNK,0.483,Samburu,"Samburu, Kenya",OWNO,////,////,Buffalo Spring,"Samburu, Kenya",OK
    UAX,////,////,////,UNK,1.0,Uaxactun,"Uaxactun, Guatemala",OWNO,////,////,????,"Uaxactún, Petén, Guatemala",OK
    UBA,////,////,SBUR,UNK,0.368,Uberaba,"Uberaba, Brazil",OWNO,////,////,Mário de Almeida Franco Airport,"Uberaba, Minas Gerais, Brazil",OK
    UBB,////,////,YMAA,UNK,1.0,Mabuiag Island,"Mabuiag Island, Australia",OWNO,////,////,????,"Mabuiag Island, Queensland, Australia",OK
    UBI,////,////,////,UNK,1.0,Buin,"Buin, Papua New Guinea",OWNO,////,////,????,"Buin, Bougainville, Papua-New Guinea",OK
    UBJ,////,////,RJDC,UNK,0.375,Ube,"Ube, Japan",OWNO,////,////,Yamaguchi Ube Airport,"Ube, Yamaguchi, Japan",OK
    UBP,////,////,VTUU,UNK,0.857,Muang Ubon,"Ubon Ratchathani, Thailand",OWNO,////,////,????,"Ubon Ratchathani, Ubon Ratchathani, Thailand",OK
    UBR,////,////,WAJU,UNK,1.0,Ubrub,"Ubrub, Indonesia",OWNO,////,////,????,"Ubrub, Papua, Indonesia",OK
    UBS,UBS,KUBS,KUBS,OK,1.0,Lowndes County,"Columbus (MS), USA",OWNO,COLUMBUS-LOWNDES COUNTY,"COLUMBUS, MS - UNITED STATES",Columbus-Lowndes County Airport,"Columbus, Mississippi, United States",OK
    UBT,////,////,SDUB,UNK,1.0,Ubatuba,"Ubatuba, Brazil",OWNO,////,////,????,"Ubatuba, São Paulo, Brazil",OK
    UBU,////,////,YKAL,UNK,1.0,Kalumburu,"Kalumburu, Australia",OWNO,////,////,????,"Kalumburu, Western Australia, Australia",OK
    UCB,////,////,ZBWL,UNK,1.0,Ulanqab,Ulanqab,IATA,////,////,Ulanqab Airport,"Ulanqab, Inner Mongolia, China",MAYBE
    UCC,NV11,////,KUCC,NG,0.623,Yucca Flat,"Yucca Flat (NV), USA",OWNO,YUCCA AIRSTRIP,"MERCURY, NV - UNITED STATES",Yucca Airstrip,"Mercury, Nevada, United States",OK
    UCE,4R7,////,////,OK,1.0,Eunice,"Eunice (LA), USA",OWNO,EUNICE,"EUNICE, LA - UNITED STATES",????,"Eunice, Louisiana, United States",OK
    UCK,////,////,////,UNK,1.0,Lutsk,"Lutsk, Ukraine",OWNO,////,////,????,"Lutsk, Volyn, Ukraine",OK
    UCN,////,////,GLLB,UNK,0.778,Buchanan,"Buchanan, Liberia",OWNO,////,////,LAMCO,"Buchanan, Liberia",OK
    UCT,////,////,UUYH,UNK,1.0,Ukhta,"Ukhta, Russia",OWNO,////,////,Ukhta Airport,"Ukhta, Komi, Russian Federation (Russia)",OK
    UCY,UCY,KUCY,KUCY,OK,0.874,Everett-Stewart,"Union City (TN), USA",OWNO,EVERETT-STEWART RGNL,"UNION CITY, TN - UNITED STATES",Everett-Stewart Regional,"Union City, Tennessee, United States",OK
    UDA,////,////,YUDA,UNK,1.0,Undarra,"Undarra, Australia",OWNO,////,////,????,"Undarra, Queensland, Australia",OK
    UDD,UDD,KUDD,KUDD,OK,1.0,Bermuda Dunes,"Palm Springs (CA), USA",OWNO,BERMUDA DUNES,"PALM SPRINGS, CA - UNITED STATES",Bermuda Dunes Airport,"Palm Springs, California, United States",OK
    UDE,////,////,EHVK,UNK,0.88,Volkel,"Uden, Netherlands",OWNO,////,////,Volkel AB,"Uden, Noord-Brabant (North Brabant), Netherlands",OK
    UDI,////,////,SBUL,UNK,0.571,Eduardo Gomes,"Uberlandia, Brazil",OWNO,////,////,Ten. Cel. Av. César Bombonato Airport,"Uberlândia, Minas Gerais, Brazil",OK
    UDJ,////,////,UKLU,UNK,1.0,Uzhgorod,"Uzhgorod, Ukraine",OWNO,////,////,????,"Uzhgorod, Zakarpattia, Ukraine",OK
    UDN,////,////,LIPD,UNK,0.556,Airfield,"Udine, Italy",OWNO,////,////,Campoformido Mil,"Udine, Friuli-Venezia Giulia, Italy",OK
    UDR,////,////,VAUD,UNK,0.7,Dabok,"Udaipur, India",OWNO,////,////,????,"Udaipur, Rajasthan, India",OK
    UEE,////,////,YQNS,UNK,1.0,Queenstown,"Queenstown, Australia",OWNO,////,////,????,"Queenstown, Tasmania, Australia",OK
    UEL,////,////,FQQL,UNK,1.0,Quelimane,"Quelimane, Mozambique",OWNO,////,////,????,"Quelimane, Mozambique",OK
    UEO,////,////,ROKJ,UNK,1.0,Kumejima,"Kumejima, Japan",OWNO,////,////,Kumejima Airport,"Kumejima, Kumejima Island, Okinawa, Japan",OK
    UES,UES,KUES,KUES,OK,0.701,Waukesha,"Waukesha (WI), USA",OWNO,WAUKESHA COUNTY,"WAUKESHA, WI - UNITED STATES",Waukesha County Airport,"Waukesha, Wisconsin, United States",OK
    UET,////,////,OPQT,UNK,1.0,Quetta,"Quetta, Pakistan",OWNO,////,////,????,"Quetta, Balochistan, Pakistan",OK
    UFA,////,////,UWUU,UNK,1.0,Ufa,"Ufa, Russia",OWNO,////,////,Ufa Airport,"Ufa, Bashkortostan, Russian Federation (Russia)",OK
    UGA,////,////,ZMBN,UNK,1.0,Bulgan,"Bulgan, Mongolia",OWNO,////,////,????,"Bulgan, Mongolia",OK
    UGB,UGB,////,////,OK,1.0,Ugashik Bay,"Pilot Point (AK), USA",OWNO,UGASHIK BAY,"PILOT POINT, AK - UNITED STATES",Ugashik Bay Airport,"Pilot Point, Alaska, United States",OK
    UGC,////,////,UTNU,UNK,0.737,Urgench,"Urgench, Uzbekistan",OWNO,////,////,Urgench International Airport,"Urgench, Xorazm, Uzbekistan",OK
    UGI,WSJ,////,////,OK,0.615,Uganik,"Uganik (AK), USA",OWNO,SAN JUAN /UGANIK/,"SAN JUAN, AK - UNITED STATES",San Juan SPB,"San Juan, Alaska, United States",OK
    UGN,UGN,KUGN,KUGN,OK,0.694,Memorial,"Waukegan (IL), USA",OWNO,WAUKEGAN RGNL,"CHICAGO/WAUKEGAN, - UNITED STATES",Waukegan Regional,"Chicago/Waukegan, Illinois, United States",OK
    UGO,////,////,FNUG,UNK,1.0,Uige,"Uige, Angola",OWNO,////,////,????,"Uíge, Angola",OK
    UGS,9A8,////,////,OK,1.0,Ugashik,"Ugashik (AK), USA",OWNO,UGASHIK,"UGASHIK, AK - UNITED STATES",????,"Ugashik, Alaska, United States",OK
    UGU,////,////,////,UNK,1.0,Zugapa,"Zugapa, Indonesia",OWNO,////,////,????,"Zugapa, Papua, Indonesia",OK
    UHE,////,////,LKKU,UNK,0.78,Uherske Hradiste,"Uherske Hradiste, Czech Republic",OWNO,////,////,Kunovice Airport,"Uherske Hradiste, Zlín, Czech Republic",OK
    UIB,////,////,SKUI,UNK,0.545,Quibdo,"Quibdo, Colombia",OWNO,////,////,El Caraño Airport,"Quibdó, Chocó, Colombia",OK
    UIH,////,////,VVPC,UNK,0.667,Qui Nhon,"Qui Nhon, Viet Nam",OWNO,////,////,Phù Cát,"Qui Nhon, Vietnam",OK
    UII,////,////,MHUT,UNK,1.0,Utila,"Utila, Honduras",OWNO,////,////,Útila Airport,"Útila Island, Islas de la Bahía, Honduras",MAYBE
    UIK,////,////,UIBS,UNK,1.0,Ust-Ilimsk,"Ust-Ilimsk, Russia",OWNO,////,////,????,"Ust-Ilimsk, Irkutskaya, Russian Federation (Russia)",OK
    UIL,UIL,KUIL,KUIL,OK,0.766,Quillayute State,"Quillayute (WA), USA",OWNO,QUILLAYUTE,"QUILLAYUTE, WA - UNITED STATES",????,"Quillayute, Washington, United States",OK
    UIN,UIN,KUIN,KUIN,OK,0.309,Municipal,"Quincy (IL), USA",OWNO,QUINCY RGNL-BALDWIN FIELD,"QUINCY, IL - UNITED STATES",Quincy Regional-Baldwin Field,"Quincy, Illinois, United States",OK
    UIO,////,////,SEQM,UNK,0.525,Mariscal Sucre International Airport,"Quito, Ecuador",OWNO,////,////,Mitad del Mundo International Airport,"Quito, Pichincha, Ecuador",OK
    UIP,////,////,LFRQ,UNK,1.0,Pluguffan,"Quimper, France",OWNO,////,////,Pluguffan,"Quimper, Bretagne, France",OK
    UIQ,////,////,NVVQ,UNK,1.0,Quine Hill,"Quine Hill, Vanuatu",OWNO,////,////,????,"Quion Hill, Éfaté Island, Shéfa, Vanuatu",OK
    UIR,////,////,YQDI,UNK,1.0,Quirindi,"Quirindi, Australia",OWNO,////,////,????,"Quirindi, New South Wales, Australia",OK
    UIT,N55,////,////,OK,0.399,Jaluit Island,"Jaluit Island, Marshall Islands",OWNO,JALUIT,"JABOR JALUIT ATOLL, - MARSHALL ISLANDS",Jaluit Airport,"Jabor Jaluit Atoll, Marshall Islands",MAYBE
    UJE,////,////,UJAP,UNK,0.571,Ujae Island,"Ujae Island, Marshall Islands",OWNO,////,////,????,"Ujae Atoll, Marshall Islands",MAYBE
    UKA,////,////,HKKA,UNK,1.0,Ukunda,"Ukunda, Kenya",OWNO,////,////,????,"Ukunda, Kenya",OK
    UKB,////,////,RJBE,UNK,1.0,Kobe,"Kobe, Japan",OWNO,////,////,Kobe Airport,"Kobe, Hyogo, Japan",OK
    UKG,////,////,UEBT,UNK,nan,////,////,////,////,////,Ust'-Kujga Airport,"Ust'-Kujga, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    UKI,UKI,KUKI,KUKI,OK,0.748,Ukiah,"Ukiah (CA), USA",OWNO,UKIAH MUNI,"UKIAH, CA - UNITED STATES",Ukiah Municipal Airport,"Ukiah, California, United States",OK
    UKK,////,////,UASK,UNK,0.514,Ust-Kamenogorsk,"Ust-Kamenogorsk, Kazakhstan",OWNO,////,////,Oskemen Airport,"Oskemen, Shyghys Qazaqstan, Kazakhstan",TO DO CHECK
    UKN,Y01,////,////,OK,1.0,Municipal,"Waukon IA, USA",OWNO,WAUKON MUNI,"WAUKON, IA - UNITED STATES",Waukon Municipal Airport,"Waukon, Iowa, United States",OK
    UKR,////,////,////,UNK,1.0,Mukeiras,"Mukeiras, Yemen",OWNO,////,////,????,"Mukeiras, Yemen",OK
    UKS,////,////,UKFB,UNK,nan,////,////,////,////,////,Sevastopol International Airport,"Sevastopol, Crimea, Ukraine",UNK
    UKT,UKT,KUKT,KUKT,OK,0.669,Upper Bucks,"Quakertown (PA), USA",OWNO,QUAKERTOWN,"QUAKERTOWN, PA - UNITED STATES",????,"Quakertown, Pennsylvania, United States",OK
    UKU,////,////,////,UNK,1.0,Nuku,"Nuku, Papua New Guinea",OWNO,////,////,????,"Nuku, Sandaun, Papua-New Guinea",OK
    UKX,////,////,UITT,UNK,1.0,Ust-Kut,"Ust-Kut, Russia",OWNO,////,////,Ust-Kut Airport,"Ust-Kut, Irkutskaya, Russian Federation (Russia)",OK
    UKY,////,////,////,UNK,1.0,Kyoto,"Kyoto, Japan",OWNO,////,////,????,"Kyoto, Kyoto, Japan",OK
    ULA,////,////,SAWJ,UNK,0.5,San Julian,"San Julian, Argentina",OWNO,////,////,Capitan Jose D. Vasquez,"San Julian, Santa Cruz, Argentina",OK
    ULB,////,////,NVSU,UNK,nan,////,////,////,////,////,????,"Uléi, Ambryn Island, Malampa, Vanuatu",UNK
    ULD,////,////,FAUL,UNK,1.0,Ulundi,"Ulundi, South Africa",OWNO,////,////,Ulundi Airport,"Ulundi, KwaZulu-Natal, South Africa",OK
    ULE,////,////,////,UNK,1.0,Sule,"Sule, Papua New Guinea",OWNO,////,////,????,"Sule, West New Britain, Papua-New Guinea",OK
    ULG,////,////,ZMUL,UNK,0.6,Ulgit,"Ulgit, Mongolia",OWNO,////,////,????,"Ölgii, Bayan-Ölgii, Mongolia",TO DO CHECK
    ULH,////,////,OEAO,UNK,nan,////,////,////,////,////,Prince Abdul Majeed bin Abdulaziz Airport,"Al-Ula, Saudi Arabia",UNK
    ULI,TT02,////,////,OK,0.445,Ulithi,"Ulithi, Micronesia",OWNO,ULITHI,"ULITHI, - MICRONESIA, FED STATES OF",????,"Falalop Island, Ulithi Atoll, Yap, Micronesia (Federated States of)",MAYBE
    ULK,////,////,UERL,UNK,nan,////,////,////,////,////,Lensk Airport,"Lensk, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    ULL,////,////,////,UNK,0.25,Mull,"Mull, United Kingdom",OWNO,////,////,Glenforsa Airfield,"Glenforsa, Aros, Isle of Mull, Scotland, United Kingdom",MAYBE
    ULM,ULM,KULM,KULM,OK,0.76,New Ulm,"New Ulm (MN), USA",OWNO,NEW ULM MUNI,"NEW ULM, MN - UNITED STATES",New Ulm Municipal Airport,"New Ulm, Minnesota, United States",OK
    ULN,////,////,ZMUB,UNK,0.5,Buyant Uhaa,"Ulaanbaatar, Mongolia",OWNO,////,////,Chinggis Khaan,"Ulan Bator, Mongolia",OK
    ULO,////,////,ZMUG,UNK,1.0,Ulaangom,"Ulaangom, Mongolia",OWNO,////,////,????,"Ulaangom, Mongolia",OK
    ULP,////,////,YQLP,UNK,1.0,Quilpie,"Quilpie, Australia",OWNO,////,////,????,"Quilpie, Queensland, Australia",OK
    ULQ,////,////,SKUL,UNK,0.462,Farfan,"Tulua, Colombia",OWNO,////,////,Heriberto Gíl Martínez Airport,"Tuluá, Valle del Cauca, Colombia",OK
    ULS,////,////,////,UNK,1.0,Mulatos,"Mulatos, Colombia",OWNO,////,////,????,"Mulatos, Antioquia, Colombia",OK
    ULU,////,////,HUGU,UNK,1.0,Gulu,"Gulu, Uganda",OWNO,////,////,????,"Gulu, Uganda",OK
    ULV,////,////,UWLL,UNK,nan,////,////,////,////,////,Baratayevka Airport,"Ulyanovsk, Ul'yanovskaya, Russian Federation (Russia)",UNK
    ULX,////,////,////,UNK,1.0,Ulusaba,"Ulusaba, South Africa",OWNO,////,////,????,"Ulusaba, Mpumalanga, South Africa",OK
    ULY,////,////,UWLW,UNK,0.643,Ulyanovsk,"Ulyanovsk, Russia",OWNO,////,////,Vostochny Airport,"Ulyanovsk, Ul'yanovskaya, Russian Federation (Russia)",OK
    ULZ,////,////,////,UNK,0.593,Uliastai,"Uliastai, Mongolia",OWNO,////,////,Jibhalanta,"Uliastai, Mongolia",OK
    UMA,////,////,MUMA,UNK,1.0,Punta De Maisi,"Punta De Maisi, Cuba",OWNO,////,////,Punta de Maisi Airport,"Punta de Maisi, Guantánamo, Cuba",OK
    UMC,////,////,////,UNK,1.0,Umba,"Umba, Papua New Guinea",OWNO,////,////,????,"Umba, Morobe, Papua-New Guinea",OK
    UMD,////,////,BGUM,UNK,1.0,Uummannaq,"Uummannaq, Greenland",OWNO,////,////,????,"Uummannaq, Qaasuitsup, Greenland",OK
    UME,////,////,ESNU,UNK,1.0,Umea,"Umea, Sweden",OWNO,////,////,????,"Umeå, Västerbottens län, Sweden",OK
    UMI,////,////,SPIL,UNK,0.8,Quincemil,"Quincemil, Peru",OWNO,////,////,????,"Quince, Cuzco, Perú",TO DO CHECK
    UMM,UMM,PAST,PAST,OK,1.0,Summit,"Summit (AK), USA",OWNO,SUMMIT,"SUMMIT, AK - UNITED STATES",????,"Summit, Alaska, United States",OK
    UMR,////,////,YPWR,UNK,1.0,Woomera,"Woomera, Australia",OWNO,////,////,????,"Woomera, South Australia, Australia",OK
    UMS,////,////,UEMU,UNK,nan,////,////,////,////,////,Ust'-Maya Airport,"Ust'-Maya, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    UMT,UMT,PAUM,PAUM,OK,1.0,Umiat,"Umiat (AK), USA",OWNO,UMIAT,"UMIAT, AK - UNITED STATES",????,"Umiat, Alaska, United States",OK
    UMU,////,////,SSUM,UNK,0.579,Ernesto Geisel,"Umuarama, Brazil",OWNO,////,////,????,"Umuarama, Paraná, Brazil",OK
    UMY,////,////,UKHS,UNK,1.0,Sumy,"Sumy, Ukraine",OWNO,////,////,????,"Sumy, Sumy, Ukraine",OK
    UNA,////,////,SBTC,UNK,1.0,Una,"Una, Brazil",OWNO,////,////,????,"Una, Bahia, Brazil",OK
    UNC,////,////,////,UNK,1.0,Unguia,"Unguia, Colombia",OWNO,////,////,????,"Unguia, Chocó, Colombia",OK
    UND,////,////,OAUZ,UNK,1.0,Kunduz Airport,"Kunduz, Afghanistan",OWNO,////,////,????,"Kunduz, Kunduz, Afghanistan",OK
    UNE,////,////,FXQN,UNK,1.0,Qachas Nek,"Qachas Nek, Lesotho",OWNO,////,////,????,"Qacha's Nek, Lesotho",OK
    UNG,////,////,AYKI,UNK,1.0,Kiunga,"Kiunga, Papua New Guinea",OWNO,////,////,????,"Kiunga, Western, Papua-New Guinea",OK
    UNI,////,////,TVSU,UNK,0.833,Union Island,"Union Island, Saint Vincent and the Grenadines",OWNO,////,////,Union Island International Airport,"Union Island, Saint Vincent and The Grenadines",OK
    UNK,UNK,PAUN,PAUN,OK,1.0,Unalakleet,"Unalakleet (AK), USA",OWNO,UNALAKLEET,"UNALAKLEET, AK - UNITED STATES",????,"Unalakleet, Alaska, United States",OK
    UNN,////,////,VTSR,UNK,1.0,Ranong,"Ranong, Thailand",OWNO,////,////,????,"Ranong, Ranong, Thailand",OK
    UNR,////,////,ZMUH,UNK,0.875,Underkhaan,"Underkhaan, Mongolia",OWNO,////,////,????,"Öndörkhaan, Mongolia",TO DO CHECK
    UNT,////,////,EGPW,UNK,1.0,Baltasound,"Unst Shetland Is, United Kingdom",OWNO,////,////,Baltasound,"Unst, Shetland Islands, Scotland, United Kingdom",OK
    UNU,UNU,KUNU,KUNU,OK,1.0,Dodge County,"Juneau (WI), USA",OWNO,DODGE COUNTY,"JUNEAU, WI - UNITED STATES",Dodge County Airport,"Juneau, Wisconsin, United States",OK
    UOA,////,////,NTTX,UNK,nan,////,////,////,////,////,Mururoa Airstrip,"Mururoa Atoll, French Polynesia",UNK
    UOL,////,////,////,UNK,1.0,Buol,"Buol, Indonesia",OWNO,////,////,????,"Buol, Sulawesi Tengah, Indonesia",OK
    UOS,UOS,KUOS,KUOS,OK,1.0,Franklin County,"Sewanee (TN), USA",OWNO,FRANKLIN COUNTY,"SEWANEE, TN - UNITED STATES",Franklin County Airport,"Sewanee, Tennessee, United States",OK
    UOX,UOX,KUOX,KUOX,OK,1.0,University-Oxford,"Oxford (MS), USA",OWNO,UNIVERSITY-OXFORD,"OXFORD, MS - UNITED STATES",University-Oxford Airport,"Oxford, Mississippi, United States",OK

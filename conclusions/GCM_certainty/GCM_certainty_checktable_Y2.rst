
List for checking certainty of Great Circle Mapper (YGK - YNO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    YGK,////,////,CYGK,UNK,1.0,Kingston,"Kingston, Canada",OWNO,////,////,Kingston/Norman Rogers Airport,"Kingston, Ontario, Canada",OK
    YGL,////,////,CYGL,UNK,0.7,La Grande,"La Grande, Canada",OWNO,////,////,Rivière Airport,"La Grande, Québec, Canada",OK
    YGM,////,////,CYGM,UNK,0.385,Gimli,"Gimli, Canada",OWNO,////,////,Gimli Industrial Park Airport,"Gimli, Manitoba, Canada",OK
    YGN,////,////,////,UNK,0.875,Greenway Sound,"Greenway Sound, Canada",OWNO,////,////,SPB,"Greenway Sound, British Columbia, Canada",OK
    YGO,////,////,CYGO,UNK,1.0,Gods Narrows,"Gods Narrows, Canada",OWNO,////,////,????,"God's Narrows, Manitoba, Canada",OK
    YGP,////,////,CYGP,UNK,0.467,Metropolitan Area,"Gaspe, Canada",OWNO,////,////,????,"Gaspé, Québec, Canada",OK
    YGQ,////,////,CYGQ,UNK,1.0,Geraldton,"Geraldton, Canada",OWNO,////,////,????,"Geraldton, Ontario, Canada",OK
    YGR,////,////,CYGR,UNK,1.0,Iles De La Madeleine,"Iles De La Madeleine, Canada",OWNO,////,////,????,"Îles de la Madeleine, Québec, Canada",OK
    YGT,////,////,CYGT,UNK,1.0,Igloolik,"Igloolik, Canada",OWNO,////,////,????,"Igloolik, Qikiqtaaluk, Northwest Territories, Canada",OK
    YGV,////,////,CYGV,UNK,1.0,Havre St Pierre,"Havre St Pierre, Canada",OWNO,////,////,????,"Havre St-Pierre, Québec, Canada",OK
    YGW,////,////,CYGW,UNK,1.0,Kuujjuarapik,"Kuujjuarapik, Canada",OWNO,////,////,????,"Kuujjuarapik, Québec, Canada",OK
    YGX,////,////,CYGX,UNK,1.0,Gillam,"Gillam, Canada",OWNO,////,////,????,"Gillam, Manitoba, Canada",OK
    YGZ,////,////,CYGZ,UNK,1.0,Grise Fiord,"Grise Fiord, Canada",OWNO,////,////,Grise Fiord Airport,"Grise Fiord, Nunavut, Canada",OK
    YHA,////,////,////,UNK,1.0,Port Hope Simpson,"Port Hope Simpson, Canada",OWNO,////,////,????,"Port Hope Simpson, Newfoundland and Labrador, Canada",OK
    YHB,////,////,CYHB,UNK,1.0,Hudson Bay,"Hudson Bay, Canada",OWNO,////,////,????,"Hudson Bay, Saskatchewan, Canada",OK
    YHC,////,////,////,UNK,1.0,Hakai Pass,"Hakai Pass, Canada",OWNO,////,////,????,"Hakai Pass, British Columbia, Canada",OK
    YHD,////,////,CYHD,UNK,0.778,Municipal,"Dryden, Canada",OWNO,////,////,Regional,"Dryden, Ontario, Canada",OK
    YHE,////,////,CYHE,UNK,1.0,Hope,"Hope, Canada",OWNO,////,////,Hope Aerodrome,"Hope, British Columbia, Canada",OK
    YHF,////,////,CYHF,UNK,0.769,Hearst,"Hearst, Canada",OWNO,////,////,Hearst (René Fontaine) Municipal,"Hearst, Ontario, Canada",OK
    YHG,////,////,////,UNK,1.0,Charlottetown,"Charlottetown, Canada",OWNO,////,////,????,"Charlottetown, Newfoundland and Labrador, Canada",OK
    YHI,////,////,CYHI,UNK,1.0,Holman,"Holman, Canada",OWNO,////,////,Holman,"Ulukhaktok, Northwest Territories, Canada",OK
    YHK,////,////,CYHK,UNK,1.0,Gjoa Haven,"Gjoa Haven, Canada",OWNO,////,////,????,"Gjoa Haven, Kitikmeot, Nunavut, Canada",OK
    YHM,////,////,CYHM,UNK,0.538,Hamilton,"Hamilton, Canada",OWNO,////,////,John C. Munro Hamilton International Airport,"Hamilton, Ontario, Canada",OK
    YHN,////,////,CYHN,UNK,0.8,Hornepayne,"Hornepayne, Canada",OWNO,////,////,Municipal,"Hornepayne, Ontario, Canada",OK
    YHO,////,////,CYHO,UNK,1.0,Hopedale,"Hopedale, Canada",OWNO,////,////,????,"Hopedale, Newfoundland and Labrador, Canada",OK
    YHP,////,////,////,UNK,1.0,Poplar Hill,"Poplar Hill, Canada",OWNO,////,////,????,"Poplar Hill, Ontario, Canada",OK
    YHR,////,////,CYHR,UNK,0.2,Chevery,"Chevery, Canada",OWNO,////,////,????,"Harrington Harbour, Québec, Canada",TO DO CHECK
    YHS,////,////,////,UNK,0.364,Sechelt,"Sechelt, Canada",OWNO,////,////,Sunshine Coast Regional Airport,"Sechelt-Gibsons, British Columbia, Canada",TO DO CHECK
    YHT,////,CYHT,CYHT,OK,1.0,Haines Junction,"Haines Junction, Canada",OWNO,HAINES JUNCTION,"HAINES JUNCTION, - CANADA",Haines Junction Airport,"Haines Junction, Yukon, Canada",OK
    YHU,////,////,CYHU,UNK,1.0,St Hubert,"Montreal, Canada",OWNO,////,////,St. Hubert,"Montréal, Québec, Canada",OK
    YHY,////,////,CYHY,UNK,1.0,Hay River,"Hay River, Canada",OWNO,////,////,????,"Hay River, Northwest Territories, Canada",OK
    YHZ,////,////,CYHZ,UNK,0.611,Halifax International,"Halifax, Canada",OWNO,////,////,Robert L. Stanfield International Airport,"Halifax, Nova Scotia, Canada",OK
    YIB,////,CYIB,CYIB,OK,0.79,Atikokan,"Atikokan, Canada",OWNO,ATIKOKAN MUNI,"ATIKOKAN, - CANADA",Municipal,"Atikokan, Ontario, Canada",OK
    YIC,////,////,ZSYC,UNK,nan,////,////,////,////,////,Mingyueshan Airport,"Yichun, Jiangxi, China",UNK
    YIE,////,////,ZBES,UNK,nan,////,////,////,////,////,Yiershi Airport,"Aershan, Inner Mongolia, China",UNK
    YIF,////,////,CYIF,UNK,0.625,Pakuashipi,"Pakuashipi, Canada",OWNO,////,////,St. Augustin,"Pakuashipi, Québec, Canada",OK
    YIG,////,////,////,UNK,0.741,Big Bay Marina,"Big Bay Marina, Canada",OWNO,////,////,Big Bay Water Aerodrome,"Stuart Island, British Columbia, Canada",MAYBE
    YIH,////,////,ZHYC,UNK,1.0,Yichang,"Yichang, PR China",OWNO,////,////,????,"Yichang, Hubei, China",OK
    YIK,////,////,CYIK,UNK,1.0,Ivujivik,"Ivujivik, Canada",OWNO,////,////,????,"Ivujivik, Québec, Canada",OK
    YIN,////,////,ZWYN,UNK,1.0,Yining,"Yining, PR China",OWNO,////,////,????,"Yining, Xinjiang, China",OK
    YIO,////,////,CYIO,UNK,1.0,Pond Inlet,"Pond Inlet, Canada",OWNO,////,////,????,"Pond Inlet, Nunavut, Canada",OK
    YIP,YIP,KYIP,KYIP,OK,1.0,Willow Run,"Detroit (MI), , USA",OWNO,WILLOW RUN,"DETROIT, MI - UNITED STATES",Willow Run Airport,"Detroit, Michigan, United States",OK
    YIV,////,////,CYIV,UNK,1.0,Island Lake Airport,"Island Lake /Garden Hill, Canada",OWNO,////,////,????,"Island Lake, Manitoba, Canada",OK
    YIW,////,////,ZSYW,UNK,1.0,Yiwu,"Yiwu, PR China",OWNO,////,////,????,"Yiwu, Zhejiang, China",OK
    YJA,////,////,CYJA,UNK,0.529,Metropolitan Area,"Jasper, Canada",OWNO,////,////,Jasper Airport,"Jasper, Alberta, Canada",OK
    YJF,////,////,CYJF,UNK,1.0,Fort Liard,"Fort Liard, Canada",OWNO,////,////,????,"Fort Liard, Northwest Territories, Canada",OK
    YJM,////,////,CYJM,UNK,nan,////,////,////,////,////,Perison,"Fort St. James, British Columbia, Canada",UNK
    YJN,////,CYJN,CYJN,OK,1.0,St Jean,"St Jean, Canada",OWNO,ST JEAN,"ST JEAN, - CANADA",????,"St. Jean, Québec, Canada",OK
    YJS,////,////,ZKSE,UNK,0.875,Samjiyon,Samjiyon,IATA,////,////,Samjiyon AB,"Samjiyon, Ryanggang, Democratic People's Republic of Korea (North Korea)",MAYBE
    YJT,////,////,CYJT,UNK,1.0,Stephenville,"Stephenville, Canada",OWNO,////,////,????,"Stephenville, Newfoundland and Labrador, Canada",OK
    YKA,////,////,CYKA,UNK,1.0,Kamloops,"Kamloops, Canada",OWNO,////,////,????,"Kamloops, British Columbia, Canada",OK
    YKC,////,////,CYKC,UNK,1.0,Collins Bay,"Collins Bay, Canada",OWNO,////,////,????,"Collins Bay, Saskatchewan, Canada",OK
    YKD,////,////,////,UNK,0.842,Township Airport,"Kincardine, Canada",OWNO,////,////,Kincardine Town and Township Airport,"Kincardine, Ontario, Canada",OK
    YKE,////,////,////,UNK,1.0,Knee Lake,"Knee Lake, Canada",OWNO,////,////,????,"Knee Lake, Manitoba, Canada",OK
    YKF,////,////,CYKF,UNK,0.652,Region of Waterloo International Airport,"Woolwich, Canada",OWNO,////,////,Kitchener-Waterloo Regional Airport,"Kitchener, Ontario, Canada",TO DO CHECK
    YKG,////,////,CYAS,UNK,1.0,Kangirsuk,"Kangirsuk, Canada",OWNO,////,////,????,"Kangirsuk, Québec, Canada",OK
    YKH,////,////,ZYYK,UNK,nan,////,////,////,////,////,Yingkou Lanqi Airport,"Yingkou, Liaoning, China",UNK
    YKJ,////,////,CYKJ,UNK,1.0,Key Lake,"Key Lake, Canada",OWNO,////,////,Key Lake Airport,"Key Lake, Saskatchewan, Canada",OK
    YKK,////,////,////,UNK,0.778,Kitkatla,"Kitkatla, Canada",OWNO,////,////,Kitkatla Water Aerodrome,"Kitkatla, Dolphin Island, British Columbia, Canada",OK
    YKL,////,////,CYKL,UNK,1.0,Schefferville,"Schefferville, Canada",OWNO,////,////,????,"Schefferville, Québec, Canada",OK
    YKM,YKM,KYKM,KYKM,OK,1.0,Yakima Air Terminal,"Yakima (WA), USA",OWNO,YAKIMA AIR TERMINAL/MCALLISTER FIELD,"YAKIMA, WA - UNITED STATES",Yakima Air Terminal/McAllister Field,"Yakima, Washington, United States",OK
    YKN,YKN,KYKN,KYKN,OK,0.861,Chan Gurney,"Yankton (SD), USA",OWNO,CHAN GURNEY MUNI,"YANKTON, SD - UNITED STATES",Chan Gurney Municipal Airport,"Yankton, South Dakota, United States",OK
    YKO,////,////,LTCW,UNK,nan,////,////,////,////,////,Hakkari Selahaddin Eyyubi Airport,"Hakkari, Hakkâri, Turkey",UNK
    YKQ,////,////,CYKQ,UNK,1.0,Waskaganish,"Waskaganish, Canada",OWNO,////,////,????,"Waskaganish, Québec, Canada",OK
    YKS,////,////,UEEE,UNK,1.0,Yakutsk,"Yakutsk, Russia",OWNO,////,////,Yakutsk Airport,"Yakutsk, Sakha (Yakutiya), Russian Federation (Russia)",OK
    YKT,////,////,////,UNK,1.0,Klemtu,"Klemtu, Canada",OWNO,////,////,????,"Klemtu, British Columbia, Canada",OK
    YKU,////,////,////,UNK,1.0,Chisasibi,"Chisasibi, Canada",OWNO,////,////,????,"Chisasibi, Québec, Canada",OK
    YKX,////,////,CYKX,UNK,1.0,Kirkland Lake,"Kirkland Lake, Canada",OWNO,////,////,????,"Kirkland Lake, Ontario, Canada",OK
    YKY,////,////,CYKY,UNK,1.0,Kindersley,"Kindersley, Canada",OWNO,////,////,????,"Kindersley, Saskatchewan, Canada",OK
    YKZ,////,////,CYKZ,UNK,0.88,Buttonville Municipal Airport,"Toronto (Buttonville), Canada",OWNO,////,////,Buttonville,"Toronto, Ontario, Canada",OK
    YLA,////,////,CWJU,UNK,1.0,Langara,"Langara, Canada",OWNO,////,////,????,"Langara, British Columbia, Canada",OK
    YLB,////,////,CYLB,UNK,0.875,Lac Biche,"Lac Biche, Canada",OWNO,////,////,????,"Lac La Biche, Alberta, Canada",MAYBE
    YLC,////,////,CYLC,UNK,1.0,Kimmirut,"Kimmirut/Lake Harbour, Canada",OWNO,////,////,????,"Kimmirut, Nunavut, Canada",OK
    YLD,////,////,CYLD,UNK,1.0,Chapleau,"Chapleau, Canada",OWNO,////,////,????,"Chapleau, Ontario, Canada",OK
    YLE,////,////,////,UNK,0.545,Wha Ti,"Wha Ti/Lac La Martre, Canada",OWNO,////,////,Whatì Airport,"Whatì, Northwest Territories, Canada",OK
    YLG,////,////,YYAL,UNK,1.0,Yalgoo,"Yalgoo, Australia",OWNO,////,////,????,"Yalgoo, Western Australia, Australia",OK
    YLH,////,////,CYLH,UNK,1.0,Lansdowne House,"Lansdowne House, Canada",OWNO,////,////,????,"Lansdowne House, Ontario, Canada",OK
    YLI,////,////,EFYL,UNK,1.0,Ylivieska,"Ylivieska, Finland",OWNO,////,////,????,"Ylivieska, Pohjois-Pohjanmaa (Norra Österbotten (Northern Ostrobothnia)), Finland",OK
    YLJ,////,////,CYLJ,UNK,1.0,Meadow Lake,"Meadow Lake, Canada",OWNO,////,////,????,"Meadow Lake, Saskatchewan, Canada",OK
    YLL,////,////,CYLL,UNK,1.0,Lloydminster,"Lloydminster, Canada",OWNO,////,////,????,"Lloydminster, Alberta, Canada",OK
    YLM,////,////,////,UNK,1.0,Clinton Creek,"Clinton Creek, Canada",OWNO,////,////,Clinton Creek Airport,"Clinton Creek, Yukon, Canada",OK
    YLN,////,////,ZYYL,UNK,1.0,Yilan,"Yilan, PR China",OWNO,////,////,????,"Yilan, Heilongjiang, China",OK
    YLP,////,////,CYLP,UNK,1.0,Mingan,"Mingan, Canada",OWNO,////,////,????,"Mingan, Québec, Canada",OK
    YLQ,////,////,CYLQ,UNK,1.0,La Tuque,"La Tuque, Canada",OWNO,////,////,????,"La Tuque, Québec, Canada",OK
    YLR,////,////,CYLR,UNK,1.0,Leaf Rapids,"Leaf Rapids, Canada",OWNO,////,////,????,"Leaf Rapids, Manitoba, Canada",OK
    YLS,////,////,////,UNK,1.0,Lebel-Sur-Quevillon,"Lebel-Sur-Quevillon, Canada",OWNO,////,////,????,"Lebel-sur-Quévillon, Québec, Canada",OK
    YLT,////,////,CYLT,UNK,1.0,Alert,"Alert, Canada",OWNO,////,////,????,"Alert, Nunavut, Canada",OK
    YLW,////,////,CYLW,UNK,1.0,Kelowna,"Kelowna, Canada",OWNO,////,////,????,"Kelowna, British Columbia, Canada",OK
    YLY,////,////,CYNJ,UNK,nan,////,////,////,////,////,Langley Regional Airport,"Langley, British Columbia, Canada",UNK
    YMA,////,////,CYMA,UNK,1.0,Mayo,"Mayo, Canada",OWNO,////,////,????,"Mayo, Yukon, Canada",OK
    YMB,////,////,////,UNK,1.0,Merritt,"Merritt, Canada",OWNO,////,////,????,"Merritt, British Columbia, Canada",OK
    YMD,////,////,CYMD,UNK,1.0,Mould Bay,"Mould Bay, Canada",OWNO,////,////,????,"Mould Bay, Prince Patrick Island, Northwest Territories, Canada",OK
    YME,////,////,CYME,UNK,1.0,Matane,"Matane, Canada",OWNO,////,////,????,"Matane, Québec, Canada",OK
    YMF,////,////,////,UNK,1.0,Montagne Harbor,"Montagne Harbor, Canada",OWNO,////,////,????,"Montagne Harbor, British Columbia, Canada",OK
    YMG,////,////,CYMG,UNK,1.0,Manitouwadge,"Manitouwadge, Canada",OWNO,////,////,????,"Manitouwadge, Ontario, Canada",OK
    YMH,////,////,CYMH,UNK,1.0,Mary's Harbour,"Mary's Harbour, Canada",OWNO,////,////,????,"Mary's Harbour, Newfoundland and Labrador, Canada",OK
    YMJ,////,////,CYMJ,UNK,1.0,Moose Jaw,"Moose Jaw, Canada",OWNO,////,////,????,"Moose Jaw, Saskatchewan, Canada",OK
    YMK,////,////,USDK,UNK,nan,////,////,////,////,////,My's Kamenny'j Airport,"My's Kamenny'j, Yamalo-Nenetskiy, Russian Federation (Russia)",UNK
    YML,////,////,CYML,UNK,1.0,Charlevoix,"Murray Bay, Canada",OWNO,////,////,Charlevoix,"La Malbaie, Québec, Canada",OK
    YMM,////,////,CYMM,UNK,1.0,Fort Mcmurray,"Fort Mcmurray, Canada",OWNO,////,////,????,"Fort McMurray, Alberta, Canada",OK
    YMN,////,////,CYFT,UNK,1.0,Makkovik,"Makkovik, Canada",OWNO,////,////,Makkovik Airport,"Makkovik, Newfoundland and Labrador, Canada",OK
    YMO,////,////,CYMO,UNK,1.0,Moosonee,"Moosonee, Canada",OWNO,////,////,????,"Moosonee, Ontario, Canada",OK
    YMP,////,////,////,UNK,1.0,Port McNeil,"Port McNeil, Canada",OWNO,////,////,Port McNeill Airport,"Port McNeill, British Columbia, Canada",OK
    YMQ,////,////,////,UNK,1.0,Metropolitan Area,"Montreal, Canada",OWNO,////,////,Metropolitan Area,"Montréal, Québec, Canada",OK
    YMS,////,////,SPMS,UNK,0.444,Yurimaguas,"Yurimaguas, Peru",OWNO,////,////,Moisés Benzaquén Rengifo Airport,"Yurimaguas, Loreto, Perú",OK
    YMT,////,////,CYMT,UNK,0.778,Chibougamau,"Chibougamau, Canada",OWNO,////,////,Chapais,"Chibougamau, Québec, Canada",OK
    YMW,////,////,CYMW,UNK,1.0,Maniwaki,"Maniwaki, Canada",OWNO,////,////,????,"Maniwaki, Québec, Canada",OK
    YMX,////,////,CYMX,UNK,0.865,Mirabel,"Montreal, Canada",OWNO,////,////,Montréal-Mirabel International Airport,"Montréal, Québec, Canada",OK
    YMY,////,////,////,UNK,0.955,Downtown Rail Station,"Montreal, Canada",OWNO,////,////,Downtown Railway Station,"Montréal, Québec, Canada",OK
    YNA,////,////,CYNA,UNK,1.0,Natashquan,"Natashquan, Canada",OWNO,////,////,????,"Natashquan, Québec, Canada",OK
    YNB,////,////,OEYN,UNK,0.235,Yanbu,"Yanbu, Saudi Arabia",OWNO,////,////,Prince Abdul Mohsin bin Abdulaziz Airport,"Yenbo, Saudi Arabia",OK
    YNC,////,////,CYNC,UNK,1.0,Wemindji,"Wemindji, Canada",OWNO,////,////,????,"Wemindji, Québec, Canada",OK
    YND,////,CYND,CYND,OK,1.0,Gatineau,"Ottawa, Canada",OWNO,OTTAWA/GATINEAU,"OTTAWA, - CANADA",Gatineau,"Ottawa, Québec, Canada",OK
    YNE,////,////,CYNE,UNK,1.0,Norway House,"Norway House, Canada",OWNO,////,////,????,"Norway House, Manitoba, Canada",OK
    YNG,YNG,KYNG,KYNG,OK,0.51,Youngstown,"Youngstown (OH), USA",OWNO,YOUNGSTOWN-WARREN RGNL,"YOUNGSTOWN/WARREN, OH - UNITED STATES",Youngstown-Warren Regional,"Youngstown/Warren, Ohio, United States",OK
    YNH,////,////,CYNH,UNK,1.0,Hudson Hope,"Hudson's Hope, Canada",OWNO,////,////,????,"Hudson's Hope, British Columbia, Canada",OK
    YNJ,////,////,ZYYJ,UNK,0.64,Yanji,"Yanji, PR China",OWNO,////,////,Yanji Chaoyangchuan Airport,"Yanji, Jilin, China",OK
    YNL,////,////,CYNL,UNK,1.0,Points North Landing,"Points North Landing, Canada",OWNO,////,////,Points North Landing Airport,"Points North Landing, Saskatchewan, Canada",OK
    YNM,////,////,CYNM,UNK,1.0,Matagami,"Matagami, Canada",OWNO,////,////,????,"Matagami, Québec, Canada",OK
    YNO,////,////,////,UNK,1.0,North Spirit Lake,"North Spirit Lake, Canada",OWNO,////,////,????,"North Spirit Lake, Ontario, Canada",OK

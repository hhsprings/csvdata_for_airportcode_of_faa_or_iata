
List for checking certainty of Great Circle Mapper (ZAA - ZMH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ZAA,////,////,////,UNK,1.0,Alice Arm,"Alice Arm, Canada",OWNO,////,////,????,"Alice Arm, British Columbia, Canada",OK
    ZAC,////,////,CZAC,UNK,1.0,York Landing,"York Landing, Canada",OWNO,////,////,????,"York Landing, Manitoba, Canada",OK
    ZAD,////,////,LDZD,UNK,1.0,Zadar,"Zadar, Croatia",OWNO,////,////,????,"Zadar, Croatia",OK
    ZAG,////,////,LDZA,UNK,0.696,Pleso,"Zagreb, Croatia",OWNO,////,////,Zagreb International Airport,"Zagreb, Croatia",OK
    ZAH,////,////,OIZH,UNK,0.737,Zahedan,"Zahedan, Iran",OWNO,////,////,Zahedan International Airport,"Zahedan, Sistan va Baluchestan, Iran",OK
    ZAJ,////,////,OAZJ,UNK,1.0,Zaranj,"Zaranj, Afghanistan",OWNO,////,////,????,"Zaranj, Nimruz, Afghanistan",OK
    ZAL,////,////,SCVD,UNK,1.0,Pichoy,"Valdivia, Chile",OWNO,////,////,Pichoy Airport,"Valdivia, Los Ríos, Chile",OK
    ZAM,////,////,RPMZ,UNK,1.0,Zamboanga International Airport,"Zamboanga, Philippines",OWNO,////,////,Zamboanga International Airport,"Zamboanga, Philippines",OK
    ZAO,////,////,LFCC,UNK,0.788,Laberandie,"Cahors, France",OWNO,////,////,Lalbenque,"Cahors, Midi-Pyrénées, France",OK
    ZAR,////,////,DNZA,UNK,1.0,Zaria,"Zaria, Nigeria",OWNO,////,////,Zaria Airport,"Zaria, Kaduna, Nigeria",OK
    ZAT,////,////,ZPZT,UNK,1.0,Zhaotong,"Zhaotong, PR China",OWNO,////,////,????,"Zhaotong, Yunnan, China",OK
    ZAZ,////,////,LEZG,UNK,1.0,Zaragoza,"Zaragoza, Spain",OWNO,////,////,Zaragoza Airport,"Zaragoza, Aragón, Spain",OK
    ZBE,////,////,LKZA,UNK,1.0,Dolni Benesov,"Zabreh, Czech Republic",OWNO,////,////,Dolní Benesov,"Zábreh, Moravia-Silesia, Czech Republic",OK
    ZBF,////,////,CZBF,UNK,1.0,Bathurst,"Bathurst, Canada",OWNO,////,////,????,"Bathurst, New Brunswick, Canada",OK
    ZBK,////,////,////,UNK,1.0,Zabljak,"Zabljak, Montenegro",OWNO,////,////,????,"Zabljak, Yugoslavia (Serbia and Montenegro)",OK
    ZBL,////,////,////,UNK,0.538,Biloela,"Biloela, Australia",OWNO,////,////,Bus Station,"Biloela, Queensland, Australia",OK
    ZBM,////,////,CZBM,UNK,1.0,Bromont,"Bromont, Canada",OWNO,////,////,????,"Bromont, Québec, Canada",OK
    ZBO,////,////,YBWN,UNK,1.0,Bowen,"Bowen, Australia",OWNO,////,////,????,"Bowen, Queensland, Australia",OK
    ZBP,////,////,////,UNK,nan,////,////,////,////,////,Penn Station,"Baltimore, Maryland, United States",UNK
    ZBR,////,////,OIZC,UNK,1.0,Chah-bahar,"Chah-Bahar, Iran",OWNO,////,////,????,"Chah-Bahar, Sistan va Baluchestan, Iran",OK
    ZBY,////,////,VLSB,UNK,1.0,Sayaboury,"Sayaboury, Lao PDR",OWNO,////,////,????,"Sayaboury, Lao People's Democratic Republic (Laos)",OK
    ZCL,////,////,MMZC,UNK,0.667,Zacatecas International Airport (La Calera),"Zacatecas, Mexico",OWNO,////,////,General Leobardo C. Ruiz International Airport,"Zacatecas, Zacatecas, México",OK
    ZCN,////,////,ETHC,UNK,nan,////,////,////,////,////,AB,"Celle, Lower Saxony, Germany",UNK
    ZCO,////,////,SCQP,UNK,0.609,Maquehue,"Temuco, Chile",OWNO,////,////,La Araucanía Airport,"Temuco, Araucanía, Chile",OK
    ZCQ,////,////,SCIC,UNK,nan,////,////,////,////,////,General Freire Airport,"Curicó, Maule, Chile",UNK
    ZDY,////,////,OMDL,UNK,1.0,Delma Island,Delma Island,IATA,////,////,Delma Airport,"Delma Island, Abu Dhabi, United Arab Emirates",MAYBE
    ZEC,////,////,FASC,UNK,1.0,Secunda,"Secunda, South Africa",OWNO,////,////,Secunda Airport,"Secunda, Mpumalanga, South Africa",OK
    ZEG,////,////,////,UNK,1.0,Senggo,"Senggo, Indonesia",OWNO,////,////,????,"Senggo, Papua, Indonesia",OK
    ZEL,////,////,CYJQ,UNK,1.0,Bella Bella,"Bella Bella, Canada",OWNO,////,////,????,"Bella Bella, Denny Island, British Columbia, Canada",OK
    ZEM,////,////,CZEM,UNK,0.737,East Main,"East Main, Canada",OWNO,////,////,Eastmain River,"Eastmain, Québec, Canada",OK
    ZEN,////,////,////,UNK,1.0,Zenag,"Zenag, Papua New Guinea",OWNO,////,////,????,"Zenag, Morobe, Papua-New Guinea",OK
    ZER,////,////,VEZO,UNK,1.0,Zero,"Zero, India",OWNO,////,////,????,"Zero, Arunachal Pradesh, India",OK
    ZES,////,////,////,UNK,nan,////,////,////,////,////,Off-line Pt,"Goeppingen, Germany",UNK
    ZFA,////,////,CZFA,UNK,1.0,Faro,"Faro, Canada",OWNO,////,////,????,"Faro, Yukon, Canada",OK
    ZFD,////,////,CZFD,UNK,nan,////,////,////,////,////,????,"Fond Du Lac, Saskatchewan, Canada",UNK
    ZFM,////,////,CZFM,UNK,1.0,Fort Mcpherson,"Fort Mcpherson, Canada",OWNO,////,////,????,"Fort McPherson, Northwest Territories, Canada",OK
    ZFN,////,////,CZFN,UNK,1.0,Tulita,"Tulita/Fort Norman, Canada",OWNO,////,////,Tulita Airport,"Tulita, Northwest Territories, Canada",OK
    ZFV,////,////,////,UNK,nan,////,////,////,////,////,30th Street Station,"Philadelphia, Pennsylvania, United States",UNK
    ZFW,////,////,////,UNK,1.0,Fairview,"Fairview, Canada",OWNO,////,////,????,"Fairview, Alberta, Canada",OK
    ZGF,////,////,CZGF,UNK,1.0,Grand Forks,"Grand Forks, Canada",OWNO,////,////,????,"Grand Forks, British Columbia, Canada",OK
    ZGI,////,////,CZGI,UNK,nan,////,////,////,////,////,????,"Gods River, Manitoba, Canada",UNK
    ZGL,////,////,YSGW,UNK,1.0,South Galway,"South Galway, Australia",OWNO,////,////,????,"South Galway, Queensland, Australia",OK
    ZGM,////,////,FLNA,UNK,1.0,Ngoma,"Ngoma, Zambia",OWNO,////,////,????,"Ngoma, Zambia",OK
    ZGR,////,////,CZGR,UNK,1.0,Little Grand Rapids,"Little Grand Rapids, Canada",OWNO,////,////,????,"Little Grand Rapids, Manitoba, Canada",OK
    ZGS,////,////,////,UNK,0.625,Gethsemani,"Gethsemani, Canada",OWNO,////,////,La Romaine Airport,"La Romaine, Québec, Canada",TO DO CHECK
    ZGU,////,////,NVSQ,UNK,0.533,Gaua,"Gaua, Vanuatu",OWNO,////,////,????,"Gaua Island, Banks Islands, Torba, Vanuatu",MAYBE
    ZHA,////,////,ZGZJ,UNK,1.0,Zhanjiang,"Zhanjiang, PR China",OWNO,////,////,????,"Zhanjiang, Guangdong, China",OK
    ZHI,////,////,LSZG,UNK,nan,////,////,////,////,////,????,"Grenchen, Solothurn, Switzerland",UNK
    ZHM,////,////,VGSH,UNK,1.0,Shamshernagar,"Shamshernagar, Bangladesh",OWNO,////,////,????,"Shamshernagar, Bangladesh",OK
    ZHP,////,////,CZHP,UNK,1.0,High Prairie,"High Prairie, Canada",OWNO,////,////,????,"High Prairie, Alberta, Canada",OK
    ZHY,////,////,ZLZW,UNK,nan,////,////,////,////,////,Zhongwei Shapotou Airport,"Zhongwei, Ningxia, China",UNK
    ZIA,////,////,UUBW,UNK,nan,////,////,////,////,////,Zhukovsky International Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",UNK
    ZIC,////,////,SCTO,UNK,1.0,Victoria,"Victoria, Chile",OWNO,////,////,Victoria Airport,"Victoria, Araucanía, Chile",OK
    ZIG,////,////,GOGG,UNK,1.0,Ziguinchor,"Ziguinchor, Senegal",OWNO,////,////,Ziguinchor Airport,"Ziguinchor, Ziguinchor, Senegal",OK
    ZIH,////,////,MMZH,UNK,1.0,Ixtapa-Zihuatanejo International Airport,"Ixtapa/Zihuatanejo, Mexico",OWNO,////,////,Ixtapa-Zihuatanejo International Airport,"Ixtapa/Zihuatanejo, Guerrero, México",OK
    ZIN,////,////,LSMI,UNK,nan,////,////,////,////,////,????,"Interlaken, Bern, Switzerland",UNK
    ZIX,////,////,UEVV,UNK,nan,////,////,////,////,////,Zhigansk Airport,"Zhigansk, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    ZJG,////,////,CZJG,UNK,1.0,Jenpeg,"Jenpeg, Canada",OWNO,////,////,????,"Jenpeg, Manitoba, Canada",OK
    ZJI,////,////,LSZL,UNK,nan,////,////,////,////,////,????,"Locarno, Ticino, Switzerland",UNK
    ZJN,////,////,CZJN,UNK,nan,////,////,////,////,////,????,"Swan River, Manitoba, Canada",UNK
    ZJT,////,////,////,UNK,0.895,Tanjung Pelepas Port,Tanjung Pelepas,IATA,////,////,Tanjung Pelepas Ferry Port,"Tanjung Pelepas, Malaysia",MAYBE
    ZKB,////,////,FLKY,UNK,1.0,Kasaba Bay,"Kasaba Bay, Zambia",OWNO,////,////,????,"Kasaba Bay, Zambia",OK
    ZKE,////,////,CZKE,UNK,1.0,Kaschechewan,"Kaschechewan, Canada",OWNO,////,////,Kashechewan Airport,"Kashechewan, Ontario, Canada",OK
    ZKG,////,////,////,UNK,1.0,Kegaska,"Kegaska, Canada",OWNO,////,////,Kegaska Airport,"Kegaska, Québec, Canada",OK
    ZKP,////,////,UESU,UNK,1.0,Zyryanka,Zyryanka,IATA,////,////,Zyryanka Airport,"Zyryanka, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    ZLO,////,////,MMZO,UNK,1.0,Playa de Oro International,"Manzanillo, Mexico",OWNO,////,////,Playa de Oro International Airport,"Manzanillo, Colima, México",OK
    ZLT,////,////,////,UNK,1.0,La Tabatiere,"La Tabatiere, Canada",OWNO,////,////,La Tabatière Airport,"La Tabatière, Québec, Canada",OK
    ZLX,////,////,////,UNK,nan,////,////,////,////,////,British Rail Terminal,"London, United Kingdom",UNK
    ZMD,////,////,SWSN,UNK,0.432,Sena Madureira,"Sena Madureira, Brazil",OWNO,////,////,Fazenda São Valentim,"Pontes e Lacerda, Mato Grosso, Brazil",TO DO CHECK
    ZMH,////,////,CZML,UNK,nan,////,////,////,////,////,????,"108 Mile Ranch, British Columbia, Canada",UNK

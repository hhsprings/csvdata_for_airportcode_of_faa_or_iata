
List for checking certainty of Great Circle Mapper (ELC - ERV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ELC,////,////,YELD,UNK,1.0,Elcho Island,"Elcho Island, Australia",OWNO,////,////,????,"Elcho Island, Northern Territory, Australia",OK
    ELD,ELD,KELD,KELD,OK,0.686,Goodwin Field,"El Dorado (AR), USA",OWNO,SOUTH ARKANSAS RGNL AT GOODWIN FIELD,"EL DORADO, AR - UNITED STATES",South Arkansas Regional at Goodwin Field,"El Dorado, Arkansas, United States",OK
    ELE,////,////,////,UNK,1.0,El Real,"El Real, Panama",OWNO,////,////,????,"El Real, Darién, Panamá",OK
    ELF,////,////,HSFS,UNK,1.0,El Fasher,"El Fasher, Sudan",OWNO,////,////,????,"El Fasher, North Darfur, Sudan",OK
    ELG,////,////,DAUE,UNK,1.0,El Golea,"El Golea, Algeria",OWNO,////,////,El Goléa Airport,"El Goléa, Ghardaïa, Algeria",OK
    ELH,////,MYEH,MYEH,OK,0.79,International,"North Eleuthera, Bahamas",OWNO,NORTH ELEUTHERA,"NORTH ELEUTHERA, - BAHAMAS",????,"North Eleuthera, Eleuthera, Bahamas",OK
    ELI,ELI,PFEL,PFEL,OK,1.0,Elim,"Elim (AK), USA",OWNO,ELIM,"ELIM, AK - UNITED STATES",????,"Elim, Alaska, United States",OK
    ELJ,////,////,////,UNK,1.0,El Recreo,"El Recreo, Colombia",OWNO,////,////,????,"El Recreo, Meta, Colombia",OK
    ELK,ELK,KELK,KELK,OK,0.412,Municipal,"Elk City (OK), USA",OWNO,ELK CITY RGNL BUSINESS,"ELK CITY, OK - UNITED STATES",Elk City Regional Business,"Elk City, Oklahoma, United States",OK
    ELL,////,////,FAER,UNK,1.0,Ellisras,"Ellisras, South Africa",OWNO,////,////,????,"Ellisras, Limpopo, South Africa",OK
    ELM,ELM,KELM,KELM,OK,1.0,Corning/Elmina Regional Airport,"Elmira (NY), USA",OWNO,ELMIRA/CORNING RGNL,"ELMIRA/CORNING, NY - UNITED STATES",Elmira/Corning Regional,"Elmira/Corning, New York, United States",OK
    ELN,ELN,KELN,KELN,OK,1.0,Bowers Field,"Ellensburg (WA), USA",OWNO,BOWERS FIELD,"ELLENSBURG, WA - UNITED STATES",Bowers Field,"Ellensburg, Washington, United States",OK
    ELO,////,////,SATD,UNK,1.0,Eldorado,"Eldorado, Argentina",OWNO,////,////,????,"Eldorado, Misiones, Argentina",OK
    ELP,ELP,KELP,KELP,OK,1.0,International,"El Paso (TX), USA",OWNO,EL PASO INTL,"EL PASO, TX - UNITED STATES",El Paso International Airport,"El Paso, Texas, United States",OK
    ELQ,////,////,OEGS,UNK,0.333,Gassim,"Gassim, Saudi Arabia",OWNO,////,////,Prince Nayef bin Abdulaziz Airport,"Gassim, Saudi Arabia",OK
    ELR,////,////,////,UNK,1.0,Elelim,"Elelim, Indonesia",OWNO,////,////,????,"Elelim, Papua, Indonesia",OK
    ELS,////,////,FAEL,UNK,1.0,East London,"East London, South Africa",OWNO,////,////,????,"East London, Eastern Cape, South Africa",OK
    ELT,////,////,HETR,UNK,1.0,El Tor/Tour Sinai Airport,"Tour Sinai City, Egypt",OWNO,////,////,Tour Sinai City,"El Tor, Janub Sina (South Sinai), Egypt",OK
    ELU,////,////,DAUO,UNK,1.0,Guemar,"El Oued, Algeria",OWNO,////,////,Guemar,"El Oued, El Oued, Algeria",OK
    ELV,ELV,PAEL,PAEL,OK,0.815,Elfin Cove SPB,"Elfin Cove (AK), USA",OWNO,ELFIN COVE,"ELFIN COVE, AK - UNITED STATES",Elfin Cove SPB,"Elfin Cove, Alaska, United States",OK
    ELW,1Z9,////,////,OK,0.774,Ellamar,"Ellamar (AK), USA",OWNO,ELLAMAR,"ELLAMAR, AK - UNITED STATES",Ellamar SPB,"Ellamar, Alaska, United States",OK
    ELY,ELY,KELY,KELY,OK,0.682,Yelland,"Ely (NV), USA",OWNO,ELY ARPT /YELLAND FLD/,"ELY, NV - UNITED STATES",Ely Airport,"Ely, Nevada, United States",OK
    ELZ,ELZ,KELZ,KELZ,OK,0.545,Municipal,"Wellsville (NY), USA",OWNO,"WELLSVILLE MUNI ARPT, TARANTINE FLD","WELLSVILLE, NY - UNITED STATES","Wellsville Municipal,Tarantine Field","Wellsville, New York, United States",OK
    EMA,////,////,EGNX,UNK,1.0,East Midlands,"Nottingham, United Kingdom",OWNO,////,////,East Midlands Airport,"Derby, Leicestershire, England, United Kingdom",OK
    EMD,////,////,YEML,UNK,1.0,Emerald,"Emerald, Australia",OWNO,////,////,????,"Emerald, Queensland, Australia",OK
    EME,////,////,EDWE,UNK,1.0,Emden Airport,"Emden, Germany",OWNO,////,////,????,"Emden, Lower Saxony, Germany",OK
    EMG,////,////,FAEM,UNK,1.0,Empangeni,"Empangeni, South Africa",OWNO,////,////,????,"Empangeni, KwaZulu-Natal, South Africa",OK
    EMI,////,////,////,UNK,1.0,Emirau,"Emirau, Papua New Guinea",OWNO,////,////,????,"Emirau, New Ireland, Papua-New Guinea",OK
    EMK,ENM,PAEM,PAEM,OK,1.0,Emmonak,"Emmonak (AK), USA",OWNO,EMMONAK,"EMMONAK, AK - UNITED STATES",????,"Emmonak, Alaska, United States",OK
    EML,////,////,LSME,UNK,0.769,Emmen,"Emmen, Switzerland",OWNO,////,////,Emmen AB,"Luzern, Luzern, Switzerland",MAYBE
    EMM,EMM,KEMM,KEMM,OK,0.757,Kemerer,"Kemerer (WY), USA",OWNO,KEMMERER MUNI,"KEMMERER, WY - UNITED STATES",Kemmerer Municipal Airport,"Kemmerer, Wyoming, United States",OK
    EMN,////,////,GQNI,UNK,1.0,Nema,"Nema, Mauritania",OWNO,////,////,Néma Airport,"Néma, Mauritania",OK
    EMO,////,////,////,UNK,1.0,Emo,"Emo, Papua New Guinea",OWNO,////,////,????,"Emo, Northern, Papua-New Guinea",OK
    EMP,EMP,KEMP,KEMP,OK,0.719,Emporia,"Emporia (KS), USA",OWNO,EMPORIA MUNI,"EMPORIA, KS - UNITED STATES",Emporia Municipal Airport,"Emporia, Kansas, United States",OK
    EMS,////,////,////,UNK,1.0,Embessa,"Embessa, Papua New Guinea",OWNO,////,////,????,"Embessa, Northern, Papua-New Guinea",OK
    EMT,EMT,KEMT,KEMT,OK,1.0,El Monte,"El Monte (CA), USA",OWNO,EL MONTE,"EL MONTE, CA - UNITED STATES",????,"El Monte, California, United States",OK
    EMX,////,////,SAVD,UNK,1.0,El Maiten,"El Maiten, Argentina",OWNO,////,////,????,"El Maitén, Chubut, Argentina",OK
    EMY,////,////,HEMN,UNK,1.0,El Minya,"El Minya, Egypt",OWNO,////,////,????,"El Minya, Al Minya (Minya), Egypt",OK
    ENA,ENA,PAEN,PAEN,OK,0.686,Kenai,"Kenai (AK), USA",OWNO,KENAI MUNI,"KENAI, AK - UNITED STATES",Kenai Municipal Airport,"Kenai, Alaska, United States",OK
    ENB,////,////,YEEB,UNK,0.737,Eneabba West,"Eneabba West, Australia",OWNO,////,////,????,"Eneabba, Western Australia, Australia",MAYBE
    ENC,////,////,LFSN,UNK,1.0,Essey,"Nancy, France",OWNO,////,////,Essey,"Nancy, Lorraine, France",OK
    END,END,KEND,KEND,OK,1.0,Vance AFB,"Enid (OK), USA",OWNO,VANCE AFB,"ENID, OK - UNITED STATES",Vance AFB,"Enid, Oklahoma, United States",OK
    ENE,////,////,WATE,UNK,0.286,Ende,"Ende, Indonesia",OWNO,////,////,H Hasan Aroeboesman Airport,"Ende, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    ENF,////,////,EFET,UNK,1.0,Enontekio,"Enontekio, Finland",OWNO,////,////,????,"Enontekio, Lappi (Lappland (Lapland)), Finland",OK
    ENH,////,////,ZHES,UNK,1.0,Enshi,"Enshi, PR China",OWNO,////,////,????,"Enshi, Hubei, China",OK
    ENI,////,////,////,UNK,1.0,El Nido,"El Nido, Philippines",OWNO,////,////,????,"El Nido, Philippines",OK
    ENJ,////,////,////,UNK,1.0,El Naranjo,"El Naranjo, Guatemala",OWNO,////,////,El Naranjo Airport,"El Naranjo, Petén, Guatemala",OK
    ENK,////,////,EGAB,UNK,1.0,St Angelo,"Enniskillen, United Kingdom",OWNO,////,////,St. Angelo,"Enniskillen, County Fermanagh, Northern Ireland, United Kingdom",OK
    ENL,ENL,KENL,KENL,OK,1.0,Municipal,"Centralia (IL), USA",OWNO,CENTRALIA MUNI,"CENTRALIA, IL - UNITED STATES",Centralia Municipal Airport,"Centralia, Illinois, United States",OK
    ENN,ENN,PANN,PANN,OK,1.0,Municipal,"Nenana (AK), USA",OWNO,NENANA MUNI,"NENANA, AK - UNITED STATES",Nenana Municipal Airport,"Nenana, Alaska, United States",OK
    ENO,////,////,SGEN,UNK,0.649,Teniente Prim Alarcon,"Encarnacion, Paraguay",OWNO,////,////,Teniente Amín Ayub González International Airport,"Encarnación, Itapúa, Paraguay",OK
    ENS,////,////,EHTW,UNK,1.0,Twente,"Enschede, Netherlands",OWNO,////,////,Twenthe,"Enschede, Overijssel, Netherlands",OK
    ENT,////,////,PKMA,UNK,1.0,Enewetak Island,"Enewetak Island, Marshall Islands",OWNO,////,////,????,"Enewetak Aux AF, Enewetak Island, Marshall Islands",OK
    ENU,////,////,DNEN,UNK,0.37,Enugu,"Enugu, Nigeria",OWNO,////,////,Akanu Ibiam International Airport,"Enugu, Enugu, Nigeria",OK
    ENV,ENV,KENV,KENV,OK,1.0,Wendover,"Wendover (UT), USA",OWNO,WENDOVER,"WENDOVER, UT - UNITED STATES",????,"Wendover, Utah, United States",OK
    ENW,ENW,KENW,KENW,OK,0.746,Municipal,"Kenosha (WI), USA",OWNO,KENOSHA RGNL,"KENOSHA, WI - UNITED STATES",Kenosha Regional,"Kenosha, Wisconsin, United States",OK
    ENY,////,////,ZLYA,UNK,1.0,Yan'an,"Yan'an, PR China",OWNO,////,////,????,"Yan'an, Shaanxi, China",OK
    EOH,////,////,SKMD,UNK,1.0,Enrique Olaya Herrera,"Medellin, Colombia",OWNO,////,////,Enrique Olaya Herrera Airport,"Medellín, Antioquia, Colombia",OK
    EOI,////,////,EGED,UNK,1.0,Eday,"Eday, United Kingdom",OWNO,////,////,Eday Airport,"Eday, Orkney Isles, Scotland, United Kingdom",OK
    EOK,EOK,KEOK,KEOK,OK,0.679,Keokuk,"Keokuk IA, USA",OWNO,KEOKUK MUNI,"KEOKUK, IA - UNITED STATES",Keokuk Municipal Airport,"Keokuk, Iowa, United States",OK
    EOR,////,////,SVED,UNK,1.0,El Dorado,"El Dorado, Venezuela",OWNO,////,////,????,"El Dorado, Bolívar, Venezuela",OK
    EOS,EOS,KEOS,KEOS,OK,0.465,Neosho,"Neosho (MO), USA",OWNO,NEOSHO HUGH ROBINSON,"NEOSHO, MO - UNITED STATES",Neosho Hugh Robinson Airport,"Neosho, Missouri, United States",OK
    EOZ,////,////,SVEZ,UNK,1.0,Elorza,"Elorza, Venezuela",OWNO,////,////,????,"Elorza, Apure, Venezuela",OK
    EPG,NE69,////,////,OK,1.0,Browns,"Weeping Water (NE), USA",OWNO,BROWNS,"WEEPING WATER, NE - UNITED STATES",Browns Airport,"Weeping Water, Nebraska, United States",OK
    EPH,EPH,KEPH,KEPH,OK,0.771,Ephrata,"Ephrata (WA), USA",OWNO,EPHRATA MUNI,"EPHRATA, WA - UNITED STATES",Ephrata Municipal Airport,"Ephrata, Washington, United States",OK
    EPL,////,////,LFSG,UNK,1.0,Mirecourt,"Epinal, France",OWNO,////,////,Mirecourt,"Épinal, Lorraine, France",OK
    EPN,////,////,////,UNK,1.0,Epena,"Epena, Congo (ROC)",OWNO,////,////,????,"Epena, Congo (Republic of)",OK
    EPR,////,////,YESP,UNK,1.0,Esperance,"Esperance, Australia",OWNO,////,////,????,"Esperance, Western Australia, Australia",OK
    EPS,////,////,MDAB,UNK,0.625,El Portillo,"El Portillo/Samana, Dominican Republic",OWNO,////,////,Arroyo Barril International Airport,"Samaná, Samaná, Dominican Republic",OK
    EPT,////,////,////,UNK,1.0,Eliptamin,"Eliptamin, Papua New Guinea",OWNO,////,////,????,"Eliptamin, Sandaun, Papua-New Guinea",OK
    EPU,////,////,EEPU,UNK,nan,////,////,////,////,////,????,"Pärnu, Estonia",UNK
    EQS,////,////,SAVE,UNK,1.0,Esquel,"Esquel, Argentina",OWNO,////,////,????,"Esquel, Chubut, Argentina",OK
    ERA,////,////,HCMU,UNK,1.0,Erigavo,"Erigavo, Somalia",OWNO,////,////,????,"Erigavo, Sanaag, Somalia",OK
    ERB,////,////,YERN,UNK,0.333,Ernabella,"Ernabella, Australia",OWNO,////,////,????,"Pukatja, South Australia, Australia",TO DO CHECK
    ERC,////,////,LTCD,UNK,1.0,Erzincan,"Erzincan, Turkey",OWNO,////,////,????,"Erzincan, Erzincan, Turkey",OK
    ERD,////,////,UKDB,UNK,1.0,Berdyansk,"Berdyansk, Ukraine",OWNO,////,////,????,"Berdyansk, Zaporizhia, Ukraine",OK
    ERE,////,////,////,UNK,1.0,Erave,"Erave, Papua New Guinea",OWNO,////,////,????,"Erave, Southern Highlands, Papua-New Guinea",OK
    ERF,////,////,EDDE,UNK,0.457,Erfurt-Weimar Airport,"Erfurt, Germany",OWNO,////,////,Binderslebn,"Erfurt, Thüringen, Germany",OK
    ERG,////,////,UIKE,UNK,nan,////,////,////,////,////,Erbogachen Airport,"Erbogachen, Irkutskaya, Russian Federation (Russia)",UNK
    ERH,////,////,GMFK,UNK,0.513,Errachidia,"Errachidia, Morocco",OWNO,////,////,Moulay Ali Cherif,"Er-Rachidia, Morocco",OK
    ERI,ERI,KERI,KERI,OK,1.0,International,"Erie (PA), USA",OWNO,ERIE INTL/TOM RIDGE FIELD,"ERIE, PA - UNITED STATES",Erie International/Tom Ridge Field,"Erie, Pennsylvania, United States",OK
    ERL,////,////,ZBER,UNK,nan,////,////,////,////,////,Erenhot Saiwusu International Airport,"Erenhot, Inner Mongolia, China",UNK
    ERM,////,////,SSER,UNK,0.615,Comandante Kraemer,"Erechim, Brazil",OWNO,////,////,????,"Erechim, Rio Grande do Sul, Brazil",OK
    ERN,////,////,SWEI,UNK,1.0,Eirunepe,"Eirunepe, Brazil",OWNO,////,////,????,"Eirunepé, Amazonas, Brazil",OK
    ERQ,////,////,YESE,UNK,nan,////,////,////,////,////,Elrose Mine Aerodrome,"Cloncurry, Queensland, Australia",UNK
    ERR,ERR,KERR,KERR,OK,1.0,Errol,"Errol (NH), USA",OWNO,ERROL,"ERROL, NH - UNITED STATES",????,"Errol, New Hampshire, United States",OK
    ERS,////,////,FYWE,UNK,1.0,Eros Airport,"Windhoek, Namibia",OWNO,////,////,Eros,"Windhoek, Namibia",OK
    ERT,////,////,////,UNK,1.0,Erdenet,"Erdenet, Mongolia",OWNO,////,////,????,"Erdenet, Mongolia",OK
    ERU,////,////,////,UNK,1.0,Erume,"Erume, Papua New Guinea",OWNO,////,////,????,"Erume, Central, Papua-New Guinea",OK
    ERV,ERV,KERV,KERV,OK,0.734,Kerrville,"Kerrville (TX), USA",OWNO,KERRVILLE MUNI/LOUIS SCHREINER FIELD,"KERRVILLE, TX - UNITED STATES",Kerrville Municipal/Louis Schreiner Field,"Kerrville, Texas, United States",OK

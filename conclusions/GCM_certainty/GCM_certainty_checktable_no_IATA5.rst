
List for checking certainty of Great Circle Mapper (FAA LID LJF - FAA LID P08)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,LJF,KLJF,KLJF,OK,1.0,////,////,////,LITCHFIELD MUNI,"LITCHFIELD, MN - UNITED STATES",Litchfield Municipal Airport,"Litchfield, Minnesota, United States",OK
    ////,LKR,KLKR,KLKR,OK,1.0,////,////,////,LANCASTER COUNTY-MC WHIRTER FIELD,"LANCASTER, SC - UNITED STATES",Lancaster County-Mc Whirter Field,"Lancaster, South Carolina, United States",OK
    ////,LLQ,KLLQ,KLLQ,OK,1.0,////,////,////,MONTICELLO MUNI/ELLIS FIELD,"MONTICELLO, AR - UNITED STATES",Monticello Municipal/Ellis Field,"Monticello, Arkansas, United States",OK
    ////,LMO,KLMO,KLMO,OK,1.0,////,////,////,VANCE BRAND,"LONGMONT, CO - UNITED STATES",Vance Brand Airport,"Longmont, Colorado, United States",OK
    ////,LNC,KLNC,KLNC,OK,1.0,////,////,////,LANCASTER RGNL,"LANCASTER, TX - UNITED STATES",Lancaster Regional,"Lancaster, Texas, United States",OK
    ////,LNL,KLNL,KLNL,OK,1.0,////,////,////,KINGS LAND O' LAKES,"LAND O' LAKES, WI - UNITED STATES",Kings Land O' Lakes Airport,"Land O' Lakes, Wisconsin, United States",OK
    ////,LPR,KLPR,KLPR,OK,1.0,////,////,////,LORAIN COUNTY RGNL,"LORAIN/ELYRIA, OH - UNITED STATES",Lorain County Regional,"Lorain/Elyria, Ohio, United States",OK
    ////,LRO,KLRO,KLRO,OK,1.0,////,////,////,MT PLEASANT RGNL-FAISON FIELD,"MOUNT PLEASANT, SC - UNITED STATES",Mt. Pleasant Regional-Faison Field,"Mount Pleasant, South Carolina, United States",OK
    ////,LUA,KLUA,KLUA,OK,1.0,////,////,////,LURAY CAVERNS,"LURAY, VA - UNITED STATES",Luray Caverns Airport,"Luray, Virginia, United States",OK
    ////,LUD,KLUD,KLUD,OK,1.0,////,////,////,DECATUR MUNI,"DECATUR, TX - UNITED STATES",Decatur Municipal Airport,"Decatur, Texas, United States",OK
    ////,LUM,KLUM,KLUM,OK,1.0,////,////,////,MENOMONIE MUNI-SCORE FIELD,"MENOMONIE, WI - UNITED STATES",Menomonie Municipal-Score Field,"Menomonie, Wisconsin, United States",OK
    ////,LUX,KLUX,KLUX,OK,1.0,////,////,////,LAURENS COUNTY,"LAURENS, SC - UNITED STATES",Laurens County Airport,"Laurens, South Carolina, United States",OK
    ////,LVJ,KLVJ,KLVJ,OK,1.0,////,////,////,PEARLAND RGNL,"HOUSTON, TX - UNITED STATES",Pearland Regional,"Houston, Texas, United States",OK
    ////,LVN,KLVN,KLVN,OK,1.0,////,////,////,AIRLAKE,"MINNEAPOLIS, MN - UNITED STATES",Airlake Airport,"Minneapolis, Minnesota, United States",OK
    ////,LWA,KLWA,KLWA,OK,1.0,////,////,////,SOUTH HAVEN AREA RGNL,"SOUTH HAVEN, MI - UNITED STATES",South Haven Area Regional,"South Haven, Michigan, United States",OK
    ////,LWD,KLWD,KLWD,OK,1.0,////,////,////,LAMONI MUNI,"LAMONI, IA - UNITED STATES",Lamoni Municipal Airport,"Lamoni, Iowa, United States",OK
    ////,LXL,KLXL,KLXL,OK,1.0,////,////,////,LITTLE FALLS/MORRISON COUNTY-LINDBERGH FLD,"LITTLE FALLS, MN - UNITED STATES",Little Falls/Morrison County-Lindbergh Field,"Little Falls, Minnesota, United States",OK
    ////,LXT,KLXT,KLXT,OK,1.0,////,////,////,LEE'S SUMMIT MUNI,"LEE'S SUMMIT, MO - UNITED STATES",Lee's Summit Municipal Airport,"Lee's Summit, Missouri, United States",OK
    ////,LXY,KLXY,KLXY,OK,0.923,////,////,////,MEXIA-LIMESTONE CO,"MEXIA, TX - UNITED STATES",Mexia-Limestone County Airport,"Mexia, Texas, United States",OK
    ////,LYV,KLYV,KLYV,OK,1.0,////,////,////,QUENTIN AANENSON FIELD,"LUVERNE, MN - UNITED STATES",Quentin Aanenson Field,"Luverne, Minnesota, United States",OK
    ////,LZU,KLZU,KLZU,OK,1.0,////,////,////,GWINNETT COUNTY - BRISCOE FIELD,"LAWRENCEVILLE, GA - UNITED STATES",Gwinnett County - Briscoe Field,"Lawrenceville, Georgia, United States",OK
    ////,LZZ,KLZZ,KLZZ,OK,1.0,////,////,////,LAMPASAS,"LAMPASAS, TX - UNITED STATES",????,"Lampasas, Texas, United States",OK
    ////,M13,////,////,OK,1.0,////,////,////,POPLARVILLE-PEARL RIVER COUNTY,"POPLARVILLE, MS - UNITED STATES",Poplarville-Pearl River County Airport,"Poplarville, Mississippi, United States",OK
    ////,M94,////,////,OK,1.0,////,////,////,DESERT AIRE,"MATTAWA, WA - UNITED STATES",Desert Aire Airport,"Mattawa, Washington, United States",OK
    ////,MAI,KMAI,KMAI,OK,1.0,////,////,////,MARIANNA MUNI,"MARIANNA, FL - UNITED STATES",Marianna Municipal Airport,"Marianna, Florida, United States",OK
    ////,MAN,KMAN,KMAN,OK,1.0,////,////,////,NAMPA MUNI,"NAMPA, ID - UNITED STATES",Nampa Municipal Airport,"Nampa, Idaho, United States",OK
    ////,MAO,KMAO,KMAO,OK,1.0,////,////,////,MARION COUNTY,"MARION, SC - UNITED STATES",Marion County Airport,"Marion, South Carolina, United States",OK
    ////,MBO,KMBO,KMBO,OK,1.0,////,////,////,BRUCE CAMPBELL FIELD,"MADISON, MS - UNITED STATES",Bruce Campbell Field,"Madison, Mississippi, United States",OK
    ////,MCX,KMCX,KMCX,OK,1.0,////,////,////,WHITE COUNTY,"MONTICELLO, IN - UNITED STATES",White County Airport,"Monticello, Indiana, United States",OK
    ////,MDQ,KMDQ,KMDQ,OK,1.0,////,////,////,HUNTSVILLE EXECUTIVE AIRPORT TOM SHARP JR FLD,"HUNTSVILLE, AL - UNITED STATES",Huntsville Executive Airport Tom Sharp Jr. Field,"Huntsville, Alabama, United States",OK
    ////,ME5,////,////,OK,1.0,////,////,////,BANKS,"SWANS ISLAND, ME - UNITED STATES",Banks Airport,"Swans Island, Maine, United States",OK
    ////,MEZ,KMEZ,KMEZ,OK,1.0,////,////,////,MENA INTERMOUNTAIN MUNI,"MENA, AR - UNITED STATES",Mena Intermountain Municipal Airport,"Mena, Arkansas, United States",OK
    ////,MGG,KMGG,KMGG,OK,1.0,////,////,////,MAPLE LAKE MUNI-BILL MAVENCAMP SR FLD,"MAPLE LAKE, MN - UNITED STATES",Maple Lake Municipal-Bill Mavencamp Sr Field,"Maple Lake, Minnesota, United States",OK
    ////,MGN,KMGN,KMGN,OK,1.0,////,////,////,HARBOR SPRINGS,"HARBOR SPRINGS, MI - UNITED STATES",????,"Harbor Springs, Michigan, United States",OK
    ////,MKJ,KMKJ,KMKJ,OK,1.0,////,////,////,MOUNTAIN EMPIRE,"MARION/WYTHEVILLE, VA - UNITED STATES",Mountain Empire Airport,"Marion/Wytheville, Virginia, United States",OK
    ////,MKN,KMKN,KMKN,OK,1.0,////,////,////,COMANCHE COUNTY-CITY,"COMANCHE, TX - UNITED STATES",Comanche County-City Airport,"Comanche, Texas, United States",OK
    ////,MKS,KMKS,KMKS,OK,1.0,////,////,////,BERKELEY COUNTY,"MONCKS CORNER, SC - UNITED STATES",Berkeley County Airport,"Moncks Corner, South Carolina, United States",OK
    ////,MMK,KMMK,KMMK,OK,1.0,////,////,////,MERIDEN MARKHAM MUNI,"MERIDEN, CT - UNITED STATES",Meriden Markham Municipal Airport,"Meriden, Connecticut, United States",OK
    ////,MMV,KMMV,KMMV,OK,1.0,////,////,////,MC MINNVILLE MUNI,"MC MINNVILLE, OR - UNITED STATES",Mc Minnville Municipal Airport,"Mc Minnville, Oregon, United States",OK
    ////,MNE,KMNE,KMNE,OK,1.0,////,////,////,MINDEN,"MINDEN, LA - UNITED STATES",????,"Minden, Louisiana, United States",OK
    ////,MNI,KMNI,KMNI,OK,1.0,////,////,////,SANTEE COOPER RGNL,"MANNING, SC - UNITED STATES",Santee Cooper Regional,"Manning, South Carolina, United States",OK
    ////,MNV,KMNV,KMNV,OK,1.0,////,////,////,MONROE COUNTY,"MADISONVILLE, TN - UNITED STATES",Monroe County Airport,"Madisonville, Tennessee, United States",OK
    ////,MNZ,KMNZ,KMNZ,OK,1.0,////,////,////,HAMILTON MUNI,"HAMILTON, TX - UNITED STATES",Hamilton Municipal Airport,"Hamilton, Texas, United States",OK
    ////,MOS,////,////,OK,1.0,////,////,////,MOSES POINT,"ELIM, AK - UNITED STATES",Moses Point Airport,"Elim, Alaska, United States",OK
    ////,MPG,KMPG,KMPG,OK,1.0,////,////,////,MARSHALL COUNTY,"MOUNDSVILLE, WV - UNITED STATES",Marshall County Airport,"Moundsville, West Virginia, United States",OK
    ////,MPI,KMPI,KMPI,OK,1.0,////,////,////,MARIPOSA-YOSEMITE,"MARIPOSA, CA - UNITED STATES",Mariposa-Yosemite Airport,"Mariposa, California, United States",OK
    ////,MQJ,KMQJ,KMQJ,OK,1.0,////,////,////,INDIANAPOLIS RGNL,"INDIANAPOLIS, IN - UNITED STATES",Indianapolis Regional,"Indianapolis, Indiana, United States",OK
    ////,MRH,KMRH,KMRH,OK,1.0,////,////,////,MICHAEL J SMITH FIELD,"BEAUFORT, NC - UNITED STATES",Michael J Smith Field,"Beaufort, North Carolina, United States",OK
    ////,MRJ,KMRJ,KMRJ,OK,1.0,////,////,////,IOWA COUNTY,"MINERAL POINT, WI - UNITED STATES",Iowa County Airport,"Mineral Point, Wisconsin, United States",OK
    ////,MRT,KMRT,KMRT,OK,1.0,////,////,////,UNION COUNTY,"MARYSVILLE, OH - UNITED STATES",Union County Airport,"Marysville, Ohio, United States",OK
    ////,MTV,KMTV,KMTV,OK,1.0,////,////,////,BLUE RIDGE,"MARTINSVILLE, VA - UNITED STATES",Blue Ridge Airport,"Martinsville, Virginia, United States",OK
    ////,MVI,KMVI,KMVI,OK,1.0,////,////,////,MONTE VISTA MUNI,"MONTE VISTA, CO - UNITED STATES",Monte Vista Municipal Airport,"Monte Vista, Colorado, United States",OK
    ////,MVM,KMVM,KMVM,OK,1.0,////,////,////,MACHIAS VALLEY,"MACHIAS, ME - UNITED STATES",Machias Valley Airport,"Machias, Maine, United States",OK
    ////,MWK,KMWK,KMWK,OK,1.0,////,////,////,MOUNT AIRY/SURRY COUNTY,"MOUNT AIRY, NC - UNITED STATES",Mount Airy/Surry County Airport,"Mount Airy, North Carolina, United States",OK
    ////,MYJ,KMYJ,KMYJ,OK,1.0,////,////,////,MEXICO MEMORIAL,"MEXICO, MO - UNITED STATES",Mexico Memorial Airport,"Mexico, Missouri, United States",OK
    ////,MYZ,KMYZ,KMYZ,OK,1.0,////,////,////,MARYSVILLE MUNI,"MARYSVILLE, KS - UNITED STATES",Marysville Municipal Airport,"Marysville, Kansas, United States",OK
    ////,MZH,KMZH,KMZH,OK,1.0,////,////,////,MOOSE LAKE CARLTON COUNTY,"MOOSE LAKE, MN - UNITED STATES",Moose Lake Carlton County Airport,"Moose Lake, Minnesota, United States",OK
    ////,N69,////,////,OK,1.0,////,////,////,STORMVILLE,"STORMVILLE, NY - UNITED STATES",????,"Stormville, New York, United States",OK
    ////,N87,////,////,OK,1.0,////,////,////,TRENTON-ROBBINSVILLE,"ROBBINSVILLE, NJ - UNITED STATES",Trenton-Robbinsville Airport,"Robbinsville, New Jersey, United States",OK
    ////,N96,////,////,OK,1.0,////,////,////,BELLEFONTE,"BELLEFONTE, PA - UNITED STATES",????,"Bellefonte, Pennsylvania, United States",OK
    ////,NBC,KNBC,KNBC,OK,1.0,////,////,////,BEAUFORT MCAS /MERRITT FIELD/,"BEAUFORT, SC - UNITED STATES",Beaufort MCAS,"Beaufort, South Carolina, United States",OK
    ////,NCA,KNCA,KNCA,OK,1.0,////,////,////,NEW RIVER MCAS /H/ /MCCUTCHEON FLD/,"JACKSONVILLE, NC - UNITED STATES",New River MCAS (H),"Jacksonville, North Carolina, United States",OK
    ////,NDZ,KNDZ,KNDZ,OK,1.0,////,////,////,WHITING FIELD NAS SOUTH,"MILTON, FL - UNITED STATES",Whiting Field NAS South Airport,"Milton, Florida, United States",OK
    ////,NFE,KNFE,KNFE,OK,1.0,////,////,////,FENTRESS NALF,"FENTRESS, VA - UNITED STATES",Fentress NALF Airport,"Fentress, Virginia, United States",OK
    ////,NFG,KNFG,KNFG,OK,1.0,////,////,////,CAMP PENDLETON MCAS (MUNN FIELD),"OCEANSIDE, CA - UNITED STATES",Camp Pendleton MCAS,"Oceanside, California, United States",OK
    ////,NGF,PHNG,PHNG,OK,1.0,////,////,////,KANEOHE BAY MCAS (MARION E CARL FIELD),"KANEOHE, HI - UNITED STATES",Kaneohe Bay MCAS,"Kaneohe, Oahu, Hawaii, United States",OK
    ////,NID,KNID,KNID,OK,1.0,////,////,////,CHINA LAKE NAWS (ARMITAGE FLD),"CHINA LAKE, CA - UNITED STATES",NAWS China Lake,"China Lake, California, United States",OK
    ////,NJM,KNJM,KNJM,OK,1.0,////,////,////,BOGUE FIELD MCALF,"SWANSBORO, NC - UNITED STATES",Bogue Field McAlf Airport,"Swansboro, North Carolina, United States",OK
    ////,NK39,////,////,OK,0.847,////,////,////,ONE POLICE PLAZA,"NEW YORK, NY - UNITED STATES",One Police Plaza Heliport,"New York, New York, United States",OK
    ////,NKT,KNKT,KNKT,OK,1.0,////,////,////,CHERRY POINT MCAS /CUNNINGHAM FIELD/,"CHERRY POINT, NC - UNITED STATES",Cherry Point MCAS,"Cherry Point, North Carolina, United States",OK
    ////,NMM,KNMM,KNMM,OK,1.0,////,////,////,MERIDIAN NAS /MC CAIN FIELD/,"MERIDIAN, MS - UNITED STATES",NAS Meridian,"Meridian, Mississippi, United States",OK
    ////,NOG,KNOG,KNOG,OK,1.0,////,////,////,ORANGE GROVE NALF,"ORANGE GROVE, TX - UNITED STATES",Orange Grove NALF Airport,"Orange Grove, Texas, United States",OK
    ////,NOW,KNOW,KNOW,OK,1.0,////,////,////,PORT ANGELES CGAS,"PORT ANGELES, WA - UNITED STATES",Port Angeles CGAS Airport,"Port Angeles, Washington, United States",OK
    ////,NQX,KNQX,KNQX,OK,1.0,////,////,////,KEY WEST NAS /BOCA CHICA FIELD/,"KEY WEST, FL - UNITED STATES",NAS Key West,"Key West, Florida, United States",OK
    ////,NSI,KNSI,KNSI,OK,1.0,////,////,////,SAN NICOLAS ISLAND NOLF,"SAN NICOLAS ISLAND, CA - UNITED STATES",San Nicolas Island NOLF Airport,"San Nicolas Island, California, United States",OK
    ////,NUC,KNUC,KNUC,OK,1.0,////,////,////,SAN CLEMENTE ISLAND NALF,"SAN CLEMENTE ISLAND, CA - UNITED STATES",NALF San Clemente Island,"San Clemente Island, California, United States",OK
    ////,NUI,KNUI,KNUI,OK,1.0,////,////,////,WEBSTER NOLF,"ST INIGOES, MD - UNITED STATES",Webster NOLF Airport,"St. Inigoes, Maryland, United States",OK
    ////,NXF,KNXF,KNXF,OK,0.786,////,////,////,MCOLF CAMP PENDLETON (RED BEACH),"OCEANSIDE, CA - UNITED STATES",Marine Corps Outlying Field Camp Pendleton Airport,"Oceanside, California, United States",OK
    ////,NXP,KNXP,KNXP,OK,1.0,////,////,////,TWENTYNINE PALMS SELF,"TWENTYNINE PALMS, CA - UNITED STATES",Twentynine Palms Self Airport,"Twentynine Palms, California, United States",OK
    ////,NY9,////,////,OK,1.0,////,////,////,LONG LAKE /HELMS,"LONG LAKE, NY - UNITED STATES",Long Lake /Helms SPB,"Long Lake, New York, United States",OK
    ////,NY94,////,////,OK,1.0,////,////,////,OLD RHINEBECK,"RHINEBECK, NY - UNITED STATES",Old Rhinebeck Airport,"Rhinebeck, New York, United States",OK
    ////,O02,////,////,OK,1.0,////,////,////,NERVINO,"BECKWOURTH, CA - UNITED STATES",Nervino Airport,"Beckwourth, California, United States",OK
    ////,O03,////,////,OK,1.0,////,////,////,MORGANTOWN,"MORGANTOWN, PA - UNITED STATES",????,"Morgantown, Pennsylvania, United States",OK
    ////,O19,////,////,OK,1.0,////,////,////,KNEELAND,"EUREKA, CA - UNITED STATES",Kneeland Airport,"Eureka, California, United States",OK
    ////,O27,////,////,OK,1.0,////,////,////,OAKDALE,"OAKDALE, CA - UNITED STATES",????,"Oakdale, California, United States",OK
    ////,O43,////,////,OK,1.0,////,////,////,YERINGTON MUNI,"YERINGTON, NV - UNITED STATES",Yerington Municipal Airport,"Yerington, Nevada, United States",OK
    ////,OAR,KOAR,KOAR,OK,1.0,////,////,////,MARINA MUNI,"MARINA, CA - UNITED STATES",Marina Municipal Airport,"Marina, California, United States",OK
    ////,OCQ,KOCQ,KOCQ,OK,1.0,////,////,////,OCONTO-J DOUGLAS BAKE MUNI,"OCONTO, WI - UNITED STATES",Oconto-J Douglas Bake Municipal Airport,"Oconto, Wisconsin, United States",OK
    ////,ODO,KODO,KODO,OK,1.0,////,////,////,ODESSA-SCHLEMEYER FIELD,"ODESSA, TX - UNITED STATES",Odessa-Schlemeyer Field,"Odessa, Texas, United States",OK
    ////,ODX,KODX,KODX,OK,1.0,////,////,////,EVELYN SHARP FIELD,"ORD, NE - UNITED STATES",Evelyn Sharp Field,"Ord, Nebraska, United States",OK
    ////,OEB,KOEB,KOEB,OK,1.0,////,////,////,BRANCH COUNTY MEMORIAL,"COLDWATER, MI - UNITED STATES",Branch County Memorial Airport,"Coldwater, Michigan, United States",OK
    ////,OFP,KOFP,KOFP,OK,1.0,////,////,////,HANOVER COUNTY MUNI,"RICHMOND/ASHLAND, VA - UNITED STATES",Hanover County Municipal Airport,"Richmond/Ashland, Virginia, United States",OK
    ////,OG39,////,////,OK,1.0,////,////,////,LONGVIEW RANCH,"KIMBERLY, OR - UNITED STATES",Longview Ranch Airport,"Kimberly, Oregon, United States",OK
    ////,OGM,KOGM,KOGM,OK,1.0,////,////,////,ONTONAGON COUNTY - SCHUSTER FIELD,"ONTONAGON, MI - UNITED STATES",Ontonagon County - Schuster Field,"Ontonagon, Michigan, United States",OK
    ////,OJA,KOJA,KOJA,OK,1.0,////,////,////,THOMAS P STAFFORD,"WEATHERFORD, OK - UNITED STATES",Thomas P Stafford Airport,"Weatherford, Oklahoma, United States",OK
    ////,OKZ,KOKZ,KOKZ,OK,1.0,////,////,////,KAOLIN FIELD,"SANDERSVILLE, GA - UNITED STATES",Kaolin Field,"Sandersville, Georgia, United States",OK
    ////,OLG,KOLG,KOLG,OK,1.0,////,////,////,SOLON SPRINGS MUNI,"SOLON SPRINGS, WI - UNITED STATES",Solon Springs Municipal Airport,"Solon Springs, Wisconsin, United States",OK
    ////,OLZ,KOLZ,KOLZ,OK,1.0,////,////,////,OELWEIN MUNI,"OELWEIN, IA - UNITED STATES",Oelwein Municipal Airport,"Oelwein, Iowa, United States",OK
    ////,OMH,KOMH,KOMH,OK,1.0,////,////,////,ORANGE COUNTY,"ORANGE, VA - UNITED STATES",Orange County Airport,"Orange, Virginia, United States",OK
    ////,OMN,KOMN,KOMN,OK,1.0,////,////,////,ORMOND BEACH MUNI,"ORMOND BEACH, FL - UNITED STATES",Ormond Beach Municipal Airport,"Ormond Beach, Florida, United States",OK
    ////,ONX,KONX,KONX,OK,1.0,////,////,////,CURRITUCK COUNTY RGNL,"CURRITUCK, NC - UNITED STATES",Currituck County Regional,"Currituck, North Carolina, United States",OK
    ////,ONZ,KONZ,KONZ,OK,1.0,////,////,////,GROSSE ILE MUNI,"DETROIT/GROSSE ILE, MI - UNITED STATES",Grosse Ile Municipal Airport,"Detroit/Grosse Ile, Michigan, United States",OK
    ////,OPN,KOPN,KOPN,OK,1.0,////,////,////,THOMASTON-UPSON COUNTY,"THOMASTON, GA - UNITED STATES",Thomaston-Upson County Airport,"Thomaston, Georgia, United States",OK
    ////,OQN,KOQN,KOQN,OK,1.0,////,////,////,BRANDYWINE,"WEST CHESTER, PA - UNITED STATES",Brandywine Airport,"West Chester, Pennsylvania, United States",OK
    ////,OQU,KOQU,KOQU,OK,1.0,////,////,////,QUONSET STATE,"NORTH KINGSTOWN, RI - UNITED STATES",Quonset State Airport,"North Kingstown, Rhode Island, United States",OK
    ////,ORB,KORB,KORB,OK,1.0,////,////,////,ORR RGNL,"ORR, MN - UNITED STATES",Orr Regional,"Orr, Minnesota, United States",OK
    ////,ORC,KORC,KORC,OK,1.0,////,////,////,ORANGE CITY MUNI,"ORANGE CITY, IA - UNITED STATES",Orange City Municipal Airport,"Orange City, Iowa, United States",OK
    ////,ORE,KORE,KORE,OK,1.0,////,////,////,ORANGE MUNI,"ORANGE, MA - UNITED STATES",Orange Municipal Airport,"Orange, Massachusetts, United States",OK
    ////,ORG,KORG,KORG,OK,1.0,////,////,////,ORANGE COUNTY,"ORANGE, TX - UNITED STATES",Orange County Airport,"Orange, Texas, United States",OK
    ////,OSA,KOSA,KOSA,OK,1.0,////,////,////,MOUNT PLEASANT RGNL,"MOUNT PLEASANT, TX - UNITED STATES",Mount Pleasant Regional,"Mount Pleasant, Texas, United States",OK
    ////,OVL,KOVL,KOVL,OK,1.0,////,////,////,OLIVIA RGNL,"OLIVIA, MN - UNITED STATES",Olivia Regional,"Olivia, Minnesota, United States",OK
    ////,OVS,KOVS,KOVS,OK,1.0,////,////,////,BOSCOBEL,"BOSCOBEL, WI - UNITED STATES",????,"Boscobel, Wisconsin, United States",OK
    ////,OWI,KOWI,KOWI,OK,1.0,////,////,////,OTTAWA MUNI,"OTTAWA, KS - UNITED STATES",Ottawa Municipal Airport,"Ottawa, Kansas, United States",OK
    ////,OWP,KOWP,KOWP,OK,1.0,////,////,////,WILLIAM R POGUE MUNI,"SAND SPRINGS, OK - UNITED STATES",William R Pogue Municipal Airport,"Sand Springs, Oklahoma, United States",OK
    ////,OWX,KOWX,KOWX,OK,1.0,////,////,////,PUTNAM COUNTY,"OTTAWA, OH - UNITED STATES",Putnam County Airport,"Ottawa, Ohio, United States",OK
    ////,OXI,KOXI,KOXI,OK,1.0,////,////,////,STARKE COUNTY,"KNOX, IN - UNITED STATES",Starke County Airport,"Knox, Indiana, United States",OK
    ////,OXV,KOXV,KOXV,OK,1.0,////,////,////,KNOXVILLE MUNI,"KNOXVILLE, IA - UNITED STATES",Knoxville Municipal Airport,"Knoxville, Iowa, United States",OK
    ////,OZS,KOZS,KOZS,OK,1.0,////,////,////,CAMDENTON MEMORIAL-LAKE RGNL,"CAMDENTON, MO - UNITED STATES",Camdenton Memorial-Lake Regional,"Camdenton, Missouri, United States",OK
    ////,OZW,KOZW,KOZW,OK,1.0,////,////,////,LIVINGSTON COUNTY SPENCER J HARDY,"HOWELL, MI - UNITED STATES",Livingston County Spencer J Hardy Airport,"Howell, Michigan, United States",OK
    ////,P08,////,////,OK,1.0,////,////,////,COOLIDGE MUNI,"COOLIDGE, AZ - UNITED STATES",Coolidge Municipal Airport,"Coolidge, Arizona, United States",OK


List for checking certainty of Great Circle Mapper (GAA - GIC)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    GAA,////,////,////,UNK,1.0,Guamal,"Guamal, Colombia",OWNO,////,////,????,"Guamal, Magdalena, Colombia",OK
    GAB,GAB,KGAB,KGAB,OK,1.0,Gabbs,"Gabbs (NV), USA",OWNO,GABBS,"GABBS, NV - UNITED STATES",????,"Gabbs, Nevada, United States",OK
    GAC,////,////,////,UNK,1.0,Gracias,"Gracias, Honduras",OWNO,////,////,????,"Gracias, Lempira, Honduras",OK
    GAD,GAD,KGAD,KGAD,OK,0.64,Municipal,"Gadsden (AL), USA",OWNO,NORTHEAST ALABAMA RGNL,"GADSDEN, AL - UNITED STATES",Northeast Alabama Regional,"Gadsden, Alabama, United States",OK
    GAE,////,////,DTTG,UNK,0.435,Gabes,"Gabes, Tunisia",OWNO,////,////,Matmata International Airport,"Gabès, Tunisia",OK
    GAF,////,////,DTTF,UNK,0.5,Gafsa,"Gafsa, Tunisia",OWNO,////,////,Ksar International Airport,"Gafsa, Tunisia",OK
    GAG,GAG,KGAG,KGAG,OK,1.0,Gage,"Gage (OK), USA",OWNO,GAGE,"GAGE, OK - UNITED STATES",????,"Gage, Oklahoma, United States",OK
    GAH,////,////,YGAY,UNK,1.0,Gayndah,"Gayndah, Australia",OWNO,////,////,????,"Gayndah, Queensland, Australia",OK
    GAI,GAI,KGAI,KGAI,OK,0.865,Montgomery County,"Gaithersburg (MD), USA",OWNO,MONTGOMERY COUNTY AIRPARK,"GAITHERSBURG, MD - UNITED STATES",Montgomery County Airpark,"Gaithersburg, Maryland, United States",OK
    GAJ,////,////,RJSC,UNK,0.667,Junmachi,"Yamagata, Japan",OWNO,////,////,Yamagata Airport,"Yamagata, Yamagata, Japan",OK
    GAL,GAL,PAGA,PAGA,OK,0.419,Galena,"Galena (AK), USA",OWNO,EDWARD G PITKA SR,"GALENA, AK - UNITED STATES",Edward G Pitka Sr. Airport,"Galena, Alaska, United States",OK
    GAM,GAM,PAGM,PAGM,OK,1.0,Gambell,"Gambell (AK), USA",OWNO,GAMBELL,"GAMBELL, AK - UNITED STATES",????,"Gambell, St. Lawrence Island, Alaska, United States",OK
    GAN,////,////,VRMG,UNK,0.8,Gan/Seenu,"Gan Island, Maldives",OWNO,////,////,Gan International Airport,"Gan, Gan Island, Maldives",OK
    GAO,////,////,MUGT,UNK,0.649,Los Canos,"Guantanamo, Cuba",OWNO,////,////,Mariana Grajales,"Guantánamo, Guantánamo, Cuba",OK
    GAP,////,////,////,UNK,1.0,Gusap,"Gusap, Papua New Guinea",OWNO,////,////,????,"Gusap, Morobe, Papua-New Guinea",OK
    GAQ,////,////,GAGO,UNK,0.545,Gao,"Gao, Mali",OWNO,////,////,Gao International Airport,"Gao, Gao, Mali",OK
    GAR,////,////,////,UNK,1.0,Garaina,"Garaina, Papua New Guinea",OWNO,////,////,????,"Garaina, Morobe, Papua-New Guinea",OK
    GAS,////,////,HKGA,UNK,1.0,Garissa,"Garissa, Kenya",OWNO,////,////,????,"Garissa, Kenya",OK
    GAT,////,////,LFNA,UNK,1.0,Tallard,"Gap, France",OWNO,////,////,Tallard,"Gap, Provence-Alpes-Côte d'Azur, France",OK
    GAU,////,////,VEGT,UNK,0.438,Borjhar,"Gawahati, India",OWNO,////,////,Lokpriya Gopinath Bordoloi International Airport,"Guwahati, Assam, India",OK
    GAV,////,////,////,UNK,1.0,Gag Island,"Gag Island, Indonesia",OWNO,////,////,????,"Gag Island, Maluku, Indonesia",OK
    GAW,////,////,VYGG,UNK,1.0,Gangaw,"Gangaw, Myanmar",OWNO,////,////,????,"Gangaw, Magway, Myanmar (Burma)",OK
    GAX,////,////,////,UNK,0.667,Gamba,"Gamba, Gabon",OWNO,////,////,Yenzi Airport,"Gamba, Nyanga, Gabon",OK
    GAY,////,////,VEGY,UNK,1.0,Gaya,"Gaya, India",OWNO,////,////,????,"Gaya, Bihar, India",OK
    GAZ,////,////,////,UNK,1.0,Guasopa,"Guasopa, Papua New Guinea",OWNO,////,////,????,"Guasopa, Milne Bay, Papua-New Guinea",OK
    GBA,////,////,////,UNK,1.0,Big Bay,"Big Bay, Vanuatu",OWNO,////,////,????,"Big Bay, Espíritu Santo Island, Sanma, Vanuatu",OK
    GBB,////,////,UBBQ,UNK,0.533,International,Gabala,IATA,////,////,Gabala International Airport,"Gabala, Qebele (Ibala), Azerbaijan",MAYBE
    GBC,////,////,////,UNK,1.0,Gasuke,"Gasuke, Papua New Guinea",OWNO,////,////,????,"Gasuke, Western, Papua-New Guinea",OK
    GBD,GBD,KGBD,KGBD,OK,0.76,Great Bend,"Great Bend (KS), USA",OWNO,GREAT BEND MUNI,"GREAT BEND, KS - UNITED STATES",Great Bend Municipal Airport,"Great Bend, Kansas, United States",OK
    GBE,////,////,FBSK,UNK,0.984,Sir Seretse Khama Int,"Gaborone, Botswana",OWNO,////,////,Sir Seretse Khama International Airport,"Gaborone, South-East, Botswana",OK
    GBF,////,////,////,UNK,1.0,Negarbo,"Negarbo, Papua New Guinea",OWNO,////,////,????,"Negarbo, Chimbu, Papua-New Guinea",OK
    GBG,GBG,KGBG,KGBG,OK,0.774,Galesburg,"Galesburg (IL), USA",OWNO,GALESBURG MUNI,"GALESBURG, IL - UNITED STATES",Galesburg Municipal Airport,"Galesburg, Illinois, United States",OK
    GBH,GBH,PAGB,PAGB,OK,1.0,Galbraith Lake,"Galbraith Lake (AK), USA",OWNO,GALBRAITH LAKE,"GALBRAITH LAKE, AK - UNITED STATES",????,"Galbraith Lake, Alaska, United States",OK
    GBJ,////,////,TFFM,UNK,0.737,Les Bases,"Marie Galante, Guadeloupe",OWNO,////,////,????,"Marie Galante, Grand Bourg, Guadeloupe",OK
    GBK,////,////,GFGK,UNK,1.0,Gbangbatok,"Gbangbatok, Sierra Leone",OWNO,////,////,????,"Gbangbatok, Sierra Leone",OK
    GBL,////,////,YGBI,UNK,1.0,Goulburn Island,"Goulburn Island, Australia",OWNO,////,////,????,"Goulburn Island, Northern Territory, Australia",OK
    GBM,////,////,////,UNK,0.923,Garbaharey,"Garbaharey, Somalia",OWNO,////,////,????,"Garba Harre, Gedo, Somalia",TO DO CHECK
    GBP,////,////,YGAM,UNK,1.0,Gamboola,"Gamboola, Australia",OWNO,////,////,????,"Gamboola, Queensland, Australia",OK
    GBT,////,////,OING,UNK,nan,////,////,////,////,////,????,"Gorgan, Mazandaran, Iran",UNK
    GBU,////,////,HSKG,UNK,1.0,Khashm El Girba,"Khashm El Girba, Sudan",OWNO,////,////,????,"Khashm El Girba, Gedaref, Sudan",OK
    GBV,////,////,YGIB,UNK,1.0,Gibb River,"Gibb River, Australia",OWNO,////,////,????,"Gibb River, Western Australia, Australia",OK
    GBW,////,////,YGIA,UNK,nan,////,////,////,////,////,Ginbata Airport,"Roy Hill Mine, Western Australia, Australia",UNK
    GBZ,////,////,NZGB,UNK,1.0,Great Barrier Island,"Great Barrier Island, New Zealand",OWNO,////,////,????,"Great Barrier Island, New Zealand",OK
    GCA,////,////,////,UNK,1.0,Guacamaya,"Guacamaya, Colombia",OWNO,////,////,????,"Guacamaya, Caquetá, Colombia",OK
    GCC,GCC,KGCC,KGCC,OK,1.0,Campbell County,"Gillette (WY), USA",OWNO,GILLETTE-CAMPBELL COUNTY,"GILLETTE, WY - UNITED STATES",Gillette-Campbell County Airport,"Gillette, Wyoming, United States",OK
    GCH,////,////,OIAH,UNK,nan,////,////,////,////,////,????,"Gachsaran, Kohgiluyeh va Buyer Ahmad, Iran",UNK
    GCI,////,////,EGJB,UNK,1.0,Guernsey,"Guernsey, Channel Island (UK)",OWNO,////,////,Guernsey Airport,"Forest, Guernsey",OK
    GCJ,////,////,FAGC,UNK,1.0,Grand Central,"Johannesburg, South Africa",OWNO,////,////,Grand Central,"Johannesburg, Gauteng, South Africa",OK
    GCK,GCK,KGCK,KGCK,OK,0.76,Municipal Airport,"Garden City (KS), USA",OWNO,GARDEN CITY RGNL,"GARDEN CITY, KS - UNITED STATES",Garden City Regional,"Garden City, Kansas, United States",OK
    GCM,////,////,MWCR,UNK,1.0,Owen Roberts International,"Grand Cayman Island, Cayman Islands",OWNO,////,////,Owen Roberts International Airport,"Grand Cayman, Cayman Islands",OK
    GCN,GCN,KGCN,KGCN,OK,1.0,National Park,"Grand Canyon (AZ), USA",OWNO,GRAND CANYON NATIONAL PARK,"GRAND CANYON, AZ - UNITED STATES",Grand Canyon National Park Airport,"Grand Canyon, Arizona, United States",OK
    GCW,1G4,////,////,OK,1.0,Grand Canyon West,Grand Canyon West,IATA,GRAND CANYON WEST,"PEACH SPRINGS, AZ - UNITED STATES",Grand Canyon West Airport,"Peach Springs, Arizona, United States",MAYBE
    GCY,GCY,KGCY,KGCY,OK,0.591,Municipal,"Greenville (TN), USA",OWNO,GREENEVILLE-GREENE COUNTY MUNI,"GREENEVILLE, TN - UNITED STATES",Greeneville-Greene County Municipal Airport,"Greeneville, Tennessee, United States",OK
    GDA,////,////,////,UNK,1.0,Gounda,"Gounda, Central African Republic",OWNO,////,////,????,"Gounda, Bamingui-Bangoran (Bamïngï-Bangoran), Central African Republic",OK
    GDC,GYH,KGYH,KGYH,OK,0.653,Donaldson Center,"Greer (SC), USA",OWNO,DONALDSON FIELD,"GREENVILLE, SC - UNITED STATES",Donaldson Field,"Greenville, South Carolina, United States",OK
    GDD,////,////,YGDN,UNK,1.0,Gordon Downs,"Gordon Downs, Australia",OWNO,////,////,????,"Gordon Downs, Western Australia, Australia",OK
    GDE,////,////,HAGO,UNK,1.0,Gode/Iddidole,"Gode/Iddidole, Ethiopia",OWNO,////,////,????,"Gode, Somali, Ethiopia",OK
    GDG,////,////,UHBI,UNK,1.0,Magdagachi,"Magdagachi, Russia",OWNO,////,////,????,"Magdagachi, Amurskaya, Russian Federation (Russia)",OK
    GDH,3Z8,////,////,OK,0.692,SPB,"Golden Horn (AK), USA",OWNO,GOLDEN HORN LODGE,"GOLDEN HORN LODGE, AK - UNITED STATES",Golden Horn Lodge SPB,"Golden Horn Lodge, Alaska, United States",OK
    GDI,////,////,////,UNK,1.0,Gordil,"Gordil, Central African Republic",OWNO,////,////,????,"Gordil, Vakaga, Central African Republic",OK
    GDJ,////,////,FZWC,UNK,1.0,Gandajika,"Gandajika, Congo (DRC)",OWNO,////,////,????,"Gandajika, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    GDL,////,////,MMGL,UNK,0.692,Miguel Hidal,"Guadalajara, Mexico",OWNO,////,////,Don Miguel Hidalgo y Costilla International Airport,"Guadalajara, Jalisco, México",OK
    GDN,////,////,EPGD,UNK,0.643,Rebiechowo,"Gdansk, Poland",OWNO,////,////,Lech Walesa,"Gdansk, Pomorskie, Poland",OK
    GDO,////,////,SVGD,UNK,1.0,Vare Maria,"Guasdualito, Venezuela",OWNO,////,////,Vare Maria,"Guasdualito, Apure, Venezuela",OK
    GDP,////,////,SNGD,UNK,0.636,Guadalupe,"Guadalupe, Brazil",OWNO,////,////,Aeropuerto Guadalupe,"Guadalupe, Piauí, Brazil",OK
    GDQ,////,////,HAGN,UNK,1.0,Gondar,"Gondar, Ethiopia",OWNO,////,////,Gondar Airport,"Gondar, Amara, Ethiopia",OK
    GDT,////,MBGT,MBGT,OK,0.142,Grand Turk Island,"Grand Turk Island, Turks and Caicos",OWNO,JAGS MCCARTNEY INTL,"COCKBURN TOWN, - TURKS AND CAICOS ISLANDS",J.A.G.S. McCartney International Airport,"Grand Turk, Grand Turk Island, Turks and Caicos Islands",TO DO CHECK
    GDV,GDV,KGDV,KGDV,OK,1.0,Dawson Community,"Glendive (MT), USA",OWNO,DAWSON COMMUNITY,"GLENDIVE, MT - UNITED STATES",Dawson Community Airport,"Glendive, Montana, United States",OK
    GDW,GDW,KGDW,KGDW,OK,0.468,Gladwin,"Gladwin (MI), USA",OWNO,GLADWIN ZETTEL MEMORIAL,"GLADWIN, MI - UNITED STATES",Gladwin Zettel Memorial Airport,"Gladwin, Michigan, United States",OK
    GDX,////,////,UHMM,UNK,0.737,Magadan,"Magadan, Russia",OWNO,////,////,Sokol,"Magadan, Magadanskaya, Russian Federation (Russia)",OK
    GDZ,////,////,URKG,UNK,1.0,Gelendzik,"Gelendzik, Russia",OWNO,////,////,Gelendzhik Airport,"Gelendzhik, Krasnodarskiy, Russian Federation (Russia)",OK
    GEA,////,////,NWWM,UNK,1.0,Magenta,"Noumea, New Caledonia",OWNO,////,////,Magenta,"Nouméa, New Caledonia",OK
    GEB,////,////,WAMJ,UNK,1.0,Gebe,"Gebe, Indonesia",OWNO,////,////,????,"Gebe, Gebe Island, Maluku, Indonesia",OK
    GEC,////,////,LCGK,UNK,0.533,Gecitkale,"Gecitkale, Cyprus",OWNO,////,////,Gazimagosa International Airport,"Geçitkale, Cyprus",OK
    GED,GED,KGED,KGED,OK,0.669,Sussex County,"Georgetown (DE), USA",OWNO,DELAWARE COASTAL,"GEORGETOWN, DE - UNITED STATES",Delaware Coastal Airport,"Georgetown, Delaware, United States",OK
    GEE,////,////,YGTO,UNK,1.0,George Town,"George Town, Australia",OWNO,////,////,????,"George Town, Tasmania, Australia",OK
    GEF,////,////,AGEV,UNK,0.471,Geva Airstrip,"Geva, Solomon Islands",OWNO,////,////,Geva,"Liangia, Vella Lavella Island, Solomon Islands",OK
    GEG,GEG,KGEG,KGEG,OK,1.0,International,"Spokane (WA), USA",OWNO,SPOKANE INTL,"SPOKANE, WA - UNITED STATES",Spokane International Airport,"Spokane, Washington, United States",OK
    GEL,////,////,SBNM,UNK,0.667,Sepe Tiaraju,"Santo Angelo, Brazil",OWNO,////,////,????,"Santo Ângelo, Rio Grande do Sul, Brazil",OK
    GEM,////,////,FGMY,UNK,nan,////,////,////,////,////,President Obiang Nguema International Airport,"Mengomeyén, Wele-Nzas, Equatorial Guinea",UNK
    GEO,////,////,SYCJ,UNK,1.0,Cheddi Jagan International,"Georgetown, Guyana",OWNO,////,////,Cheddi Jagan International Airport,"Georgetown, Demerara-Mahaica, Guyana",OK
    GER,////,////,MUNG,UNK,1.0,Rafael Cabrera,"Nueva Gerona, Cuba",OWNO,////,////,Rafael Cabrera Airport,"Nueva Gerona, Isla de la Juventud, Cuba",OK
    GES,////,////,RPMB,UNK,1.0,Buayan,"General Santos, Philippines",OWNO,////,////,Buayan,"General Santos, Philippines",OK
    GET,////,////,YGEL,UNK,1.0,Geraldton,"Geraldton, Australia",OWNO,////,////,????,"Geraldton, Western Australia, Australia",OK
    GEV,////,////,ESNG,UNK,1.0,Gallivare,"Gallivare, Sweden",OWNO,////,////,????,"Gällivare, Norrbottens län, Sweden",OK
    GEW,////,////,////,UNK,1.0,Gewoia,"Gewoia, Papua New Guinea",OWNO,////,////,????,"Gewoia, Northern, Papua-New Guinea",OK
    GEX,////,////,YGLG,UNK,1.0,Geelong,"Geelong, Australia",OWNO,////,////,????,"Geelong, Victoria, Australia",OK
    GEY,GEY,KGEY,KGEY,OK,1.0,South Big Horn County,"Greybull (WY), USA",OWNO,SOUTH BIG HORN COUNTY,"GREYBULL, WY - UNITED STATES",South Big Horn County Airport,"Greybull, Wyoming, United States",OK
    GFA,GFA,KGFA,KGFA,OK,0.774,Malmstrom AFB,"Great Falls (MT), USA",OWNO,MALMSTROM AFB,"GREAT FALLS, MT - UNITED STATES",Malmstrom AFB Heliport,"Great Falls, Montana, United States",OK
    GFD,GFD,KGFD,KGFD,OK,1.0,Pope Field,"Greenfield (IN), USA",OWNO,POPE FIELD,"GREENFIELD, IN - UNITED STATES",Pope Field,"Greenfield, Indiana, United States",OK
    GFE,////,////,////,UNK,0.56,Grenfell,"Grenfell, Australia",OWNO,////,////,Railway Station,"Grenfell, New South Wales, Australia",OK
    GFF,////,////,YGTH,UNK,1.0,Griffith,"Griffith, Australia",OWNO,////,////,????,"Griffith, New South Wales, Australia",OK
    GFK,GFK,KGFK,KGFK,OK,0.834,Grand Forks,"Grand Forks (ND), USA",OWNO,GRAND FORKS INTL,"GRAND FORKS, ND - UNITED STATES",Grand Forks International Airport,"Grand Forks, North Dakota, United States",OK
    GFL,GFL,KGFL,KGFL,OK,0.653,Warren County,"Glens Falls (NY), USA",OWNO,FLOYD BENNETT MEMORIAL,"GLENS FALLS, NY - UNITED STATES",Floyd Bennett Memorial Airport,"Glens Falls, New York, United States",OK
    GFN,////,////,YGFN,UNK,1.0,Grafton,"Grafton, Australia",OWNO,////,////,????,"Grafton, New South Wales, Australia",OK
    GFO,////,////,SYBT,UNK,1.0,Bartica,"Bartica, Guyana",OWNO,////,////,????,"Bartica, Upper Demerara-Berbice, Guyana",OK
    GFR,////,////,LFRF,UNK,1.0,Granville,"Granville, France",OWNO,////,////,Granville Airport,"Granville, Basse-Normandie (Lower Normandy), France",OK
    GFY,////,////,FYGF,UNK,1.0,Grootfontein,"Grootfontein, Namibia",OWNO,////,////,????,"Grootfontein, Namibia",OK
    GGC,////,////,////,UNK,0.667,Lumbala,"Lumbala, Angola",OWNO,////,////,????,"Lumbala N'guimbo, Angola",MAYBE
    GGD,////,////,YGDS,UNK,1.0,Gregory Downs,"Gregory Downs, Australia",OWNO,////,////,????,"Gregory Downs, Queensland, Australia",OK
    GGE,GGE,KGGE,KGGE,OK,0.79,Georgetown,"Georgetown (SC), USA",OWNO,GEORGETOWN COUNTY,"GEORGETOWN, SC - UNITED STATES",Georgetown County Airport,"Georgetown, South Carolina, United States",OK
    GGG,GGG,KGGG,KGGG,OK,0.396,Gregg County Airport,"Longview (TX), USA",OWNO,EAST TEXAS RGNL,"LONGVIEW, TX - UNITED STATES",East Texas Regional,"Longview, Texas, United States",OK
    GGL,////,////,////,UNK,1.0,Gilgal,"Gilgal, Colombia",OWNO,////,////,????,"Gilgal, Chocó, Colombia",OK
    GGM,////,////,HKKG,UNK,nan,////,////,////,////,////,Kakamega Airport,"Kakamega, Kenya",UNK
    GGN,////,////,DIGA,UNK,1.0,Gagnoa,"Gagnoa, Cote d'Ivoire",OWNO,////,////,????,"Gagnoa, Fromager, Côte d'Ivoire (Ivory Coast)",OK
    GGO,////,////,DIGL,UNK,1.0,Guiglo,"Guiglo, Cote d'Ivoire",OWNO,////,////,Guiglo Airport,"Guiglo, 18 Montagnes (Dix-Huit Montagnes), Côte d'Ivoire (Ivory Coast)",OK
    GGR,////,////,HCMW,UNK,0.625,Garoe,"Garoe, Somalia",OWNO,////,////,Garowe International Airport,"Garowe, Nugaal, Somalia",OK
    GGS,////,////,SAWR,UNK,1.0,Gobernador Gregores,"Gobernador Gregores, Argentina",OWNO,////,////,????,"Gobernador Gregores, Santa Cruz, Argentina",OK
    GGT,////,MYEF,MYEF,OK,1.0,Exuma International,"George Town, Bahamas",OWNO,EXUMA INTL,"GEORGE TOWN, - BAHAMAS",Exuma International Airport,"Moss Town, Great Exuma Island, Bahamas",OK
    GGW,GGW,KGGW,KGGW,OK,1.0,International,"Glasgow (MT), USA",OWNO,WOKAL FIELD/GLASGOW INTL,"GLASGOW, MT - UNITED STATES",Wokal Field/Glasgow International Airport,"Glasgow, Montana, United States",OK
    GHA,////,////,DAUG,UNK,0.706,Noumerate,"Ghardaia, Algeria",OWNO,////,////,Noumérat-Moufdi Zakaria Airport,"Ghardaia, Ghardaïa, Algeria",OK
    GHB,////,MYEM,MYEM,OK,1.0,Governors Harbour,"Governors Harbour, Bahamas",OWNO,GOVERNORS HARBOUR,"GOVERNORS HARBOUR, - BAHAMAS",????,"Governors Harbour, Eleuthera, Bahamas",OK
    GHC,////,MYBG,MYBG,OK,0.81,Great Harbour,"Great Harbour, Bahamas",OWNO,GREAT HARBOUR CAY,"BULLOCKS HARBOUR, - BAHAMAS",Great Harbour Cay,"Bullock Harbour, Great Harbour Cay, Berry Islands, Bahamas",MAYBE
    GHE,////,////,////,UNK,1.0,Garachine,"Garachine, Panama",OWNO,////,////,????,"Garachiné, Darién, Panamá",OK
    GHM,GHM,KGHM,KGHM,OK,1.0,Municipal,"Centerville (TN), USA",OWNO,CENTERVILLE MUNI,"CENTERVILLE, TN - UNITED STATES",Centerville Municipal Airport,"Centerville, Tennessee, United States",OK
    GHN,////,////,ZUGH,UNK,0.909,Guanghan,"Guanghan, PR China",OWNO,////,////,Chongdu/Changhan,"Guanghan, Sichuan, China",OK
    GHT,////,////,HLGT,UNK,1.0,Ghat,"Ghat, Libya",OWNO,////,////,????,"Ghat, Libyan Arab Jamahiriya (Libya)",OK
    GHU,////,////,SAAG,UNK,1.0,Gualeguaychu,"Gualeguaychu, Argentina",OWNO,////,////,????,"Gualeguaychú, Entre Ríos, Argentina",OK
    GIB,////,////,LXGB,UNK,0.688,North Front,"Gibraltar, Gibraltar",OWNO,////,////,Gibraltar International Airport,"Gibraltar, Gibraltar",OK
    GIC,////,////,YBOI,UNK,1.0,Boigu Island,"Boigu Island, Australia",OWNO,////,////,????,"Boigu Island, Queensland, Australia",OK

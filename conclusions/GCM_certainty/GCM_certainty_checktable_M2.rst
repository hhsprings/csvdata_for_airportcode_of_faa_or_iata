
List for checking certainty of Great Circle Mapper (MDP - MHF)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MDP,////,////,WAKD,UNK,1.0,Mindiptana,"Mindiptana, Indonesia",OWNO,////,////,????,"Mindiptana, Papua, Indonesia",OK
    MDQ,////,////,SAZM,UNK,1.0,Mar Del Plata,"Mar Del Plata, Argentina",OWNO,////,////,????,"Mar del Plata, Buenos Aires, Argentina",OK
    MDS,////,////,MBMC,UNK,1.0,Middle Caicos,"Middle Caicos, Turks and Caicos Islands",OWNO,////,////,????,"Middle Caicos, Turks and Caicos Islands",OK
    MDT,MDT,KMDT,KMDT,OK,1.0,Harrisburg International,"Harrisburg (PA), USA",OWNO,HARRISBURG INTL,"HARRISBURG, PA - UNITED STATES",Harrisburg International Airport,"Harrisburg, Pennsylvania, United States",OK
    MDU,////,////,AYMN,UNK,1.0,Mendi,"Mendi, Papua New Guinea",OWNO,////,////,????,"Mendi, Southern Highlands, Papua-New Guinea",OK
    MDV,////,////,////,UNK,1.0,Medouneu,"Medouneu, Gabon",OWNO,////,////,????,"Médouneu, Woleu-Ntem, Gabon",OK
    MDW,MDW,KMDW,KMDW,OK,0.814,Midway,"Chicago (IL), USA",OWNO,CHICAGO MIDWAY INTL,"CHICAGO, IL - UNITED STATES",Chicago Midway International Airport,"Chicago, Illinois, United States",OK
    MDX,////,////,SATM,UNK,1.0,Mercedes,"Mercedes, Argentina",OWNO,////,////,????,"Mercedes, Corrientes, Argentina",OK
    MDY,MDY,PMDY,PMDY,OK,0.557,Sand Island Field,"Midway Island, US Minor Outlying Islands",OWNO,HENDERSON FIELD,"MIDWAY ATOLL, MQ - UNITED STATES",Henderson Field,"Sand Island, Midway Atoll, United States Minor Outlying Islands",MAYBE
    MDZ,////,////,SAME,UNK,0.533,El Plumerillo,"Mendoza, Argentina",OWNO,////,////,Governor Francisco Gabrielli International Airport,"Mendoza, Mendoza, Argentina",OK
    MEA,////,////,SBME,UNK,1.0,Macae,"Macae, Brazil",OWNO,////,////,????,"Macaé, Rio de Janeiro, Brazil",OK
    MEB,////,////,YMEN,UNK,1.0,Essendon,"Melbourne, Australia",OWNO,////,////,Essendon Airport,"Melbourne, Victoria, Australia",OK
    MEC,////,////,SEMT,UNK,0.375,Manta,"Manta, Ecuador",OWNO,////,////,Eloy Alfaro International Airport,"Manta, Manabí, Ecuador",OK
    MED,////,////,OEMA,UNK,0.833,Mohammad Bin Abdulaziz,"Madinah, Saudi Arabia",OWNO,////,////,Prince Mohammad bin Abdulaziz International Airport,"Madinah, Saudi Arabia",OK
    MEE,////,////,NWWR,UNK,0.5,Mare,"Mare, New Caledonia",OWNO,////,////,La Roche,"Maré, Loyalty Islands, New Caledonia",OK
    MEF,////,////,////,UNK,1.0,Melfi,"Melfi, Chad",OWNO,////,////,Melfi Airport,"Melfi, Guéra, Chad",OK
    MEG,////,////,FNMA,UNK,1.0,Malange,"Malange, Angola",OWNO,////,////,????,"Malange, Angola",OK
    MEH,////,////,ENMH,UNK,1.0,Mehamn,"Mehamn, Norway",OWNO,////,////,????,"Mehamn, Norway",OK
    MEI,MEI,KMEI,KMEI,OK,1.0,Key Field,"Meridian (MS), USA",OWNO,KEY FIELD,"MERIDIAN, MS - UNITED STATES",Key Field,"Meridian, Mississippi, United States",OK
    MEJ,GKJ,KGKJ,KGKJ,OK,0.806,Meadville,"Meadville (PA), USA",OWNO,PORT MEADVILLE,"MEADVILLE, PA - UNITED STATES",Port Meadville Airport,"Meadville, Pennsylvania, United States",OK
    MEK,////,////,GMFM,UNK,1.0,Meknes,"Meknes, Morocco",OWNO,////,////,????,"Meknes/Bassatine, Morocco",OK
    MEL,////,////,YMML,UNK,0.894,Tullamarine,"Melbourne, Australia",OWNO,////,////,Tullamarine International Airport,"Melbourne, Victoria, Australia",OK
    MEM,MEM,KMEM,KMEM,OK,1.0,International,"Memphis (TN), USA",OWNO,MEMPHIS INTL,"MEMPHIS, TN - UNITED STATES",Memphis International Airport,"Memphis, Tennessee, United States",OK
    MEN,////,////,LFNB,UNK,1.0,Brenoux,"Mende, France",OWNO,////,////,Brenoux,"Mende, Languedoc-Roussillon, France",OK
    MEO,MQI,KMQI,KMQI,OK,1.0,Dare County Regional,"Manteo (NC), USA",OWNO,DARE COUNTY RGNL,"MANTEO, NC - UNITED STATES",Dare County Regional,"Manteo, North Carolina, United States",OK
    MEP,////,////,WMAU,UNK,1.0,Mersing,"Mersing, Malaysia",OWNO,////,////,????,"Mersing, Johor, Malaysia",OK
    MEQ,////,////,WITC,UNK,1.0,Seunagan,"Meulaboh, Indonesia",OWNO,////,////,Seunagan,"Meulaboh, Aceh, Indonesia",OK
    MER,MER,KMER,KMER,OK,0.815,Castle AFB,"Merced (CA), USA",OWNO,CASTLE,"ATWATER, CA - UNITED STATES",Castle Airport,"Atwater, California, United States",OK
    MES,////,////,WIMK,UNK,0.582,"Polania Int'l, since 25 July 2013 Soewondo AFB","Medan, Indonesia",OWNO,////,////,Soewondo AFB,"Medan, Sumatera Utara, Indonesia",OK
    MET,////,////,YMOT,UNK,1.0,Moreton,"Moreton, Australia",OWNO,////,////,????,"Moreton, Queensland, Australia",OK
    MEU,////,////,SBMD,UNK,1.0,Monte Dourado,"Monte Dourado, Brazil",OWNO,////,////,Monte Dourado,"Almeirim, Pará, Brazil",OK
    MEV,MEV,KMEV,KMEV,OK,0.413,Douglas County,"Minden (NV), USA",OWNO,MINDEN-TAHOE,"MINDEN, NV - UNITED STATES",Minden-Tahoe Airport,"Minden, Nevada, United States",OK
    MEW,////,////,FZVM,UNK,1.0,Mweka,"Mweka, Congo (DRC)",OWNO,////,////,Mweka Airport,"Mweka, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    MEX,////,////,MMMX,UNK,0.868,Juarez International,"Mexico City, Mexico",OWNO,////,////,Benito Juárez International Airport,"México City, Distrito Federal, México",OK
    MEY,////,////,VNMG,UNK,1.0,Meghauli,"Meghauli, Nepal",OWNO,////,////,????,"Meghauli, Nepal",OK
    MEZ,////,////,////,UNK,1.0,Messina,"Messina, South Africa",OWNO,////,////,????,"Messina, Limpopo, South Africa",OK
    MFA,////,////,HTMA,UNK,1.0,Mafia,"Mafia, Tanzania",OWNO,////,////,Mafia Airport,"Mafia Island, Coast (Pwani), Tanzania",OK
    MFB,////,////,////,UNK,1.0,Monfort,"Monfort, Colombia",OWNO,////,////,????,"Monfort, Colombia",OK
    MFC,////,////,FXMF,UNK,1.0,Mafeteng,"Mafeteng, Lesotho",OWNO,////,////,????,"Mafeteng, Lesotho",OK
    MFD,MFD,KMFD,KMFD,OK,0.722,Lahm Municipal,"Mansfield (OH), USA",OWNO,MANSFIELD LAHM RGNL,"MANSFIELD, OH - UNITED STATES",Mansfield Lahm Regional,"Mansfield, Ohio, United States",OK
    MFE,MFE,KMFE,KMFE,OK,0.626,Miller International,"Mission (TX), USA",OWNO,MC ALLEN MILLER INTL,"MC ALLEN, TX - UNITED STATES",Mc Allen Miller International Airport,"Mc Allen, Texas, United States",OK
    MFF,////,////,FOOD,UNK,1.0,Moanda,"Moanda, Gabon",OWNO,////,////,????,"Moanda, Haut-Ogooué, Gabon",OK
    MFG,////,////,OPMF,UNK,1.0,Muzaffarabad,"Muzaffarabad, Pakistan",OWNO,////,////,????,"Muzaffarabad, Azad Jammu and Kashmir (Azad Kashmir), Pakistan",OK
    MFH,67L,////,////,OK,1.0,Mesquite,"Mesquite (NV), USA",OWNO,MESQUITE,"MESQUITE, NV - UNITED STATES",????,"Mesquite, Nevada, United States",OK
    MFI,MFI,KMFI,KMFI,OK,1.0,Municipal,"Marshfield (WI), USA",OWNO,MARSHFIELD MUNI,"MARSHFIELD, WI - UNITED STATES",Marshfield Municipal Airport,"Marshfield, Wisconsin, United States",OK
    MFJ,////,////,NFMO,UNK,1.0,Moala,"Moala, Fiji",OWNO,////,////,????,"Moala, Moala Island, Fiji",OK
    MFK,////,////,RCMT,UNK,0.588,Matsu,"Matsu, Taiwan",OWNO,////,////,Matsu Beigan,"Beigan, Matsu Island, Taiwan",MAYBE
    MFL,////,////,////,UNK,1.0,Mount Full Stop,"Mount Full Stop, Australia",OWNO,////,////,????,"Mt. Full Stop, Queensland, Australia",MAYBE
    MFM,////,////,VMMC,UNK,0.815,Macau,"Macau, Macau (SAR), PR China",OWNO,////,////,Macau International Airport,"Macau, Taipa Island, Macau",OK
    MFN,////,////,NZMF,UNK,1.0,Milford Sound,"Milford Sound, New Zealand",OWNO,////,////,????,"Milford Sound, New Zealand",OK
    MFO,////,////,////,UNK,1.0,Manguna,"Manguna, Papua New Guinea",OWNO,////,////,????,"Manguna, East New Britain, Papua-New Guinea",OK
    MFP,////,////,YMCR,UNK,1.0,Manners Creek,"Manners Creek, Australia",OWNO,////,////,????,"Manners Creek, Northern Territory, Australia",OK
    MFQ,////,////,DRRM,UNK,1.0,Maradi,"Maradi, Niger",OWNO,////,////,????,"Maradi, Niger",OK
    MFR,MFR,KMFR,KMFR,OK,0.391,Jackson County,"Medford (OR), USA",OWNO,ROGUE VALLEY INTL - MEDFORD,"MEDFORD, OR - UNITED STATES",Rogue Valley International - Medford,"Medford, Oregon, United States",OK
    MFS,////,////,SKMF,UNK,1.0,Miraflores,"Miraflores, Colombia",OWNO,////,////,Miraflores Airport,"Miraflores, Guaviare, Colombia",OK
    MFU,////,////,FLMF,UNK,1.0,Mfuwe,"Mfuwe, Zambia",OWNO,////,////,????,"Mfuwe, Eastern, Zambia",OK
    MFV,MFV,KMFV,KMFV,OK,1.0,Accomack County,"Melfa (VA), USA",OWNO,ACCOMACK COUNTY,"MELFA, VA - UNITED STATES",Accomack County Airport,"Melfa, Virginia, United States",OK
    MFX,////,////,LFKX,UNK,1.0,Meribel,"Meribel, France",OWNO,////,////,Meribel Airport,"Meribel, Rhône-Alpes, France",OK
    MFY,////,////,////,UNK,1.0,Mayfa'ah,"Mayfa'ah, Yemen",OWNO,////,////,????,"Mayfa'ah, Yemen",OK
    MGA,////,////,MNMG,UNK,0.847,Augusto C Sandino,"Managua, Nicaragua",OWNO,////,////,Augusto César Sandino International Airport,"Managua, Managua, Nicaragua",OK
    MGB,////,////,YMTG,UNK,1.0,Mount Gambier,"Mount Gambier, Australia",OWNO,////,////,????,"Mount Gambier, South Australia, Australia",OK
    MGC,MGC,KMGC,KMGC,OK,0.506,Michigan City Airport,"Michigan City (IN), USA",OWNO,MICHIGAN CITY MUNI-PHILLIPS FIELD,"MICHIGAN CITY, IN - UNITED STATES",Michigan City Municipal-Phillips Field,"Michigan City, Indiana, United States",OK
    MGD,////,////,SLMG,UNK,1.0,Magdalena,"Magdalena, Bolivia",OWNO,////,////,Magdalena Airport,"Magdalena, Iténez, El Beni, Bolivia",OK
    MGE,MGE,KMGE,KMGE,OK,0.613,Dobbins AFB,"Marietta (GA), USA",OWNO,DOBBINS AIR RESERVE BASE,"MARIETTA, GA - UNITED STATES",Dobbins Air Reserve Base Airport,"Marietta, Georgia, United States",OK
    MGF,////,////,SBMG,UNK,0.438,Maringa,"Maringa, Brazil",OWNO,////,////,Aeroporto Regional de Maringá,"Maringá, Paraná, Brazil",OK
    MGG,////,////,////,UNK,1.0,Margarima,"Margarima, Papua New Guinea",OWNO,////,////,????,"Margarima, Southern Highlands, Papua-New Guinea",OK
    MGH,////,////,FAMG,UNK,1.0,Margate,"Margate, South Africa",OWNO,////,////,Margate Airport,"Margate, KwaZulu-Natal, South Africa",OK
    MGI,XS10,////,////,UNK,0.593,Matagorda AFB,"Matagorda Island (TX), USA",OWNO,////,////,Aransas National Wildlife Refuge Airport,"Matagorda Island, Texas, United States",OK
    MGJ,MGJ,KMGJ,KMGJ,OK,1.0,Orange County,"Montgomery (NY), USA",OWNO,ORANGE COUNTY,"MONTGOMERY, NY - UNITED STATES",Orange County Airport,"Montgomery, New York, United States",OK
    MGK,////,////,VYMT,UNK,0.941,Mong Ton,"Mong Ton, Myanmar",OWNO,////,////,????,"Mong Tong, Shan, Myanmar (Burma)",MAYBE
    MGL,////,////,EDLN,UNK,0.652,Moenchen-Gl. - DUS Exp,"Dusseldorf, Germany",OWNO,////,////,Düsseldorf-Mönchengladbach,"Mönchengladbach, North Rhine-Westphalia, Germany",TO DO CHECK
    MGM,MGM,KMGM,KMGM,OK,0.667,Dannelly Fld,"Montgomery (AL), USA",OWNO,MONTGOMERY RGNL (DANNELLY FIELD),"MONTGOMERY, AL - UNITED STATES",Montgomery Regional,"Montgomery, Alabama, United States",OK
    MGN,////,////,SKMG,UNK,0.865,Baracoa,"Magangue, Colombia",OWNO,////,////,Baracoa Regional Airport,"Magangué, Bolívar, Colombia",OK
    MGO,////,////,////,UNK,1.0,Manega,"Manega, Gabon",OWNO,////,////,????,"Manega, Ogooué-Maritime, Gabon",OK
    MGP,////,////,////,UNK,1.0,Manga,"Manga, Papua New Guinea",OWNO,////,////,????,"Manga, New Ireland, Papua-New Guinea",OK
    MGQ,////,////,HCMM,UNK,1.0,Aden Adde International Airport,"Mogadishu, Somalia",OWNO,////,////,Aden Adde International Airport,"Mogadishu, Banaadir, Somalia",OK
    MGR,MGR,KMGR,KMGR,OK,0.621,Thomasville,"Moultrie (GA), USA",OWNO,MOULTRIE MUNI,"MOULTRIE, GA - UNITED STATES",Moultrie Municipal Airport,"Moultrie, Georgia, United States",OK
    MGS,////,////,NCMG,UNK,1.0,Mangaia Island,"Mangaia Island, Cook Islands",OWNO,////,////,????,"Mangaia Island, Cook Islands",OK
    MGT,////,////,YMGB,UNK,1.0,Milingimbi,"Milingimbi, Australia",OWNO,////,////,????,"Milingimbi, Northern Territory, Australia",OK
    MGU,////,////,VYMN,UNK,1.0,Manaung,"Manaung, Myanmar",OWNO,////,////,Manaung Airport,"Manaung, Rakhine, Myanmar (Burma)",OK
    MGV,////,////,YMGR,UNK,1.0,Margaret River,"Margaret River Station, Australia",OWNO,////,////,Margaret River Station Airport,"Margaret River Station, Western Australia, Australia",OK
    MGW,MGW,KMGW,KMGW,OK,0.378,Morgantown,"Morgantown (WV), USA",OWNO,MORGANTOWN MUNI-WALTER L BILL HART FLD,"MORGANTOWN, WV - UNITED STATES",Morgantown Municipal-Walter L Bill Hart Field,"Morgantown, West Virginia, United States",OK
    MGX,////,////,FOGI,UNK,1.0,Moabi,"Moabi, Gabon",OWNO,////,////,Moabi Airport,"Moabi, Nyanga, Gabon",OK
    MGY,MGY,KMGY,KMGY,OK,1.0,Dayton-Wright Brothers Airport (Montgomery County),"Dayton (OH), USA",OWNO,DAYTON-WRIGHT BROTHERS,"DAYTON, OH - UNITED STATES",Dayton-Wright Brothers Airport,"Dayton, Ohio, United States",OK
    MGZ,////,////,VYME,UNK,1.0,Myeik Airport,"Myeik, Myanmar",OWNO,////,////,????,"Myeik, Taninthayi, Myanmar (Burma)",OK
    MHA,////,////,SYMD,UNK,1.0,Mahdia,"Mahdia, Guyana",OWNO,////,////,????,"Mahdia, Potaro-Siparuni, Guyana",OK
    MHC,////,////,SCPQ,UNK,1.0,Mocopulli,Castro,IATA,////,////,Mocopulli Airport,"Dalcahue, Chiloé Island, Los Lagos, Chile",MAYBE
    MHD,////,////,OIMM,UNK,1.0,Shahid Hashemi Nejad Airport,"Mashad, Iran",OWNO,////,////,Mashhad International (Shahid Hashemi Nejad)),"Mashhad, Khorasan-e Razavi, Iran",OK
    MHE,MHE,KMHE,KMHE,OK,1.0,Municipal,"Mitchell (SD), USA",OWNO,MITCHELL MUNI,"MITCHELL, SD - UNITED STATES",Mitchell Municipal Airport,"Mitchell, South Dakota, United States",OK
    MHF,////,////,////,UNK,1.0,Morichal,"Morichal, Colombia",OWNO,////,////,????,"Morichal, Guainía, Colombia",OK

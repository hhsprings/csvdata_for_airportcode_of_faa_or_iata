
List for checking certainty of Great Circle Mapper (FAA LID DEW - FAA LID GKY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,DEW,KDEW,KDEW,OK,1.0,////,////,////,DEER PARK,"DEER PARK, WA - UNITED STATES",????,"Deer Park, Washington, United States",OK
    ////,DIJ,KDIJ,KDIJ,OK,1.0,////,////,////,DRIGGS-REED MEMORIAL,"DRIGGS, ID - UNITED STATES",Driggs-Reed Memorial Airport,"Driggs, Idaho, United States",OK
    ////,DKB,KDKB,KDKB,OK,1.0,////,////,////,DE KALB TAYLOR MUNI,"DE KALB, IL - UNITED STATES",De Kalb Taylor Municipal Airport,"De Kalb, Illinois, United States",OK
    ////,DKR,KDKR,KDKR,OK,1.0,////,////,////,HOUSTON COUNTY,"CROCKETT, TX - UNITED STATES",Houston County Airport,"Crockett, Texas, United States",OK
    ////,DKX,KDKX,KDKX,OK,1.0,////,////,////,KNOXVILLE DOWNTOWN ISLAND,"KNOXVILLE, TN - UNITED STATES",Knoxville Downtown Island Airport,"Knoxville, Tennessee, United States",OK
    ////,DLL,KDLL,KDLL,OK,1.0,////,////,////,BARABOO WISCONSIN DELLS,"BARABOO, WI - UNITED STATES",Baraboo Wisconsin Dells Airport,"Baraboo, Wisconsin, United States",OK
    ////,DLZ,KDLZ,KDLZ,OK,1.0,////,////,////,DELAWARE MUNI - JIM MOORE FIELD,"DELAWARE, OH - UNITED STATES",Delaware Municipal - Jim Moore Field,"Delaware, Ohio, United States",OK
    ////,DMW,KDMW,KDMW,OK,1.0,////,////,////,CARROLL COUNTY RGNL/JACK B POAGE FIELD,"WESTMINSTER, MD - UNITED STATES",Carroll County Regional/Jack B Poage Field,"Westminster, Maryland, United States",OK
    ////,DNL,KDNL,KDNL,OK,1.0,////,////,////,DANIEL FIELD,"AUGUSTA, GA - UNITED STATES",Daniel Field,"Augusta, Georgia, United States",OK
    ////,DPL,KDPL,KDPL,OK,0.954,////,////,////,DUPLIN CO,"KENANSVILLE, NC - UNITED STATES",Duplin County Airport,"Kenansville, North Carolina, United States",OK
    ////,DQH,KDQH,KDQH,OK,1.0,////,////,////,DOUGLAS MUNI,"DOUGLAS, GA - UNITED STATES",Douglas Municipal Airport,"Douglas, Georgia, United States",OK
    ////,DTO,KDTO,KDTO,OK,1.0,////,////,////,DENTON ENTERPRISE,"DENTON, TX - UNITED STATES",Denton Enterprise Airport,"Denton, Texas, United States",OK
    ////,DUB,KDUB,KDUB,OK,1.0,////,////,////,DUBOIS MUNI,"DUBOIS, WY - UNITED STATES",Dubois Municipal Airport,"Dubois, Wyoming, United States",OK
    ////,DUH,KDUH,KDUH,OK,1.0,////,////,////,TOLEDO SUBURBAN,"LAMBERTVILLE, MI - UNITED STATES",Toledo Suburban Airport,"Lambertville, Michigan, United States",OK
    ////,DUX,KDUX,KDUX,OK,1.0,////,////,////,MOORE COUNTY,"DUMAS, TX - UNITED STATES",Moore County Airport,"Dumas, Texas, United States",OK
    ////,DVK,KDVK,KDVK,OK,1.0,////,////,////,STUART POWELL FIELD,"DANVILLE, KY - UNITED STATES",Stuart Powell Field,"Danville, Kentucky, United States",OK
    ////,DVP,KDVP,KDVP,OK,1.0,////,////,////,SLAYTON MUNI,"SLAYTON, MN - UNITED STATES",Slayton Municipal Airport,"Slayton, Minnesota, United States",OK
    ////,DWA,KDWA,KDWA,OK,1.0,////,////,////,YOLO COUNTY,"DAVIS/WOODLAND/WINTERS, CA - UNITED STATES",Yolo County Airport,"Davis/Woodland/Winters, California, United States",OK
    ////,DWU,KDWU,KDWU,OK,1.0,////,////,////,ASHLAND RGNL,"ASHLAND, KY - UNITED STATES",Ashland Regional,"Ashland, Kentucky, United States",OK
    ////,DWX,KDWX,KDWX,OK,1.0,////,////,////,DIXON,"DIXON, WY - UNITED STATES",????,"Dixon, Wyoming, United States",OK
    ////,DXE,KDXE,KDXE,OK,1.0,////,////,////,DEXTER MUNI,"DEXTER, MO - UNITED STATES",Dexter Municipal Airport,"Dexter, Missouri, United States",OK
    ////,DXX,KDXX,KDXX,OK,1.0,////,////,////,LAC QUI PARLE COUNTY,"MADISON, MN - UNITED STATES",Lac Qui Parle County Airport,"Madison, Minnesota, United States",OK
    ////,DYA,KDYA,KDYA,OK,1.0,////,////,////,DEMOPOLIS RGNL,"DEMOPOLIS, AL - UNITED STATES",Demopolis Regional,"Demopolis, Alabama, United States",OK
    ////,DYB,KDYB,KDYB,OK,1.0,////,////,////,SUMMERVILLE,"SUMMERVILLE, SC - UNITED STATES",????,"Summerville, South Carolina, United States",OK
    ////,DYR,KDYR,KDYR,OK,1.0,////,////,////,DYERSBURG RGNL,"DYERSBURG, TN - UNITED STATES",Dyersburg Regional,"Dyersburg, Tennessee, United States",OK
    ////,DYT,KDYT,KDYT,OK,1.0,////,////,////,SKY HARBOR,"DULUTH, MN - UNITED STATES",Sky Harbor Airport,"Duluth, Minnesota, United States",OK
    ////,DZB,KDZB,KDZB,OK,1.0,////,////,////,HORSESHOE BAY RESORT,"HORSESHOE BAY, TX - UNITED STATES",Horseshoe Bay Resort Airport,"Horseshoe Bay, Texas, United States",OK
    ////,DZJ,KDZJ,KDZJ,OK,1.0,////,////,////,BLAIRSVILLE,"BLAIRSVILLE, GA - UNITED STATES",????,"Blairsville, Georgia, United States",OK
    ////,E25,////,////,OK,1.0,////,////,////,WICKENBURG MUNI,"WICKENBURG, AZ - UNITED STATES",Wickenburg Municipal Airport,"Wickenburg, Arizona, United States",OK
    ////,E55,////,////,OK,1.0,////,////,////,OCEAN RIDGE,"GUALALA, CA - UNITED STATES",Ocean Ridge Airport,"Gualala, California, United States",OK
    ////,E63,////,////,OK,1.0,////,////,////,GILA BEND MUNI,"GILA BEND, AZ - UNITED STATES",Gila Bend Municipal Airport,"Gila Bend, Arizona, United States",OK
    ////,E91,////,////,OK,1.0,////,////,////,CHINLE MUNI,"CHINLE, AZ - UNITED STATES",Chinle Municipal Airport,"Chinle, Arizona, United States",OK
    ////,EBA,KEBA,KEBA,OK,1.0,////,////,////,ELBERT COUNTY-PATZ FIELD,"ELBERTON, GA - UNITED STATES",Elbert County-Patz Field,"Elberton, Georgia, United States",OK
    ////,EBG,KEBG,KEBG,OK,1.0,////,////,////,SOUTH TEXAS INTL AT EDINBURG,"EDINBURG, TX - UNITED STATES",South Texas International at Edinburg,"Edinburg, Texas, United States",OK
    ////,ECU,KECU,KECU,OK,1.0,////,////,////,EDWARDS COUNTY,"ROCKSPRINGS, TX - UNITED STATES",Edwards County Airport,"Rocksprings, Texas, United States",OK
    ////,EDC,KEDC,KEDC,OK,1.0,////,////,////,AUSTIN EXECUTIVE,"AUSTIN, TX - UNITED STATES",Austin Executive Airport,"Austin, Texas, United States",OK
    ////,EDJ,KEDJ,KEDJ,OK,1.0,////,////,////,BELLEFONTAINE RGNL,"BELLEFONTAINE, OH - UNITED STATES",Bellefontaine Regional,"Bellefontaine, Ohio, United States",OK
    ////,EDU,KEDU,KEDU,OK,1.0,////,////,////,UNIVERSITY,"DAVIS, CA - UNITED STATES",University Airport,"Davis, California, United States",OK
    ////,EEO,KEEO,KEEO,OK,1.0,////,////,////,MEEKER COULTER FLD,"MEEKER, CO - UNITED STATES",Meeker Coulter Field,"Meeker, Colorado, United States",OK
    ////,EET,KEET,KEET,OK,1.0,////,////,////,SHELBY COUNTY,"ALABASTER, AL - UNITED STATES",Shelby County Airport,"Alabaster, Alabama, United States",OK
    ////,EFT,KEFT,KEFT,OK,1.0,////,////,////,MONROE MUNI,"MONROE, WI - UNITED STATES",Monroe Municipal Airport,"Monroe, Wisconsin, United States",OK
    ////,EGT,KEGT,KEGT,OK,1.0,////,////,////,WELLINGTON MUNI,"WELLINGTON, KS - UNITED STATES",Wellington Municipal Airport,"Wellington, Kansas, United States",OK
    ////,EHA,KEHA,KEHA,OK,1.0,////,////,////,ELKHART-MORTON COUNTY,"ELKHART, KS - UNITED STATES",Elkhart-Morton County Airport,"Elkhart, Kansas, United States",OK
    ////,EHO,KEHO,KEHO,OK,1.0,////,////,////,SHELBY-CLEVELAND COUNTY RGNL,"SHELBY, NC - UNITED STATES",Shelby-Cleveland County Regional,"Shelby, North Carolina, United States",OK
    ////,EHR,KEHR,KEHR,OK,1.0,////,////,////,HENDERSON CITY-COUNTY,"HENDERSON, KY - UNITED STATES",Henderson City-County Airport,"Henderson, Kentucky, United States",OK
    ////,EIK,KEIK,KEIK,OK,1.0,////,////,////,ERIE MUNI,"ERIE, CO - UNITED STATES",Erie Municipal Airport,"Erie, Colorado, United States",OK
    ////,EKQ,KEKQ,KEKQ,OK,1.0,////,////,////,WAYNE COUNTY,"MONTICELLO, KY - UNITED STATES",Wayne County Airport,"Monticello, Kentucky, United States",OK
    ////,EKS,KEKS,KEKS,OK,1.0,////,////,////,ENNIS - BIG SKY,"ENNIS, MT - UNITED STATES",Ennis - Big Sky Airport,"Ennis, Montana, United States",OK
    ////,EKY,KEKY,KEKY,OK,1.0,////,////,////,BESSEMER,"BESSEMER, AL - UNITED STATES",????,"Bessemer, Alabama, United States",OK
    ////,EMV,KEMV,KEMV,OK,1.0,////,////,////,EMPORIA-GREENSVILLE RGNL,"EMPORIA, VA - UNITED STATES",Emporia-Greensville Regional,"Emporia, Virginia, United States",OK
    ////,EOE,KEOE,KEOE,OK,1.0,////,////,////,NEWBERRY COUNTY,"NEWBERRY, SC - UNITED STATES",Newberry County Airport,"Newberry, South Carolina, United States",OK
    ////,EPM,KEPM,KEPM,OK,1.0,////,////,////,EASTPORT MUNI,"EASTPORT, ME - UNITED STATES",Eastport Municipal Airport,"Eastport, Maine, United States",OK
    ////,EQY,KEQY,KEQY,OK,1.0,////,////,////,CHARLOTTE-MONROE EXECUTIVE,"MONROE, NC - UNITED STATES",Charlotte-Monroe Executive Airport,"Monroe, North Carolina, United States",OK
    ////,ERY,KERY,KERY,OK,1.0,////,////,////,LUCE COUNTY,"NEWBERRY, MI - UNITED STATES",Luce County Airport,"Newberry, Michigan, United States",OK
    ////,ETC,KETC,KETC,OK,1.0,////,////,////,TARBORO-EDGECOMBE,"TARBORO, NC - UNITED STATES",Tarboro-Edgecombe Airport,"Tarboro, North Carolina, United States",OK
    ////,ETH,KETH,KETH,OK,1.0,////,////,////,WHEATON MUNI,"WHEATON, MN - UNITED STATES",Wheaton Municipal Airport,"Wheaton, Minnesota, United States",OK
    ////,EUL,KEUL,KEUL,OK,1.0,////,////,////,CALDWELL INDUSTRIAL,"CALDWELL, ID - UNITED STATES",Caldwell Industrial Airport,"Caldwell, Idaho, United States",OK
    ////,EVB,KEVB,KEVB,OK,1.0,////,////,////,NEW SMYRNA BEACH MUNI,"NEW SMYRNA BEACH, FL - UNITED STATES",New Smyrna Beach Municipal Airport,"New Smyrna Beach, Florida, United States",OK
    ////,EXX,KEXX,KEXX,OK,1.0,////,////,////,DAVIDSON COUNTY,"LEXINGTON, NC - UNITED STATES",Davidson County Airport,"Lexington, North Carolina, United States",OK
    ////,EYE,KEYE,KEYE,OK,1.0,////,////,////,EAGLE CREEK AIRPARK,"INDIANAPOLIS, IN - UNITED STATES",Eagle Creek Airpark,"Indianapolis, Indiana, United States",OK
    ////,EYF,KEYF,KEYF,OK,1.0,////,////,////,CURTIS L BROWN JR FIELD,"ELIZABETHTOWN, NC - UNITED STATES",Curtis L Brown Jr. Field,"Elizabethtown, North Carolina, United States",OK
    ////,EZF,KEZF,KEZF,OK,1.0,////,////,////,SHANNON,"FREDERICKSBURG, VA - UNITED STATES",Shannon Airport,"Fredericksburg, Virginia, United States",OK
    ////,EZM,KEZM,KEZM,OK,1.0,////,////,////,HEART OF GEORGIA RGNL,"EASTMAN, GA - UNITED STATES",Heart of Georgia Regional,"Eastman, Georgia, United States",OK
    ////,EZS,KEZS,KEZS,OK,1.0,////,////,////,SHAWANO MUNI,"SHAWANO, WI - UNITED STATES",Shawano Municipal Airport,"Shawano, Wisconsin, United States",OK
    ////,F05,////,////,OK,1.0,////,////,////,WILBARGER COUNTY,"VERNON, TX - UNITED STATES",Wilbarger County Airport,"Vernon, Texas, United States",OK
    ////,F57,////,////,OK,0.81,////,////,////,JACK BROWNS,"WINTER HAVEN, FL - UNITED STATES",Jack Brown's Seaplane Base,"Winter Haven, Florida, United States",OK
    ////,FA08,////,////,OK,1.0,////,////,////,ORLAMPA INC,"POLK CITY, FL - UNITED STATES",Orlampa Inc. Airport,"Polk City, Florida, United States",OK
    ////,FA54,////,////,OK,1.0,////,////,////,CORAL CREEK,"PLACIDA, FL - UNITED STATES",Coral Creek Airport,"Placida, Florida, United States",OK
    ////,FBG,KFBG,KFBG,OK,1.0,////,////,////,SIMMONS AAF,"FORT BRAGG, NC - UNITED STATES",Simmons AAF Airport,"Fort Bragg, North Carolina, United States",OK
    ////,FCI,KFCI,KFCI,OK,1.0,////,////,////,RICHMOND EXECUTIVE-CHESTERFIELD COUNTY,"RICHMOND, VA - UNITED STATES",Richmond Executive-Chesterfield County Airport,"Richmond, Virginia, United States",OK
    ////,FCS,KFCS,KFCS,OK,1.0,////,////,////,BUTTS AAF (FORT CARSON),"FORT CARSON, CO - UNITED STATES",Butts AAF Airport,"Fort Carson, Colorado, United States",OK
    ////,FDW,KFDW,KFDW,OK,1.0,////,////,////,FAIRFIELD COUNTY,"WINNSBORO, SC - UNITED STATES",Fairfield County Airport,"Winnsboro, South Carolina, United States",OK
    ////,FFC,KFFC,KFFC,OK,1.0,////,////,////,ATLANTA RGNL FALCON FIELD,"ATLANTA, GA - UNITED STATES",Atlanta Regional Falcon Field,"Atlanta, Georgia, United States",OK
    ////,FFX,KFFX,KFFX,OK,1.0,////,////,////,FREMONT MUNI,"FREMONT, MI - UNITED STATES",Fremont Municipal Airport,"Fremont, Michigan, United States",OK
    ////,FGX,KFGX,KFGX,OK,1.0,////,////,////,FLEMING-MASON,"FLEMINGSBURG, KY - UNITED STATES",Fleming-Mason Airport,"Flemingsburg, Kentucky, United States",OK
    ////,FHB,KFHB,KFHB,OK,1.0,////,////,////,FERNANDINA BEACH MUNI,"FERNANDINA BEACH, FL - UNITED STATES",Fernandina Beach Municipal Airport,"Fernandina Beach, Florida, United States",OK
    ////,FIG,KFIG,KFIG,OK,1.0,////,////,////,CLEARFIELD-LAWRENCE,"CLEARFIELD, PA - UNITED STATES",Clearfield-Lawrence Airport,"Clearfield, Pennsylvania, United States",OK
    ////,FIN,KFIN,KFIN,OK,1.0,////,////,////,FLAGLER EXECUTIVE,"PALM COAST, FL - UNITED STATES",Flagler Executive Airport,"Palm Coast, Florida, United States",OK
    ////,FIT,KFIT,KFIT,OK,1.0,////,////,////,FITCHBURG MUNI,"FITCHBURG, MA - UNITED STATES",Fitchburg Municipal Airport,"Fitchburg, Massachusetts, United States",OK
    ////,FKA,KFKA,KFKA,OK,1.0,////,////,////,FILLMORE COUNTY,"PRESTON, MN - UNITED STATES",Fillmore County Airport,"Preston, Minnesota, United States",OK
    ////,FKS,KFKS,KFKS,OK,1.0,////,////,////,FRANKFORT DOW MEMORIAL FIELD,"FRANKFORT, MI - UNITED STATES",Frankfort Dow Memorial Field,"Frankfort, Michigan, United States",OK
    ////,FLY,KFLY,KFLY,OK,1.0,////,////,////,MEADOW LAKE,"COLORADO SPRINGS, CO - UNITED STATES",Meadow Lake Airport,"Colorado Springs, Colorado, United States",OK
    ////,FMM,KFMM,KFMM,OK,1.0,////,////,////,FORT MORGAN MUNI,"FORT MORGAN, CO - UNITED STATES",Fort Morgan Municipal Airport,"Fort Morgan, Colorado, United States",OK
    ////,FNB,KFNB,KFNB,OK,1.0,////,////,////,BRENNER FIELD,"FALLS CITY, NE - UNITED STATES",Brenner Field,"Falls City, Nebraska, United States",OK
    ////,FOA,KFOA,KFOA,OK,1.0,////,////,////,FLORA MUNI,"FLORA, IL - UNITED STATES",Flora Municipal Airport,"Flora, Illinois, United States",OK
    ////,FOT,KFOT,KFOT,OK,1.0,////,////,////,ROHNERVILLE,"FORTUNA, CA - UNITED STATES",Rohnerville Airport,"Fortuna, California, United States",OK
    ////,FOZ,KFOZ,KFOZ,OK,1.0,////,////,////,BIGFORK MUNI,"BIGFORK, MN - UNITED STATES",Bigfork Municipal Airport,"Bigfork, Minnesota, United States",OK
    ////,FPK,KFPK,KFPK,OK,1.0,////,////,////,FITCH H BEACH,"CHARLOTTE, MI - UNITED STATES",Fitch H Beach Airport,"Charlotte, Michigan, United States",OK
    ////,FQD,KFQD,KFQD,OK,1.0,////,////,////,RUTHERFORD CO - MARCHMAN FIELD,"RUTHERFORDTON, NC - UNITED STATES",Rutherford Co. - Marchman Field,"Rutherfordton, North Carolina, United States",OK
    ////,FSE,KFSE,KFSE,OK,1.0,////,////,////,FOSSTON MUNI,"FOSSTON, MN - UNITED STATES",Fosston Municipal Airport,"Fosston, Minnesota, United States",OK
    ////,FSO,KFSO,KFSO,OK,1.0,////,////,////,FRANKLIN COUNTY STATE,"HIGHGATE, VT - UNITED STATES",Franklin County State Airport,"Highgate, Vermont, United States",OK
    ////,FTG,KFTG,KFTG,OK,1.0,////,////,////,FRONT RANGE,"DENVER, CO - UNITED STATES",Front Range Airport,"Denver, Colorado, United States",OK
    ////,FVX,KFVX,KFVX,OK,1.0,////,////,////,FARMVILLE RGNL,"FARMVILLE, VA - UNITED STATES",Farmville Regional,"Farmville, Virginia, United States",OK
    ////,FWB,KFWB,KFWB,OK,1.0,////,////,////,BRANSON WEST MUNI - EMERSON FIELD,"BRANSON WEST, MO - UNITED STATES",Branson West Municipal - Emerson Field,"Branson West, Missouri, United States",OK
    ////,FWC,KFWC,KFWC,OK,1.0,////,////,////,FAIRFIELD MUNI,"FAIRFIELD, IL - UNITED STATES",Fairfield Municipal Airport,"Fairfield, Illinois, United States",OK
    ////,FWN,KFWN,KFWN,OK,1.0,////,////,////,SUSSEX,"SUSSEX, NJ - UNITED STATES",????,"Sussex, New Jersey, United States",OK
    ////,FWS,KFWS,KFWS,OK,1.0,////,////,////,FORT WORTH SPINKS,"FORT WORTH, TX - UNITED STATES",Fort Worth Spinks Airport,"Fort Worth, Texas, United States",OK
    ////,FYG,KFYG,KFYG,OK,1.0,////,////,////,WASHINGTON RGNL,"WASHINGTON, MO - UNITED STATES",Washington Regional,"Washington, Missouri, United States",OK
    ////,FYJ,KFYJ,KFYJ,OK,1.0,////,////,////,MIDDLE PENINSULA RGNL,"WEST POINT, VA - UNITED STATES",Middle Peninsula Regional,"West Point, Virginia, United States",OK
    ////,FZG,KFZG,KFZG,OK,1.0,////,////,////,FITZGERALD MUNI,"FITZGERALD, GA - UNITED STATES",Fitzgerald Municipal Airport,"Fitzgerald, Georgia, United States",OK
    ////,FZI,KFZI,KFZI,OK,1.0,////,////,////,FOSTORIA METROPOLITAN,"FOSTORIA, OH - UNITED STATES",Fostoria Metropolitan Airport,"Fostoria, Ohio, United States",OK
    ////,FZY,KFZY,KFZY,OK,1.0,////,////,////,OSWEGO COUNTY,"FULTON, NY - UNITED STATES",Oswego County Airport,"Fulton, New York, United States",OK
    ////,GA04,////,////,OK,1.0,////,////,////,MALLARDS LANDING,"LOCUST GROVE, GA - UNITED STATES",Mallards Landing Airport,"Locust Grove, Georgia, United States",OK
    ////,GAF,KGAF,KGAF,OK,1.0,////,////,////,HUTSON FIELD,"GRAFTON, ND - UNITED STATES",Hutson Field,"Grafton, North Dakota, United States",OK
    ////,GAO,KGAO,KGAO,OK,1.0,////,////,////,SOUTH LAFOURCHE LEONARD MILLER JR,"GALLIANO, LA - UNITED STATES",South Lafourche Leonard Miller Jr. Airport,"Galliano, Louisiana, United States",OK
    ////,GBR,KGBR,KGBR,OK,1.0,////,////,////,WALTER J KOLADZA,"GREAT BARRINGTON, MA - UNITED STATES",Walter J Koladza Airport,"Great Barrington, Massachusetts, United States",OK
    ////,GCM,KGCM,KGCM,OK,1.0,////,////,////,CLAREMORE RGNL,"CLAREMORE, OK - UNITED STATES",Claremore Regional,"Claremore, Oklahoma, United States",OK
    ////,GDB,KGDB,KGDB,OK,1.0,////,////,////,GRANITE FALLS MUNI/LENZEN-ROE-FAGEN MEMORIAL FIELD,"GRANITE FALLS, MN - UNITED STATES",Granite Falls Municipal/Lenzen-Roe-Fagen Memorial Field,"Granite Falls, Minnesota, United States",OK
    ////,GDJ,KGDJ,KGDJ,OK,1.0,////,////,////,GRANBURY RGNL,"GRANBURY, TX - UNITED STATES",Granbury Regional,"Granbury, Texas, United States",OK
    ////,GDM,KGDM,KGDM,OK,1.0,////,////,////,GARDNER MUNI,"GARDNER, MA - UNITED STATES",Gardner Municipal Airport,"Gardner, Massachusetts, United States",OK
    ////,GEO,KGEO,KGEO,OK,1.0,////,////,////,BROWN COUNTY,"GEORGETOWN, OH - UNITED STATES",Brown County Airport,"Georgetown, Ohio, United States",OK
    ////,GEU,KGEU,KGEU,OK,1.0,////,////,////,GLENDALE MUNI,"GLENDALE, AZ - UNITED STATES",Glendale Municipal Airport,"Glendale, Arizona, United States",OK
    ////,GEV,KGEV,KGEV,OK,1.0,////,////,////,ASHE COUNTY,"JEFFERSON, NC - UNITED STATES",Ashe County Airport,"Jefferson, North Carolina, United States",OK
    ////,GEZ,KGEZ,KGEZ,OK,1.0,////,////,////,SHELBYVILLE MUNI,"SHELBYVILLE, IN - UNITED STATES",Shelbyville Municipal Airport,"Shelbyville, Indiana, United States",OK
    ////,GGI,KGGI,KGGI,OK,1.0,////,////,////,GRINNELL RGNL,"GRINNELL, IA - UNITED STATES",Grinnell Regional,"Grinnell, Iowa, United States",OK
    ////,GGP,KGGP,KGGP,OK,1.0,////,////,////,LOGANSPORT/CASS COUNTY,"LOGANSPORT, IN - UNITED STATES",Logansport/Cass County Airport,"Logansport, Indiana, United States",OK
    ////,GHG,KGHG,KGHG,OK,1.0,////,////,////,MARSHFIELD MUNI - GEORGE HARLOW FIELD,"MARSHFIELD, MA - UNITED STATES",Marshfield Municipal - George Harlow Field,"Marshfield, Massachusetts, United States",OK
    ////,GHW,KGHW,KGHW,OK,1.0,////,////,////,GLENWOOD MUNI,"GLENWOOD, MN - UNITED STATES",Glenwood Municipal Airport,"Glenwood, Minnesota, United States",OK
    ////,GIC,KGIC,KGIC,OK,1.0,////,////,////,IDAHO COUNTY,"GRANGEVILLE, ID - UNITED STATES",Idaho County Airport,"Grangeville, Idaho, United States",OK
    ////,GKY,KGKY,KGKY,OK,1.0,////,////,////,ARLINGTON MUNI,"ARLINGTON, TX - UNITED STATES",Arlington Municipal Airport,"Arlington, Texas, United States",OK

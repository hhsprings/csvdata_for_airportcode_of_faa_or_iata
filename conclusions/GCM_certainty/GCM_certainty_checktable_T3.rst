
List for checking certainty of Great Circle Mapper (TKE - TOE)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    TKE,TKE,////,////,OK,0.859,Tenakee SPB,"Tenakee Springs (AK), USA",OWNO,TENAKEE,"TENAKEE SPRINGS, AK - UNITED STATES",Tenakee SPB,"Tenakee Springs, Alaska, United States",OK
    TKF,TRK,KTRK,KTRK,OK,0.87,Truckee,"Truckee (CA), USA",OWNO,TRUCKEE-TAHOE,"TRUCKEE, CA - UNITED STATES",Truckee-Tahoe Airport,"Truckee, California, United States",OK
    TKG,////,////,WICT,UNK,1.0,Branti,"Bandar Lampung, Indonesia",OWNO,////,////,Branti,"Bandar Lampung, Lampung, Indonesia",OK
    TKH,////,////,VTPI,UNK,0.556,Takhli,"Takhli, Thailand",OWNO,////,////,Nakhon Sawan,"Takhli, Nakhon Sawan, Thailand",OK
    TKI,57A,////,////,OK,0.774,Tokeen,"Tokeen (AK), USA",OWNO,TOKEEN,"TOKEEN, AK - UNITED STATES",Tokeen SPB,"Tokeen, Alaska, United States",OK
    TKJ,TKJ,////,PATJ,UNK,1.0,Tok,"Tok (AK), USA",OWNO,////,////,????,"Tok, Alaska, United States",OK
    TKK,TKK,PTKK,PTKK,OK,0.132,Truk,"Truk, Micronesia",OWNO,CHUUK INTL,"WENO ISLAND, - MICRONESIA, FED STATES OF",Chuuk International Airport,"Weno Island, Chuuk, Micronesia (Federated States of)",TO DO CHECK
    TKL,TKL,////,////,OK,0.805,Taku SPB,"Taku Lodge (AK), USA",OWNO,TAKU LODGE,"TAKU LODGE, AK - UNITED STATES",Taku Lodge SPB,"Taku Lodge, Alaska, United States",OK
    TKN,////,////,RJKN,UNK,1.0,Tokunoshima,"Tokunoshima, Japan",OWNO,////,////,Tokunoshima Airport,"Tokunoshima, Satsunan Islands, Kagoshima, Japan",OK
    TKO,////,////,FXTK,UNK,1.0,Tlokoeng,"Tlokoeng, Lesotho",OWNO,////,////,????,"Tlokoeng, Lesotho",OK
    TKP,////,////,NTGT,UNK,1.0,Takapoto,"Takapoto, French Polynesia",OWNO,////,////,????,"Takapoto, French Polynesia",OK
    TKQ,////,////,HTKA,UNK,1.0,Kigoma,"Kigoma, Tanzania",OWNO,////,////,Kigoma Airport,"Ujiji, Kigoma, Tanzania",OK
    TKR,////,////,VGSG,UNK,1.0,Thakurgaon,"Thakurgaon, Bangladesh",OWNO,////,////,????,"Thakurgaon, Bangladesh",OK
    TKS,////,////,RJOS,UNK,1.0,Tokushima,"Tokushima, Japan",OWNO,////,////,Tokushima Airport,"Tokushima, Shikoku Island, Tokushima, Japan",OK
    TKT,////,////,VTPT,UNK,1.0,Tak,"Tak, Thailand",OWNO,////,////,????,"Tak, Tak, Thailand",OK
    TKU,////,////,EFTU,UNK,1.0,Turku,"Turku, Finland",OWNO,////,////,????,"Turku, Varsinais-Suomi (Egentliga Finland (Southwest Finland)), Finland",OK
    TKV,////,////,NTGO,UNK,1.0,Tatakoto,"Tatakoto, French Polynesia",OWNO,////,////,????,"Tatakoto, French Polynesia",OK
    TKW,////,////,////,UNK,1.0,Tekin,"Tekin, Papua New Guinea",OWNO,////,////,????,"Tekin, Sandaun, Papua-New Guinea",OK
    TKX,////,////,NTKR,UNK,1.0,Takaroa,"Takaroa, French Polynesia",OWNO,////,////,????,"Takaroa, French Polynesia",OK
    TKY,////,////,YTKY,UNK,1.0,Turkey Creek,"Turkey Creek, Australia",OWNO,////,////,????,"Turkey Creek, Western Australia, Australia",OK
    TKZ,////,////,NZTO,UNK,1.0,Tokoroa,"Tokoroa, New Zealand",OWNO,////,////,????,"Tokoroa, New Zealand",OK
    TLA,TER,PATE,PATE,OK,1.0,Teller,"Teller (AK), USA",OWNO,TELLER,"TELLER, AK - UNITED STATES",????,"Teller, Alaska, United States",OK
    TLB,////,////,OPTA,UNK,0.8,Tarbela,"Tarbela, Pakistan",OWNO,////,////,Tarbela Dam Airport,"Tarbela Dam, Khyber Pakhtunkhwa, Pakistan",MAYBE
    TLC,////,////,MMTO,UNK,0.27,Toluca,"Toluca, Mexico",OWNO,////,////,Licenciado Adolfo López Mateos International Airport,"Toluca, México, México",OK
    TLD,////,////,FBTL,UNK,1.0,Tuli Lodge,"Tuli Lodge, Botswana",OWNO,////,////,Tuli Lodge Airport,"Tuli Lodge, Central, Botswana",OK
    TLE,////,////,FMST,UNK,1.0,Tulear,"Tulear, Madagascar",OWNO,////,////,????,"Tulear, Madagascar",OK
    TLF,2K5,////,////,OK,1.0,Telida,"Telida (AK), USA",OWNO,TELIDA,"TELIDA, AK - UNITED STATES",????,"Telida, Alaska, United States",OK
    TLG,////,////,////,UNK,1.0,Tulagi Island,"Tulagi Island, Solomon Islands",OWNO,////,////,????,"Tulagi Island, Solomon Islands",OK
    TLH,TLH,KTLH,KTLH,OK,0.766,Municipal,"Tallahassee (FL), USA",OWNO,TALLAHASSEE INTL,"TALLAHASSEE, FL - UNITED STATES",Tallahassee International Airport,"Tallahassee, Florida, United States",OK
    TLI,////,////,WAMI,UNK,1.0,Tolitoli,"Tolitoli, Indonesia",OWNO,////,////,????,"Tolitoli, Sulawesi Tengah, Indonesia",OK
    TLK,////,////,UECT,UNK,1.0,Talakan,Talakan,IATA,////,////,Talakan Airport,"Talakan, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    TLL,////,////,EETN,UNK,1.0,Ulemiste,"Tallinn, Estonia",OWNO,////,////,Ülemiste,"Tallinn, Estonia",OK
    TLM,////,////,DAON,UNK,0.686,Zenata,"Tlemcen, Algeria",OWNO,////,////,Zenata-Messali El Hadj Airport,"Tlemcen, Tlemcen, Algeria",OK
    TLN,////,////,LFTH,UNK,0.615,Hyeres,"Toulon, France",OWNO,////,////,Le Palyvestre,"Hyeres, Provence-Alpes-Côte d'Azur, France",OK
    TLO,////,////,////,UNK,1.0,Tol,"Tol, Papua New Guinea",OWNO,////,////,????,"Tol, East New Britain, Papua-New Guinea",OK
    TLP,////,////,////,UNK,1.0,Tumolbil,"Tumolbil, Papua New Guinea",OWNO,////,////,????,"Tumolbil, Sandaun, Papua-New Guinea",OK
    TLQ,////,////,ZWTP,UNK,nan,////,////,////,////,////,Turpan Jiaohe Airport,"Turpan, Xinjiang, China",UNK
    TLR,TLR,KTLR,KTLR,OK,0.576,Tulare,"Tulare (CA), USA",OWNO,MEFFORD FIELD,"TULARE, CA - UNITED STATES",Mefford Field,"Tulare, California, United States",OK
    TLS,////,////,LFBO,UNK,1.0,Blagnac Airport,"Toulouse, France",OWNO,////,////,Blagnac,"Toulouse, Midi-Pyrénées, France",OK
    TLT,TLT,////,////,OK,1.0,Tuluksak,"Tuluksak (AK), USA",OWNO,TULUKSAK,"TULUKSAK, AK - UNITED STATES",????,"Tuluksak, Alaska, United States",OK
    TLU,////,////,SKTL,UNK,0.273,Tolu,"Tolu, Colombia",OWNO,////,////,Golfo de Morrosquillo Airport,"Tolú, Sucre, Colombia",OK
    TLV,////,////,LLBG,UNK,0.833,Ben Gurion International,"Tel Aviv Yafo, Israel",OWNO,////,////,Ben Gurion,"Tel Aviv, Israel",OK
    TLW,////,////,////,UNK,1.0,Talasea,"Talasea, Papua New Guinea",OWNO,////,////,????,"Talasea, West New Britain, Papua-New Guinea",OK
    TLX,////,////,SCTL,UNK,0.476,Talca,"Talca, Chile",OWNO,////,////,Panguilemo,"Talca, Maule, Chile",OK
    TLY,////,////,UHWP,UNK,nan,////,////,////,////,////,Plastun Airport,"Plastun, Primorskiy, Russian Federation (Russia)",UNK
    TLZ,////,////,SWKT,UNK,1.0,Catalao,"Catalao, Brazil",OWNO,////,////,????,"Catalão, Goiás, Brazil",OK
    TMA,TMA,KTMA,KTMA,OK,1.0,Henry Tift Myers,"Tifton (GA), USA",OWNO,HENRY TIFT MYERS,"TIFTON, GA - UNITED STATES",Henry Tift Myers Airport,"Tifton, Georgia, United States",OK
    TMB,TMB,KTMB,KTMB,OK,0.468,Tamiami,"Miami (FL), USA",OWNO,MIAMI EXECUTIVE,"MIAMI, FL - UNITED STATES",Miami Executive Airport,"Miami, Florida, United States",OK
    TMC,////,////,WADT,UNK,1.0,Tambolaka,"Tambolaka, Indonesia",OWNO,////,////,????,"Tambolaka, Nusa Tenggara Timur, Indonesia",OK
    TMD,////,////,GQNH,UNK,1.0,Timbedra,"Timbedra, Mauritania",OWNO,////,////,Timbedra Airport,"Timbedra, Mauritania",OK
    TME,////,////,SKTM,UNK,0.267,Tame,"Tame, Colombia",OWNO,////,////,Gabriel Vargas Santos Airport,"Tame, Arauca, Colombia",OK
    TMF,////,////,VRNT,UNK,nan,////,////,////,////,////,Thimarafushi Airport,"Thimarafushi, Thaa Atoll, Maldives",UNK
    TMG,////,////,WBKM,UNK,1.0,Tomanggong,"Tomanggong, Malaysia",OWNO,////,////,????,"Tommanggong, Sabah, Malaysia",OK
    TMH,////,////,WAKT,UNK,1.0,Tanahmerah,"Tanahmerah, Indonesia",OWNO,////,////,????,"Tanahmerah, Papua, Indonesia",OK
    TMI,////,////,VNTR,UNK,1.0,Tumling Tar,"Tumling Tar, Nepal",OWNO,////,////,????,"Tumling Tar, Nepal",OK
    TMJ,////,////,UTST,UNK,1.0,Termez,"Termez, Uzbekistan",OWNO,////,////,Termez Airport,"Termez, Surxondaryo, Uzbekistan",OK
    TMK,////,////,////,UNK,0.6,Tamky,"Tamky, Viet Nam",OWNO,////,////,????,"Tam-Ky, Vietnam",OK
    TML,////,////,DGLE,UNK,1.0,Tamale,"Tamale, Ghana",OWNO,////,////,????,"Tamale, Ghana",OK
    TMM,////,////,FMMT,UNK,0.714,Tamatave,"Tamatave, Madagascar",OWNO,////,////,????,"Toamasina, Madagascar",TO DO CHECK
    TMN,////,////,NGTM,UNK,1.0,Tamana Island,"Tamana Island, Kiribati",OWNO,////,////,????,"Tamana Island, Kiribati",OK
    TMO,////,////,SVTM,UNK,1.0,Tumeremo,"Tumeremo, Venezuela",OWNO,////,////,????,"Tumeremo, Bolívar, Venezuela",OK
    TMP,////,////,EFTP,UNK,1.0,Tampere-Pirkkala,"Tampere, Finland",OWNO,////,////,Pirkkala,"Tampere, Pirkanmaa (Birkaland (Pirkanmaa)), Finland",OK
    TMQ,////,////,DFEM,UNK,1.0,Tambao,"Tambao, Burkina Faso",OWNO,////,////,Tambao Airport,"Tambao, Burkina Faso",OK
    TMR,////,////,DAAT,UNK,0.744,Aguemar,"Tamanrasset, Algeria",OWNO,////,////,Aguemar-Hadj Bey Akhamok,"Tamanrasset, Tamanrasset, Algeria",OK
    TMS,////,////,FPST,UNK,0.786,Sao Tome,"Sao Tome Island, Sao Tome and Principe",OWNO,////,////,São Tomé International Airport,"São Tomé, São Tomé, São Tomé and Príncipe",OK
    TMT,////,////,SBTB,UNK,0.75,Trombetas,"Trombetas, Brazil",OWNO,////,////,????,"Porto Trombetas, Pará, Brazil",MAYBE
    TMU,////,////,MRTR,UNK,1.0,Tambor,"Tambor, Costa Rica",OWNO,////,////,????,"Tambor, Puntarenas, Costa Rica",OK
    TMW,////,////,YSTW,UNK,1.0,Tamworth,"Tamworth, Australia",OWNO,////,////,????,"Tamworth, New South Wales, Australia",OK
    TMX,////,////,DAUT,UNK,1.0,Timimoun,"Timimoun, Algeria",OWNO,////,////,Timimoun Airport,"Timimoun, Adrar, Algeria",OK
    TMY,////,////,////,UNK,1.0,Tiom,"Tiom, Indonesia",OWNO,////,////,????,"Tiom, Papua, Indonesia",OK
    TMZ,////,////,NZTH,UNK,1.0,Thames,"Thames, New Zealand",OWNO,////,////,????,"Thames, New Zealand",OK
    TNA,////,////,ZSJN,UNK,0.727,Jinan,"Jinan, PR China",OWNO,////,////,Yaoqiang International Airport,"Jinan, Shandong, China",OK
    TNB,////,////,WRLH,UNK,1.0,Tanah Grogot,"Tanah Grogot, Indonesia",OWNO,////,////,????,"Tanah Grogot, Kalimantan Timur (East Borneo), Indonesia",OK
    TNC,TNC,PATC,PATC,OK,0.661,Tin City AFS,"Tin City (AK), USA",OWNO,TIN CITY LRRS,"TIN CITY, AK - UNITED STATES",Tin City LRRS Airport,"Tin City, Alaska, United States",OK
    TND,////,////,MUTD,UNK,0.533,Trinidad,"Trinidad, Cuba",OWNO,////,////,Alberto Delgado,"Trinidad, Sancti Spíritus, Cuba",OK
    TNE,////,////,RJFG,UNK,1.0,New Tanegashima Airport,"Nakatane, Japan",OWNO,////,////,New Tanegashima Airport,"Tanegashima, Tanegashima Island, Osumi Islands, Satsunan Islands, Kagoshima, Japan",OK
    TNF,////,////,LFPN,UNK,1.0,Toussus-le-Noble,"Toussus-le-Noble, France",OWNO,////,////,????,"Toussus-Le-Noble, Île-de-France, France",OK
    TNG,////,////,GMTT,UNK,1.0,Boukhalef,"Tangier, Morocco",OWNO,////,////,Boukhalef,"Tanger, Morocco",OK
    TNH,////,////,ZYTN,UNK,0.741,Tonghua Liuhe,"Tonghua, PR China",OWNO,////,////,Tonghua Sanyuanpu Airport,"Tonghua, Jilin, China",OK
    TNI,////,////,VIST,UNK,1.0,Satna,"Satna, India",OWNO,////,////,????,"Satna, Madhya Pradesh, India",OK
    TNJ,////,////,WIDN,UNK,0.654,Kidjang,"Tanjung Pinang, Indonesia",OWNO,////,////,Raja Haji Fisabilillah International Airport,"Tanjung Pinang, Riau, Indonesia",OK
    TNK,4KA,////,////,OK,1.0,Tununak,"Tununak (AK), USA",OWNO,TUNUNAK,"TUNUNAK, AK - UNITED STATES",????,"Tununak, Alaska, United States",OK
    TNL,////,////,UKLT,UNK,0.8,Ternopol,"Ternopol, Ukraine",OWNO,////,////,Ternopol International Airport,"Ternopol, Ternopil, Ukraine",OK
    TNM,////,////,SCRM,UNK,0.931,Teniente R. Marsh,"Teniente R. Marsh, Antarctica",OWNO,////,////,Tiente Rodolfo Marsh Martin Airport,"Base Antarctica, King George Island, Magallanes y de la Antártica Chilena, Chile",OK
    TNN,////,////,RCNN,UNK,1.0,Tainan,"Tainan, Taiwan",OWNO,////,////,????,"Tainan, Taiwan",OK
    TNO,////,////,MRTM,UNK,0.593,Tamarindo,"Tamarindo, Costa Rica",OWNO,////,////,Tamarindo de Santa Cruz,"Tamarindo, Guanacaste, Costa Rica",OK
    TNP,TNP,KTNP,KTNP,OK,1.0,Twentynine Palms,"Twentynine Palms (CA), USA",OWNO,TWENTYNINE PALMS,"TWENTYNINE PALMS, CA - UNITED STATES",????,"Twentynine Palms, California, United States",OK
    TNQ,////,////,////,UNK,1.0,Teraina,"Teraina, Kiribati",OWNO,////,////,????,"Teraina, Kiribati",OK
    TNR,////,////,FMMI,UNK,0.733,Antananarivo,"Antananarivo, Madagascar",OWNO,////,////,Ivato International Airport,"Antananarivo, Madagascar",OK
    TNS,////,////,////,UNK,1.0,Tungsten,"Tungsten, Canada",OWNO,////,////,Tungsten,"Tungsten, Northwest Territories, Canada",OK
    TNT,TNT,KTNT,KTNT,OK,0.496,Dade Collier,"Miami (FL), USA",OWNO,DADE-COLLIER TRAINING AND TRANSITION,"MIAMI, FL - UNITED STATES",Dade-Collier Training and Transition Airport,"Miami, Florida, United States",OK
    TNU,TNU,KTNU,KTNU,OK,0.393,Municipal,"Newton IA, USA",OWNO,NEWTON MUNI-EARL JOHNSON FIELD,"NEWTON, IA - UNITED STATES",Newton Municipal-Earl Johnson Field,"Newton, Iowa, United States",OK
    TNV,////,////,PLFA,UNK,0.72,Tabuaeran,"Tabuaeran, Kiribati",OWNO,////,////,????,"Tabuaeran Island, Kiribati",MAYBE
    TNW,////,////,SEJD,UNK,nan,////,////,////,////,////,Jumandy,"Tena, Napo, Ecuador",UNK
    TNX,////,////,VDST,UNK,1.0,Stung Treng,"Stung Treng, Cambodia",OWNO,////,////,????,"Stung Treng, Cambodia",OK
    TOA,TOA,KTOA,KTOA,OK,0.521,Torrance,"Torrance (CA), USA",OWNO,ZAMPERINI FIELD,"TORRANCE, CA - UNITED STATES",Zamperini Field,"Torrance, California, United States",OK
    TOB,////,////,HLGN,UNK,0.444,Tobruk,"Tobruk, Libya",OWNO,////,////,Gamal Abdel Nasser,"Tobruk, Libyan Arab Jamahiriya (Libya)",OK
    TOC,TOC,KTOC,KTOC,OK,0.34,Toccoa,"Toccoa (GA), USA",OWNO,TOCCOA RG LETOURNEAU FIELD,"TOCCOA, GA - UNITED STATES",Toccoa Rg Letourneau Field,"Toccoa, Georgia, United States",OK
    TOD,////,////,WMBT,UNK,0.714,Tioman,"Tioman, Malaysia",OWNO,////,////,????,"Pulau Tioman, Johor, Malaysia",MAYBE
    TOE,////,////,DTTZ,UNK,0.526,Tozeur,"Tozeur, Tunisia",OWNO,////,////,Nefta International Airport,"Tozeur, Tunisia",OK

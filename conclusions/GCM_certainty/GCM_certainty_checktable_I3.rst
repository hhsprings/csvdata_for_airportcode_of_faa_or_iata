
List for checking certainty of Great Circle Mapper (ISD - IZT)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ISD,////,////,////,UNK,1.0,Iscuande,"Iscuande, Colombia",OWNO,////,////,????,"Iscuande, Nariño, Colombia",OK
    ISE,////,////,LTFC,UNK,0.452,Isparta,"Isparta, Turkey",OWNO,////,////,Süleyman Demirel,"Isparta, Isparta, Turkey",OK
    ISG,////,////,ROIG,UNK,1.0,New Ishigaki Airport,"Ishigaki, Japan",OWNO,////,////,New Ishigaki Airport,"Ishigaki, Ishigaki Island, Okinawa, Japan",OK
    ISI,////,////,YISF,UNK,1.0,Isisford,"Isisford, Australia",OWNO,////,////,????,"Isisford, Queensland, Australia",OK
    ISJ,////,////,MMIM,UNK,1.0,Isla Mujeres,"Isla Mujeres, Mexico",OWNO,////,////,????,"Isla Mujeres, Quintana Roo, México",OK
    ISK,////,////,VANR,UNK,1.0,Gandhinagar Airport,"Nasik, India",OWNO,////,////,Gandhinagar,"Nasik, Maharashtra, India",OK
    ISM,ISM,KISM,KISM,OK,0.591,Municipal,"Kissimmee (FL), USA",OWNO,KISSIMMEE GATEWAY,"ORLANDO, FL - UNITED STATES",Kissimmee Gateway Airport,"Orlando, Florida, United States",OK
    ISN,ISN,KISN,KISN,OK,1.0,Sloulin Field International,"Williston (ND), USA",OWNO,SLOULIN FLD INTL,"WILLISTON, ND - UNITED STATES",Sloulin Field International Airport,"Williston, North Dakota, United States",OK
    ISO,ISO,KISO,KISO,OK,0.682,Stallings Field,"Kinston (NC), USA",OWNO,KINSTON RGNL JETPORT AT STALLINGS FLD,"KINSTON, - UNITED STATES",Kinston Regional Jetport at Stallings Field,"Kinston, North Carolina, United States",OK
    ISP,ISP,KISP,KISP,OK,0.619,Long Island Macarthur,"Islip (NY), USA",OWNO,LONG ISLAND MAC ARTHUR,"NEW YORK, NY - UNITED STATES",Long Island Mac Arthur Airport,"New York, New York, United States",OK
    ISQ,ISQ,KISQ,KISQ,OK,1.0,Schoolcraft County,"Manistique (MI), USA",OWNO,SCHOOLCRAFT COUNTY,"MANISTIQUE, MI - UNITED STATES",Schoolcraft County Airport,"Manistique, Michigan, United States",OK
    ISS,IWI,KIWI,KIWI,OK,1.0,Wiscasset,"Wiscasset (ME), USA",OWNO,WISCASSET,"WISCASSET, ME - UNITED STATES",????,"Wiscasset, Maine, United States",OK
    IST,////,////,LTBA,UNK,0.882,Istanbul Atatürk Airport,"Istanbul, Turkey",OWNO,////,////,Atatürk International Airport,"Istanbul, Turkey",OK
    ISU,////,////,ORSU,UNK,1.0,Sulaimaniyah International Airport,"Sulaimaniyah, Kurdistan, Iraq",OWNO,////,////,Sulaimaniyah International Airport,"Sulaimaniyah, Iraq",OK
    ISW,ISW,KISW,KISW,OK,0.699,Alexander Field,"Wisconsin Rapids (WI), USA",OWNO,ALEXANDER FIELD SOUTH WOOD COUNTY,"WISCONSIN RAPIDS, WI - UNITED STATES",Alexander Field South Wood County Airport,"Wisconsin Rapids, Wisconsin, United States",OK
    ITA,////,////,SBIC,UNK,1.0,Itacoatiara,"Itacoatiara, Brazil",OWNO,////,////,????,"Itacoiatiara, Amazonas, Brazil",OK
    ITB,////,////,SBIH,UNK,1.0,Itaituba,"Itaituba, Brazil",OWNO,////,////,????,"Itaituba, Pará, Brazil",OK
    ITE,////,////,SNZW,UNK,1.0,Itubera,"Itubera, Brazil",OWNO,////,////,????,"Ituberá, Bahia, Brazil",OK
    ITH,ITH,KITH,KITH,OK,0.64,Tompkins County,"Ithaca (NY), USA",OWNO,ITHACA TOMPKINS RGNL,"ITHACA, NY - UNITED STATES",Ithaca Tompkins Regional,"Ithaca, New York, United States",OK
    ITI,////,////,SNYH,UNK,0.4,Itambacuri,"Itambacuri, Brazil",OWNO,////,////,Fazenda Vale Sereno,"Cumaru do Norte, Pará, Brazil",TO DO CHECK
    ITK,////,////,////,UNK,1.0,Itokama,"Itokama, Papua New Guinea",OWNO,////,////,????,"Itokama, Northern, Papua-New Guinea",OK
    ITM,////,////,RJOO,UNK,0.815,Osaka-Itami International Airport,"Osaka, Japan",OWNO,////,////,Itami,"Osaka, Hyogo, Japan",OK
    ITN,////,////,SNHA,UNK,1.0,Itabuna,"Itabuna, Brazil",OWNO,////,////,????,"Itabuna, Bahia, Brazil",OK
    ITO,ITO,PHTO,PHTO,OK,1.0,Hilo International,"Hilo (HI), USA",OWNO,HILO INTL,"HILO, HI - UNITED STATES",Hilo International Airport,"Hilo, Hawaii, Hawaii, United States",OK
    ITQ,////,////,SSIQ,UNK,0.522,Itaqui,"Itaqui, Brazil",OWNO,////,////,Aeropuerto Itaqui,"Itaqui, Rio Grande do Sul, Brazil",OK
    ITU,////,////,UHSI,UNK,nan,////,////,////,////,////,Iturup Airport,"Kurilsk, Iturup Island, Kuril Islands, Sakhalinskaya, Russian Federation (Russia)",UNK
    IUE,////,////,NIUE,UNK,0.788,Hanan,"Niue Island, Niue",OWNO,////,////,Niue International Airport,"Alofi, Niue Island, Niue",OK
    IUL,////,////,////,UNK,1.0,Ilu,"Ilu, Indonesia",OWNO,////,////,????,"Ilu, Papua, Indonesia",OK
    IUS,////,////,////,UNK,1.0,Inus,"Inus, Papua New Guinea",OWNO,////,////,????,"Inus, Bougainville, Papua-New Guinea",OK
    IVA,////,////,FMNJ,UNK,1.0,Ambanja,"Ambanja, Madagascar",OWNO,////,////,????,"Ambanja, Madagascar",OK
    IVC,////,////,NZNV,UNK,1.0,Invercargill,"Invercargill, New Zealand",OWNO,////,////,????,"Invercargill, New Zealand",OK
    IVG,////,////,LYBR,UNK,1.0,Dolac Airport,"Ivangrad (Berane), Montenegro",OWNO,////,////,Dolac,"Berane, Montenegro",OK
    IVL,////,////,EFIV,UNK,1.0,Ivalo,"Ivalo, Finland",OWNO,////,////,????,"Ivalo, Lappi (Lappland (Lapland)), Finland",OK
    IVO,////,////,////,UNK,1.0,Chivolo,"Chivolo, Colombia",OWNO,////,////,????,"Chivolo, Magdalena, Colombia",OK
    IVR,////,////,YIVL,UNK,1.0,Inverell,"Inverell, Australia",OWNO,////,////,????,"Inverell, New South Wales, Australia",OK
    IVW,////,////,YINW,UNK,1.0,Inverway,"Inverway, Australia",OWNO,////,////,????,"Inverway, Northern Territory, Australia",OK
    IWA,////,////,UUBI,UNK,0.667,Ivanova,"Ivanova, Russia",OWNO,////,////,Yuzhny Airport,"Ivanovo, Ivanovskaya, Russian Federation (Russia)",OK
    IWD,IWD,KIWD,KIWD,OK,0.87,Gogebic County,"Ironwood (MI), USA",OWNO,GOGEBIC-IRON COUNTY,"IRONWOOD, MI - UNITED STATES",Gogebic-Iron County Airport,"Ironwood, Michigan, United States",OK
    IWJ,////,////,RJOW,UNK,1.0,Iwami,"Iwami, Japan",OWNO,////,////,Iwami Airport,"Iwami, Shimane, Japan",OK
    IWK,////,////,RJOI,UNK,nan,////,////,////,////,////,MCAS Iwakuni,"Iwakuni, Yamaguchi, Japan",UNK
    IWO,////,////,RJAW,UNK,0.8,Iwo Jima Air Base,"Iwo Jima, Japan",OWNO,////,////,Iwo Jima AB,"Iwo Jima, Iwo Jima Island, Tokyo, Japan",OK
    IWS,IWS,KIWS,KIWS,OK,1.0,West Houston,"Houston (TX), USA",OWNO,WEST HOUSTON,"HOUSTON, TX - UNITED STATES",West Houston Airport,"Houston, Texas, United States",OK
    IXA,////,////,VEAT,UNK,0.75,Singerbhil,"Agartala, India",OWNO,////,////,????,"Agartala, Tripura, India",OK
    IXB,////,////,VEBD,UNK,1.0,Bagdogra,"Bagdogra, India",OWNO,////,////,????,"Bagdogra, West Bengal, India",OK
    IXC,////,////,VICG,UNK,1.0,Chandigarh,"Chandigarh, India",OWNO,////,////,????,"Chandigarh, Chandigarh, India",OK
    IXD,////,////,VIAL,UNK,1.0,Bamrauli,"Allahabad, India",OWNO,////,////,Bamrauli,"Allahabad, Uttar Pradesh, India",OK
    IXE,////,////,VOML,UNK,0.69,Bajpe,"Mangalore, India",OWNO,////,////,Mangalore International Airport,"Mangalore, Karnataka, India",OK
    IXG,////,////,VABM,UNK,0.706,Sambre,"Belgaum, India",OWNO,////,////,????,"Belgaum, Karnataka, India",OK
    IXH,////,////,VEKR,UNK,1.0,Kailashahar,"Kailashahar, India",OWNO,////,////,????,"Kailashahar, Tripura, India",OK
    IXI,////,////,VELR,UNK,0.5,Lilabari,"Lilabari, India",OWNO,////,////,North Lakhimpur,"Lilabari, Assam, India",OK
    IXJ,////,////,VIJU,UNK,0.609,Satwari Airport,"Jammu, India",OWNO,////,////,????,"Jammu, Jammu and Kashmir, India",OK
    IXK,////,////,VAKS,UNK,1.0,Keshod,"Keshod, India",OWNO,////,////,????,"Keshod, Gujarat, India",OK
    IXL,////,////,VILH,UNK,1.0,Leh,"Leh, India",OWNO,////,////,????,"Leh, Jammu and Kashmir, India",OK
    IXM,////,////,VOMD,UNK,1.0,Madurai,"Madurai, India",OWNO,////,////,????,"Madurai, Tamil Nadu, India",OK
    IXN,////,////,VEKW,UNK,1.0,Khowai,"Khowai, India",OWNO,////,////,????,"Khowai, Tripura, India",OK
    IXP,////,////,VIPK,UNK,1.0,Pathankot,"Pathankot, India",OWNO,////,////,????,"Pathankot, Punjab, India",OK
    IXQ,////,////,VEKM,UNK,1.0,Kamalpur,"Kamalpur, India",OWNO,////,////,????,"Kamalpur, Tripura, India",OK
    IXR,////,////,VERC,UNK,0.5,Ranchi,"Ranchi, India",OWNO,////,////,Birsa Munda,"Ranchi, Jharkhand, India",OK
    IXS,////,////,VEKU,UNK,0.973,Kumbhirgram,"Silchar, India",OWNO,////,////,Kumbhigram,"Silchar, Assam, India",OK
    IXT,////,////,VEPG,UNK,1.0,Pasighat,"Pasighat, India",OWNO,////,////,????,"Pasighat, Arunachal Pradesh, India",OK
    IXU,////,////,VAAU,UNK,0.727,Chikkalthana,"Aurangabad, India",OWNO,////,////,????,"Aurangabad, Maharashtra, India",OK
    IXV,////,////,VEAN,UNK,1.0,Along,"Along, India",OWNO,////,////,????,"Along, Arunachal Pradesh, India",OK
    IXW,////,////,VEJS,UNK,0.783,Sonari Airport,"Jamshedpur, India",OWNO,////,////,????,"Jamshedpur, Jharkhand, India",OK
    IXY,////,////,VAKE,UNK,1.0,Kandla,"Kandla, India",OWNO,////,////,????,"Kandla, Gujarat, India",OK
    IXZ,////,////,VOPB,UNK,1.0,Port Blair,"Port Blair, India",OWNO,////,////,????,"Port Blair, Andaman and Nicobar Islands, India",OK
    IYK,IYK,KIYK,KIYK,OK,0.826,Kern County,"Inyokern (CA), USA",OWNO,INYOKERN,"INYOKERN, CA - UNITED STATES",????,"Inyokern, California, United States",OK
    IZA,////,////,SBZM,UNK,nan,////,////,////,////,////,Presidente Itamar Franco Airport,"Juiz de Fora, Minas Gerais, Brazil",UNK
    IZM,////,////,////,UNK,1.0,Izmir Metropolitan Area,"Izmir, Turkey",OWNO,////,////,Metropolitan Area,"Izmir, Turkey",OK
    IZO,////,////,RJOC,UNK,1.0,Izumo,"Izumo, Japan",OWNO,////,////,Izumo Airport,"Izumo, Shimane, Japan",OK
    IZT,////,////,MMIT,UNK,1.0,Ixtepec,"Ixtepec, Mexico",OWNO,////,////,????,"Ixtepec, Oaxaca, México",OK

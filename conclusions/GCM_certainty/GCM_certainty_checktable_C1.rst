
List for checking certainty of Great Circle Mapper (CAA - CEF)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    CAA,////,////,MHCA,UNK,1.0,Catacamas,"Catacamas, Honduras",OWNO,////,////,????,"Catacamas, Olancho, Honduras",OK
    CAB,////,////,FNCA,UNK,1.0,Cabinda,"Cabinda, Angola",OWNO,////,////,????,"Cabinda, Angola",OK
    CAC,////,////,SBCA,UNK,1.0,Cascavel,"Cascavel, Brazil",OWNO,////,////,????,"Cascavel, Paraná, Brazil",OK
    CAD,CAD,KCAD,KCAD,OK,0.543,Cadillac,"Cadillac (MI), USA",OWNO,WEXFORD COUNTY,"CADILLAC, MI - UNITED STATES",Wexford County Airport,"Cadillac, Michigan, United States",OK
    CAE,CAE,KCAE,KCAE,OK,1.0,Metropolitan Airport,"Columbia (SC), USA",OWNO,COLUMBIA METROPOLITAN,"COLUMBIA, SC - UNITED STATES",Columbia Metropolitan Airport,"Columbia, South Carolina, United States",OK
    CAF,////,////,SWCA,UNK,1.0,Carauari,"Carauari, Brazil",OWNO,////,////,????,"Carauari, Amazonas, Brazil",OK
    CAG,////,////,LIEE,UNK,1.0,Elmas,"Cagliari, Italy",OWNO,////,////,Elmas,"Cagliari, Sardinia, Italy",OK
    CAH,////,////,VVCM,UNK,1.0,Ca Mau,"Ca Mau, Viet Nam",OWNO,////,////,????,"Ca Mau, Vietnam",OK
    CAI,////,////,HECA,UNK,1.0,International,"Cairo, Egypt",OWNO,////,////,Cairo International Airport,"Cairo, Al Qahirah (Cairo), Egypt",OK
    CAJ,////,////,SVCN,UNK,1.0,Canaima,"Canaima, Venezuela",OWNO,////,////,????,"Canaima, Bolívar, Venezuela",OK
    CAK,CAK,KCAK,KCAK,OK,1.0,Akron/Canton Regional,"Akron/Canton (OH), USA",OWNO,AKRON-CANTON RGNL,"AKRON, OH - UNITED STATES",Akron-Canton Regional,"Akron, Ohio, United States",OK
    CAL,////,////,EGEC,UNK,0.714,Machrihanish,"Campbeltown, United Kingdom",OWNO,////,////,Campbeltown Airport,"Campbeltown, Argyll, Scotland, United Kingdom",OK
    CAM,////,////,SLCA,UNK,1.0,Camiri,"Camiri, Bolivia",OWNO,////,////,Camiri Airport,"Camiri, Cordillera, Santa Cruz, Bolivia",OK
    CAN,////,////,ZGGG,UNK,1.0,Baiyun,"Guangzhou, PR China",OWNO,////,////,Baiyun,"Guangzhou, Guangdong, China",OK
    CAO,CAO,KCAO,KCAO,OK,0.409,Clayton,"Clayton, USA (NM)",OWNO,CLAYTON MUNI ARPK,"CLAYTON, NM - UNITED STATES",Clayton Municipal Airpark,"Clayton, New Mexico, United States",OK
    CAP,////,////,MTCH,UNK,0.815,Cap Haitien,"Cap Haitien, Haiti",OWNO,////,////,Cap-Haïtien International Airport,"Cap-Haïtien, Haiti",OK
    CAQ,////,////,SKCU,UNK,0.588,Caucasia,"Caucasia, Colombia",OWNO,////,////,Juan H White Airport,"Caucasia, Antioquia, Colombia",OK
    CAR,CAR,KCAR,KCAR,OK,1.0,Municipal,"Caribou (ME), USA",OWNO,CARIBOU MUNI,"CARIBOU, ME - UNITED STATES",Caribou Municipal Airport,"Caribou, Maine, United States",OK
    CAS,////,////,GMMC,UNK,1.0,Anfa,"Casablanca, Morocco",OWNO,////,////,Anfa,"Casablanca, Morocco",OK
    CAT,////,MYCB,MYCB,OK,0.529,Cat Island,"Cat Island, Bahamas",OWNO,NEW BIGHT,"NEW BIGHT, - BAHAMAS",New Bight,"Cat Island, Bahamas",OK
    CAU,////,////,SNRU,UNK,1.0,Caruaru,"Caruaru, Brazil",OWNO,////,////,????,"Caruaru, Pernambuco, Brazil",OK
    CAV,////,////,FNCZ,UNK,1.0,Cazombo,"Cazombo, Angola",OWNO,////,////,????,"Cazombo, Angola",OK
    CAW,////,////,SBCP,UNK,1.0,Bartolomeu Lisandro,"Campos, Brazil",OWNO,////,////,Bartolomeu Lysandro,"Campos dos Goitacazes, Rio de Janeiro, Brazil",OK
    CAX,////,////,EGNC,UNK,1.0,Carlisle,"Carlisle, United Kingdom",OWNO,////,////,????,"Carlisle, Cumbria, England, United Kingdom",OK
    CAY,////,////,SOCA,UNK,1.0,Rochambeau,"Cayenne, French Guiana",OWNO,////,////,Rochambeau,"Cayenne, French Guiana",OK
    CAZ,////,////,YCBA,UNK,1.0,Cobar,"Cobar, Australia",OWNO,////,////,????,"Cobar, New South Wales, Australia",OK
    CBB,////,////,SLCB,UNK,0.844,J Wilsterman,"Cochabamba, Bolivia",OWNO,////,////,Jórge Wilsterman International Airport,"Cochabamba, Cercado, Cochabamba, Bolivia",OK
    CBC,////,////,////,UNK,1.0,Cherrabun,"Cherrabun, Australia",OWNO,////,////,????,"Cherrabun, Western Australia, Australia",OK
    CBD,////,////,VOCX,UNK,0.87,Car Nicobar,"Car Nicobar, India",OWNO,////,////,Car Nicobar AFB,"Malacca, Car Nicobar, Andaman and Nicobar Islands, India",OK
    CBE,CBE,KCBE,KCBE,OK,0.57,Wiley Ford,"Cumberland (MD), USA",OWNO,GREATER CUMBERLAND RGNL,"CUMBERLAND, MD - UNITED STATES",Greater Cumberland Regional,"Cumberland, Maryland, United States",OK
    CBF,CBF,KCBF,KCBF,OK,1.0,Municipal,"Council Bluffs IA, USA",OWNO,COUNCIL BLUFFS MUNI,"COUNCIL BLUFFS, IA - UNITED STATES",Council Bluffs Municipal Airport,"Council Bluffs, Iowa, United States",OK
    CBG,////,////,EGSC,UNK,1.0,Cambridge,"Cambridge, United Kingdom",OWNO,////,////,????,"Cambridge, Cambridgeshire, England, United Kingdom",OK
    CBH,////,////,DAOR,UNK,0.516,Leger,"Bechar, Algeria",OWNO,////,////,Boudghene Ben Ali Lotfi,"Béchar, Béchar, Algeria",OK
    CBI,////,////,YCBN,UNK,1.0,Cape Barren Island,"Cape Barren Island, Australia",OWNO,////,////,????,"Cape Barren Island, Tasmania, Australia",OK
    CBJ,////,////,MDCR,UNK,1.0,Cabo Rojo,"Cabo Rojo, Dominican Republic",OWNO,////,////,Cabo Rojo Airport,"Pedernales, Pedernales, Dominican Republic",OK
    CBK,CBK,KCBK,KCBK,OK,0.599,Municipal,"Colby (KS), USA",OWNO,SHALZ FIELD,"COLBY, KS - UNITED STATES",Shalz Field,"Colby, Kansas, United States",OK
    CBL,////,////,SVCB,UNK,0.774,Ciudad Bolivar,"Ciudad Bolivar, Venezuela",OWNO,////,////,Tomás de Heres Airport,"Ciudad Bolívar, Bolívar, Venezuela",OK
    CBM,CBM,KCBM,KCBM,OK,1.0,Columbus AFB,"Columbus (MS), USA",OWNO,COLUMBUS AFB,"COLUMBUS, MS - UNITED STATES",Columbus AFB,"Columbus, Mississippi, United States",OK
    CBN,////,////,WICD,UNK,1.0,Penggung,"Cirebon, Indonesia",OWNO,////,////,Penggung,"Cirebon, Jawa Barat, Indonesia",OK
    CBO,////,////,RPMC,UNK,0.737,Awang,"Cotabato, Philippines",OWNO,////,////,????,"Cotabato, Philippines",OK
    CBP,////,////,LPCO,UNK,1.0,Coimbra,"Coimbra, Portugal",OWNO,////,////,Coimbra,"Coimbra, Coimbra, Portugal",OK
    CBQ,////,////,DNCA,UNK,0.483,Calabar,"Calabar, Nigeria",OWNO,////,////,Margaret Ekpo International Airport,"Calabar, Cross River, Nigeria",OK
    CBR,////,////,YSCB,UNK,1.0,Canberra,"Canberra, Australia",OWNO,////,////,Canberra Airport,"Canberra, Australian Capital Territory, Australia",OK
    CBS,////,////,SVON,UNK,1.0,Oro Negro,"Cabimas, Venezuela",OWNO,////,////,Oro Negro,"Cabimas, Zulia, Venezuela",OK
    CBT,////,////,FNCT,UNK,1.0,Catumbela,"Catumbela, Angola",OWNO,////,////,????,"Catumbela, Angola",OK
    CBU,////,////,EDCD,UNK,1.0,Cottbus-Drewitz Airport,"Cottbus, Germany",OWNO,////,////,Drewitz,"Cottbus, Brandenburg, Germany",OK
    CBV,////,////,MGCB,UNK,1.0,Coban,"Coban, Guatemala",OWNO,////,////,????,"Cobán, Alta Verapaz, Guatemala",OK
    CBW,////,////,SSKM,UNK,1.0,Campo Mourao,"Campo Mourao, Brazil",OWNO,////,////,????,"Campo Mourão, Paraná, Brazil",OK
    CBX,////,////,YCDO,UNK,1.0,Condobolin,"Condobolin, Australia",OWNO,////,////,????,"Condobolin, New South Wales, Australia",OK
    CBY,////,////,YCBE,UNK,1.0,Canobie,"Canobie, Australia",OWNO,////,////,????,"Canobie, Queensland, Australia",OK
    CCB,CCB,KCCB,KCCB,OK,1.0,Cable,"Upland (CA), USA",OWNO,CABLE,"UPLAND, CA - UNITED STATES",Cable Airport,"Upland, California, United States",OK
    CCC,////,////,MUCC,UNK,0.45,Cayo Coco,"Cayo Coco, Cuba",OWNO,////,////,Jardines del Rey International Airport,"Cayo Coco, Ciego de Ávila, Cuba",OK
    CCE,////,TFFG,TFFG,OK,0.64,Grand Case,"St. Martin, Guadeloupe",OWNO,GRAND CASE,"GRAND CASE, - SAINT MARTIN",L'Espérance,"Grand Case, St-Martin, Guadeloupe",OK
    CCF,////,////,LFMK,UNK,1.0,Salvaza,"Carcassonne, France",OWNO,////,////,Salvaza,"Carcassonne, Languedoc-Roussillon, France",OK
    CCG,E13,////,////,OK,1.0,Crane County Airport,"Crane (TX), USA",OWNO,CRANE COUNTY,"CRANE, TX - UNITED STATES",Crane County Airport,"Crane, Texas, United States",OK
    CCH,////,////,SCCC,UNK,1.0,Chile Chico,"Chile Chico, Chile",OWNO,////,////,Chile Chico Airport,"Chile Chico, Aysén del General Carlos Ibáñez del Campo, Chile",OK
    CCI,////,////,SSCK,UNK,1.0,Concordia,"Concordia, Brazil",OWNO,////,////,????,"Concordia, Santa Catarina, Brazil",OK
    CCJ,////,////,VOCL,UNK,0.714,Kozhikode,"Kozhikode, India",OWNO,////,////,????,"Calicut, Kerala, India",TO DO CHECK
    CCK,////,////,YPCC,UNK,1.0,Cocos Islands,"Cocos Islands, Cocos (Keeling) Islands",OWNO,////,////,Cocos (Keeling) Islands,"West Island, Keeling Cocos",MAYBE
    CCL,////,////,YCCA,UNK,1.0,Chinchilla,"Chinchilla, Australia",OWNO,////,////,????,"Chinchilla, Queensland, Australia",OK
    CCM,////,////,SBCM,UNK,0.522,Criciuma,"Criciuma, Brazil",OWNO,////,////,Diomício Freitas Airport,"Criciúma, Santa Catarina, Brazil",OK
    CCN,////,////,OACC,UNK,0.952,Chakcharan,"Chakcharan, Afghanistan",OWNO,////,////,????,"Chakhcharan, Ghor, Afghanistan",OK
    CCO,////,////,SKCI,UNK,1.0,Carimagua,"Carimagua, Colombia",OWNO,////,////,Carimagua,"Puerto López, Meta, Colombia",OK
    CCP,////,////,SCIE,UNK,0.898,Carriel Sur,"Concepcion, Chile",OWNO,////,////,Carriel Sur International Airport,"Concepción, Bío Bío, Chile",OK
    CCR,CCR,KCCR,KCCR,OK,1.0,Buchanan Field,"Concord (CA), USA",OWNO,BUCHANAN FIELD,"CONCORD, CA - UNITED STATES",Buchanan Field,"Concord, California, United States",OK
    CCS,////,////,SVMI,UNK,0.894,Simon Bolivar Airport,"Caracas, Venezuela",OWNO,////,////,Simón Bolívar International Airport,"Caracas, Vargas, Venezuela",OK
    CCT,////,////,////,UNK,1.0,Colonia Catriel,"Colonia Catriel, Argentina",OWNO,////,////,????,"Colonia Catriel, Río Negro, Argentina",OK
    CCU,////,////,VECC,UNK,0.8,Netaji Subhas Chandra,"Kolkata (Calcutta), India",OWNO,////,////,Netaji Subhash Chandra Bose International Airport,"Kolkata, West Bengal, India",MAYBE
    CCV,////,////,NVSF,UNK,1.0,Craig Cove,"Craig Cove, Vanuatu",OWNO,////,////,????,"Craig Cove, Ambryn Island, Malampa, Vanuatu",OK
    CCW,////,////,YCWL,UNK,1.0,Cowell,"Cowell, Australia",OWNO,////,////,????,"Cowell, South Australia, Australia",OK
    CCX,////,////,SWKC,UNK,1.0,Caceres,"Caceres, Brazil",OWNO,////,////,????,"Cáceres, Mato Grosso, Brazil",OK
    CCY,CCY,KCCY,KCCY,OK,0.585,Municipal,"Charles City IA, USA",OWNO,NORTHEAST IOWA RGNL,"CHARLES CITY, IA - UNITED STATES",Northeast Iowa Regional,"Charles City, Iowa, United States",OK
    CCZ,////,////,MYBC,UNK,1.0,Chub Cay,"Chub Cay, Bahamas",OWNO,////,////,????,"Chub Cay, Berry Islands, Bahamas",OK
    CDA,////,////,YCOO,UNK,1.0,Cooinda,"Cooinda, Australia",OWNO,////,////,????,"Cooinda, Northern Territory, Australia",OK
    CDB,CDB,PACD,PACD,OK,1.0,Cold Bay,"Cold Bay (AK), USA",OWNO,COLD BAY,"COLD BAY, AK - UNITED STATES",????,"Cold Bay, Alaska, United States",OK
    CDC,CDC,KCDC,KCDC,OK,0.734,Cedar City,"Cedar City (UT), USA",OWNO,CEDAR CITY RGNL,"CEDAR CITY, UT - UNITED STATES",Cedar City Regional,"Cedar City, Utah, United States",OK
    CDD,////,////,////,UNK,1.0,Cauquira,"Cauquira, Honduras",OWNO,////,////,????,"Cauquira, Gracias a Dios, Honduras",OK
    CDG,////,////,LFPG,UNK,1.0,Charles De Gaulle,"Paris, France",OWNO,////,////,Charles de Gaulle,"Paris, Île-de-France, France",OK
    CDH,CDH,KCDH,KCDH,OK,1.0,Harrell Fld,"Camden (AR), USA",OWNO,HARRELL FIELD,"CAMDEN, AR - UNITED STATES",Harrell Field,"Camden, Arkansas, United States",OK
    CDI,////,////,SNKI,UNK,0.638,Cachoeiro Itapemirim,"Cachoeiro Itapemirim, Brazil",OWNO,////,////,Raimundo de Andrade Airport,"Cachoeiro do Itapemirim, Espírito Santo, Brazil",MAYBE
    CDJ,////,////,SBAA,UNK,1.0,Conceicao Do Araguaia,"Conceicao Do Araguaia, Brazil",OWNO,////,////,????,"Conceicao do Araguaia, Pará, Brazil",OK
    CDK,CDK,KCDK,KCDK,OK,0.699,Lewis,"Cedar Key (FL), USA",OWNO,GEORGE T LEWIS,"CEDAR KEY, FL - UNITED STATES",George T Lewis Airport,"Cedar Key, Florida, United States",OK
    CDL,AK75,////,////,OK,1.0,Candle,"Candle (AK), USA",OWNO,CANDLE 2,"CANDLE, AK - UNITED STATES",Candle 2 Airport,"Candle, Alaska, United States",OK
    CDN,CDN,KCDN,KCDN,OK,1.0,Woodward Field,"Camden (SC), USA",OWNO,WOODWARD FIELD,"CAMDEN, SC - UNITED STATES",Woodward Field,"Camden, South Carolina, United States",OK
    CDO,////,////,FACD,UNK,1.0,Cradock,"Cradock, South Africa",OWNO,////,////,????,"Cradock, Eastern Cape, South Africa",OK
    CDP,////,////,VOCP,UNK,1.0,Cuddapah,"Cuddapah, India",OWNO,////,////,????,"Cuddapah, Andhra Pradesh, India",OK
    CDQ,////,////,YCRY,UNK,1.0,Croydon,"Croydon, Australia",OWNO,////,////,????,"Croydon, Queensland, Australia",OK
    CDR,CDR,KCDR,KCDR,OK,0.748,Chadron,"Chadron (NE), USA",OWNO,CHADRON MUNI,"CHADRON, NE - UNITED STATES",Chadron Municipal Airport,"Chadron, Nebraska, United States",OK
    CDS,CDS,KCDS,KCDS,OK,0.734,Childress,"Childress (TX), USA",OWNO,CHILDRESS MUNI,"CHILDRESS, TX - UNITED STATES",Childress Municipal Airport,"Childress, Texas, United States",OK
    CDT,////,////,LECH,UNK,nan,////,////,////,////,////,Castellón Airport,"Castellón de la Plana, Valenciana, Spain",UNK
    CDU,////,////,YSCN,UNK,1.0,Camden,"Camden, Australia",OWNO,////,////,????,"Camden, New South Wales, Australia",OK
    CDV,CDV,PACV,PACV,OK,1.0,Mudhole Smith,"Cordova (AK), USA",OWNO,MERLE K (MUDHOLE) SMITH,"CORDOVA, AK - UNITED STATES",Merle K Airport (Mudhole) Smith,"Cordova, Alaska, United States",OK
    CDW,CDW,KCDW,KCDW,OK,0.479,Caldwell Wright,"Caldwell (NJ), USA",OWNO,ESSEX COUNTY,"CALDWELL, NJ - UNITED STATES",Essex County Airport,"Caldwell, New Jersey, United States",OK
    CDY,////,////,RPMU,UNK,1.0,Cagayan De Sulu,"Cagayan De Sulu, Philippines",OWNO,////,////,Cagayan de Sulu,"Mapun, Tawi-Tawi, Philippines",OK
    CEA,CEA,KCEA,KCEA,OK,1.0,Cessna Aircraft Field,"Wichita (KS), USA",OWNO,CESSNA ACFT FIELD,"WICHITA, KS - UNITED STATES",Cessna Aircraft Field,"Wichita, Kansas, United States",OK
    CEB,////,////,RPVM,UNK,1.0,Mactan-Cebu International Airport,"Cebu, Philippines",OWNO,////,////,Mactan-Cebu International Airport,"Lapu-Lapu, Philippines",OK
    CEC,CEC,KCEC,KCEC,OK,0.912,Mc Namara Fld,"Crescent City (CA), USA",OWNO,JACK MC NAMARA FIELD,"CRESCENT CITY, CA - UNITED STATES",Jack Mc Namara Field,"Crescent City, California, United States",OK
    CED,////,////,YCDU,UNK,1.0,Ceduna,"Ceduna, Australia",OWNO,////,////,????,"Ceduna, South Australia, Australia",OK
    CEE,////,////,ULWC,UNK,1.0,Cherepovets,"Cherepovets, Russia",OWNO,////,////,Cherepovets Airport,"Cherepovets, Vologodskaya, Russian Federation (Russia)",OK
    CEF,CEF,KCEF,KCEF,OK,0.826,Westover Metro Airport,"Springfield (MA), USA",OWNO,WESTOVER ARB/METROPOLITAN,"SPRINGFIELD/CHICOPEE, MA - UNITED STATES",Westover ARB/Metropolitan Airport,"Springfield/Chicopee, Massachusetts, United States",OK

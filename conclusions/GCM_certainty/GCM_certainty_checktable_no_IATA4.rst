
List for checking certainty of Great Circle Mapper (FAA LID GLY - FAA LID LHX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,GLY,KGLY,KGLY,OK,1.0,////,////,////,CLINTON RGNL,"CLINTON, MO - UNITED STATES",Clinton Regional,"Clinton, Missouri, United States",OK
    ////,GMJ,KGMJ,KGMJ,OK,1.0,////,////,////,GROVE MUNI,"GROVE, OK - UNITED STATES",Grove Municipal Airport,"Grove, Oklahoma, United States",OK
    ////,GNC,KGNC,KGNC,OK,1.0,////,////,////,GAINES COUNTY,"SEMINOLE, TX - UNITED STATES",Gaines County Airport,"Seminole, Texas, United States",OK
    ////,GNF,KGNF,KGNF,OK,1.0,////,////,////,GRENADA MUNI,"GRENADA, MS - UNITED STATES",Grenada Municipal Airport,"Grenada, Mississippi, United States",OK
    ////,GOO,KGOO,KGOO,OK,1.0,////,////,////,NEVADA COUNTY AIR PARK,"GRASS VALLEY, CA - UNITED STATES",Nevada County Air Park,"Grass Valley, California, United States",OK
    ////,GOP,KGOP,KGOP,OK,1.0,////,////,////,GATESVILLE MUNI,"GATESVILLE, TX - UNITED STATES",Gatesville Municipal Airport,"Gatesville, Texas, United States",OK
    ////,GOV,KGOV,KGOV,OK,1.0,////,////,////,GRAYLING AAF,"GRAYLING, MI - UNITED STATES",Grayling AAF Airport,"Grayling, Michigan, United States",OK
    ////,GPH,KGPH,KGPH,OK,1.0,////,////,////,MIDWEST NATIONAL AIR CENTER,"MOSBY, MO - UNITED STATES",Midwest National Air Center Airport,"Mosby, Missouri, United States",OK
    ////,GPM,KGPM,KGPM,OK,1.0,////,////,////,GRAND PRAIRIE MUNI,"GRAND PRAIRIE, TX - UNITED STATES",Grand Prairie Municipal Airport,"Grand Prairie, Texas, United States",OK
    ////,GTB,KGTB,KGTB,OK,1.0,////,////,////,WHEELER-SACK AAF,"FORT DRUM, NY - UNITED STATES",Wheeler-Sack AAF Airport,"Fort Drum, New York, United States",OK
    ////,GTU,KGTU,KGTU,OK,1.0,////,////,////,GEORGETOWN MUNI,"GEORGETOWN, TX - UNITED STATES",Georgetown Municipal Airport,"Georgetown, Texas, United States",OK
    ////,GVQ,KGVQ,KGVQ,OK,1.0,////,////,////,GENESEE COUNTY,"BATAVIA, NY - UNITED STATES",Genesee County Airport,"Batavia, New York, United States",OK
    ////,GWB,KGWB,KGWB,OK,1.0,////,////,////,DE KALB COUNTY,"AUBURN, IN - UNITED STATES",De Kalb County Airport,"Auburn, Indiana, United States",OK
    ////,GWR,KGWR,KGWR,OK,1.0,////,////,////,GWINNER-ROGER MELROE FIELD,"GWINNER, ND - UNITED STATES",Gwinner-Roger Melroe Field,"Gwinner, North Dakota, United States",OK
    ////,GXF,KGXF,KGXF,OK,0.889,////,////,////,GILA BEND AF AUX,"GILA BEND, AZ - UNITED STATES",Gila Bend AF Auxiliary Airport,"Gila Bend, Arizona, United States",OK
    ////,GYB,KGYB,KGYB,OK,1.0,////,////,////,GIDDINGS-LEE COUNTY,"GIDDINGS, TX - UNITED STATES",Giddings-Lee County Airport,"Giddings, Texas, United States",OK
    ////,GYL,KGYL,KGYL,OK,1.0,////,////,////,GLENCOE MUNI,"GLENCOE, MN - UNITED STATES",Glencoe Municipal Airport,"Glencoe, Minnesota, United States",OK
    ////,GZH,KGZH,KGZH,OK,1.0,////,////,////,MIDDLETON FIELD,"EVERGREEN, AL - UNITED STATES",Middleton Field,"Evergreen, Alabama, United States",OK
    ////,GZL,KGZL,KGZL,OK,1.0,////,////,////,STIGLER RGNL,"STIGLER, OK - UNITED STATES",Stigler Regional,"Stigler, Oklahoma, United States",OK
    ////,GZS,KGZS,KGZS,OK,1.0,////,////,////,ABERNATHY FIELD,"PULASKI, TN - UNITED STATES",Abernathy Field,"Pulaski, Tennessee, United States",OK
    ////,HAE,KHAE,KHAE,OK,1.0,////,////,////,HANNIBAL RGNL,"HANNIBAL, MO - UNITED STATES",Hannibal Regional,"Hannibal, Missouri, United States",OK
    ////,HBI,KHBI,KHBI,OK,1.0,////,////,////,ASHEBORO RGNL,"ASHEBORO, NC - UNITED STATES",Asheboro Regional,"Asheboro, North Carolina, United States",OK
    ////,HBV,KHBV,KHBV,OK,1.0,////,////,////,JIM HOGG COUNTY,"HEBBRONVILLE, TX - UNITED STATES",Jim Hogg County Airport,"Hebbronville, Texas, United States",OK
    ////,HCD,KHCD,KHCD,OK,1.0,////,////,////,HUTCHINSON MUNI-BUTLER FIELD,"HUTCHINSON, MN - UNITED STATES",Hutchinson Municipal-Butler Field,"Hutchinson, Minnesota, United States",OK
    ////,HCO,KHCO,KHCO,OK,1.0,////,////,////,HALLOCK MUNI,"HALLOCK, MN - UNITED STATES",Hallock Municipal Airport,"Hallock, Minnesota, United States",OK
    ////,HDC,KHDC,KHDC,OK,1.0,////,////,////,HAMMOND NORTHSHORE RGNL,"HAMMOND, LA - UNITED STATES",Hammond Northshore Regional,"Hammond, Louisiana, United States",OK
    ////,HDI,////,KHDI,UNK,nan,////,////,////,////,////,Hardwick Field,"Cleveland, Tennessee, United States",UNK
    ////,HDO,KHDO,KHDO,OK,1.0,////,////,////,SOUTH TEXAS RGNL AT HONDO,"HONDO, TX - UNITED STATES",South Texas Regional at Hondo,"Hondo, Texas, United States",OK
    ////,HEI,KHEI,KHEI,OK,1.0,////,////,////,HETTINGER MUNI,"HETTINGER, ND - UNITED STATES",Hettinger Municipal Airport,"Hettinger, North Dakota, United States",OK
    ////,HEQ,KHEQ,KHEQ,OK,1.0,////,////,////,HOLYOKE,"HOLYOKE, CO - UNITED STATES",????,"Holyoke, Colorado, United States",OK
    ////,HFJ,KHFJ,KHFJ,OK,1.0,////,////,////,MONETT RGNL,"MONETT, MO - UNITED STATES",Monett Regional,"Monett, Missouri, United States",OK
    ////,HHF,KHHF,KHHF,OK,1.0,////,////,////,HEMPHILL COUNTY,"CANADIAN, TX - UNITED STATES",Hemphill County Airport,"Canadian, Texas, United States",OK
    ////,HHG,KHHG,KHHG,OK,1.0,////,////,////,HUNTINGTON MUNI,"HUNTINGTON, IN - UNITED STATES",Huntington Municipal Airport,"Huntington, Indiana, United States",OK
    ////,HJH,KHJH,KHJH,OK,1.0,////,////,////,HEBRON MUNI,"HEBRON, NE - UNITED STATES",Hebron Municipal Airport,"Hebron, Nebraska, United States",OK
    ////,HJO,KHJO,KHJO,OK,1.0,////,////,////,HANFORD MUNI,"HANFORD, CA - UNITED STATES",Hanford Municipal Airport,"Hanford, California, United States",OK
    ////,HLX,KHLX,KHLX,OK,1.0,////,////,////,TWIN COUNTY,"GALAX HILLSVILLE, VA - UNITED STATES",Twin County Airport,"Galax Hillsville, Virginia, United States",OK
    ////,HMY,KHMY,KHMY,OK,0.857,////,////,////,MULDROW AHP,"LEXINGTON, OK - UNITED STATES",Muldrow Army Heliport,"Lexington, Oklahoma, United States",OK
    ////,HMZ,KHMZ,KHMZ,OK,1.0,////,////,////,BEDFORD COUNTY,"BEDFORD, PA - UNITED STATES",Bedford County Airport,"Bedford, Pennsylvania, United States",OK
    ////,HNR,KHNR,KHNR,OK,1.0,////,////,////,HARLAN MUNI,"HARLAN, IA - UNITED STATES",Harlan Municipal Airport,"Harlan, Iowa, United States",OK
    ////,HNZ,KHNZ,KHNZ,OK,1.0,////,////,////,HENDERSON-OXFORD,"OXFORD, NC - UNITED STATES",Henderson-Oxford Airport,"Oxford, North Carolina, United States",OK
    ////,HOC,KHOC,KHOC,OK,1.0,////,////,////,HIGHLAND COUNTY,"HILLSBORO, OH - UNITED STATES",Highland County Airport,"Hillsboro, Ohio, United States",OK
    ////,HOE,KHOE,KHOE,OK,1.0,////,////,////,HOMERVILLE,"HOMERVILLE, GA - UNITED STATES",????,"Homerville, Georgia, United States",OK
    ////,HQG,KHQG,KHQG,OK,1.0,////,////,////,HUGOTON MUNI,"HUGOTON, KS - UNITED STATES",Hugoton Municipal Airport,"Hugoton, Kansas, United States",OK
    ////,HQU,KHQU,KHQU,OK,1.0,////,////,////,THOMSON-MCDUFFIE COUNTY,"THOMSON, GA - UNITED STATES",Thomson-McDuffie County Airport,"Thomson, Georgia, United States",OK
    ////,HQZ,KHQZ,KHQZ,OK,1.0,////,////,////,MESQUITE METRO,"MESQUITE, TX - UNITED STATES",Mesquite Metro Airport,"Mesquite, Texas, United States",OK
    ////,HRJ,KHRJ,KHRJ,OK,1.0,////,////,////,HARNETT RGNL JETPORT,"ERWIN, NC - UNITED STATES",Harnett Regional Jetport,"Erwin, North Carolina, United States",OK
    ////,HRT,KHRT,KHRT,OK,1.0,////,////,////,HURLBURT FIELD,"MARY ESTHER, FL - UNITED STATES",Hurlburt Field,"Mary Esther, Florida, United States",OK
    ////,HRX,KHRX,KHRX,OK,1.0,////,////,////,HEREFORD MUNI,"HEREFORD, TX - UNITED STATES",Hereford Municipal Airport,"Hereford, Texas, United States",OK
    ////,HSA,KHSA,KHSA,OK,1.0,////,////,////,STENNIS INTL,"BAY ST LOUIS, MS - UNITED STATES",Stennis International Airport,"Bay St. Louis, Mississippi, United States",OK
    ////,HSD,KHSD,KHSD,OK,1.0,////,////,////,SUNDANCE,"OKLAHOMA CITY, OK - UNITED STATES",Sundance Airport,"Oklahoma City, Oklahoma, United States",OK
    ////,HTW,KHTW,KHTW,OK,1.0,////,////,////,LAWRENCE COUNTY AIRPARK,"CHESAPEAKE/HUNTINGTON WVA, OH - UNITED STATES",Lawrence County Airpark,"Chesapeake/Huntington Wva, Ohio, United States",OK
    ////,HWY,KHWY,KHWY,OK,1.0,////,////,////,WARRENTON-FAUQUIER,"WARRENTON, VA - UNITED STATES",Warrenton-Fauquier Airport,"Warrenton, Virginia, United States",OK
    ////,HYI,KHYI,KHYI,OK,1.0,////,////,////,SAN MARCOS REGIONAL,"AUSTIN, TX - UNITED STATES",San Marcos Regional,"Austin, Texas, United States",OK
    ////,HYW,KHYW,KHYW,OK,1.0,////,////,////,CONWAY-HORRY COUNTY,"CONWAY, SC - UNITED STATES",Conway-Horry County Airport,"Conway, South Carolina, United States",OK
    ////,HYX,KHYX,KHYX,OK,1.0,////,////,////,SAGINAW COUNTY H W BROWNE,"SAGINAW, MI - UNITED STATES",Saginaw County H W Browne Airport,"Saginaw, Michigan, United States",OK
    ////,HZD,KHZD,KHZD,OK,1.0,////,////,////,CARROLL COUNTY,"HUNTINGDON, TN - UNITED STATES",Carroll County Airport,"Huntingdon, Tennessee, United States",OK
    ////,HZE,KHZE,KHZE,OK,1.0,////,////,////,MERCER COUNTY RGNL,"HAZEN, ND - UNITED STATES",Mercer County Regional,"Hazen, North Dakota, United States",OK
    ////,HZR,KHZR,KHZR,OK,1.0,////,////,////,FALSE RIVER RGNL,"NEW ROADS, LA - UNITED STATES",False River Regional,"New Roads, Louisiana, United States",OK
    ////,HZX,KHZX,KHZX,OK,1.0,////,////,////,ISEDOR IVERSON,"MC GREGOR, MN - UNITED STATES",Isedor Iverson Airport,"Mc Gregor, Minnesota, United States",OK
    ////,IBM,KIBM,KIBM,OK,1.0,////,////,////,KIMBALL MUNI/ROBERT E ARRAJ FIELD,"KIMBALL, NE - UNITED STATES",Kimball Municipal/Robert E Arraj Field,"Kimball, Nebraska, United States",OK
    ////,ICR,KICR,KICR,OK,1.0,////,////,////,WINNER RGNL,"WINNER, SD - UNITED STATES",Winner Regional,"Winner, South Dakota, United States",OK
    ////,IDL,KIDL,KIDL,OK,1.0,////,////,////,INDIANOLA MUNI,"INDIANOLA, MS - UNITED STATES",Indianola Municipal Airport,"Indianola, Mississippi, United States",OK
    ////,IEM,PAWR,PAWR,OK,1.0,////,////,////,WHITTIER,"WHITTIER, AK - UNITED STATES",????,"Whittier, Alaska, United States",OK
    ////,IER,KIER,KIER,OK,1.0,////,////,////,NATCHITOCHES RGNL,"NATCHITOCHES, LA - UNITED STATES",Natchitoches Regional,"Natchitoches, Louisiana, United States",OK
    ////,IGQ,KIGQ,KIGQ,OK,1.0,////,////,////,LANSING MUNI,"CHICAGO, IL - UNITED STATES",Lansing Municipal Airport,"Chicago, Illinois, United States",OK
    ////,IGX,KIGX,KIGX,OK,1.0,////,////,////,HORACE WILLIAMS,"CHAPEL HILL, NC - UNITED STATES",Horace Williams Airport,"Chapel Hill, North Carolina, United States",OK
    ////,IIB,KIIB,KIIB,OK,1.0,////,////,////,INDEPENDENCE MUNI,"INDEPENDENCE, IA - UNITED STATES",Independence Municipal Airport,"Independence, Iowa, United States",OK
    ////,IIY,KIIY,KIIY,OK,1.0,////,////,////,WASHINGTON-WILKES COUNTY,"WASHINGTON, GA - UNITED STATES",Washington-Wilkes County Airport,"Washington, Georgia, United States",OK
    ////,IJD,KIJD,KIJD,OK,1.0,////,////,////,WINDHAM,"WILLIMANTIC, CT - UNITED STATES",Windham Airport,"Willimantic, Connecticut, United States",OK
    ////,IKV,KIKV,KIKV,OK,1.0,////,////,////,ANKENY RGNL,"ANKENY, IA - UNITED STATES",Ankeny Regional,"Ankeny, Iowa, United States",OK
    ////,IKW,KIKW,KIKW,OK,1.0,////,////,////,JACK BARSTOW,"MIDLAND, MI - UNITED STATES",Jack Barstow Airport,"Midland, Michigan, United States",OK
    ////,INF,KINF,KINF,OK,1.0,////,////,////,INVERNESS,"INVERNESS, FL - UNITED STATES",????,"Inverness, Florida, United States",OK
    ////,INJ,KINJ,KINJ,OK,1.0,////,////,////,HILLSBORO MUNI,"HILLSBORO, TX - UNITED STATES",Hillsboro Municipal Airport,"Hillsboro, Texas, United States",OK
    ////,IOB,KIOB,KIOB,OK,1.0,////,////,////,MOUNT STERLING-MONTGOMERY COUNTY,"MOUNT STERLING, KY - UNITED STATES",Mount Sterling-Montgomery County Airport,"Mount Sterling, Kentucky, United States",OK
    ////,IPJ,KIPJ,KIPJ,OK,1.0,////,////,////,LINCOLNTON-LINCOLN COUNTY RGNL,"LINCOLNTON, NC - UNITED STATES",Lincolnton-Lincoln County Regional,"Lincolnton, North Carolina, United States",OK
    ////,ITR,KITR,KITR,OK,1.0,////,////,////,KIT CARSON COUNTY,"BURLINGTON, CO - UNITED STATES",Kit Carson County Airport,"Burlington, Colorado, United States",OK
    ////,IXA,KIXA,KIXA,OK,1.0,////,////,////,HALIFAX-NORTHAMPTON RGNL,"ROANOKE RAPIDS, NC - UNITED STATES",Halifax-Northampton Regional,"Roanoke Rapids, North Carolina, United States",OK
    ////,IYA,KIYA,KIYA,OK,1.0,////,////,////,ABBEVILLE CHRIS CRUSTA MEMORIAL,"ABBEVILLE, LA - UNITED STATES",Abbeville Chris Crusta Memorial Airport,"Abbeville, Louisiana, United States",OK
    ////,JAQ,KJAQ,KJAQ,OK,1.0,////,////,////,WESTOVER FIELD AMADOR COUNTY,"JACKSON, CA - UNITED STATES",Westover Field Amador County Airport,"Jackson, California, United States",OK
    ////,JAU,KJAU,KJAU,OK,1.0,////,////,////,CAMPBELL COUNTY,"JACKSBORO, TN - UNITED STATES",Campbell County Airport,"Jacksboro, Tennessee, United States",OK
    ////,JCA,KJCA,KJCA,OK,1.0,////,////,////,JACKSON COUNTY,"JEFFERSON, GA - UNITED STATES",Jackson County Airport,"Jefferson, Georgia, United States",OK
    ////,JER,KJER,KJER,OK,1.0,////,////,////,JEROME COUNTY,"JEROME, ID - UNITED STATES",Jerome County Airport,"Jerome, Idaho, United States",OK
    ////,JES,KJES,KJES,OK,1.0,////,////,////,JESUP-WAYNE COUNTY,"JESUP, GA - UNITED STATES",Jesup-Wayne County Airport,"Jesup, Georgia, United States",OK
    ////,JFX,KJFX,KJFX,OK,1.0,////,////,////,WALKER COUNTY-BEVILL FIELD,"JASPER, AL - UNITED STATES",Walker County-Bevill Field,"Jasper, Alabama, United States",OK
    ////,JFZ,KJFZ,KJFZ,OK,1.0,////,////,////,TAZEWELL COUNTY,"RICHLANDS, VA - UNITED STATES",Tazewell County Airport,"Richlands, Virginia, United States",OK
    ////,JGG,KJGG,KJGG,OK,1.0,////,////,////,WILLIAMSBURG-JAMESTOWN,"WILLIAMSBURG, VA - UNITED STATES",Williamsburg-Jamestown Airport,"Williamsburg, Virginia, United States",OK
    ////,JHN,KJHN,KJHN,OK,1.0,////,////,////,STANTON COUNTY MUNI,"JOHNSON, KS - UNITED STATES",Stanton County Municipal Airport,"Johnson, Kansas, United States",OK
    ////,JKJ,KJKJ,KJKJ,OK,1.0,////,////,////,MOORHEAD MUNI,"MOORHEAD, MN - UNITED STATES",Moorhead Municipal Airport,"Moorhead, Minnesota, United States",OK
    ////,JKL,KJKL,KJKL,OK,1.0,////,////,////,JULIAN CARROLL,"JACKSON, KY - UNITED STATES",Julian Carroll Airport,"Jackson, Kentucky, United States",OK
    ////,JMR,KJMR,KJMR,OK,1.0,////,////,////,MORA MUNI,"MORA, MN - UNITED STATES",Mora Municipal Airport,"Mora, Minnesota, United States",OK
    ////,JNX,KJNX,KJNX,OK,1.0,////,////,////,JOHNSTON REGIONAL,"SMITHFIELD, NC - UNITED STATES",Johnston Regional,"Smithfield, North Carolina, United States",OK
    ////,JSV,KJSV,KJSV,OK,1.0,////,////,////,SALLISAW MUNI,"SALLISAW, OK - UNITED STATES",Sallisaw Municipal Airport,"Sallisaw, Oklahoma, United States",OK
    ////,JTC,KJTC,KJTC,OK,1.0,////,////,////,SPRINGERVILLE MUNI,"SPRINGERVILLE, AZ - UNITED STATES",Springerville Municipal Airport,"Springerville, Arizona, United States",OK
    ////,JVW,KJVW,KJVW,OK,1.0,////,////,////,JOHN BELL WILLIAMS,"RAYMOND, MS - UNITED STATES",John Bell Williams Airport,"Raymond, Mississippi, United States",OK
    ////,JWG,KJWG,KJWG,OK,1.0,////,////,////,WATONGA RGNL,"WATONGA, OK - UNITED STATES",Watonga Regional,"Watonga, Oklahoma, United States",OK
    ////,JWN,KJWN,KJWN,OK,1.0,////,////,////,JOHN C TUNE,"NASHVILLE, TN - UNITED STATES",John C Tune Airport,"Nashville, Tennessee, United States",OK
    ////,JWY,KJWY,KJWY,OK,1.0,////,////,////,MID-WAY RGNL,"MIDLOTHIAN/WAXAHACHIE, TX - UNITED STATES",Mid-Way Regional,"Midlothian/Waxahachie, Texas, United States",OK
    ////,JXI,KJXI,KJXI,OK,1.0,////,////,////,FOX STEPHENS FIELD - GILMER MUNI,"GILMER, TX - UNITED STATES",Fox Stephens Field - Gilmer Municipal Airport,"Gilmer, Texas, United States",OK
    ////,JYG,KJYG,KJYG,OK,1.0,////,////,////,ST JAMES MUNI,"ST JAMES, MN - UNITED STATES",St. James Municipal Airport,"St. James, Minnesota, United States",OK
    ////,JYL,KJYL,KJYL,OK,0.96,////,////,////,PLANTATION ARPK,"SYLVANIA, GA - UNITED STATES",Plantation Airpark,"Sylvania, Georgia, United States",OK
    ////,JYM,KJYM,KJYM,OK,1.0,////,////,////,HILLSDALE MUNI,"HILLSDALE, MI - UNITED STATES",Hillsdale Municipal Airport,"Hillsdale, Michigan, United States",OK
    ////,JYO,KJYO,KJYO,OK,1.0,////,////,////,LEESBURG EXECUTIVE,"LEESBURG, VA - UNITED STATES",Leesburg Executive Airport,"Leesburg, Virginia, United States",OK
    ////,JYR,KJYR,KJYR,OK,1.0,////,////,////,YORK MUNI,"YORK, NE - UNITED STATES",York Municipal Airport,"York, Nebraska, United States",OK
    ////,JZI,KJZI,KJZI,OK,1.0,////,////,////,CHARLESTON EXECUTIVE,"CHARLESTON, SC - UNITED STATES",Charleston Executive Airport,"Charleston, South Carolina, United States",OK
    ////,JZP,KJZP,KJZP,OK,1.0,////,////,////,PICKENS COUNTY,"JASPER, GA - UNITED STATES",Pickens County Airport,"Jasper, Georgia, United States",OK
    ////,K03,////,////,OK,0.813,////,////,////,LONG LAKE SAGAMORE SPB &amp; MARINA,"LONG LAKE, NY - UNITED STATES",Long Lake Sagamore SPB & Marina Seaplane Base,"Long Lake, New York, United States",OK
    ////,K27,////,////,OK,1.0,////,////,////,BURRELLO-MECHANICVILLE,"MECHANICVILLE, NY - UNITED STATES",Burrello-Mechanicville Airport,"Mechanicville, New York, United States",OK
    ////,K83,////,////,OK,1.0,////,////,////,SABETHA MUNI,"SABETHA, KS - UNITED STATES",Sabetha Municipal Airport,"Sabetha, Kansas, United States",OK
    ////,KAE,////,////,OK,0.846,////,////,////,KAKE,"KAKE, AK - UNITED STATES",Kake SPB,"Kake, Alaska, United States",OK
    ////,KWT,PFKW,PFKW,OK,1.0,////,////,////,KWETHLUK,"KWETHLUK, AK - UNITED STATES",????,"Kwethluk, Alaska, United States",OK
    ////,L06,////,////,OK,1.0,////,////,////,FURNACE CREEK,"DEATH VALLEY NATIONAL PARK, CA - UNITED STATES",Furnace Creek Airport,"Death Valley National Park, California, United States",OK
    ////,L52,////,////,OK,1.0,////,////,////,OCEANO COUNTY,"OCEANO, CA - UNITED STATES",Oceano County Airport,"Oceano, California, United States",OK
    ////,LCG,KLCG,KLCG,OK,1.0,////,////,////,WAYNE MUNI/ STAN MORRIS FLD,"WAYNE, NE - UNITED STATES",Wayne Municipal/ Stan Morris Field,"Wayne, Nebraska, United States",OK
    ////,LCQ,KLCQ,KLCQ,OK,1.0,////,////,////,LAKE CITY GATEWAY,"LAKE CITY, FL - UNITED STATES",Lake City Gateway Airport,"Lake City, Florida, United States",OK
    ////,LFK,KLFK,KLFK,OK,1.0,////,////,////,ANGELINA COUNTY,"LUFKIN, TX - UNITED STATES",Angelina County Airport,"Lufkin, Texas, United States",OK
    ////,LHB,KLHB,KLHB,OK,1.0,////,////,////,HEARNE MUNI,"HEARNE, TX - UNITED STATES",Hearne Municipal Airport,"Hearne, Texas, United States",OK
    ////,LHD,PALH,PALH,OK,0.929,////,////,////,LAKE HOOD,"ANCHORAGE, AK - UNITED STATES",Lake Hood SPB,"Anchorage, Alaska, United States",OK
    ////,LHM,KLHM,KLHM,OK,1.0,////,////,////,LINCOLN RGNL/KARL HARDER FIELD,"LINCOLN, CA - UNITED STATES",Lincoln Regional/Karl Harder Field,"Lincoln, California, United States",OK
    ////,LHQ,KLHQ,KLHQ,OK,1.0,////,////,////,FAIRFIELD COUNTY,"LANCASTER, OH - UNITED STATES",Fairfield County Airport,"Lancaster, Ohio, United States",OK
    ////,LHX,KLHX,KLHX,OK,1.0,////,////,////,LA JUNTA MUNI,"LA JUNTA, CO - UNITED STATES",La Junta Municipal Airport,"La Junta, Colorado, United States",OK

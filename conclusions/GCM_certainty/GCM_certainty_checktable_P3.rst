
List for checking certainty of Great Circle Mapper (PMY - PSS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    PMY,////,////,SAVY,UNK,1.0,El Tehuelche,"Puerto Madryn, Argentina",OWNO,////,////,El Tehuelche,"Puerto Madryn, Argentina",OK
    PMZ,////,////,MRPM,UNK,1.0,Palmar Sur,"Palmar, Costa Rica",OWNO,////,////,Palmar Sur,"Palmar, Puntarenas, Costa Rica",OK
    PNA,////,////,LEPP,UNK,1.0,Pamplona,"Pamplona, Spain",OWNO,////,////,????,"Pamplona, Navarra, Spain",OK
    PNB,////,////,SBPN,UNK,1.0,Porto Nacional,"Porto Nacional, Brazil",OWNO,////,////,????,"Porto Nacional, Tocantins, Brazil",OK
    PNC,PNC,KPNC,KPNC,OK,0.781,Ponca City,"Ponca City (OK), USA",OWNO,PONCA CITY RGNL,"PONCA CITY, OK - UNITED STATES",Ponca City Regional,"Ponca City, Oklahoma, United States",OK
    PND,////,////,////,UNK,1.0,Punta Gorda,"Punta Gorda, Belize",OWNO,////,////,Punta Gorda Airport,"Punta Gorda, Toledo, Belize",OK
    PNE,PNE,KPNE,KPNE,OK,0.882,North Philadelphia,"Philadelphia (PA), USA",OWNO,NORTHEAST PHILADELPHIA,"PHILADELPHIA, PA - UNITED STATES",Northeast Philadelphia Airport,"Philadelphia, Pennsylvania, United States",OK
    PNG,////,////,SSPG,UNK,0.833,Municipal,"Paranagua, Brazil",OWNO,////,////,????,"Paranaguá, Paraná, Brazil",OK
    PNH,////,////,VDPP,UNK,0.895,Pochentong,"Phnom Penh, Cambodia",OWNO,////,////,Pochentong International Airport,"Phnom Penh, Cambodia",OK
    PNI,PNI,PTPN,PTPN,OK,0.543,Pohnpei,"Pohnpei, Micronesia",OWNO,POHNPEI INTL,"POHNPEI ISLAND, - MICRONESIA, FED STATES OF",Pohnpei International Airport,"Pohnpei, Pohnpei, Micronesia (Federated States of)",OK
    PNK,////,////,WIOO,UNK,1.0,Supadio,"Pontianak, Indonesia",OWNO,////,////,Supadio,"Pontianak, Kalimantan Barat (West Borneo), Indonesia",OK
    PNL,////,////,LICG,UNK,1.0,Pantelleria,"Pantelleria, Italy",OWNO,////,////,????,"Pantelleria, Sicily, Italy",OK
    PNN,PNN,KPNN,KPNN,OK,1.0,Princeton,"Princeton (ME), USA",OWNO,PRINCETON MUNI,"PRINCETON, ME - UNITED STATES",Princeton Municipal Airport,"Princeton, Maine, United States",OK
    PNP,////,////,AYGR,UNK,1.0,Girua,"Popondetta, Papua New Guinea",OWNO,////,////,Girua,"Popondetta, Northern, Papua-New Guinea",OK
    PNQ,////,////,VAPO,UNK,1.0,Lohegaon,"Pune (Poona), India",OWNO,////,////,Lohegaon,"Pune, Maharashtra, India",OK
    PNR,////,////,FCPP,UNK,1.0,Pointe Noire,"Pointe Noire, Congo (ROC)",OWNO,////,////,????,"Pointe Noire, Congo (Republic of)",OK
    PNS,PNS,KPNS,KPNS,OK,0.623,Regional,"Pensacola (FL), USA",OWNO,PENSACOLA INTL,"PENSACOLA, FL - UNITED STATES",Pensacola International Airport,"Pensacola, Florida, United States",OK
    PNT,////,////,SCNT,UNK,0.945,Teniente J. Gallardo,"Puerto Natales, Chile",OWNO,////,////,Teniente Julio Gallardo,"Puerto Natales, Magallanes y de la Antártica Chilena, Chile",OK
    PNU,U55,////,////,OK,0.719,Panguitch,"Panguitch (UT), USA",OWNO,PANGUITCH MUNI,"PANGUITCH, UT - UNITED STATES",Panguitch Municipal Airport,"Panguitch, Utah, United States",OK
    PNV,////,////,EYPP,UNK,1.0,Panevezys,"Panevezys, Lithuania",OWNO,////,////,????,"Panevezys, Lithuania",OK
    PNX,GYI,KGYI,KGYI,OK,0.534,Grayson County,"Sherman-Denison (TX), USA",OWNO,NORTH TEXAS RGNL/PERRIN FIELD,"SHERMAN/DENISON, TX - UNITED STATES",North Texas Regional/Perrin Field,"Sherman/Denison, Texas, United States",OK
    PNY,////,////,VOPC,UNK,1.0,Pondicherry,"Pondicherry (see: Puducherry), India",OWNO,////,////,????,"Pondicherry, Tamil Nadu, India",OK
    PNZ,////,////,SBPL,UNK,0.591,Internacional,"Petrolina, Brazil",OWNO,////,////,Senador Nilo Coelho,"Petrolina, Pernambuco, Brazil",OK
    POA,////,////,SBPA,UNK,0.933,Salgado Filho International Airport,"Porto Alegre, Brazil",OWNO,////,////,Porto Alegre International / Salgado Filho,"Porto Alegre, Rio Grande do Sul, Brazil",OK
    POB,POB,KPOB,KPOB,OK,0.964,Pope AFB,"Fayetteville (NC), USA",OWNO,POPE AAF,"FAYETTEVILLE, NC - UNITED STATES",Pope AAF Airport,"Fayetteville, North Carolina, United States",OK
    POC,POC,KPOC,KPOC,OK,1.0,Brackett Field,"La Verne (CA), USA",OWNO,BRACKETT FIELD,"LA VERNE, CA - UNITED STATES",Brackett Field,"La Verne, California, United States",OK
    POD,////,////,GOSP,UNK,1.0,Podor,"Podor, Senegal",OWNO,////,////,Podor Airport,"Podor, Saint-Louis, Senegal",OK
    POE,POE,KPOE,KPOE,OK,1.0,Polk AAF,"Fort Polk (LA), USA",OWNO,POLK AAF,"FORT POLK, LA - UNITED STATES",Polk AAF Airport,"Fort Polk, Louisiana, United States",OK
    POF,POF,KPOF,KPOF,OK,0.598,Earl Fields Memorial,"Poplar Bluff (MO), USA",OWNO,POPLAR BLUFF MUNI,"POPLAR BLUFF, MO - UNITED STATES",Poplar Bluff Municipal Airport,"Poplar Bluff, Missouri, United States",OK
    POG,////,////,FOOG,UNK,1.0,Port Gentil,"Port Gentil, Gabon",OWNO,////,////,????,"Port Gentil, Ogooué-Maritime, Gabon",OK
    POH,POH,KPOH,KPOH,OK,1.0,Municipal,"Pocahontas IA, USA",OWNO,POCAHONTAS MUNI,"POCAHONTAS, IA - UNITED STATES",Pocahontas Municipal Airport,"Pocahontas, Iowa, United States",OK
    POI,////,////,SLPO,UNK,0.476,Potosi,"Potosi, Bolivia",OWNO,////,////,Capitán Nicolás Rojas,"Potosí, Tomás Frías, Potosí, Bolivia",OK
    POJ,////,////,SNPD,UNK,0.9,Patos De Minas,"Patos De Minas, Brazil",OWNO,////,////,Pará de Minas 2,"Pará de Minas, Minas Gerais, Brazil",MAYBE
    POL,////,////,FQPB,UNK,1.0,Pemba,"Pemba, Mozambique",OWNO,////,////,????,"Pemba, Mozambique",OK
    POM,////,////,AYPY,UNK,0.885,Jackson Field,"Port Moresby, Papua New Guinea",OWNO,////,////,Jacksons International Airport,"Port Moresby, National Capital District, Papua-New Guinea",OK
    PON,////,////,MGPP,UNK,1.0,Poptun,"Poptun, Guatemala",OWNO,////,////,????,"Poptún, Petén, Guatemala",OK
    POO,////,////,SBPC,UNK,0.469,Pocos De Caldas,"Pocos De Caldas, Brazil",OWNO,////,////,Embaixador Walther Moreira Salles,"Poços de Caldas, Minas Gerais, Brazil",OK
    POP,////,////,MDPP,UNK,0.55,La Union,"Puerto Plata, Dominican Republic",OWNO,////,////,Gregorio Luperón International Airport,"Puerto Plata, Puerto Plata, Dominican Republic",OK
    POR,////,////,EFPO,UNK,1.0,Pori,"Pori, Finland",OWNO,////,////,Pori Airport,"Pori, Satakunta (Satakunda (Satakunta)), Finland",OK
    POS,////,////,TTPP,UNK,1.0,Piarco International Airport,"Port of Spain, Trinidad and Tobago",OWNO,////,////,Piarco International Airport,"Port of Spain, Trinidad Island, Trinidad and Tobago",OK
    POT,////,////,MKKJ,UNK,1.0,Ken Jones,"Port Antonio, Jamaica",OWNO,////,////,Ken Jones,"Port Antonio, Jamaica",OK
    POU,POU,KPOU,KPOU,OK,1.0,Dutchess County,"Poughkeepsie (NY), USA",OWNO,DUTCHESS COUNTY,"POUGHKEEPSIE, NY - UNITED STATES",Dutchess County Airport,"Poughkeepsie, New York, United States",OK
    POV,////,////,LZPW,UNK,1.0,Presov,"Presov, Slovakia",OWNO,////,////,????,"Presov, Slovakia",OK
    POW,////,////,LJPZ,UNK,1.0,Portoroz,"Portoroz, Slovenia",OWNO,////,////,????,"Portoroz, Slovenia",OK
    POX,////,////,LFPT,UNK,0.632,Paris Cergy Pontoise,"Paris, France",OWNO,////,////,Pontoise - Cormeilles en Vexin,"Pontoise, Île-de-France, France",MAYBE
    POY,POY,KPOY,KPOY,OK,0.664,Lovell,"Powell (WY), USA",OWNO,POWELL MUNI,"POWELL, WY - UNITED STATES",Powell Municipal Airport,"Powell, Wyoming, United States",OK
    POZ,////,////,EPPO,UNK,1.0,Lawica,"Poznan, Poland",OWNO,////,////,Poznan-Lawica Airport,"Poznan, Wielkopolskie, Poland",OK
    PPA,PPA,KPPA,KPPA,OK,1.0,Perry Lefors Field,"Pampa (TX), USA",OWNO,PERRY LEFORS FIELD,"PAMPA, TX - UNITED STATES",Perry Lefors Field,"Pampa, Texas, United States",OK
    PPB,////,////,SBDN,UNK,1.0,A. De Barros,"Presidente Prudente, Brazil",OWNO,////,////,A. De Barros,"Presidente Prudente, São Paulo, Brazil",OK
    PPC,PPC,PAPR,PAPR,OK,1.0,Prospect Creek,"Prospect Creek (AK), USA",OWNO,PROSPECT CREEK,"PROSPECT CREEK, AK - UNITED STATES",????,"Prospect Creek, Alaska, United States",OK
    PPE,////,////,MMPE,UNK,0.769,Puerto Penasco,"Puerto Penasco, Mexico",OWNO,////,////,Punta Peñasco International Airport,"Punta Peñasco, Sonora, México",MAYBE
    PPF,PPF,KPPF,KPPF,OK,1.0,Tri-City,"Parsons (KS), USA",OWNO,TRI-CITY,"PARSONS, KS - UNITED STATES",Tri-City Airport,"Parsons, Kansas, United States",OK
    PPG,PPG,NSTU,NSTU,OK,1.0,International,"Pago Pago, American Samoa",OWNO,PAGO PAGO INTL,"PAGO PAGO, AS - UNITED STATES",Tafuna/Pago Pago International Airport,"Pago Pago, American Samoa",OK
    PPH,////,////,////,UNK,1.0,Peraitepuy,"Peraitepuy, Venezuela",OWNO,////,////,????,"Peraitepuy, Bolívar, Venezuela",OK
    PPI,////,////,YPIR,UNK,1.0,Port Pirie,"Port Pirie, Australia",OWNO,////,////,????,"Port Pirie, South Australia, Australia",OK
    PPJ,////,////,WIIG,UNK,1.0,Pulau Panjang,"Pulau Panjang, Indonesia",OWNO,////,////,????,"Pulau Panjang, Banten, Indonesia",OK
    PPK,////,////,UACP,UNK,0.667,Petropavlovsk,"Petropavlovsk, Kazakhstan",OWNO,////,////,Petropavl Airort,"Petropavl, Soltüstik Qazaqstan, Kazakhstan",TO DO CHECK
    PPL,////,////,VNPL,UNK,1.0,Phaplu,"Phaplu, Nepal",OWNO,////,////,????,"Phaplu, Nepal",OK
    PPM,PMP,KPMP,KPMP,OK,0.722,Pompano Beach,"Pompano Beach (FL), USA",OWNO,POMPANO BEACH AIRPARK,"POMPANO BEACH, FL - UNITED STATES",Pompano Beach Airpark,"Pompano Beach, Florida, United States",OK
    PPN,////,////,SKPP,UNK,0.6,Machangara,"Popayan, Colombia",OWNO,////,////,Guillermo León Valencia Airport,"Popayán, Cauca, Colombia",OK
    PPO,////,////,////,UNK,1.0,Powell Point,"Powell Point, Bahamas",OWNO,////,////,????,"Powell Point, Eleuthera, Bahamas",OK
    PPP,////,////,YBPN,UNK,1.0,Proserpine,"Proserpine, Australia",OWNO,////,////,????,"Proserpine, Whitsunday Coast, Queensland, Australia",OK
    PPQ,////,////,NZPP,UNK,1.0,Paraparaumu,"Paraparaumu, New Zealand",OWNO,////,////,????,"Paraparaumu, New Zealand",OK
    PPR,////,////,WIDE,UNK,1.0,Pasir Pangarayan,"Pasir Pangarayan, Indonesia",OWNO,////,////,????,"Pasir Pangarayan, Riau, Indonesia",OK
    PPS,////,////,RPVP,UNK,0.857,Puerto Princesa,"Puerto Princesa, Philippines",OWNO,////,////,Puerto Princesa International Airport,"Puerto Princesa, Philippines",OK
    PPT,////,////,NTAA,UNK,1.0,Faaa,"Papeete, French Polynesia",OWNO,////,////,Faa'a,"Papeete, Tahiti, Society Islands, French Polynesia",OK
    PPU,////,////,VBPP,UNK,1.0,Papun,"Papun, Myanmar",OWNO,////,////,????,"Papun, Kayin, Myanmar (Burma)",OK
    PPV,19P,////,////,OK,0.859,Port Protection,"Port Protection (AK), USA",OWNO,PORT PROTECTION,"PORT PROTECTION, AK - UNITED STATES",Port Protection SPB,"Port Protection, Alaska, United States",OK
    PPW,////,////,EGEP,UNK,1.0,Papa Westray,"Papa Westray, United Kingdom",OWNO,////,////,Papa Westray Airport,"Papa Westray, Orkney Isles, Scotland, United Kingdom",OK
    PPX,////,////,////,UNK,1.0,Param,"Param, Papua New Guinea",OWNO,////,////,????,"Param, Milne Bay, Papua-New Guinea",OK
    PPY,////,////,SNZA,UNK,1.0,Pouso Alegre,"Pouso Alegre, Brazil",OWNO,////,////,????,"Pouso Alegre, Minas Gerais, Brazil",OK
    PPZ,////,////,////,UNK,1.0,Puerto Paez,"Puerto Paez, Venezuela",OWNO,////,////,Puerto Páez Airport,"Puerto Páez, Apure, Venezuela",OK
    PQC,////,////,VVPQ,UNK,1.0,Phu Quoc International Airport,"Phu Quoc, Vietnam",OWNO,////,////,Phú Quóc International Airport,"Phú Quóc, Vietnam",OK
    PQD,////,////,////,UNK,0.629,Passikudah SPB,Batticaloa,IATA,////,////,Passikudah Waterdrome,"Batticaloa, Eastern Province, Sri Lanka (Ceylon)",MAYBE
    PQI,PQI,KPQI,KPQI,OK,0.43,Municipal,"Presque Isle (ME), USA",OWNO,NORTHERN MAINE RGNL ARPT AT PRESQUE IS,"PRESQUE ISLE, ME - UNITED STATES",Northern Maine Regional Airport at Presque Is,"Presque Isle, Maine, United States",OK
    PQM,////,////,MMPQ,UNK,0.762,Palenque,"Palenque, Mexico",OWNO,////,////,Palenque International Airport,"Palenque, Chiapas, México",OK
    PQQ,////,////,YPMQ,UNK,1.0,Port Macquarie,"Port Macquarie, Australia",OWNO,////,////,????,"Port Macquarie, New South Wales, Australia",OK
    PQS,0AK,////,////,OK,1.0,Pilot Station,"Pilot Station (AK), USA",OWNO,PILOT STATION,"PILOT STATION, AK - UNITED STATES",????,"Pilot Station, Alaska, United States",OK
    PRA,////,////,SAAP,UNK,0.455,Parana,"Parana, Argentina",OWNO,////,////,General Urquiza,"Parana, Argentina",OK
    PRB,PRB,KPRB,KPRB,OK,0.806,Paso Robles,"Paso Robles (CA), USA",OWNO,PASO ROBLES MUNI,"PASO ROBLES, CA - UNITED STATES",Paso Robles Municipal Airport,"Paso Robles, California, United States",OK
    PRC,PRC,KPRC,KPRC,OK,0.552,Prescott,"Prescott (AZ), USA",OWNO,ERNEST A LOVE FIELD,"PRESCOTT, AZ - UNITED STATES",Ernest A Love Field,"Prescott, Arizona, United States",OK
    PRD,////,////,YPDO,UNK,1.0,Pardoo,"Pardoo, Australia",OWNO,////,////,????,"Pardoo, Western Australia, Australia",OK
    PRE,////,////,////,UNK,1.0,Pore,"Pore, Colombia",OWNO,////,////,????,"Pore, Casanare, Colombia",OK
    PRG,////,////,LKPR,UNK,0.538,Ruzyne,"Prague, Czech Republic",OWNO,////,////,Václav Havel Airport Prague,"Prague, Prague, Czech Republic",OK
    PRH,////,////,VTCP,UNK,1.0,Phrae,"Phrae, Thailand",OWNO,////,////,????,"Phrae, Phrae, Thailand",OK
    PRI,////,////,FSPP,UNK,1.0,Praslin Island,"Praslin Island, Seychelles",OWNO,////,////,????,"Praslin Island, Seychelles",OK
    PRJ,////,////,LIQC,UNK,0.455,Capri,"Capri, Italy",OWNO,////,////,Anacapri Damecuta,"Capri, Campania, Italy",OK
    PRK,////,////,FAPK,UNK,1.0,Prieska,"Prieska, South Africa",OWNO,////,////,Prieska Airport,"Prieska, Northern Cape, South Africa",OK
    PRM,////,////,LPPM,UNK,1.0,Portimao,"Portimao, Portugal",OWNO,////,////,????,"Portimão, Faro, Portugal",OK
    PRN,////,////,BKPR,UNK,1.0,Pristina,"Pristina, Serbia",OWNO,////,////,????,"Pristina, Kosovo",OK
    PRO,PRO,KPRO,KPRO,OK,1.0,Municipal,"Perry IA, USA",OWNO,PERRY MUNI,"PERRY, IA - UNITED STATES",Perry Municipal Airport,"Perry, Iowa, United States",OK
    PRP,////,////,LFKO,UNK,1.0,Propriano,"Propriano, France",OWNO,////,////,Propriano Airport,"Propriano, Corse (Corsica), France",OK
    PRQ,////,////,SARS,UNK,0.75,Pres. Roque Saenz Pena,"Pres. Roque Saenz Pena, Argentina",OWNO,////,////,Termal,"Presidencia Roque Sáenz Peña, Chaco, Argentina",MAYBE
    PRR,////,////,SYPR,UNK,1.0,Paruima,"Paruima, Guyana",OWNO,////,////,????,"Paruima, Cuyuni-Mazaruni, Guyana",OK
    PRS,////,////,AGGP,UNK,1.0,Parasi,"Parasi, Solomon Islands",OWNO,////,////,????,"Parasi, Solomon Islands",OK
    PRU,////,////,VYPY,UNK,1.0,Prome,"Prome, Myanmar",OWNO,////,////,????,"Prome, Bago, Myanmar (Burma)",OK
    PRV,////,////,LKPO,UNK,1.0,Prerov,"Prerov, Czech Republic",OWNO,////,////,????,"Prerov, Olomouc, Czech Republic",OK
    PRW,5N2,////,////,OK,1.0,Prentice,"Prentice (WI), USA",OWNO,PRENTICE,"PRENTICE, WI - UNITED STATES",????,"Prentice, Wisconsin, United States",OK
    PRX,PRX,KPRX,KPRX,OK,1.0,Cox Field,"Paris (TX), USA",OWNO,COX FIELD,"PARIS, TX - UNITED STATES",Cox Field,"Paris, Texas, United States",OK
    PRY,////,////,FAWB,UNK,1.0,Wonderboom Airport,"Pretoria, South Africa",OWNO,////,////,Wonderboom Airport,"Pretoria, Gauteng, South Africa",OK
    PRZ,S39,////,////,OK,1.0,Prineville,"Prineville (OR), USA",OWNO,PRINEVILLE,"PRINEVILLE, OR - UNITED STATES",????,"Prineville, Oregon, United States",OK
    PSA,////,////,LIRP,UNK,0.857,Galileo Galilei,"Florence, Italy",OWNO,////,////,Galileo Galilei International Airport,"Pisa, Tuscany, Italy",OK
    PSB,PSB,KPSB,KPSB,OK,1.0,Mid-State,"Philipsburg (PA), USA",OWNO,MID-STATE,"PHILIPSBURG, PA - UNITED STATES",Mid-State Airport,"Philipsburg, Pennsylvania, United States",OK
    PSC,PSC,KPSC,KPSC,OK,1.0,Tri-Cities,"Pasco (WA), USA",OWNO,TRI-CITIES,"PASCO, WA - UNITED STATES",Tri-Cities Airport,"Pasco, Washington, United States",OK
    PSD,////,////,HEPS,UNK,1.0,Port Said,"Port Said, Egypt",OWNO,////,////,????,"Port Said, Ash Sharqiyah (Al Sharqia), Egypt",OK
    PSE,PSE,TJPS,TJPS,OK,1.0,Mercedita,"Ponce, Puerto Rico",OWNO,MERCEDITA,"PONCE, PR - UNITED STATES",Mercedita Airport,"Ponce, Puerto Rico, United States",OK
    PSF,PSF,KPSF,KPSF,OK,0.821,Pittsfield,"Pittsfield (MA), USA",OWNO,PITTSFIELD MUNI,"PITTSFIELD, MA - UNITED STATES",Pittsfield Municipal Airport,"Pittsfield, Massachusetts, United States",OK
    PSG,PSG,PAPG,PAPG,OK,0.654,Municipal,"Petersburg (AK), USA",OWNO,PETERSBURG JAMES A JOHNSON,"PETERSBURG, AK - UNITED STATES",Petersburg James A Johnson Airport,"Petersburg, Alaska, United States",OK
    PSH,////,////,EDXO,UNK,0.7,St Peter,"St Peter, Germany",OWNO,////,////,Ording,"St. Peter, Schleswig-Holstein, Germany",OK
    PSI,////,////,OPPI,UNK,1.0,Pasni,"Pasni, Pakistan",OWNO,////,////,????,"Pasni, Balochistan, Pakistan",OK
    PSJ,////,////,WAMP,UNK,1.0,Poso,"Poso, Indonesia",OWNO,////,////,????,"Poso, Sulawesi Tengah, Indonesia",OK
    PSK,PSK,KPSK,KPSK,OK,1.0,New River Valley,"Dublin (VA), USA",OWNO,NEW RIVER VALLEY,"DUBLIN, VA - UNITED STATES",New River Valley Airport,"Dublin, Virginia, United States",OK
    PSL,////,////,EGPT,UNK,0.8,Perth,"Perth, Scotland, UK",OWNO,////,////,Scone,"Perth, Perthshire, Scotland, United Kingdom",OK
    PSM,PSM,KPSM,KPSM,OK,0.742,Pease AFB,"Portsmouth (NH), USA",OWNO,PORTSMOUTH INTL AT PEASE,"PORTSMOUTH, NH - UNITED STATES",Pease International Airport,"Portsmouth, New Hampshire, United States",OK
    PSN,PSN,KPSN,KPSN,OK,0.734,Palestine,"Palestine (TX), USA",OWNO,PALESTINE MUNI,"PALESTINE, TX - UNITED STATES",Palestine Municipal Airport,"Palestine, Texas, United States",OK
    PSO,////,////,SKPS,UNK,0.5,Cano,"Pasto, Colombia",OWNO,////,////,Antonio Nariño Airport,"San Juan de Pasto, Nariño, Colombia",MAYBE
    PSP,PSP,KPSP,KPSP,OK,0.797,Municipal,"Palm Springs (CA), , USA",OWNO,PALM SPRINGS INTL,"PALM SPRINGS, CA - UNITED STATES",Palm Springs International Airport,"Palm Springs, California, United States",OK
    PSQ,9N2,////,////,OK,0.786,PB,"Philadelphia (PA), USA",OWNO,PHILADELPHIA,"ESSINGTON, PA - UNITED STATES",Philadelphia SPB,"Essington, Pennsylvania, United States",OK
    PSR,////,////,LIBP,UNK,0.741,Liberi,"Pescara, Italy",OWNO,////,////,Abruzzo International Airport,"Pescara, Abruzzo, Italy",OK
    PSS,////,////,SARP,UNK,1.0,Posadas,"Posadas, Argentina",OWNO,////,////,????,"Posadas, Argentina",OK

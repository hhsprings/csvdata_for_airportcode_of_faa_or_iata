
List for checking certainty of Great Circle Mapper (CIU - CNR)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    CIU,CIU,KCIU,KCIU,OK,0.893,Chippewa County,"Sault Ste Marie (MI), USA",OWNO,CHIPPEWA COUNTY INTL,"SAULT STE MARIE, MI - UNITED STATES",Chippewa County International Airport,"Sault Ste. Marie, Michigan, United States",OK
    CIW,////,////,TVSC,UNK,1.0,Canouan Island,"Canouan Island, Saint Vincent and the Grenadines",OWNO,////,////,????,"Canouan Island, Saint Vincent and The Grenadines",OK
    CIX,////,////,SPHI,UNK,0.316,Cornel Ruiz,"Chiclayo, Peru",OWNO,////,////,Captaán FAP José Abelardo Quiñones Gonzáles International Airport,"Chiclayo, Lambayeque, Perú",OK
    CIY,////,////,LICB,UNK,1.0,Comiso,"Comiso, Italy",OWNO,////,////,Comiso Airport,"Comiso, Sicily, Italy",OK
    CIZ,////,////,SWKO,UNK,1.0,Coari,"Coari, Brazil",OWNO,////,////,????,"Coari, Amazonas, Brazil",OK
    CJA,////,////,SPJR,UNK,0.308,Cajamarca,"Cajamarca, Peru",OWNO,////,////,Major General FAP Armando Revoredo Iglesias,"Cajamarca, Cajamarca, Perú",OK
    CJB,////,////,VOCB,UNK,0.765,Peelamedu,"Coimbatore, India",OWNO,////,////,????,"Coimbatore, Tamil Nadu, India",OK
    CJC,////,////,SCCF,UNK,1.0,El Loa,"Calama, Chile",OWNO,////,////,El Loa,"Calama, Antofagasta, Chile",OK
    CJD,////,////,////,UNK,1.0,Candilejas,"Candilejas, Colombia",OWNO,////,////,????,"Candilejas, Caquetá, Colombia",OK
    CJF,////,////,YCWA,UNK,nan,////,////,////,////,////,Coondewanna Airport,"Coondewanna, Western Australia, Australia",UNK
    CJH,////,////,////,UNK,0.552,Chilko Lake,"Chilko Lake, Canada",OWNO,////,////,Tsylos Park Lodge,"Chilko Lake, British Columbia, Canada",OK
    CJJ,////,////,RKTU,UNK,0.857,Cheong Ju Airport,"Cheong Ju City, South Korea",OWNO,////,////,Cheong Ju International Airport,"Cheong Ju, Republic of Korea (South Korea)",OK
    CJL,////,////,OPCH,UNK,1.0,Chitral,"Chitral, Pakistan",OWNO,////,////,Chitral Airport,"Chitral, Khyber Pakhtunkhwa, Pakistan",OK
    CJM,////,////,VTSE,UNK,1.0,Chumphon Airport,"Chumphon, Thailand",OWNO,////,////,????,"Chumphon, Chumphon, Thailand",OK
    CJS,////,////,MMCS,UNK,1.0,International Abraham Gonzalez,"Ciudad Juarez, Mexico",OWNO,////,////,Abraham González International Airport,"Ciudad Juárez, Chihuahua, México",OK
    CJT,////,////,MMCO,UNK,1.0,Copalar,"Comitan, Mexico",OWNO,////,////,Copalar,"Comitán, Chiapas, México",OK
    CJU,////,////,RKPC,UNK,0.429,Cheju Airport,"Cheju, South Korea",OWNO,////,////,Jeju International Airport,"Jeju, Republic of Korea (South Korea)",TO DO CHECK
    CKA,CKA,KCKA,KCKA,OK,0.658,Kegelman AF,"Cherokee (OK), USA",OWNO,KEGELMAN AF AUX FIELD,"CHEROKEE, OK - UNITED STATES",Kegelman AF Auxiliary Field,"Cherokee, Oklahoma, United States",OK
    CKB,CKB,KCKB,KCKB,OK,0.596,Benedum,"Clarksburg (WV), USA",OWNO,NORTH CENTRAL WEST VIRGINIA,"CLARKSBURG, WV - UNITED STATES",North Central West Virginia Airport,"Clarksburg, West Virginia, United States",OK
    CKC,////,////,UKKE,UNK,0.727,Cherkassy,"Cherkassy, Ukraine",OWNO,////,////,Cherkasy International Airport,"Cherkasy, Cherkasy, Ukraine",OK
    CKD,CJX,////,////,OK,1.0,Crooked Creek,"Crooked Creek (AK), USA",OWNO,CROOKED CREEK,"CROOKED CREEK, AK - UNITED STATES",????,"Crooked Creek, Alaska, United States",OK
    CKE,CN20,////,////,OK,0.402,Clear Lake,"Clear Lake (CA), USA",OWNO,FERNDALE RESORT,"KELSEYVILLE, CA - UNITED STATES",Ferndale Resort SPB,"Kelseyville, California, United States",OK
    CKG,////,////,ZUCK,UNK,0.878,Jiangbei International,"Chongqing, PR China",OWNO,////,////,Jiangbei Airport,"Chongqing, Chongqing, China",OK
    CKH,////,////,UESO,UNK,0.947,Chokurdah,"Chokurdah, Russia",OWNO,////,////,Chokurdakh Airport,"Chokurdakh, Sakha (Yakutiya), Russian Federation (Russia)",TO DO CHECK
    CKI,////,////,YCKI,UNK,1.0,Croker Island,"Croker Island, Australia",OWNO,////,////,????,"Croker Island, Northern Territory, Australia",OK
    CKM,CKM,KCKM,KCKM,OK,1.0,Fletcher Field,"Clarksdale (MS), USA",OWNO,FLETCHER FIELD,"CLARKSDALE, MS - UNITED STATES",Fletcher Field,"Clarksdale, Mississippi, United States",OK
    CKN,CKN,KCKN,KCKN,OK,0.581,Municipal,"Crookston (MN), USA",OWNO,CROOKSTON MUNI KIRKWOOD FLD,"CROOKSTON, MN - UNITED STATES",Crookston Municipal Kirkwood Field,"Crookston, Minnesota, United States",OK
    CKO,////,////,SSCP,UNK,1.0,Cornelio Procopio,"Cornelio Procopio, Brazil",OWNO,////,////,????,"Cornelio Procopio, Paraná, Brazil",OK
    CKS,////,////,SBCJ,UNK,1.0,Carajas,"Carajas, Brazil",OWNO,////,////,????,"Carajás, Pará, Brazil",OK
    CKT,////,////,OIMC,UNK,1.0,Sarakhs,"Sarakhs, Iran",OWNO,////,////,????,"Sarakhs, Khorasan-e Razavi, Iran",OK
    CKU,CKU,////,////,OK,0.563,City,"Cordova (AK), USA",OWNO,CORDOVA MUNI,"CORDOVA, AK - UNITED STATES",Cordova Municipal Airport,"Cordova, Alaska, United States",OK
    CKV,CKV,KCKV,KCKV,OK,1.0,Outlaw Field,"Clarksville (TN), USA",OWNO,OUTLAW FIELD,"CLARKSVILLE, TN - UNITED STATES",Outlaw Field,"Clarksville, Tennessee, United States",OK
    CKX,CKX,////,////,OK,1.0,Chicken,"Chicken (AK), USA",OWNO,CHICKEN,"CHICKEN, AK - UNITED STATES",????,"Chicken, Alaska, United States",OK
    CKY,////,////,GUCY,UNK,0.737,Conakry,"Conakry, Guinea",OWNO,////,////,Conakry International Airport,"Conakry, Guinea",OK
    CKZ,////,////,LTBH,UNK,1.0,Canakkale,"Canakkale, Turkey",OWNO,////,////,????,"Canakkale, Çanakkale, Turkey",OK
    CLA,////,////,VGCM,UNK,0.609,Comilla,"Comilla, Bangladesh",OWNO,////,////,Comilla STOLport,"Comilla, Bangladesh",OK
    CLD,CRQ,KCRQ,KCRQ,OK,0.501,Carlsbad,"Carlsbad (CA), USA",OWNO,MC CLELLAN-PALOMAR,"CARLSBAD, CA - UNITED STATES",Mc Clellan-Palomar Airport,"Carlsbad, California, United States",OK
    CLE,CLE,KCLE,KCLE,OK,1.0,Hopkins International,"Cleveland (OH), USA",OWNO,CLEVELAND-HOPKINS INTL,"CLEVELAND, OH - UNITED STATES",Cleveland-Hopkins International Airport,"Cleveland, Ohio, United States",OK
    CLG,C80,////,////,OK,0.694,Coalinga,"Coalinga (CA), USA",OWNO,NEW COALINGA MUNI,"COALINGA, CA - UNITED STATES",New Coalinga Municipal Airport,"Coalinga, California, United States",OK
    CLH,////,////,YCAH,UNK,1.0,Coolah,"Coolah, Australia",OWNO,////,////,????,"Coolah, New South Wales, Australia",OK
    CLI,CLI,KCLI,KCLI,OK,0.81,Clintonville,"Clintonville (WI), USA",OWNO,CLINTONVILLE MUNI,"CLINTONVILLE, WI - UNITED STATES",Clintonville Municipal Airport,"Clintonville, Wisconsin, United States",OK
    CLJ,////,////,LRCL,UNK,0.818,Napoca,"Cluj, Romania",OWNO,////,////,Cluj International Airport,"Cluj-Napoca, Romania",OK
    CLK,CLK,KCLK,KCLK,OK,0.64,Municipal,"Clinton (OK), USA",OWNO,CLINTON RGNL,"CLINTON, OK - UNITED STATES",Clinton Regional,"Clinton, Oklahoma, United States",OK
    CLL,CLL,KCLL,KCLL,OK,1.0,Easterwood Field,"College Station (TX), USA",OWNO,EASTERWOOD FIELD,"COLLEGE STATION, TX - UNITED STATES",Easterwood Field,"College Station, Texas, United States",OK
    CLM,CLM,KCLM,KCLM,OK,0.832,Fairchild International,"Port Angeles (WA), USA",OWNO,WILLIAM R FAIRCHILD INTL,"PORT ANGELES, WA - UNITED STATES",William R Fairchild International Airport,"Port Angeles, Washington, United States",OK
    CLN,////,////,SBCI,UNK,1.0,Carolina,"Carolina, Brazil",OWNO,////,////,????,"Carolina, Maranhão, Brazil",OK
    CLO,////,////,SKCL,UNK,0.811,Alfonso B. Aragon,"Cali, Colombia",OWNO,////,////,Alfonso Bonilla Aragón International Airport,"Cali, Valle del Cauca, Colombia",OK
    CLP,CLP,PFCL,PFCL,OK,1.0,Clarks Point,"Clarks Point (AK), USA",OWNO,CLARKS POINT,"CLARKS POINT, AK - UNITED STATES",????,"Clarks Point, Alaska, United States",OK
    CLQ,////,////,MMIA,UNK,0.286,Colima,"Colima, Mexico",OWNO,////,////,Licenciado Miguel de la Madrid,"Colima, Colima, México",OK
    CLR,CLR,KCLR,KCLR,OK,0.487,Calipatria,"Calipatria (CA), USA",OWNO,CLIFF HATFIELD MEMORIAL,"CALIPATRIA, CA - UNITED STATES",Cliff Hatfield Memorial Airport,"Calipatria, California, United States",OK
    CLS,CLS,KCLS,KCLS,OK,1.0,Centralia,"Chehalis (WA), USA",OWNO,CHEHALIS-CENTRALIA,"CHEHALIS, WA - UNITED STATES",Chehalis-Centralia Airport,"Chehalis, Washington, United States",OK
    CLT,CLT,KCLT,KCLT,OK,0.863,Douglas,"Charlotte (NC), USA",OWNO,CHARLOTTE/DOUGLAS INTL,"CHARLOTTE, NC - UNITED STATES",Charlotte/Douglas International Airport,"Charlotte, North Carolina, United States",OK
    CLU,BAK,KBAK,KBAK,OK,1.0,Columbus Municipal,"Columbus (IN), USA",OWNO,COLUMBUS MUNI,"COLUMBUS, IN - UNITED STATES",Columbus Municipal Airport,"Columbus, Indiana, United States",OK
    CLV,////,////,SBCN,UNK,1.0,Caldas Novas,"Caldas Novas, Brazil",OWNO,////,////,????,"Caldas Novas, Goiás, Brazil",OK
    CLW,CLW,KCLW,KCLW,OK,1.0,Clearwater Air Park,"Clearwater (FL), USA",OWNO,CLEARWATER AIR PARK,"CLEARWATER, FL - UNITED STATES",Clearwater Air Park,"Clearwater, Florida, United States",OK
    CLX,////,////,SATC,UNK,1.0,Clorinda,"Clorinda, Argentina",OWNO,////,////,????,"Clorinda, Formosa, Argentina",OK
    CLY,////,////,LFKC,UNK,1.0,Ste Catherine,"Calvi, France",OWNO,////,////,Ste-Catherine,"Calvi, Corse (Corsica), France",OK
    CLZ,////,////,SVCL,UNK,1.0,Calabozo,"Calabozo, Venezuela",OWNO,////,////,????,"Calabozo, Guárico, Venezuela",OK
    CMA,////,////,YCMU,UNK,1.0,Cunnamulla,"Cunnamulla, Australia",OWNO,////,////,????,"Cunnamulla, Queensland, Australia",OK
    CMB,////,////,VCBI,UNK,0.977,Bandaranayake International,"Colombo, Sri Lanka",OWNO,////,////,Bandaranaike International Airport,"Colombo, Western Province, Sri Lanka (Ceylon)",OK
    CMC,////,////,SNWC,UNK,1.0,Camocim,"Camocim, Brazil",OWNO,////,////,????,"Camocim, Ceará, Brazil",OK
    CMD,////,////,YCTM,UNK,1.0,Cootamundra,"Cootamundra, Australia",OWNO,////,////,????,"Cootamundra, New South Wales, Australia",OK
    CME,////,////,MMCE,UNK,0.872,Ciudad Del Carmen,"Ciudad Del Carmen, Mexico",OWNO,////,////,Ciudad del Carmen International Airport,"Ciudad del Carmen, Campeche, México",OK
    CMF,////,////,LFLB,UNK,0.533,Chambery,"Chambery, France",OWNO,////,////,Aix-lès-Bains,"Chambery, Rhône-Alpes, France",OK
    CMG,////,////,SBCR,UNK,0.727,Internacional,"Corumba, Brazil",OWNO,////,////,Corumbá International Airport,"Corumbá, Mato Grosso do Sul, Brazil",OK
    CMH,CMH,KCMH,KCMH,OK,1.0,Port Columbus International,"Columbus (OH), USA",OWNO,PORT COLUMBUS INTL,"COLUMBUS, - UNITED STATES",Port Columbus International Airport,"Columbus, Ohio, United States",OK
    CMI,CMI,KCMI,KCMI,OK,0.929,Willard University,"Champaign (IL), USA",OWNO,UNIVERSITY OF ILLINOIS-WILLARD,"CHAMPAIGN/URBANA, IL - UNITED STATES",University of Illinois-Willard Airport,"Champaign/Urbana, Illinois, United States",OK
    CMJ,////,////,RCCM,UNK,1.0,Chi Mei,"Chi Mei, Taiwan",OWNO,////,////,????,"Cimei, Pescadores Islands, Taiwan",OK
    CMK,////,////,FWCM,UNK,1.0,Club Makokola,"Club Makokola, Malawi",OWNO,////,////,????,"Club Makokola, Malawi",OK
    CML,////,////,YCMW,UNK,1.0,Camooweal,"Camooweal, Australia",OWNO,////,////,????,"Camooweal, Queensland, Australia",OK
    CMM,////,////,MGCR,UNK,1.0,Carmelita,"Carmelita, Guatemala",OWNO,////,////,????,"Carmelita, Petén, Guatemala",OK
    CMN,////,////,GMMN,UNK,0.914,Mohamed V,"Casablanca, Morocco",OWNO,////,////,Mohammed V International Airport,"Casablanca, Morocco",OK
    CMO,////,////,HCMO,UNK,1.0,Obbia,"Obbia, Somalia",OWNO,////,////,????,"Obbia, Mudug, Somalia",OK
    CMP,////,////,SNKE,UNK,1.0,Campo Alegre,"Santana Do Araguaia, Brazil",OWNO,////,////,Campo Alegre,"Santana do Araguaia, Pará, Brazil",OK
    CMQ,////,////,YCMT,UNK,1.0,Clermont,"Clermont, Australia",OWNO,////,////,????,"Clermont, Queensland, Australia",OK
    CMR,////,////,LFGA,UNK,1.0,Colmar-Houssen,"Colmar, France",OWNO,////,////,Houssen,"Colmar, Alsace, France",OK
    CMS,////,////,HCMS,UNK,1.0,Scusciuban,"Scusciuban, Somalia",OWNO,////,////,????,"Scusciuban, Bari, Somalia",OK
    CMT,////,////,////,UNK,1.0,Cameta,"Cameta, Brazil",OWNO,////,////,????,"Cametá, Pará, Brazil",OK
    CMU,////,////,AYCH,UNK,1.0,Chimbu,"Kundiawa, Papua New Guinea",OWNO,////,////,Chimbu,"Kundiawa, Chimbu, Papua-New Guinea",OK
    CMV,////,////,NZCX,UNK,1.0,Coromandel,"Coromandel, New Zealand",OWNO,////,////,????,"Coromandel, New Zealand",OK
    CMW,////,////,MUCM,UNK,0.958,Ign Agramonte International,"Camaguey, Cuba",OWNO,////,////,Ignacio Agramonte International Airport,"Camagüey, Camagüey, Cuba",OK
    CMX,CMX,KCMX,KCMX,OK,0.769,Houghton County,"Hancock (MI), USA",OWNO,HOUGHTON COUNTY MEMORIAL,"HANCOCK, MI - UNITED STATES",Houghton County Memorial Airport,"Hancock, Michigan, United States",OK
    CMY,CMY,KCMY,KCMY,OK,0.553,Camp Mccoy AAF,"Sparta (WI), USA",OWNO,SPARTA/FORT MC COY,"SPARTA, WI - UNITED STATES",Sparta/Fort Mc Coy Airport,"Sparta, Wisconsin, United States",OK
    CNA,////,////,MMCA,UNK,1.0,Cananea,"Cananea, Mexico",OWNO,////,////,????,"Cananea, Sonora, México",OK
    CNB,////,////,YCNM,UNK,1.0,Coonamble,"Coonamble, Australia",OWNO,////,////,????,"Coonamble, New South Wales, Australia",OK
    CNC,////,////,YCCT,UNK,0.72,Coconut Island,"Coconut Island, Australia",OWNO,////,////,????,"Poruma, Queensland, Australia",TO DO CHECK
    CND,////,////,LRCK,UNK,0.917,Mihail Kogălniceanu,"Constanta, Romania",OWNO,////,////,Mihail Kogainiceanu International Airport,"Constanta, Romania",OK
    CNE,1V6,////,////,OK,0.694,Canon City,"Canon City (CO), USA",OWNO,FREMONT COUNTY,"CANON CITY, CO - UNITED STATES",Fremont County Airport,"Canon City, Colorado, United States",OK
    CNF,////,////,SBCF,UNK,1.0,Tancredo Neves International Airport,"Belo Horizonte, Brazil",OWNO,////,////,Tancredo Neves International Airport,"Belo Horizonte, Minas Gerais, Brazil",OK
    CNG,////,////,LFBG,UNK,0.444,Parvaud,"Cognac, France",OWNO,////,////,Châteaubernard AB,"Cognac, Poitou-Charentes, France",OK
    CNH,CNH,KCNH,KCNH,OK,1.0,Municipal,"Claremont (NH), USA",OWNO,CLAREMONT MUNI,"CLAREMONT, NH - UNITED STATES",Claremont Municipal Airport,"Claremont, New Hampshire, United States",OK
    CNI,////,////,ZYCH,UNK,1.0,Changhai,"Changhai, PR China",OWNO,////,////,????,"Changhai, Liaoning, China",OK
    CNJ,////,////,YCCY,UNK,1.0,Cloncurry,"Cloncurry, Australia",OWNO,////,////,????,"Cloncurry, Queensland, Australia",OK
    CNK,CNK,KCNK,KCNK,OK,1.0,Blosser Municipal,"Concordia (KS), USA",OWNO,BLOSSER MUNI,"CONCORDIA, KS - UNITED STATES",Blosser Municipal Airport,"Concordia, Kansas, United States",OK
    CNL,////,////,EKSN,UNK,1.0,Sindal,"Sindal, Denmark",OWNO,////,////,????,"Sindal, Denmark",OK
    CNM,CNM,KCNM,KCNM,OK,0.208,Carlsbad,"Carlsbad (NM), USA",OWNO,CAVERN CITY AIR TRML,"CARLSBAD, NM - UNITED STATES",Cavern City Air Terminal Airport,"Carlsbad, New Mexico, United States",OK
    CNO,CNO,KCNO,KCNO,OK,1.0,Chino,"Chino (CA), USA",OWNO,CHINO,"CHINO, CA - UNITED STATES",????,"Chino, California, United States",OK
    CNP,////,////,BGCO,UNK,1.0,Neerlerit Inaat,"Neerlerit Inaat, Greenland",OWNO,////,////,????,"Neerlerit Inaat, Sermersooq, Greenland",OK
    CNQ,////,////,SARC,UNK,0.667,Camba Punta,"Corrientes, Argentina",OWNO,////,////,????,"Corrientes, Argentina",OK
    CNR,////,////,SCRA,UNK,1.0,Chanaral,"Chanaral, Chile",OWNO,////,////,Chañaral Airport,"Chañaral, Atacama, Chile",OK


List for checking certainty of Great Circle Mapper (GID - GRJ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    GID,////,////,HBBE,UNK,1.0,Gitega,"Gitega, Burundi",OWNO,////,////,????,"Gitega, Burundi",OK
    GIF,GIF,KGIF,KGIF,OK,1.0,Winter Haven's Gilbert Airport,"Winter Haven (FL), USA",OWNO,WINTER HAVEN'S GILBERT,"WINTER HAVEN, FL - UNITED STATES",Winter Haven's Gilbert Airport,"Winter Haven, Florida, United States",OK
    GIG,////,////,SBGL,UNK,0.667,Internacional,"Rio De Janeiro, Brazil",OWNO,////,////,Galeão - Antônio Carlos (Tom) Jobim International Airport,"Rio de Janeiro, Rio de Janeiro, Brazil",OK
    GII,////,////,GUSI,UNK,1.0,Siguiri,"Siguiri, Guinea",OWNO,////,////,????,"Siguiri, Guinea",OK
    GIL,////,////,OPGT,UNK,1.0,Gilgit,"Gilgit, Pakistan",OWNO,////,////,Gilgit Airport,"Gilgit, Gilgit-Baltistan, Pakistan",OK
    GIM,////,////,////,UNK,1.0,Miele Mimbale,"Miele Mimbale, Gabon",OWNO,////,////,????,"Miele Mimbale, Ogooué-Ivindo, Gabon",OK
    GIR,////,////,SKGI,UNK,0.615,Girardot,"Girardot, Colombia",OWNO,////,////,Santiago Vila Airport,"Girardot, Cundinamarca, Colombia",OK
    GIS,////,////,NZGS,UNK,1.0,Gisborne,"Gisborne, New Zealand",OWNO,////,////,????,"Gisborne, New Zealand",OK
    GIT,////,////,////,UNK,1.0,Geita,"Geita, Tanzania",OWNO,////,////,????,"Geita, Mwanza, Tanzania",OK
    GIU,////,////,VCCS,UNK,nan,////,////,////,////,////,Sigiriya Airport,"Sigiriya, Central Province, Sri Lanka (Ceylon)",UNK
    GIY,////,////,FAGI,UNK,1.0,Giyani,"Giyani, South Africa",OWNO,////,////,????,"Giyani, Limpopo, South Africa",OK
    GIZ,////,////,OEGN,UNK,0.296,Gizan,"Gizan, Saudi Arabia",OWNO,////,////,King Abdullah bin Abdulaziz Airport,"Jazan, Saudi Arabia",TO DO CHECK
    GJA,////,////,MHNJ,UNK,1.0,Guanaja,"Guanaja, Honduras",OWNO,////,////,????,"Guanaja, Islas de la Bahía, Honduras",OK
    GJL,////,////,DAAV,UNK,0.526,Jijel,"Jijel, Algeria",OWNO,////,////,Ferhat Abbas,"Jijel, Jijel, Algeria",OK
    GJM,////,////,SBGM,UNK,1.0,Guajara-Mirim,"Guajara-Mirim, Brazil",OWNO,////,////,????,"Guajara-Mirim, Rondônia, Brazil",OK
    GJR,////,////,BIGJ,UNK,1.0,Gjogur,"Gjogur, Iceland",OWNO,////,////,????,"Gjögur, Iceland",OK
    GJT,GJT,KGJT,KGJT,OK,0.627,Walker Field,"Grand Junction (CO), USA",OWNO,GRAND JUNCTION REGIONAL,"GRAND JUNCTION, CO - UNITED STATES",Grand Junction Regional,"Grand Junction, Colorado, United States",OK
    GKA,////,////,AYGA,UNK,1.0,Goroka,"Goroka, Papua New Guinea",OWNO,////,////,????,"Goroka, Eastern Highlands, Papua-New Guinea",OK
    GKD,////,////,LTFK,UNK,nan,////,////,////,////,////,Çanakkale Gökçeada Airport,"Çanakkale/Gökçeada, Turkey",UNK
    GKE,////,////,ETNG,UNK,1.0,Geilenkirchen,"Geilenkirchen, Germany",OWNO,////,////,????,"Geilenkirchen, North Rhine-Westphalia, Germany",OK
    GKH,////,////,VNGK,UNK,1.0,Gorkha,"Gorkha, Nepal",OWNO,////,////,????,"Gorkha, Nepal",OK
    GKK,////,////,VRMO,UNK,nan,////,////,////,////,////,Kooddoo Airport,"Kooddoo Island, Gaafu Alifu Atoll, Maldives",UNK
    GKL,////,////,YGKL,UNK,1.0,Great Keppel Island,"Great Keppel Island, Australia",OWNO,////,////,????,"Great Keppel Island, Queensland, Australia",OK
    GKN,GKN,PAGK,PAGK,OK,1.0,Gulkana,"Gulkana (AK), USA",OWNO,GULKANA,"GULKANA, AK - UNITED STATES",????,"Gulkana, Alaska, United States",OK
    GKO,////,////,////,UNK,0.522,Kongoboumba,"Kongoboumba, Gabon",OWNO,////,////,Kongo Boumba Airport,"Kongo Boumba, Ogooué-Ivindo, Gabon",OK
    GKT,GKT,KGKT,KGKT,OK,0.581,Gatlinburg,"Gatlinburg (TN), USA",OWNO,GATLINBURG-PIGEON FORGE,"SEVIERVILLE, TN - UNITED STATES",Gatlinburg-Pigeon Forge Airport,"Sevierville, Tennessee, United States",OK
    GLA,////,////,EGPF,UNK,0.467,Glasgow International,"Glasgow, United Kingdom",OWNO,////,////,Abbotsinch,"Glasgow, Renfrewshire, Scotland, United Kingdom",OK
    GLC,////,////,HAGL,UNK,0.923,Geladi,"Geladi, Ethiopia",OWNO,////,////,????,"Geladin, Somali, Ethiopia",TO DO CHECK
    GLD,GLD,KGLD,KGLD,OK,1.0,Renner Field,"Goodland (KS), USA",OWNO,RENNER FLD /GOODLAND MUNI/,"GOODLAND, KS - UNITED STATES",Renner Field,"Goodland, Kansas, United States",OK
    GLE,GLE,KGLE,KGLE,OK,1.0,Municipal,"Gainesville (TX), USA",OWNO,GAINESVILLE MUNI,"GAINESVILLE, TX - UNITED STATES",Gainesville Municipal Airport,"Gainesville, Texas, United States",OK
    GLF,////,////,MRGF,UNK,1.0,Golfito,"Golfito, Costa Rica",OWNO,////,////,????,"Golfito, Puntarenas, Costa Rica",OK
    GLG,////,////,YGLE,UNK,1.0,Glengyle,"Glengyle, Australia",OWNO,////,////,????,"Glengyle, Queensland, Australia",OK
    GLH,GLH,KGLH,KGLH,OK,0.664,Greenville,"Greenville (MS), USA",OWNO,GREENVILLE MID-DELTA,"GREENVILLE, MS - UNITED STATES",Greenville Mid-Delta Airport,"Greenville, Mississippi, United States",OK
    GLI,////,////,YGLI,UNK,1.0,Glen Innes,"Glen Innes, Australia",OWNO,////,////,????,"Glen Innes, New South Wales, Australia",OK
    GLK,////,////,HCMR,UNK,0.348,Galcaio,"Galcaio, Somalia",OWNO,////,////,Abdullahi Yusuf International Airport,"Galkayo, Mudug, Somalia",OK
    GLL,////,////,ENUK,UNK,1.0,Klanten Airport,"Gol, Norway",OWNO,////,////,Klanten,"Gol, Norway",OK
    GLM,////,////,YGLO,UNK,1.0,Glenormiston,"Glenormiston, Australia",OWNO,////,////,????,"Glenormiston, Queensland, Australia",OK
    GLN,////,////,GMAG,UNK,1.0,Goulimime,"Goulimime, Morocco",OWNO,////,////,????,"Goulimime, Morocco",OK
    GLO,////,////,EGBJ,UNK,1.0,Gloucestershire,"Gloucester, United Kingdom",OWNO,////,////,Gloucestershire,"Gloucester/Cheltenham, Gloucestershire, England, United Kingdom",OK
    GLP,////,////,////,UNK,1.0,Gulgubip,"Gulgubip, Papua New Guinea",OWNO,////,////,????,"Gulgubip, Western, Papua-New Guinea",OK
    GLR,GLR,KGLR,KGLR,OK,0.498,Otsego County,"Gaylord (MI), USA",OWNO,GAYLORD RGNL,"GAYLORD, MI - UNITED STATES",Gaylord Regional,"Gaylord, Michigan, United States",OK
    GLS,GLS,KGLS,KGLS,OK,0.701,Scholes Field,"Galveston (TX), USA",OWNO,SCHOLES INTL AT GALVESTON,"GALVESTON, TX - UNITED STATES",Scholes International at Galveston,"Galveston, Texas, United States",OK
    GLT,////,////,YGLA,UNK,1.0,Gladstone,"Gladstone, Australia",OWNO,////,////,????,"Gladstone, Queensland, Australia",OK
    GLV,GLV,PAGL,PAGL,OK,1.0,Golovin,"Golovin (AK), USA",OWNO,GOLOVIN,"GOLOVIN, AK - UNITED STATES",????,"Golovin, Alaska, United States",OK
    GLW,GLW,KGLW,KGLW,OK,1.0,Municipal,"Glasgow (KY), USA",OWNO,GLASGOW MUNI,"GLASGOW, KY - UNITED STATES",Glasgow Municipal Airport,"Glasgow, Kentucky, United States",OK
    GLX,////,////,WAMA,UNK,0.533,Galela,"Galela, Indonesia",OWNO,////,////,Gamarmalamo,"Galela, Maluku, Indonesia",OK
    GLY,////,////,////,UNK,1.0,Goldsworthy,"Goldsworthy, Australia",OWNO,////,////,????,"Goldsworthy, Western Australia, Australia",OK
    GLZ,////,////,EHGR,UNK,0.968,Gilze-Rijen,"Breda, Netherlands",OWNO,////,////,Gilze-Rijen AB,"Breda, Noord-Brabant (North Brabant), Netherlands",OK
    GMA,////,////,FZFK,UNK,1.0,Gemena,"Gemena, Congo (DRC)",OWNO,////,////,Gemana Airport,"Gemana, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    GMB,////,////,HAGM,UNK,1.0,Gambela,"Gambela, Ethiopia",OWNO,////,////,????,"Gambela, Gambela Peoples', Ethiopia",OK
    GMC,////,////,////,UNK,1.0,Guerima,"Guerima, Colombia",OWNO,////,////,????,"Guerima, Meta, Colombia",OK
    GME,////,////,UMGG,UNK,1.0,Gomel,"Gomel, Belarus",OWNO,////,////,Gomel Airport,"Gomel, Homyel'skaya (Gomel), Belarus",OK
    GMI,////,////,////,UNK,1.0,Gasmata Island,"Gasmata Island, Papua New Guinea",OWNO,////,////,????,"Gasmata Island, West New Britain, Papua-New Guinea",OK
    GML,////,////,UKKM,UNK,nan,////,////,////,////,////,Antonov International Airport,"Gostomel, Kiev, Ukraine",UNK
    GMM,////,////,FCOG,UNK,1.0,Gamboma,"Gamboma, Congo (ROC)",OWNO,////,////,????,"Gamboma, Congo (Republic of)",OK
    GMN,////,////,NZGM,UNK,1.0,Greymouth,"Greymouth, New Zealand",OWNO,////,////,????,"Greymouth, New Zealand",OK
    GMO,////,////,DNGO,UNK,nan,////,////,////,////,////,Gombe Lawanti International Airport,"Gombe, Gombe, Nigeria",UNK
    GMP,////,////,RKSS,UNK,0.842,Gimpo International,"Seoul, South Korea",OWNO,////,////,Gimpo,"Seoul, Republic of Korea (South Korea)",OK
    GMR,////,////,NTGJ,UNK,0.571,Gambier Is,"Gambier Is, French Polynesia",OWNO,////,////,Totegegie,"Mangareva, Gambier Islands, French Polynesia",MAYBE
    GMS,////,////,SNGM,UNK,1.0,Guimaraes,"Guimaraes, Brazil",OWNO,////,////,????,"Guimaraes, Maranhão, Brazil",OK
    GMT,GSZ,PAGZ,PAGZ,OK,0.651,Granite Mountain,"Granite Mountain (AK), USA",OWNO,GRANITE MOUNTAIN AS,"GRANITE MOUNTAIN, AK - UNITED STATES",Granite Mountain Air Station,"Granite Mountain, Alaska, United States",OK
    GMU,GMU,KGMU,KGMU,OK,0.79,Downtown,"Greer (SC), USA",OWNO,GREENVILLE DOWNTOWN,"GREENVILLE, SC - UNITED STATES",Greenville Downtown Airport,"Greenville, South Carolina, United States",OK
    GMV,UT25,////,////,OK,1.0,Monument Valley,"Goulding (UT), USA",OWNO,MONUMENT VALLEY,"MONUMENT VALLEY, UT - UNITED STATES",????,"Monument Valley, Utah, United States",OK
    GMZ,////,////,GCGM,UNK,1.0,La Gomera,"San Sebas de la Gomera, Canary Islands, Spain",OWNO,////,////,La Gomera,"San Sebas de la Gomera, Canary Islands, Spain",OK
    GNA,////,////,UMMG,UNK,0.5,Grodna,"Grodna, Belarus",OWNO,////,////,Obukhovo,"Hrodna, Hrodzenskaya (Grodno), Belarus",TO DO CHECK
    GNB,////,////,LFLS,UNK,0.923,Saint Geoirs,"Grenoble, France",OWNO,////,////,St-Geoirs,"Grenoble, Rhône-Alpes, France",OK
    GND,////,////,TGPY,UNK,0.615,Point Saline International,"Grenada, Grenada",OWNO,////,////,Maurice Bishop International Airport,"St. George's, Grenada, Grenada",OK
    GNG,GNG,KGNG,KGNG,OK,0.704,Gooding,"Gooding (ID), USA",OWNO,GOODING MUNI,"GOODING, ID - UNITED STATES",Gooding Municipal Airport,"Gooding, Idaho, United States",OK
    GNI,////,////,RCGI,UNK,0.545,Green Island,"Green Island, Taiwan",OWNO,////,////,????,"Lyudao, Tao Island, Taiwan",MAYBE
    GNM,////,////,SNGI,UNK,1.0,Guanambi,"Guanambi, Brazil",OWNO,////,////,????,"Guanambi, Bahia, Brazil",OK
    GNN,////,////,HAGH,UNK,1.0,Ghinnir,"Ghinnir, Ethiopia",OWNO,////,////,Ghinnir Airport,"Ghinnir, Oromia, Ethiopia",OK
    GNR,////,////,SAHR,UNK,0.541,General Roca,"General Roca, Argentina",OWNO,////,////,Dr. Arturo Umberto Illia,"General Roca, Río Negro, Argentina",OK
    GNS,////,////,WIMB,UNK,0.786,Gunungsitoli,"Gunungsitoli, Indonesia",OWNO,////,////,Binaka Airport,"Gunungsitoli, Nias Island, Sumatera Utara, Indonesia",OK
    GNT,GNT,KGNT,KGNT,OK,0.841,Milan,"Grants (NM), USA",OWNO,GRANTS-MILAN MUNI,"GRANTS, NM - UNITED STATES",Grants-Milan Municipal Airport,"Grants, New Mexico, United States",OK
    GNU,GNU,////,////,OK,0.826,Goodnews Bay,"Goodnews Bay (AK), USA",OWNO,GOODNEWS,"GOODNEWS, AK - UNITED STATES",????,"Goodnews, Alaska, United States",OK
    GNV,GNV,KGNV,KGNV,OK,0.507,J R Alison Municipal,"Gainesville (FL), USA",OWNO,GAINESVILLE RGNL,"GAINESVILLE, FL - UNITED STATES",Gainesville Regional,"Gainesville, Florida, United States",OK
    GNY,////,////,LTCS,UNK,nan,////,////,////,////,////,Sanliurfa Güney Anadolu Projesi Airport,"Sanliurfa, Sanliorfa, Turkey",UNK
    GNZ,////,////,FBGZ,UNK,1.0,Ghanzi,"Ghanzi, Botswana",OWNO,////,////,Ghanzi Airport,"Ghanzi, Ghanzi, Botswana",OK
    GOA,////,////,LIMJ,UNK,1.0,Cristoforo Colombo,"Genoa, Italy",OWNO,////,////,Genoa Cristoforo Colombo,"Genoa, Liguria, Italy",OK
    GOB,////,////,HAGB,UNK,0.667,Goba,"Goba, Ethiopia",OWNO,////,////,Robe Airport,"Goba, Oromia, Ethiopia",OK
    GOC,////,////,////,UNK,1.0,Gora,"Gora, Papua New Guinea",OWNO,////,////,????,"Gora, Northern, Papua-New Guinea",OK
    GOE,////,////,////,UNK,1.0,Gonalia,"Gonalia, Papua New Guinea",OWNO,////,////,????,"Gonalia, East New Britain, Papua-New Guinea",OK
    GOG,////,////,FYGB,UNK,1.0,Gobabis,"Gobabis, Namibia",OWNO,////,////,????,"Gobabis, Namibia",OK
    GOH,////,////,BGGH,UNK,1.0,Nuuk,"Nuuk, Greenland",OWNO,////,////,Godthåb/Nuuk,"Nuuk, Sermersooq, Greenland",OK
    GOI,////,////,VOGO,UNK,1.0,Dabolim,"Goa, India",OWNO,////,////,Dabolim Airport,"Goa, Goa, India",OK
    GOJ,////,////,UWGG,UNK,0.833,Nizhniy Novgorod,"Nizhniy Novgorod, Russia",OWNO,////,////,Nizhny Novgorod International Airport,"Nizhny Novgorod, Nizhegorodskaya, Russian Federation (Russia)",OK
    GOK,GOK,KGOK,KGOK,OK,0.529,Guthrie,"Guthrie (OK), USA",OWNO,GUTHRIE-EDMOND RGNL,"GUTHRIE, OK - UNITED STATES",Guthrie-Edmond Regional,"Guthrie, Oklahoma, United States",OK
    GOL,4S1,////,////,OK,0.64,State,"Gold Beach (OR), USA",OWNO,GOLD BEACH MUNI,"GOLD BEACH, OR - UNITED STATES",Gold Beach Municipal Airport,"Gold Beach, Oregon, United States",OK
    GOM,////,////,FZNA,UNK,0.615,Goma,"Goma, Congo (DRC)",OWNO,////,////,Goma International Airport,"Goma, Nord-Kivu (North Kivu), Democratic Republic of Congo (Zaire)",OK
    GON,GON,KGON,KGON,OK,0.745,New London,"New London (CT), USA",OWNO,GROTON-NEW LONDON,"GROTON (NEW LONDON), CT - UNITED STATES",Groton-New London Airport,"Groton, Connecticut, United States",OK
    GOO,////,////,YGDI,UNK,1.0,Goondiwindi,"Goondiwindi, Australia",OWNO,////,////,????,"Goondiwindi, Queensland, Australia",OK
    GOP,////,////,VEGK,UNK,1.0,Gorakhpur,"Gorakhpur, India",OWNO,////,////,????,"Gorakhpur, Uttar Pradesh, India",OK
    GOQ,////,////,ZLGM,UNK,1.0,Golmud,"Golmud, PR China",OWNO,////,////,????,"Golmud, Qinghai, China",OK
    GOR,////,////,HAGR,UNK,1.0,Gore,"Gore, Ethiopia",OWNO,////,////,Gore Airport,"Gore, Oromia, Ethiopia",OK
    GOS,////,////,////,UNK,1.0,Gosford,"Gosford, Australia",OWNO,////,////,????,"Gosford, New South Wales, Australia",OK
    GOT,////,////,ESGG,UNK,1.0,Landvetter,"Gothenburg, Sweden",OWNO,////,////,Landvetter,"Göteborg, Västra Götalands län, Sweden",OK
    GOU,////,////,FKKR,UNK,0.706,Garoua,"Garoua, Cameroon",OWNO,////,////,Garoua International Airport,"Garoua, North (Nord), Cameroon",OK
    GOV,////,////,YPGV,UNK,1.0,Nhulunbuy,"Gove, Australia",OWNO,////,////,Nhulunbuy,"Gove, Northern Territory, Australia",OK
    GOZ,////,////,LBGO,UNK,1.0,Gorna Orechovitsa,"Gorna Orechovitsa, Bulgaria",OWNO,////,////,Orechovitsa,"Gorna Orechovitsa, Lovech, Bulgaria",OK
    GPA,////,////,LGRX,UNK,1.0,Araxos Airport,"Patras, Greece",OWNO,////,////,Araxos,"Patras, Dytikí Elláda (Western Greece), Greece",OK
    GPB,////,////,SBGU,UNK,0.958,Tancredo Thomaz Faria,"Guarapuava, Brazil",OWNO,////,////,Tancredo Thomaz de Faria,"Guarapuava, Paraná, Brazil",OK
    GPD,////,////,YGON,UNK,nan,////,////,////,////,////,Mount Gordon Mine Airport,"Mount Gordon, Queensland, Australia",UNK
    GPI,////,////,SKGP,UNK,0.345,Guapi,"Guapi, Colombia",OWNO,////,////,Juan Casiano Solis Airport,"Guapi, Cauca, Colombia",OK
    GPL,////,////,MRGP,UNK,1.0,Guapiles,"Guapiles, Costa Rica",OWNO,////,////,????,"Guápiles, Limón, Costa Rica",OK
    GPN,////,////,YGPT,UNK,1.0,Garden Point,"Garden Point, Australia",OWNO,////,////,Garden Point,"Pirlangimpi, Melville Island, Northern Territory, Australia",OK
    GPO,////,////,SAZG,UNK,1.0,General Pico,"General Pico, Argentina",OWNO,////,////,????,"General Pico, La Pampa, Argentina",OK
    GPS,////,////,SEGS,UNK,1.0,Baltra,"Galapagos Is, Ecuador",OWNO,////,////,????,"Baltra, Baltra Island, Galápagos Islands, Ecuador",OK
    GPT,GPT,KGPT,KGPT,OK,0.81,Biloxi Regional,"Gulfport (MS), USA",OWNO,GULFPORT-BILOXI INTL,"GULFPORT, MS - UNITED STATES",Gulfport-Biloxi International Airport,"Gulfport, Mississippi, United States",OK
    GPZ,GPZ,KGPZ,KGPZ,OK,1.0,Grand Rapids,"Grand Rapids (MN), USA",OWNO,GRAND RAPIDS/ITASCA CO-GORDON NEWSTROM FLD,"GRAND RAPIDS, MN - UNITED STATES",Grand Rapids/Itasca Co-Gordon Newstrom Field,"Grand Rapids, Minnesota, United States",OK
    GQQ,GQQ,KGQQ,KGQQ,OK,0.694,Galion,"Galion (OH), USA",OWNO,GALION MUNI,"GALION, OH - UNITED STATES",Galion Municipal Airport,"Galion, Ohio, United States",OK
    GRA,////,////,////,UNK,1.0,Gamarra,"Gamarra, Colombia",OWNO,////,////,????,"Gamarra, Cesar, Colombia",OK
    GRB,GRB,KGRB,KGRB,OK,0.783,Austin-Straubel Field,"Green Bay (WI), USA",OWNO,GREEN BAY-AUSTIN STRAUBEL INTL,"GREEN BAY, WI - UNITED STATES",Green Bay-Austin Straubel International Airport,"Green Bay, Wisconsin, United States",OK
    GRC,////,////,////,UNK,1.0,Grand Cess,"Grand Cess, Liberia",OWNO,////,////,????,"Grand Cess, Liberia",OK
    GRD,GRD,KGRD,KGRD,OK,0.781,Greenwood,"Greenwood (SC), USA",OWNO,GREENWOOD COUNTY,"GREENWOOD, SC - UNITED STATES",Greenwood County Airport,"Greenwood, South Carolina, United States",OK
    GRE,GRE,KGRE,KGRE,OK,0.826,Municipal,"Greenville (IL), USA",OWNO,GREENVILLE,"GREENVILLE, IL - UNITED STATES",????,"Greenville, Illinois, United States",OK
    GRF,GRF,KGRF,KGRF,OK,1.0,Gray AAF,"Tacoma (WA), USA",OWNO,GRAY AAF (JOINT BASE LEWIS-MCCHORD),"FORT LEWIS/TACOMA, WA - UNITED STATES",Gray AAF Airport,"Fort Lewis/Tacoma, Washington, United States",OK
    GRG,////,////,OAGZ,UNK,1.0,Gardez,"Gardez, Afghanistan",OWNO,////,////,????,"Gardez, Paktia, Afghanistan",OK
    GRH,////,////,////,UNK,1.0,Garuahi,"Garuahi, Papua New Guinea",OWNO,////,////,????,"Garuahi, Milne Bay, Papua-New Guinea",OK
    GRI,GRI,KGRI,KGRI,OK,0.584,Grand Island,"Grand Island (NE), USA",OWNO,CENTRAL NEBRASKA RGNL,"GRAND ISLAND, NE - UNITED STATES",Central Nebraska Regional,"Grand Island, Nebraska, United States",OK
    GRJ,////,////,FAGG,UNK,1.0,George,"George, South Africa",OWNO,////,////,????,"George, Western Cape, South Africa",OK


List for checking certainty of Great Circle Mapper (LAA - LFQ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    LAA,LAA,KLAA,KLAA,OK,0.621,Lamar Field,"Lamar (CO), USA",OWNO,LAMAR MUNI,"LAMAR, CO - UNITED STATES",Lamar Municipal Airport,"Lamar, Colorado, United States",OK
    LAB,////,////,////,UNK,1.0,Lablab,"Lablab, Papua New Guinea",OWNO,////,////,????,"Lablab, Morobe, Papua-New Guinea",OK
    LAC,////,////,////,UNK,0.652,Layang-Layang Airstrip,"Pulau Layang-Layang Island, Malaysia",OWNO,////,////,Layang-Layang Airport,"Layang-Layang Island, Spratly Islands, Sabah, Malaysia",MAYBE
    LAD,////,////,FNLU,UNK,0.808,4 de Fevereiro,"Luanda, Angola",OWNO,////,////,Aeroporto 4 de Fevereiro,"Luanda, Angola",OK
    LAE,////,////,AYNZ,UNK,1.0,Lae Nadzab Airport,"Lae, Papua New Guinea",OWNO,////,////,Lae Nadzab Airport,"Lae, Morobe, Papua-New Guinea",OK
    LAF,LAF,KLAF,KLAF,OK,1.0,Purdue University,"Lafayette (IN), USA",OWNO,PURDUE UNIVERSITY,"LAFAYETTE, IN - UNITED STATES",Purdue University Airport,"Lafayette, Indiana, United States",OK
    LAH,////,////,WAPH,UNK,0.48,Labuha,"Labuha, Indonesia",OWNO,////,////,Oesman Sadik Airport,"Labuha, Halmahera Island, Maluku Utara, Indonesia",OK
    LAI,////,////,LFRO,UNK,1.0,Servel,"Lannion, France",OWNO,////,////,Servel,"Lannion, Bretagne, France",OK
    LAJ,////,////,SBLJ,UNK,1.0,Lages,"Lages, Brazil",OWNO,////,////,????,"Lages, Santa Catarina, Brazil",OK
    LAK,////,CYKD,CYKD,OK,0.289,Aklavik,"Aklavik, Canada",OWNO,AKLAVIK,"AKLAVIK, - CANADA",Freddie Carmichael Airport,"Aklavik, Northwest Territories, Canada",OK
    LAL,LAL,KLAL,KLAL,OK,1.0,Lakeland Linder Regional Airport,"Lakeland (FL), USA",OWNO,LAKELAND LINDER RGNL,"LAKELAND, - UNITED STATES",Lakeland Linder Regional,"Lakeland, Florida, United States",OK
    LAM,LAM,KLAM,KLAM,OK,1.0,Los Alamos,"Los Alamos (NM), USA",OWNO,LOS ALAMOS,"LOS ALAMOS, NM - UNITED STATES",????,"Los Alamos, New Mexico, United States",OK
    LAN,LAN,KLAN,KLAN,OK,0.726,Capital City,"Lansing (MI), USA",OWNO,CAPITAL REGION INTL,"LANSING, MI - UNITED STATES",Capital Region International Airport,"Lansing, Michigan, United States",OK
    LAO,////,////,RPLI,UNK,0.667,Laoag,"Laoag, Philippines",OWNO,////,////,Laoag International Airport,"Laoag, Luzon Island, Philippines",OK
    LAP,////,////,MMLP,UNK,0.415,Leon,"La Paz, Mexico",OWNO,////,////,General Manuel Márquez de León International Airport,"La Paz, Baja California Sur, México",OK
    LAQ,////,////,HLLQ,UNK,0.625,La Braq,"Beida, Libya",OWNO,////,////,Al Abraq International Airport,"Bayda, Libyan Arab Jamahiriya (Libya)",OK
    LAR,LAR,KLAR,KLAR,OK,0.445,General Brees Field,"Laramie (WY), USA",OWNO,LARAMIE RGNL,"LARAMIE, WY - UNITED STATES",Laramie Regional,"Laramie, Wyoming, United States",OK
    LAS,LAS,KLAS,KLAS,OK,0.619,Mccarran International,"Las Vegas (NV), USA",OWNO,MC CARRAN INTL,"LAS VEGAS, NV - UNITED STATES",Mc Carran International Airport,"Las Vegas, Nevada, United States",OK
    LAU,////,////,HKLU,UNK,0.571,Lamu,"Lamu, Kenya",OWNO,////,////,Mwana,"Lamu, Kenya",OK
    LAW,LAW,KLAW,KLAW,OK,0.36,Municipal,"Lawton (OK), USA",OWNO,LAWTON-FORT SILL RGNL,"LAWTON, OK - UNITED STATES",Lawton-Fort Sill Regional,"Lawton, Oklahoma, United States",OK
    LAX,LAX,KLAX,KLAX,OK,1.0,International,"Los Angeles (CA), USA",OWNO,LOS ANGELES INTL,"LOS ANGELES, CA - UNITED STATES",Los Angeles International Airport,"Los Angeles, California, United States",OK
    LAY,////,////,FALY,UNK,1.0,Ladysmith,"Ladysmith, South Africa",OWNO,////,////,Ladysmith Airport,"Ladysmith, KwaZulu-Natal, South Africa",OK
    LAZ,////,////,SBLP,UNK,1.0,Bom Jesus Da Lapa,"Bom Jesus Da Lapa, Brazil",OWNO,////,////,????,"Bom Jesus da Lapa, Bahia, Brazil",OK
    LBA,////,////,EGNM,UNK,0.88,Leeds/Bradford,"Leeds, United Kingdom",OWNO,////,////,Leeds Bradford International Airport,"Leeds/Bradford, Yorkshire, England, United Kingdom",OK
    LBB,LBB,KLBB,KLBB,OK,0.518,International,"Lubbock (TX), USA",OWNO,LUBBOCK PRESTON SMITH INTL,"LUBBOCK, TX - UNITED STATES",Lubbock Preston Smith International Airport,"Lubbock, Texas, United States",OK
    LBC,////,////,EDHL,UNK,0.615,Lübeck Airport,"Hamburg, Germany",OWNO,////,////,Blankensee,"Lübeck, Schleswig-Holstein, Germany",OK
    LBD,////,////,UTDL,UNK,0.75,Khudzhand,"Khudzhand, Tajikistan",OWNO,////,////,Khujand Airport,"Khujand, Sughd, Tajikistan",TO DO CHECK
    LBE,LBE,KLBE,KLBE,OK,0.473,Westmoreland County,"Latrobe (PA), USA",OWNO,ARNOLD PALMER RGNL,"LATROBE, PA - UNITED STATES",Arnold Palmer Regional,"Latrobe, Pennsylvania, United States",OK
    LBF,LBF,KLBF,KLBF,OK,0.874,Lee Bird Field,"North Platte (NE), USA",OWNO,NORTH PLATTE RGNL AIRPORT LEE BIRD FIELD,"NORTH PLATTE, NE - UNITED STATES",North Platte Regional Airport Lee Bird Field,"North Platte, Nebraska, United States",OK
    LBG,////,////,LFPB,UNK,1.0,Le Bourget,"Paris, France",OWNO,////,////,Le Bourget,"Paris, Île-de-France, France",OK
    LBH,////,////,////,UNK,1.0,Palm Beach SPB,"Sydney, Australia",OWNO,////,////,Palm Beach SPB,"Palm Beach, New South Wales, Australia",OK
    LBI,////,////,LFCI,UNK,1.0,Le Sequestre,"Albi, France",OWNO,////,////,Le Sequestre,"Albi, Midi-Pyrénées, France",OK
    LBJ,////,////,WATO,UNK,0.643,Mutiara,"Labuan Bajo, Indonesia",OWNO,////,////,Komodo Airport,"Labuan Bajo, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    LBK,////,////,////,UNK,1.0,Liboi,"Liboi, Kenya",OWNO,////,////,????,"Liboi, Kenya",OK
    LBL,LBL,KLBL,KLBL,OK,0.554,Municipal,"Liberal (KS), USA",OWNO,LIBERAL MID-AMERICA RGNL,"LIBERAL, KS - UNITED STATES",Liberal Mid-America Regional,"Liberal, Kansas, United States",OK
    LBM,////,////,////,UNK,1.0,Luabo,"Luabo, Mozambique",OWNO,////,////,????,"Luabo, Mozambique",OK
    LBN,////,////,////,UNK,1.0,Lake Baringo,"Lake Baringo, Kenya",OWNO,////,////,????,"Lake Baringo, Kenya",OK
    LBO,////,////,FZVI,UNK,1.0,Lusambo,"Lusambo, Congo (DRC)",OWNO,////,////,Lusambo Airport,"Lusambo, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    LBP,////,////,////,UNK,0.75,Long Banga Airfield,"Long Banga, Malaysia",OWNO,////,////,????,"Long Banga, Sarawak, Malaysia",OK
    LBQ,////,////,FOGR,UNK,1.0,Lambarene,"Lambarene, Gabon",OWNO,////,////,????,"Lambarene, Moyen-Ogooué, Gabon",OK
    LBR,////,////,SWLB,UNK,1.0,Labrea,"Labrea, Brazil",OWNO,////,////,????,"Lábrea, Amazonas, Brazil",OK
    LBS,////,////,NFNL,UNK,1.0,Labasa,"Labasa, Fiji",OWNO,////,////,????,"Labasa, Fiji",OK
    LBT,LBT,KLBT,KLBT,OK,0.834,Lumberton,"Lumberton (NC), USA",OWNO,LUMBERTON RGNL,"LUMBERTON, NC - UNITED STATES",Lumberton Regional,"Lumberton, North Carolina, United States",OK
    LBU,////,////,WBKL,UNK,1.0,Labuan,"Labuan, Malaysia",OWNO,////,////,????,"Labuan, Wilayah Persekutuan Labuan, Malaysia",OK
    LBV,////,////,FOOL,UNK,0.727,Libreville,"Libreville, Gabon",OWNO,////,////,Leon M'Ba,"Libreville, Estuaire, Gabon",OK
    LBW,////,////,WALB,UNK,0.571,Long Bawan,"Long Bawan, Indonesia",OWNO,////,////,Juvai Semaring Airport,"Long Bawan, Kalimantan Timur (East Borneo), Indonesia",OK
    LBX,////,////,RPLU,UNK,1.0,Lubang,"Lubang, Philippines",OWNO,////,////,????,"Lubang, Lubang Island, Philippines",OK
    LBY,////,////,LFRE,UNK,0.545,Montoir,"La Baule, France",OWNO,////,////,????,"La Baule-Escoublac, Pays de la Loire, France",MAYBE
    LBZ,////,////,FNLK,UNK,1.0,Lukapa,"Lukapa, Angola",OWNO,////,////,????,"Lucapa, Angola",OK
    LCA,////,////,LCLK,UNK,1.0,Larnaca,"Larnaca, Cyprus",OWNO,////,////,????,"Larnaca, Cyprus",OK
    LCB,////,////,////,UNK,1.0,Pontes e Lacerda,"Pontes e Lacerda, Brazil",OWNO,////,////,????,"Pontes e Lacerda, Mato Grosso, Brazil",OK
    LCC,////,////,LIBN,UNK,1.0,Galatina,"Lecce, Italy",OWNO,////,////,Galatina,"Lecce, Apulia, Italy",OK
    LCD,////,////,FALO,UNK,1.0,Louis Trichardt,"Louis Trichardt, South Africa",OWNO,////,////,Louis Trichardt Airport,"Louis Trichardt, Limpopo, South Africa",OK
    LCE,////,////,MHLC,UNK,1.0,Goloson International,"La Ceiba, Honduras",OWNO,////,////,Golosón International Airport,"La Ceiba, Atlántida, Honduras",OK
    LCF,////,////,MGRD,UNK,1.0,Las Vegas,"Rio Dulce, Guatemala",OWNO,////,////,Las Vegas Airport,"Río Dulce, Izabal, Guatemala",OK
    LCG,////,////,LECO,UNK,0.769,La Coruna,"La Coruna, Spain",OWNO,////,////,A Coruña Airport,"A Coruña, Galicia, Spain",MAYBE
    LCH,LCH,KLCH,KLCH,OK,0.79,Municipal,"Lake Charles (LA), USA",OWNO,LAKE CHARLES RGNL,"LAKE CHARLES, LA - UNITED STATES",Lake Charles Regional,"Lake Charles, Louisiana, United States",OK
    LCI,LCI,KLCI,KLCI,OK,1.0,Municipal,"Laconia (NH), USA",OWNO,LACONIA MUNI,"LACONIA, NH - UNITED STATES",Laconia Municipal Airport,"Laconia, New Hampshire, United States",OK
    LCJ,////,////,EPLL,UNK,0.514,Lodz Lublinek,"Lodz, Poland",OWNO,////,////,Lodz Wladyslaw Reymont Airport,"Lodz, Lódzkie, Poland",OK
    LCK,LCK,KLCK,KLCK,OK,0.859,Rickenbacker,"Columbus (OH), USA",OWNO,RICKENBACKER INTL,"COLUMBUS, OH - UNITED STATES",Rickenbacker International Airport,"Columbus, Ohio, United States",OK
    LCL,////,////,MULM,UNK,1.0,La Coloma,"La Coloma, Cuba",OWNO,////,////,????,"La Coloma, Pinar del Río, Cuba",OK
    LCM,////,////,SACC,UNK,1.0,La Cumbre,"La Cumbre, Argentina",OWNO,////,////,????,"La Cumbre, Córdoba, Argentina",OK
    LCN,////,////,YBLC,UNK,1.0,Balcanoona,"Balcanoona, Australia",OWNO,////,////,????,"Balcanoona, South Australia, Australia",OK
    LCO,////,////,FCBL,UNK,1.0,Lague,"Lague, Congo (ROC)",OWNO,////,////,????,"Lague, Congo (Republic of)",OK
    LCR,////,////,////,UNK,1.0,La Chorrera,"La Chorrera, Colombia",OWNO,////,////,????,"La Chorrera, Amazonas, Colombia",OK
    LCV,////,////,LIQL,UNK,0.27,Lucca,"Lucca, Italy",OWNO,////,////,Lucca-Tassignano Enrico Squaglia Airport,"Lucca, Tuscany, Italy",OK
    LCX,////,////,ZSLO,UNK,1.0,Guanzhishan,Longyan,IATA,////,////,Guanzaishan,"Longyan, Fujian, China",MAYBE
    LCY,////,////,EGLC,UNK,1.0,London City Airport,"London, United Kingdom",OWNO,////,////,London City Airport,"London, England, United Kingdom",OK
    LDA,////,////,VEMH,UNK,1.0,Malda,"Malda, India",OWNO,////,////,????,"Malda, West Bengal, India",OK
    LDB,////,////,SBLO,UNK,1.0,Londrina,"Londrina, Brazil",OWNO,////,////,????,"Londrina, Paraná, Brazil",OK
    LDC,////,////,YLIN,UNK,1.0,Lindeman Island,"Lindeman Island, Australia",OWNO,////,////,????,"Lindeman Island, Queensland, Australia",OK
    LDE,////,////,LFBT,UNK,0.833,Tarbes Ossun Lourdes,"Lourdes/Tarbes, France",OWNO,////,////,????,"Tarbes/Lourdes/Pyrénées, Midi-Pyrénées, France",OK
    LDG,////,////,ULAL,UNK,nan,////,////,////,////,////,Leshukonskoye Airport,"Leshukonskoye, Arkhangel'skaya, Russian Federation (Russia)",UNK
    LDH,////,////,YLHI,UNK,1.0,Lord Howe Island,"Lord Howe Island, Australia",OWNO,////,////,????,"Lord Howe Island, New South Wales, Australia",OK
    LDI,////,////,HTLI,UNK,0.556,Kikwetu,"Lindi, Tanzania",OWNO,////,////,Lindi Airport,"Lindi, Lindi, Tanzania",OK
    LDJ,LDJ,KLDJ,KLDJ,OK,1.0,Linden,"Linden (NJ), USA",OWNO,LINDEN,"LINDEN, NJ - UNITED STATES",????,"Linden, New Jersey, United States",OK
    LDK,////,////,ESGL,UNK,0.88,Hovby,"Lidkoping, Sweden",OWNO,////,////,Lidköping AB,"Lidköping, Östergötlands län, Sweden",OK
    LDM,LDM,KLDM,KLDM,OK,1.0,Mason County,"Ludington (MI), USA",OWNO,MASON COUNTY,"LUDINGTON, MI - UNITED STATES",Mason County Airport,"Ludington, Michigan, United States",OK
    LDN,////,////,VNLD,UNK,1.0,Lamidanda,"Lamidanda, Nepal",OWNO,////,////,????,"Lamidanda, Nepal",OK
    LDO,////,////,SMDO,UNK,0.56,Ladouanie,"Ladouanie, Suriname",OWNO,////,////,Laduani Airstrip,"Laduani, Sipaliwini, Suriname",OK
    LDR,////,////,////,UNK,1.0,Lodar,"Lodar, Yemen",OWNO,////,////,????,"Lodar, Yemen",OK
    LDS,////,////,ZYLD,UNK,nan,////,////,////,////,////,Yichun Lindu Airport,"Yichun, Heilongjiang, China",UNK
    LDU,////,////,WBKD,UNK,1.0,Lahad Datu,"Lahad Datu, Malaysia",OWNO,////,////,????,"Lahad Datu, Sabah, Malaysia",OK
    LDV,////,////,LFRJ,UNK,1.0,Landivisiau,"Landivisiau, France",OWNO,////,////,Landivisiau Airport,"Landivisiau, Bretagne, France",OK
    LDW,////,////,////,UNK,1.0,Lansdowne,"Lansdowne, Australia",OWNO,////,////,????,"Lansdowne, Western Australia, Australia",OK
    LDX,////,////,SOOM,UNK,1.0,St-Laurent du Maroni,"St Laurent du Maroni, French Guiana",OWNO,////,////,????,"St-Laurent du Maroni, French Guiana",OK
    LDY,////,////,EGAE,UNK,1.0,Eglinton,"Londonderry, United Kingdom",OWNO,////,////,Eglinton,"Londonderry, County Londonderry, Northern Ireland, United Kingdom",OK
    LDZ,////,////,////,UNK,1.0,Londolozi,"Londolozi, South Africa",OWNO,////,////,????,"Londolozi, Mpumalanga, South Africa",OK
    LEA,////,////,YPLM,UNK,1.0,Learmonth,"Learmonth, Australia",OWNO,////,////,????,"Learmonth, Western Australia, Australia",OK
    LEB,LEB,KLEB,KLEB,OK,0.766,Hanover/Lebanon/White River Airport,"White River (VT), USA",OWNO,LEBANON MUNI,"LEBANON, NH - UNITED STATES",Lebanon Municipal Airport,"Lebanon, New Hampshire, United States",OK
    LEC,////,////,SBLE,UNK,1.0,Chapada Diamantina,"Lencois, Brazil",OWNO,////,////,Chapada Diamantina,"Lençõis, Bahia, Brazil",OK
    LED,////,////,ULLI,UNK,1.0,Pulkovo,"St Petersburg, Russia",OWNO,////,////,Pulkovo Airport,"St. Petersburg, Leningradskaya, Russian Federation (Russia)",OK
    LEE,LEE,KLEE,KLEE,OK,0.87,Leesburg,"Leesburg (FL), USA",OWNO,LEESBURG INTL,"LEESBURG, FL - UNITED STATES",Leesburg International Airport,"Leesburg, Florida, United States",OK
    LEF,////,////,FXLK,UNK,1.0,Lebakeng,"Lebakeng, Lesotho",OWNO,////,////,????,"Lebakeng, Lesotho",OK
    LEG,////,////,////,UNK,1.0,Aleg,"Aleg, Mauritania",OWNO,////,////,????,"Aleg, Mauritania",OK
    LEH,////,////,LFOH,UNK,1.0,Octeville,"Le Havre, France",OWNO,////,////,Octeville,"Le Havre, Haute-Normandie (Upper Normandy), France",OK
    LEI,////,////,LEAM,UNK,0.737,Almeria,"Almeria, Spain",OWNO,////,////,Almería International Airport,"Almería, Andalusia, Spain",OK
    LEJ,////,////,EDDP,UNK,1.0,Leipzig/Halle Airport,"Leipzig, Germany",OWNO,////,////,Leipzig/Halle Airport,"Leipzig, Saxony, Germany",OK
    LEK,////,////,GULB,UNK,1.0,Labe,"Labe, Guinea",OWNO,////,////,????,"Labe, Guinea",OK
    LEL,////,////,YLEV,UNK,1.0,Lake Evella,"Lake Evella, Australia",OWNO,////,////,????,"Lake Evella, Northern Territory, Australia",OK
    LEM,LEM,KLEM,KLEM,OK,0.826,lemmon,"Lemmon (SD), USA",OWNO,LEMMON MUNI,"LEMMON, SD - UNITED STATES",Lemmon Municipal Airport,"Lemmon, South Dakota, United States",OK
    LEN,////,////,LELN,UNK,0.316,Leon,"Leon, Spain",OWNO,////,////,Aeropuero de Bajio,"León, Castille and León, Spain",OK
    LEO,////,////,////,UNK,1.0,Leconi,"Leconi, Gabon",OWNO,////,////,????,"Lekoni, Haut-Ogooué, Gabon",OK
    LEP,////,////,SNDN,UNK,1.0,Leopoldina,"Leopoldina, Brazil",OWNO,////,////,????,"Leopoldina, Minas Gerais, Brazil",OK
    LEQ,////,////,EGHC,UNK,1.0,Lands End,"Lands End, United Kingdom",OWNO,////,////,Land's End Aerodrome,"St. Just, Cornwall, England, United Kingdom",OK
    LER,////,////,YLST,UNK,1.0,Leinster,"Leinster, Australia",OWNO,////,////,????,"Leinster, Western Australia, Australia",OK
    LES,////,////,FXLS,UNK,1.0,Lesobeng,"Lesobeng, Lesotho",OWNO,////,////,????,"Lesobeng, Lesotho",OK
    LET,////,////,SKLT,UNK,0.656,Gen. A.V. Cobo,"Leticia, Colombia",OWNO,////,////,General Alfredo Vásquez Cobo International Airport,"Leticia, Amazonas, Colombia",OK
    LEU,////,////,LESU,UNK,0.692,Aeroport De La Seu,"Seo De Urgel, Spain",OWNO,////,////,Andorra-La Seu d'Urgell Airport,"Montferrer, Catalonia, Spain",MAYBE
    LEV,////,////,NFNB,UNK,1.0,Levuka Airfield,"Bureta, Fiji",OWNO,////,////,Levuka Airfield,"Bureta, Ovalau Island, Fiji",OK
    LEW,LEW,KLEW,KLEW,OK,1.0,Auburn Airport,"Lewiston (ME), USA",OWNO,AUBURN/LEWISTON MUNI,"AUBURN/LEWISTON, ME - UNITED STATES",Auburn/Lewiston Municipal Airport,"Auburn/Lewiston, Maine, United States",OK
    LEX,LEX,KLEX,KLEX,OK,1.0,Blue Grass,"Lexington (KY), USA",OWNO,BLUE GRASS,"LEXINGTON, KY - UNITED STATES",Blue Grass Airport,"Lexington, Kentucky, United States",OK
    LEY,////,////,EHLE,UNK,1.0,Lelystad,"Lelystad, Netherlands",OWNO,////,////,????,"Lelystad, Flevoland, Netherlands",OK
    LEZ,////,////,MHLE,UNK,1.0,La Esperanza,"La Esperanza, Honduras",OWNO,////,////,????,"La Esperanza, Intibucá, Honduras",OK
    LFI,LFI,KLFI,KLFI,OK,1.0,Langley AFB,"Williamsburg (VA), USA",OWNO,LANGLEY AFB,"HAMPTON, VA - UNITED STATES",Langley AFB,"Hampton, Virginia, United States",OK
    LFM,////,////,OISR,UNK,nan,////,////,////,////,////,????,"Lamerd, Fars, Iran",UNK
    LFN,LHZ,KLHZ,KLHZ,OK,0.551,Franklin,"Louisburg (NC), USA",OWNO,TRIANGLE NORTH EXECUTIVE,"LOUISBURG, NC - UNITED STATES",Triangle North Executive Airport,"Louisburg, North Carolina, United States",OK
    LFO,////,////,HAKL,UNK,0.714,Kelafo,"Kelafo/Callaf, Ethiopia",OWNO,////,////,Kelafo East,"Kelafo, Somali, Ethiopia",OK
    LFP,////,////,YLFD,UNK,1.0,Lakefield,"Lakefield, Australia",OWNO,////,////,????,"Lakefield, Queensland, Australia",OK
    LFQ,////,////,////,UNK,1.0,Linfen Qiaoli Airport,"Linfen, Shanxi, China",OWNO,////,////,Linfen Qiaoli Airport,"Linfen, Shanxi, China",OK

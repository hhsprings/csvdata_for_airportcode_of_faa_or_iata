
List for checking certainty of Great Circle Mapper (ILI - ISC)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ILI,ILI,PAIL,PAIL,OK,1.0,Iliamna,"Iliamna (AK), USA",OWNO,ILIAMNA,"ILIAMNA, AK - UNITED STATES",????,"Iliamna, Alaska, United States",OK
    ILK,////,////,FMMQ,UNK,0.476,Ilaka,"Ilaka, Madagascar",OWNO,////,////,Atsinanana,"Ilaka, Madagascar",OK
    ILM,ILM,KILM,KILM,OK,0.672,New Hanover County,"Wilmington (NC), USA",OWNO,WILMINGTON INTL,"WILMINGTON, NC - UNITED STATES",Wilmington International Airport,"Wilmington, North Carolina, United States",OK
    ILN,ILN,KILN,KILN,OK,0.439,Clinton Field,"Wilmington (OH), USA",OWNO,WILMINGTON AIR PARK,"WILMINGTON, OH - UNITED STATES",Wilmington Air Park,"Wilmington, Ohio, United States",OK
    ILO,////,////,RPVI,UNK,1.0,Iloilo International Airport,"Iloilo, Philippines",OWNO,////,////,Iloilo International Airport,"Iloilo, Philippines",OK
    ILP,////,////,NWWE,UNK,0.9,Ile Des Pins,"Ile Des Pins, New Caledonia",OWNO,////,////,Moué,"Île des Pins, New Caledonia",OK
    ILQ,////,////,SPLO,UNK,1.0,Ilo,"Ilo, Peru",OWNO,////,////,Ilo Airport,"Ilo, Moquegua, Perú",OK
    ILR,////,////,DNIL,UNK,1.0,Ilorin,"Ilorin, Nigeria",OWNO,////,////,Ilorin Airport,"Ilorin, Kwara, Nigeria",OK
    ILU,////,////,HKKL,UNK,1.0,Kilaguni,"Kilaguni, Kenya",OWNO,////,////,????,"Kilaguni, Kenya",OK
    ILX,////,////,////,UNK,1.0,Ileg,"Ileg, Papua New Guinea",OWNO,////,////,????,"Ileg, Madang, Papua-New Guinea",OK
    ILY,////,////,EGPI,UNK,0.588,Glenegedale,"Islay, United Kingdom",OWNO,////,////,Port Ellen Airport,"Islay, Argyll, Scotland, United Kingdom",OK
    ILZ,////,////,LZZI,UNK,1.0,Zilina,"Zilina, Slovakia",OWNO,////,////,????,"Zilina, Slovakia",OK
    IMA,////,////,////,UNK,1.0,Iamalele,"Iamalele, Papua New Guinea",OWNO,////,////,????,"Iamalele, Milne Bay, Papua-New Guinea",OK
    IMB,////,////,SYIB,UNK,1.0,Imbaimadai,"Imbaimadai, Guyana",OWNO,////,////,????,"Imbaimadai, Cuyuni-Mazaruni, Guyana",OK
    IMD,////,////,////,UNK,1.0,Imonda,"Imonda, Papua New Guinea",OWNO,////,////,????,"Imonda, Sandaun, Papua-New Guinea",OK
    IMF,////,////,VEIM,UNK,0.8,Municipal,"Imphal, India",OWNO,////,////,????,"Imphal, Manipur, India",OK
    IMG,////,////,////,UNK,1.0,Inhaminga,"Inhaminga, Mozambique",OWNO,////,////,????,"Inhaminga, Mozambique",OK
    IMI,N20,////,////,OK,0.25,Ine Island,"Ine Island, Marshall Islands",OWNO,INE,"ARNO ATOLL, - MARSHALL ISLANDS",Ine,"Arno Atoll, Marshall Islands",MAYBE
    IMK,////,////,VNST,UNK,1.0,Simikot,"Simikot, Nepal",OWNO,////,////,????,"Simikot, Nepal",OK
    IML,IML,KIML,KIML,OK,0.766,Imperial,"Imperial (NE), USA",OWNO,IMPERIAL MUNI,"IMPERIAL, NE - UNITED STATES",Imperial Municipal Airport,"Imperial, Nebraska, United States",OK
    IMM,IMM,KIMM,KIMM,OK,0.76,Immokalee,"Immokalee (FL), USA",OWNO,IMMOKALEE RGNL,"IMMOKALEE, - UNITED STATES",Immokalee Regional,"Immokalee, Florida, United States",OK
    IMN,////,////,////,UNK,1.0,Imane,"Imane, Papua New Guinea",OWNO,////,////,????,"Imane, Morobe, Papua-New Guinea",OK
    IMO,////,////,FEFZ,UNK,1.0,Zemio,"Zemio, Central African Republic",OWNO,////,////,Zemio Airport,"Zemio, Haut-Mbomou (Tö-Mbömü), Central African Republic",OK
    IMP,////,////,SBIZ,UNK,1.0,Imperatriz,"Imperatriz, Brazil",OWNO,////,////,????,"Imperatriz, Maranhão, Brazil",OK
    IMT,IMT,KIMT,KIMT,OK,1.0,Ford,"Iron Mountain (MI), USA",OWNO,FORD,"IRON MOUNTAIN KINGSFORD, MI - UNITED STATES",Ford Airport,"Iron Mountain/Kingsford, Michigan, United States",OK
    INA,////,////,UUYI,UNK,1.0,Inta,"Inta, Russia",OWNO,////,////,Inta Airport,"Inta, Komi, Russian Federation (Russia)",OK
    INB,////,////,////,UNK,1.0,Independence,"Independence, Belize",OWNO,////,////,Independence Airport,"Independence, Stann Creek, Belize",OK
    INC,////,////,ZLIC,UNK,0.821,Yinchuan,"Yinchuan, PR China",OWNO,////,////,Hedong,"Yinchuan, Ningxia, China",OK
    IND,IND,KIND,KIND,OK,1.0,Indianapolis International,"Indianapolis (IN), USA",OWNO,INDIANAPOLIS INTL,"INDIANAPOLIS, IN - UNITED STATES",Indianapolis International Airport,"Indianapolis, Indiana, United States",OK
    INE,////,////,////,UNK,1.0,Chinde,"Chinde, Mozambique",OWNO,////,////,????,"Chinde, Mozambique",OK
    INF,////,////,DATG,UNK,1.0,In Guezzam,"In Guezzam, Algeria",OWNO,////,////,In Guezzam Airport,"In Guezzam, Tamanrasset, Algeria",OK
    ING,////,////,SAWA,UNK,1.0,Lago Argentino,"Lago Argentino, Argentina",OWNO,////,////,????,"Lago Argentino, Santa Cruz, Argentina",OK
    INH,////,////,FQIN,UNK,1.0,Inhambane,"Inhambane, Mozambique",OWNO,////,////,????,"Inhambane, Mozambique",OK
    INI,////,////,LYNI,UNK,0.182,Nis,"Nis, Serbia",OWNO,////,////,Constantine the Great International Airport,"Nis, Serbia",OK
    INJ,////,////,YINJ,UNK,1.0,Injune,"Injune, Australia",OWNO,////,////,????,"Injune, Queensland, Australia",OK
    INK,INK,KINK,KINK,OK,0.479,Wink,"Wink (TX), USA",OWNO,WINKLER COUNTY,"WINK, TX - UNITED STATES",Winkler County Airport,"Wink, Texas, United States",OK
    INL,INL,KINL,KINL,OK,0.529,Falls International,"International Falls (MN), USA",OWNO,FALLS INTL-EINARSON FIELD,"INTERNATIONAL FALLS, MN - UNITED STATES",Falls International-Einarson Field,"International Falls, Minnesota, United States",OK
    INM,////,////,YINN,UNK,1.0,Innamincka,"Innamincka, Australia",OWNO,////,////,????,"Innamincka, South Australia, Australia",OK
    INN,////,////,LOWI,UNK,0.609,Kranebitten,"Innsbruck, Austria",OWNO,////,////,Innsbruck Airport,"Innsbruck, Tirol (Tyrol), Austria",OK
    INO,////,////,FZBA,UNK,1.0,Inongo,"Inongo, Congo (DRC)",OWNO,////,////,Inongo Airport,"Inongo, Bandundu, Democratic Republic of Congo (Zaire)",OK
    INQ,////,////,EIIR,UNK,1.0,Inisheer,"Inisheer, Ireland",OWNO,////,////,????,"Inisheer, Aran Islands, Connacht, Ireland",OK
    INS,INS,KINS,KINS,OK,0.781,Af Aux,"Indian Springs (NV), USA",OWNO,CREECH AFB,"INDIAN SPRINGS, NV - UNITED STATES",Creech AFB,"Indian Springs, Nevada, United States",OK
    INT,INT,KINT,KINT,OK,1.0,Smith-Reynolds,"Winston Salem (NC), USA",OWNO,SMITH REYNOLDS,"WINSTON SALEM, NC - UNITED STATES",Smith Reynolds Airport,"Winston Salem, North Carolina, United States",OK
    INU,////,////,ANYN,UNK,1.0,Nauru International Airport,"Nauru Island, Nauru",OWNO,////,////,Nauru International Airport,"Nauru, Nauru",OK
    INV,////,////,EGPE,UNK,1.0,Inverness,"Inverness, United Kingdom",OWNO,////,////,????,"Inverness, Inverness, Scotland, United Kingdom",OK
    INW,INW,KINW,KINW,OK,0.445,Winslow,"Winslow (AZ), USA",OWNO,WINSLOW-LINDBERGH RGNL,"WINSLOW, AZ - UNITED STATES",Winslow-Lindbergh Regional,"Winslow, Arizona, United States",OK
    INX,////,////,WASI,UNK,1.0,Inanwatan,"Inanwatan, Indonesia",OWNO,////,////,????,"Inanwatan, Papua, Indonesia",OK
    INY,////,////,////,UNK,1.0,Inyati,"Inyati, South Africa",OWNO,////,////,????,"Inyati, Mpumalanga, South Africa",OK
    INZ,////,////,DAUI,UNK,1.0,In Salah,"In Salah, Algeria",OWNO,////,////,In Salah Airport,"In Salah, Tamanrasset, Algeria",OK
    IOA,////,////,LGIO,UNK,1.0,Ioannina,"Ioannina, Greece",OWNO,////,////,????,"Ioannina, Ípeiros (Epirus), Greece",OK
    IOK,////,////,////,UNK,1.0,Iokea,"Iokea, Papua New Guinea",OWNO,////,////,????,"Iokea, Gulf, Papua-New Guinea",OK
    IOM,////,////,EGNS,UNK,0.667,Ronaldsway,"Isle Of Man, United Kingdom",OWNO,////,////,Isle of Man Airport,"Isle of Man, Isle of Man",OK
    ION,////,////,FCOI,UNK,1.0,Impfondo,"Impfondo, Congo (ROC)",OWNO,////,////,????,"Impfondo, Congo (Republic of)",OK
    IOP,////,////,////,UNK,1.0,Ioma,"Ioma, Papua New Guinea",OWNO,////,////,????,"Ioma, Northern, Papua-New Guinea",OK
    IOR,////,////,EIIM,UNK,1.0,Kilronan,"Inishmore, Ireland",OWNO,////,////,Inishmore,"Kilronan, Inishmore, Aran Islands, Connacht, Ireland",OK
    IOS,////,////,SBIL,UNK,0.526,Eduardo Gomes,"Ilheus, Brazil",OWNO,////,////,Jorge Amado Airport,"Ilhéus, Bahia, Brazil",OK
    IOW,IOW,KIOW,KIOW,OK,0.613,Iowa City,"Iowa City IA, USA",OWNO,IOWA CITY MUNI,"IOWA CITY, - UNITED STATES",Iowa City Municipal Airport,"Iowa City, Iowa, United States",OK
    IPA,////,////,NVVI,UNK,1.0,Ipota,"Ipota, Vanuatu",OWNO,////,////,????,"Ipota, Erromango Island, Taféa, Vanuatu",OK
    IPC,////,////,SCIP,UNK,1.0,Mataveri International,"Easter Island, Chile",OWNO,////,////,Mataveri International Airport,"Easter Island, Valparaíso, Chile",OK
    IPE,////,////,RPMV,UNK,1.0,Ipil,"Ipil, Philippines",OWNO,////,////,????,"Ipil, Zamboanga Sibugay, Philippines",OK
    IPG,////,////,SWII,UNK,1.0,Ipiranga,"Ipiranga, Brazil",OWNO,////,////,Ipiranga,"Santo Antônio do Içá, Amazonas, Brazil",OK
    IPH,////,////,WMKI,UNK,0.308,Ipoh,"Ipoh, Malaysia",OWNO,////,////,Sultan Azlan Shah,"Ipoh, Perak, Malaysia",OK
    IPI,////,////,SKIP,UNK,1.0,San Luis,"Ipiales, Colombia",OWNO,////,////,San Luis Airport,"Ipiales, Nariño, Colombia",OK
    IPL,IPL,KIPL,KIPL,OK,1.0,Imperial County Airport,"Imperial (CA), USA",OWNO,IMPERIAL COUNTY,"IMPERIAL, CA - UNITED STATES",Imperial County Airport,"Imperial, California, United States",OK
    IPN,////,////,SBIP,UNK,1.0,Usiminas,"Ipatinga, Brazil",OWNO,////,////,Usiminas,"Ipatinga/Santana Do Paraiso, Minas Gerais, Brazil",OK
    IPT,IPT,KIPT,KIPT,OK,0.572,Lycoming County,"Williamsport (PA), USA",OWNO,WILLIAMSPORT RGNL,"WILLIAMSPORT, PA - UNITED STATES",Williamsport Regional,"Williamsport, Pennsylvania, United States",OK
    IPU,////,////,SNIU,UNK,1.0,Ipiau,"Ipiau, Brazil",OWNO,////,////,????,"Ipiau, Bahia, Brazil",OK
    IQM,////,////,ZWCM,UNK,1.0,Qiemo,"Qiemo, PR China",OWNO,////,////,????,"Qiemo, Xinjiang, China",OK
    IQN,////,////,ZLQY,UNK,1.0,Qingyang,"Qingyang, PR China",OWNO,////,////,????,"Qingyang, Gansu, China",OK
    IQQ,////,////,SCDA,UNK,0.571,Cavancha,"Iquique, Chile",OWNO,////,////,Diego Aracena International Airport,"Iquique, Tarapacá, Chile",OK
    IQT,////,////,SPQT,UNK,0.529,C.F. Secada,"Iquitos, Peru",OWNO,////,////,Coronel FAP Francisco Secada Vignetta International Airport,"Iquitos, Loreto, Perú",OK
    IRA,////,////,AGGK,UNK,0.625,Kirakira,"Kirakira, Solomon Islands",OWNO,////,////,Ngorangora,"Kirakira, Makira Island, Solomon Islands",OK
    IRB,2F0,////,////,OK,1.0,Municipal,"Iraan (TX), USA",OWNO,IRAAN MUNI,"IRAAN, TX - UNITED STATES",Iraan Municipal Airport,"Iraan, Texas, United States",OK
    IRC,CRC,PACR,PACR,OK,1.0,Circle City,"Circle (AK), USA",OWNO,CIRCLE CITY,"CIRCLE, AK - UNITED STATES",Circle City Airport,"Circle, Alaska, United States",OK
    IRD,////,////,VGIS,UNK,1.0,Ishurdi,"Ishurdi, Bangladesh",OWNO,////,////,????,"Ishhurdi, Bangladesh",OK
    IRE,////,////,SNIC,UNK,1.0,Irece,"Irece, Brazil",OWNO,////,////,????,"Irece, Bahia, Brazil",OK
    IRG,////,////,YLHR,UNK,1.0,Lockhart River,"Lockhart River, Australia",OWNO,////,////,????,"Lockhart River, Queensland, Australia",OK
    IRI,////,////,HTIR,UNK,0.667,Nduli,"Iringa, Tanzania",OWNO,////,////,????,"Iringa, Iringa, Tanzania",OK
    IRJ,////,////,SANL,UNK,0.4,La Rioja,"La Rioja, Argentina",OWNO,////,////,Capitan Vicente Almando,"La Rioja, La Rioja, Argentina",OK
    IRK,IRK,KIRK,KIRK,OK,0.766,Municipal,"Kirksville (MO), USA",OWNO,KIRKSVILLE RGNL,"KIRKSVILLE, MO - UNITED STATES",Kirksville Regional,"Kirksville, Missouri, United States",OK
    IRM,////,////,USHI,UNK,nan,////,////,////,////,////,Igrim Airport,"Igrim, Khanty-Mansiyskiy, Russian Federation (Russia)",UNK
    IRN,////,////,////,UNK,1.0,Iriona,"Iriona, Honduras",OWNO,////,////,????,"Iriona, Colón, Honduras",OK
    IRO,////,////,FEFI,UNK,1.0,Birao,"Birao, Central African Republic",OWNO,////,////,????,"Birao, Vakaga, Central African Republic",OK
    IRP,////,////,FZJH,UNK,1.0,Matari,"Isiro, Congo (DRC)",OWNO,////,////,Matari Airport,"Isiro, Orientale, Democratic Republic of Congo (Zaire)",OK
    IRS,IRS,KIRS,KIRS,OK,1.0,Kirsch Municipal,"Sturgis (MI), USA",OWNO,KIRSCH MUNI,"STURGIS, MI - UNITED STATES",Kirsch Municipal Airport,"Sturgis, Michigan, United States",OK
    IRU,////,////,////,UNK,0.643,SPB,"Iranamadu, St Thomas Island",IATA,////,////,Iranamadu Waterdrome,"Iranamadu, Northern Province, Sri Lanka (Ceylon)",MAYBE
    IRZ,////,////,SWTP,UNK,nan,////,////,////,////,////,Santa Izabel do Río Negro,"Santa Izabel do Río Negro, Amazonas, Brazil",UNK
    ISA,////,////,YBMA,UNK,1.0,Mount Isa,"Mount Isa, Australia",OWNO,////,////,????,"Mount Isa, Queensland, Australia",OK
    ISB,////,////,OPRN,UNK,0.667,Islamabad International,"Islamabad, Pakistan",OWNO,////,////,Benazir Bhutto International Airport,"Islamabad, Punjab, Pakistan",OK
    ISC,////,////,EGHE,UNK,1.0,St Marys,"Isles Of Scilly, United Kingdom",OWNO,////,////,????,"St. Mary's, Isles of Scilly, England, United Kingdom",OK

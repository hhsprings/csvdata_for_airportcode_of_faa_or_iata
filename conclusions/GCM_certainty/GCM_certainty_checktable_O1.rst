
List for checking certainty of Great Circle Mapper (OAA - OKU)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    OAA,////,////,OASH,UNK,nan,////,////,////,////,////,FOB Shank,"Gardez, Logar, Afghanistan",UNK
    OAG,////,////,YORG,UNK,1.0,Springhill,"Orange, Australia",OWNO,////,////,Springhill,"Orange, New South Wales, Australia",OK
    OAI,////,////,OAIX,UNK,nan,////,////,////,////,////,????,"Bagram, Parwan, Afghanistan",UNK
    OAJ,OAJ,KOAJ,KOAJ,OK,1.0,Albert J Ellis,"Jacksonville (NC), USA",OWNO,ALBERT J ELLIS,"JACKSONVILLE, NC - UNITED STATES",Albert J Ellis Airport,"Jacksonville, North Carolina, United States",OK
    OAK,OAK,KOAK,KOAK,OK,0.935,Metropolitan Oak International,"Oakland (CA), USA",OWNO,METROPOLITAN OAKLAND INTL,"OAKLAND, CA - UNITED STATES",Metropolitan Oakland International Airport,"Oakland, California, United States",OK
    OAL,////,////,SSKW,UNK,0.588,Cacoal,"Cacoal, Brazil",OWNO,////,////,Capital do Café Airport,"Cacoal, Rondônia, Brazil",OK
    OAM,////,////,NZOU,UNK,1.0,Oamaru,"Oamaru, New Zealand",OWNO,////,////,????,"Oamaru, New Zealand",OK
    OAN,////,////,MHOA,UNK,1.0,Olanchito,"Olanchito, Honduras",OWNO,////,////,????,"Olanchito, Yoro, Honduras",OK
    OAS,////,////,OASA,UNK,nan,////,////,////,////,////,Sharana AB,"Sharana, Paktia, Afghanistan",UNK
    OAX,////,////,MMOX,UNK,0.872,Xoxocotlan,"Oaxaca, Mexico",OWNO,////,////,Xoxocotlán International Airport,"Oaxaca, Oaxaca, México",OK
    OAZ,////,////,OAZI,UNK,nan,////,////,////,////,////,????,"Camp Bastion, Helmand, Afghanistan",UNK
    OBA,////,////,////,UNK,1.0,Oban,"Oban, Australia",OWNO,////,////,????,"Oban, Queensland, Australia",OK
    OBC,////,////,HDOB,UNK,1.0,Obock,"Obock, Djibouti",OWNO,////,////,????,"Obock, Djibouti",OK
    OBD,////,////,////,UNK,1.0,Obano,"Obano, Indonesia",OWNO,////,////,????,"Obano, Papua, Indonesia",OK
    OBE,OBE,KOBE,KOBE,OK,1.0,County,"Okeechobee (FL), USA",OWNO,OKEECHOBEE COUNTY,"OKEECHOBEE, - UNITED STATES",Okeechobee County Airport,"Okeechobee, Florida, United States",OK
    OBF,////,////,EDMO,UNK,0.889,Oberpfaffenhofen,"Oberpfaffenhofen, Germany",OWNO,////,////,Dornier/DLR,"Oberpfaffenhofen, Bavaria, Germany",OK
    OBI,////,////,SNTI,UNK,1.0,Obidos,"Obidos, Brazil",OWNO,////,////,????,"Óbidos, Pará, Brazil",OK
    OBL,////,////,EBZR,UNK,0.424,Zoersel,"Zoersel, Belgium",OWNO,////,////,Zoersel-Oostmalle Airfield,"Zoersel, Antwerpen, Belgium",OK
    OBM,////,////,////,UNK,1.0,Morobe,"Morobe, Papua New Guinea",OWNO,////,////,????,"Morobe, Morobe, Papua-New Guinea",OK
    OBN,////,////,EGEO,UNK,0.737,Connel,"Oban, United Kingdom",OWNO,////,////,Oban Airfield,"Oban, Argyll, Scotland, United Kingdom",OK
    OBO,////,////,RJCB,UNK,0.636,Obihiro,"Obihiro, Japan",OWNO,////,////,Tokachi-Obihiro Airport,"Obihiro, Hokkaido, Japan",OK
    OBS,////,////,LFHO,UNK,1.0,Vals-Lanas,"Aubenas, France",OWNO,////,////,Vals-Lanas,"Aubenas, Rhône-Alpes, France",OK
    OBU,OBU,PAOB,PAOB,OK,1.0,Kobuk/Wien,"Kobuk (AK), USA",OWNO,KOBUK,"KOBUK, AK - UNITED STATES",????,"Kobuk, Alaska, United States",OK
    OBX,////,////,////,UNK,1.0,Obo,"Obo, Papua New Guinea",OWNO,////,////,????,"Obo, Western, Papua-New Guinea",OK
    OBY,////,////,BGSC,UNK,0.78,Ittoqqortoormiit,"Ittoqqortoormiit, Greenland",OWNO,////,////,Ittoqqortoormiit Heliport,"Scoresbysund, Sermersooq, Greenland",OK
    OCA,07FA,////,////,OK,0.771,Ocean Reef,"Ocean Reef (FL), USA",OWNO,OCEAN REEF CLUB,"KEY LARGO, FL - UNITED STATES",Ocean Reef Club Airport,"Key Largo, Florida, United States",OK
    OCC,////,////,SECO,UNK,0.316,Coca,"Coca, Ecuador",OWNO,////,////,Francisco de Oreliana,"El Coca, Oreliana, Ecuador",MAYBE
    OCE,OXB,KOXB,KOXB,OK,1.0,Municipal,"Ocean City (MD), USA",OWNO,OCEAN CITY MUNI,"OCEAN CITY, MD - UNITED STATES",Ocean City Municipal Airport,"Ocean City, Maryland, United States",OK
    OCF,OCF,KOCF,KOCF,OK,0.726,Taylor Field,"Ocala (FL), USA",OWNO,OCALA INTL-JIM TAYLOR FIELD,"OCALA, FL - UNITED STATES",Ocala International-Jim Taylor Field,"Ocala, Florida, United States",OK
    OCH,OCH,KOCH,KOCH,OK,0.496,Lufkin Nacogdoches,"Nacogdoches (TX), USA",OWNO,A L MANGHAM JR RGNL,"NACOGDOCHES, TX - UNITED STATES",A L Mangham Jr. Regional,"Nacogdoches, Texas, United States",OK
    OCJ,////,////,MKBS,UNK,1.0,Boscobel,"Ocho Rios, Jamaica",OWNO,////,////,Boscobel,"Ocho Rios, Jamaica",OK
    OCM,////,////,YBGD,UNK,nan,////,////,////,////,////,Boolgeeda Airport,"Boolgeeda, Western Australia, Australia",UNK
    OCN,OKB,KOKB,KOKB,OK,0.445,Municipal,"Oceanside (CA), USA",OWNO,BOB MAXWELL MEMORIAL AIRFIELD,"OCEANSIDE, CA - UNITED STATES",Bob Maxwell Memorial Airfield,"Oceanside, California, United States",OK
    OCS,////,////,////,UNK,0.769,Corisco Island,Corisco Island,IATA,////,////,Corisco International Airport,"Corisco, Litoral, Equatorial Guinea",MAYBE
    OCV,////,////,SKOC,UNK,1.0,Aguasclaras,"Ocana, Colombia",OWNO,////,////,Aguas Claras Airport,"Ocaña, Norte de Santander, Colombia",OK
    OCW,OCW,KOCW,KOCW,OK,0.867,Warren Field,"Washington (NC), USA",OWNO,WASHINGTON-WARREN,"WASHINGTON, NC - UNITED STATES",Washington-Warren Airport,"Washington, North Carolina, United States",OK
    ODA,////,////,FEFW,UNK,1.0,Ouadda,"Ouadda, Central African Republic",OWNO,////,////,????,"Ouadda, Haute-Kotto (Tö-Kötö), Central African Republic",OK
    ODB,////,////,LEBA,UNK,0.519,Cordoba,"Cordoba, Spain",OWNO,////,////,San Jeronimo,"Cordoba, Andalusia, Spain",OK
    ODD,////,////,YOOD,UNK,1.0,Oodnadatta,"Oodnadatta, Australia",OWNO,////,////,????,"Oodnadatta, South Australia, Australia",OK
    ODE,////,////,EKOD,UNK,1.0,Beldringe,"Odense, Denmark",OWNO,////,////,Beldringe,"Odense, Denmark",OK
    ODH,////,////,EGVO,UNK,0.714,RAF Station,"Odiham, United Kingdom",OWNO,////,////,RAF Odiham,"Odiham, Hampshire, England, United Kingdom",OK
    ODJ,////,////,FEGO,UNK,1.0,Ouanda Djalle,"Ouanda Djalle, Central African Republic",OWNO,////,////,Ouanda-Djallé Airport,"Ouanda-Djallé, Vakaga, Central African Republic",OK
    ODL,////,////,YCOD,UNK,1.0,Cordillo Downs,"Cordillo Downs, Australia",OWNO,////,////,????,"Cordillo Downs, South Australia, Australia",OK
    ODN,////,////,WBGI,UNK,1.0,Long Seridan,"Long Seridan, Malaysia",OWNO,////,////,????,"Long Seridan, Sarawak, Malaysia",OK
    ODO,////,////,UIKB,UNK,nan,////,////,////,////,////,Bodaybo Airport,"Bodaybo, Irkutskaya, Russian Federation (Russia)",UNK
    ODR,////,////,YORV,UNK,1.0,Ord River,"Ord River, Australia",OWNO,////,////,????,"Ord River, Western Australia, Australia",OK
    ODS,////,////,UKOO,UNK,0.625,Central,"Odessa, Ukraine",OWNO,////,////,Tsentrainy,"Odessa, Odessa, Ukraine",OK
    ODW,OKH,KOKH,KOKH,OK,0.584,Oak Harbor,"Oak Harbor (WA), USA",OWNO,AJ EISENBERG,"OAK HARBOR, WA - UNITED STATES",Aj Eisenberg Airport,"Oak Harbor, Washington, United States",OK
    ODY,////,////,VLOS,UNK,0.875,Oudomxay,"Oudomxay, Lao PDR",OWNO,////,////,Oudomsay,"Muang Xay, Lao People's Democratic Republic (Laos)",OK
    OEC,////,////,WPOC,UNK,0.923,Ocussi,"Ocussi, Indonesia",OWNO,////,////,????,"Oecussi, Timor-Leste (East Timor)",OK
    OEL,////,////,UUOR,UNK,0.615,Orel,"Orel, Russia",OWNO,////,////,Uzhny Airport,"Oryol, Orlovskaya, Russian Federation (Russia)",OK
    OEM,////,////,SMPA,UNK,1.0,Vincent Fayks,"Paloemeu, Suriname",OWNO,////,////,Vincent Fayks,"Paloemeu, Sipaliwini, Suriname",OK
    OEO,OEO,KOEO,KOEO,OK,0.578,Municipal,"Osceola (WI), USA",OWNO,L O SIMENSTAD MUNI,"OSCEOLA, WI - UNITED STATES",L O Simenstad Municipal Airport,"Osceola, Wisconsin, United States",OK
    OER,////,////,ESNO,UNK,1.0,Ornskoldsvik,"Ornskoldsvik, Sweden",OWNO,////,////,????,"Örnsköldsvik, Västernorrlands län, Sweden",OK
    OES,////,////,SAVN,UNK,0.621,San Antonio Oeste,"San Antonio Oeste, Argentina",OWNO,////,////,Antoine de Saint-Exupéry,"San Antonio Oeste, Río Negro, Argentina",OK
    OFF,OFF,KOFF,KOFF,OK,1.0,////,////,////,OFFUTT AFB,"OMAHA, NE - UNITED STATES",Offutt AFB,"Omaha, Nebraska, United States",OK
    OFI,////,////,DIOF,UNK,1.0,Ouango Fitini,"Ouango Fitini, Cote d'Ivoire",OWNO,////,////,Ouango Fitini Airport,"Ouango Fitini, Zanzan, Côte d'Ivoire (Ivory Coast)",OK
    OFJ,////,////,BIOF,UNK,1.0,Olafsfjordur,"Olafsfjordur, Iceland",OWNO,////,////,????,"Ólafsfjörður, Iceland",OK
    OFK,OFK,KOFK,KOFK,OK,0.672,Stefan Field,"Norfolk (NE), USA",OWNO,NORFOLK RGNL/KARL STEFAN MEMORIAL FLD,"NORFOLK, NE - UNITED STATES",Norfolk Regional/Karl Stefan Memorial Field,"Norfolk, Nebraska, United States",OK
    OFU,Z08,NSAS,NSAS,OK,0.429,Ofu,"Ofu, American Samoa",OWNO,OFU,"OFU VILLAGE, AS - UNITED STATES",????,"Ofu Village, American Samoa",MAYBE
    OGA,OGA,KOGA,KOGA,OK,1.0,Searle Field,"Ogallala (NE), USA",OWNO,SEARLE FIELD,"OGALLALA, NE - UNITED STATES",Searle Field,"Ogallala, Nebraska, United States",OK
    OGB,OGB,KOGB,KOGB,OK,1.0,Municipal,"Orangeburg (SC), USA",OWNO,ORANGEBURG MUNI,"ORANGEBURG, SC - UNITED STATES",Orangeburg Municipal Airport,"Orangeburg, South Carolina, United States",OK
    OGD,OGD,KOGD,KOGD,OK,0.613,Municipal,"Ogden (UT), USA",OWNO,OGDEN-HINCKLEY,"OGDEN, UT - UNITED STATES",Ogden-Hinckley Airport,"Ogden, Utah, United States",OK
    OGE,////,////,////,UNK,1.0,Ogeranang,"Ogeranang, Papua New Guinea",OWNO,////,////,????,"Ogeranang, Morobe, Papua-New Guinea",OK
    OGG,OGG,PHOG,PHOG,OK,1.0,Kahului,"Kahului (HI), USA",OWNO,KAHULUI,"KAHULUI, HI - UNITED STATES",????,"Kahului, Maui, Hawaii, United States",OK
    OGL,////,////,SYGO,UNK,1.0,Ogle,"Ogle, Guyana",OWNO,////,////,????,"Ogle, Demerara-Mahaica, Guyana",OK
    OGM,////,////,////,UNK,nan,////,////,////,////,////,Ustupo Ogobsucum Airport,"Ustupo Island, Panamá",UNK
    OGN,////,////,ROYN,UNK,0.824,Yonaguni Jima,"Yonaguni Jima, Japan",OWNO,////,////,Yonaguni Airport,"Yonaguni, Yonaguni Island, Okinawa, Japan",MAYBE
    OGO,////,////,DIAU,UNK,1.0,Abengourou,"Abengourou, Cote d'Ivoire",OWNO,////,////,????,"Abengourou, Moyen-Comoé, Côte d'Ivoire (Ivory Coast)",OK
    OGR,////,////,FTTB,UNK,1.0,Bongor,"Bongor, Chad",OWNO,////,////,????,"Bongor, Mayo-Kébbi-Est, Chad",OK
    OGS,OGS,KOGS,KOGS,OK,0.781,Ogdensburg,"Ogdensburg (NY), USA",OWNO,OGDENSBURG INTL,"OGDENSBURG, NY - UNITED STATES",Ogdensburg International Airport,"Ogdensburg, New York, United States",OK
    OGU,////,////,LTCB,UNK,nan,////,////,////,////,////,Ordu Giresun Airport,"Ordu/Giresun, Giresun, Turkey",UNK
    OGX,////,////,DAUU,UNK,1.0,Ain Beida,"Ouargla, Algeria",OWNO,////,////,Ain Beida Airport,"Ouargla, Ouargla, Algeria",OK
    OGZ,////,////,URMO,UNK,0.759,Vladikavkaz,"Vladikavkaz, Russia",OWNO,////,////,Beslan Airport,"Vladikavkaz, Severnaya Osetiya-Alaniya, Russian Federation (Russia)",OK
    OHA,////,////,NZOH,UNK,0.364,Royal Air Force Base,"Ohakea, New Zealand",OWNO,////,////,????,"Ohakea, New Zealand",OK
    OHD,////,////,LWOH,UNK,0.333,Ohrid,"Ohrid, Macedonia",OWNO,////,////,Ohrid St. Paul the Apostle Airport,"Ohrid, Macedonia (Republic of)",OK
    OHE,////,////,ZYMH,UNK,1.0,Gulian,Mohe,IATA,////,////,Mohe Gulian Airport,"Mohe, Heilongjiang, China",MAYBE
    OHH,////,////,UHSH,UNK,1.0,Novostroyka,Okha,IATA,////,////,Novostroyka Airport,"Okha, Sakhalinskaya, Russian Federation (Russia)",MAYBE
    OHI,////,////,FYOS,UNK,1.0,Oshakati,"Oshakati, Namibia",OWNO,////,////,????,"Oshakati, Namibia",OK
    OHO,////,////,UHOO,UNK,1.0,Okhotsk,"Okhotsk, Russia",OWNO,////,////,????,"Okhotsk, Khabarovskiy, Russian Federation (Russia)",OK
    OHR,////,////,EDXY,UNK,1.0,Wyk auf Foehr,"Wyk auf Foehr, Germany",OWNO,////,////,????,"Wyk Auf Foehr, Schleswig-Holstein, Germany",OK
    OHS,////,////,OOSH,UNK,nan,////,////,////,////,////,????,"Sohar, Oman",UNK
    OHT,////,////,OPKT,UNK,0.769,Kohat,"Kohat, Pakistan",OWNO,////,////,Kohat AB,"Kohat, Khyber Pakhtunkhwa, Pakistan",OK
    OIA,////,////,SDOW,UNK,0.5,Ourilandia,"Ourilandia, Brazil",OWNO,////,////,Aeropuerto Ourilândia do Norte,"Ourilândia do Norte, Pará, Brazil",MAYBE
    OIC,OIC,KOIC,KOIC,OK,0.664,Eaton,"Norwich (NY), USA",OWNO,LT WARREN EATON,"NORWICH, NY - UNITED STATES",Lt. Warren Eaton Airport,"Norwich, New York, United States",OK
    OIM,////,////,RJTO,UNK,0.632,Oshima,"Oshima, Japan",OWNO,////,////,Oshima Aiport,"Izu Oshima, Tokyo, Japan",MAYBE
    OIR,////,////,RJEO,UNK,1.0,Okushiri,"Okushiri, Japan",OWNO,////,////,????,"Okushiri, Okushiri Island, Hokkaido, Japan",OK
    OIT,////,////,RJFO,UNK,1.0,Oita,"Oita, Japan",OWNO,////,////,Oita Airport,"Oita, Oita, Japan",OK
    OJC,OJC,KOJC,KOJC,OK,0.956,Johnson Executive,"Kansas City (MO), USA",OWNO,JOHNSON COUNTY EXECUTIVE,"OLATHE, KS - UNITED STATES",Johnson County Executive Airport,"Olathe, Kansas, United States",OK
    OKA,////,////,ROAH,UNK,1.0,Naha Airport,"Okinawa, Japan",OWNO,////,////,Naha Airport / Naha AB,"Okinawa, Ryukyu Island, Okinawa, Japan",OK
    OKB,////,////,YORC,UNK,1.0,Fraser Island,"Orchid Beach, Australia",OWNO,////,////,????,"Orchid Beach, Fraser Island, Queensland, Australia",OK
    OKC,OKC,KOKC,KOKC,OK,1.0,Will Rogers World,"Oklahoma City (OK), USA",OWNO,WILL ROGERS WORLD,"OKLAHOMA CITY, OK - UNITED STATES",Will Rogers World Airport,"Oklahoma City, Oklahoma, United States",OK
    OKD,////,////,RJCO,UNK,1.0,Okadama,"Sapporo, Japan",OWNO,////,////,Okadama Airport,"Sapporo, Hokkaido, Japan",OK
    OKE,////,////,RJKB,UNK,0.476,Okino Erabu,"Okino Erabu, Japan",OWNO,////,////,Okinoerabu Airport,"Okinoerabu, Satsunan Islands, Kagoshima, Japan",OK
    OKF,////,////,FYOO,UNK,1.0,Okaukuejo,"Okaukuejo, Namibia",OWNO,////,////,????,"Okaukuejo, Namibia",OK
    OKG,////,////,////,UNK,1.0,Okoyo,"Okoyo, Congo (ROC)",OWNO,////,////,????,"Okoyo, Congo (Republic of)",OK
    OKH,////,////,EGXJ,UNK,1.0,Cottesmor RAF,"Oakham, United Kingdom",OWNO,////,////,RAF Cottesmor,"Oakham, Rutland, England, United Kingdom",OK
    OKI,////,////,RJNO,UNK,0.5,Oki Island,"Oki Island, Japan",OWNO,////,////,Oki Airport,"Okinoshima, Shimane, Japan",MAYBE
    OKJ,////,////,RJOB,UNK,1.0,Okayama,"Okayama, Japan",OWNO,////,////,Okayama Airport,"Okayama, Okayama, Japan",OK
    OKK,OKK,KOKK,KOKK,OK,0.719,Kokomo,"Kokomo (IN), USA",OWNO,KOKOMO MUNI,"KOKOMO, IN - UNITED STATES",Kokomo Municipal Airport,"Kokomo, Indiana, United States",OK
    OKL,////,////,WAJO,UNK,0.483,Oksibil,"Oksibil, Indonesia",OWNO,////,////,Gunung Bintang Airport,"Oksibil, Papua, Indonesia",OK
    OKM,OKM,KOKM,KOKM,OK,0.76,Okmulgee,"Okmulgee (OK), USA",OWNO,OKMULGEE RGNL,"OKMULGEE, OK - UNITED STATES",Okmulgee Regional,"Okmulgee, Oklahoma, United States",OK
    OKN,////,////,FOGQ,UNK,1.0,Okondja,"Okondja, Gabon",OWNO,////,////,????,"Okondja, Haut-Ogooué, Gabon",OK
    OKO,////,////,RJTY,UNK,1.0,Yokota AFB,"Tokyo, Japan",OWNO,////,////,Yokota AFB,"Tokyo, Tokyo, Japan",OK
    OKP,////,////,////,UNK,1.0,Oksapmin,"Oksapmin, Papua New Guinea",OWNO,////,////,????,"Oksapmin, Sandaun, Papua-New Guinea",OK
    OKQ,////,////,WAKO,UNK,1.0,Okaba,"Okaba, Indonesia",OWNO,////,////,????,"Okaba, Papua, Indonesia",OK
    OKR,////,////,YYKI,UNK,0.7,Yorke Island,"Yorke Island, Australia",OWNO,////,////,????,"Masig Island, Queensland, Australia",MAYBE
    OKS,OKS,KOKS,KOKS,OK,0.498,Oshkosh,"Oshkosh (NE), USA",OWNO,GARDEN COUNTY,"OSHKOSH, NE - UNITED STATES",Garden County Airport,"Oshkosh, Nebraska, United States",OK
    OKT,////,////,UWUK,UNK,0.842,Oktiabrskij,"Oktiabrskij, Russia",OWNO,////,////,????,"Oktyabrsky, Bashkortostan, Russian Federation (Russia)",OK
    OKU,////,////,FYMO,UNK,1.0,Mokuti Lodge,"Mokuti Lodge, Namibia",OWNO,////,////,????,"Mokuti Lodge, Namibia",OK

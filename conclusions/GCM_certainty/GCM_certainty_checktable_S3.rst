
List for checking certainty of Great Circle Mapper (SKG - SPE)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    SKG,////,////,LGTS,UNK,1.0,Makedonia Airport,"Thessaloniki, Greece",OWNO,////,////,Makedonia,"Thessaloniki, Kentrikí Makedonía (Central Macedonia), Greece",OK
    SKH,////,////,VNSK,UNK,1.0,Surkhet,"Surkhet, Nepal",OWNO,////,////,????,"Surkhet, Nepal",OK
    SKI,////,////,DABP,UNK,1.0,Skikda,"Skikda, Algeria",OWNO,////,////,Skikda Airport,"Skikda, Skikda, Algeria",OK
    SKK,2C7,PFSH,PFSH,OK,1.0,Shaktoolik,"Shaktoolik (AK), USA",OWNO,SHAKTOOLIK,"SHAKTOOLIK, AK - UNITED STATES",????,"Shaktoolik, Alaska, United States",OK
    SKL,////,////,////,UNK,1.0,Broadford,"Isle Of Skye, United Kingdom",OWNO,////,////,Broadford,"Isle Of Skye, Scotland, United Kingdom",OK
    SKM,////,////,////,UNK,1.0,Skeldon,"Skeldon, Guyana",OWNO,////,////,????,"Skeldon, East Berbice-Corentyne, Guyana",OK
    SKN,////,////,ENSK,UNK,1.0,Skagen,"Stokmarknes, Norway",OWNO,////,////,Skagen,"Stokmarknes, Nordland, Norway",OK
    SKO,////,////,DNSO,UNK,0.37,Sokoto,"Sokoto, Nigeria",OWNO,////,////,Sadiq Abubakar III International Airport,"Sokoto, Sokoto, Nigeria",OK
    SKP,////,////,LWSK,UNK,0.4,Skopje,"Skopje, Macedonia",OWNO,////,////,Skopje Alexander the Great Airport,"Skopje, Macedonia (Republic of)",OK
    SKQ,////,////,FXSK,UNK,1.0,Sekakes,"Sekakes, Lesotho",OWNO,////,////,????,"Sekakes, Lesotho",OK
    SKR,////,////,HASK,UNK,1.0,Shakiso,"Shakiso, Ethiopia",OWNO,////,////,Shakiso Airport,"Shakiso, Oromia, Ethiopia",OK
    SKS,////,////,EKSP,UNK,0.462,Vojens,"Vojens, Denmark",OWNO,////,////,Skrydstrup AB,"Vojens, Denmark",OK
    SKT,////,////,OPST,UNK,0.75,Sialkot,"Sialkot, Pakistan",OWNO,////,////,Sialkot International Airport,"Sialkot, Punjab, Pakistan",OK
    SKU,////,////,LGSY,UNK,1.0,Skiros,"Skiros, Greece",OWNO,////,////,????,"Skiros, Stereá Elláda (Central Greece), Greece",OK
    SKV,////,////,HESC,UNK,0.774,Mount Sinai,"Santa Katarina, Egypt",OWNO,////,////,St. Catherine International Airport,"St. Catherine, Janub Sina (South Sinai), Egypt",MAYBE
    SKW,SKW,PASW,PASW,OK,0.521,Intermediate,"Skwentna (AK), USA",OWNO,SKWENTNA,"SKWENTNA, AK - UNITED STATES",????,"Skwentna, Alaska, United States",OK
    SKX,////,////,UWPS,UNK,1.0,Saransk,"Saransk, Russia",OWNO,////,////,Saransk Airport,"Saransk, Mordoviya, Russian Federation (Russia)",OK
    SKY,SKY,////,KSKY,UNK,1.0,Griffing Sandusky,"Sandusky (OH), USA",OWNO,////,////,Griffing Sandusky Airport,"Sandusky, Ohio, United States",OK
    SKZ,////,////,OPSK,UNK,0.4,Sukkur,"Sukkur, Pakistan",OWNO,////,////,Begum Nusrat Bhutto Airport,"Sukkur, Sindh, Pakistan",OK
    SLA,////,////,SASA,UNK,0.6,Gen Belgrano,"Salta, Argentina",OWNO,////,////,Martín Miguel de Güemes International Airport,"Salta, Salta, Argentina",OK
    SLB,SLB,KSLB,KSLB,OK,1.0,Municipal,"Storm Lake IA, USA",OWNO,STORM LAKE MUNI,"STORM LAKE, IA - UNITED STATES",Storm Lake Municipal Airport,"Storm Lake, Iowa, United States",OK
    SLC,SLC,KSLC,KSLC,OK,1.0,International,"Salt Lake City (UT), USA",OWNO,SALT LAKE CITY INTL,"SALT LAKE CITY, UT - UNITED STATES",Salt Lake City International Airport,"Salt Lake City, Utah, United States",OK
    SLD,////,////,LZSL,UNK,1.0,Sliac,"Sliac, Slovakia",OWNO,////,////,????,"Sliac, Slovakia",OK
    SLE,SLE,KSLE,KSLE,OK,1.0,Mcnary Field,"Salem (OR), USA",OWNO,MCNARY FLD,"SALEM, OR - UNITED STATES",McNary Field,"Salem, Oregon, United States",OK
    SLF,////,////,OESL,UNK,1.0,Sulayel,"Sulayel, Saudi Arabia",OWNO,////,////,????,"Sulayel, Saudi Arabia",OK
    SLG,SLG,KSLG,KSLG,OK,1.0,Smith Field,"Siloam Springs (AR), USA",OWNO,SMITH FIELD,"SILOAM SPRINGS, AR - UNITED STATES",Smith Field,"Siloam Springs, Arkansas, United States",OK
    SLH,////,////,NVSC,UNK,1.0,Sola,"Sola, Vanuatu",OWNO,////,////,????,"Sola, Vanua Lava Island, Banks Islands, Torba, Vanuatu",OK
    SLI,////,////,FLSW,UNK,1.0,Solwezi,"Solwezi, Zambia",OWNO,////,////,????,"Solwezi, Zambia",OK
    SLJ,////,////,YSOL,UNK,1.0,Solomon,Solomon,IATA,////,////,Solomon Airport,"Solomon, Western Australia, Australia",MAYBE
    SLK,SLK,KSLK,KSLK,OK,0.861,Adirondack,"Saranac Lake (NY), USA",OWNO,ADIRONDACK RGNL,"SARANAC LAKE, NY - UNITED STATES",Adirondack Regional,"Saranac Lake, New York, United States",OK
    SLL,////,////,OOSA,UNK,0.737,Salalah,"Salalah, Oman",OWNO,////,////,Salalah International Airport,"Salalah, Oman",OK
    SLM,////,////,LESA,UNK,1.0,Matacan,"Salamanca, Spain",OWNO,////,////,Matacán,"Salamanca, Castille and León, Spain",OK
    SLN,SLN,KSLN,KSLN,OK,0.704,Salina,"Salina (KS), USA",OWNO,SALINA RGNL,"SALINA, KS - UNITED STATES",Salina Regional,"Salina, Kansas, United States",OK
    SLO,SLO,KSLO,KSLO,OK,1.0,Leckrone,"Salem (IL), USA",OWNO,SALEM-LECKRONE,"SALEM, IL - UNITED STATES",Salem-Leckrone Airport,"Salem, Illinois, United States",OK
    SLP,////,////,MMSP,UNK,0.632,San Luis Potosi,"San Luis Potosi, Mexico",OWNO,////,////,Ponciano Arriaga International Airport,"San Luis Potosí, San Luis Potosí, México",OK
    SLQ,SLQ,PASL,PASL,OK,1.0,Sleetmute,"Sleetmute (AK), USA",OWNO,SLEETMUTE,"SLEETMUTE, AK - UNITED STATES",????,"Sleetmute, Alaska, United States",OK
    SLR,SLR,KSLR,KSLR,OK,0.799,Sulphur Springs,"Sulphur Springs (TX), USA",OWNO,SULPHUR SPRINGS MUNI,"SULPHUR SPRINGS, TX - UNITED STATES",Sulphur Springs Municipal Airport,"Sulphur Springs, Texas, United States",OK
    SLS,////,////,LBSS,UNK,0.432,Silistra,"Silistra, Bulgaria",OWNO,////,////,Polkovnik Lambrinovo,"Silistra, Razgrad, Bulgaria",OK
    SLT,ANK,KANK,KANK,OK,0.381,Salida,"Salida (CO), USA",OWNO,HARRIET ALEXANDER FIELD,"SALIDA, CO - UNITED STATES",Harriet Alexander Field,"Salida, Colorado, United States",OK
    SLU,////,////,TLPC,UNK,0.387,Vigie,"Saint Lucia, Saint Lucia",OWNO,////,////,George F.L. Charles,"Castries, St. Lucia Island, Saint Lucia",MAYBE
    SLV,////,////,VISM,UNK,1.0,Simla,"Simla, India",OWNO,////,////,????,"Shimla, Himachal Pradesh, India",OK
    SLW,////,////,MMIO,UNK,0.414,Saltillo,"Saltillo, Mexico",OWNO,////,////,Plan de Guadelupe International Airport,"Saltillo, Coahuila, México",OK
    SLX,////,////,MBSY,UNK,1.0,Salt Cay,"Salt Cay, Turks and Caicos Islands",OWNO,////,////,????,"Salt Cay, Turks and Caicos Islands",OK
    SLY,////,////,USDD,UNK,0.941,Salehard,"Salehard, Russia",OWNO,////,////,Salehkard Airport,"Salekhard, Yamalo-Nenetskiy, Russian Federation (Russia)",TO DO CHECK
    SLZ,////,////,SBSL,UNK,0.925,Mal. Cunha Machado International Airport,"Sao Luis, Brazil",OWNO,////,////,Marechal Cunha Machado International Airport,"São Luis, Maranhão, Brazil",OK
    SMA,////,////,LPAZ,UNK,0.611,Vila Do Porto,"Santa Maria, Portugal",OWNO,////,////,????,"Santa Maria, Região Autónoma dos Açores (Azores), Portugal",OK
    SMB,////,////,SCSB,UNK,0.667,Cerro Sombrero,"Cerro Sombrero, Chile",OWNO,////,////,Franco Bianco Airport,"Cerro Sombrero, Magallanes y de la Antártica Chilena, Chile",OK
    SMC,////,////,////,UNK,1.0,Santa Maria,"Santa Maria, Colombia",OWNO,////,////,????,"Santa Maria, Chocó, Colombia",OK
    SMD,SMD,KSMD,KSMD,OK,1.0,Smith Field,"Fort Wayne (IN), USA",OWNO,SMITH FIELD,"FORT WAYNE, IN - UNITED STATES",Smith Field,"Fort Wayne, Indiana, United States",OK
    SME,SME,KSME,KSME,OK,0.51,Pulaski County,"Somerset (KY), USA",OWNO,LAKE CUMBERLAND RGNL,"SOMERSET, KY - UNITED STATES",Lake Cumberland Regional,"Somerset, Kentucky, United States",OK
    SMF,SMF,KSMF,KSMF,OK,0.679,Metropolitan,"Sacramento (CA), USA",OWNO,SACRAMENTO INTL,"SACRAMENTO, CA - UNITED STATES",Sacramento International Airport,"Sacramento, California, United States",OK
    SMG,////,////,SPMR,UNK,1.0,Santa Maria,"Santa Maria, Peru",OWNO,////,////,????,"Santa María, Lima, Perú",OK
    SMH,////,////,////,UNK,1.0,Sapmanga,"Sapmanga, Papua New Guinea",OWNO,////,////,????,"Sapmanga, Morobe, Papua-New Guinea",OK
    SMI,////,////,LGSM,UNK,1.0,Samos,"Samos, Greece",OWNO,////,////,????,"Samos, Voreío Aigaío (Northern Aegean), Greece",OK
    SMJ,////,////,////,UNK,1.0,Sim,"Sim, Papua New Guinea",OWNO,////,////,????,"Sim, Morobe, Papua-New Guinea",OK
    SMK,SMK,PAMK,PAMK,OK,1.0,St Michael,"St Michael (AK), USA",OWNO,ST MICHAEL,"ST MICHAEL, AK - UNITED STATES",St. Michael Airport,"St. Michael, Alaska, United States",OK
    SML,////,MYLS,MYLS,OK,0.36,Estate Airstrip,"Stella Maris, Bahamas",OWNO,STELLA MARIS,"STELLA MARIS, - BAHAMAS",Estate Airstrip,"Stella Maris, Long Island, Bahamas",OK
    SMM,////,////,WBKA,UNK,1.0,Semporna,"Semporna, Malaysia",OWNO,////,////,????,"Semporna, Sabah, Malaysia",OK
    SMN,SMN,KSMN,KSMN,OK,0.421,Salmon,"Salmon (ID), USA",OWNO,LEMHI COUNTY,"SALMON, ID - UNITED STATES",Lemhi County Airport,"Salmon, Idaho, United States",OK
    SMO,SMO,KSMO,KSMO,OK,0.819,Santa Monica,"Santa Monica (CA), USA",OWNO,SANTA MONICA MUNI,"SANTA MONICA, CA - UNITED STATES",Santa Monica Municipal Airport,"Santa Monica, California, United States",OK
    SMP,////,////,////,UNK,1.0,Stockholm,"Stockholm, Papua New Guinea",OWNO,////,////,????,"Stockholm, East New Britain, Papua-New Guinea",OK
    SMQ,////,////,WAOS,UNK,1.0,Sampit,"Sampit, Indonesia",OWNO,////,////,Sampit Airport,"Sampit, Kalimantan Tengah (Central Borneo), Indonesia",OK
    SMR,////,////,SKSM,UNK,0.917,Simon Bolivar,"Santa Marta, Colombia",OWNO,////,////,Simón Bolívar International Airport,"Santa Marta, Magdalena, Colombia",OK
    SMS,////,////,FMMS,UNK,1.0,Sainte Marie,"Sainte Marie, Madagascar",OWNO,////,////,????,"Sainte Marie, Madagascar",OK
    SMT,////,////,////,UNK,1.0,Sun Moon Lake,"Sun Moon Lake, Taiwan",OWNO,////,////,????,"Sun Moon Lake, Taiwan",OK
    SMU,SMU,PASP,PASP,OK,1.0,Sheep Mountain,"Sheep Mountain (AK), USA",OWNO,SHEEP MOUNTAIN,"SHEEP MOUNTAIN, AK - UNITED STATES",????,"Sheep Mountain, Alaska, United States",OK
    SMV,////,////,LSZS,UNK,1.0,Samedan,"St Moritz, Switzerland",OWNO,////,////,Samedan Airport,"St. Moritz / Samedan, Graubünden, Switzerland",OK
    SMW,////,////,GMMA,UNK,1.0,Smara,"Smara, Morocco",OWNO,////,////,????,"Smara, Western Sahara",OK
    SMX,SMX,KSMX,KSMX,OK,0.893,Public,"Santa Maria (CA), USA",OWNO,SANTA MARIA PUB/CAPT G ALLAN HANCOCK FLD,"SANTA MARIA, CA - UNITED STATES",Santa Maria Pub/Capt G Allan Hancock Field,"Santa Maria, California, United States",OK
    SMY,////,////,GOTS,UNK,1.0,Simenti,"Simenti, Senegal",OWNO,////,////,Simenti Airport,"Simenti, Tambacounda, Senegal",OK
    SMZ,////,////,SMST,UNK,1.0,Stoelmans Eiland,"Stoelmans Eiland, Suriname",OWNO,////,////,????,"Stoelmans Eiland, Sipaliwini, Suriname",OK
    SNA,SNA,KSNA,KSNA,OK,0.706,John Wayne International,"Santa Ana (CA), USA",OWNO,JOHN WAYNE AIRPORT-ORANGE COUNTY,"SANTA ANA, CA - UNITED STATES",John Wayne Airport-Orange County,"Santa Ana, California, United States",OK
    SNB,////,////,YSNB,UNK,1.0,Snake Bay,"Snake Bay, Australia",OWNO,////,////,Snake Bay,"Milikapiti, Melville Island, Northern Territory, Australia",OK
    SNC,////,////,SESA,UNK,0.5,Salinas,"Salinas, Ecuador",OWNO,////,////,General Ulpiano Páez,"Salinas, Santa Elena, Ecuador",OK
    SNE,////,////,GVSN,UNK,1.0,Preguica,"Sao Nicolau, Cape Verde",OWNO,////,////,Preguiça,"Preguiça, São Nicolau, Cape Verde",OK
    SNF,////,////,SVSP,UNK,0.486,San Felipe,"San Felipe, Venezuela",OWNO,////,////,Sub Teniente Nestor Arias,"San Felipe, Yaracuy, Venezuela",OK
    SNG,////,////,SLSI,UNK,0.622,San Ignacio De Velasco,"San Ignacio De Velasco, Bolivia",OWNO,////,////,Capitán Av. Juan Cochamanidis Airport,"San Ignacio de Velasco, Velasco, Santa Cruz, Bolivia",OK
    SNH,////,////,YSPE,UNK,1.0,Stanthorpe,"Stanthorpe, Australia",OWNO,////,////,????,"Stanthorpe, Queensland, Australia",OK
    SNI,////,////,GLGE,UNK,1.0,R.E. Murray,"Sinoe, Liberia",OWNO,////,////,R.E. Murray,"Sinoe, Liberia",OK
    SNJ,////,////,MUSJ,UNK,0.889,San Julian,"San Julian, Cuba",OWNO,////,////,San Julian AB,"Pinar del Río, Pinar del Río, Cuba",MAYBE
    SNK,SNK,KSNK,KSNK,OK,1.0,Winston Field,"Snyder (TX), USA",OWNO,WINSTON FIELD,"SNYDER, TX - UNITED STATES",Winston Field,"Snyder, Texas, United States",OK
    SNL,SNL,KSNL,KSNL,OK,0.581,Municipal,"Shawnee (OK), USA",OWNO,SHAWNEE RGNL,"SHAWNEE, OK - UNITED STATES",Shawnee Regional,"Shawnee, Oklahoma, United States",OK
    SNM,////,////,SLSM,UNK,0.889,San Ignacio De M,"San Ignacio De M, Bolivia",OWNO,////,////,San Ignacio de Moxos Airport,"San Ignacio de Moxos, Moxos, El Beni, Bolivia",MAYBE
    SNN,////,////,EINN,UNK,0.737,Shannon,"Shannon, Ireland",OWNO,////,////,Shannon International Airport,"Shannon, County Clare, Munster, Ireland",OK
    SNO,////,////,VTUI,UNK,1.0,Sakon Nakhon,"Sakon Nakhon, Thailand",OWNO,////,////,????,"Sakon Nakhon, Sakon Nakhon, Thailand",OK
    SNP,SNP,PASN,PASN,OK,0.87,Saint Paul Island,"Saint Paul Island (AK), USA",OWNO,ST PAUL ISLAND,"ST PAUL ISLAND, AK - UNITED STATES",St. Paul Island Airport,"St. Paul Island, Alaska, United States",OK
    SNQ,////,////,////,UNK,1.0,San Quintin,"San Quintin, Mexico",OWNO,////,////,????,"San Quintín, Baja California, México",OK
    SNR,////,////,LFRZ,UNK,1.0,Montoir,"St Nazaire, France",OWNO,////,////,Montoir,"St-Nazaire, Pays de la Loire, France",OK
    SNS,SNS,KSNS,KSNS,OK,0.771,Salinas,"Salinas (CA), USA",OWNO,SALINAS MUNI,"SALINAS, CA - UNITED STATES",Salinas Municipal Airport,"Salinas, California, United States",OK
    SNT,////,////,////,UNK,0.744,Sabana De Torres,"Sabana De Torres, Colombia",OWNO,////,////,Las Cruces,"Sabana de Torres, Santander, Colombia",OK
    SNU,////,////,MUSC,UNK,0.6,Santa Clara,"Santa Clara, Cuba",OWNO,////,////,Abel Santamaria,"Santa Clara, Villa Clara, Cuba",OK
    SNV,////,////,SVSE,UNK,0.696,Santa Elena,"Santa Elena, Venezuela",OWNO,////,////,????,"Santa Elena de Uairen, Bolívar, Venezuela",MAYBE
    SNW,////,////,VYTD,UNK,0.7,Thandwe,"Thandwe, Myanmar",OWNO,////,////,Mazin,"Thandwe, Rakhine, Myanmar (Burma)",OK
    SNY,SNY,KSNY,KSNY,OK,0.752,Sidney,"Sidney (NE), USA",OWNO,SIDNEY MUNI/LLOYD W CARR FIELD,"SIDNEY, NE - UNITED STATES",Sidney Municipal/Lloyd W Carr Field,"Sidney, Nebraska, United States",OK
    SNZ,////,////,SBSC,UNK,0.833,Santa Cruz,"Santa Cruz, Brazil",OWNO,////,////,Santa Cruz AFB,"Rio de Janeiro, Rio de Janeiro, Brazil",MAYBE
    SOB,////,////,LHSM,UNK,0.909,Saarmelleek/Balaton,"Saarmelleek, Hungary",OWNO,////,////,Hévíz-Belaton Airport,"Sármellék, Zala, Hungary",OK
    SOC,////,////,WARQ,UNK,0.81,Adi Sumarmo,"Solo City, Indonesia",OWNO,////,////,Adi Sumarmo Wiryokusumo,"Solo City, Jawa Tengah, Indonesia",OK
    SOD,////,////,SDCO,UNK,1.0,Sorocaba,"Sorocaba, Brazil",OWNO,////,////,????,"Sorocaba, São Paulo, Brazil",OK
    SOE,////,////,FCOS,UNK,1.0,Souanke,"Souanke, Congo (ROC)",OWNO,////,////,Souanké Airport,"Souanké, Congo (Republic of)",OK
    SOF,////,////,LBSF,UNK,0.5,Vrazhdebna,"Sofia, Bulgaria",OWNO,////,////,Sofia International Airport,"Sofia, Sofia-Grad, Bulgaria",OK
    SOG,////,////,ENSG,UNK,1.0,Haukasen,"Sogndal, Norway",OWNO,////,////,Sogndal Airport Haukåsen,"Sogndal, Sogn og Fjordane, Norway",OK
    SOJ,////,////,ENSR,UNK,1.0,Sorkjosen,"Sorkjosen, Norway",OWNO,////,////,Sørkjosen Airport,"Sørkjosen, Troms (Romsa), Norway",OK
    SOK,////,////,FXSM,UNK,0.947,Semongkong,"Semongkong, Lesotho",OWNO,////,////,????,"Semonkong, Lesotho",OK
    SOM,////,////,SVST,UNK,0.64,El Tigre,"San Tome, Venezuela",OWNO,////,////,????,"San Tomé, Anzoátegui, Venezuela",OK
    SON,////,////,NVSS,UNK,0.914,Pekoa,"Espiritu Santo, Vanuatu",OWNO,////,////,Santo-Pekoa International Airport,"Espíritu Santo, Espíritu Santo Island, Sanma, Vanuatu",OK
    SOO,////,////,ESNY,UNK,1.0,Soderhamn,"Soderhamn, Sweden",OWNO,////,////,????,"Söderhamn, Gävleborgs län, Sweden",OK
    SOP,SOP,KSOP,KSOP,OK,0.757,Pinehurst-S. Pines,"Southern Pines (NC), USA",OWNO,MOORE COUNTY,"PINEHURST/SOUTHERN PINES, NC - UNITED STATES",Moore County Airport,"Pinehurst/Southern Pines, North Carolina, United States",OK
    SOQ,////,////,WASS,UNK,0.93,Dominique Edward Osok Airport,"Sorong, West Papua, Indonesia",OWNO,////,////,Domine Eduard Osok Airport,"Sorong, Papua Barat, Indonesia",OK
    SOT,////,////,EFSO,UNK,1.0,Sodankyla,"Sodankyla, Finland",OWNO,////,////,????,"Sodankylä, Lappi (Lappland (Lapland)), Finland",OK
    SOU,////,////,EGHI,UNK,1.0,Eastleigh,"Southampton, United Kingdom",OWNO,////,////,Eastleigh,"Southampton, Hampshire, England, United Kingdom",OK
    SOV,SOV,PASO,PASO,OK,1.0,Seldovia,"Seldovia (AK), USA",OWNO,SELDOVIA,"SELDOVIA, AK - UNITED STATES",????,"Seldovia, Alaska, United States",OK
    SOW,SOW,KSOW,KSOW,OK,0.748,Show Low,"Show Low (AZ), USA",OWNO,SHOW LOW RGNL,"SHOW LOW, AZ - UNITED STATES",Show Low Regional,"Show Low, Arizona, United States",OK
    SOX,////,////,SKSO,UNK,0.438,Sogamoso,"Sogamoso, Colombia",OWNO,////,////,Alberto Lleras Camargo Airport,"Sogamoso, Boyacá, Colombia",OK
    SOY,////,////,EGER,UNK,1.0,Stronsay,"Stronsay, United Kingdom",OWNO,////,////,????,"Stronsay, Orkney Isles, Scotland, United Kingdom",OK
    SOZ,////,////,LFKS,UNK,1.0,Solenzara,"Solenzara, France",OWNO,////,////,Solenzara Airport,"Solenzara, Corse (Corsica), France",OK
    SPA,SPA,KSPA,KSPA,OK,1.0,Downtown Memorial,"Spartanburg (SC), USA",OWNO,SPARTANBURG DOWNTOWN MEMORIAL,"SPARTANBURG, SC - UNITED STATES",Spartanburg Downtown Memorial Airport,"Spartanburg, South Carolina, United States",OK
    SPB,VI22,////,////,OK,0.276,SPB,"St Thomas Island, U.S. Virgin Islands",OWNO,CHARLOTTE AMALIE HARBOR,"CHARLOTTE AMALIE ST THOMAS, VI - UNITED STATES",Charlotte Amalie Harbor SPB,"Charlotte Amalie, St. Thomas, Virgin Islands, United States",MAYBE
    SPC,////,////,GCLA,UNK,1.0,La Palma,"Santa Cruz De La Palma, Canary Islands, Spain",OWNO,////,////,????,"Santa Cruz de La Palma, Canary Islands, Spain",OK
    SPD,////,////,VGSD,UNK,1.0,Saidpur,"Saidpur, Bangladesh",OWNO,////,////,????,"Saidpur, Bangladesh",OK
    SPE,////,////,WBKO,UNK,1.0,Sepulot,"Sepulot, Malaysia",OWNO,////,////,????,"Sepulot, Sabah, Malaysia",OK


List for checking certainty of Great Circle Mapper (KGA - KLZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    KGA,////,////,FZUA,UNK,1.0,Kananga,"Kananga, Congo (DRC)",OWNO,////,////,Kananga Airport,"Kananga, Kasai-Occidental (West Kasai), Democratic Republic of Congo (Zaire)",OK
    KGB,////,////,////,UNK,1.0,Konge,"Konge, Papua New Guinea",OWNO,////,////,????,"Konge, Morobe, Papua-New Guinea",OK
    KGC,////,////,YKSC,UNK,1.0,Kingscote,"Kingscote, Australia",OWNO,////,////,????,"Kingscote, Kangaroo Island, South Australia, Australia",OK
    KGD,////,////,UMKK,UNK,0.733,Kaliningrad,"Kaliningrad, Russia",OWNO,////,////,Khrabrovo Airport,"Kaliningrad, Kaliningradskaya, Russian Federation (Russia)",OK
    KGE,////,////,AGKG,UNK,0.588,Kagau,"Kagau, Solomon Islands",OWNO,////,////,????,"Kagau Island, Solomon Islands",MAYBE
    KGF,////,////,UAKK,UNK,0.643,Karaganda,"Karaganda, Kazakhstan",OWNO,////,////,Sary-Arka Airport,"Karaganda, Qaraghandy, Kazakhstan",OK
    KGG,////,////,GOTK,UNK,1.0,Kedougou,"Kedougou, Senegal",OWNO,////,////,Kédougou Airport,"Kédougou, Kédougou, Senegal",OK
    KGH,////,////,////,UNK,1.0,Yongai,"Yongai, Papua New Guinea",OWNO,////,////,????,"Yongai, Central, Papua-New Guinea",OK
    KGI,////,////,YPKG,UNK,1.0,Kalgoorlie,"Kalgoorlie, Australia",OWNO,////,////,????,"Kalgoorlie, Western Australia, Australia",OK
    KGJ,////,////,FWKA,UNK,1.0,Karonga,"Karonga, Malawi",OWNO,////,////,????,"Karonga, Malawi",OK
    KGK,JZZ,PAJZ,PAJZ,OK,0.87,New Koliganek,"New Koliganek (AK), USA",OWNO,KOLIGANEK,"KOLIGANEK, AK - UNITED STATES",????,"Koliganek, Alaska, United States",OK
    KGL,////,////,HRYR,UNK,0.563,Gregoire Kayibanda,"Kigali, Rwanda",OWNO,////,////,Kanombe,"Kigali, Rwanda",OK
    KGN,////,////,////,UNK,1.0,Kasongo Lunda,"Kasongo Lunda, Congo (DRC)",OWNO,////,////,????,"Kasongo Lunda, Bandundu, Democratic Republic of Congo (Zaire)",OK
    KGO,////,////,UKKG,UNK,1.0,Kirovograd,"Kirovograd, Ukraine",OWNO,////,////,????,"Kirovograd, Kirovohrad, Ukraine",OK
    KGP,////,////,USRK,UNK,1.0,Kogalym International,"Kogalym, Russia",OWNO,////,////,Kogalym International Airport,"Kogalym, Tyumenskaya, Russian Federation (Russia)",OK
    KGR,////,////,////,UNK,1.0,Kulgera,"Kulgera, Australia",OWNO,////,////,????,"Kulgera, Northern Territory, Australia",OK
    KGS,////,////,LGKO,UNK,1.0,Kos,"Kos, Greece",OWNO,////,////,????,"Kos, Kos Island, Notío Aigaío (Southern Aegean), Greece",OK
    KGT,////,////,ZUKD,UNK,nan,////,////,////,////,////,Kangding Airport,"Kangding, Sichuan, China",UNK
    KGU,////,////,WBKG,UNK,1.0,Keningau,"Keningau, Malaysia",OWNO,////,////,????,"Keningau, Sabah, Malaysia",OK
    KGW,////,////,////,UNK,1.0,Kagi,"Kagi, Papua New Guinea",OWNO,////,////,????,"Kagi, Central, Papua-New Guinea",OK
    KGX,KGX,PAGX,PAGX,OK,1.0,Grayling,"Grayling (AK), USA",OWNO,GRAYLING,"GRAYLING, AK - UNITED STATES",????,"Grayling, Alaska, United States",OK
    KGY,////,////,YKRY,UNK,1.0,Kingaroy,"Kingaroy, Australia",OWNO,////,////,????,"Kingaroy, Queensland, Australia",OK
    KGZ,KGZ,////,////,OK,1.0,Glacier Creek,"Glacier Creek (AK), USA",OWNO,GLACIER CREEK,"GLACIER CREEK, AK - UNITED STATES",????,"Glacier Creek, Alaska, United States",OK
    KHA,////,////,OITH,UNK,1.0,Khaneh,"Khaneh, Iran",OWNO,////,////,Khaneh,"Piranshahr, Azarbayjan-e Gharbi, Iran",OK
    KHC,////,////,UKFK,UNK,0.8,Kerch Airport (out of service),"Kerch, Crimea, Ukraine, Russia",OWNO,////,////,Voykovo,"Kerch, Crimea, Ukraine",OK
    KHD,////,////,OICK,UNK,1.0,Khorramabad,"Khorramabad, Iran",OWNO,////,////,????,"Khorramabad, Lorestan, Iran",OK
    KHE,////,////,UKOH,UNK,0.522,Kherson,"Kherson, Ukraine",OWNO,////,////,Chernobayevka,"Kherson, Kherson, Ukraine",OK
    KHG,////,////,ZWSH,UNK,0.917,Kashi,"Kashi, PR China",OWNO,////,////,Kashgar Airport,"Kashgar, Xinjiang, China",OK
    KHH,////,////,RCKH,UNK,1.0,International,"Kaohsiung, Taiwan",OWNO,////,////,Kaohsiung International Airport,"Kaohsiung, Taiwan",OK
    KHI,////,////,OPKC,UNK,1.0,Jinnah International Airport (Quaid-E-Azam),"Karachi, Pakistan",OWNO,////,////,Quaid-e-Azam / Jinnah International Airport,"Karachi, Sindh, Pakistan",OK
    KHJ,////,////,EFKJ,UNK,1.0,Kauhajoki,"Kauhajoki, Finland",OWNO,////,////,????,"Kauhajoki, Etelä-Pohjanmaa (Södra Österbotten (Southern Ostrobothnia)), Finland",OK
    KHK,////,////,OIBQ,UNK,1.0,Khark Island,"Khark Island, Iran",OWNO,////,////,????,"Khark Island, Bushehr, Iran",OK
    KHM,////,////,VYKI,UNK,1.0,Khamti,"Khamti, Myanmar",OWNO,////,////,????,"Khamti, Sagaing, Myanmar (Burma)",OK
    KHN,////,////,ZSCN,UNK,0.759,Nanchang,"Nanchang, PR China",OWNO,////,////,Nanchang Changbei International Airport,"Nanchang, Jiangxi, China",OK
    KHO,////,////,////,UNK,1.0,Khoka Moya,"Khoka Moya, South Africa",OWNO,////,////,????,"Khoka Moya, Limpopo, South Africa",OK
    KHR,////,////,ZMHH,UNK,1.0,Kharkhorin,"Kharkhorin, Mongolia",OWNO,////,////,????,"Kharkhorin, Mongolia",OK
    KHS,////,////,OOKB,UNK,1.0,Khasab,"Khasab, Oman",OWNO,////,////,????,"Khasab, Oman",OK
    KHT,////,////,OAKS,UNK,1.0,Khost,"Khost, Afghanistan",OWNO,////,////,????,"Khost, Khost, Afghanistan",OK
    KHU,////,////,UKHK,UNK,0.571,Kremenchug,"Kremenchug, Ukraine",OWNO,////,////,Velyka Kohnovka,"Kremenchuk, Poltava, Ukraine",OK
    KHV,////,////,UHHH,UNK,0.968,Novyy,"Khabarovsk, Russia",OWNO,////,////,Novy Airport,"Khabarovsk, Khabarovskiy, Russian Federation (Russia)",OK
    KHW,////,////,FBKR,UNK,1.0,Khwai River Lodge,"Khwai River Lodge, Botswana",OWNO,////,////,Khwai River Airport,"Khwai River Lodge, North-West, Botswana",OK
    KHX,////,////,////,UNK,1.0,Kihihi,Kihihi,IATA,////,////,Kihihi Airport,"Kihihi, Uganda",MAYBE
    KHY,////,////,OITK,UNK,nan,////,////,////,////,////,????,"Khoy, Azarbayjan-e Gharbi, Iran",UNK
    KHZ,////,////,NTKA,UNK,1.0,Kauehi,"Kauehi, French Polynesia",OWNO,////,////,????,"Kauehi, French Polynesia",OK
    KIA,////,////,////,UNK,1.0,Kaiapit,"Kaiapit, Papua New Guinea",OWNO,////,////,????,"Kaiapit, Morobe, Papua-New Guinea",OK
    KIB,KIB,////,////,OK,0.815,SPB,"Ivanof Bay (AK), USA",OWNO,IVANOF BAY,"IVANOF BAY, AK - UNITED STATES",Ivanof Bay SPB,"Ivanof Bay, Alaska, United States",OK
    KIC,KIC,KKIC,KKIC,OK,1.0,Mesa Del Rey,"King City (CA), USA",OWNO,MESA DEL REY,"KING CITY, CA - UNITED STATES",Mesa Del Rey Airport,"King City, California, United States",OK
    KID,////,////,ESMK,UNK,1.0,Kristianstad,"Kristianstad, Sweden",OWNO,////,////,????,"Kristianstad, Skåne län, Sweden",OK
    KIE,////,////,AYKT,UNK,1.0,Aropa,"Kieta, Papua New Guinea",OWNO,////,////,Aropa,"Kieta, Bougainville Island, Bougainville, Papua-New Guinea",OK
    KIF,////,////,////,UNK,1.0,Kingfisher Lake,"Kingfisher Lake, Canada",OWNO,////,////,????,"Kingfisher Lake, Ontario, Canada",OK
    KIG,////,////,////,UNK,1.0,Koinghaas,"Koinghaas, South Africa",OWNO,////,////,????,"Koinghaas, Northern Cape, South Africa",OK
    KIH,////,////,OIBK,UNK,1.0,Kish Island,"Kish Island, Iran",OWNO,////,////,????,"Kish Island, Hormozgan, Iran",OK
    KIJ,////,////,RJSN,UNK,1.0,Niigata,"Niigata, Japan",OWNO,////,////,Niigata Airport,"Niigata, Niigata, Japan",OK
    KIK,////,////,ORKK,UNK,1.0,Kirkuk,"Kirkuk, Iraq",OWNO,////,////,????,"Kirkuk, Iraq",OK
    KIL,////,////,////,UNK,1.0,Kilwa,"Kilwa, Congo (DRC)",OWNO,////,////,????,"Kilwa, Katanga, Democratic Republic of Congo (Zaire)",OK
    KIM,////,////,FAKM,UNK,1.0,Kimberley,"Kimberley, South Africa",OWNO,////,////,Kimberley Airport,"Kimberley, Northern Cape, South Africa",OK
    KIN,////,////,MKJP,UNK,1.0,Norman Manley International,"Kingston, Jamaica",OWNO,////,////,Norman Manley International Airport,"Kingston, Jamaica",OK
    KIO,Q51,////,////,OK,1.0,Kili Island,"Kili Island, Marshall Islands",OWNO,KILI,"KILI ISLAND, - MARSHALL ISLANDS",Kili Airport,"Kili Island, Marshall Islands",OK
    KIP,CWC,KCWC,KCWC,OK,0.757,Kickapoo,"Wichita Falls (TX), USA",OWNO,KICKAPOO DOWNTOWN,"WICHITA FALLS, TX - UNITED STATES",Kickapoo Downtown Airport,"Wichita Falls, Texas, United States",OK
    KIQ,////,////,////,UNK,1.0,Kira,"Kira, Papua New Guinea",OWNO,////,////,????,"Kira, Northern, Papua-New Guinea",OK
    KIR,////,////,EIKY,UNK,1.0,Kerry County Airport,"Kerry County, Ireland",OWNO,////,////,Kerry,"Killarney, County Kerry, Munster, Ireland",MAYBE
    KIS,////,////,HKKI,UNK,1.0,Kisumu,"Kisumu, Kenya",OWNO,////,////,????,"Kisumu, Kenya",OK
    KIT,////,////,LGKC,UNK,1.0,Kithira,"Kithira, Greece",OWNO,////,////,????,"Kithira, Attikí (Attica), Greece",OK
    KIU,////,////,////,UNK,1.0,Kiunga,"Kiunga, Kenya",OWNO,////,////,????,"Kiunga, Kenya",OK
    KIV,////,////,LUKK,UNK,0.762,Chisinau,"Chisinau, Moldova",OWNO,////,////,Chisinau International Airport,"Chisinau, Moldova",OK
    KIW,////,////,FLSO,UNK,1.0,Southdowns,"Kitwe, Zambia",OWNO,////,////,Southdowns,"Kitwe, Copperbelt, Zambia",OK
    KIX,////,////,RJBB,UNK,1.0,Kansai International,"Osaka, Japan",OWNO,////,////,Kansai International Airport,"Osaka, Osaka, Japan",OK
    KIY,////,////,HTKI,UNK,0.588,Kilwa,"Kilwa, Tanzania",OWNO,////,////,Kilwa Masoko Airport,"Kilwa Masoko, Lindi, Tanzania",MAYBE
    KIZ,////,////,////,UNK,1.0,Kikinonda,"Kikinonda, Papua New Guinea",OWNO,////,////,????,"Kikinonda, Northern, Papua-New Guinea",OK
    KJA,////,////,UNKL,UNK,0.545,Krasnojarsk,"Krasnojarsk, Russia",OWNO,////,////,Yemelyanovo International Airport,"Krasnoyarsk, Krasnoyarskiy, Russian Federation (Russia)",TO DO CHECK
    KJH,////,////,ZUKJ,UNK,0.476,Kai Li,Huang Ping,IATA,////,////,Kaili Huangping Airport,"Kaili, Guizhou, China",MAYBE
    KJI,////,////,ZWKN,UNK,nan,////,////,////,////,////,Kanas Airport,"Burqin County, Xinjiang, China",UNK
    KJK,////,////,EBKT,UNK,0.571,Kortrijk,"Kortrijk, Belgium",OWNO,////,////,Kortrijk-Wevelgem International Airport,"Kortrijk, Vlaams-Brabant (Flemish Brabant), Belgium",OK
    KJP,////,////,ROKR,UNK,1.0,Kerama,"Kerama, Japan",OWNO,////,////,Kerama Airport,"Zamami, Kerama Islands, Okinawa, Japan",OK
    KJU,////,////,////,UNK,1.0,Kamiraba,"Kamiraba, Papua New Guinea",OWNO,////,////,????,"Kamiraba, New Ireland, Papua-New Guinea",OK
    KKA,KKA,PAKK,PAKK,OK,0.445,Koyuk,"Koyuk (AK), USA",OWNO,KOYUK ALFRED ADAMS,"KOYUK, AK - UNITED STATES",Koyuk Alfred Adams Airport,"Koyuk, Alaska, United States",OK
    KKB,KKB,////,////,OK,0.79,SPB,"Kitoi Bay (AK), USA",OWNO,KITOI BAY,"KITOI BAY, AK - UNITED STATES",Kitoi Bay SPB,"Kitoi Bay, Alaska, United States",OK
    KKC,////,////,VTUK,UNK,1.0,Khon Kaen,"Khon Kaen, Thailand",OWNO,////,////,????,"Khon Kaen, Khon Kaen, Thailand",OK
    KKD,////,////,////,UNK,1.0,Kokoda,"Kokoda, Papua New Guinea",OWNO,////,////,????,"Kokoda, Northern, Papua-New Guinea",OK
    KKE,////,////,NZKK,UNK,1.0,Kerikeri,"Kerikeri, New Zealand",OWNO,////,////,????,"Kerikeri/Bay of Islands, New Zealand",OK
    KKG,////,////,////,UNK,1.0,Konawaruk,"Konawaruk, Guyana",OWNO,////,////,????,"Konawaruk, Potaro-Siparuni, Guyana",OK
    KKH,DUY,PADY,PADY,OK,1.0,Kongiganak,"Kongiganak (AK), USA",OWNO,KONGIGANAK,"KONGIGANAK, AK - UNITED STATES",????,"Kongiganak, Alaska, United States",OK
    KKI,KKI,////,////,OK,0.778,Spb,"Akiachak (AK), USA",OWNO,AKIACHAK,"AKIACHAK, AK - UNITED STATES",Akiachak SPB,"Akiachak, Alaska, United States",OK
    KKJ,////,////,RJFR,UNK,1.0,Kitakyushu Airport,"Fukuoka, Kitakyushu, Japan",OWNO,////,////,Kitakyushu Airport,"Kitakyushu, Fukuoka, Japan",OK
    KKK,1KC,////,////,OK,0.492,Kalakaket AFS,"Kalakaket (AK), USA",OWNO,KALAKAKET CREEK AS,"KALAKAKET CREEK, AK - UNITED STATES",Kalakaket Creek Air Station,"Kalakaket Creek, Alaska, United States",OK
    KKL,KKL,////,////,OK,0.81,Karluk Lake SPB,"Karluk Lake (AK), USA",OWNO,KARLUK LAKE,"KARLUK LAKE, AK - UNITED STATES",Karluk Lake SPB,"Karluk Lake, Alaska, United States",OK
    KKM,////,////,VTBH,UNK,0.636,Lop Buri,"Lop Buri, Thailand",OWNO,////,////,Sa Pran Nak,"Lop Buri, Lop Buri, Thailand",OK
    KKN,////,////,ENKR,UNK,0.976,Hoeybuktmoen,"Kirkenes, Norway",OWNO,////,////,Høybuktmoen,"Kirkenes, Norway",OK
    KKO,////,////,NZKO,UNK,1.0,Kaikohe,"Kaikohe, New Zealand",OWNO,////,////,????,"Kaikohe, New Zealand",OK
    KKP,////,////,YKLB,UNK,1.0,Koolburra,"Koolburra, Australia",OWNO,////,////,????,"Koolburra, Queensland, Australia",OK
    KKQ,////,////,USDP,UNK,nan,////,////,////,////,////,Krasnosel'kup Airport,"Krasnosel'kup, Yamalo-Nenetskiy, Russian Federation (Russia)",UNK
    KKR,////,////,NTGK,UNK,1.0,Kaukura Atoll,"Kaukura Atoll, French Polynesia",OWNO,////,////,????,"Kaukura Atoll, French Polynesia",OK
    KKT,50I,////,////,OK,0.752,Kentland,"Kentland (IN), USA",OWNO,KENTLAND MUNI,"KENTLAND, IN - UNITED STATES",Kentland Municipal Airport,"Kentland, Indiana, United States",OK
    KKU,KKU,////,////,OK,1.0,Ekuk,"Ekuk (AK), USA",OWNO,EKUK,"EKUK, AK - UNITED STATES",????,"Ekuk, Alaska, United States",OK
    KKW,////,////,FZCA,UNK,1.0,Kikwit,"Kikwit, Congo (DRC)",OWNO,////,////,Kikwit Airport,"Kikwit, Bandundu, Democratic Republic of Congo (Zaire)",OK
    KKX,////,////,RJKI,UNK,0.7,Kikaiga Shima,"Kikaiga Shima, Japan",OWNO,////,////,Kikai Airport,"Kikai, Satsunan Islands, Kagoshima, Japan",TO DO CHECK
    KKY,////,////,EIKL,UNK,1.0,Kilkenny,"Kilkenny, Ireland",OWNO,////,////,????,"Kilkenny, County Kilkenny, Leinster, Ireland",OK
    KKZ,////,////,VDKK,UNK,1.0,Koh Kong,"Koh Kong, Cambodia",OWNO,////,////,????,"Koh Kong, Cambodia",OK
    KLB,////,////,FLKL,UNK,1.0,Kalabo,"Kalabo, Zambia",OWNO,////,////,????,"Kalabo, Zambia",OK
    KLC,////,////,GOOK,UNK,1.0,Kaolack,"Kaolack, Senegal",OWNO,////,////,Kaolack Airport,"Kaolack, Kaolack, Senegal",OK
    KLD,////,////,UUEM,UNK,1.0,Migalovo,"Kalinin, Russia",OWNO,////,////,Migalovo,"Tver, Tverskaya, Russian Federation (Russia)",OK
    KLE,////,////,FKKH,UNK,1.0,Kaele,"Kaele, Cameroon",OWNO,////,////,Kaélé Airport,"Kaélé, Far North (Extrême-Nord), Cameroon",OK
    KLF,////,////,UUBC,UNK,0.545,Kaluga,"Kaluga, Russia",OWNO,////,////,Grabtsevo Airport,"Kaluga, Kaluzhskaya, Russian Federation (Russia)",OK
    KLG,KLG,PALG,PALG,OK,0.805,Municipal,"Kalskag (AK), USA",OWNO,KALSKAG,"KALSKAG, AK - UNITED STATES",????,"Kalskag, Alaska, United States",OK
    KLH,////,////,VAKP,UNK,1.0,Kolhapur,"Kolhapur, India",OWNO,////,////,????,"Kolhapur, Maharashtra, India",OK
    KLI,////,////,FZFP,UNK,0.471,Kota Koli,"Kota Koli, Congo (DRC)",OWNO,////,////,Kotakoli Airport,"Kotakoli, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    KLK,////,////,HKFG,UNK,0.519,Kalokol,"Kalokol, Kenya",OWNO,////,////,Fergusons Gulf Airport,"Kalokol, Kenya",OK
    KLL,9Z8,////,////,OK,1.0,Levelock,"Levelock (AK), USA",OWNO,LEVELOCK,"LEVELOCK, AK - UNITED STATES",????,"Levelock, Alaska, United States",OK
    KLM,////,////,OINE,UNK,nan,////,////,////,////,////,????,"Kalaleh, Mazandaran, Iran",UNK
    KLN,2A3,PALB,PALB,OK,0.815,Larsen SPB,"Larsen Bay (AK), USA",OWNO,LARSEN BAY,"LARSEN BAY, AK - UNITED STATES",????,"Larsen Bay, Alaska, United States",OK
    KLO,////,////,RPVK,UNK,0.714,Kalibo,"Kalibo, Philippines",OWNO,////,////,Kalibo International Airport,"Kalibo, Philippines",OK
    KLQ,////,////,WIPV,UNK,1.0,Keluang,"Keluang, Indonesia",OWNO,////,////,????,"Keluang, Sumatera Selatan, Indonesia",OK
    KLR,////,////,ESMQ,UNK,1.0,Kalmar,"Kalmar, Sweden",OWNO,////,////,????,"Kalmar, Kalmar län, Sweden",OK
    KLS,KLS,KKLS,KKLS,OK,0.514,Longview,"Kelso (WA), USA",OWNO,SOUTHWEST WASHINGTON RGNL,"KELSO, WA - UNITED STATES",Southwest Washington Regional,"Kelso, Washington, United States",OK
    KLU,////,////,LOXK,UNK,0.541,Klagenfurt,"Klagenfurt, Austria",OWNO,////,////,Woerthersee International Airport,"Klagenfurt, Kärnten (Carinthia), Austria",OK
    KLV,////,////,LKKV,UNK,1.0,Karlovy Vary,"Karlovy Vary, Czech Republic",OWNO,////,////,Karlovy Vary Airport,"Karlovy Vary, Karlovy Vary, Czech Republic",OK
    KLW,AKW,PAKW,PAKW,OK,1.0,Klawock,"Klawock (AK), USA",OWNO,KLAWOCK,"KLAWOCK, AK - UNITED STATES",????,"Klawock, Alaska, United States",OK
    KLX,////,////,LGKL,UNK,1.0,Kalamata,"Kalamata, Greece",OWNO,////,////,????,"Kalamata, Peloponnísos (Peloponnese), Greece",OK
    KLY,////,////,FZOD,UNK,0.615,Kalima,"Kalima, Congo (DRC)",OWNO,////,////,Kinkungwa Airport,"Kalima, Maniema, Democratic Republic of Congo (Zaire)",OK
    KLZ,////,////,FAKZ,UNK,1.0,Kleinzee,"Kleinzee, South Africa",OWNO,////,////,Kleinzee Airport,"Kleinzee, Northern Cape, South Africa",OK

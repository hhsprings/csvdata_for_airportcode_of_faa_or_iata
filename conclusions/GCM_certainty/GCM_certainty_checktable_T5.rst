
List for checking certainty of Great Circle Mapper (TTI - TZX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    TTI,////,////,NTTE,UNK,0.842,Tetiaroa Is,"Tetiaroa Is, French Polynesia",OWNO,////,////,????,"Tetiaroa, Society Islands, French Polynesia",OK
    TTJ,////,////,RJOR,UNK,1.0,Tottori,"Tottori, Japan",OWNO,////,////,Tottori Airport,"Tottori, Tottori, Japan",OK
    TTK,////,////,////,UNK,1.0,Tottenham Hale Station,"Tottenham Hale Station, United Kingdom",OWNO,////,////,Tottenham Hale Station,"London, England, United Kingdom",OK
    TTL,////,////,NFUL,UNK,0.421,Turtle Island,"Turtle Island, Fiji",OWNO,////,////,????,"Nanuya Levu, Fiji",TO DO CHECK
    TTM,////,////,////,UNK,1.0,Tablon De Tamara,"Tablon De Tamara, Colombia",OWNO,////,////,????,"Tablon de Tamara, Casanare, Colombia",OK
    TTN,TTN,KTTN,KTTN,OK,1.0,Trenton-Mercer,"Trenton/Philadelphia (PA), USA",OWNO,TRENTON MERCER,"TRENTON, NJ - UNITED STATES",Trenton Mercer Airport,"Trenton, New Jersey, United States",OK
    TTO,BTN,KBTN,KBTN,OK,1.0,Municipal,"Britton (SD), USA",OWNO,BRITTON MUNI,"BRITTON, SD - UNITED STATES",Britton Municipal Airport,"Britton, South Dakota, United States",OK
    TTQ,////,////,MRBT,UNK,1.0,Tortuquero,"Tortuquero, Costa Rica",OWNO,////,////,????,"Tortuquero, Limón, Costa Rica",OK
    TTR,////,////,WAWT,UNK,0.71,Tana Toraja,"Tana Toraja, Indonesia",OWNO,////,////,Pongtiku,"Tana Toraja, Sulawesi Selatan, Indonesia",OK
    TTS,////,////,FMNT,UNK,1.0,Tsaratanana,"Tsaratanana, Madagascar",OWNO,////,////,Tsaratanana Airport,"Tsaratanana, Madagascar",OK
    TTT,////,////,RCFN,UNK,0.636,Taitung,"Taitung, Taiwan",OWNO,////,////,Fengnin,"Taitung, Taiwan",OK
    TTU,////,////,GMTN,UNK,1.0,Sania Ramel,"Tetuan, Morocco",OWNO,////,////,Sania Ramel,"Tetuan, Morocco",OK
    TTX,////,////,YTST,UNK,nan,////,////,////,////,////,Truscott-Mungalalu Airport,"Truscott-Mungalalu, Western Australia, Australia",UNK
    TUA,////,////,SETU,UNK,0.385,Tulcan,"Tulcan, Ecuador",OWNO,////,////,El Rosal - Teniente Mantilla,"Tulcán, Carchi, Ecuador",OK
    TUB,////,////,NTAT,UNK,1.0,Tubuai,"Tubuai, French Polynesia",OWNO,////,////,????,"Tubuai, Tubuai Islands, Austral Islands, French Polynesia",OK
    TUC,////,////,SANT,UNK,0.778,Benj Matienzo International,"Tucuman (Tucumán), Argentina",OWNO,////,////,Teniente Benjamin Matienzo International Airport,"Tucumán, Tucumán, Argentina",MAYBE
    TUD,////,////,GOTT,UNK,1.0,Tambacounda,"Tambacounda, Senegal",OWNO,////,////,Tambacounda Airport,"Tambacounda, Tambacounda, Senegal",OK
    TUF,////,////,LFOT,UNK,0.541,St Symphorien,"Tours, France",OWNO,////,////,Val de Loire,"Tours, Centre, France",OK
    TUG,////,////,RPUT,UNK,1.0,Tuguegarao,"Tuguegarao, Philippines",OWNO,////,////,????,"Tuguegarao, Philippines",OK
    TUI,////,////,OETR,UNK,1.0,Turaif,"Turaif, Saudi Arabia",OWNO,////,////,Turaif Airport,"Turaif, Saudi Arabia",OK
    TUJ,////,////,HAMJ,UNK,1.0,Tum,"Tum, Ethiopia",OWNO,////,////,Tum Airport,"Maji, SNNPR, Ethiopia",OK
    TUK,////,////,OPTU,UNK,0.75,Turbat,"Turbat, Pakistan",OWNO,////,////,Turbat International Airport,"Turbat, Balochistan, Pakistan",OK
    TUL,TUL,KTUL,KTUL,OK,1.0,International,"Tulsa (OK), USA",OWNO,TULSA INTL,"TULSA, OK - UNITED STATES",Tulsa International Airport,"Tulsa, Oklahoma, United States",OK
    TUM,////,////,YTMU,UNK,1.0,Tumut,"Tumut, Australia",OWNO,////,////,????,"Tumut, New South Wales, Australia",OK
    TUN,////,////,DTTA,UNK,0.87,Carthage,"Tunis, Tunisia",OWNO,////,////,Carthage International Airport,"Tunis, Tunisia",OK
    TUO,////,////,NZAP,UNK,1.0,Taupo,"Taupo, New Zealand",OWNO,////,////,????,"Taupo, New Zealand",OK
    TUP,TUP,KTUP,KTUP,OK,0.57,Lemons Municipal,"Tupelo (MS), USA",OWNO,TUPELO RGNL,"TUPELO, MS - UNITED STATES",Tupelo Regional,"Tupelo, Mississippi, United States",OK
    TUQ,////,////,DFOT,UNK,1.0,Tougan,"Tougan, Burkina Faso",OWNO,////,////,????,"Tougan, Burkina Faso",OK
    TUR,////,////,SBTU,UNK,1.0,Tucurui,"Tucurui, Brazil",OWNO,////,////,????,"Tucuruí, Pará, Brazil",OK
    TUS,TUS,KTUS,KTUS,OK,1.0,International,"Tucson (AZ), USA",OWNO,TUCSON INTL,"TUCSON, AZ - UNITED STATES",Tucson International Airport,"Tucson, Arizona, United States",OK
    TUT,////,////,////,UNK,1.0,Tauta,"Tauta, Papua New Guinea",OWNO,////,////,????,"Tauta, Madang, Papua-New Guinea",OK
    TUU,////,////,OETB,UNK,0.294,Tabuk,"Tabuk, Saudi Arabia",OWNO,////,////,Prince Sultan bin Abdulaziz Airport,"Tabuk, Saudi Arabia",OK
    TUV,////,////,SVTC,UNK,1.0,Tucupita,"Tucupita, Venezuela",OWNO,////,////,????,"Tucupita, Delta Amacuro, Venezuela",OK
    TUW,////,////,////,UNK,1.0,Tubala,"Tubala, Panama",OWNO,////,////,????,"Tubala, Kuna Yala (Guna Yala), Panamá",OK
    TUX,////,////,////,UNK,1.0,Tumbler Ridge,"Tumbler Ridge, Canada",OWNO,////,////,????,"Tumbler Ridge, British Columbia, Canada",OK
    TUY,////,////,////,UNK,0.769,Tulum,"Tulum, Mexico",OWNO,////,////,NAS Tulum,"Tulum, Quintana Roo, México",OK
    TUZ,////,////,////,UNK,1.0,Tucuma,"Tucuma, Brazil",OWNO,////,////,????,"Tucumã, Pará, Brazil",OK
    TVA,////,////,FMMR,UNK,1.0,Morafenobe,"Morafenobe, Madagascar",OWNO,////,////,????,"Morafenobe, Madagascar",OK
    TVC,TVC,KTVC,KTVC,OK,0.587,Traverse City,"Traverse City (MI), USA",OWNO,CHERRY CAPITAL,"TRAVERSE CITY, MI - UNITED STATES",Cherry Capital Airport,"Traverse City, Michigan, United States",OK
    TVF,TVF,KTVF,KTVF,OK,1.0,Regional,"Thief River Falls (MN), USA",OWNO,THIEF RIVER FALLS RGNL,"THIEF RIVER FALLS, MN - UNITED STATES",Thief River Falls Regional,"Thief River Falls, Minnesota, United States",OK
    TVI,TVI,KTVI,KTVI,OK,0.766,Municipal,"Thomasville (GA), USA",OWNO,THOMASVILLE RGNL,"THOMASVILLE, GA - UNITED STATES",Thomasville Regional,"Thomasville, Georgia, United States",OK
    TVL,TVL,KTVL,KTVL,OK,1.0,South Lake Tahoe,"South Lake Tahoe (CA), USA",OWNO,LAKE TAHOE,"SOUTH LAKE TAHOE, CA - UNITED STATES",Lake Tahoe Airport,"South Lake Tahoe, California, United States",OK
    TVS,////,////,ZBTS,UNK,nan,////,////,////,////,////,Tangshan Sannühe Airport,"Tangshan, Hubei, China",UNK
    TVU,////,////,NFNM,UNK,1.0,Matei,"Taveuni, Fiji",OWNO,////,////,????,"Matei, Taveuni, Fiji",OK
    TVY,////,////,VYDW,UNK,1.0,Dawe,"Dawe, Myanmar",OWNO,////,////,????,"Dawe, Taninthayi, Myanmar (Burma)",OK
    TWA,A63,////,////,OK,1.0,Twin Hills,"Twin Hills (AK), USA",OWNO,TWIN HILLS,"TWIN HILLS, AK - UNITED STATES",????,"Twin Hills, Alaska, United States",OK
    TWB,////,////,YTWB,UNK,1.0,Toowoomba,"Toowoomba, Australia",OWNO,////,////,????,"Toowoomba, Queensland, Australia",OK
    TWD,0S9,////,////,OK,0.529,Port Townsend,"Port Townsend (WA), USA",OWNO,JEFFERSON COUNTY INTL,"PORT TOWNSEND, WA - UNITED STATES",Jefferson County International Airport,"Port Townsend, Washington, United States",OK
    TWE,AK49,////,////,OK,1.0,Taylor,"Taylor (AK), USA",OWNO,TAYLOR,"TAYLOR, AK - UNITED STATES",????,"Taylor, Alaska, United States",OK
    TWF,TWF,KTWF,KTWF,OK,0.194,City County,"Twin Falls (ID), USA",OWNO,JOSLIN FIELD - MAGIC VALLEY RGNL,"TWIN FALLS, ID - UNITED STATES",Joslin Field - Magic Valley Regional,"Twin Falls, Idaho, United States",OK
    TWN,////,////,////,UNK,1.0,Tewantin,"Tewantin, Australia",OWNO,////,////,????,"Tewantin, Queensland, Australia",OK
    TWP,////,////,////,UNK,1.0,Torwood,"Torwood, Australia",OWNO,////,////,????,"Torwood, Queensland, Australia",OK
    TWT,////,////,////,UNK,1.0,Tawitawi,"Tawitawi, Philippines",OWNO,////,////,????,"Tawitawi, Philippines",OK
    TWU,////,////,WBKW,UNK,1.0,Tawau,"Tawau, Malaysia",OWNO,////,////,????,"Tawau, Sabah, Malaysia",OK
    TWY,////,////,////,UNK,1.0,Tawa,"Tawa, Papua New Guinea",OWNO,////,////,????,"Tawa, Morobe, Papua-New Guinea",OK
    TWZ,////,////,NZUK,UNK,1.0,Pukaki/Twizel,"Mount Cook, New Zealand",OWNO,////,////,Pukaki,"Twizel, New Zealand",OK
    TXF,////,////,SNTF,UNK,1.0,Teixeira de Freitas,"Teixeira de Freitas, Brazil",OWNO,////,////,????,"Teixeira de Freitas, Bahia, Brazil",OK
    TXG,////,////,RCLG,UNK,1.0,Taichung,"Taichung, Taiwan",OWNO,////,////,????,"Taichung, Taiwan",OK
    TXK,TXK,KTXK,KTXK,OK,0.579,Municipal,"Texarkana (AR), USA",OWNO,TEXARKANA RGNL-WEBB FIELD,"TEXARKANA, AR - UNITED STATES",Texarkana Regional-Webb Field,"Texarkana, Arkansas, United States",OK
    TXL,////,////,EDDT,UNK,1.0,Tegel,"Berlin, Germany",OWNO,////,////,Berlin Tegel Airport,"Berlin, Berlin, Germany",OK
    TXM,////,////,WAST,UNK,1.0,Teminabuan,"Teminabuan, Indonesia",OWNO,////,////,????,"Teminabuan, Papua, Indonesia",OK
    TXN,////,////,ZSTX,UNK,0.894,Tunxi,"Tunxi, PR China",OWNO,////,////,Tunxi International Airport,"Huangshan, Anhui, China",OK
    TXR,////,////,////,UNK,1.0,Tanbar,"Tanbar, Australia",OWNO,////,////,????,"Tanbar, Queensland, Australia",OK
    TXU,////,////,DITB,UNK,1.0,Tabou,"Tabou, Cote d'Ivoire",OWNO,////,////,????,"Tabou, Bas-Sassandra, Côte d'Ivoire (Ivory Coast)",OK
    TYA,////,////,UUBT,UNK,0.5,Tula,"Tula, Russia",OWNO,////,////,Klokovo,"Tula, Tul'skaya, Russian Federation (Russia)",OK
    TYB,////,////,YTIB,UNK,1.0,Tibooburra,"Tibooburra, Australia",OWNO,////,////,????,"Tibooburra, New South Wales, Australia",OK
    TYD,////,////,UHBW,UNK,1.0,Tynda,"Tynda, Russia",OWNO,////,////,Tynda Airport,"Tynda, Amurskaya, Russian Federation (Russia)",OK
    TYE,TYE,////,////,OK,1.0,Tyonek,"Tyonek (AK), USA",OWNO,TYONEK,"TYONEK, AK - UNITED STATES",????,"Tyonek, Alaska, United States",OK
    TYF,////,////,ESST,UNK,0.571,Torsby Airport,"Torsby, Sweden",OWNO,////,////,Fryklanda,"Torsby, Värmlands län, Sweden",OK
    TYG,////,////,YTHY,UNK,1.0,Thylungra,"Thylungra, Australia",OWNO,////,////,????,"Thylungra, Queensland, Australia",OK
    TYL,////,////,SPYL,UNK,0.316,Talara,"Talara, Peru",OWNO,////,////,Capitán FAP Victor Montes Airport,"Talara, Piura, Perú",OK
    TYM,////,MYES,MYES,OK,1.0,Staniel Cay,"Staniel Cay, Bahamas",OWNO,STANIEL CAY,"STANIEL CAY, - BAHAMAS",????,"Staniel Cay, Exuma Islands, Bahamas",OK
    TYN,////,////,ZBYN,UNK,0.75,Taiyuan,"Taiyuan, PR China",OWNO,////,////,Taiyuan Wusu International Airport,"Taiyuan, Shaanxi, China",OK
    TYO,////,////,RJTD,UNK,1.0,Metropolitan Area,"Tokyo, Japan",OWNO,////,////,Metropolitan Area,"Tokyo, Tokyo, Japan",OK
    TYP,////,////,YTMY,UNK,1.0,Tobermorey,"Tobermorey, Australia",OWNO,////,////,????,"Tobermorey, Northern Territory, Australia",OK
    TYR,TYR,KTYR,KTYR,OK,0.623,Pounds Field,"Tyler (TX), USA",OWNO,TYLER POUNDS RGNL,"TYLER, TX - UNITED STATES",Tyler Pounds Regional,"Tyler, Texas, United States",OK
    TYS,TYS,KTYS,KTYS,OK,1.0,Mc Ghee Tyson,"Knoxville (TN), USA",OWNO,MC GHEE TYSON,"KNOXVILLE, TN - UNITED STATES",Mc Ghee Tyson Airport,"Knoxville, Tennessee, United States",OK
    TYT,////,////,SUTR,UNK,1.0,Treinta-y-Tres,"Treinta-y-Tres, Uruguay",OWNO,////,////,Treinta y Tres Airport,"Treinta y Tres, Treinta y Tres, Uruguay",OK
    TYZ,TYL,KTYL,KTYL,OK,1.0,Taylor,"Taylor (AZ), USA",OWNO,TAYLOR,"TAYLOR, AZ - UNITED STATES",????,"Taylor, Arizona, United States",OK
    TZA,////,////,////,UNK,1.0,Municipal,"Belize City, Belize",OWNO,////,////,Belize City Municipal Airport,"Belize City, Belize, Belize",OK
    TZL,////,////,LQTZ,UNK,1.0,Tuzla International,"Tuzla, Bosnia and Herzegovina",OWNO,////,////,Tuzla International Airport,"Tuzla, Bosnia and Herzegovina",OK
    TZM,////,////,////,UNK,1.0,Tizimin,"Tizimin, Mexico",OWNO,////,////,????,"Tizimin, Yucatán, México",OK
    TZX,////,////,LTCG,UNK,0.875,Trabzon,"Trabzon, Turkey",OWNO,////,////,Trabzon AB,"Trabzon, Trabzon, Turkey",OK


List for checking certainty of Great Circle Mapper (FAA - FNI)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    FAA,////,////,GUFH,UNK,1.0,Faranah,"Faranah, Guinea",OWNO,////,////,????,"Faranah, Guinea",OK
    FAB,////,////,EGLF,UNK,1.0,Farnborough,"Farnborough, United Kingdom",OWNO,////,////,????,"Farnborough, Hampshire, England, United Kingdom",OK
    FAC,////,////,NTKF,UNK,1.0,Faaite,"Faaite, French Polynesia",OWNO,////,////,????,"Faaite, French Polynesia",OK
    FAE,////,////,EKVG,UNK,1.0,Vagar,"Faroe Islands, Faroe Islands",OWNO,////,////,Vágar,"Sorvag, Faroe Islands",OK
    FAF,FAF,KFAF,KFAF,OK,1.0,Felker AAF,"Fort Eustis (VA), USA",OWNO,FELKER AAF,"FORT EUSTIS, VA - UNITED STATES",Felker AAF Airport,"Fort Eustis, Virginia, United States",OK
    FAG,////,////,BIFM,UNK,1.0,Fagurholsmyri,"Fagurholsmyri, Iceland",OWNO,////,////,????,"Fagurhólsmýri, Iceland",OK
    FAH,////,////,OAFR,UNK,1.0,Farah,"Farah, Afghanistan",OWNO,////,////,????,"Farah, Farah, Afghanistan",OK
    FAI,FAI,PAFA,PAFA,OK,1.0,International,"Fairbanks (AK), USA",OWNO,FAIRBANKS INTL,"FAIRBANKS, AK - UNITED STATES",Fairbanks International Airport,"Fairbanks, Alaska, United States",OK
    FAJ,X95,////,TJFA,UNK,0.444,Fajardo,"Fajardo, Puerto Rico",OWNO,////,////,Diego Jiménez Torres Airport,"Fajardo, Puerto Rico, United States",OK
    FAK,2Z6,////,////,OK,0.819,False Island,"False Island (AK), USA",OWNO,FALSE ISLAND,"FALSE ISLAND, AK - UNITED STATES",False Island SPB,"False Island, Alaska, United States",OK
    FAM,FAM,KFAM,KFAM,OK,1.0,Regional,"Farmington (MO), USA",OWNO,FARMINGTON RGNL,"FARMINGTON, MO - UNITED STATES",Farmington Regional,"Farmington, Missouri, United States",OK
    FAN,////,////,ENLI,UNK,1.0,Lista,"Farsund, Norway",OWNO,////,////,Lista,"Farsund, Norway",OK
    FAO,////,////,LPFR,UNK,0.229,Aeroporto Internacional de Faro,"Faro, Portugal",OWNO,////,////,????,"Faro, Faro, Portugal",OK
    FAQ,////,////,AYFR,UNK,1.0,Freida River,"Freida River, Papua New Guinea",OWNO,////,////,????,"Frieda River, Sandaun, Papua-New Guinea",OK
    FAR,FAR,KFAR,KFAR,OK,0.672,Hector Field,"Fargo (ND), USA",OWNO,HECTOR INTL,"FARGO, ND - UNITED STATES",Hector International Airport,"Fargo, North Dakota, United States",OK
    FAS,////,////,BIFF,UNK,1.0,Faskrudsfjordur,"Faskrudsfjordur, Iceland",OWNO,////,////,????,"Fáskrúðsfjörður, Iceland",OK
    FAT,FAT,KFAT,KFAT,OK,0.34,Airterminal,"Fresno (CA), USA",OWNO,FRESNO YOSEMITE INTL,"FRESNO, CA - UNITED STATES",Fresno Yosemite International Airport,"Fresno, California, United States",OK
    FAV,////,////,NTGF,UNK,1.0,Fakarava,"Fakarava, French Polynesia",OWNO,////,////,????,"Fakarava, Society Islands, French Polynesia",OK
    FAY,FAY,KFAY,KFAY,OK,0.769,Municipal,"Fayetteville (NC), USA",OWNO,FAYETTEVILLE RGNL/GRANNIS FIELD,"FAYETTEVILLE, NC - UNITED STATES",Fayetteville Regional/Grannis Field,"Fayetteville, North Carolina, United States",OK
    FAZ,////,////,OISF,UNK,nan,////,////,////,////,////,????,"Fasa, Fars, Iran",UNK
    FBA,////,////,SWOB,UNK,nan,////,////,////,////,////,????,"Fonte Boa, Amazonas, Brazil",UNK
    FBD,////,////,OAFZ,UNK,1.0,Faizabad,"Faizabad, Afghanistan",OWNO,////,////,????,"Faizabad, Badakhshan, Afghanistan",OK
    FBE,////,////,SSFB,UNK,1.0,Francisco Beltrao,"Francisco Beltrao, Brazil",OWNO,////,////,????,"Francisco Beltrao, Paraná, Brazil",OK
    FBK,FBK,PAFB,PAFB,OK,0.814,Ft Wainwright,"Fairbanks (AK), USA",OWNO,LADD AAF,"FAIRBANKS/FT WAINWRIGHT, AK - UNITED STATES",Ladd AAF Airport,"Fairbanks/Ft Wainwright, Alaska, United States",OK
    FBL,FBL,KFBL,KFBL,OK,1.0,Municipal,"Faribault (MN), USA",OWNO,FARIBAULT MUNI,"FARIBAULT, MN - UNITED STATES",Faribault Municipal Airport,"Faribault, Minnesota, United States",OK
    FBM,////,////,FZQA,UNK,0.783,Luano,"Lubumbashi, Congo (DRC)",OWNO,////,////,Lubumbashi International Airport,"Lubumbashi, Katanga, Democratic Republic of Congo (Zaire)",OK
    FBR,FBR,KFBR,KFBR,OK,1.0,Fort Bridger,"Fort Bridger (WY), USA",OWNO,FORT BRIDGER,"FORT BRIDGER, WY - UNITED STATES",????,"Fort Bridger, Wyoming, United States",OK
    FBS,W33,////,////,OK,0.859,Friday Harbor SPB,"Friday Harbor SPB (WA), USA",OWNO,FRIDAY HARBOR,"FRIDAY HARBOR, WA - UNITED STATES",Friday Harbor SPB,"Friday Harbor, Washington, United States",OK
    FBU,////,////,////,UNK,nan,////,////,////,////,////,Fornebu Seaplane Base,"Oslo, Norway",UNK
    FBY,FBY,KFBY,KFBY,OK,1.0,Municipal,"Fairbury (NE), USA",OWNO,FAIRBURY MUNI,"FAIRBURY, NE - UNITED STATES",Fairbury Municipal Airport,"Fairbury, Nebraska, United States",OK
    FCA,GPI,KGPI,KGPI,OK,0.79,Glacier National Park,"Kalispell (MT), USA",OWNO,GLACIER PARK INTL,"KALISPELL, MT - UNITED STATES",Glacier Park International Airport,"Kalispell, Montana, United States",OK
    FCB,////,////,FAFB,UNK,1.0,Ficksburg Sentra Oes,"Ficksburg, South Africa",OWNO,////,////,Ficksburg Sentra Oes,"Ficksburg, Free State, South Africa",OK
    FCH,FCH,KFCH,KFCH,OK,0.704,Fresno-Chandler,"Fresno (CA), USA",OWNO,FRESNO CHANDLER EXECUTIVE,"FRESNO, CA - UNITED STATES",Fresno Chandler Executive Airport,"Fresno, California, United States",OK
    FCM,FCM,KFCM,KFCM,OK,1.0,Flying Cloud Airport,"Minneapolis (MN), USA",OWNO,FLYING CLOUD,"MINNEAPOLIS, MN - UNITED STATES",Flying Cloud Airport,"Minneapolis, Minnesota, United States",OK
    FCO,////,////,LIRF,UNK,0.615,Fiumicino,"Rome, Italy",OWNO,////,////,Leonardo da Vinci International Airport,"Rome, Lazio, Italy",OK
    FCY,FCY,KFCY,KFCY,OK,1.0,Municipal,"Forrest City (AR), USA",OWNO,FORREST CITY MUNI,"FORREST CITY, AR - UNITED STATES",Forrest City Municipal Airport,"Forrest City, Arkansas, United States",OK
    FDE,////,////,ENBL,UNK,1.0,Bringeland,"Forde, Norway",OWNO,////,////,Førde,"Bringeland, Norway",OK
    FDF,////,////,TFFF,UNK,0.615,Lamentin,"Fort De France, Martinique",OWNO,////,////,Martinique Aimé Césaire International Airport,"Fort de France, Martinique",OK
    FDH,////,////,EDNY,UNK,1.0,Friedrichshafen Airport (Bodensee Airport),"Friedrichshafen, Germany",OWNO,////,////,Bodensee Airport Friedrichshafen,"Friedrichshafen, Baden-Württemberg, Germany",OK
    FDK,FDK,KFDK,KFDK,OK,1.0,Municipal,"Frederick (MD), USA",OWNO,FREDERICK MUNI,"FREDERICK, MD - UNITED STATES",Frederick Municipal Airport,"Frederick, Maryland, United States",OK
    FDR,FDR,KFDR,KFDR,OK,0.618,Municipal,"Frederick (OK), USA",OWNO,FREDERICK RGNL,"FREDERICK, OK - UNITED STATES",Frederick Regional,"Frederick, Oklahoma, United States",OK
    FDU,////,////,FZBO,UNK,1.0,Bandundu,"Bandundu, Congo (DRC)",OWNO,////,////,Bandundu Airport,"Bandundu, Bandundu, Democratic Republic of Congo (Zaire)",OK
    FDY,FDY,KFDY,KFDY,OK,1.0,Findlay,"Findlay (OH), USA",OWNO,FINDLAY,"FINDLAY, OH - UNITED STATES",????,"Findlay, Ohio, United States",OK
    FEA,////,////,////,UNK,1.0,Fetlar,"Fetlar, United Kingdom",OWNO,////,////,????,"Fetlar, Shetland Islands, Scotland, United Kingdom",OK
    FEB,////,////,VNSR,UNK,1.0,Sanfebagar,"Sanfebagar, Nepal",OWNO,////,////,????,"Sanfebagar, Nepal",OK
    FEC,////,////,SNJD,UNK,0.615,Feira De Santana,"Feira De Santana, Brazil",OWNO,////,////,João Durval Carneiro,"Feira de Santana, Bahia, Brazil",OK
    FEG,////,////,UTKF,UNK,0.75,Fergana,"Fergana, Uzbekistan",OWNO,////,////,Fergana International Airport,"Fergana, Farg'ona, Uzbekistan",OK
    FEJ,////,////,SNOU,UNK,0.357,Feijo,"Feijo, Brazil",OWNO,////,////,Novo Aeródromo de Feijó,"Feijó, Acre, Brazil",OK
    FEK,////,////,DIFK,UNK,1.0,Ferkessedougou,"Ferkessedougou, Cote d'Ivoire",OWNO,////,////,????,"Ferkessédougou, Savanes, Côte d'Ivoire (Ivory Coast)",OK
    FEL,////,////,ETSF,UNK,0.938,Fuerstenfeldbruck,"Fürstenfeldbruck, Germany",OWNO,////,////,Fürstenfeldbruck AB,"Fürstenfeldbruck, Bavaria, Germany",OK
    FEN,////,////,SBFN,UNK,1.0,Fernando De Noronha,"Fernando De Noronha, Brazil",OWNO,////,////,????,"Fernando de Noronha, FN, Pernambuco, Brazil",OK
    FEP,FEP,KFEP,KFEP,OK,1.0,Albertus,"Freeport (IL), USA",OWNO,ALBERTUS,"FREEPORT, IL - UNITED STATES",Albertus Airport,"Freeport, Illinois, United States",OK
    FET,FET,KFET,KFET,OK,1.0,Municipal,"Fremont (NE), USA",OWNO,FREMONT MUNI,"FREMONT, NE - UNITED STATES",Fremont Municipal Airport,"Fremont, Nebraska, United States",OK
    FEW,FEW,KFEW,KFEW,OK,0.557,Warren AFB,"Cheyenne (WY), USA",OWNO,FRANCIS E WARREN AFB,"CHEYENNE, WY - UNITED STATES",Francis E Warren AFB Heliport,"Cheyenne, Wyoming, United States",OK
    FEZ,////,////,GMFF,UNK,1.0,Sais,"Fez, Morocco",OWNO,////,////,Saïss,"Fez, Morocco",OK
    FFA,FFA,KFFA,KFFA,OK,1.0,First Flight,"Kill Devil Hills (NC), USA",OWNO,FIRST FLIGHT,"KILL DEVIL HILLS, NC - UNITED STATES",First Flight Airport,"Kill Devil Hills, North Carolina, United States",OK
    FFD,////,////,EGVA,UNK,0.769,RAF Station,"Fairford, United Kingdom",OWNO,////,////,RAF Fairford,"Fairford, Gloucestershire, England, United Kingdom",OK
    FFL,FFL,KFFL,KFFL,OK,1.0,Municipal,"Fairfield IA, USA",OWNO,FAIRFIELD MUNI,"FAIRFIELD, IA - UNITED STATES",Fairfield Municipal Airport,"Fairfield, Iowa, United States",OK
    FFM,FFM,KFFM,KFFM,OK,0.397,Fergus Falls,"Fergus Falls (MN), USA",OWNO,FERGUS FALLS MUNI-EINAR MICKELSON FLD,"FERGUS FALLS, MN - UNITED STATES",Fergus Falls Municipal-Einar Mickelson Field,"Fergus Falls, Minnesota, United States",OK
    FFO,FFO,KFFO,KFFO,OK,0.801,Patterson AFB,"Dayton (OH), USA",OWNO,WRIGHT-PATTERSON AFB,"DAYTON, OH - UNITED STATES",Wright-Patterson AFB,"Dayton, Ohio, United States",OK
    FFT,FFT,KFFT,KFFT,OK,1.0,Capital City,"Frankfort (KY), USA",OWNO,CAPITAL CITY,"FRANKFORT, KY - UNITED STATES",Capital City Airport,"Frankfort, Kentucky, United States",OK
    FFU,////,////,SCFT,UNK,1.0,Futaleufu,"Futaleufu, Chile",OWNO,////,////,Futaleufú Airport,"Futaleufú, Los Lagos, Chile",OK
    FGD,////,////,GQPF,UNK,1.0,Fderik,"Fderik, Mauritania",OWNO,////,////,????,"Fderik, Mauritania",OK
    FGI,////,////,NSFI,UNK,1.0,Fagali I,"Apia, Samoa",OWNO,////,////,Fagali'i,"Apia, Samoa (Western Samoa)",OK
    FGL,////,////,NZFH,UNK,0.75,Fox Glacier,"Fox Glacier, New Zealand",OWNO,////,////,Heliport,"Fox Glacier, New Zealand",OK
    FGU,////,////,NTGB,UNK,1.0,Fangatau,"Fangatau, French Polynesia",OWNO,////,////,????,"Fangatau, French Polynesia",OK
    FHU,FHU,KFHU,KFHU,OK,0.752,Fort Huachu Municipal Airport,"Sierra Vista (AZ), USA",OWNO,SIERRA VISTA MUNI-LIBBY AAF,"FORT HUACHUCA SIERRA VISTA, AZ - UNITED STATES",Sierra Vista Municipal-Libby AAF,"Fort Huachuca/Sierra Vista, Arizona, United States",OK
    FHZ,////,////,NTKH,UNK,1.0,Fakahina,"Fakahina, French Polynesia",OWNO,////,////,????,"Fakahina, French Polynesia",OK
    FID,0B8,////,////,OK,1.0,Elizabeth Field,"Fishers Island (NY), USA",OWNO,ELIZABETH FIELD,"FISHERS ISLAND, NY - UNITED STATES",Elizabeth Field,"Fishers Island, New York, United States",OK
    FIE,////,////,EGEF,UNK,1.0,Fair Isle,"Fair Isle, United Kingdom",OWNO,////,////,????,"Fair Isle, Shetland Islands, Scotland, United Kingdom",OK
    FIG,////,////,GUFA,UNK,1.0,Fria,"Fria, Guinea",OWNO,////,////,????,"Fria, Guinea",OK
    FIH,////,////,FZAA,UNK,0.865,N'Djili,"Kinshasa, Congo (DRC)",OWNO,////,////,N'Djili International Airport,"Kinshasa, Kinshasa, Democratic Republic of Congo (Zaire)",OK
    FIK,////,////,YFNE,UNK,1.0,Finke,"Finke, Australia",OWNO,////,////,????,"Finke, Northern Territory, Australia",OK
    FIL,FOM,KFOM,KFOM,OK,1.0,Municipal,"Fillmore (UT), USA",OWNO,FILLMORE MUNI,"FILLMORE, UT - UNITED STATES",Fillmore Municipal Airport,"Fillmore, Utah, United States",OK
    FIN,////,////,AYFI,UNK,1.0,Finschhafen,"Finschhafen, Papua New Guinea",OWNO,////,////,????,"Finschhafen, Morobe, Papua-New Guinea",OK
    FIZ,////,////,YFTZ,UNK,1.0,Fitzroy Crossing,"Fitzroy Crossing, Australia",OWNO,////,////,????,"Fitzroy Crossing, Western Australia, Australia",OK
    FJR,////,////,OMFJ,UNK,1.0,Fujairah International Airport,"Al-Fujairah, United Arab Emirates",OWNO,////,////,Fujairah International Airport,"Al-Fujairah, Fujairah, United Arab Emirates",OK
    FKB,////,////,EDSB,UNK,0.652,Soellingen,"Karlsruhe/Baden Baden, Germany",OWNO,////,////,Baden Airpark,"Karlsruhe/Baden-Baden, Baden-Württemberg, Germany",OK
    FKI,////,////,FZIC,UNK,0.615,Kisangani,"Kisangani, Congo (DRC)",OWNO,////,////,Bangoka International Airport,"Kisangani, Orientale, Democratic Republic of Congo (Zaire)",OK
    FKJ,////,////,RJNF,UNK,1.0,Fukui,"Fukui, Japan",OWNO,////,////,Fukui Airport,"Fukui, Fukui, Japan",OK
    FKL,FKL,KFKL,KFKL,OK,0.468,Chess-Lambertin,"Franklin (PA), USA",OWNO,VENANGO RGNL,"FRANKLIN, PA - UNITED STATES",Venango Regional,"Franklin, Pennsylvania, United States",OK
    FKN,FKN,KFKN,KFKN,OK,0.504,Municipal,"Franklin (VA), USA",OWNO,FRANKLIN MUNI-JOHN BEVERLY ROSE,"FRANKLIN, VA - UNITED STATES",Franklin Municipal-John Beverly Rose,"Franklin, Virginia, United States",OK
    FKQ,////,////,WASF,UNK,1.0,Fak Fak,"Fak Fak, Indonesia",OWNO,////,////,????,"Fak Fak, Irian Jaya, Papua, Indonesia",OK
    FKS,////,////,RJSF,UNK,1.0,Fukushima Airport,"Fukushima, Japan",OWNO,////,////,Fukushima Airport,"Fukushima, Fukushima, Japan",OK
    FLA,////,////,SKFL,UNK,0.5,Capitolio,"Florencia, Colombia",OWNO,////,////,Gustavo Artunduaga Paredes Airport,"Florencia, Caquetá, Colombia",OK
    FLB,////,////,SNQG,UNK,1.0,Cangapara,"Floriano, Brazil",OWNO,////,////,Cangapara,"Floriano, Piauí, Brazil",OK
    FLD,FLD,KFLD,KFLD,OK,0.771,Fond Du Lac,"Fond Du Lac (WI), USA",OWNO,FOND DU LAC COUNTY,"FOND DU LAC, WI - UNITED STATES",Fond Du Lac County Airport,"Fond Du Lac, Wisconsin, United States",OK
    FLF,////,////,EDXF,UNK,1.0,Schaferhaus,"Flensburg, Germany",OWNO,////,////,Schaferhaus,"Flensburg, Schleswig-Holstein, Germany",OK
    FLG,FLG,KFLG,KFLG,OK,0.613,Pulliam Field,"Grand Canyon (AZ), USA",OWNO,FLAGSTAFF PULLIAM,"FLAGSTAFF, AZ - UNITED STATES",Flagstaff Pulliam Airport,"Flagstaff, Arizona, United States",OK
    FLH,////,////,////,UNK,0.727,Flotta,"Flotta, United Kingdom",OWNO,////,////,????,"Flotta Isle, Orkney Isles, Scotland, United Kingdom",MAYBE
    FLI,////,////,////,UNK,1.0,Flateyri,"Flateyri, Iceland",OWNO,////,////,????,"Flateyri, Iceland",OK
    FLL,FLL,KFLL,KFLL,OK,0.859,International,"Fort Lauderdale (FL), USA",OWNO,FORT LAUDERDALE/HOLLYWOOD INTL,"FORT LAUDERDALE, FL - UNITED STATES",Fort Lauderdale/Hollywood International Airport,"Fort Lauderdale, Florida, United States",OK
    FLM,////,////,SGFI,UNK,1.0,Filadelfia,"Filadelfia, Paraguay",OWNO,////,////,????,"Filadelfia, Boquerón, Paraguay",OK
    FLN,////,////,SBFL,UNK,1.0,Hercílio Luz International,"Florianopolis, Brazil",OWNO,////,////,Hercílio Luz International Airport,"Florianópolis, Santa Catarina, Brazil",OK
    FLO,FLO,KFLO,KFLO,OK,0.826,Florence,"Florence (SC), USA",OWNO,FLORENCE RGNL,"FLORENCE, SC - UNITED STATES",Florence Regional,"Florence, South Carolina, United States",OK
    FLP,FLP,KFLP,KFLP,OK,0.466,Flippin,"Flippin (AR), USA",OWNO,MARION COUNTY RGNL,"FLIPPIN, AR - UNITED STATES",Marion County Regional,"Flippin, Arkansas, United States",OK
    FLR,////,////,LIRQ,UNK,1.0,Peretola,"Florence, Italy",OWNO,////,////,Peretola,"Florence, Tuscany, Italy",OK
    FLS,////,////,YFLI,UNK,1.0,Flinders Island,"Flinders Island, Australia",OWNO,////,////,????,"Flinders Island, Tasmania, Australia",OK
    FLT,FLT,////,////,OK,1.0,Flat,"Flat (AK), USA",OWNO,FLAT,"FLAT, AK - UNITED STATES",????,"Flat, Alaska, United States",OK
    FLV,FLV,KFLV,KFLV,OK,1.0,Sherman AAF,"Fort Leavenworth (KS), USA",OWNO,SHERMAN AAF,"FORT LEAVENWORTH, KS - UNITED STATES",Sherman AAF Airport,"Fort Leavenworth, Kansas, United States",OK
    FLW,////,////,LPFL,UNK,1.0,Santa Cruz,"Flores Island, Portugal",OWNO,////,////,Santa Cruz,"Flores Island, Região Autónoma dos Açores (Azores), Portugal",OK
    FLX,FLX,KFLX,KFLX,OK,1.0,Municipal,"Fallon (NV), USA",OWNO,FALLON MUNI,"FALLON, NV - UNITED STATES",Fallon Municipal Airport,"Fallon, Nevada, United States",OK
    FLY,////,////,YFIL,UNK,1.0,Finley,"Finley, Australia",OWNO,////,////,????,"Finley, New South Wales, Australia",OK
    FLZ,////,////,WIMS,UNK,nan,////,////,////,////,////,Dr. Ferdinand Lumban Tobing Airport,"Sibolga, Sumatera Utara, Indonesia",UNK
    FMA,////,////,SARF,UNK,1.0,El Pucu,"Formosa, Argentina",OWNO,////,////,El Pucu,"Formosa, Formosa, Argentina",OK
    FME,FME,KFME,KFME,OK,0.863,Tipton AAF,"Fort Meade (MD), USA",OWNO,TIPTON,"FORT MEADE(ODENTON), MD - UNITED STATES",Tipton Airport,"Fort Meade, Maryland, United States",OK
    FMH,FMH,KFMH,KFMH,OK,0.43,Otis AFB,"Falmouth (MA), USA",OWNO,CAPE COD COAST GUARD AIR STATION,"FALMOUTH, MA - UNITED STATES",Cape Cod Coast Guard Air Station,"Falmouth, Massachusetts, United States",OK
    FMI,////,////,FZRF,UNK,1.0,Kalemie,"Kalemie, Congo (DRC)",OWNO,////,////,Kalemie Airport,"Kalemie, Katanga, Democratic Republic of Congo (Zaire)",OK
    FMM,////,////,EDJA,UNK,0.72,Memmingen Airport,"Memmingen, Germany",OWNO,////,////,Allgäu,"Memmingen, Bavaria, Germany",OK
    FMN,FMN,KFMN,KFMN,OK,0.471,Municipal,"Farmington (NM), USA",OWNO,FOUR CORNERS RGNL,"FARMINGTON, NM - UNITED STATES",Four Corners Regional,"Farmington, New Mexico, United States",OK
    FMO,////,////,EDDG,UNK,1.0,Münster Osnabrück International Airport,"Muenster/Osnabrueck, Germany",OWNO,////,////,Münster/Osnabrück International Airport,"Münster/Osnabrück, North Rhine-Westphalia, Germany",OK
    FMS,FSW,KFSW,KFSW,OK,1.0,Municipal,"Fort Madison IA, USA",OWNO,FORT MADISON MUNI,"FORT MADISON, IA - UNITED STATES",Fort Madison Municipal Airport,"Fort Madison, Iowa, United States",OK
    FMY,FMY,KFMY,KFMY,OK,1.0,Page Field,"Fort Myers (FL), USA",OWNO,PAGE FIELD,"FORT MYERS, FL - UNITED STATES",Page Field,"Fort Myers, Florida, United States",OK
    FNA,////,////,GFLL,UNK,1.0,Lungi International,"Freetown, Sierra Leone",OWNO,////,////,Lungi International Airport,"Freetown, Sierra Leone",OK
    FNB,////,////,EDBN,UNK,1.0,Neubrandenburg Airport,"Neubrandenburg, Germany",OWNO,////,////,Neubrandenburg Airport,"Neubrandenburg, Mecklenburg-Vorpommern, Germany",OK
    FNC,////,////,LPMA,UNK,1.0,Funchal,"Funchal, Portugal",OWNO,////,////,????,"Funchal, Região Autónoma da Madeira (Madeira), Portugal",OK
    FNE,////,////,////,UNK,1.0,Fane,"Fane, Papua New Guinea",OWNO,////,////,????,"Fane, Central, Papua-New Guinea",OK
    FNG,////,////,DFEF,UNK,1.0,Fada Ngourma,"Fada Ngourma, Burkina Faso",OWNO,////,////,????,"Fada N'gourma, Burkina Faso",OK
    FNH,////,////,HAFN,UNK,1.0,Fincha,"Fincha, Ethiopia",OWNO,////,////,Fincha Airport,"Fincha, Oromia, Ethiopia",OK
    FNI,////,////,LFTW,UNK,0.857,Garons,"Nimes, France",OWNO,////,////,NAS Nîmes-Garons,"Nîmes, Languedoc-Roussillon, France",OK

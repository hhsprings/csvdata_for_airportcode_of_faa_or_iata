
List for checking certainty of Great Circle Mapper (TOF - TTH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    TOF,////,////,UNTT,UNK,0.556,Tomsk,"Tomsk, Russia",OWNO,////,////,Bogashevo Airport,"Tomsk, Tomskaya, Russian Federation (Russia)",OK
    TOG,TOG,PATG,PATG,OK,1.0,Togiak Village,"Togiak Village (AK), USA",OWNO,TOGIAK,"TOGIAK VILLAGE, AK - UNITED STATES",Togiak Airport,"Togiak Village, Alaska, United States",OK
    TOH,////,////,NVSD,UNK,1.0,Torres Airstrip,"Torres, Vanuatu",OWNO,////,////,Torres Airstrip,"Loh/Linua, Linua Island, Torba, Vanuatu",OK
    TOI,TOI,KTOI,KTOI,OK,0.355,Municipal,"Troy (AL), USA",OWNO,TROY MUNI AIRPORT AT N KENNETH CAMPBELL FIELD,"TROY, AL - UNITED STATES",Troy Municipal at N Kenneth Campbell Field,"Troy, Alabama, United States",OK
    TOJ,////,////,LETO,UNK,1.0,Torrejon AFB,"Madrid, Spain",OWNO,////,////,Torrejón AFB,"Madrid, Madrid, Spain",OK
    TOK,////,////,////,UNK,1.0,Torokina,"Torokina, Papua New Guinea",OWNO,////,////,????,"Torokina, Bougainville, Papua-New Guinea",OK
    TOL,TOL,KTOL,KTOL,OK,1.0,Express,"Toledo (OH), USA",OWNO,TOLEDO EXPRESS,"TOLEDO, OH - UNITED STATES",Toledo Express Airport,"Toledo, Ohio, United States",OK
    TOM,////,////,GATB,UNK,1.0,Tombouctou,"Tombouctou, Mali",OWNO,////,////,????,"Tombouctou, Gao, Mali",OK
    TON,////,////,////,UNK,1.0,Tonu,"Tonu, Papua New Guinea",OWNO,////,////,????,"Tonu, Bougainville, Papua-New Guinea",OK
    TOO,////,////,MRSV,UNK,0.667,San Vito,"San Vito, Costa Rica",OWNO,////,////,????,"San Vito de Java, Puntarenas, Costa Rica",MAYBE
    TOP,TOP,KTOP,KTOP,OK,0.843,Philip Billard,"Topeka (KS), USA",OWNO,PHILIP BILLARD MUNI,"TOPEKA, KS - UNITED STATES",Philip Billard Municipal Airport,"Topeka, Kansas, United States",OK
    TOQ,////,////,SCBE,UNK,1.0,Barriles,"Tocopilla, Chile",OWNO,////,////,Barriles,"Tocopilla, Antofagasta, Chile",OK
    TOR,TOR,KTOR,KTOR,OK,1.0,Municipal,"Torrington (WY), USA",OWNO,TORRINGTON MUNI,"TORRINGTON, WY - UNITED STATES",Torrington Municipal Airport,"Torrington, Wyoming, United States",OK
    TOS,////,////,ENTC,UNK,1.0,Tromso/Langnes,"Tromso, Norway",OWNO,////,////,Langnes,"Tromsø, Norway",OK
    TOT,////,////,SMCO,UNK,1.0,Coronie,"Totness, Suriname",OWNO,////,////,????,"Totness, Coronie, Suriname",OK
    TOU,////,////,NWWU,UNK,1.0,Touho,"Touho, New Caledonia",OWNO,////,////,????,"Touho, New Caledonia",OK
    TOV,////,////,////,UNK,0.467,West End SPB,"Tortola, British Virgin Islands",OWNO,////,////,Metropolitan Area,"Tortola, Virgin Islands (British)",OK
    TOW,////,////,SBTD,UNK,1.0,Toledo,"Toledo, Brazil",OWNO,////,////,????,"Toledo, Paraná, Brazil",OK
    TOX,////,////,USTO,UNK,1.0,Tobolsk,"Tobolsk, Russia",OWNO,////,////,Tobol'sk Airport,"Tobol'sk, Tyumenskaya, Russian Federation (Russia)",OK
    TOY,////,////,RJNT,UNK,1.0,Toyama,"Toyama, Japan",OWNO,////,////,Toyama Airport,"Toyama, Toyama, Japan",OK
    TOZ,////,////,DITM,UNK,0.6,Touba,"Touba, Cote d'Ivoire",OWNO,////,////,Mahana,"Touba, Bafing, Côte d'Ivoire (Ivory Coast)",OK
    TPA,TPA,KTPA,KTPA,OK,1.0,Tampa International,"Tampa (FL), USA",OWNO,TAMPA INTL,"TAMPA, FL - UNITED STATES",Tampa International Airport,"Tampa, Florida, United States",OK
    TPC,////,////,SETR,UNK,0.609,Tarapoa,"Tarapoa, Ecuador",OWNO,////,////,Nacional,"Tarapoa, Sucumbíos, Ecuador",OK
    TPE,////,////,RCTP,UNK,0.897,Taoyuan International Airport,"Taipei, Taiwan",OWNO,////,////,Taiwan Taoyuan International Airport,"Taipei, Taiwan",OK
    TPF,TPF,KTPF,KTPF,OK,1.0,Peter O. Knight Airport,"Tampa (FL), USA",OWNO,PETER O KNIGHT,"TAMPA, FL - UNITED STATES",Peter O Knight Airport,"Tampa, Florida, United States",OK
    TPG,////,////,WMBI,UNK,1.0,Taiping,"Taiping, Malaysia",OWNO,////,////,????,"Taiping, Perak, Malaysia",OK
    TPH,TPH,KTPH,KTPH,OK,1.0,Tonopah Airport,"Tonopah (NV), USA",OWNO,TONOPAH,"TONOPAH, NV - UNITED STATES",????,"Tonopah, Nevada, United States",OK
    TPI,////,////,////,UNK,1.0,Tapini,"Tapini, Papua New Guinea",OWNO,////,////,????,"Tapini, Central, Papua-New Guinea",OK
    TPJ,////,////,VNTJ,UNK,1.0,Taplejung,"Taplejung, Nepal",OWNO,////,////,????,"Taplejung, Nepal",OK
    TPK,////,////,WITA,UNK,0.64,Tapaktuan,"Tapaktuan, Indonesia",OWNO,////,////,Teuku Cut Ali,"Tapak Tuan, Aceh, Indonesia",OK
    TPL,TPL,KTPL,KTPL,OK,0.659,Draughon-Miller,"Temple (TX), USA",OWNO,DRAUGHON-MILLER CENTRAL TEXAS RGNL,"TEMPLE, TX - UNITED STATES",Draughon-Miller Central Texas Regional,"Temple, Texas, United States",OK
    TPN,////,////,SETI,UNK,1.0,Tiputini,"Tiputini, Ecuador",OWNO,////,////,????,"Tiputini, Oreliana, Ecuador",OK
    TPP,////,////,SPST,UNK,0.292,Tarapoto,"Tarapoto, Peru",OWNO,////,////,Capitán FAP Guillermo del Castillo Paredes Airport,"Tarapoto, San Martín, Perú",OK
    TPQ,////,////,MMEP,UNK,0.37,Tepic,"Tepic, Mexico",OWNO,////,////,Amado Nervo National,"Tepic, Nayarit, México",OK
    TPR,////,////,YTMP,UNK,1.0,Tom Price,"Tom Price, Australia",OWNO,////,////,Tom Price Airport,"Mt. Sheila, Western Australia, Australia",OK
    TPS,////,////,LICT,UNK,0.6,Birgi,"Trapani, Italy",OWNO,////,////,Vincenzo Florio Airport,"Trapani, Sicily, Italy",OK
    TPT,////,////,////,UNK,1.0,Tapeta,"Tapeta, Liberia",OWNO,////,////,????,"Tapeta, Liberia",OK
    TPU,////,////,VNTP,UNK,1.0,Tikapur,"Tikapur, Nepal",OWNO,////,////,????,"Tikapur, Nepal",OK
    TQL,////,////,USDS,UNK,nan,////,////,////,////,////,Tarko-Sale Airport,"Tarko-Sale, Yamalo-Nenetskiy, Russian Federation (Russia)",UNK
    TQN,////,////,OATQ,UNK,1.0,Taluqan,"Taluqan, Afghanistan",OWNO,////,////,????,"Taluqan, Takhar, Afghanistan",OK
    TQP,////,////,YTEE,UNK,nan,////,////,////,////,////,Trepell Airport,"Trepell, Queensland, Australia",UNK
    TQQ,////,////,////,UNK,1.0,Maranggo,Tomia,IATA,////,////,Maranggo Airport,"Tomia, Wakatobi Island, Sulawesi Tenggara, Indonesia",MAYBE
    TQR,////,////,LINI,UNK,0.8,San Domino Island,"San Domino Island, Italy",OWNO,////,////,San Domino Island Heliport,"San Domino, Apulia, Italy",OK
    TQS,////,////,SKTQ,UNK,0.483,Tres Esquinas,"Tres Esquinas, Colombia",OWNO,////,////,Ernesto Esguerra Cubides AB,"Tres Esquinas, Caquetá, Colombia",OK
    TRA,////,////,RORT,UNK,1.0,Tarama,"Taramajima, Japan",OWNO,////,////,Tarama Airport,"Tarama, Tarama Island, Okinawa, Japan",OK
    TRB,////,////,SKTU,UNK,0.846,Gonzalo,"Turbo, Colombia",OWNO,////,////,Gonzalo Mejía Airport,"Turbo, Antioquia, Colombia",OK
    TRC,////,////,MMTC,UNK,0.378,Torreon,"Torreon, Mexico",OWNO,////,////,Francisco Sarabia International Airport,"Torreón, Coahuila, México",OK
    TRD,////,////,ENVA,UNK,1.0,Vaernes,"Trondheim, Norway",OWNO,////,////,Vaernes,"Trondheim, Norway",OK
    TRE,////,////,EGPU,UNK,1.0,Tiree,"Tiree, United Kingdom",OWNO,////,////,????,"Tiree, Isle of Tiree, Argyll, Scotland, United Kingdom",OK
    TRF,////,////,ENTO,UNK,0.8,Sandefjord,"Oslo, Norway",OWNO,////,////,Torp,"Sandefjord, Norway",OK
    TRG,////,////,NZTG,UNK,1.0,Tauranga,"Tauranga, New Zealand",OWNO,////,////,????,"Tauranga, New Zealand",OK
    TRH,L72,////,////,OK,1.0,Trona,"Trona (CA), USA",OWNO,TRONA,"TRONA, CA - UNITED STATES",????,"Trona, California, United States",OK
    TRI,TRI,KTRI,KTRI,OK,0.906,Tri-Cities Regional,"Kingsport (TN), USA",OWNO,TRI-CITIES RGNL TN/VA,"BRISTOL/JOHNSON/KINGSPORT, TN - UNITED STATES",Tri-Cities Regional TN/Va,"Bristol/Johnson/Kingsport, Tennessee, United States",OK
    TRJ,////,////,////,UNK,1.0,Tarakbits,"Tarakbits, Papua New Guinea",OWNO,////,////,????,"Tarakbits, Western, Papua-New Guinea",OK
    TRK,////,////,WALR,UNK,0.636,Tarakan,"Tarakan, Indonesia",OWNO,////,////,Juwata International Airport,"Tarakan, Kalimantan Utara (North Borneo), Indonesia",OK
    TRL,TRL,KTRL,KTRL,OK,0.658,Terrell Field,"Terrell (TX), USA",OWNO,TERRELL MUNI,"TERRELL, TX - UNITED STATES",Terrell Municipal Airport,"Terrell, Texas, United States",OK
    TRM,TRM,KTRM,KTRM,OK,0.376,Thermal,"Thermal (CA), USA",OWNO,JACQUELINE COCHRAN RGNL,"PALM SPRINGS, CA - UNITED STATES",Jacqueline Cochran Regional,"Palm Springs, California, United States",OK
    TRN,////,////,LIMF,UNK,0.818,Citta Di Torino,"Turin, Italy",OWNO,////,////,Caselle,"Turin, Piedmont, Italy",OK
    TRO,////,////,YTRE,UNK,1.0,Taree,"Taree, Australia",OWNO,////,////,????,"Taree, New South Wales, Australia",OK
    TRQ,////,////,SBTK,UNK,1.0,Tarauaca,"Tarauaca, Brazil",OWNO,////,////,????,"Tarauacá, Acre, Brazil",OK
    TRR,////,////,VCCT,UNK,1.0,China Bay,"Trincomalee, Sri Lanka",OWNO,////,////,China Bay Airport,"Trincomalee, Eastern Province, Sri Lanka (Ceylon)",OK
    TRS,////,////,LIPQ,UNK,0.857,Dei Legionari,"Trieste, Italy",OWNO,////,////,Ronchi dei Legionari,"Trieste, Friuli-Venezia Giulia, Italy",OK
    TRU,////,////,SPRU,UNK,0.262,Trujillo,"Trujillo, Peru",OWNO,////,////,Capitán FAP Carlos Martínez de Pinillos International Airport,"Trujillo, La Libertad, Perú",OK
    TRV,////,////,VOTV,UNK,1.0,International,"Thiruvananthapuram, India",OWNO,////,////,Thiruvananthapuram International Airport,"Trivandrum, Kerala, India",TO DO CHECK
    TRW,////,////,NGTA,UNK,0.8,Bonriki,"Tarawa, Kiribati",OWNO,////,////,Bonriki International Airport,"Tarawa Island, Kiribati",OK
    TRX,TRX,KTRX,KTRX,OK,0.774,Memorial,"Trenton (MO), USA",OWNO,TRENTON MUNI,"TRENTON, MO - UNITED STATES",Trenton Municipal Airport,"Trenton, Missouri, United States",OK
    TRY,////,////,HUTO,UNK,1.0,Tororo,"Tororo, Uganda",OWNO,////,////,????,"Tororo, Uganda",OK
    TRZ,////,////,VOTR,UNK,1.0,Civil,"Tiruchirapally, India",OWNO,////,////,Civil,"Tiruchirapally, Tamil Nadu, India",OK
    TSA,////,////,RCSS,UNK,0.71,Sung Shan,"Taipei, Taiwan",OWNO,////,////,Taipei Songshan,"Taipei, Taiwan",OK
    TSB,////,////,FYTM,UNK,1.0,Tsumeb,"Tsumeb, Namibia",OWNO,////,////,????,"Tsumeb, Namibia",OK
    TSC,////,////,SETH,UNK,1.0,Taisha,"Taisha, Ecuador",OWNO,////,////,????,"Taisha, Morona-Santiago, Ecuador",OK
    TSD,////,////,////,UNK,1.0,Tshipise,"Tshipise, South Africa",OWNO,////,////,????,"Tshipise, Limpopo, South Africa",OK
    TSE,////,////,UACC,UNK,0.714,Astana,"Astana, Kazakhstan",OWNO,////,////,Astana International Airport,"Astana, Aqmola, Kazakhstan",OK
    TSF,////,////,LIPH,UNK,0.647,Treviso,"Venice, Italy",OWNO,////,////,Treviso-Sant'Angelo,"Treviso, Veneto, Italy",OK
    TSG,TSG,////,////,OK,0.549,Intermediate,"Tanacross (AK), USA",OWNO,TANACROSS,"TANACROSS, AK - UNITED STATES",????,"Tanacross, Alaska, United States",OK
    TSH,////,////,FZUK,UNK,1.0,Tshikapa,"Tshikapa, Congo (DRC)",OWNO,////,////,Tshikapa Airport,"Tshikapa, Kasai-Occidental (West Kasai), Democratic Republic of Congo (Zaire)",OK
    TSI,////,////,////,UNK,1.0,Tsili Tsili,"Tsili Tsili, Papua New Guinea",OWNO,////,////,????,"Tsili Tsili, Morobe, Papua-New Guinea",OK
    TSJ,////,////,RJDT,UNK,1.0,Tsushima,"Tsushima, Japan",OWNO,////,////,Tsushima Airport,"Tsushima, Tsushima Island, Nagasaki, Japan",OK
    TSK,////,////,////,UNK,1.0,Taskul,"Taskul, Papua New Guinea",OWNO,////,////,????,"Taskul, New Ireland, Papua-New Guinea",OK
    TSL,////,////,MMTN,UNK,1.0,Tamuin,"Tamuin, Mexico",OWNO,////,////,????,"Tamuín, San Luis Potosí, México",OK
    TSM,SKX,KSKX,KSKX,OK,0.378,Taos,"Taos (NM), USA",OWNO,TAOS RGNL,"TAOS, NM - UNITED STATES",Taos Regional,"Taos, New Mexico, United States",OK
    TSN,////,////,ZBTJ,UNK,0.632,Tianjin,"Tianjin, PR China",OWNO,////,////,Binhai International Airport,"Tianjin, Tianjin, China",OK
    TSO,////,////,EGHT,UNK,0.83,Tresco,"Isles Of Scilly, United Kingdom",OWNO,////,////,Tresco Heliport,"Tresco, Isles of Scilly, England, United Kingdom",OK
    TSP,TSP,KTSP,KTSP,OK,0.694,Kern County,"Tehachapi (CA), USA",OWNO,TEHACHAPI MUNI,"TEHACHAPI, CA - UNITED STATES",Tehachapi Municipal Airport,"Tehachapi, California, United States",OK
    TSR,////,////,LRTR,UNK,0.583,Timisoara,"Timisoara, Romania",OWNO,////,////,Traian Vuia International Airport,"Timisoara, Romania",OK
    TSS,6N5,////,////,OK,0.607,East 34th St Heliport,"New York (NY), USA",OWNO,EAST 34TH STREET,"NEW YORK, NY - UNITED STATES",East 34th Street Heliport,"New York, New York, United States",OK
    TST,////,////,VTST,UNK,1.0,Trang,"Trang, Thailand",OWNO,////,////,????,"Trang, Trang, Thailand",OK
    TSU,////,////,NGTS,UNK,1.0,Tabiteuea South,"Tabiteuea South, Kiribati",OWNO,////,////,????,"Tabiteuea South, Kiribati",OK
    TSV,////,////,YBTL,UNK,1.0,Townsville,"Townsville, Australia",OWNO,////,////,????,"Townsville, Queensland, Australia",OK
    TSW,////,////,////,UNK,1.0,Tsewi,"Tsewi, Papua New Guinea",OWNO,////,////,????,"Tsewi, Morobe, Papua-New Guinea",OK
    TSX,////,////,WALT,UNK,1.0,Tanjung Santan,"Tanjung Santan, Indonesia",OWNO,////,////,????,"Tanjung Santan, Kalimantan (Borneo), Kalimantan Timur (East Borneo), Indonesia",OK
    TSY,////,////,WICM,UNK,0.72,Tasikmalaya,"Tasikmalaya, Indonesia",OWNO,////,////,Cibeureum,"Tasikmalaya, Jawa Barat, Indonesia",OK
    TSZ,////,////,ZMTG,UNK,1.0,Tsetserleg,"Tsetserleg, Mongolia",OWNO,////,////,????,"Tsetserleg, Mongolia",OK
    TTA,////,////,GMAT,UNK,1.0,Tan Tan,"Tan Tan, Morocco",OWNO,////,////,Tan Tan Airport,"Tan Tan, Morocco",OK
    TTB,////,////,LIET,UNK,0.769,Arbatax,"Tortoli, Italy",OWNO,////,////,????,"Tortolì, Sardinia, Italy",OK
    TTC,////,////,SCTT,UNK,0.632,Taltal,"Taltal, Chile",OWNO,////,////,Las Breas Airport,"Taltal, Antofagasta, Chile",OK
    TTD,TTD,KTTD,KTTD,OK,0.608,Troutdale,"Troutdale (OR), USA",OWNO,PORTLAND-TROUTDALE,"PORTLAND, OR - UNITED STATES",Portland-Troutdale Airport,"Portland, Oregon, United States",OK
    TTE,////,////,WAMT,UNK,0.821,Babullah,"Ternate, Indonesia",OWNO,////,////,Sultan Babullah,"Ternate, Maluku Utara, Indonesia",OK
    TTG,////,////,SAST,UNK,0.444,Tartagal,"Tartagal, Argentina",OWNO,////,////,General Enrique Mosconi Tartagal,"Tartagal, Salta, Argentina",OK
    TTH,////,////,OOTH,UNK,0.762,Thumrait,"Thumrait, Oman",OWNO,////,////,RAFO Thumrait,"Thumrait, Oman",OK


List for checking certainty of Great Circle Mapper (LND - LTR)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    LND,LND,KLND,KLND,OK,1.0,Hunt Field,"Lander (WY), USA",OWNO,HUNT FIELD,"LANDER, WY - UNITED STATES",Hunt Field,"Lander, Wyoming, United States",OK
    LNE,////,////,NVSO,UNK,1.0,Lonorore,"Lonorore, Vanuatu",OWNO,////,////,????,"Lonorore, Pentecost (Pentecôte) Island, Pénama, Vanuatu",OK
    LNF,////,////,////,UNK,1.0,Munbil,"Munbil, Papua New Guinea",OWNO,////,////,????,"Munbil, Sandaun, Papua-New Guinea",OK
    LNG,////,////,////,UNK,1.0,Lese,"Lese, Papua New Guinea",OWNO,////,////,????,"Lese, Gulf, Papua-New Guinea",OK
    LNH,////,////,YLKN,UNK,1.0,Lake Nash,"Lake Nash, Australia",OWNO,////,////,????,"Lake Nash, Northern Territory, Australia",OK
    LNI,AK71,////,PALN,NG,0.423,Dew Station,"Lonely (AK), USA",OWNO,LONELY AS,"LONELY, AK - UNITED STATES",Lonely Air Station,"Lonely, Alaska, United States",OK
    LNJ,////,////,ZPLC,UNK,nan,////,////,////,////,////,????,"Lincang, Yunnan, China",UNK
    LNK,LNK,KLNK,KLNK,OK,0.766,Municipal,"Lincoln (NE), USA",OWNO,LINCOLN,"LINCOLN, NE - UNITED STATES",????,"Lincoln, Nebraska, United States",OK
    LNM,////,////,////,UNK,1.0,Langimar,"Langimar, Papua New Guinea",OWNO,////,////,????,"Langimar, Morobe, Papua-New Guinea",OK
    LNN,LNN,KLNN,KLNN,OK,0.837,Lost Nation,"Willoughby (OH), USA",OWNO,WILLOUGHBY LOST NATION MUNI,"WILLOUGHBY, OH - UNITED STATES",Willoughby Lost Nation Municipal Airport,"Willoughby, Ohio, United States",OK
    LNO,////,////,YLEO,UNK,1.0,Leonora,"Leonora, Australia",OWNO,////,////,????,"Leonora, Western Australia, Australia",OK
    LNP,LNP,KLNP,KLNP,OK,0.423,Wise,"Wise (VA), USA",OWNO,LONESOME PINE,"WISE, VA - UNITED STATES",Lonesome Pine Airport,"Wise, Virginia, United States",OK
    LNQ,////,////,////,UNK,1.0,Loani,"Loani, Papua New Guinea",OWNO,////,////,????,"Loani, Milne Bay, Papua-New Guinea",OK
    LNR,LNR,KLNR,KLNR,OK,0.852,Tri County,"Lone Rock (WI), USA",OWNO,TRI-COUNTY RGNL,"LONE ROCK, WI - UNITED STATES",Tri-County Regional,"Lone Rock, Wisconsin, United States",OK
    LNS,LNS,KLNS,KLNS,OK,1.0,Lancaster,"Lancaster (PA), USA",OWNO,LANCASTER,"LANCASTER, PA - UNITED STATES",????,"Lancaster, Pennsylvania, United States",OK
    LNV,////,////,AYKY,UNK,1.0,Lihir Island,"Lihir Island, Papua New Guinea",OWNO,////,////,????,"Londolovit, Lihir Island, New Ireland, Papua-New Guinea",OK
    LNX,////,////,////,UNK,0.696,Smolensk,"Smolensk, Russia",OWNO,////,////,Smolensk Severny Aerodrome,"Smolensk, Smolenskaya, Russian Federation (Russia)",OK
    LNY,LNY,PHNY,PHNY,OK,1.0,Lanai City,"Lanai City (HI), USA",OWNO,LANAI,"LANAI CITY, HI - UNITED STATES",Lanai Airport,"Lanai City, Lanai, Hawaii, United States",OK
    LNZ,////,////,LOWL,UNK,0.545,Hoersching,"Linz, Austria",OWNO,////,////,Blue Danube Airport Linz,"Linz, Oberösterreich (Upper Austria), Austria",OK
    LOA,////,////,YLOR,UNK,1.0,Lorraine,"Lorraine, Australia",OWNO,////,////,????,"Lorraine, Queensland, Australia",OK
    LOB,////,////,SCAN,UNK,0.621,Los Andes,"Los Andes, Chile",OWNO,////,////,San Rafael Airport,"Los Andes, Valparaíso, Chile",OK
    LOC,////,////,YLOK,UNK,1.0,Lock,"Lock, Australia",OWNO,////,////,????,"Lock, South Australia, Australia",OK
    LOD,////,////,NVSG,UNK,1.0,Longana,"Longana, Vanuatu",OWNO,////,////,????,"Longana, Ambae Island, Pénama, Vanuatu",OK
    LOE,////,////,VTUL,UNK,1.0,Loei,"Loei, Thailand",OWNO,////,////,????,"Loei, Loei, Thailand",OK
    LOF,////,////,////,UNK,1.0,Loen,"Loen, Marshall Islands",OWNO,////,////,????,"Loen, Marshall Islands",OK
    LOH,////,////,SETM,UNK,0.267,Loja,"Loja, Ecuador",OWNO,////,////,Camilo Ponce Enriquez,"Loja, Loja, Ecuador",OK
    LOK,////,////,HKLO,UNK,1.0,Lodwar,"Lodwar, Kenya",OWNO,////,////,????,"Lodwar, Kenya",OK
    LOL,LOL,KLOL,KLOL,OK,1.0,Derby Field,"Lovelock (NV), USA",OWNO,DERBY FIELD,"LOVELOCK, NV - UNITED STATES",Derby Field,"Lovelock, Nevada, United States",OK
    LON,////,////,////,UNK,1.0,Metropolitan Area,"London, United Kingdom",OWNO,////,////,Metropolitan Area,"London, England, United Kingdom",OK
    LOO,////,////,DAUL,UNK,1.0,L'Mekrareg,"Laghouat, Algeria",OWNO,////,////,L'Mekrareg Airport,"Laghouat, Laghouat, Algeria",OK
    LOP,////,////,WADL,UNK,1.0,Lombok International Airport,"Mataram, Lombok, Indonesia",OWNO,////,////,Lombok International Airport,"Praya, Lombok Island, Nusa Tenggara Barat, Indonesia",OK
    LOQ,////,////,FBLO,UNK,1.0,Lobatse,"Lobatse, Botswana",OWNO,////,////,Lobatse Airport,"Lobatse, South-East, Botswana",OK
    LOR,LOR,KLOR,KLOR,OK,0.542,Lowe Army Heliport,"Ozark (AL), USA",OWNO,LOWE AHP (FORT RUCKER),"FORT RUCKER OZARK, AL - UNITED STATES",Lowe Army Heliport,"Fort Rucker, Alabama, United States",OK
    LOS,////,////,DNMM,UNK,0.914,Murtala Muhammed,"Lagos, Nigeria",OWNO,////,////,Murtala Muhammed International Airport,"Lagos, Lagos, Nigeria",OK
    LOT,LOT,KLOT,KLOT,OK,0.501,Lewis Lockport,"Lockport (IL), USA",OWNO,LEWIS UNIVERSITY,"CHICAGO/ROMEOVILLE, IL - UNITED STATES",Lewis University Airport,"Chicago/Romeoville, Illinois, United States",OK
    LOU,LOU,KLOU,KLOU,OK,1.0,Bowman Field,"Louisville (KY), USA",OWNO,BOWMAN FIELD,"LOUISVILLE, KY - UNITED STATES",Bowman Field,"Louisville, Kentucky, United States",OK
    LOV,////,////,MMMV,UNK,0.414,Monclova,"Monclova, Mexico",OWNO,////,////,Venustiano Carranza International Airport,"Monclova, Coahuila, México",OK
    LOW,LKU,KLKU,KLKU,OK,0.658,Louisa,"Louisa (VA), USA",OWNO,LOUISA COUNTY/FREEMAN FIELD,"LOUISA, VA - UNITED STATES",Louisa County/Freeman Field,"Louisa, Virginia, United States",OK
    LOX,////,////,////,UNK,1.0,Los Tablones Airport,"Los Tablones, Guatemala",OWNO,////,////,????,"Los Tablones, El Progreso, Guatemala",OK
    LOY,////,////,HKLY,UNK,1.0,Loyangalani,"Loyangalani, Kenya",OWNO,////,////,????,"Loyangalani, Kenya",OK
    LOZ,LOZ,KLOZ,KLOZ,OK,0.712,Corbin-London,"London (KY), USA",OWNO,LONDON-CORBIN ARPT-MAGEE FIELD,"LONDON, KY - UNITED STATES",London-Corbin Airport-Magee Field,"London, Kentucky, United States",OK
    LPA,////,////,GCLP,UNK,0.875,Airport de Gran Canaria,"Las Palmas, Canary Islands, Spain",OWNO,////,////,Gran Canaria International Airport,"Las Palmas, Gran Canaria Island, Canary Islands, Spain",OK
    LPB,////,////,SLLP,UNK,0.848,El Alto,"La Paz, Bolivia",OWNO,////,////,J. F. Kennedy/El Alto International Airport,"La Paz, Pedro Domingo Murillo, La Paz, Bolivia",OK
    LPC,LPC,KLPC,KLPC,OK,1.0,Lompoc Airport,"Lompoc (CA), USA",OWNO,LOMPOC,"LOMPOC, CA - UNITED STATES",????,"Lompoc, California, United States",OK
    LPD,////,////,SKLP,UNK,1.0,La Pedrera,"La Pedrera, Colombia",OWNO,////,////,La Pedrera Airport,"La Pedrera, Amazonas, Colombia",OK
    LPE,////,////,////,UNK,1.0,La Primavera,"La Primavera, Colombia",OWNO,////,////,????,"La Primavera, Vichada, Colombia",OK
    LPF,////,////,ZUPS,UNK,nan,////,////,////,////,////,Liupanshui Yuezhao Airport,"Liupanshui, Guizhou, China",UNK
    LPG,////,////,SADL,UNK,1.0,La Plata,"La Plata, Argentina",OWNO,////,////,????,"La Plata, Buenos Aires, Argentina",OK
    LPI,////,////,ESSL,UNK,0.818,Linkoping,"Linkoping, Sweden",OWNO,////,////,Linköping City Airport,"Linköping, Östergötlands län, Sweden",OK
    LPJ,////,////,SVAS,UNK,0.514,Pijiguaos,"Pijiguaos, Venezuela",OWNO,////,////,Armando Schwarck,"Pijiguaos, Bolívar, Venezuela",OK
    LPK,////,////,UUOL,UNK,1.0,Lipetsk,"Lipetsk, Russia",OWNO,////,////,Lipetsk Airport,"Lipetsk, Lipetskaya, Russian Federation (Russia)",OK
    LPL,////,////,EGGP,UNK,0.686,Liverpool International Airport,"Liverpool, United Kingdom",OWNO,////,////,Liverpool John Lennon,"Liverpool, Lancashire, England, United Kingdom",OK
    LPM,////,////,NVSL,UNK,1.0,Lamap,"Lamap, Vanuatu",OWNO,////,////,????,"Lamap, Malekula Island, Malampa, Vanuatu",OK
    LPO,PPO,KPPO,KPPO,OK,0.563,Municipal,"Laporte (IN), USA",OWNO,LA PORTE MUNI,"LA PORTE, IN - UNITED STATES",La Porte Municipal Airport,"La Porte, Indiana, United States",OK
    LPP,////,////,EFLP,UNK,1.0,Lappeenranta,"Lappeenranta, Finland",OWNO,////,////,????,"Lappeenranta, Etelä-Karjala (Södra Karelen (South Karelia)), Finland",OK
    LPQ,////,////,VLLB,UNK,0.88,Luang Prabang,"Luang Prabang, Lao PDR",OWNO,////,////,Luang Prabang International Airport,"Luang Prabang, Lao People's Democratic Republic (Laos)",OK
    LPS,////,////,////,UNK,0.529,Lopez Island,"Lopez Island (WA), USA",OWNO,////,////,Liupanshui Yuezhao Airport,"Liupanshui, Guizhou, China",NG
    LPS,81W,////,////,OK,0.531,Lopez Island,"Lopez Island (WA), USA",OWNO,FISHERMANS BAY,"LOPEZ, WA - UNITED STATES",Fishermans Bay SPB,"Lopez Island, Washington, United States",OK
    LPT,////,////,VTCL,UNK,1.0,Lampang,"Lampang, Thailand",OWNO,////,////,????,"Lampang, Lampang, Thailand",OK
    LPU,////,////,WALP,UNK,1.0,Long Apung,"Long Apung, Indonesia",OWNO,////,////,Long Apung Airport,"Long Apung, Kalimantan Timur (East Borneo), Indonesia",OK
    LPX,////,////,EVLA,UNK,0.737,Liepaya,"Liepaya, Latvia",OWNO,////,////,Liepaya International Airport,"Liepaya, Latvia",OK
    LPY,////,////,LFHP,UNK,1.0,Loudes,"Le Puy, France",OWNO,////,////,Loudes,"Le Puy, Auvergne, France",OK
    LQK,LQK,KLQK,KLQK,OK,0.76,Pickens,"Pickens (SC), USA",OWNO,PICKENS COUNTY,"PICKENS, SC - UNITED STATES",Pickens County Airport,"Pickens, South Carolina, United States",OK
    LQM,////,////,SKLG,UNK,0.828,Puerto Leguizamo,"Puerto Leguizamo, Colombia",OWNO,////,////,Caucayá Airport,"Puerto Leguizamo, Putumayo, Colombia",OK
    LQN,////,////,OAQN,UNK,0.909,Qala Nau,"Qala Nau, Afghanistan",OWNO,////,////,????,"Qala I Naw, Badghis, Afghanistan",OK
    LRA,////,////,LGLR,UNK,1.0,Larisa,"Larisa, Greece",OWNO,////,////,????,"Larisa, Thessalía (Thessaly), Greece",OK
    LRB,////,////,FXLR,UNK,1.0,Leribe,"Leribe, Lesotho",OWNO,////,////,????,"Leribe, Lesotho",OK
    LRD,LRD,KLRD,KLRD,OK,1.0,International,"Laredo (TX), USA",OWNO,LAREDO INTL,"LAREDO, TX - UNITED STATES",Laredo International Airport,"Laredo, Texas, United States",OK
    LRE,////,////,YLRE,UNK,1.0,Longreach,"Longreach, Australia",OWNO,////,////,????,"Longreach, Queensland, Australia",OK
    LRF,LRF,KLRF,KLRF,OK,1.0,Little Rock AFB,"Jacksonville (AR), USA",OWNO,LITTLE ROCK AFB,"JACKSONVILLE, AR - UNITED STATES",Little Rock AFB,"Jacksonville, Arkansas, United States",OK
    LRG,////,////,////,UNK,0.6,Lora Lai,"Lora Lai, Pakistan",OWNO,////,////,????,"Loralai, Balochistan, Pakistan",OK
    LRH,////,////,LFBH,UNK,0.833,Laleu,"La Rochelle, France",OWNO,////,////,Île de Re,"La Rochelle, Poitou-Charentes, France",OK
    LRI,////,////,////,UNK,0.545,Lorica,"Lorica, Colombia",OWNO,////,////,????,"Santa Cruz de Lorica, Córdoba, Colombia",MAYBE
    LRJ,LRJ,KLRJ,KLRJ,OK,1.0,Municipal,"Lemars IA, USA",OWNO,LE MARS MUNI,"LE MARS, IA - UNITED STATES",Le Mars Municipal Airport,"Le Mars, Iowa, United States",OK
    LRL,////,////,DXNG,UNK,0.75,Lama-Kara,"Niamtougou, Togo",OWNO,////,////,Niamtougou International Airport,"Niamtougou, Kara, Togo",OK
    LRM,////,////,MDLR,UNK,0.667,La Romana International Airport,"La Romana, Dominican Republic",OWNO,////,////,Casa de Campo International Airport,"La Romana, La Romana, Dominican Republic",OK
    LRQ,////,////,////,UNK,1.0,Laurie River,"Laurie River, Canada",OWNO,////,////,Laurie River Airport,"Laurie River, Manitoba, Canada",OK
    LRR,////,////,OISL,UNK,0.857,Lar A/P,"Lar, Iran",OWNO,////,////,????,"Lar, Fars, Iran",OK
    LRS,////,////,LGLE,UNK,1.0,Leros,"Leros, Greece",OWNO,////,////,????,"Leros, Notío Aigaío (Southern Aegean), Greece",OK
    LRT,////,////,LFRH,UNK,1.0,Lann Bihoue,"Lorient, France",OWNO,////,////,Lann Bihoue,"Lorient, Bretagne, France",OK
    LRU,LRU,KLRU,KLRU,OK,0.529,Municipal,"Las Cruces (NM), USA",OWNO,LAS CRUCES INTL,"LAS CRUCES, NM - UNITED STATES",Las Cruces International Airport,"Las Cruces, New Mexico, United States",OK
    LRV,////,////,SVRS,UNK,1.0,Los Roques,"Los Roques, Venezuela",OWNO,////,////,Los Roques,"El Gran Roque, Los Roques Islands, Dependencias Federales, Venezuela",OK
    LSA,////,////,AYKA,UNK,1.0,Losuia,"Losuia, Papua New Guinea",OWNO,////,////,Losuia Airport,"Losuia, Kiriwina Island, Milne Bay, Papua-New Guinea",OK
    LSB,LSB,KLSB,KLSB,OK,0.734,Lordsburg,"Lordsburg (NM), USA",OWNO,LORDSBURG MUNI,"LORDSBURG, NM - UNITED STATES",Lordsburg Municipal Airport,"Lordsburg, New Mexico, United States",OK
    LSC,////,////,SCSE,UNK,1.0,La Florida,"La Serena, Chile",OWNO,////,////,La Florida Airport,"La Serena, Coquimbo, Chile",OK
    LSE,LSE,KLSE,KLSE,OK,0.766,Municipal,"La Crosse (WI), USA",OWNO,LA CROSSE RGNL,"LA CROSSE, WI - UNITED STATES",La Crosse Regional,"La Crosse, Wisconsin, United States",OK
    LSF,LSF,KLSF,KLSF,OK,1.0,Lawson AAF,"Fort Benning (GA), USA",OWNO,LAWSON AAF (FORT BENNING),"FORT BENNING(COLUMBUS), GA - UNITED STATES",Lawson AAF Airport,"Fort Benning, Georgia, United States",OK
    LSH,////,////,VYLS,UNK,1.0,Lashio,"Lashio, Myanmar",OWNO,////,////,????,"Lashio, Shan, Myanmar (Burma)",OK
    LSI,////,////,EGPB,UNK,1.0,Sumburgh,"Shetland Islands, United Kingdom",OWNO,////,////,Sumburgh Airport,"Sumburgh, Shetland Islands, Scotland, United Kingdom",OK
    LSJ,////,////,////,UNK,1.0,Long Island,"Long Island, Papua New Guinea",OWNO,////,////,????,"Long Island, Madang, Papua-New Guinea",OK
    LSK,LSK,KLSK,KLSK,OK,0.686,Lusk,"Lusk (WY), USA",OWNO,LUSK MUNI,"LUSK, WY - UNITED STATES",Lusk Municipal Airport,"Lusk, Wyoming, United States",OK
    LSL,////,////,MRLC,UNK,1.0,Los Chiles,"Los Chiles, Costa Rica",OWNO,////,////,????,"Los Chiles, Alajuela, Costa Rica",OK
    LSM,////,////,WBGD,UNK,1.0,Lawas,"Long Semado, Malaysia",OWNO,////,////,Lawas,"Long Semado, Sarawak, Malaysia",OK
    LSN,LSN,KLSN,KLSN,OK,0.79,Los Banos,"Los Banos (CA), USA",OWNO,LOS BANOS MUNI,"LOS BANOS, CA - UNITED STATES",Los Baños Municipal Airport,"Los Baños, California, United States",OK
    LSO,////,////,LFOO,UNK,1.0,Talmont,"Les Sables, France",OWNO,////,////,Talmont,"Les Sables d'Olonne, Pays de la Loire, France",OK
    LSP,////,////,SVJC,UNK,1.0,Josefa Camejo,"Las Piedras, Venezuela",OWNO,////,////,Josefa Camejo,"Las Piedras, Falcón, Venezuela",OK
    LSQ,////,////,SCGE,UNK,0.611,Los Angeles,"Los Angeles, Chile",OWNO,////,////,María Dolores Airport,"Los Ángeles, Bío Bío, Chile",OK
    LSS,////,////,TFFS,UNK,0.684,Terre-de-Haut,"Terre-de-Haut, Guadeloupe",OWNO,////,////,Les Saintes Airport,"Terre-de-Haut, Îles des Saintes, Guadeloupe",OK
    LST,////,////,YMLT,UNK,1.0,Launceston,"Launceston, Australia",OWNO,////,////,????,"Launceston, Tasmania, Australia",OK
    LSU,////,////,WBGU,UNK,1.0,Long Sukang,"Long Sukang, Malaysia",OWNO,////,////,????,"Long Sukang, Sarawak, Malaysia",OK
    LSV,LSV,KLSV,KLSV,OK,1.0,Nellis AFB,"Las Vegas (NV), USA",OWNO,NELLIS AFB,"LAS VEGAS, NV - UNITED STATES",Nellis AFB,"Las Vegas, Nevada, United States",OK
    LSW,////,////,WITM,UNK,0.667,Lhoksumawe,"Lhoksumawe, Indonesia",OWNO,////,////,Malikus Saleh,"Lhoksumawe, Aceh, Indonesia",OK
    LSX,////,////,WITL,UNK,1.0,Lhok Sukon,"Lhok Sukon, Indonesia",OWNO,////,////,????,"Lhok Sukon, Aceh, Indonesia",OK
    LSY,////,////,YLIS,UNK,1.0,Lismore,"Lismore, Australia",OWNO,////,////,????,"Lismore, New South Wales, Australia",OK
    LSZ,////,////,LDLO,UNK,1.0,Losinj Airport,"Mali Losinj, Croatia",OWNO,////,////,????,"Mali Losinj, Losinj, Croatia",OK
    LTA,////,////,FATZ,UNK,0.8,Letaba,"Tzaneen, South Africa",OWNO,////,////,Tzaneen Airport,"Tzaneen, Limpopo, South Africa",OK
    LTB,////,////,////,UNK,1.0,Latrobe,"Latrobe, Australia",OWNO,////,////,????,"Latrobe, Tasmania, Australia",OK
    LTC,////,////,FTTH,UNK,1.0,Lai,"Lai, Chad",OWNO,////,////,Laï Airport,"Laï, Tandjilé, Chad",OK
    LTD,////,////,HLTD,UNK,1.0,Ghadames,"Ghadames, Libya",OWNO,////,////,????,"Ghadames, Libyan Arab Jamahiriya (Libya)",OK
    LTF,////,////,////,UNK,1.0,Leitre,"Leitre, Papua New Guinea",OWNO,////,////,????,"Leitre, Sandaun, Papua-New Guinea",OK
    LTG,////,////,VNLT,UNK,1.0,Langtang,"Langtang, Nepal",OWNO,////,////,????,"Langtang, Nepal",OK
    LTI,////,////,ZMAT,UNK,1.0,Altai,"Altai, Mongolia",OWNO,////,////,????,"Altai, Govi-Altai, Mongolia",OK
    LTK,////,////,OSLK,UNK,0.417,Latakia,"Latakia, Syria",OWNO,////,////,Bassel al Assad International Airport,"Latakia, Syrian Arab Republic (Syria)",OK
    LTL,////,////,FOOR,UNK,1.0,Lastourville,"Lastourville, Gabon",OWNO,////,////,????,"Lastourville, Ogooué-Lolo, Gabon",OK
    LTM,////,////,SYLT,UNK,1.0,Lethem,"Lethem, Guyana",OWNO,////,////,????,"Lethem, Upper Takutu-Upper Essequibo, Guyana",OK
    LTN,////,////,EGGW,UNK,1.0,Luton Airport,"London, United Kingdom",OWNO,////,////,Luton,"London, Bedfordshire, England, United Kingdom",OK
    LTO,////,////,MMLT,UNK,0.769,Loreto,"Loreto, Mexico",OWNO,////,////,Loreto International Airport,"Loreto, Baja California Sur, México",OK
    LTP,////,////,YLHS,UNK,1.0,Lyndhurst,"Lyndhurst, Australia",OWNO,////,////,????,"Lyndhurst, Queensland, Australia",OK
    LTQ,////,////,LFAT,UNK,0.625,Le Touquet,"Le Touquet, France",OWNO,////,////,????,"Le Touquet-Paris-Plage, Nord-Pas-de-Calais, France",MAYBE
    LTR,////,////,EILT,UNK,1.0,Letterkenny,"Letterkenny, Ireland",OWNO,////,////,????,"Letterkenny, County Donegal, Ulster, Ireland",OK

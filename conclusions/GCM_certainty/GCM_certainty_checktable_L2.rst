
List for checking certainty of Great Circle Mapper (LFR - LNC)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    LFR,////,////,SVLF,UNK,1.0,La Fria,"La Fria, Venezuela",OWNO,////,////,????,"La Fria, Táchira, Venezuela",OK
    LFT,LFT,KLFT,KLFT,OK,1.0,Lafayette Regional Airport,"Lafayette (LA), USA",OWNO,LAFAYETTE RGNL/PAUL FOURNET FIELD,"LAFAYETTE, LA - UNITED STATES",Lafayette Regional/Paul Fournet Field,"Lafayette, Louisiana, United States",OK
    LFW,////,////,DXXX,UNK,0.261,Lome,"Lome, Togo",OWNO,////,////,Gnassingbé Eyadéma International Airport,"Lomé, Maritime, Togo",OK
    LGA,LGA,KLGA,KLGA,OK,1.0,LaGuardia,"New York (NY), USA",OWNO,LAGUARDIA,"NEW YORK, NY - UNITED STATES",Laguardia Airport,"New York, New York, United States",OK
    LGB,LGB,KLGB,KLGB,OK,0.845,Long Beach Municipal,"Long Beach (CA), USA",OWNO,LONG BEACH /DAUGHERTY FIELD/,"LONG BEACH, CA - UNITED STATES",Long Beach Airport,"Long Beach, California, United States",OK
    LGC,LGC,KLGC,KLGC,OK,0.778,Calloway,"La Grange (GA), USA",OWNO,LAGRANGE-CALLAWAY,"LAGRANGE, GA - UNITED STATES",Lagrange-Callaway Airport,"Lagrange, Georgia, United States",OK
    LGD,LGD,KLGD,KLGD,OK,1.0,La Grande,"La Grande (OR), USA",OWNO,LA GRANDE/UNION COUNTY,"LA GRANDE, OR - UNITED STATES",La Grande/Union County Airport,"La Grande, Oregon, United States",OK
    LGE,////,////,////,UNK,0.4,Lake Gregory,"Lake Gregory, Australia",OWNO,////,////,Mulan Airport,"Mulan, Western Australia, Australia",TO DO CHECK
    LGF,LGF,KLGF,KLGF,OK,1.0,Laguna AAF,"Yuma (AZ), USA",OWNO,LAGUNA AAF (YUMA PROVING GROUND),"YUMA PROVING GROUND(YUMA), AZ - UNITED STATES",Laguna AAF Airport,"Yuma Proving Ground, Arizona, United States",OK
    LGG,////,////,EBLG,UNK,0.556,Bierset,"Liege, Belgium",OWNO,////,////,Liège Airport,"Liège, Liège, Belgium",OK
    LGH,////,////,YLEC,UNK,1.0,Leigh Creek,"Leigh Creek, Australia",OWNO,////,////,????,"Leigh Creek, South Australia, Australia",OK
    LGI,////,MYLD,MYLD,OK,1.0,Deadmans Cay,"Long Island, Bahamas",OWNO,DEADMANS CAY,"DEADMANS CAY, - BAHAMAS",????,"Deadman's Cay, Long Island, Bahamas",OK
    LGK,////,////,WMKL,UNK,0.769,Langkawi,"Langkawi, Malaysia",OWNO,////,////,Langkawi International Airport,"Langkawi, Kedah, Malaysia",OK
    LGL,////,////,WBGF,UNK,1.0,Long Lellang,"Long Lellang, Malaysia",OWNO,////,////,????,"Long Lellang, Sarawak, Malaysia",OK
    LGM,////,////,////,UNK,1.0,Laiagam,"Laiagam, Papua New Guinea",OWNO,////,////,????,"Laiagam, Enga, Papua-New Guinea",OK
    LGN,////,////,////,UNK,1.0,Linga Linga,"Linga Linga, Papua New Guinea",OWNO,////,////,????,"Linga Linga, West New Britain, Papua-New Guinea",OK
    LGO,////,////,EDWL,UNK,1.0,Langeoog,"Langeoog, Germany",OWNO,////,////,????,"Langeoog, Lower Saxony, Germany",OK
    LGP,////,////,RPLP,UNK,1.0,Legaspi,"Legaspi, Philippines",OWNO,////,////,????,"Legazpi, Philippines",OK
    LGQ,////,////,SENL,UNK,1.0,Lago Agrio,"Lago Agrio, Ecuador",OWNO,////,////,Lago Agrio,"Nueva Loja, Sucumbíos, Ecuador",OK
    LGR,////,////,SCHR,UNK,0.7,Cochrane,"Cochrane, Chile",OWNO,////,////,Cochrane Airfield,"Cochrane, Aysén del General Carlos Ibáñez del Campo, Chile",OK
    LGS,////,////,SAMM,UNK,1.0,Malargue,"Malargue, Argentina",OWNO,////,////,????,"Malargüe, Mendoza, Argentina",OK
    LGT,////,////,SKGA,UNK,1.0,Las Gaviotas,"Las Gaviotas, Colombia",OWNO,////,////,Las Gaviotas Airport,"Las Gaviotas, Vichada, Colombia",OK
    LGU,LGU,KLGU,KLGU,OK,1.0,Cache,"Logan (UT), USA",OWNO,LOGAN-CACHE,"LOGAN, UT - UNITED STATES",Logan-Cache Airport,"Logan, Utah, United States",OK
    LGW,////,////,EGKK,UNK,1.0,Gatwick,"London, United Kingdom",OWNO,////,////,Gatwick,"London, Surrey, England, United Kingdom",OK
    LGX,////,////,HCMJ,UNK,1.0,Lugh Ganane,"Lugh Ganane, Somalia",OWNO,////,////,Lugh Ganane Airport,"Luuq, Gedo, Somalia",OK
    LGY,////,////,////,UNK,1.0,Lagunillas,"Lagunillas, Venezuela",OWNO,////,////,????,"Lagunillas, Zulia, Venezuela",OK
    LHA,////,////,EDTL,UNK,0.5,Black Forest Airport,"Lahr, Germany",OWNO,////,////,????,"Lahr, Baden-Württemberg, Germany",OK
    LHE,////,////,OPLA,UNK,1.0,Allama Iqbal International Airport,"Lahore, Pakistan",OWNO,////,////,Allama Iqbal International Airport,"Lahore, Punjab, Pakistan",OK
    LHG,////,////,YLRD,UNK,1.0,Lightning Ridge,"Lightning Ridge, Australia",OWNO,////,////,????,"Lightning Ridge, New South Wales, Australia",OK
    LHI,////,////,WAJL,UNK,1.0,Lereh,"Lereh, Indonesia",OWNO,////,////,????,"Lereh, Papua, Indonesia",OK
    LHK,////,////,ZHGH,UNK,1.0,Guanghua,"Guanghua, PR China",OWNO,////,////,????,"Guanghua, Hubei, China",OK
    LHP,////,////,////,UNK,1.0,Lehu,"Lehu, Papua New Guinea",OWNO,////,////,????,"Lehu, Bougainville, Papua-New Guinea",OK
    LHR,////,////,EGLL,UNK,1.0,Heathrow,"London, United Kingdom",OWNO,////,////,Heathrow,"London, Middlesex, England, United Kingdom",OK
    LHS,////,////,SAVH,UNK,1.0,Las Heras,"Las Heras, Argentina",OWNO,////,////,????,"Las Heras, Santa Cruz, Argentina",OK
    LHV,LHV,KLHV,KLHV,OK,0.885,W T Piper Memorial,"Lock Haven (PA), USA",OWNO,WILLIAM T PIPER MEMORIAL,"LOCK HAVEN, PA - UNITED STATES",William T Piper Memorial Airport,"Lock Haven, Pennsylvania, United States",OK
    LHW,////,////,ZLLL,UNK,0.643,Lanzhou Airport,"Lanzhou, PR China",OWNO,////,////,Lanzhou Zhongchuan International Airport,"Lanzhou, Gansu, China",OK
    LIA,////,////,ZULP,UNK,1.0,Liangping,Liangping,IATA,////,////,????,"Liangping, Sichuan, China",MAYBE
    LIB,////,////,////,UNK,1.0,Limbunya,"Limbunya, Australia",OWNO,////,////,????,"Limbunya, Northern Territory, Australia",OK
    LIC,LIC,KLIC,KLIC,OK,1.0,Municipal,"Limon (CO), USA",OWNO,LIMON MUNI,"LIMON, CO - UNITED STATES",Limon Municipal Airport,"Limon, Colorado, United States",OK
    LIE,////,////,FZFA,UNK,1.0,Libenge,"Libenge, Congo (DRC)",OWNO,////,////,????,"Libenge, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    LIF,////,////,NWWL,UNK,0.6,Lifou,"Lifou, New Caledonia",OWNO,////,////,Ouanaham,"Lifou, Loyalty Islands, New Caledonia",OK
    LIG,////,////,LFBL,UNK,1.0,Bellegarde,"Limoges, France",OWNO,////,////,Bellegarde,"Limoges, Limousin, France",OK
    LIH,LIH,PHLI,PHLI,OK,1.0,Lihue,"Kauai Island (HI), USA",OWNO,LIHUE,"LIHUE, HI - UNITED STATES",Lihue Airport,"Lihue, Kauai, Hawaii, United States",OK
    LII,////,////,WAJM,UNK,1.0,Mulia,"Mulia, Indonesia",OWNO,////,////,????,"Mulia, Papua, Indonesia",OK
    LIK,////,////,////,UNK,1.0,Likiep Island,"Likiep Island, Marshall Islands",OWNO,////,////,????,"Likiep Island, Marshall Islands",OK
    LIL,////,////,LFQQ,UNK,1.0,Lesquin,"Lille, France",OWNO,////,////,Lesquin,"Lille, Nord-Pas-de-Calais, France",OK
    LIM,////,////,SPJC,UNK,0.9,J Chavez International,"Lima, Peru",OWNO,////,////,Jorge Chávez International Airport,"Lima, Callao, Perú",OK
    LIN,////,////,LIML,UNK,1.0,Linate,"Milan, Italy",OWNO,////,////,Linate,"Milan, Lombardy, Italy",OK
    LIO,////,////,MRLM,UNK,0.588,Limon,"Limon, Costa Rica",OWNO,////,////,Puerto Limón International Airport,"Puerto Limón, Limón, Costa Rica",OK
    LIP,////,////,SBLN,UNK,1.0,Lins,"Lins, Brazil",OWNO,////,////,????,"Lins, São Paulo, Brazil",OK
    LIQ,////,////,FZGA,UNK,1.0,Lisala,"Lisala, Congo (DRC)",OWNO,////,////,Lisala Airport,"Lisala, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    LIR,////,////,MRLB,UNK,0.435,Liberia,"Liberia, Costa Rica",OWNO,////,////,Daniel Oduber Quirós International Airport,"Liberia, Guanacaste, Costa Rica",OK
    LIS,////,////,LPPT,UNK,0.458,Lisboa,"Lisbon, Portugal",OWNO,////,////,Aeroporto da Portela de Sacavem,"Lisbon, Lisboa, Portugal",OK
    LIT,LIT,KLIT,KLIT,OK,0.506,Regional Airport,"Little Rock (AR), USA",OWNO,BILL AND HILLARY CLINTON NATIONAL/ADAMS FIELD,"LITTLE ROCK, AR - UNITED STATES",Bill and Hillary Clinton National Airport/Adams Field,"Little Rock, Arkansas, United States",OK
    LIV,4AK,////,////,OK,0.766,Livengood,"Livengood (AK), USA",OWNO,LIVENGOOD CAMP,"LIVENGOOD, AK - UNITED STATES",Livengood Camp Airport,"Livengood, Alaska, United States",OK
    LIW,////,////,VYLK,UNK,1.0,Loikaw,"Loikaw, Myanmar",OWNO,////,////,????,"Loikaw, Kayah, Myanmar (Burma)",OK
    LIX,////,////,FWLK,UNK,1.0,Likoma,"Likoma Island, Malawi",OWNO,////,////,????,"Likoma Island, Malawi",OK
    LIY,LHW,KLHW,KLHW,OK,1.0,Wright AAF,"Hinesville (GA), USA",OWNO,WRIGHT AAF (FORT STEWART)/MIDCOAST RGNL,"FORT STEWART(HINESVILLE), GA - UNITED STATES",Wright AAF (Fort Stewart)/Midcoast Regional,"Fort Stewart, Georgia, United States",OK
    LIZ,ME16,////,////,OK,0.753,Loring AFB,"Limestone (ME), USA",OWNO,LORING INTL,"LIMESTONE, ME - UNITED STATES",Loring International Airport,"Limestone, Maine, United States",OK
    LJA,////,////,FZVA,UNK,1.0,Lodja,"Lodja, Congo (DRC)",OWNO,////,////,Lodja Airport,"Lodja, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    LJG,////,////,ZPLJ,UNK,0.842,Lijiang,"Lijiang City, PR China",OWNO,////,////,Lijiang Sanyi Airport,"Lijiang, Yunnan, China",OK
    LJN,LBX,KLBX,KLBX,OK,0.441,Brazoria County,"Lake Jackson (TX), USA",OWNO,TEXAS GULF COAST RGNL,"ANGLETON/LAKE JACKSON, TX - UNITED STATES",Texas Gulf Coast Regional,"Angleton/Lake Jackson, Texas, United States",OK
    LJU,////,////,LJLJ,UNK,0.563,Brnik,"Ljubljana, Slovenia",OWNO,////,////,Ljubljana Joze Pucnik,"Ljubljana, Slovenia",OK
    LKA,////,////,WATL,UNK,0.696,Larantuka,"Larantuka, Indonesia",OWNO,////,////,Gewayenta Airport,"Larantuka, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    LKB,////,////,NFNK,UNK,0.632,Lakeba,"Lakeba, Fiji",OWNO,////,////,????,"Lakeba Island, Fiji",MAYBE
    LKC,////,////,////,UNK,1.0,Lekana,"Lekana, Congo (ROC)",OWNO,////,////,????,"Lekana, Congo (Republic of)",OK
    LKD,////,////,YLND,UNK,1.0,Lakeland Downs,"Lakeland Downs, Australia",OWNO,////,////,????,"Lakeland Downs, Queensland, Australia",OK
    LKE,W55,////,////,OK,0.483,Lake Union SPB,"Seattle (WA), USA",OWNO,KENMORE AIR HARBOR,"SEATTLE, WA - UNITED STATES",Kenmore Air Harbor SPB,"Seattle, Washington, United States",OK
    LKG,////,////,HKLK,UNK,1.0,Lokichoggio,"Lokichoggio, Kenya",OWNO,////,////,????,"Lokichogio, Kenya",OK
    LKH,////,////,WBGL,UNK,nan,////,////,////,////,////,????,"Long Akah, Sarawak, Malaysia",UNK
    LKK,LKK,PAKL,PAKL,OK,1.0,Kulik Lake,"Kulik Lake (AK), USA",OWNO,KULIK LAKE,"KULIK LAKE, AK - UNITED STATES",????,"Kulik Lake, Alaska, United States",OK
    LKL,////,////,ENNA,UNK,1.0,Banak,"Lakselv, Norway",OWNO,////,////,Banak,"Lakselv, Finnmark, Norway",OK
    LKN,////,////,ENLK,UNK,1.0,Leknes,"Leknes, Norway",OWNO,////,////,????,"Leknes, Nordland, Norway",OK
    LKO,////,////,VILK,UNK,0.667,Amausi,"Lucknow, India",OWNO,////,////,????,"Lucknow, Uttar Pradesh, India",OK
    LKP,LKP,KLKP,KLKP,OK,1.0,Lake Placid,"Lake Placid (NY), USA",OWNO,LAKE PLACID,"LAKE PLACID, NY - UNITED STATES",????,"Lake Placid, New York, United States",OK
    LKR,////,////,////,UNK,1.0,Las Khoreh,"Las Khoreh, Somalia",OWNO,////,////,????,"Las Khoreh, Sanaag, Somalia",OK
    LKV,LKV,KLKV,KLKV,OK,1.0,Lake County,"Lakeview (OR), USA",OWNO,LAKE COUNTY,"LAKEVIEW, OR - UNITED STATES",Lake County Airport,"Lakeview, Oregon, United States",OK
    LKY,////,////,HTLM,UNK,1.0,Lake Manyara,"Lake Manyara, Tanzania",OWNO,////,////,Lake Manyara Airport,"Lake Manyara, Arusha, Tanzania",OK
    LKZ,////,////,EGUL,UNK,1.0,Lakenheath RAF,"Brandon, United Kingdom",OWNO,////,////,RAF Lakenheath,"Lakenheath, Suffolk, England, United Kingdom",MAYBE
    LLA,////,////,ESPA,UNK,1.0,Kallax,"Lulea, Sweden",OWNO,////,////,Kallax,"Luleå, Norrbottens län, Sweden",OK
    LLB,////,////,ZULB,UNK,nan,////,////,////,////,////,Libo Airport,"Libo County, Guizhou, China",UNK
    LLE,////,////,FAMN,UNK,1.0,Malelane,"Malelane, South Africa",OWNO,////,////,Malelane Airport,"Malelane, Mpumalanga, South Africa",OK
    LLF,////,////,ZGLG,UNK,0.8,Lingling,Yongzhou,IATA,////,////,Yongzhou Lingling Airport,"Yongzhou, Hunan, China",MAYBE
    LLG,////,////,YCGO,UNK,1.0,Chillagoe,"Chillagoe, Australia",OWNO,////,////,????,"Chillagoe, Queensland, Australia",OK
    LLH,////,////,////,UNK,1.0,Las Limas,"Las Limas, Honduras",OWNO,////,////,????,"Las Limas, Cortés, Honduras",OK
    LLI,////,////,HALL,UNK,1.0,Lalibela,"Lalibela, Ethiopia",OWNO,////,////,????,"Lalibela, Amara, Ethiopia",OK
    LLJ,////,////,VGLM,UNK,nan,////,////,////,////,////,????,"Lalmonirhat, Bangladesh",UNK
    LLK,////,////,UBBL,UNK,nan,////,////,////,////,////,Lankaran International Airport,"Lankaran, Lenkeran (Lankaran), Azerbaijan",UNK
    LLL,////,////,////,UNK,1.0,Lissadell,"Lissadell, Australia",OWNO,////,////,????,"Lissadell, Western Australia, Australia",OK
    LLN,////,////,////,UNK,1.0,Kelila,"Kelila, Indonesia",OWNO,////,////,????,"Kelila, Papua, Indonesia",OK
    LLP,////,////,////,UNK,1.0,Linda Downs,"Linda Downs, Australia",OWNO,////,////,????,"Linda Downs, Queensland, Australia",OK
    LLS,////,////,SATK,UNK,0.458,Las Lomitas,"Las Lomitas, Argentina",OWNO,////,////,Alferez Armando Rodriguez,"Las Lomitas, Formosa, Argentina",OK
    LLU,////,////,BGAP,UNK,1.0,Alluitsup Paa,"Alluitsup Paa, Greenland",OWNO,////,////,????,"Alluitsup Paa, Kujalleq, Greenland",OK
    LLV,////,////,ZBLL,UNK,1.0,Luliang,Luliang,IATA,////,////,Lüliang Airport,"Lüliang, Shaanxi, China",MAYBE
    LLW,////,////,FWKI,UNK,0.788,Lilongwe International,"Lilongwe, Malawi",OWNO,////,////,Kumuzu International Airport,"Lilongwe, Malawi",OK
    LLX,CDA,KCDA,KCDA,OK,0.506,Lyndonville,"Lyndonville (VT), USA",OWNO,CALEDONIA COUNTY,"LYNDONVILLE, VT - UNITED STATES",Caledonia County Airport,"Lyndonville, Vermont, United States",OK
    LLY,VAY,KVAY,KVAY,OK,0.321,Burlington County,"Mount Holly (NJ), USA",OWNO,SOUTH JERSEY RGNL,"MOUNT HOLLY, NJ - UNITED STATES",South Jersey Regional,"Mount Holly, New Jersey, United States",OK
    LMA,MHM,PAMH,////,NG,0.852,Lake Minchumina,"Lake Minchumina (AK), USA",OWNO,MINCHUMINA,"MINCHUMINA, AK - UNITED STATES",????,"Lake Minchumina, Alaska, United States",OK
    LMB,////,////,FWSM,UNK,1.0,Salima,"Salima, Malawi",OWNO,////,////,????,"Salima, Malawi",OK
    LMC,////,////,////,UNK,1.0,Lamacarena,"Lamacarena, Colombia",OWNO,////,////,????,"La Macarena, Meta, Colombia",OK
    LMD,////,////,////,UNK,1.0,Los Menucos,"Los Menucos, Argentina",OWNO,////,////,????,"Los Menucos, Río Negro, Argentina",OK
    LME,////,////,LFRM,UNK,1.0,Arnage,"Le Mans, France",OWNO,////,////,Arnage,"Le Mans, Pays de la Loire, France",OK
    LMG,////,////,////,UNK,1.0,Lamassa,"Lamassa, Papua New Guinea",OWNO,////,////,????,"Lamassa, New Ireland, Papua-New Guinea",OK
    LMH,////,////,////,UNK,1.0,Limon,"Limon, Honduras",OWNO,////,////,????,"Limón, Colón, Honduras",OK
    LMI,////,////,////,UNK,1.0,Lumi,"Lumi, Papua New Guinea",OWNO,////,////,????,"Lumi, Sandaun, Papua-New Guinea",OK
    LML,////,////,////,UNK,1.0,Lae Island,"Lae Island, Marshall Islands",OWNO,////,////,????,"Lae Island, Marshall Islands",OK
    LMM,////,////,MMLM,UNK,0.621,Federal,"Los Mochis, Mexico",OWNO,////,////,Federal del Valle del Fuerte International Airport,"Los Mochis, Sinaloa, México",OK
    LMN,////,////,WBGJ,UNK,1.0,Limbang,"Limbang, Malaysia",OWNO,////,////,????,"Limbang, Sarawak, Malaysia",OK
    LMO,////,////,EGQS,UNK,0.789,RAF Station,"Lossiemouth, United Kingdom",OWNO,////,////,RAF,"Lossiemouth, Morayshire, Scotland, United Kingdom",OK
    LMP,////,////,LICD,UNK,1.0,Lampedusa,"Lampedusa, Italy",OWNO,////,////,????,"Lampedusa, Sicily, Italy",OK
    LMQ,////,////,HLMB,UNK,1.0,Marsa Brega,"Marsa Brega, Libya",OWNO,////,////,????,"Marsa Brega, Libyan Arab Jamahiriya (Libya)",OK
    LMR,////,////,FALC,UNK,0.667,Lime Acres,"Lime Acres, South Africa",OWNO,////,////,Finsch Mine Airport,"Lime Acres, Northern Cape, South Africa",OK
    LMS,LMS,KLMS,KLMS,OK,1.0,Winston County,"Louisville (MS), USA",OWNO,LOUISVILLE WINSTON COUNTY,"LOUISVILLE, MS - UNITED STATES",Louisville Winston County Airport,"Louisville, Mississippi, United States",OK
    LMT,LMT,KLMT,KLMT,OK,0.315,Kingsley Field,"Klamath Falls (OR), USA",OWNO,CRATER LAKE-KLAMATH RGNL,"KLAMATH FALLS, OR - UNITED STATES",Crater Lake-Klamath Regional,"Klamath Falls, Oregon, United States",OK
    LMX,////,////,////,UNK,1.0,Lopez De Micay,"Lopez De Micay, Colombia",OWNO,////,////,López de Micay Airport,"El Trapiche, Cauca, Colombia",OK
    LMY,////,////,////,UNK,1.0,Lake Murray,"Lake Murray, Papua New Guinea",OWNO,////,////,????,"Lake Murray, Western, Papua-New Guinea",OK
    LMZ,////,////,////,UNK,1.0,Palma,"Palma, Mozambique",OWNO,////,////,????,"Palma, Mozambique",OK
    LNA,LNA,KLNA,KLNA,OK,0.852,Palm Beach County,"West Palm Beach (FL), USA",OWNO,PALM BEACH COUNTY PARK,"WEST PALM BEACH, FL - UNITED STATES",Palm Beach County Park Airport,"West Palm Beach, Florida, United States",OK
    LNB,////,////,NVSM,UNK,1.0,Lamen Bay,"Lamen Bay, Vanuatu",OWNO,////,////,????,"Lamen Bay, Épi Island, Shéfa, Vanuatu",OK
    LNC,////,////,////,UNK,1.0,Lengbati,"Lengbati, Papua New Guinea",OWNO,////,////,????,"Lengbati, Morobe, Papua-New Guinea",OK

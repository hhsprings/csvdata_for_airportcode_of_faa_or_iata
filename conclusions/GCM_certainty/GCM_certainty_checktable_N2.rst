
List for checking certainty of Great Circle Mapper (NIP - NSB)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    NIP,NIP,KNIP,KNIP,OK,1.0,Jacksonville NAS,"Jacksonville (FL), USA",OWNO,JACKSONVILLE NAS (TOWERS FLD),"JACKSONVILLE, FL - UNITED STATES",NAS Jacksonville,"Jacksonville, Florida, United States",OK
    NIS,////,////,////,UNK,1.0,Simberi,"Simberi Island, Papua New Guinea",OWNO,////,////,????,"Simberi Island, New Ireland, Papua-New Guinea",OK
    NIT,////,////,LFBN,UNK,0.588,Niort,"Niort, France",OWNO,////,////,Souche,"Niort, Poitou-Charentes, France",OK
    NIU,////,////,NTKN,UNK,nan,////,////,////,////,////,Niau Airport,"Niau, French Polynesia",UNK
    NIX,////,////,GANR,UNK,0.526,Nioro,"Nioro, Mali",OWNO,////,////,????,"Nioro du Sahel, Kayes, Mali",MAYBE
    NJA,////,////,RJTA,UNK,0.9,NAS,"Atsugi, Japan",OWNO,////,////,NAF Atsugi,"Atsugi, Kanagawa, Japan",OK
    NJC,////,////,USNN,UNK,1.0,Nizhnevartovsk,"Nizhnevartovsk, Russia",OWNO,////,////,Nizhnevartovsk Airport,"Nizhnevartovsk, Tyumenskaya, Russian Federation (Russia)",OK
    NJF,////,////,ORNI,UNK,1.0,Al-Ashraf Intl.,"Al Najaf, Iraq",OWNO,////,////,Al-Ashraf International Airport,"Al Najaf, Iraq",OK
    NJK,NJK,KNJK,KNJK,OK,0.748,NAF,"Imperial (CA), USA",OWNO,EL CENTRO NAF,"EL CENTRO, CA - UNITED STATES",NAF El Centro,"El Centro, California, United States",OK
    NKA,////,////,////,UNK,1.0,Nkan,"Nkan, Gabon",OWNO,////,////,????,"Nkan, Estuaire, Gabon",OK
    NKB,////,////,YNKA,UNK,1.0,Noonkanbah,"Noonkanbah, Australia",OWNO,////,////,????,"Noonkanbah, Western Australia, Australia",OK
    NKC,////,////,GQNN,UNK,0.8,Nouakchott,"Nouakchott, Mauritania",OWNO,////,////,Nouakchott International Airport,"Nouakchott, Mauritania",OK
    NKD,////,////,////,UNK,1.0,Sinak,"Sinak, Indonesia",OWNO,////,////,????,"Sinak, Papua, Indonesia",OK
    NKG,////,////,ZSNJ,UNK,0.765,Nanking/Nanjing,"Nanking/Nanjing, PR China",OWNO,////,////,Nanjing Lukou International Airport,"Nanjing, Jiangsu, China",OK
    NKI,AK62,////,////,OK,0.683,Naukiti,"Naukiti (AK), USA",OWNO,NAUKATI BAY,"TUXEKAN ISLAND, AK - UNITED STATES",Naukati Bay SPB,"Tuxekan Island, Alaska, United States",OK
    NKL,////,////,FZAR,UNK,0.667,Nkolo,"Nkolo, Congo (DRC)",OWNO,////,////,????,"Nkolo Fuma, Bas-Congo, Democratic Republic of Congo (Zaire)",MAYBE
    NKM,////,////,RJNA,UNK,nan,////,////,////,////,////,Nagoya Airport / Komaki AB,"Nagoya, Aichi, Japan",UNK
    NKN,////,////,////,UNK,1.0,Nankina,"Nankina, Papua New Guinea",OWNO,////,////,????,"Nankina, Madang, Papua-New Guinea",OK
    NKS,////,////,FKAN,UNK,1.0,Nkongsamba,"Nkongsamba, Cameroon",OWNO,////,////,????,"Nkongsamba, Littoral, Cameroon",OK
    NKT,////,////,LTCV,UNK,nan,////,////,////,////,////,Sirnak Airport,"Sirnak, Turkey",UNK
    NKU,////,////,FXNK,UNK,1.0,Nkaus,"Nkaus, Lesotho",OWNO,////,////,????,"Nkaus, Lesotho",OK
    NKW,////,////,FJDG,UNK,nan,////,////,////,////,////,????,"Diego Garcia, Chagos Archipelago, British Indian Ocean Territory",UNK
    NKX,NKX,KNKX,KNKX,OK,0.783,Miramar NAS,"San Diego (CA), USA",OWNO,MIRAMAR MCAS,"SAN DIEGO, CA - UNITED STATES",Miramar MCAS,"San Diego, California, United States",OK
    NKY,////,////,FCBY,UNK,0.462,Nkayi,"Nkayi, Congo (ROC)",OWNO,////,////,Yokangassi Airport,"N'Kayi, Congo (Republic of)",OK
    NLA,////,////,FLSK,UNK,0.263,Ndola,"Ndola, Zambia",OWNO,////,////,Simon Mwansa Kapwepwe International Airport,"N'Dola, Copperbelt, Zambia",OK
    NLC,NLC,KNLC,KNLC,OK,1.0,NAS/Reeves Field,"Lemoore (CA), USA",OWNO,LEMOORE NAS (REEVES FLD),"LEMOORE, CA - UNITED STATES",NAS Lemoore,"Lemoore, California, United States",OK
    NLD,////,////,MMNL,UNK,1.0,International Quetzalcoatl,"Nuevo Laredo, Mexico",OWNO,////,////,Quetzalcóatl International Airport,"Nuevo Laredo, Tamaulipas, México",OK
    NLE,3TR,////,////,OK,1.0,Jerry Tyler Memorial,"Niles (MI), USA",OWNO,JERRY TYLER MEMORIAL,"NILES, MI - UNITED STATES",Jerry Tyler Memorial Airport,"Niles, Michigan, United States",OK
    NLF,////,////,YDNI,UNK,1.0,Darnley Island,"Darnley Island, Australia",OWNO,////,////,????,"Darnley Island, Queensland, Australia",OK
    NLG,OUL,PAOU,PAOU,OK,1.0,Nelson Lagoon,"Nelson Lagoon (AK), USA",OWNO,NELSON LAGOON,"NELSON LAGOON, AK - UNITED STATES",????,"Nelson Lagoon, Alaska, United States",OK
    NLH,////,////,////,UNK,0.571,Luguhu,Ninglang,IATA,////,////,Ninglang Luguhu Airport,"Ninglang, Yunnan, China",MAYBE
    NLK,////,////,YSNF,UNK,1.0,Norfolk Island,"Norfolk Island, Norfolk Island",OWNO,////,////,????,"Norfolk Island, Norfolk Island",OK
    NLL,////,////,YNUL,UNK,1.0,Nullagine,"Nullagine, Australia",OWNO,////,////,????,"Nullagine, Western Australia, Australia",OK
    NLO,////,////,FZAB,UNK,1.0,N'Dolo Airport,"Kinshasa, Congo (DRC)",OWNO,////,////,N'Dolo Airport,"Kinshasa, Kinshasa, Democratic Republic of Congo (Zaire)",OK
    NLP,////,////,FANS,UNK,nan,////,////,////,////,////,????,"Nelspruit, Mpumalanga, South Africa",UNK
    NLS,////,////,YNIC,UNK,1.0,Nicholson,"Nicholson, Australia",OWNO,////,////,????,"Nicholson, Western Australia, Australia",OK
    NLT,////,////,ZWNL,UNK,nan,////,////,////,////,////,Xinyuan Nalati Airport,"Xinyuan, Xinjiang, China",UNK
    NLU,////,////,MMSM,UNK,0.625,Santa Lucia Airport at Santa María Ajoloapan,"Mexico City, Mexico",OWNO,////,////,Santa Lucia AFB,"Zumpango, México, México",MAYBE
    NLV,////,////,UKON,UNK,0.778,Nikolaev,"Nikolaev, Ukraine",OWNO,////,////,Nikolaev International Airport,"Nikolaev, Mykolaiv, Ukraine",OK
    NMA,////,////,UTKN,UNK,1.0,Namangan,"Namangan, Uzbekistan",OWNO,////,////,Namangan Airport,"Namangan, Namangan, Uzbekistan",OK
    NMB,////,////,VADN,UNK,1.0,Daman,"Daman, India",OWNO,////,////,????,"Daman, Daman and Diu, India",OK
    NMC,////,////,MYEN,UNK,1.0,Norman's Cay,"Norman's Cay, Bahamas",OWNO,////,////,????,"Norman's Cay, Exuma Islands, Bahamas",OK
    NME,IGT,PAGT,PAGT,OK,1.0,Nightmute,"Nightmute (AK), USA",OWNO,NIGHTMUTE,"NIGHTMUTE, AK - UNITED STATES",????,"Nightmute, Alaska, United States",OK
    NMG,////,////,////,UNK,1.0,San Miguel,"San Miguel, Panama",OWNO,////,////,????,"San Miguel, Panamá, Panamá",OK
    NMN,////,////,////,UNK,1.0,Nomane,"Nomane, Papua New Guinea",OWNO,////,////,????,"Nomane, Chimbu, Papua-New Guinea",OK
    NMP,////,////,YNMN,UNK,1.0,New Moon,"New Moon, Australia",OWNO,////,////,New Moon Airport,"New Moon, Queensland, Australia",OK
    NMR,////,////,YNAP,UNK,1.0,Nappa Merry,"Nappa Merry, Australia",OWNO,////,////,Nappa Merrie Airport,"Nappa Merrie, Queensland, Australia",OK
    NMS,////,////,VYNS,UNK,1.0,Namsang,"Namsang, Myanmar",OWNO,////,////,????,"Namsang, Shan, Myanmar (Burma)",OK
    NMT,////,////,VYNT,UNK,1.0,Namtu,"Namtu, Myanmar",OWNO,////,////,????,"Namtu, Shan, Myanmar (Burma)",OK
    NNA,////,////,GMMY,UNK,0.457,NAF,"Kenitra, Morocco",OWNO,////,////,Third Royal Air Force Base,"Kenitra, Morocco",OK
    NNB,////,////,AGGT,UNK,1.0,Santa Ana,"Santa Ana, Solomon Islands",OWNO,////,////,Santa Ana Airport,"Santa Ana Island, Solomon Islands",OK
    NND,////,////,////,UNK,1.0,Nangade,"Nangade, Mozambique",OWNO,////,////,????,"Nangade, Mozambique",OK
    NNG,////,////,ZGNN,UNK,0.8,Nanning,"Nanning, PR China",OWNO,////,////,Wuxu International Airport,"Nanning, Guangxi, China",OK
    NNI,////,////,FYNA,UNK,1.0,Namutoni,"Namutoni, Namibia",OWNO,////,////,????,"Namutoni, Namibia",OK
    NNK,5NK,////,////,OK,1.0,Naknek,"Naknek (AK), USA",OWNO,NAKNEK,"NAKNEK, AK - UNITED STATES",????,"Naknek, Alaska, United States",OK
    NNL,5NN,PANO,PANO,OK,1.0,Nondalton,"Nondalton (AK), USA",OWNO,NONDALTON,"NONDALTON, AK - UNITED STATES",????,"Nondalton, Alaska, United States",OK
    NNM,////,////,ULAM,UNK,1.0,Naryan-Mar,"Naryan-Mar, Russia",OWNO,////,////,Naryan-Mar Airport,"Naryan-Mar, Nenetskiy, Russian Federation (Russia)",OK
    NNR,////,////,EICA,UNK,1.0,Connemara,"Spiddal, Ireland",OWNO,////,////,Connemara,"Inverin, County Galway, Connacht, Ireland",OK
    NNT,////,////,VTCN,UNK,1.0,Nan,"Nan, Thailand",OWNO,////,////,????,"Nan, Nan, Thailand",OK
    NNU,////,////,SNNU,UNK,1.0,Nanuque,"Nanuque, Brazil",OWNO,////,////,????,"Nanuque, Minas Gerais, Brazil",OK
    NNX,////,////,WALF,UNK,1.0,Nunukan,"Nunukan, Indonesia",OWNO,////,////,Nunukan Airport,"Nunukan, Kalimantan Utara (North Borneo), Indonesia",OK
    NNY,////,////,ZHNY,UNK,1.0,Nanyang,"Nanyang, PR China",OWNO,////,////,????,"Nanyang, Henan, China",OK
    NOA,////,////,YSNW,UNK,1.0,Nowra,"Nowra, Australia",OWNO,////,////,????,"Nowra, New South Wales, Australia",OK
    NOB,////,////,MRNS,UNK,1.0,Nosara Beach,"Nosara Beach, Costa Rica",OWNO,////,////,????,"Nosara Beach, Guanacaste, Costa Rica",OK
    NOC,////,////,EIKN,UNK,0.571,Knock International,"Knock, Ireland",OWNO,////,////,Ireland West Airport Knock,"Knock, County Mayo, Connacht, Ireland",OK
    NOD,////,////,////,UNK,1.0,Norden,"Norden, Germany",OWNO,////,////,????,"Norden, Lower Saxony, Germany",OK
    NOE,////,////,EDWS,UNK,0.538,Norddeich,"Norddeich, Germany",OWNO,////,////,Norden-Norddeich Airfield,"Norddeich, Lower Saxony, Germany",OK
    NOG,////,////,MMNG,UNK,0.737,Nogales,"Nogales, Mexico",OWNO,////,////,Nogales International Airport,"Nogales, Sonora, México",OK
    NOI,////,////,////,UNK,0.688,Novorossijsk,"Novorossijsk, Russia",OWNO,////,////,Krymska Airport,"Novorossiysk, Krasnodarskiy, Russian Federation (Russia)",OK
    NOJ,////,////,USRO,UNK,0.824,Nojabrxsk,"Nojabrxsk, Russia",OWNO,////,////,Noyabrsk Airport,"Noyabrsk, Yamalo-Nenetskiy, Russian Federation (Russia)",TO DO CHECK
    NOK,////,////,SWXV,UNK,1.0,Nova Xavantina,"Nova Xavantina, Brazil",OWNO,////,////,????,"Nova Xavantina, Mato Grosso, Brazil",OK
    NOM,////,////,////,UNK,1.0,Nomad River,"Nomad River, Papua New Guinea",OWNO,////,////,????,"Nomad River, Western, Papua-New Guinea",OK
    NON,////,////,NGTO,UNK,1.0,Nonouti,"Nonouti, Kiribati",OWNO,////,////,????,"Nonouti, Kiribati",OK
    NOO,////,////,////,UNK,1.0,Naoro,"Naoro, Papua New Guinea",OWNO,////,////,????,"Naoro, Central, Papua-New Guinea",OK
    NOP,////,////,LTCM,UNK,1.0,Sinop,Sinop,IATA,////,////,Sinop Airport,"Sinop, Turkey",MAYBE
    NOR,////,////,BINF,UNK,1.0,Nordfjordur,"Nordfjordur, Iceland",OWNO,////,////,????,"Norðfjörður, Iceland",OK
    NOS,////,////,FMNN,UNK,1.0,Fascene,"Nossi-be, Madagascar",OWNO,////,////,Fascène,"Nosy-Bé, Madagascar",OK
    NOT,DVO,KDVO,KDVO,OK,0.64,Novato,"Novato (CA), USA",OWNO,GNOSS FIELD,"NOVATO, CA - UNITED STATES",Gnoss Field,"Novato, California, United States",OK
    NOU,////,////,NWWW,UNK,0.8,Tontouta,"Noumea, New Caledonia",OWNO,////,////,La Tontouta International Airport,"Nouméa, New Caledonia",OK
    NOV,////,////,FNHU,UNK,0.444,Huambo,"Huambo, Angola",OWNO,////,////,Albano Machado,"Huambo, Angola",OK
    NOZ,////,////,UNWW,UNK,0.727,Novokuznetsk,"Novokuznetsk, Russia",OWNO,////,////,Spichenkovo Airport,"Novokuznetsk, Kemerovskaya, Russian Federation (Russia)",OK
    NPA,NPA,KNPA,KNPA,OK,1.0,Pensacola NAS,"Pensacola (FL), USA",OWNO,PENSACOLA NAS/FORREST SHERMAN FIELD,"PENSACOLA, FL - UNITED STATES",Pensacola NAS/Forrest Sherman Field,"Pensacola, Florida, United States",OK
    NPE,////,////,NZNR,UNK,0.774,Hawkes Bay,"Napier-Hastings, New Zealand",OWNO,////,////,????,"Napier/Hastings, New Zealand",TO DO CHECK
    NPG,////,////,////,UNK,1.0,Nipa,"Nipa, Papua New Guinea",OWNO,////,////,????,"Nipa, Southern Highlands, Papua-New Guinea",OK
    NPH,U14,////,////,OK,0.64,Nephi,"Nephi (UT), USA",OWNO,NEPHI MUNI,"NEPHI, UT - UNITED STATES",Nephi Municipal Airport,"Nephi, Utah, United States",OK
    NPL,////,////,NZNP,UNK,1.0,New Plymouth Airport,"New Plymouth, New Zealand",OWNO,////,////,????,"New Plymouth, New Zealand",OK
    NPO,////,////,WIOG,UNK,1.0,Nangapinoh,"Nangapinoh, Indonesia",OWNO,////,////,????,"Nangapinoh, Kalimantan (Borneo), Kalimantan Barat (West Borneo), Indonesia",OK
    NPP,////,////,YNPB,UNK,1.0,Napperby,"Napperby, Australia",OWNO,////,////,????,"Napperby, Northern Territory, Australia",OK
    NPR,////,////,SJNP,UNK,nan,////,////,////,////,////,Aeropuerto Novo Progresso,"Novo Progresso, Pará, Brazil",UNK
    NPT,UUU,KUUU,KUUU,OK,1.0,State,"Newport (RI), USA",OWNO,NEWPORT STATE,"NEWPORT, RI - UNITED STATES",Newport State Airport,"Newport, Rhode Island, United States",OK
    NQA,NQA,KNQA,KNQA,OK,0.269,NAS,"Memphis (TN), USA",OWNO,MILLINGTON RGNL JETPORT,"MILLINGTON, TN - UNITED STATES",Millington Regional Jetport,"Millington, Tennessee, United States",OK
    NQI,NQI,KNQI,KNQI,OK,1.0,NAS,"Kingsville (TX), USA",OWNO,KINGSVILLE NAS,"KINGSVILLE, TX - UNITED STATES",NAS Kingsville,"Kingsville, Texas, United States",OK
    NQL,////,////,SWNQ,UNK,1.0,Niquelandia,"Niquelandia, Brazil",OWNO,////,////,????,"Niquelândia, Goiás, Brazil",OK
    NQN,////,////,SAZN,UNK,0.452,Neuquen,"Neuquen, Argentina",OWNO,////,////,Presidente Perón,"Neuquén, Neuquén, Argentina",OK
    NQT,////,////,EGBN,UNK,0.8,Nottingham Airport,"Nottingham, United Kingdom",OWNO,////,////,Nottingham City Airport,"Nottingham, Nottinghamshire, England, United Kingdom",OK
    NQU,////,////,SKNQ,UNK,0.417,Nuqui,"Nuqui, Colombia",OWNO,////,////,Reyes Murillo Airport,"Nuquí, Chocó, Colombia",OK
    NQY,////,////,EGHQ,UNK,0.609,St Mawgan,"Newquay, United Kingdom",OWNO,////,////,Newquay Cornwall Airport,"Newquay, Cornwall, England, United Kingdom",OK
    NRA,////,////,YNAR,UNK,1.0,Narrandera,"Narrandera, Australia",OWNO,////,////,????,"Narrandera, New South Wales, Australia",OK
    NRB,NRB,KNRB,KNRB,OK,1.0,Ns,"Mayport (FL), USA",OWNO,MAYPORT NS (ADM DAVID L MCDONALD FIELD),"MAYPORT, FL - UNITED STATES",Mayport NS Airport,"Mayport, Florida, United States",OK
    NRD,////,////,EDWY,UNK,1.0,Norderney,"Norderney, Germany",OWNO,////,////,????,"Norderney, Lower Saxony, Germany",OK
    NRE,////,////,WAPG,UNK,1.0,Namrole,"Namrole, Indonesia",OWNO,////,////,Namrole Airport,"Namrole, Buru Island, Maluku, Indonesia",OK
    NRG,////,////,YNRG,UNK,1.0,Narrogin,"Narrogin, Australia",OWNO,////,////,????,"Narrogin, Western Australia, Australia",OK
    NRK,////,////,ESSP,UNK,1.0,Kungsangen,"Norrkoping, Sweden",OWNO,////,////,Kungsängen,"Norrköping, Östergötlands län, Sweden",OK
    NRL,////,////,EGEN,UNK,1.0,North Ronaldsay,"North Ronaldsay, United Kingdom",OWNO,////,////,????,"North Ronaldsay, Orkney Isles, Scotland, United Kingdom",OK
    NRM,////,////,GANK,UNK,0.5,Nara,"Nara, Mali",OWNO,////,////,Keibane,"Nara, Bamako, Mali",OK
    NRN,////,////,EDLV,UNK,1.0,Weeze Airport (Niederrhein Airport),"Weeze, North Rhine-Westphalia, Germany",OWNO,////,////,????,"Niederrhein, North Rhine-Westphalia, Germany",OK
    NRS,NRS,KNRS,KNRS,OK,0.865,NAS,"Imperial Beach (CA), USA",OWNO,IMPERIAL BEACH NOLF (REAM FLD),"IMPERIAL BEACH, CA - UNITED STATES",Imperial Beach NOLF Airport,"Imperial Beach, California, United States",OK
    NRT,////,////,RJAA,UNK,0.667,Narita,"Tokyo, Japan",OWNO,////,////,New Tokyo International Airport,"Tokyo, Chiba, Japan",OK
    NRY,////,////,////,UNK,1.0,Newry,"Newry, Australia",OWNO,////,////,????,"Newry, Northern Territory, Australia",OK
    NSB,////,////,////,UNK,1.0,North SPB,"Bimini, Bahamas",OWNO,////,////,North SPB,"Bimini, Bimini Island, Bahamas",OK


List for checking certainty of Great Circle Mapper (FAA LID TAZ - FAA LID ZUN)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,TAZ,KTAZ,KTAZ,OK,1.0,////,////,////,TAYLORVILLE MUNI,"TAYLORVILLE, IL - UNITED STATES",Taylorville Municipal Airport,"Taylorville, Illinois, United States",OK
    ////,TCY,KTCY,KTCY,OK,1.0,////,////,////,TRACY MUNI,"TRACY, CA - UNITED STATES",Tracy Municipal Airport,"Tracy, California, United States",OK
    ////,TDF,KTDF,KTDF,OK,1.0,////,////,////,PERSON COUNTY,"ROXBORO, NC - UNITED STATES",Person County Airport,"Roxboro, North Carolina, United States",OK
    ////,TEW,KTEW,KTEW,OK,1.0,////,////,////,MASON JEWETT FIELD,"MASON, MI - UNITED STATES",Mason Jewett Field,"Mason, Michigan, United States",OK
    ////,TFP,KTFP,KTFP,OK,1.0,////,////,////,MCCAMPBELL-PORTER,"INGLESIDE, TX - UNITED STATES",McCampbell-Porter Airport,"Ingleside, Texas, United States",OK
    ////,TGI,KTGI,KTGI,OK,1.0,////,////,////,TANGIER ISLAND,"TANGIER, VA - UNITED STATES",Tangier Island Airport,"Tangier, Virginia, United States",OK
    ////,THM,KTHM,KTHM,OK,1.0,////,////,////,THOMPSON FALLS,"THOMPSON FALLS, MT - UNITED STATES",????,"Thompson Falls, Montana, United States",OK
    ////,TIF,KTIF,KTIF,OK,1.0,////,////,////,THOMAS COUNTY,"THEDFORD, NE - UNITED STATES",Thomas County Airport,"Thedford, Nebraska, United States",OK
    ////,TIP,KTIP,KTIP,OK,0.943,////,////,////,RANTOUL NATL AVN CNTR-FRANK ELLIOTT FLD,"RANTOUL, IL - UNITED STATES",Rantoul National Avn Center-Frank Elliott Field,"Rantoul, Illinois, United States",OK
    ////,TKC,KTKC,KTKC,OK,1.0,////,////,////,TRACY MUNI,"TRACY, MN - UNITED STATES",Tracy Municipal Airport,"Tracy, Minnesota, United States",OK
    ////,TKI,KTKI,KTKI,OK,1.0,////,////,////,MCKINNEY NATIONAL,"DALLAS, TX - UNITED STATES",McKinney National Airport,"Dallas, Texas, United States",OK
    ////,TKV,KTKV,KTKV,OK,1.0,////,////,////,TOMAHAWK RGNL,"TOMAHAWK, WI - UNITED STATES",Tomahawk Regional,"Tomahawk, Wisconsin, United States",OK
    ////,TLJ,PATL,PATL,OK,1.0,////,////,////,TATALINA LRRS,"TAKOTNA, AK - UNITED STATES",Tatalina LRRS Airport,"Takotna, Alaska, United States",OK
    ////,TME,KTME,KTME,OK,1.0,////,////,////,HOUSTON EXECUTIVE,"HOUSTON, TX - UNITED STATES",Houston Executive Airport,"Houston, Texas, United States",OK
    ////,TMK,KTMK,KTMK,OK,1.0,////,////,////,TILLAMOOK,"TILLAMOOK, OR - UNITED STATES",????,"Tillamook, Oregon, United States",OK
    ////,TOB,KTOB,KTOB,OK,1.0,////,////,////,DODGE CENTER,"DODGE CENTER, MN - UNITED STATES",????,"Dodge Center, Minnesota, United States",OK
    ////,TQE,KTQE,KTQE,OK,1.0,////,////,////,TEKAMAH MUNI,"TEKAMAH, NE - UNITED STATES",Tekamah Municipal Airport,"Tekamah, Nebraska, United States",OK
    ////,TQH,KTQH,KTQH,OK,1.0,////,////,////,TAHLEQUAH MUNI,"TAHLEQUAH, OK - UNITED STATES",Tahlequah Municipal Airport,"Tahlequah, Oklahoma, United States",OK
    ////,TTA,KTTA,KTTA,OK,1.0,////,////,////,RALEIGH EXEC JETPORT AT SANFORD-LEE COUNTY,"SANFORD, NC - UNITED STATES",Raleigh Exec Jetport Airport at Sanford-Lee County,"Sanford, North Carolina, United States",OK
    ////,TTF,KTTF,KTTF,OK,1.0,////,////,////,CUSTER,"MONROE, MI - UNITED STATES",Custer Airport,"Monroe, Michigan, United States",OK
    ////,TTS,KTTS,KTTS,OK,1.0,////,////,////,NASA SHUTTLE LANDING FACILITY,"TITUSVILLE, FL - UNITED STATES",NASA Shuttle Landing Facility Airport,"Titusville, Florida, United States",OK
    ////,TTW,PATW,PATW,OK,1.0,////,////,////,CANTWELL,"CANTWELL, AK - UNITED STATES",????,"Cantwell, Alaska, United States",OK
    ////,TVK,KTVK,KTVK,OK,1.0,////,////,////,CENTERVILLE MUNI,"CENTERVILLE, IA - UNITED STATES",Centerville Municipal Airport,"Centerville, Iowa, United States",OK
    ////,TVR,KTVR,KTVR,OK,1.0,////,////,////,VICKSBURG TALLULAH RGNL,"TALLULAH, LA - UNITED STATES",Vicksburg Tallulah Regional,"Tallulah, Louisiana, United States",OK
    ////,TVY,KTVY,KTVY,OK,1.0,////,////,////,BOLINDER FIELD-TOOELE VALLEY,"TOOELE, UT - UNITED STATES",Bolinder Field-Tooele Valley Airport,"Tooele, Utah, United States",OK
    ////,TWM,KTWM,KTWM,OK,1.0,////,////,////,RICHARD B HELGESON,"TWO HARBORS, MN - UNITED STATES",Richard B Helgeson Airport,"Two Harbors, Minnesota, United States",OK
    ////,TYQ,KTYQ,KTYQ,OK,1.0,////,////,////,INDIANAPOLIS EXECUTIVE,"INDIANAPOLIS, IN - UNITED STATES",Indianapolis Executive Airport,"Indianapolis, Indiana, United States",OK
    ////,TZR,KTZR,KTZR,OK,1.0,////,////,////,BOLTON FIELD,"COLUMBUS, OH - UNITED STATES",Bolton Field,"Columbus, Ohio, United States",OK
    ////,U76,////,////,OK,1.0,////,////,////,MOUNTAIN HOME MUNI,"MOUNTAIN HOME, ID - UNITED STATES",Mountain Home Municipal Airport,"Mountain Home, Idaho, United States",OK
    ////,UAO,KUAO,KUAO,OK,1.0,////,////,////,AURORA STATE,"AURORA, OR - UNITED STATES",Aurora State Airport,"Aurora, Oregon, United States",OK
    ////,UBE,KUBE,KUBE,OK,1.0,////,////,////,CUMBERLAND MUNI,"CUMBERLAND, WI - UNITED STATES",Cumberland Municipal Airport,"Cumberland, Wisconsin, United States",OK
    ////,UCP,KUCP,KUCP,OK,1.0,////,////,////,NEW CASTLE MUNI,"NEW CASTLE, PA - UNITED STATES",New Castle Municipal Airport,"New Castle, Pennsylvania, United States",OK
    ////,UDG,KUDG,KUDG,OK,1.0,////,////,////,DARLINGTON COUNTY,"DARLINGTON, SC - UNITED STATES",Darlington County Airport,"Darlington, South Carolina, United States",OK
    ////,ULS,KULS,KULS,OK,1.0,////,////,////,ULYSSES,"ULYSSES, KS - UNITED STATES",????,"Ulysses, Kansas, United States",OK
    ////,UMP,KUMP,KUMP,OK,1.0,////,////,////,INDIANAPOLIS METROPOLITAN,"INDIANAPOLIS, IN - UNITED STATES",Indianapolis Metropolitan Airport,"Indianapolis, Indiana, United States",OK
    ////,UNO,KUNO,KUNO,OK,1.0,////,////,////,WEST PLAINS RGNL,"WEST PLAINS, MO - UNITED STATES",West Plains Regional,"West Plains, Missouri, United States",OK
    ////,USE,KUSE,KUSE,OK,1.0,////,////,////,FULTON COUNTY,"WAUSEON, OH - UNITED STATES",Fulton County Airport,"Wauseon, Ohio, United States",OK
    ////,UXL,KUXL,KUXL,OK,1.0,////,////,////,SOUTHLAND FIELD,"SULPHUR, LA - UNITED STATES",Southland Field,"Sulphur, Louisiana, United States",OK
    ////,UYF,KUYF,KUYF,OK,1.0,////,////,////,MADISON COUNTY,"LONDON, OH - UNITED STATES",Madison County Airport,"London, Ohio, United States",OK
    ////,VBG,KVBG,KVBG,OK,1.0,////,////,////,VANDENBERG AFB,"LOMPOC, CA - UNITED STATES",Vandenberg AFB,"Lompoc, California, United States",OK
    ////,VBT,KVBT,KVBT,OK,1.0,////,////,////,BENTONVILLE MUNI/LOUISE M THADEN FIELD,"BENTONVILLE, AR - UNITED STATES",Bentonville Municipal/Louise M Thaden Field,"Bentonville, Arkansas, United States",OK
    ////,VBW,KVBW,KVBW,OK,1.0,////,////,////,BRIDGEWATER AIR PARK,"BRIDGEWATER, VA - UNITED STATES",Bridgewater Air Park,"Bridgewater, Virginia, United States",OK
    ////,VDF,KVDF,KVDF,OK,1.0,////,////,////,TAMPA EXECUTIVE,"TAMPA, FL - UNITED STATES",Tampa Executive Airport,"Tampa, Florida, United States",OK
    ////,VER,KVER,KVER,OK,1.0,////,////,////,JESSE VIERTEL MEMORIAL,"BOONVILLE, MO - UNITED STATES",Jesse Viertel Memorial Airport,"Boonville, Missouri, United States",OK
    ////,VES,KVES,KVES,OK,1.0,////,////,////,DARKE COUNTY,"VERSAILLES, OH - UNITED STATES",Darke County Airport,"Versailles, Ohio, United States",OK
    ////,VLL,KVLL,KVLL,OK,1.0,////,////,////,OAKLAND/TROY,"TROY, MI - UNITED STATES",Oakland/Troy Airport,"Troy, Michigan, United States",OK
    ////,VNW,KVNW,KVNW,OK,1.0,////,////,////,VAN WERT COUNTY,"VAN WERT, OH - UNITED STATES",Van Wert County Airport,"Van Wert, Ohio, United States",OK
    ////,VPC,KVPC,KVPC,OK,1.0,////,////,////,CARTERSVILLE,"CARTERSVILLE, GA - UNITED STATES",????,"Cartersville, Georgia, United States",OK
    ////,VQQ,KVQQ,KVQQ,OK,1.0,////,////,////,CECIL,"JACKSONVILLE, FL - UNITED STATES",Cecil Airport,"Jacksonville, Florida, United States",OK
    ////,VTA,KVTA,KVTA,OK,1.0,////,////,////,NEWARK-HEATH,"NEWARK, OH - UNITED STATES",Newark-Heath Airport,"Newark, Ohio, United States",OK
    ////,VTI,KVTI,KVTI,OK,0.963,////,////,////,VINTON VETERANS MEMORIAL ARPK,"VINTON, IA - UNITED STATES",Vinton Veterans Memorial Airpark,"Vinton, Iowa, United States",OK
    ////,VUJ,KVUJ,KVUJ,OK,1.0,////,////,////,STANLY COUNTY,"ALBEMARLE, NC - UNITED STATES",Stanly County Airport,"Albemarle, North Carolina, United States",OK
    ////,VUO,KVUO,KVUO,OK,1.0,////,////,////,PEARSON FIELD,"VANCOUVER, WA - UNITED STATES",Pearson Field,"Vancouver, Washington, United States",OK
    ////,VVS,KVVS,KVVS,OK,1.0,////,////,////,JOSEPH A HARDY CONNELLSVILLE,"CONNELLSVILLE, PA - UNITED STATES",Joseph A Hardy Connellsville Airport,"Connellsville, Pennsylvania, United States",OK
    ////,VVV,KVVV,KVVV,OK,1.0,////,////,////,ORTONVILLE MUNI-MARTINSON FIELD,"ORTONVILLE, MN - UNITED STATES",Ortonville Municipal-Martinson Field,"Ortonville, Minnesota, United States",OK
    ////,VWU,KVWU,KVWU,OK,1.0,////,////,////,WASKISH MUNI,"WASKISH, MN - UNITED STATES",Waskish Municipal Airport,"Waskish, Minnesota, United States",OK
    ////,W04,////,////,OK,1.0,////,////,////,OCEAN SHORES MUNI,"OCEAN SHORES, WA - UNITED STATES",Ocean Shores Municipal Airport,"Ocean Shores, Washington, United States",OK
    ////,W13,////,////,OK,1.0,////,////,////,EAGLE'S NEST,"WAYNESBORO, VA - UNITED STATES",Eagle's Nest Airport,"Waynesboro, Virginia, United States",OK
    ////,WA19,////,////,OK,0.901,////,////,////,BERKLEY STRUCTURES,"REDMOND, WA - UNITED STATES",Berkley Structures Heliport,"Redmond, Washington, United States",OK
    ////,WA77,////,////,OK,1.0,////,////,////,ENUMCLAW,"ENUMCLAW, WA - UNITED STATES",????,"Enumclaw, Washington, United States",OK
    ////,WA81,////,////,OK,1.0,////,////,////,EVERGREEN NORTH-SOUTH AIRPARK,"VANCOUVER, WA - UNITED STATES",Evergreen North-South Airpark,"Vancouver, Washington, United States",OK
    ////,WAL,KWAL,KWAL,OK,0.87,////,////,////,WALLOPS FLIGHT FACILITY,"WALLOPS ISLAND, VA - UNITED STATES",Wallops Flight Center,"Chincoteague, Wallops Island, Virginia, United States",OK
    ////,WWT,////,////,OK,0.88,////,////,////,NEWTOK,"NEWTOK, AK - UNITED STATES",Newtok SPB,"Newtok, Alaska, United States",OK
    ////,X01,////,////,OK,1.0,////,////,////,EVERGLADES AIRPARK,"EVERGLADES, FL - UNITED STATES",Everglades Airpark,"Everglades, Florida, United States",OK
    ////,X04,////,////,OK,1.0,////,////,////,ORLANDO APOPKA,"APOPKA, FL - UNITED STATES",Orlando Apopka Airport,"Apopka, Florida, United States",OK
    ////,X07,////,////,OK,1.0,////,////,////,LAKE WALES MUNI,"LAKE WALES, FL - UNITED STATES",Lake Wales Municipal Airport,"Lake Wales, Florida, United States",OK
    ////,X21,////,////,OK,1.0,////,////,////,ARTHUR DUNN AIR PARK,"TITUSVILLE, FL - UNITED STATES",Arthur Dunn Air Park,"Titusville, Florida, United States",OK
    ////,X26,////,////,OK,1.0,////,////,////,SEBASTIAN MUNI,"SEBASTIAN, FL - UNITED STATES",Sebastian Municipal Airport,"Sebastian, Florida, United States",OK
    ////,X39,////,////,OK,1.0,////,////,////,TAMPA NORTH AERO PARK,"TAMPA, FL - UNITED STATES",Tampa North Aero Park Airport,"Tampa, Florida, United States",OK
    ////,X49,////,////,OK,1.0,////,////,////,SOUTH LAKELAND,"LAKELAND, FL - UNITED STATES",South Lakeland Airport,"Lakeland, Florida, United States",OK
    ////,X59,////,////,OK,1.0,////,////,////,VALKARIA,"VALKARIA, FL - UNITED STATES",????,"Valkaria, Florida, United States",OK
    ////,XBP,KXBP,KXBP,OK,1.0,////,////,////,BRIDGEPORT MUNI,"BRIDGEPORT, TX - UNITED STATES",Bridgeport Municipal Airport,"Bridgeport, Texas, United States",OK
    ////,XLL,KXLL,KXLL,OK,1.0,////,////,////,ALLENTOWN QUEEN CITY MUNI,"ALLENTOWN, PA - UNITED STATES",Allentown Queen City Municipal Airport,"Allentown, Pennsylvania, United States",OK
    ////,XMR,KXMR,KXMR,OK,1.0,////,////,////,CAPE CANAVERAL AFS SKID STRIP,"COCOA BEACH, FL - UNITED STATES",Cape Canaveral Afs Skid Strip,"Cocoa Beach, Florida, United States",OK
    ////,XNO,KXNO,KXNO,OK,0.9,////,////,////,NORTH AF AUX,"NORTH, SC - UNITED STATES",North AF Auxiliary Airport,"North, South Carolina, United States",OK
    ////,XS46,////,////,OK,0.826,////,////,////,PORT O'CONNOR PRIVATE,"PORT O'CONNOR, TX - UNITED STATES",Port O'Connor Airport,"Port O'Connor, Texas, United States",OK
    ////,XSA,KXSA,KXSA,OK,1.0,////,////,////,TAPPAHANNOCK-ESSEX COUNTY,"TAPPAHANNOCK, VA - UNITED STATES",Tappahannock-Essex County Airport,"Tappahannock, Virginia, United States",OK
    ////,XVG,KXVG,KXVG,OK,1.0,////,////,////,LONGVILLE MUNI,"LONGVILLE, MN - UNITED STATES",Longville Municipal Airport,"Longville, Minnesota, United States",OK
    ////,Y31,////,////,OK,1.0,////,////,////,WEST BRANCH COMMUNITY,"WEST BRANCH, MI - UNITED STATES",West Branch Community Airport,"West Branch, Michigan, United States",OK
    ////,Y51,////,////,OK,1.0,////,////,////,VIROQUA MUNI,"VIROQUA, WI - UNITED STATES",Viroqua Municipal Airport,"Viroqua, Wisconsin, United States",OK
    ////,Y72,////,////,OK,1.0,////,////,////,BLOYER FIELD,"TOMAH, WI - UNITED STATES",Bloyer Field,"Tomah, Wisconsin, United States",OK
    ////,Z13,////,////,OK,1.0,////,////,////,AKIACHAK,"AKIACHAK, AK - UNITED STATES",????,"Akiachak, Alaska, United States",OK
    ////,Z84,PACL,PACL,OK,1.0,////,////,////,CLEAR,"CLEAR, AK - UNITED STATES",????,"Clear, Alaska, United States",OK
    ////,ZUN,KZUN,KZUN,OK,1.0,////,////,////,BLACK ROCK,"ZUNI PUEBLO, NM - UNITED STATES",Black Rock Airport,"Zuñí Pueblo, New Mexico, United States",OK

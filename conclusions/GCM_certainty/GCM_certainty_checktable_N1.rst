
List for checking certainty of Great Circle Mapper (NAA - NIO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    NAA,////,////,YNBR,UNK,1.0,Narrabri,"Narrabri, Australia",OWNO,////,////,????,"Narrabri, New South Wales, Australia",OK
    NAC,////,////,YNRC,UNK,1.0,Naracoorte,"Naracoorte, Australia",OWNO,////,////,????,"Naracoorte, South Australia, Australia",OK
    NAD,////,////,////,UNK,1.0,Macanal,"Macanal, Colombia",OWNO,////,////,????,"Macanal, Guainía, Colombia",OK
    NAE,////,////,DBBN,UNK,0.706,Natitingou,"Natitingou, Benin",OWNO,////,////,Boundétingou Airport,"Natitingou, Atakora, Benin",OK
    NAF,////,////,////,UNK,1.0,Banaina,"Banaina, Indonesia",OWNO,////,////,????,"Banaina, Kalimantan Timur (East Borneo), Indonesia",OK
    NAG,////,////,VANP,UNK,0.632,Sonegaon,"Nagpur, India",OWNO,////,////,????,"Nagpur, Maharashtra, India",OK
    NAH,////,////,WAMH,UNK,0.5,Naha,"Naha, Indonesia",OWNO,////,////,????,"Tahuna, Sulawesi Utara, Indonesia",TO DO CHECK
    NAI,////,////,SYAN,UNK,1.0,Annai,"Annai, Guyana",OWNO,////,////,????,"Annai, Upper Takutu-Upper Essequibo, Guyana",OK
    NAJ,////,////,UBBN,UNK,0.889,Nakhichevan,"Nakhichevan, Azerbaijan",OWNO,////,////,????,"Nakhchivan, Naxcivan (Nakhchivan), Azerbaijan",OK
    NAK,////,////,VTUQ,UNK,1.0,Nakhon Ratchasima,"Nakhon Ratchasima, Thailand",OWNO,////,////,????,"Nakhon Ratchasima, Nakhon Ratchasima, Thailand",OK
    NAL,////,////,URMN,UNK,1.0,Nalchik,"Nalchik, Russia",OWNO,////,////,Nalchik Airport,"Nalchik, Kabardino-Balkarskaya, Russian Federation (Russia)",OK
    NAM,////,////,WAPR,UNK,1.0,Namlea,"Namlea, Indonesia",OWNO,////,////,Namlea Airport,"Namlea, Buru Island, Maluku, Indonesia",OK
    NAN,////,////,NFFN,UNK,1.0,International,"Nadi, Suva, Fiji",OWNO,////,////,Nadi International Airport,"Nadi, Fiji",OK
    NAO,////,////,ZUNC,UNK,1.0,Nanchong,"Nanchong, PR China",OWNO,////,////,????,"Nanchong, Sichuan, China",OK
    NAP,////,////,LIRN,UNK,0.706,Naples,"Naples, Italy",OWNO,////,////,Naples International Airport,"Naples, Campania, Italy",OK
    NAQ,////,////,BGQQ,UNK,1.0,Qaanaaq,"Qaanaaq, Greenland",OWNO,////,////,????,"Qaanaaq, Qaasuitsup, Greenland",OK
    NAR,////,////,SKPN,UNK,0.545,Nare,"Nare, Colombia",OWNO,////,////,Puerto Nare Airport,"Puerto Nare, Antioquia, Colombia",MAYBE
    NAS,////,MYNN,MYNN,OK,0.335,Nassau International,"Nassau, Bahamas",OWNO,LYNDEN PINDLING INTL,"NASSAU, - BAHAMAS",Lynden Pindling International Airport,"Nassau, New Providence Island, Bahamas",OK
    NAT,////,////,SBSG,UNK,nan,////,////,////,////,////,Greater Natal International Airport,"Natal, Rio Grande do Norte, Brazil",UNK
    NAU,////,////,NTGN,UNK,1.0,Napuka Island,"Napuka Island, French Polynesia",OWNO,////,////,????,"Napuka Island, French Polynesia",OK
    NAV,////,////,LTAZ,UNK,0.615,Nevsehir,"Nevsehir, Turkey",OWNO,////,////,Nevsehir Kapadokya Airport,"Nevsehir, Nevsehir, Turkey",OK
    NAW,////,////,VTSC,UNK,1.0,Narathiwat,"Narathiwat, Thailand",OWNO,////,////,????,"Narathiwat, Narathiwat, Thailand",OK
    NAY,////,////,ZBNY,UNK,1.0,Nanyuan Airport,"Beijing, PR China",OWNO,////,////,Beijing Nanyuan Airport,"Beijing, Beijing, China",OK
    NBA,////,////,////,UNK,1.0,Nambaiyufa,"Nambaiyufa, Papua New Guinea",OWNO,////,////,????,"Nambaiyufa, Chimbu, Papua-New Guinea",OK
    NBB,////,////,////,UNK,1.0,Barrancominas,"Barrancominas, Colombia",OWNO,////,////,????,"Barrancominas, Vichada, Colombia",OK
    NBC,////,////,UWKE,UNK,0.424,Naberevnye Chelny,"Naberevnye Chelny, Russia",OWNO,////,////,Begishevo Airport,"Nizhnekamsk, Tatarstan, Russian Federation (Russia)",TO DO CHECK
    NBE,////,////,DTNH,UNK,nan,////,////,////,////,////,Enfidha-Hammamet International Airport,"Enfidha, Tunisia",UNK
    NBG,NBG,KNBG,KNBG,OK,0.857,NAS/Alvin Callendar,"New Orleans (LA), USA",OWNO,NEW ORLEANS NAS JRB/ALVIN CALLENDER FIELD,"NEW ORLEANS, LA - UNITED STATES",New Orléans NAS JRB/Alvin Callender Field,"New Orléans, Louisiana, United States",OK
    NBH,////,////,YNHS,UNK,1.0,Nambucca Heads,"Nambucca Heads, Australia",OWNO,////,////,????,"Nambucca Heads, New South Wales, Australia",OK
    NBL,////,////,MPWN,UNK,1.0,San Blas,"San Blas, Panama",OWNO,////,////,San Blas,"Wannukandi, Kuna Yala (Guna Yala), Panamá",OK
    NBN,////,////,FGAB,UNK,nan,////,////,////,////,////,Annobón Airport,"San Antonio de Palé, Annobón, Equatorial Guinea",UNK
    NBO,////,////,HKJK,UNK,1.0,Jomo Kenyatta International,"Nairobi, Kenya",OWNO,////,////,Jomo Kenyatta International Airport,"Nairobi, Kenya",OK
    NBS,////,////,ZYBS,UNK,1.0,Changbaishan,Baishan,IATA,////,////,Changbaishan Airport,"Baishan, Jilin, China",MAYBE
    NBW,////,////,MUGM,UNK,0.909,Guantanamo NAS,"Guantanamo, Cuba",OWNO,////,////,NAS Guantánamo Bay,"Guantánamo, Guantánamo, Cuba",OK
    NBX,////,////,WABI,UNK,1.0,Nabire,"Nabire, Indonesia",OWNO,////,////,????,"Nabire, Papua, Indonesia",OK
    NCA,////,MBNC,MBNC,OK,0.779,North Caicos,"North Caicos, Turks and Caicos Islands",OWNO,NORTH CAICOS INTL,"BOTTLE CREEK AND WHITBY, - TURKS AND CAICOS ISLANDS",North Caicos Airport,"Bottle Creek and Whitby, North Caicos, Turks and Caicos Islands",OK
    NCE,////,////,LFMN,UNK,1.0,Cote D'Azur,"Nice, France",OWNO,////,////,Côte d'Azur,"Nice, Provence-Alpes-Côte d'Azur, France",OK
    NCG,////,////,MMCG,UNK,1.0,Nueva Casas Grandes,"Nueva Casas Grandes, Mexico",OWNO,////,////,????,"Nuevo Casas Grandes, Chihuahua, México",OK
    NCH,////,////,HTNA,UNK,1.0,Nachingwea,"Nachingwea, Tanzania",OWNO,////,////,Nachingwea Airport,"Nachingwea, Lindi, Tanzania",OK
    NCI,////,////,SKNC,UNK,1.0,Necocli,"Necocli, Colombia",OWNO,////,////,Necoclí Airport,"Necoclí, Antioquia, Colombia",OK
    NCJ,////,////,SAFS,UNK,nan,////,////,////,////,////,Sunchales Airport,"Sunchales, Santa Fe, Argentina",UNK
    NCL,////,////,EGNT,UNK,0.783,Newcastle Airport,"Newcastle, United Kingdom",OWNO,////,////,Newcastle International Airport,"Newcastle upon Tyne, Northumberland, England, United Kingdom",OK
    NCN,C05,PFCB,PFCB,OK,0.716,New Chenega,"New Chenega (AK), USA",OWNO,CHENEGA BAY,"CHENEGA, AK - UNITED STATES",Chenega Bay Airport,"Chenega, Alaska, United States",OK
    NCO,RI12,////,////,UNK,0.549,NAS,"Quonset Point (RI), USA",OWNO,////,////,Quonset State Air Reserve National Guard Helipad,"North Kingstown, Rhode Island, United States",OK
    NCS,////,////,FANC,UNK,1.0,Newcastle,"Newcastle, South Africa",OWNO,////,////,Newcastle Airport,"Newcastle, KwaZulu-Natal, South Africa",OK
    NCT,////,////,MRNC,UNK,1.0,Guanacaste,"Nicoya, Costa Rica",OWNO,////,////,Guanacaste,"Nicoya, Guanacaste, Costa Rica",OK
    NCU,////,////,UTNN,UNK,1.0,Nukus,"Nukus, Uzbekistan",OWNO,////,////,Nukus Airport,"Nukus, Qoraqalpog'iston Respublikasi (Karakalpakstan), Uzbekistan",OK
    NCY,////,////,LFLP,UNK,0.963,Annecy-Meythe,"Annecy, France",OWNO,////,////,Meythet,"Annecy, Rhône-Alpes, France",OK
    NDA,////,////,WAPC,UNK,1.0,Bandanaira,"Bandanaira, Indonesia",OWNO,////,////,????,"Bandanaira, Banda Islands, Maluku, Indonesia",OK
    NDB,////,////,GQPP,UNK,0.8,Nouadhibou,"Nouadhibou, Mauritania",OWNO,////,////,Nouadhibou International Airport,"Nouadhibou, Mauritania",OK
    NDC,////,////,VAND,UNK,1.0,Nanded,"Nanded, India",OWNO,////,////,????,"Nanded, Maharashtra, India",OK
    NDD,////,////,FNSU,UNK,1.0,Sumbe,"Sumbe, Angola",OWNO,////,////,????,"Sumbe, Angola",OK
    NDE,////,////,HKMA,UNK,1.0,Mandera,"Mandera, Kenya",OWNO,////,////,????,"Mandera, Kenya",OK
    NDG,////,////,ZYQQ,UNK,0.824,Qiqihar,"Qiqihar, PR China",OWNO,////,////,Qiqihar Sanjiazi Airport,"Qiqihar, Heilongjiang, China",OK
    NDI,////,////,////,UNK,1.0,Namudi,"Namudi, Papua New Guinea",OWNO,////,////,????,"Namudi, Northern, Papua-New Guinea",OK
    NDJ,////,////,FTTJ,UNK,1.0,Ndjamena,"Ndjamena, Chad",OWNO,////,////,????,"N'Djamena, Chari-Baguirmi, Chad",OK
    NDK,3N0,////,////,OK,0.356,Namdrik Island,"Namdrik Island, Marshall Islands",OWNO,NAMORIK,"NAMORIK ATOLL, - MARSHALL ISLANDS",????,"Namorik Atoll, Marshall Islands",TO DO CHECK
    NDL,////,////,FEFN,UNK,1.0,Ndele,"Ndele, Central African Republic",OWNO,////,////,????,"N'Délé, Bamingui-Bangoran (Bamïngï-Bangoran), Central African Republic",OK
    NDM,////,////,HAMN,UNK,1.0,Mendi,"Mendi, Ethiopia",OWNO,////,////,Mendi Airport,"Mendi, Oromia, Ethiopia",OK
    NDN,////,////,////,UNK,1.0,Nadunumu,"Nadunumu, Papua New Guinea",OWNO,////,////,????,"Nadunumu, Central, Papua-New Guinea",OK
    NDR,////,////,GMMW,UNK,0.714,Nador,"Nador, Morocco",OWNO,////,////,Arwi,"Nador, Morocco",OK
    NDS,////,////,YSAN,UNK,1.0,Sandstone,"Sandstone, Australia",OWNO,////,////,????,"Sandstone, Western Australia, Australia",OK
    NDU,////,////,FYRU,UNK,1.0,Rundu,"Rundu, Namibia",OWNO,////,////,????,"Rundu, Namibia",OK
    NDY,////,////,EGES,UNK,1.0,Sanday,"Sanday, United Kingdom",OWNO,////,////,????,"Sanday, Orkney Isles, Scotland, United Kingdom",OK
    NDZ,////,////,ETMN,UNK,1.0,Cuxhaven,"Nordholz-Spieka, Germany",OWNO,////,////,Cuxhaven,"Nordholz-Spieka, Lower Saxony, Germany",OK
    NEA,////,////,////,UNK,1.0,NAS,"Glynco (GA), USA",OWNO,////,////,NAS,"Glynco, Georgia, United States",OK
    NEC,////,////,SAZO,UNK,1.0,Necochea,"Necochea, Argentina",OWNO,////,////,????,"Necochea, Buenos Aires, Argentina",OK
    NEF,////,////,UWUF,UNK,1.0,Neftekamsk,"Neftekamsk, Russia",OWNO,////,////,????,"Neftekamsk, Bashkortostan, Russian Federation (Russia)",OK
    NEG,////,////,MKNG,UNK,1.0,Negril,"Negril, Jamaica",OWNO,////,////,????,"Negril, Jamaica",OK
    NEJ,////,////,HANJ,UNK,1.0,Nejjo,"Nejjo, Ethiopia",OWNO,////,////,Nejjo Airport,"Nejo, Oromia, Ethiopia",OK
    NEK,////,////,HANK,UNK,1.0,Nekemt,"Nekemt, Ethiopia",OWNO,////,////,Nekemte Airport,"Nekemte, Oromia, Ethiopia",OK
    NEL,NEL,KNEL,KNEL,OK,0.588,Naec,"Lakehurst (NJ), USA",OWNO,LAKEHURST MAXFIELD FLD,"LAKEHURST, NJ - UNITED STATES",Lakehurst Maxfield Field,"Lakehurst, New Jersey, United States",OK
    NEN,NEN,KNEN,KNEN,OK,0.806,Olf Usn,"Whitehouse (FL), USA",OWNO,WHITEHOUSE NOLF,"JACKSONVILLE, FL - UNITED STATES",Whitehouse NOLF Airport,"Jacksonville, Florida, United States",OK
    NER,////,////,UELL,UNK,0.737,Neryungri,"Neryungri, Russia",OWNO,////,////,Chulman Neryungri Airport,"Cul'man, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    NEU,////,////,VLSN,UNK,1.0,Sam Neua,"Sam Neua, Lao PDR",OWNO,////,////,????,"Sam Neua, Lao People's Democratic Republic (Laos)",OK
    NEV,////,////,TKPN,UNK,0.5,Newcastle,"Nevis, St. Kitts And Nevis",OWNO,////,////,Vance Winkworth Amory International Airport,"Newcastle, Nevis Island, Saint Kitts and Nevis",OK
    NEW,NEW,KNEW,KNEW,OK,1.0,Lakefront,"New Orleans (LA), USA",OWNO,LAKEFRONT,"NEW ORLEANS, LA - UNITED STATES",Lakefront Airport,"New Orléans, Louisiana, United States",OK
    NFG,////,////,USRN,UNK,1.0,Nefteyugansk,"Nefteyugansk, Russia",OWNO,////,////,????,"Nefteyugansk, Khanty-Mansiyskiy, Russian Federation (Russia)",OK
    NFL,NFL,KNFL,KNFL,OK,1.0,NAS,"Fallon (NV), USA",OWNO,FALLON NAS (VAN VOORHIS FLD),"FALLON, NV - UNITED STATES",NAS Fallon,"Fallon, Nevada, United States",OK
    NFO,////,////,NFTO,UNK,1.0,Mata'aho,"Niuafo'ou, Tonga",OWNO,////,////,Mata'aho,"Angaha, Niuafo'ou, Tonga",OK
    NGA,////,////,YYNG,UNK,1.0,Young,"Young, Australia",OWNO,////,////,????,"Young, New South Wales, Australia",OK
    NGB,////,////,ZSNB,UNK,0.774,Ningbo,"Ningbo, PR China",OWNO,////,////,Lishe International Airport,"Ningbo, Zhejiang, China",OK
    NGD,////,////,TUPA,UNK,0.462,Anegada,"Anegada, British Virgin Islands",OWNO,////,////,Capt. Auguste George,"The Settlement, Anegada, Virgin Islands (British)",OK
    NGE,////,////,FKKN,UNK,1.0,Ngaoundere,"Ngaoundere, Cameroon",OWNO,////,////,N'Gaoundéré Airport,"N'Gaoundéré, Adamaoua, Cameroon",OK
    NGI,////,////,NFNG,UNK,1.0,Ngau Island,"Ngau Island, Fiji",OWNO,////,////,????,"Ngau, Ngau Island, Fiji",OK
    NGK,////,////,UHSN,UNK,nan,////,////,////,////,////,Nogliki Airport,"Nogliki, Sakhalinskaya, Russian Federation (Russia)",UNK
    NGN,////,////,////,UNK,1.0,Nargana,"Nargana, Panama",OWNO,////,////,????,"Narganá, Kuna Yala (Guna Yala), Panamá",OK
    NGO,////,////,RJGG,UNK,0.645,Komaki AFB,"Nagoya, Japan",OWNO,////,////,Chubu Centrair International Airport,"Nagoya, Aichi, Japan",OK
    NGP,NGP,KNGP,KNGP,OK,1.0,NAS,"Corpus Christi (TX), USA",OWNO,CORPUS CHRISTI NAS/TRUAX FIELD,"CORPUS CHRISTI, TX - UNITED STATES",Corpus Christi NAS/Truax Field,"Corpus Christi, Texas, United States",OK
    NGQ,////,////,ZUAL,UNK,nan,////,////,////,////,////,Ngari Gunsa Airport,"Shiquanhe, Ngari, Tibet, China",UNK
    NGR,////,////,////,UNK,1.0,Ningerum,"Ningerum, Papua New Guinea",OWNO,////,////,????,"Ningerum, Western, Papua-New Guinea",OK
    NGS,////,////,RJFU,UNK,1.0,Nagasaki,"Nagasaki, Japan",OWNO,////,////,Nagasaki Airport,"Nagasaki, Nagasaki, Japan",OK
    NGU,NGU,KNGU,KNGU,OK,0.655,NAS Chambers,"Norfolk (VA), USA",OWNO,NORFOLK NS (CHAMBERS FLD),"NORFOLK, VA - UNITED STATES",Norfolk NS Airport,"Norfolk, Virginia, United States",OK
    NGW,NGW,KNGW,KNGW,OK,0.87,Cabaniss Field,"Corpus Christi (TX), USA",OWNO,CABANISS FIELD NOLF,"CORPUS CHRISTI, TX - UNITED STATES",Cabaniss Field NOLF Airport,"Corpus Christi, Texas, United States",OK
    NGX,////,////,VNMA,UNK,1.0,Manang,"Manang, Nepal",OWNO,////,////,????,"Manang, Nepal",OK
    NHD,////,////,OMDM,UNK,0.786,Military Airport,"Minhad Ab, United Arab Emirates",OWNO,////,////,Al Minhad AB,"Dubai, Dubai, United Arab Emirates",MAYBE
    NHF,////,////,HSNW,UNK,1.0,New Halfa Airport,"New Halfa, Sudan",OWNO,////,////,????,"New Halfa, Kassala, Sudan",OK
    NHK,NHK,KNHK,KNHK,OK,1.0,NAS,"Patuxent River (MD), USA",OWNO,PATUXENT RIVER NAS/TRAPNELL FIELD/,"PATUXENT RIVER, MD - UNITED STATES",NAS Patuxent River,"Patuxent River, Maryland, United States",OK
    NHS,////,////,OPNK,UNK,1.0,Nushki,"Nushki, Pakistan",OWNO,////,////,????,"Nushki, Balochistan, Pakistan",OK
    NHT,////,////,EGWU,UNK,1.0,Northolt,"Northolt, United Kingdom",OWNO,////,////,????,"Northolt, Middlesex, England, United Kingdom",OK
    NHV,////,////,NTMD,UNK,1.0,Nuku Hiva,"Nuku Hiva, French Polynesia",OWNO,////,////,????,"Nuku Hiva, Marquesas Islands, French Polynesia",OK
    NHX,NBJ,KNBJ,KNBJ,OK,0.826,Barin OLF Osn,"Foley (AL), USA",OWNO,BARIN NOLF,"FOLEY, AL - UNITED STATES",Barin NOLF Airport,"Foley, Alabama, United States",OK
    NIA,////,////,GLNA,UNK,0.625,Nimba,"Nimba, Liberia",OWNO,////,////,LAMCO,"Nimba, Liberia",OK
    NIB,FSP,PAFS,PAFS,OK,1.0,Nikolai,"Nikolai (AK), USA",OWNO,NIKOLAI,"NIKOLAI, AK - UNITED STATES",????,"Nikolai, Alaska, United States",OK
    NIF,////,////,YCNF,UNK,1.0,Nifty,"Nifty, Australia",OWNO,////,////,????,"Nifty, Western Australia, Australia",OK
    NIG,////,////,NGNU,UNK,1.0,Nikunau,"Nikunau, Kiribati",OWNO,////,////,????,"Nikunau, Kiribati",OK
    NIK,////,////,////,UNK,1.0,Niokolo Koba,"Niokolo Koba, Senegal",OWNO,////,////,????,"Niokolo-Koba, Tambacounda, Senegal",OK
    NIM,////,////,DRRN,UNK,0.421,Niamey,"Niamey, Niger",OWNO,////,////,Diori Hamani International Airport,"Niamey, Niger",OK
    NIN,NIN,////,////,OK,1.0,Ninilchik,"Ninilchik (AK), USA",OWNO,NINILCHIK,"NINILCHIK, AK - UNITED STATES",????,"Ninilchik, Alaska, United States",OK
    NIO,////,////,FZBI,UNK,1.0,Nioki,"Nioki, Congo (DRC)",OWNO,////,////,Nioki Airport,"Nioki, Bandundu, Democratic Republic of Congo (Zaire)",OK

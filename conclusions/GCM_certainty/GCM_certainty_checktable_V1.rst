
List for checking certainty of Great Circle Mapper (VAA - VLY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    VAA,////,////,EFVA,UNK,1.0,Vaasa,"Vaasa, Finland",OWNO,////,////,????,"Vaasa, Pohjanmaa (Österbotten (Ostrobothnia)), Finland",OK
    VAB,////,////,////,UNK,1.0,Yavarate,"Yavarate, Colombia",OWNO,////,////,????,"Yavarate, Vaupés, Colombia",OK
    VAC,////,////,EDWU,UNK,1.0,Varrelbusch,"Varrelbusch, Germany",OWNO,////,////,????,"Varrelbusch, Lower Saxony, Germany",OK
    VAD,VAD,KVAD,KVAD,OK,1.0,Moody AFB,"Valdosta (GA), USA",OWNO,MOODY AFB,"VALDOSTA, GA - UNITED STATES",Moody AFB,"Valdosta, Georgia, United States",OK
    VAF,////,////,LFLU,UNK,1.0,Chabeuil,"Valence, France",OWNO,////,////,Chabeuil,"Valence, Rhône-Alpes, France",OK
    VAG,////,////,SBVG,UNK,0.875,Maj. Brig. Trompowsky,"Varginha, Brazil",OWNO,////,////,Major Brigadeiro Trompowsky,"Varginha, Minas Gerais, Brazil",OK
    VAH,////,////,SLVG,UNK,0.391,Vallegrande,"Vallegrande, Bolivia",OWNO,////,////,Capitán Av. Vidal Villagomez Toledo,"Vallegrande, Vallegrande, Santa Cruz, Bolivia",OK
    VAI,////,////,AYVN,UNK,1.0,Vanimo,"Vanimo, Papua New Guinea",OWNO,////,////,????,"Vanimo, Sandaun, Papua-New Guinea",OK
    VAK,VAK,PAVA,PAVA,OK,1.0,Chevak,"Chevak (AK), USA",OWNO,CHEVAK,"CHEVAK, AK - UNITED STATES",????,"Chevak, Alaska, United States",OK
    VAL,////,////,SNVB,UNK,1.0,Valenca,"Valenca, Brazil",OWNO,////,////,????,"Valença, Bahia, Brazil",OK
    VAM,////,////,VRMV,UNK,nan,////,////,////,////,////,Villa International Airport,"Maamigili Island, Alif Dhaal Atoll, Maldives",UNK
    VAN,////,////,LTCI,UNK,0.375,Van,"Van, Turkey",OWNO,////,////,Van Ferit Melen Airport,"Van, Van, Turkey",OK
    VAO,////,////,AGGV,UNK,0.64,Suavanao Airstrip,"Suavanao, Solomon Islands",OWNO,////,////,????,"Suavanao, Santa Isabel Island, Solomon Islands",OK
    VAP,////,////,SCRD,UNK,0.667,Valparaiso,"Valparaiso, Chile",OWNO,////,////,Rodelillo Airport,"Viña del Mar, Valparaíso, Chile",OK
    VAR,////,////,LBWN,UNK,0.667,Varna,"Varna, Bulgaria",OWNO,////,////,Varna International Airport,"Varna, Varna, Bulgaria",OK
    VAS,////,////,LTAR,UNK,1.0,Sivas,"Sivas, Turkey",OWNO,////,////,????,"Sivas, Sivas, Turkey",OK
    VAT,////,////,FMMY,UNK,1.0,Vatomandry,"Vatomandry, Madagascar",OWNO,////,////,Vatomandry Airport,"Vatomandry, Madagascar",OK
    VAU,////,////,NFNV,UNK,1.0,Vatukoula,"Vatukoula, Fiji",OWNO,////,////,????,"Vatukoula, Fiji",OK
    VAV,////,////,NFTV,UNK,0.593,Lupepau'u,"Vava'u, Tonga",OWNO,////,////,Vava'u International Airport,"Vava'u, Tonga",OK
    VAW,////,////,ENSS,UNK,0.5,Vardoe,"Vardoe, Norway",OWNO,////,////,Vardø Airport Svartnes,"Vardø, Finnmark, Norway",OK
    VBA,////,////,VYAN,UNK,nan,////,////,////,////,////,Ann Airport,"Ann, Rakhine, Myanmar (Burma)",UNK
    VBC,////,////,VYCZ,UNK,nan,////,////,////,////,////,Mandalay Chanmyathazi Airport,"Mandalay, Mandalay, Myanmar (Burma)",UNK
    VBP,////,////,VYBP,UNK,nan,////,////,////,////,////,Bokepyin Airport,"Bokepyin, Taninthayi, Myanmar (Burma)",UNK
    VBS,////,////,LIPO,UNK,1.0,Montichiari,"Verona, Italy",OWNO,////,////,Montichiari,"Brescia, Lombardy, Italy",OK
    VBV,////,////,NFVB,UNK,0.556,Vanuabalavu,"Vanuabalavu, Fiji",OWNO,////,////,????,"Vanua Balavu, Fiji",OK
    VBY,////,////,ESSV,UNK,1.0,Visby,"Visby, Sweden",OWNO,////,////,????,"Visby, Gotlands län, Sweden",OK
    VCA,////,////,VVCT,UNK,0.737,Can Tho,"Can Tho, Viet Nam",OWNO,////,////,Cân Tho International Airport,"Cân Tho, Vietnam",OK
    VCB,VCB,KVCB,KVCB,OK,0.371,View Cove,"View Cove (AK), USA",OWNO,NUT TREE,"VACAVILLE, CA - UNITED STATES",Nut Tree Airport,"Vacaville, California, United States",OK
    VCD,////,////,YVRD,UNK,1.0,Victoria River Downs,"Victoria River Downs, Australia",OWNO,////,////,????,"Victoria River Downs, Northern Territory, Australia",OK
    VCE,////,////,LIPZ,UNK,1.0,Marco Polo,"Venice, Italy",OWNO,////,////,Marco Polo / Tessera,"Venice, Veneto, Italy",OK
    VCF,////,////,////,UNK,1.0,Valcheta,"Valcheta, Argentina",OWNO,////,////,????,"Valcheta, Río Negro, Argentina",OK
    VCH,////,////,SUVO,UNK,1.0,Vichadero,"Vichadero, Uruguay",OWNO,////,////,????,"Vichadero, Rivera, Uruguay",OK
    VCL,////,////,VVCA,UNK,nan,////,////,////,////,////,Chu Lai International Airport,"Chu Lai, Vietnam",UNK
    VCP,////,////,SBKP,UNK,0.75,Viracopos,"Sao Paulo, Brazil",OWNO,////,////,Viracopos-Campinas International Airport,"Campinas, São Paulo, Brazil",OK
    VCR,////,////,SVCO,UNK,1.0,Carora,"Carora, Venezuela",OWNO,////,////,????,"Carora, Lara, Venezuela",OK
    VCS,////,////,VVCS,UNK,nan,////,////,////,////,////,Con Dao Airport,"Con Son Island, Con Dao Archipelago, Vietnam",UNK
    VCT,VCT,KVCT,KVCT,OK,0.473,County-Foster,"Victoria (TX), USA",OWNO,VICTORIA RGNL,"VICTORIA, TX - UNITED STATES",Victoria Regional,"Victoria, Texas, United States",OK
    VCV,VCV,KVCV,KVCV,OK,0.494,George AFB,"Victorville (CA), USA",OWNO,SOUTHERN CALIFORNIA LOGISTICS,"VICTORVILLE, CA - UNITED STATES",Southern California Logistics Airport,"Victorville, California, United States",OK
    VDA,////,////,LLOV,UNK,0.615,Ovda,"Ovda, Israel",OWNO,////,////,Ovda International Airport,"Eilat, Israel",OK
    VDB,////,////,ENFG,UNK,0.769,Valdres,"Fagernes, Norway",OWNO,////,////,Leirin,"Fagernes, Norway",OK
    VDC,////,////,SBQV,UNK,1.0,Vitoria Da Conquista,"Vitoria Da Conquista, Brazil",OWNO,////,////,Vitoria da Conquista Airport,"Vitoria da Conquista, Bahia, Brazil",OK
    VDE,////,////,GCHI,UNK,0.933,Hierro,"Valverde, Canary Islands, Spain",OWNO,////,////,El Hierro,"Valverde, El Hierro Island, Canary Islands, Spain",OK
    VDH,////,////,////,UNK,1.0,Dong Hoi,Dong Hoi,IATA,////,////,????,"Dong Hoi, Vietnam",MAYBE
    VDI,VDI,KVDI,KVDI,OK,0.722,Municipal,"Vidalia (GA), USA",OWNO,VIDALIA RGNL,"VIDALIA, GA - UNITED STATES",Vidalia Regional,"Vidalia, Georgia, United States",OK
    VDM,////,////,SAVV,UNK,0.375,Viedma,"Viedma, Argentina",OWNO,////,////,Gobernador Castello,"Viedma, Río Negro, Argentina",OK
    VDP,////,////,SVVP,UNK,0.909,Valle De Pascua,"Valle De Pascua, Venezuela",OWNO,////,////,????,"Valle de la Pascua, Guárico, Venezuela",MAYBE
    VDR,////,////,SAOD,UNK,1.0,Villa Dolores,"Villa Dolores, Argentina",OWNO,////,////,????,"Villa Dolores, Córdoba, Argentina",OK
    VDS,////,////,ENVD,UNK,1.0,Vadso,"Vadso, Norway",OWNO,////,////,????,"Vadsø, Finnmark, Norway",OK
    VDZ,VDZ,PAVD,PAVD,OK,0.621,Municipal,"Valdez (AK), USA",OWNO,VALDEZ PIONEER FIELD,"VALDEZ, AK - UNITED STATES",Valdez Pioneer Field,"Valdez, Alaska, United States",OK
    VEE,VEE,PAVE,PAVE,OK,1.0,Venetie,"Venetie (AK), USA",OWNO,VENETIE,"VENETIE, AK - UNITED STATES",????,"Venetie, Alaska, United States",OK
    VEG,////,////,////,UNK,1.0,Maikwak,"Maikwak, Guyana",OWNO,////,////,????,"Maikwak, Potaro-Siparuni, Guyana",OK
    VEL,VEL,KVEL,KVEL,OK,0.664,Vernal,"Vernal (UT), USA",OWNO,VERNAL RGNL,"VERNAL, UT - UNITED STATES",Vernal Regional,"Vernal, Utah, United States",OK
    VER,////,////,MMVR,UNK,0.44,Las Bajadas,"Veracruz, Mexico",OWNO,////,////,General Heriberto Jara Corona International Airport,"Veracruz, Veracruz, México",OK
    VEV,////,////,AGBA,UNK,1.0,Barakoma,"Barakoma, Solomon Islands",OWNO,////,////,????,"Barakoma, Vella Lavella Island, Solomon Islands",OK
    VEX,D60,////,////,OK,1.0,Municipal,"Tioga (ND), USA",OWNO,TIOGA MUNI,"TIOGA, ND - UNITED STATES",Tioga Municipal Airport,"Tioga, North Dakota, United States",OK
    VEY,////,////,BIVM,UNK,1.0,Vestmannaeyjar,"Vestmannaeyjar, Iceland",OWNO,////,////,????,"Vestmannaeyjar, Iceland",OK
    VFA,////,////,FVFA,UNK,1.0,Victoria Falls,"Victoria Falls, Zimbabwe",OWNO,////,////,????,"Victoria Falls, Zimbabwe",OK
    VGA,////,////,VOBZ,UNK,1.0,Vijayawada,"Vijayawada, India",OWNO,////,////,????,"Vijayawada, Andhra Pradesh, India",OK
    VGD,////,////,ULWW,UNK,1.0,Vologda,"Vologda, Russia",OWNO,////,////,Vologda Airport,"Vologda, Vologodskaya, Russian Federation (Russia)",OK
    VGO,////,////,LEVX,UNK,1.0,Vigo,"Vigo, Spain",OWNO,////,////,????,"Vigo, Galicia, Spain",OK
    VGS,////,////,////,UNK,1.0,General Villegas,"General Villegas, Argentina",OWNO,////,////,????,"General Villegas, Buenos Aires, Argentina",OK
    VGT,VGT,KVGT,KVGT,OK,0.598,North Air Terminal,"Las Vegas (NV), USA",OWNO,NORTH LAS VEGAS,"LAS VEGAS, NV - UNITED STATES",North Las Vegas Airport,"Las Vegas, Nevada, United States",OK
    VGZ,////,////,SKVG,UNK,0.632,Villagarzon,"Villagarzon, Colombia",OWNO,////,////,Villa Garzón Airport,"Villa Garzón, Putumayo, Colombia",OK
    VHC,////,////,FNSA,UNK,1.0,Saurimo,"Saurimo, Angola",OWNO,////,////,????,"Saurimo, Angola",OK
    VHM,////,////,ESNV,UNK,1.0,Vilhelmina,"Vilhelmina, Sweden",OWNO,////,////,????,"Vilhelmina, Västerbottens län, Sweden",OK
    VHN,VHN,KVHN,KVHN,OK,1.0,Culberson County,"Van Horn (TX), USA",OWNO,CULBERSON COUNTY,"VAN HORN, TX - UNITED STATES",Culberson County Airport,"Van Horn, Texas, United States",OK
    VHV,////,////,UENI,UNK,nan,////,////,////,////,////,Verkhnevilyujsk Airport,"Verkhnevilyujsk, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    VHY,////,////,LFLV,UNK,1.0,Charmeil,"Vichy, France",OWNO,////,////,Charmeil,"Vichy, Auvergne, France",OK
    VHZ,////,////,NTUV,UNK,1.0,Vahitahi,"Vahitahi, French Polynesia",OWNO,////,////,????,"Vahitahi, French Polynesia",OK
    VIA,////,////,SSVI,UNK,0.412,Videira,"Videira, Brazil",OWNO,////,////,Ângelo Ponzoni Municipal Airport,"Videira, Santa Catarina, Brazil",OK
    VIB,////,////,////,UNK,0.733,Villa Constitucion,"Villa Constitucion, Mexico",OWNO,////,////,????,"Ciudad Constitución, Baja California Sur, México",MAYBE
    VIC,////,////,LIPT,UNK,0.462,Vicenza,"Vicenza, Italy",OWNO,////,////,Tommaso Dal Molin,"Vicenza, Veneto, Italy",OK
    VIE,////,////,LOWW,UNK,0.759,Wien-Schwechat International,"Vienna (Wien), Austria",OWNO,////,////,Vienna International Airport,"Wien, Wien (Vienna), Austria",MAYBE
    VIF,////,////,////,UNK,0.571,Vieste,"Vieste, Italy",OWNO,////,////,Vieste Heliport,"Vieste, Apulia, Italy",OK
    VIG,////,////,SVVG,UNK,0.39,El Vigia,"El Vigia, Venezuela",OWNO,////,////,Juan Pablo Perez Alfonso,"El Vigia, Mérida, Venezuela",OK
    VIH,VIH,KVIH,KVIH,OK,1.0,Rolla National,"Vichy (MO), USA",OWNO,ROLLA NATIONAL,"ROLLA/VICHY, MO - UNITED STATES",Rolla National Airport,"Rolla/Vichy, Missouri, United States",OK
    VII,////,////,VVVH,UNK,1.0,Vinh City,"Vinh City, Viet Nam",OWNO,////,////,????,"Vinh City, Vietnam",OK
    VIJ,////,////,TUPW,UNK,1.0,Virgin Gorda,"Virgin Gorda, British Virgin Islands",OWNO,////,////,????,"Virgin Gorda, Virgin Islands (British)",OK
    VIL,////,////,GMMH,UNK,1.0,Dakhla,"Dakhla, Morocco",OWNO,////,////,Dakhla,"Villa Cisneros, Western Sahara",OK
    VIN,////,////,UKWW,UNK,0.571,Vinnica,"Vinnica, Ukraine",OWNO,////,////,Gavrishevka,"Vinnitsa, Vinnytsia, Ukraine",TO DO CHECK
    VIQ,////,////,WPVQ,UNK,1.0,Viqueque,"Viqueque, Indonesia",OWNO,////,////,????,"Viqueque, Timor-Leste (East Timor)",OK
    VIR,////,////,FAVG,UNK,1.0,Virginia,"Durban, South Africa",OWNO,////,////,Virginia Airport,"Durban, KwaZulu-Natal, South Africa",OK
    VIS,VIS,KVIS,KVIS,OK,0.771,Visalia,"Visalia (CA), USA",OWNO,VISALIA MUNI,"VISALIA, CA - UNITED STATES",Visalia Municipal Airport,"Visalia, California, United States",OK
    VIT,////,////,LEVT,UNK,1.0,Vitoria,"Vitoria, Spain",OWNO,////,////,????,"Vitoria, Basque Country, Spain",OK
    VIU,////,////,////,UNK,1.0,Viru Harbour Airstrip,"Viru, Solomon Islands",OWNO,////,////,Viru Harbour Airstrip,"Viru, Solomon Islands",OK
    VIV,////,////,////,UNK,1.0,Vivigani,"Vivigani, Papua New Guinea",OWNO,////,////,????,"Vivigani, Goodenough Island, Milne Bay, Papua-New Guinea",OK
    VIX,////,////,SBVT,UNK,1.0,Eurico de Aguiar Salles Airport,"Vitoria, Brazil",OWNO,////,////,Eurico de Aguiar Salles Airport,"Vitória, Espírito Santo, Brazil",OK
    VJB,////,////,FQXA,UNK,1.0,Xai Xai,"Xai Xai, Mozambique",OWNO,////,////,????,"Xai Xai, Mozambique",OK
    VJI,VJI,KVJI,KVJI,OK,1.0,Virginia Highlands,"Abingdon (VA), USA",OWNO,VIRGINIA HIGHLANDS,"ABINGDON, VA - UNITED STATES",Virginia Highlands Airport,"Abingdon, Virginia, United States",OK
    VJQ,////,////,////,UNK,1.0,Gurue,"Gurue, Mozambique",OWNO,////,////,????,"Gurue, Mozambique",OK
    VKG,////,////,VVRG,UNK,1.0,Rach Gia,"Rach Gia, Viet Nam",OWNO,////,////,????,"Rach Gia, Vietnam",OK
    VKO,////,////,UUWW,UNK,0.848,Vnukovo,"Moscow, Russia",OWNO,////,////,Vnukovo International Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",OK
    VKS,VKS,KVKS,KVKS,OK,0.799,Vicksburg,"Vicksburg (MS), USA",OWNO,VICKSBURG MUNI,"VICKSBURG, MS - UNITED STATES",Vicksburg Municipal Airport,"Vicksburg, Mississippi, United States",OK
    VKT,////,////,UUYW,UNK,1.0,Vorkuta,"Vorkuta, Russia",OWNO,////,////,Vorkuta Airport,"Vorkuta, Komi, Russian Federation (Russia)",OK
    VLA,VLA,KVLA,KVLA,OK,0.76,Vandalia,"Vandalia (IL), USA",OWNO,VANDALIA MUNI,"VANDALIA, IL - UNITED STATES",Vandalia Municipal Airport,"Vandalia, Illinois, United States",OK
    VLC,////,////,LEVC,UNK,0.667,Valencia,"Valencia, Spain",OWNO,////,////,Manises,"Valencia, Valenciana, Spain",OK
    VLD,VLD,KVLD,KVLD,OK,1.0,Regional,"Valdosta (GA), USA",OWNO,VALDOSTA RGNL,"VALDOSTA, GA - UNITED STATES",Valdosta Regional,"Valdosta, Georgia, United States",OK
    VLE,40G,////,////,OK,0.476,J t Robidoux,"Valle (AZ), USA",OWNO,VALLE,"GRAND CANYON, AZ - UNITED STATES",Valle Airport,"Grand Canyon, Arizona, United States",OK
    VLG,////,////,SAZV,UNK,1.0,Villa Gesell,"Villa Gesell, Argentina",OWNO,////,////,????,"Villa Gesell, Argentina",OK
    VLI,////,////,NVVV,UNK,0.889,Bauerfield,"Port Vila, Vanuatu",OWNO,////,////,Bauerfield International Airport,"Port Vila, Éfaté Island, Shéfa, Vanuatu",OK
    VLK,////,////,URRQ,UNK,1.0,Volgodonsk,"Volgodonsk, Russia",OWNO,////,////,????,"Volgodonsk, Rostovskaya, Russian Federation (Russia)",OK
    VLL,////,////,LEVD,UNK,1.0,Valladolid Airport,"Valladolid, Spain",OWNO,////,////,????,"Valladolid, Castille and León, Spain",OK
    VLM,////,////,SLVM,UNK,0.452,Villamontes,"Villamontes, Bolivia",OWNO,////,////,Teniente Coronel Rafael Pabón Airport,"Villamontes, Gran Chaco, Tarija, Bolivia",OK
    VLN,////,////,SVVA,UNK,0.421,Valencia,"Valencia, Venezuela",OWNO,////,////,Arturo Michelena International Airport,"Valencia, Carabobo, Venezuela",OK
    VLP,////,////,SWVC,UNK,0.692,Municipal,"Vila Rica, Brazil",OWNO,////,////,Aeropuerto Vila Rica,"Vila Rica, Mato Grosso, Brazil",OK
    VLR,////,////,SCLL,UNK,1.0,Vallenar,"Vallenar, Chile",OWNO,////,////,????,"Vallenar, Atacama, Chile",OK
    VLS,////,////,NVSV,UNK,1.0,Valesdir,"Valesdir, Vanuatu",OWNO,////,////,????,"Valesdir, Épi Island, Shéfa, Vanuatu",OK
    VLU,////,////,ULOL,UNK,1.0,Velikiye Luki,"Velikiye Luki, Russia",OWNO,////,////,Velikiye Luki Airport,"Velikiye Luki, Pskovskaya, Russian Federation (Russia)",OK
    VLV,////,////,SVVL,UNK,0.55,Carvajal,"Valera, Venezuela",OWNO,////,////,Dr. Antonio Nicolás Briceno,"Valera, Trujillo, Venezuela",OK
    VLY,////,////,EGOV,UNK,0.75,Valley,Anglesey,IATA,////,////,Anglesey Airport/RAF Valley,"Anglesey, Anglesey, Wales, United Kingdom",MAYBE

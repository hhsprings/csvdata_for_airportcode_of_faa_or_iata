
List for checking certainty of Great Circle Mapper (YAA - YGJ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    YAA,////,////,////,UNK,1.0,Anahim Lake,"Anahim Lake, Canada",OWNO,////,////,????,"Anahim Lake, British Columbia, Canada",OK
    YAB,////,////,////,UNK,1.0,Arctic Bay,"Arctic Bay, Canada",OWNO,////,////,Arctic Bay Airport,"Arctic Bay, Nunavut, Canada",OK
    YAC,////,////,CYAC,UNK,1.0,Cat Lake,"Cat Lake, Canada",OWNO,////,////,Cat Lake Airport,"Cat Lake, Ontario, Canada",OK
    YAD,////,////,////,UNK,1.0,Moose Lake,"Moose Lake, Canada",OWNO,////,////,????,"Moose Lake, Manitoba, Canada",OK
    YAG,////,////,CYAG,UNK,1.0,Municipal,"Fort Frances, Canada",OWNO,////,////,Municipal,"Fort Frances, Ontario, Canada",OK
    YAH,////,////,CYAH,UNK,0.667,Lagrande 4,"Lagrande 4, Canada",OWNO,////,////,????,"La Grande IV, Québec, Canada",TO DO CHECK
    YAI,////,////,SCCH,UNK,0.345,Chillan,"Chillan, Chile",OWNO,////,////,General Bernardo O'Higgins Airport,"Chillan, Bío Bío, Chile",OK
    YAJ,////,////,////,UNK,1.0,Lyall Harbour,"Lyall Harbour, Canada",OWNO,////,////,????,"Lyall Harbour, British Columbia, Canada",OK
    YAK,YAK,PAYA,PAYA,OK,1.0,Yakutat,"Yakutat (AK), USA",OWNO,YAKUTAT,"YAKUTAT, AK - UNITED STATES",????,"Yakutat, Alaska, United States",OK
    YAL,////,////,CYAL,UNK,1.0,Alert Bay,"Alert Bay, Canada",OWNO,////,////,Alert Bay Airport,"Alert Bay, British Columbia, Canada",OK
    YAM,////,CYAM,CYAM,OK,1.0,Sault Ste Marie,"Sault Ste Marie, Canada",OWNO,SAULT STE MARIE,"SAULT STE MARIE, - CANADA",????,"Sault Ste. Marie, Ontario, Canada",OK
    YAN,////,////,FZIR,UNK,1.0,Yangambi,"Yangambi, Congo (DRC)",OWNO,////,////,????,"Yangambi, Orientale, Democratic Republic of Congo (Zaire)",OK
    YAO,////,////,FKKY,UNK,0.727,Yaounde Airport,"Yaounde, Cameroon",OWNO,////,////,Ville Airport,"Yaoundé, Centre, Cameroon",OK
    YAP,T11,PTYA,PTYA,OK,0.297,Yap,"Yap, Micronesia",OWNO,YAP INTL,"YAP ISLAND, - MICRONESIA, FED STATES OF",Yap International Airport,"Yap Island, Yap, Micronesia (Federated States of)",OK
    YAQ,////,////,////,UNK,1.0,Maple Bay,"Maple Bay, Canada",OWNO,////,////,????,"Maple Bay, British Columbia, Canada",OK
    YAR,////,////,CYAD,UNK,0.762,Lagrande 3,"Lagrande 3, Canada",OWNO,////,////,????,"La Grande-3, Québec, Canada",OK
    YAS,////,////,NFSW,UNK,1.0,Yasawa Island,"Yasawa Island, Fiji",OWNO,////,////,????,"Yasawa Island, Fiji",OK
    YAT,////,////,CYAT,UNK,1.0,Attawapiskat,"Attawapiskat, Canada",OWNO,////,////,Attawapiskat Airport,"Attawapiskat, Ontario, Canada",OK
    YAU,////,////,////,UNK,1.0,Kattiniq/Donaldson Lak,"Kattiniq/Donaldson Lak, Canada",OWNO,////,////,Kattiniq/Donaldson Airport,"Kattiniq, Québec, Canada",OK
    YAV,////,////,////,UNK,1.0,Miners Bay,"Miners Bay, Canada",OWNO,////,////,????,"Miner's Bay, British Columbia, Canada",OK
    YAW,////,////,CYAW,UNK,1.0,Shearwater,"Halifax, Canada",OWNO,////,////,Shearwater,"Halifax, Nova Scotia, Canada",OK
    YAX,////,////,////,UNK,1.0,Angling Lake,"Angling Lake, Canada",OWNO,////,////,????,"Angling Lake, Ontario, Canada",OK
    YAY,////,////,CYAY,UNK,1.0,St Anthony,"St Anthony, Canada",OWNO,////,////,????,"St. Anthony, Newfoundland and Labrador, Canada",OK
    YAZ,////,CYAZ,CYAZ,OK,1.0,Tofino Airport,"Tofino, Canada",OWNO,TOFINO/LONG BEACH,"TOFINO, - CANADA",????,"Tofino, British Columbia, Canada",OK
    YBA,////,////,CYBA,UNK,1.0,Banff,"Banff, Canada",OWNO,////,////,????,"Banff, Alberta, Canada",OK
    YBB,////,////,CYBB,UNK,1.0,Kugaaruk,"Kugaaruk, Canada",OWNO,////,////,????,"Kugaaruk, Kitikmeot, Nunavut, Canada",OK
    YBC,////,////,CYBC,UNK,1.0,Baie Comeau,"Baie Comeau, Canada",OWNO,////,////,????,"Baie-Comeau, Québec, Canada",OK
    YBE,////,////,CYBE,UNK,1.0,Uranium City,"Uranium City, Canada",OWNO,////,////,????,"Uranium City, Saskatchewan, Canada",OK
    YBF,////,////,////,UNK,0.737,Bamfield,"Bamfield, Canada",OWNO,////,////,Bamfield Water Aerodrome,"Bamfield, British Columbia, Canada",OK
    YBG,////,////,CYBG,UNK,1.0,Bagotville,"Bagotville, Canada",OWNO,////,////,????,"Bagotville, Québec, Canada",OK
    YBH,////,////,CYBH,UNK,1.0,Bull Harbour,"Bull Harbour, Canada",OWNO,////,////,????,"Bull Harbour, British Columbia, Canada",OK
    YBI,////,////,////,UNK,1.0,Black Tickle,"Black Tickle, Canada",OWNO,////,////,????,"Black Tickle, Newfoundland and Labrador, Canada",OK
    YBJ,////,////,////,UNK,1.0,Baie Johan Beetz,"Baie Johan Beetz, Canada",OWNO,////,////,????,"Baie Johan Beetz, Québec, Canada",OK
    YBK,////,////,CYBK,UNK,1.0,Baker Lake,"Baker Lake, Canada",OWNO,////,////,????,"Baker Lake, Nunavut, Canada",OK
    YBL,////,CYBL,CYBL,OK,1.0,Metropolitan Area/Airport,"Campbell River, Canada",IATA+OWNO,CAMPBELL RIVER,"CAMPBELL RIVER, - CANADA",????,"Campbell River, British Columbia, Canada",OK
    YBN,////,////,CYBN,UNK,1.0,Borden,"Borden, Canada",OWNO,////,////,????,"Borden, Ontario, Canada",OK
    YBO,////,////,////,UNK,0.632,Bobquinn Lake,"Bobquinn Lake, Canada",OWNO,////,////,Bob Quinn Lake Airport,"Bob Quinn Lake, British Columbia, Canada",OK
    YBP,////,////,ZUYB,UNK,0.818,Yibin Caiba Airport,"Yibin, Sichuan, China",OWNO,////,////,????,"Yibin, Sichuan, China",OK
    YBQ,////,////,////,UNK,1.0,Telegraph Harbour,"Telegraph Harbour, Canada",OWNO,////,////,????,"Telegraph Harbour, British Columbia, Canada",OK
    YBR,////,////,CYBR,UNK,1.0,Brandon,"Brandon, Canada",OWNO,////,////,????,"Brandon, Manitoba, Canada",OK
    YBT,////,////,CYBT,UNK,1.0,Brochet,"Brochet, Canada",OWNO,////,////,????,"Brochet, Manitoba, Canada",OK
    YBV,////,////,CYBV,UNK,1.0,Berens River,"Berens River, Canada",OWNO,////,////,????,"Berens River, Manitoba, Canada",OK
    YBW,////,////,////,UNK,1.0,Bedwell Harbor,"Bedwell Harbor, Canada",OWNO,////,////,????,"Bedwell Harbor, British Columbia, Canada",OK
    YBX,////,////,CYBX,UNK,0.759,Blanc Sablon,"Blanc Sablon, Canada",OWNO,////,////,????,"Lourdes de Blanc Sablon, Québec, Canada",MAYBE
    YBY,////,////,CYBF,UNK,1.0,Bonnyville,"Bonnyville, Canada",OWNO,////,////,????,"Bonnyville, Alberta, Canada",OK
    YBZ,////,////,////,UNK,0.791,Downtown Rail Station,"Toronto, Canada",OWNO,////,////,Union Station,"Toronto, Ontario, Canada",OK
    YCA,////,////,////,UNK,0.737,Courtenay,"Courtenay, Canada",OWNO,////,////,Courtenay Airpark,"Courtenay, British Columbia, Canada",OK
    YCB,////,////,CYCB,UNK,1.0,Cambridge Bay,"Cambridge Bay, Canada",OWNO,////,////,????,"Cambridge Bay, Kitikmeot, Nunavut, Canada",OK
    YCC,////,////,CYCC,UNK,1.0,Regional,"Cornwall, Canada",OWNO,////,////,Regional,"Cornwall, Ontario, Canada",OK
    YCD,////,CYCD,CYCD,OK,1.0,Nanaimo Airport,"Nanaimo, Canada",OWNO,NANAIMO,"NANAIMO, - CANADA",Nanaimo Airport,"Nanaimo, British Columbia, Canada",OK
    YCE,////,////,CYCE,UNK,0.609,Centralia,"Centralia, Canada",OWNO,////,////,Huron Airpark,"Centralia, Ontario, Canada",OK
    YCF,////,////,////,UNK,1.0,Cortes Bay,"Cortes Bay, Canada",OWNO,////,////,????,"Cortes Bay, British Columbia, Canada",OK
    YCG,////,////,CYCG,UNK,0.563,Castlegar,"Castlegar, Canada",OWNO,////,////,West Kootenay Regional Airport,"Castlegar, British Columbia, Canada",OK
    YCH,////,////,CYCH,UNK,1.0,Miramichi,"Miramichi, Canada",OWNO,////,////,????,"Miramichi, New Brunswick, Canada",OK
    YCI,////,////,CWCI,UNK,1.0,Caribou Island,"Caribou Island, Canada",OWNO,////,////,????,"Caribou Island, Ontario, Canada",OK
    YCK,////,////,CYVL,UNK,0.667,Colville Lake,"Colville Lake, Canada",OWNO,////,////,Tommy Kochon Aerodrome,"Colville Lake, Northwest Territories, Canada",OK
    YCL,////,////,CYCL,UNK,1.0,Charlo,"Charlo, Canada",OWNO,////,////,????,"Charlo, New Brunswick, Canada",OK
    YCM,////,CYSN,CYSN,OK,1.0,St Catharines,"St Catharines, Canada",OWNO,ST CATHARINES/NIAGARA DISTRICT,"ST CATHARINES, - CANADA",????,"St. Catharines, Ontario, Canada",OK
    YCN,////,////,CYCN,UNK,1.0,Cochrane,"Cochrane, Canada",OWNO,////,////,????,"Cochrane, Ontario, Canada",OK
    YCO,////,////,CYCO,UNK,1.0,Kugluktuk,"Kugluktuk/Coppermine, Canada",OWNO,////,////,Kugluktuk,"Kugluktuk, Kitikmeot, Nunavut, Canada",OK
    YCP,////,////,CYCP,UNK,0.667,Co-op Point,"Co-op Point, Canada",OWNO,////,////,Blue River,"Co-op Point, British Columbia, Canada",OK
    YCQ,////,////,CYCQ,UNK,1.0,Chetwynd,"Chetwynd, Canada",OWNO,////,////,????,"Chetwynd, British Columbia, Canada",OK
    YCR,////,////,CYCR,UNK,0.462,Cross Lake,"Cross Lake, Canada",OWNO,////,////,Charlie Sinclair Memorial,"Cross Lake, Manitoba, Canada",OK
    YCS,////,////,CYCS,UNK,1.0,Chesterfield Inlet,"Chesterfield Inlet, Canada",OWNO,////,////,????,"Chesterfield Inlet, Nunavut, Canada",OK
    YCT,////,////,CYCT,UNK,1.0,Coronation,"Coronation, Canada",OWNO,////,////,????,"Coronation, Alberta, Canada",OK
    YCU,////,////,ZBYC,UNK,nan,////,////,////,////,////,Yuncheng Guangong Airport,"Yuncheng, Shaanxi, China",UNK
    YCW,////,////,CYCW,UNK,1.0,Chilliwack,"Chilliwack, Canada",OWNO,////,////,????,"Chilliwack, British Columbia, Canada",OK
    YCY,////,////,CYCY,UNK,1.0,Clyde River,"Clyde River, Canada",OWNO,////,////,????,"Clyde River, Nunavut, Canada",OK
    YCZ,////,////,CYCZ,UNK,0.929,Fairmount Springs,"Fairmount Springs, Canada",OWNO,////,////,????,"Fairmont Hot Springs, Alberta, Canada",MAYBE
    YDA,////,CYDA,CYDA,OK,1.0,Dawson City,"Dawson City, Canada",OWNO,DAWSON CITY,"DAWSON CITY, - CANADA",????,"Dawson City, Yukon, Canada",OK
    YDB,////,CYDB,CYDB,OK,0.399,Burwash Landings,"Burwash Landings, Canada",OWNO,BURWASH,"BURWASH, - CANADA",????,"Burwash Landings, Yukon, Canada",MAYBE
    YDC,////,////,////,UNK,1.0,Industrial Airport,"Drayton Valley, Canada",OWNO,////,////,Drayton Valley Industrial Aerodrome,"Drayton Valley, Alberta, Canada",OK
    YDE,////,////,////,UNK,1.0,Paradise River,"Paradise River, Canada",OWNO,////,////,????,"Paradise River, Newfoundland and Labrador, Canada",OK
    YDF,////,////,CYDF,UNK,1.0,Deer Lake,"Deer Lake, Canada",OWNO,////,////,????,"Deer Lake, Newfoundland and Labrador, Canada",OK
    YDG,////,////,CYID,UNK,1.0,Digby,"Digby, Canada",OWNO,////,////,????,"Digby, Nova Scotia, Canada",OK
    YDJ,////,////,////,UNK,1.0,Hatchet Lake,"Hatchet Lake, Canada",OWNO,////,////,Hatchet Lake Airport,"Hatchet Lake, Saskatchewan, Canada",OK
    YDL,////,CYDL,CYDL,OK,1.0,Dease Lake,"Dease Lake, Canada",OWNO,DEASE LAKE,"DEASE LAKE, - CANADA",????,"Dease Lake, British Columbia, Canada",OK
    YDN,////,////,CYDN,UNK,0.323,Dauphin,"Dauphin, Canada",OWNO,////,////,"Lt. Colonel W.G. ""Billy"" Barker","Dauphin, Manitoba, Canada",OK
    YDO,////,////,CYDO,UNK,0.703,St Methode,"Dolbeau, Canada",OWNO,////,////,????,"Dolbeau / St-Félicien, Québec, Canada",OK
    YDP,////,////,CYDP,UNK,1.0,Nain,"Nain, Canada",OWNO,////,////,Nain Airport,"Nain, Newfoundland and Labrador, Canada",OK
    YDQ,////,////,CYDQ,UNK,1.0,Dawson Creek,"Dawson Creek, Canada",OWNO,////,////,????,"Dawson Creek, British Columbia, Canada",OK
    YDT,////,CZBB,CZBB,OK,1.0,Boundary Bay,"Vancouver, Canada",OWNO,VANCOUVER/BOUNDARY BAY,"BOUNDARY BAY, - CANADA",Boundary Bay,"Delta, British Columbia, Canada",OK
    YDV,////,////,CZTA,UNK,1.0,Bloodvein,"Bloodvein, Canada",OWNO,////,////,????,"Bloodvein, Manitoba, Canada",OK
    YEA,////,////,////,UNK,1.0,Metropolitan Area,"Edmonton, Canada",OWNO,////,////,Metropolitan Area,"Edmonton, Alberta, Canada",OK
    YEC,////,////,RKTY,UNK,0.8,Yechon,"Yechon, South Korea",OWNO,////,////,K 58,"Yechon, Republic of Korea (South Korea)",OK
    YED,////,////,CYED,UNK,0.667,Namao Fld,"Edmonton, Canada",OWNO,////,////,CFB Edmonton,"Edmonton, Alberta, Canada",OK
    YEE,////,////,CYEE,UNK,nan,////,////,////,////,////,Huronia,"Midland, Ontario, Canada",UNK
    YEG,////,////,CYEG,UNK,1.0,International,"Edmonton, Canada",OWNO,////,////,Edmonton International Airport,"Edmonton, Alberta, Canada",OK
    YEH,////,////,OIBI,UNK,nan,////,////,////,////,////,????,"Asaloyeh, Bushehr, Iran",UNK
    YEI,////,////,LTBR,UNK,1.0,Yenisehir,"Bursa, Turkey",OWNO,////,////,Yenisehir,"Bursa, Bursa, Turkey",OK
    YEK,////,////,CYEK,UNK,1.0,Arviat,"Arviat, Canada",OWNO,////,////,Arviat,"Arviat, Kivalliq, Nunavut, Canada",OK
    YEL,////,////,CYEL,UNK,0.815,Elliot Lake,"Elliot Lake, Canada",OWNO,////,////,Municipal,"Elliot Lake, Ontario, Canada",OK
    YEM,////,////,CYEM,UNK,0.936,East Manitoulin,"Manitowaning, Canada",OWNO,////,////,Manitoulin East Municipal,"Manitowaning, Ontario, Canada",OK
    YEN,////,////,CYEN,UNK,1.0,Estevan,"Estevan, Canada",OWNO,////,////,????,"Estevan, Saskatchewan, Canada",OK
    YEO,////,////,EGDY,UNK,1.0,Yeovilton,"Yeovilton, United Kingdom",OWNO,////,////,????,"Yeovilton, Somerset, England, United Kingdom",OK
    YEQ,////,////,////,UNK,1.0,Yenkis,"Yenkis, Papua New Guinea",OWNO,////,////,????,"Yenkis, East Sepik, Papua-New Guinea",OK
    YER,////,////,CYER,UNK,1.0,Fort Severn,"Fort Severn, Canada",OWNO,////,////,????,"Fort Severn, Ontario, Canada",OK
    YES,////,////,OISY,UNK,nan,////,////,////,////,////,????,"Yasuj, Kohgiluyeh va Buyer Ahmad, Iran",UNK
    YET,////,////,CYET,UNK,1.0,Edson,"Edson, Canada",OWNO,////,////,????,"Edson, Alberta, Canada",OK
    YEU,////,////,CYEU,UNK,1.0,Eureka,"Eureka, Canada",OWNO,////,////,????,"Eureka, Northwest Territories, Canada",OK
    YEV,////,////,CYEV,UNK,1.0,Inuvik/Mike Zubko,"Inuvik, Canada",OWNO,////,////,Inuvik Mike Zubko Airport,"Inuvik, Northwest Territories, Canada",OK
    YEY,////,CYEY,CYEY,OK,0.7,Amos,"Amos, Canada",OWNO,AMOS/MAGNY,"AMOS, - CANADA",Municipal,"Amos, Québec, Canada",OK
    YFA,////,////,CYFA,UNK,1.0,Fort Albany,"Fort Albany, Canada",OWNO,////,////,????,"Fort Albany, Ontario, Canada",OK
    YFB,////,////,CYFB,UNK,0.5,Iqaluit,"Iqaluit, Canada",OWNO,////,////,Frobisher Bay,"Iqaluit, Nunavut, Canada",OK
    YFC,////,////,CYFC,UNK,1.0,Fredericton,"Fredericton, Canada",OWNO,////,////,????,"Fredericton, New Brunswick, Canada",OK
    YFD,////,////,CYFD,UNK,nan,////,////,////,////,////,????,"Brantford, Ontario, Canada",UNK
    YFE,////,////,CYFE,UNK,1.0,Forestville,"Forestville, Canada",OWNO,////,////,????,"Forestville, Québec, Canada",OK
    YFG,////,////,////,UNK,0.941,Fontanges,"Fontanges, Canada",OWNO,////,////,Fontages Airport,"Fontanges, Québec, Canada",OK
    YFH,////,////,CYFH,UNK,1.0,Fort Hope,"Fort Hope, Canada",OWNO,////,////,????,"Fort Hope, Ontario, Canada",OK
    YFL,////,////,CYFL,UNK,1.0,Fort Reliance,"Fort Reliance, Canada",OWNO,////,////,????,"Fort Reliance, Northwest Territories, Canada",OK
    YFO,////,////,CYFO,UNK,1.0,Flin Flon,"Flin Flon, Canada",OWNO,////,////,????,"Flin Flon, Manitoba, Canada",OK
    YFR,////,////,CYFR,UNK,1.0,Fort Resolution,"Fort Resolution, Canada",OWNO,////,////,????,"Fort Resolution, Northwest Territories, Canada",OK
    YFS,////,////,CYFS,UNK,1.0,Fort Simpson,"Fort Simpson, Canada",OWNO,////,////,????,"Fort Simpson, Northwest Territories, Canada",OK
    YFX,////,////,////,UNK,1.0,St Lewis,"Fox Harbour, Canada",OWNO,////,////,Fox Harbour,"St. Lewis, Newfoundland and Labrador, Canada",OK
    YGB,////,////,CYGB,UNK,1.0,Gillies Bay,"Gillies Bay, Canada",OWNO,////,////,????,"Gillies Bay, British Columbia, Canada",OK
    YGC,////,////,////,UNK,1.0,Grande Cache,"Grande Cache, Canada",OWNO,////,////,????,"Grande Cache, Alberta, Canada",OK
    YGE,////,////,////,UNK,1.0,Gorge Harbor,"Gorge Harbor, Canada",OWNO,////,////,????,"Gorge Harbor, British Columbia, Canada",OK
    YGG,////,////,////,UNK,1.0,Ganges Harbor,"Ganges Harbor, Canada",OWNO,////,////,????,"Ganges Harbor, British Columbia, Canada",OK
    YGH,////,////,CYGH,UNK,1.0,Fort Good Hope,"Fort Good Hope, Canada",OWNO,////,////,????,"Fort Good Hope, Northwest Territories, Canada",OK
    YGJ,////,////,RJOH,UNK,1.0,Miho,"Yonago, Japan",OWNO,////,////,Miho-Yonago Airport,"Yonago, Tottori, Japan",OK


List for checking certainty of Great Circle Mapper (TEP - TKD)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    TEP,////,////,////,UNK,1.0,Teptep,"Teptep, Papua New Guinea",OWNO,////,////,????,"Teptep, Madang, Papua-New Guinea",OK
    TEQ,////,////,LTBU,UNK,1.0,Corlu,"Tekirdag, Turkey",OWNO,////,////,Corlu,"Tekirdag, Tekirdag, Turkey",OK
    TER,////,////,LPLA,UNK,0.653,Lajes,"Terceira Island, Portugal",OWNO,////,////,Aeroporto das Lajes,"Terceira, Região Autónoma dos Açores (Azores), Portugal",MAYBE
    TES,////,////,HHTS,UNK,0.909,Tessenei,"Tessenei, Eritrea",OWNO,////,////,????,"Teseney, Eritrea",OK
    TET,////,////,FQTT,UNK,1.0,Matunda,"Tete, Mozambique",OWNO,////,////,Matunda,"Tete, Mozambique",OK
    TEU,////,////,NZMO,UNK,1.0,Manapouri,"Te Anau, New Zealand",OWNO,////,////,Manapouri,"Te Anau, New Zealand",OK
    TEX,TEX,KTEX,KTEX,OK,0.778,Telluride,"Telluride (CO), USA",OWNO,TELLURIDE RGNL,"TELLURIDE, CO - UNITED STATES",Telluride Regional,"Telluride, Colorado, United States",OK
    TEY,////,////,BITE,UNK,1.0,Thingeyri,"Thingeyri, Iceland",OWNO,////,////,????,"Þingeyri, Iceland",OK
    TEZ,////,////,VETZ,UNK,0.692,Salonibari,"Tezpur, India",OWNO,////,////,????,"Tezpur, Assam, India",OK
    TFF,////,////,SBTF,UNK,1.0,Tefe,"Tefe, Brazil",OWNO,////,////,????,"Tefé, Amazonas, Brazil",OK
    TFI,////,////,////,UNK,1.0,Tufi,"Tufi, Papua New Guinea",OWNO,////,////,????,"Tufi, Northern, Papua-New Guinea",OK
    TFL,////,////,SNTO,UNK,0.542,Teofilo Otoni,"Teofilo Otoni, Brazil",OWNO,////,////,Juscelino Kubitscheck,"Teófilo Otoni, Minas Gerais, Brazil",OK
    TFM,////,////,////,UNK,1.0,Telefomin,"Telefomin, Papua New Guinea",OWNO,////,////,????,"Telefomin, Sandaun, Papua-New Guinea",OK
    TFN,////,////,GCXO,UNK,0.857,Norte Los Rodeos,"Tenerife, Canary Islands, Spain",OWNO,////,////,Tenerife Norte,"Santa Cruz de Tenerife, Tenerife Island, Canary Islands, Spain",OK
    TFS,////,////,GCTS,UNK,0.824,Sur Reina Sofia,"Tenerife, Canary Islands, Spain",OWNO,////,////,Tenerife Sur,"Granadilla de Abone, Tenerife Island, Canary Islands, Spain",OK
    TFT,////,////,OPTT,UNK,1.0,Taftan,"Taftan, Pakistan",OWNO,////,////,????,"Taftan, Balochistan, Pakistan",OK
    TGA,////,////,WSAT,UNK,nan,////,////,////,////,////,RSAF Tengah AB,"Singapore, Singapore",UNK
    TGC,////,////,WBTM,UNK,nan,////,////,////,////,////,????,"Tanjung Manis, Sarawak, Malaysia",UNK
    TGD,////,////,LYPG,UNK,0.643,Golubovci,"Podgorica, Montenegro",OWNO,////,////,????,"Podgorica, Montenegro",OK
    TGE,06A,////,////,OK,0.666,Sharpe Field,"Tuskegee (AL), USA",OWNO,MOTON FIELD MUNI,"TUSKEGEE, AL - UNITED STATES",Moton Field Municipal Airport,"Tuskegee, Alabama, United States",OK
    TGG,////,////,WMKN,UNK,1.0,Sultan Mahmood,"Kuala Terengganu, Malaysia",OWNO,////,////,Sultan Mahmud,"Kuala Terengganu, Terengganu, Malaysia",OK
    TGH,////,////,NVST,UNK,0.632,Tongoa,"Tongoa, Vanuatu",OWNO,////,////,????,"Tongoa Island, Shepherd Islands, Shéfa, Vanuatu",MAYBE
    TGI,////,////,SPGM,UNK,1.0,Tingo Maria,"Tingo Maria, Peru",OWNO,////,////,Tingo María Airport,"Tingo María, Huánuco, Perú",OK
    TGJ,////,////,NWWA,UNK,1.0,Tiga,"Tiga, New Caledonia",OWNO,////,////,????,"Tiga, New Caledonia",OK
    TGK,////,////,URRT,UNK,nan,////,////,////,////,////,Yuzhny Airport,"Taganrog, Rostovskaya, Russian Federation (Russia)",UNK
    TGL,////,////,////,UNK,1.0,Tagula,"Tagula, Papua New Guinea",OWNO,////,////,????,"Tagula, Milne Bay, Papua-New Guinea",OK
    TGM,////,////,LRTM,UNK,0.5,Tirgu Mures,"Tirgu Mures, Romania",OWNO,////,////,Transilvania Targu Mures International Airport,"Targu Mures, Romania",OK
    TGN,////,////,YLTV,UNK,0.744,La Trobe Regional,"Traralgon, Australia",OWNO,////,////,Latrobe Valley Regional,"Traralgon, Victoria, Australia",OK
    TGO,////,////,ZBTL,UNK,1.0,Tongliao,"Tongliao, PR China",OWNO,////,////,????,"Tongliao, Inner Mongolia, China",OK
    TGP,////,////,UNIP,UNK,nan,////,////,////,////,////,Podkamennaya Tunguska Airport,"Bor, Krasnoyarskiy, Russian Federation (Russia)",UNK
    TGR,////,////,DAUK,UNK,0.621,Touggourt,"Touggourt, Algeria",OWNO,////,////,Sidi Mahdi Airport,"Touggourt, Ouargla, Algeria",OK
    TGS,////,////,////,UNK,1.0,Chokwe,"Chokwe, Mozambique",OWNO,////,////,????,"Chokwé, Mozambique",OK
    TGT,////,////,HTTG,UNK,1.0,Tanga,"Tanga, Tanzania",OWNO,////,////,Tanga Airport,"Tanga, Tanga, Tanzania",OK
    TGU,////,////,MHTG,UNK,0.909,Toncontin,"Tegucigalpa, Honduras",OWNO,////,////,Toncontín International Airport,"Tegucigalpa, Francisco Morazán, Honduras",OK
    TGV,////,////,////,UNK,1.0,Targovishte,"Targovishte, Bulgaria",OWNO,////,////,????,"Targovishte, Plovdiv, Bulgaria",OK
    TGZ,////,////,MMTG,UNK,0.571,Llano San Juan,"Tuxtla Gutierrez, Mexico",OWNO,////,////,Ángel Albino Corzo International Airport,"Tuxtla Gutiérrez, Chiapas, México",OK
    THA,THA,KTHA,KTHA,OK,0.726,Northern,"Tullahoma (TN), USA",OWNO,TULLAHOMA RGNL ARPT/WM NORTHERN FIELD,"TULLAHOMA, TN - UNITED STATES",Tullahoma Regional Airport/William Northern Field,"Tullahoma, Tennessee, United States",OK
    THB,////,////,FXTA,UNK,1.0,Thaba-Tseka,"Thaba-Tseka, Lesotho",OWNO,////,////,????,"Thaba-Tseka, Lesotho",OK
    THC,////,////,GLTN,UNK,1.0,Tchien,"Tchien, Liberia",OWNO,////,////,????,"Tchien, Liberia",OK
    THD,////,////,VVTX,UNK,1.0,Tho Xuan,Thanh Hoa,IATA,////,////,Tho Xuan Airport,"Tho Xuan, Vietnam",MAYBE
    THE,////,////,SBTE,UNK,1.0,Senador Petronio Portella Airport,"Teresina, Brazil",OWNO,////,////,Senador Petrônio Portella,"Teresina, Piauí, Brazil",OK
    THG,////,////,YTNG,UNK,1.0,Thangool,"Thangool, Australia",OWNO,////,////,????,"Thangool, Queensland, Australia",OK
    THH,////,////,NZTS,UNK,1.0,Taharoa,"Taharoa, New Zealand",OWNO,////,////,????,"Taharoa, New Zealand",OK
    THI,////,////,GQNC,UNK,1.0,Tichitt,"Tichitt, Mauritania",OWNO,////,////,Tichit Airport,"Tichit, Mauritania",OK
    THK,////,////,VLTK,UNK,1.0,Thakhek,"Thakhek, Lao PDR",OWNO,////,////,????,"Thakhek, Lao People's Democratic Republic (Laos)",OK
    THL,////,////,VYTL,UNK,1.0,Tachilek,"Tachilek, Myanmar",OWNO,////,////,????,"Tachilek, Shan, Myanmar (Burma)",OK
    THN,////,////,ESGT,UNK,0.667,Trollhattan,"Trollhattan, Sweden",OWNO,////,////,Trollhättan-Vänersborg Airport,"Trollhättan, Västra Götalands län, Sweden",OK
    THO,////,////,BITN,UNK,1.0,Thorshofn,"Thorshofn, Iceland",OWNO,////,////,????,"Þórshöfn, Iceland",OK
    THP,THP,KTHP,KTHP,OK,0.797,Hot Springs,"Thermopolis (WY), USA",OWNO,HOT SPRINGS CO-THERMOPOLIS MUNI,"THERMOPOLIS, WY - UNITED STATES",Hot Springs Co-Thermopolis Municipal Airport,"Thermopolis, Wyoming, United States",OK
    THQ,////,////,ZLTS,UNK,nan,////,////,////,////,////,Tianshui Maijishan Airport,Tianshui,UNK
    THR,////,////,OIII,UNK,0.857,Mehrabad,"Tehran, Iran",OWNO,////,////,Mehrabad International Airport,"Tehran, Tehran, Iran",OK
    THS,////,////,VTPO,UNK,1.0,Sukhothai,"Sukhothai, Thailand",OWNO,////,////,????,"Sukhothai, Sukhothai, Thailand",OK
    THT,////,////,GQNT,UNK,1.0,Tamchakett,"Tamchakett, Mauritania",OWNO,////,////,Tamchakett Airport,"Tamchakett, Mauritania",OK
    THU,////,////,BGTL,UNK,0.737,Pituffik,"Pituffik, Greenland",OWNO,////,////,Thule AB,"Pituffik, Greenland",OK
    THV,THV,KTHV,KTHV,OK,1.0,York,"York (PA), USA",OWNO,YORK,"YORK, PA - UNITED STATES",????,"York, Pennsylvania, United States",OK
    THW,////,////,////,UNK,0.39,Harbour SPB,Trincomalee,IATA,////,////,Trincomalee Harbour Waterdrome,"Trincomalee, Eastern Province, Sri Lanka (Ceylon)",MAYBE
    THX,////,////,UOTT,UNK,nan,////,////,////,////,////,????,"Turukhansk, Krasnoyarskiy, Russian Federation (Russia)",UNK
    THY,////,////,FATH,UNK,0.647,Thohoyandou,"Thohoyandou, South Africa",OWNO,////,////,P.R. Mphephu Airport,"Thohoyandou, Limpopo, South Africa",OK
    THZ,////,////,DRRT,UNK,1.0,Tahoua,"Tahoua, Niger",OWNO,////,////,????,"Tahoua, Niger",OK
    TIA,////,////,LATI,UNK,0.75,Rinas,"Tirana, Albania",OWNO,////,////,Rinas Mother Teresa,"Tiranë, Albania",OK
    TIB,////,////,SKTB,UNK,1.0,Tibu,"Tibu, Colombia",OWNO,////,////,Tibú Airport,"Tibú, Norte de Santander, Colombia",OK
    TIC,N18,////,////,OK,0.391,Tinak Island,"Tinak Island, Marshall Islands",OWNO,TINAK,"ARNO ATOLL, - MARSHALL ISLANDS",Tinak,"Arno Atoll, Marshall Islands",MAYBE
    TID,////,////,DAOB,UNK,0.632,Bouchekif,"Tiaret, Algeria",OWNO,////,////,Abdelhafid Boussouf Bou Chekif Airport,"Tiaret, Tiaret, Algeria",OK
    TIE,////,////,HATP,UNK,1.0,Tippi,"Tippi, Ethiopia",OWNO,////,////,Tippi Airport,"Tippi, SNNPR, Ethiopia",OK
    TIF,////,////,OETF,UNK,1.0,Taif,"Taif, Saudi Arabia",OWNO,////,////,Taif Airport,"Ta'if, Saudi Arabia",OK
    TIG,////,////,////,UNK,1.0,Tingwon,"Tingwon, Papua New Guinea",OWNO,////,////,????,"Tingwon, New Ireland, Papua-New Guinea",OK
    TIH,////,////,NTGC,UNK,1.0,Tikehau Atoll,"Tikehau Atoll, French Polynesia",OWNO,////,////,????,"Tikehau Atoll, French Polynesia",OK
    TII,////,////,OATN,UNK,0.667,Tirinkot,"Tirinkot, Afghanistan",OWNO,////,////,Tarin Kowt,"Tereen, Urozgan, Afghanistan",OK
    TIJ,////,////,MMTJ,UNK,0.586,Rodriguez,"Tijuana, Mexico",OWNO,////,////,General Abelardo L. Rodríguez International Airport,"Tijuana, Baja California, México",OK
    TIK,TIK,KTIK,KTIK,OK,1.0,Tinker AFB,"Oklahoma City (OK), USA",OWNO,TINKER AFB,"OKLAHOMA CITY, OK - UNITED STATES",Tinker AFB,"Oklahoma City, Oklahoma, United States",OK
    TIM,////,////,WABP,UNK,0.667,Timika,"Tembagapura, Indonesia",OWNO,////,////,Moses Kilangin,"Timika, Irian Jaya Island, Papua, Indonesia",OK
    TIN,////,////,DAOF,UNK,1.0,Tindouf,"Tindouf, Algeria",OWNO,////,////,Tindouf Airport,"Tindouf, Tindouf, Algeria",OK
    TIO,////,////,VYHN,UNK,1.0,Tilin,"Tilin, Myanmar",OWNO,////,////,????,"Tilin, Magway, Myanmar (Burma)",OK
    TIP,////,////,HLLT,UNK,1.0,International,"Tripoli, Libya",OWNO,////,////,Tripoli International Airport,"Tripoli, Libyan Arab Jamahiriya (Libya)",OK
    TIQ,TNI,PGWT,PGWT,OK,0.51,Tinian,"Tinian, Northern Mariana Islands",OWNO,TINIAN INTL,"TINIAN ISLAND, CQ - UNITED STATES",Tinian International Airport,"Tinian Island, Northern Mariana Islands, United States",OK
    TIR,////,////,VOTP,UNK,1.0,Tirupati,"Tirupati, India",OWNO,////,////,Tirupati Airport,"Tirupati, Andhra Pradesh, India",OK
    TIS,////,////,YHID,UNK,0.857,Thursday Island,"Thursday Island, Australia",OWNO,////,////,Horn Island Airport,"Thursday Island, Queensland, Australia",OK
    TIU,////,////,NZTU,UNK,1.0,Timaru,"Timaru, New Zealand",OWNO,////,////,????,"Timaru, New Zealand",OK
    TIV,////,////,LYTV,UNK,nan,////,////,////,////,////,????,"Tivat, Montenegro",UNK
    TIW,TIW,KTIW,KTIW,OK,0.68,Industrial,"Tacoma (WA), USA",OWNO,TACOMA NARROWS,"TACOMA, WA - UNITED STATES",Tacoma Narrows Airport,"Tacoma, Washington, United States",OK
    TIX,TIX,KTIX,KTIX,OK,0.608,Space Center Exect.,"Titusville (FL), USA",OWNO,SPACE COAST RGNL,"TITUSVILLE, FL - UNITED STATES",Space Coast Regional,"Titusville, Florida, United States",OK
    TIY,////,////,GQND,UNK,1.0,Tidjikja,"Tidjikja, Mauritania",OWNO,////,////,????,"Tidjikja, Mauritania",OK
    TIZ,////,////,AYTA,UNK,1.0,Tari,"Tari, Papua New Guinea",OWNO,////,////,????,"Tari, Southern Highlands, Papua-New Guinea",OK
    TJA,////,////,SLTJ,UNK,0.381,Tarija,"Tarija, Bolivia",OWNO,////,////,Capitán Oriel Lea Plaza,"Tarija, Cercado, Tarija, Bolivia",OK
    TJB,////,////,WIBT,UNK,0.786,Tanjung Balai,"Tanjung Balai, Indonesia",OWNO,////,////,Sei Bati Airport,"Tanjung Balai, Riau, Indonesia",OK
    TJG,////,////,WAON,UNK,1.0,Tanjung Warukin,"Tanjung Warukin, Indonesia",OWNO,////,////,????,"Tanjung Warukin, Kalimantan Selatan (South Borneo), Indonesia",OK
    TJH,////,////,RJBT,UNK,1.0,Tajima,"Toyooka, Japan",OWNO,////,////,Tajima Airport,"Toyooka, Hyogo, Japan",OK
    TJI,////,////,MHTJ,UNK,0.783,Capiro,"Trujillo, Honduras",OWNO,////,////,????,"Trujillo, Colón, Honduras",OK
    TJK,////,////,LTAW,UNK,1.0,Tokat,"Tokat, Turkey",OWNO,////,////,????,"Tokat, Tokat, Turkey",OK
    TJL,////,////,SSTL,UNK,nan,////,////,////,////,////,Plínio Alarcom Airport,"Três Lagoas, Mato Grosso do Sul, Brazil",UNK
    TJM,////,////,USTR,UNK,0.5,Tyumen,"Tyumen, Russia",OWNO,////,////,Roschino International Airport,"Tyumen, Tyumenskaya, Russian Federation (Russia)",OK
    TJN,////,////,NTKM,UNK,1.0,Takume,"Takume, French Polynesia",OWNO,////,////,Takume Airport,"Takume, French Polynesia",OK
    TJQ,////,////,WIOD,UNK,0.72,Bulutumbang,"Tanjung Pandan, Indonesia",OWNO,////,////,H.A.S. Hanandjoeddin Airport,"Tanjung Pandan, Bangka Belitung, Indonesia",OK
    TJS,////,////,WALG,UNK,0.774,Tanjung Selor,"Tanjung Selor, Indonesia",OWNO,////,////,Tanjung Harapan Airport,"Tanjung Selor, Kalimantan Utara (North Borneo), Indonesia",OK
    TJU,////,////,UTDK,UNK,nan,////,////,////,////,////,Kulob Airport,"Kulob, Khatlon, Tajikistan",UNK
    TJV,////,////,VOTJ,UNK,1.0,Thanjavur,"Thanjavur, India",OWNO,////,////,????,"Thanjavur, Tamil Nadu, India",OK
    TKA,TKA,PATK,PATK,OK,1.0,Talkeetna,"Talkeetna (AK), USA",OWNO,TALKEETNA,"TALKEETNA, AK - UNITED STATES",????,"Talkeetna, Alaska, United States",OK
    TKB,////,////,////,UNK,1.0,Tekadu,"Tekadu, Papua New Guinea",OWNO,////,////,????,"Tekadu, Gulf, Papua-New Guinea",OK
    TKC,////,////,FKKC,UNK,1.0,Tiko,"Tiko, Cameroon",OWNO,////,////,Tiko Airport,"Tiko, South-West (Sud-Ouest), Cameroon",OK
    TKD,////,////,DGTK,UNK,1.0,Takoradi,"Takoradi, Ghana",OWNO,////,////,????,"Takoradi, Ghana",OK


List for checking certainty of Great Circle Mapper (QAK - QZN)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    QAK,////,////,SBBQ,UNK,nan,////,////,////,////,////,Major Brigadeiro do Ar Doorgal Gomes,"Barbacena, Minas Gerais, Brazil",UNK
    QAM,////,////,LFAY,UNK,1.0,Glisy Aerodrome,"Amiens, France",OWNO,////,////,Glisy,"Amiens, Picardie (Picardy), France",OK
    QAQ,////,////,LIAP,UNK,nan,////,////,////,////,////,L'Aquila-Preturo,"L'Aquila, Abruzzo, Italy",UNK
    QAR,////,////,EHDL,UNK,nan,////,////,////,////,////,Deelen AB,"Arnhem, Gelderland, Netherlands",UNK
    QBC,////,CYBD,CYBD,OK,1.0,Bella Coola,"Bella Coola, Canada",OWNO,BELLA COOLA,"BELLA COOLA, - CANADA",????,"Bella Coola, British Columbia, Canada",OK
    QCA,////,////,OEMK,UNK,nan,////,////,////,////,////,????,"Makkah, Saudi Arabia",UNK
    QCU,////,////,BGAK,UNK,nan,////,////,////,////,////,Akunnaaq Heliport,"Akunnaaq, Qaasuitsup, Greenland",UNK
    QCY,////,////,EGXC,UNK,0.8,RAF Station,"Coningsby, United Kingdom",OWNO,////,////,RAF Coningsby,"Coningsby, Lincolnshire, England, United Kingdom",OK
    QDJ,////,////,DAFI,UNK,nan,////,////,////,////,////,Tletsi Airport,"Djelfa, Djelfa, Algeria",UNK
    QDU,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Düsseldorf, North Rhine-Westphalia, Germany",UNK
    QFD,////,////,DAAK,UNK,nan,////,////,////,////,////,Boufarik Airport,"Boufarik, Blida, Algeria",UNK
    QFO,////,////,EGSU,UNK,nan,////,////,////,////,////,????,"Duxford, Cambridgeshire, England, United Kingdom",UNK
    QFR,////,////,LIRH,UNK,nan,////,////,////,////,////,Frosinone Girolamo Moscardini Airport,"Frosinone, Lazio, Italy",UNK
    QGU,////,////,RJNG,UNK,nan,////,////,////,////,////,Gifu Air Airfield,"Gifu, Gifu, Japan",UNK
    QGY,////,////,LHPR,UNK,nan,////,////,////,////,////,Gyor-Pér International Airport,"Gyor-Pér, Gyor-Moson-Sopron, Hungary",UNK
    QHA,////,////,EBZH,UNK,nan,////,////,////,////,////,Kiewit Airfield,"Hasselt, Limburg, Belgium",UNK
    QHP,////,////,SBTA,UNK,nan,////,////,////,////,////,Base de Aviação de Taubaté,"Taubaté, São Paulo, Brazil",UNK
    QIE,////,////,LFMI,UNK,nan,////,////,////,////,////,Istres-Le Tubé AB,"Istres, Provence-Alpes-Côte d'Azur, France",UNK
    QIG,////,////,SNIG,UNK,nan,////,////,////,////,////,????,"Iguatu, Ceará, Brazil",UNK
    QJB,////,////,OEJB,UNK,nan,////,////,////,////,////,Jubail Airport,"Jubail, Saudi Arabia",UNK
    QKC,////,////,OIIP,UNK,nan,////,////,////,////,////,Payam,"Karaj, Tehran, Iran",UNK
    QKL,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Cologne-Bonn, North Rhine-Westphalia, Germany",UNK
    QKN,////,////,DTTK,UNK,nan,////,////,////,////,////,????,"Kairouan, Tunisia",UNK
    QLA,////,////,EGHL,UNK,nan,////,////,////,////,////,????,"Lasham, Hampshire, England, United Kingdom",UNK
    QLD,////,////,DAAB,UNK,nan,////,////,////,////,////,Blida Airport,"Blida, Blida, Algeria",UNK
    QLF,////,////,EFLA,UNK,nan,////,////,////,////,////,????,"Lahti-Vesivehmaa, Päijät-Häme (Päijänne-Tavastland (Päijänne Tavastia)), Finland",UNK
    QLS,////,////,LSGL,UNK,nan,////,////,////,////,////,Lausanne Airport,"Lausanne/Blécherette, Vaud, Switzerland",UNK
    QLT,////,////,LIRL,UNK,nan,////,////,////,////,////,E Comani,"Latina, Lazio, Italy",UNK
    QME,////,////,LICF,UNK,nan,////,////,////,////,////,????,"Messina, Sicily, Italy",UNK
    QMJ,////,////,OIAI,UNK,nan,////,////,////,////,////,Shahid Asyaee,"Masjed Soleiman, Khuzestan, Iran",UNK
    QMZ,////,////,EDFZ,UNK,nan,////,////,////,////,////,????,"Mainz Finthen, Rhineland-Palatinate, Germany",UNK
    QNC,////,////,LSGN,UNK,nan,////,////,////,////,////,Neuchâtel Airport,"Neuchâtel, Neuchâtel, Switzerland",UNK
    QNJ,////,////,LFLI,UNK,nan,////,////,////,////,////,????,"Annemasse, Rhône-Alpes, France",UNK
    QNM,////,////,EBNM,UNK,nan,////,////,////,////,////,Namur-Suarlée Airport,"Namur, Namur, Belgium",UNK
    QNX,////,////,LFLM,UNK,nan,////,////,////,////,////,Charnay,"Macon, Bourgogne (Burgundy), France",UNK
    QOB,////,////,EDQF,UNK,nan,////,////,////,////,////,????,"Ansbach-Petersdorf, Bavaria, Germany",UNK
    QOE,////,////,ETNN,UNK,nan,////,////,////,////,////,????,"Nörvenich, North Rhine-Westphalia, Germany",UNK
    QOW,////,////,DNIM,UNK,nan,////,////,////,////,////,Sam Mbakwe Airport,"Owerri, Imo, Nigeria",UNK
    QPA,////,////,LIPU,UNK,nan,////,////,////,////,////,Gino Allegri,"Padua, Veneto, Italy",UNK
    QPD,////,////,MUPR,UNK,nan,////,////,////,////,////,????,"Pinar del Río, Pinar del Río, Cuba",UNK
    QPG,////,////,WSAP,UNK,0.842,Paya Lebar Air Base,"Singapore, Singapore",OWNO,////,////,Paya Lebar Airport,"Singapore, Singapore",OK
    QPS,////,////,SBYS,UNK,nan,////,////,////,////,////,Campo Fontenelle,"Pirassununga, São Paulo, Brazil",UNK
    QPZ,////,////,LIMS,UNK,nan,////,////,////,////,////,????,"Piacenza, Emilia-Romagna, Italy",UNK
    QQK,////,////,////,UNK,nan,////,////,////,////,////,Kings Cross Railway Station,"London, England, United Kingdom",UNK
    QQP,////,////,////,UNK,nan,////,////,////,////,////,Paddington Railway Station,"London, England, United Kingdom",UNK
    QQS,////,////,////,UNK,nan,////,////,////,////,////,St. Pancras Railway Station,"London, England, United Kingdom",UNK
    QQU,////,////,////,UNK,nan,////,////,////,////,////,Euston Railway Station,"London, England, United Kingdom",UNK
    QQW,////,////,////,UNK,nan,////,////,////,////,////,Waterloo Railway Station,"London, England, United Kingdom",UNK
    QRA,////,////,FAGM,UNK,0.923,Rand Germiston Airport,"Johannesburg, South Africa",OWNO,////,////,Rand/Germiston Airport,"Johannesburg, Gauteng, South Africa",OK
    QRC,////,////,SCRG,UNK,nan,////,////,////,////,////,De La Independencia Airport,"Rancagua, Libertador General Bernardo O'Higgins, Chile",UNK
    QRO,////,////,MMQT,UNK,0.424,Querétaro International Airport,"Queretaro, Mexico",OWNO,////,////,Ingeniero Fernando Espinoza Gutiérrez International Airport,"Querétaro, Querétaro, México",OK
    QRT,////,////,LIQN,UNK,nan,////,////,////,////,////,????,"Rieti, Lazio, Italy",UNK
    QRW,////,////,DNSU,UNK,nan,////,////,////,////,////,Warri Airport,"Warri, Delta, Nigeria",UNK
    QSA,////,////,LELL,UNK,nan,////,////,////,////,////,????,"Sabadell, Catalonia, Spain",UNK
    QSC,////,////,SDSC,UNK,nan,////,////,////,////,////,Francisco Pereira Lopez,"São Carlos, São Paulo, Brazil",UNK
    QSF,////,////,DAAS,UNK,nan,////,////,////,////,////,Ain Arnat Airport,"Sétif, Sétif, Algeria",UNK
    QSR,////,////,LIRI,UNK,nan,////,////,////,////,////,Pontecagnano,"Salerno, Campania, Italy",UNK
    QUB,////,////,HLUB,UNK,1.0,Ubari,"Ubari, Libya",OWNO,////,////,????,"Ubari, Libyan Arab Jamahiriya (Libya)",OK
    QUG,////,////,EGHR,UNK,1.0,Goodwood Aerodrome,"Chichester, United Kingdom",OWNO,////,////,Chichester/Goodwood Aerodrome,"Chichester, West Sussex, England, United Kingdom",OK
    QUO,////,////,DNAI,UNK,nan,////,////,////,////,////,Akwa Ibom International Airport,"Uyo, Akwa Ibom, Nigeria",UNK
    QUS,////,////,DNGU,UNK,nan,////,////,////,////,////,Gusau Airstrip,"Gusau, Zamfara, Nigeria",UNK
    QUT,////,////,RJTU,UNK,nan,////,////,////,////,////,Utsunomiya Air Field,"Utsunomiya, Tochigi, Japan",UNK
    QUY,////,////,EGUY,UNK,nan,////,////,////,////,////,????,"Wyton, Huntingdonshire, England, United Kingdom",UNK
    QWG,8A6,////,////,OK,1.0,Wilgrove Air Park,"Charlotte (NC), USA",OWNO,WILGROVE AIR PARK,"CHARLOTTE, NC - UNITED STATES",Wilgrove Air Park,"Charlotte, North Carolina, United States",OK
    QXB,////,////,////,UNK,nan,////,////,////,////,////,Off-line Pt,"Aix-en-Provence, France",UNK
    QXH,////,////,EDAZ,UNK,nan,////,////,////,////,////,????,"Schönhagen, Brandenburg, Germany",UNK
    QYI,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Hilversum, Noord-Holland (North Holland), Netherlands",UNK
    QYR,////,////,LFQB,UNK,nan,////,////,////,////,////,Barberey,"Troyes, Champagne-Ardenne, France",UNK
    QZN,////,////,DAAZ,UNK,nan,////,////,////,////,////,Relizane Airport,"Relizane, Relizane, Algeria",UNK


List for checking certainty of Great Circle Mapper (RAA - RHO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    RAA,////,////,////,UNK,1.0,Rakanda,"Rakanda, Papua New Guinea",OWNO,////,////,????,"Rakanda, East New Britain, Papua-New Guinea",OK
    RAB,////,////,AYTK,UNK,0.769,Tokua Airport,"Rabaul, Papua New Guinea",OWNO,////,////,Rabaul Airport,"Kokopo, East New Britain, Papua-New Guinea",OK
    RAC,RAC,KRAC,KRAC,OK,0.536,Horlick,"Racine (WI), USA",OWNO,JOHN H BATTEN,"RACINE, WI - UNITED STATES",John H Batten Airport,"Racine, Wisconsin, United States",OK
    RAE,////,////,OERR,UNK,1.0,Arar,"Arar, Saudi Arabia",OWNO,////,////,Arar Airport,"Arar, Saudi Arabia",OK
    RAG,////,////,NZRA,UNK,1.0,Raglan,"Raglan, New Zealand",OWNO,////,////,????,"Raglan, New Zealand",OK
    RAH,////,////,OERF,UNK,1.0,Rafha,"Rafha, Saudi Arabia",OWNO,////,////,Rafha Airport,"Rafha, Saudi Arabia",OK
    RAI,////,////,GVNP,UNK,0.444,Francisco Mendes Airport,"Praia, Cape Verde",OWNO,////,////,Praia International Airport,"Praia, Santiago Island, Cape Verde",OK
    RAJ,////,////,VARK,UNK,0.706,Rajkot Civil Airport,"Rajkot, India",OWNO,////,////,????,"Rajkot, Gujarat, India",OK
    RAK,////,////,GMMX,UNK,1.0,Menara Airport,"Marrakesh, Morocco",OWNO,////,////,Menara,"Marrakech, Morocco",OK
    RAL,RAL,KRAL,KRAL,OK,1.0,Riverside Municipal,"Riverside (CA), USA",OWNO,RIVERSIDE MUNI,"RIVERSIDE, CA - UNITED STATES",Riverside Municipal Airport,"Riverside, California, United States",OK
    RAM,////,////,YRNG,UNK,1.0,Ramingining,"Ramingining, Australia",OWNO,////,////,????,"Ramingining, Northern Territory, Australia",OK
    RAN,////,////,LIDR,UNK,1.0,La Spreta Airport,"Ravenna, Italy",OWNO,////,////,La Spreta,"Ravenna, Emilia-Romagna, Italy",OK
    RAO,////,////,SBRP,UNK,1.0,Leite Lopes,"Ribeirao Preto, Brazil",OWNO,////,////,Leite Lopes,"Riberão Preto, São Paulo, Brazil",OK
    RAP,RAP,KRAP,KRAP,OK,1.0,Regional Airport,"Rapid City (SD), USA",OWNO,RAPID CITY RGNL,"RAPID CITY, SD - UNITED STATES",Rapid City Regional,"Rapid City, South Dakota, United States",OK
    RAQ,////,////,WAWR,UNK,0.606,Sugimanuru,"Raha, Indonesia",OWNO,////,////,Sugi Manuru,"Raha, Sulawesi Tenggara, Indonesia",OK
    RAR,////,////,NCRG,UNK,0.8,Rarotonga,"Rarotonga, Cook Islands",OWNO,////,////,Rarotonga International Airport,"Avarua, Rarotonga Island, Cook Islands",OK
    RAS,////,////,OIGG,UNK,1.0,Rasht,"Rasht, Iran",OWNO,////,////,????,"Rasht, Gilan, Iran",OK
    RAT,////,////,USNR,UNK,0.563,Raduzhnyi,"Raduzhnyi, Russia",OWNO,////,////,Vareghan West,"Raduzhnyi, Khanty-Mansiyskiy, Russian Federation (Russia)",OK
    RAV,////,////,SKCN,UNK,1.0,Cravo Norte,"Cravo Norte, Colombia",OWNO,////,////,Cravo Norte Airport,"Cravo Norte, Arauca, Colombia",OK
    RAW,////,////,////,UNK,1.0,Arawa,"Arawa, Papua New Guinea",OWNO,////,////,????,"Arawa, Bougainville, Papua-New Guinea",OK
    RAX,////,////,////,UNK,1.0,Oram,"Oram, Papua New Guinea",OWNO,////,////,????,"Oram, Central, Papua-New Guinea",OK
    RAZ,////,////,OPRT,UNK,1.0,Rawala Kot,"Rawala Kot, Pakistan",OWNO,////,////,????,"Rawala Kot, Azad Jammu and Kashmir (Azad Kashmir), Pakistan",OK
    RBA,////,////,GMME,UNK,1.0,Sale Airport,"Rabat, Morocco",OWNO,////,////,Rabat-Salé,"Rabat, Morocco",OK
    RBB,////,////,SWBR,UNK,1.0,Borba,"Borba, Brazil",OWNO,////,////,????,"Borba, Amazonas, Brazil",OK
    RBC,////,////,YROI,UNK,1.0,Robinvale,"Robinvale, Australia",OWNO,////,////,????,"Robinvale, Victoria, Australia",OK
    RBD,RBD,KRBD,KRBD,OK,0.36,Redbird,"Dallas/Fort Worth (TX), USA",OWNO,DALLAS EXECUTIVE,"DALLAS, TX - UNITED STATES",Dallas Executive Airport,"Dallas, Texas, United States",OK
    RBE,////,////,////,UNK,1.0,Ratanakiri,"Ratanakiri, Cambodia",OWNO,////,////,????,"Ratanakiri, Cambodia",OK
    RBF,L35,////,////,OK,1.0,Big Bear City Airport,"Big Bear (CA), USA",OWNO,BIG BEAR CITY,"BIG BEAR CITY, CA - UNITED STATES",????,"Big Bear City, California, United States",OK
    RBG,RBG,KRBG,KRBG,OK,0.591,Municipal Airport,"Roseburg (OR), USA",OWNO,ROSEBURG RGNL,"ROSEBURG, OR - UNITED STATES",Roseburg Regional,"Roseburg, Oregon, United States",OK
    RBI,////,////,NFFR,UNK,0.533,Rabi,"Rabi, Fiji",OWNO,////,////,????,"Rabi Island, Fiji",MAYBE
    RBJ,////,////,RJCR,UNK,1.0,Rebun,"Rebun, Japan",OWNO,////,////,????,"Rebun, Rebun Island, Hokkaido, Japan",OK
    RBK,F70,////,////,OK,1.0,French Valley,"Rancho (CA), USA",OWNO,FRENCH VALLEY,"MURRIETA/TEMECULA, CA - UNITED STATES",French Valley Airport,"Murrieta/Temecula, California, United States",OK
    RBL,RBL,KRBL,KRBL,OK,0.734,Red Bluff Fss,"Red Bluff (CA), USA",OWNO,RED BLUFF MUNI,"RED BLUFF, CA - UNITED STATES",Red Bluff Municipal Airport,"Red Bluff, California, United States",OK
    RBM,////,////,EDMS,UNK,1.0,Wallmuhle,"Straubing, Germany",OWNO,////,////,Wallmuhle,"Straubing, Bavaria, Germany",OK
    RBN,////,////,////,UNK,1.0,Fort Jefferson,"Fort Jefferson (FL), USA",OWNO,////,////,????,"Fort Jefferson, Florida, United States",OK
    RBO,////,////,SLRB,UNK,1.0,Robore,"Robore, Bolivia",OWNO,////,////,Roboré Airport,"Roboré, Chiquitos, Santa Cruz, Bolivia",OK
    RBP,////,////,////,UNK,1.0,Rabaraba,"Rabaraba, Papua New Guinea",OWNO,////,////,????,"Rabaraba, Milne Bay, Papua-New Guinea",OK
    RBQ,////,////,SLRQ,UNK,1.0,Rurrenabaque,"Rurrenabaque, Bolivia",OWNO,////,////,Rurrenabaque Airport,"Rurrenabaque, José Ballivián, El Beni, Bolivia",OK
    RBR,////,////,SBRB,UNK,0.667,Plácido de Castro International Airport,"Rio Branco, Brazil",OWNO,////,////,Presidente Medici,"Rio Branco, Acre, Brazil",OK
    RBS,////,////,YORB,UNK,1.0,Orbost,"Orbost, Australia",OWNO,////,////,????,"Orbost, Victoria, Australia",OK
    RBT,////,////,HKMB,UNK,1.0,Marsabit,"Marsabit, Kenya",OWNO,////,////,????,"Marsabit, Kenya",OK
    RBU,////,////,YROE,UNK,1.0,Roebourne,"Roebourne, Australia",OWNO,////,////,????,"Roebourne, Western Australia, Australia",OK
    RBV,////,////,AGRM,UNK,1.0,Ramata,"Ramata, Solomon Islands",OWNO,////,////,Ramata Airport,"Ramata Island, Solomon Islands",OK
    RBW,RBW,KRBW,KRBW,OK,0.686,Municipal Airport,"Walterboro (SC), USA",OWNO,LOWCOUNTRY RGNL,"WALTERBORO, SC - UNITED STATES",Lowcountry Regional,"Walterboro, South Carolina, United States",OK
    RBX,////,////,HSMK,UNK,nan,////,////,////,////,////,Rumbek Airport,"Rumbek, Lakes, South Sudan",UNK
    RBY,RBY,PARY,PARY,OK,1.0,Ruby,"Ruby (AK), USA",OWNO,RUBY,"RUBY, AK - UNITED STATES",????,"Ruby, Alaska, United States",OK
    RCA,RCA,KRCA,KRCA,OK,1.0,Ellsworth AFB,"Rapid City (SD), USA",OWNO,ELLSWORTH AFB,"RAPID CITY, SD - UNITED STATES",Ellsworth AFB,"Rapid City, South Dakota, United States",OK
    RCB,////,////,FARB,UNK,1.0,Richards Bay,"Richards Bay, South Africa",OWNO,////,////,Richards Bay Airport,"Richards Bay, KwaZulu-Natal, South Africa",OK
    RCE,WA09,////,////,OK,1.0,Roche Harbor,"Roche Harbor (WA), USA",OWNO,ROCHE HARBOR,"ROCHE HARBOR, WA - UNITED STATES",????,"Roche Harbor, Washington, United States",OK
    RCH,////,////,SKRH,UNK,0.471,Riohacha,"Riohacha, Colombia",OWNO,////,////,Almirante Padilla Airport,"Riohacha, La Guajira, Colombia",OK
    RCK,RCK,KRCK,KRCK,OK,0.753,Coffield,"Rockdale (TX), USA",OWNO,H H COFFIELD RGNL,"ROCKDALE, TX - UNITED STATES",H H Coffield Regional,"Rockdale, Texas, United States",OK
    RCL,////,////,NVSR,UNK,1.0,Redcliffe,"Redcliffe, Vanuatu",OWNO,////,////,????,"Redcliffe, Ambae Island, Pénama, Vanuatu",OK
    RCM,////,////,YRMD,UNK,1.0,Richmond,"Richmond, Australia",OWNO,////,////,????,"Richmond, Queensland, Australia",OK
    RCN,////,////,////,UNK,1.0,American River,"American River, Australia",OWNO,////,////,????,"American River, Kangaroo Island, South Australia, Australia",OK
    RCO,////,////,LFDN,UNK,0.927,Saint Agnant,"Rochefort, France",OWNO,////,////,St-Agnant,"Rochefort, Poitou-Charentes, France",OK
    RCQ,////,////,SATR,UNK,1.0,Reconquista,"Reconquista, Argentina",OWNO,////,////,????,"Reconquista, Santa Fe, Argentina",OK
    RCR,RCR,KRCR,KRCR,OK,1.0,Fulton County,"Rochester (IN), USA",OWNO,FULTON COUNTY,"ROCHESTER, IN - UNITED STATES",Fulton County Airport,"Rochester, Indiana, United States",OK
    RCS,////,////,EGTO,UNK,1.0,Rochester,"Rochester, United Kingdom",OWNO,////,////,????,"Rochester, Kent, England, United Kingdom",OK
    RCT,RCT,KRCT,KRCT,OK,0.79,Miller Field,"Reed City (MI), USA",OWNO,NARTRON FIELD,"REED CITY, MI - UNITED STATES",Nartron Field,"Reed City, Michigan, United States",OK
    RCU,////,////,SAOC,UNK,1.0,Rio Cuarto,"Rio Cuarto, Argentina",OWNO,////,////,????,"Rio Cuarto, Córdoba, Argentina",OK
    RCY,////,MYRP,MYRP,OK,0.127,Rum Cay,"Rum Cay, Bahamas",OWNO,NEW PORT NELSON,"PORT NELSON, - BAHAMAS",New Port Nelson Airport,"Port Nelson, Rum Cay, Bahamas",TO DO CHECK
    RDA,////,////,////,UNK,1.0,Rockhampton Downs,"Rockhampton Downs, Australia",OWNO,////,////,????,"Rockhampton Downs, Northern Territory, Australia",OK
    RDB,DGG,PADG,PADG,OK,1.0,Red Dog,"Red Dog (AK), USA",OWNO,RED DOG,"RED DOG, AK - UNITED STATES",????,"Red Dog, Alaska, United States",OK
    RDC,////,////,SNDC,UNK,1.0,Redencao,"Redencao, Brazil",OWNO,////,////,????,"Redenção, Pará, Brazil",OK
    RDD,RDD,KRDD,KRDD,OK,0.778,Redding,"Redding (CA), USA",OWNO,REDDING MUNI,"REDDING, CA - UNITED STATES",Redding Municipal Airport,"Redding, California, United States",OK
    RDE,////,////,////,UNK,1.0,Merdey,"Merdey, Indonesia",OWNO,////,////,????,"Merdey, Papua, Indonesia",OK
    RDG,RDG,KRDG,KRDG,OK,0.821,Municipal/Spaatz Fld,"Reading (PA), USA",OWNO,READING RGNL/CARL A SPAATZ FIELD,"READING, PA - UNITED STATES",Reading Regional/Carl A Spaatz Field,"Reading, Pennsylvania, United States",OK
    RDM,RDM,KRDM,KRDM,OK,1.0,Roberts Field,"Redmond (OR), USA",OWNO,ROBERTS FIELD,"REDMOND, OR - UNITED STATES",Roberts Field,"Redmond, Oregon, United States",OK
    RDN,////,////,WMPR,UNK,nan,////,////,////,////,////,Redang Airport,"Redang, Redang Island (Pulau Redang), Terengganu, Malaysia",UNK
    RDO,////,////,EPRA,UNK,nan,////,////,////,////,////,Sadków Airport,"Radom, Mazowieckie, Poland",UNK
    RDP,////,////,VEDG,UNK,nan,////,////,////,////,////,Kazi Nazrul Islam Airport,"Andal, West Bengal, India",UNK
    RDS,////,////,SAHS,UNK,1.0,Rincon de Los Sauces,"Rincon de los Sauces, Argentina",OWNO,////,////,????,"Rincón de los Sauces, Neuquén, Argentina",OK
    RDT,////,////,GOSR,UNK,1.0,Richard Toll,"Richard Toll, Senegal",OWNO,////,////,Richard Toll Airport,"Richard Toll, Saint-Louis, Senegal",OK
    RDU,RDU,KRDU,KRDU,OK,0.848,Durham/Raleigh Airport,"Raleigh/Durham (NC), USA",OWNO,RALEIGH-DURHAM INTL,"RALEIGH/DURHAM, NC - UNITED STATES",Raleigh-Durham International Airport,"Raleigh/Durham, North Carolina, United States",OK
    RDV,RDV,////,////,OK,1.0,Red Devil,"Red Devil (AK), USA",OWNO,RED DEVIL,"RED DEVIL, AK - UNITED STATES",????,"Red Devil, Alaska, United States",OK
    RDZ,////,////,LFCR,UNK,1.0,Marcillac,"Rodez, France",OWNO,////,////,Marcillac,"Rodez, Midi-Pyrénées, France",OK
    REA,////,////,NTGE,UNK,1.0,Reao,"Reao, French Polynesia",OWNO,////,////,????,"Reao, French Polynesia",OK
    REB,////,////,EDAX,UNK,0.778,Rechlin,"Rechlin, Germany",OWNO,////,////,Rechlin-Lärz,"Rechlin, Müritz, Mecklenburg-Vorpommern, Germany",OK
    REC,////,////,SBRF,UNK,0.735,Guararapes-Gilberto Freyre International Airport,"Recife, Brazil",OWNO,////,////,Guararapes International Airport,"Recife, Pernambuco, Brazil",OK
    RED,RVL,KRVL,KRVL,OK,1.0,Mifflin County,"Reedsville (PA), USA",OWNO,MIFFLIN COUNTY,"REEDSVILLE, PA - UNITED STATES",Mifflin County Airport,"Reedsville, Pennsylvania, United States",OK
    REG,////,////,LICR,UNK,0.698,Tito Menniti,"Reggio Calabria, Italy",OWNO,////,////,????,"Reggio Calabria, Calabria, Italy",OK
    REI,////,////,SOOR,UNK,1.0,Regina,"Regina, French Guiana",OWNO,////,////,????,"Régina, French Guiana",OK
    REK,////,////,////,UNK,1.0,Metropolitan Area,"Reykjavik, Iceland",OWNO,////,////,Metropolitan Area,"Reykjavík, Iceland",OK
    REL,////,////,SAVT,UNK,0.85,Almirante Marcos A. Zar Airport,"Trelew, Argentina",OWNO,////,////,Almirante Zar,"Trelew, Argentina",OK
    REN,////,////,UWOO,UNK,0.615,Orenburg,"Orenburg, Russia",OWNO,////,////,Orenburg Tsentralny Airport,"Orenburg, Orenburgskaya, Russian Federation (Russia)",OK
    REO,REO,KREO,KREO,OK,1.0,State,"Rome (OR), USA",OWNO,ROME STATE,"ROME, OR - UNITED STATES",Rome State Airport,"Rome, Oregon, United States",OK
    REP,////,////,VDSR,UNK,1.0,Siem Reap-Angkor International Airport,"Siem Reap, Cambodia",OWNO,////,////,Angkor International Airport,"Siem Reap, Cambodia",OK
    RER,////,////,MGRT,UNK,1.0,Base Aerea Del Sur,"Retalhuleu, Guatemala",OWNO,////,////,Base Aérea del Sur,"Retalhuleu, Retalhuleu, Guatemala",OK
    RES,////,////,SARE,UNK,1.0,Resistencia,"Resistencia, Argentina",OWNO,////,////,????,"Resistencia, Argentina",OK
    RET,////,////,ENRS,UNK,0.571,Stolport,"Rost, Norway",OWNO,////,////,????,"Røst, Nordland, Norway",OK
    REU,////,////,LERS,UNK,1.0,Reus,"Reus, Spain",OWNO,////,////,????,"Reus, Catalonia, Spain",OK
    REW,////,////,////,UNK,1.0,Rewa,"Rewa, India",OWNO,////,////,????,"Rewa, Madhya Pradesh, India",OK
    REX,////,////,MMRX,UNK,0.842,Gen. Lucio Blanco,"Reynosa, Mexico",OWNO,////,////,General Lucio Blanco International Airport,"Reynosa, Tamaulipas, México",OK
    REY,////,////,SLRY,UNK,1.0,Reyes,"Reyes, Bolivia",OWNO,////,////,Reyes Airport,"Reyes, José Ballivián, El Beni, Bolivia",OK
    REZ,////,////,SDRS,UNK,1.0,Resende,"Resende, Brazil",OWNO,////,////,????,"Resende, Rio de Janeiro, Brazil",OK
    RFA,////,////,FEGR,UNK,1.0,Rafai,"Rafai, Central African Republic",OWNO,////,////,Rafaï Airport,"Rafaï, Mbomou (Mbömü), Central African Republic",OK
    RFD,RFD,KRFD,KRFD,OK,0.608,Greater Rockford,"Rockford (IL), USA",OWNO,CHICAGO/ROCKFORD INTL,"CHICAGO/ROCKFORD, IL - UNITED STATES",Chicago/Rockford International Airport,"Chicago/Rockford, Illinois, United States",OK
    RFG,RFG,KRFG,KRFG,OK,1.0,Rooke Field,"Refugio (TX), USA",OWNO,ROOKE FIELD,"REFUGIO, TX - UNITED STATES",Rooke Field,"Refugio, Texas, United States",OK
    RFK,5MS1,////,KRFK,NG,1.0,Rollang Field,"Anguilla (MS), USA",OWNO,ROLLANG FIELD,"ROLLING FORK, MS - UNITED STATES",Rollang Field,"Rolling Fork, Mississippi, United States",OK
    RFN,////,////,BIRG,UNK,1.0,Raufarhofn,"Raufarhofn, Iceland",OWNO,////,////,????,"Raufarhöfn, Iceland",OK
    RFP,////,////,NTTR,UNK,0.667,Raiatea,"Raiatea, French Polynesia",OWNO,////,////,Uturoa,"Raiatea, Society Islands, French Polynesia",OK
    RFR,////,////,MRRF,UNK,1.0,Rio Frio,"Rio Frio, Costa Rica",OWNO,////,////,????,"Rio Frio, Heredia, Costa Rica",OK
    RGA,////,////,SAWE,UNK,1.0,Rio Grande,"Rio Grande, Argentina",OWNO,////,////,????,"Rio Grande, Argentina",OK
    RGE,////,////,////,UNK,1.0,Porgera,"Porgera, Papua New Guinea",OWNO,////,////,????,"Porgera, Enga, Papua-New Guinea",OK
    RGH,////,////,VEBG,UNK,1.0,Balurghat,"Balurghat, India",OWNO,////,////,????,"Balurghat, West Bengal, India",OK
    RGI,////,////,NTTG,UNK,1.0,Rangiroa,"Rangiroa, French Polynesia",OWNO,////,////,????,"Rangiroa, Tuamotu Islands, French Polynesia",OK
    RGK,////,////,UNBG,UNK,nan,////,////,////,////,////,Gorno-Altaysk Airport,"Gorno-Altaysk, Altay, Russian Federation (Russia)",UNK
    RGL,////,////,SAWG,UNK,0.632,Internacional,"Rio Gallegos, Argentina",OWNO,////,////,????,"Rio Gallegos, Santa Cruz, Argentina",OK
    RGN,////,////,VYYY,UNK,0.714,Mingaladon,"Yangon, Myanmar",OWNO,////,////,Yangon International Airport,"Yangon, Yangon, Myanmar (Burma)",OK
    RGO,////,////,ZKHM,UNK,nan,////,////,////,////,////,Orang Airport,"Chongjin, North Hamgyong, Democratic People's Republic of Korea (North Korea)",UNK
    RGR,F23,////,////,OK,1.0,Ranger Municipal,"Ranger (TX), USA",OWNO,RANGER MUNI,"RANGER, TX - UNITED STATES",Ranger Municipal Airport,"Ranger, Texas, United States",OK
    RGS,////,////,LEBG,UNK,nan,////,////,////,////,////,Burgos,"Burgos, Castille and León, Spain",UNK
    RGT,////,////,WIPR,UNK,1.0,Japura,"Rengat, Indonesia",OWNO,////,////,Japura,"Rengat, Riau, Indonesia",OK
    RHA,////,////,BIRE,UNK,1.0,Reykholar,"Reykholar, Iceland",OWNO,////,////,????,"Reykhólar, Iceland",OK
    RHD,////,////,SANH,UNK,0.563,Rio Hondo,"Rio Hondo, Argentina",OWNO,////,////,Las Termas,"Termas de Río Hondo, Santiago del Estero, Argentina",MAYBE
    RHE,////,////,LFSR,UNK,0.5,Reims,"Reims, France",OWNO,////,////,????,"Reims-Champagne, Champagne-Ardenne, France",TO DO CHECK
    RHG,////,////,HRYU,UNK,1.0,Ruhengeri,"Ruhengeri, Rwanda",OWNO,////,////,????,"Ruhengeri, Rwanda",OK
    RHI,RHI,KRHI,KRHI,OK,1.0,Oneida County,"Rhinelander (WI), USA",OWNO,RHINELANDER-ONEIDA COUNTY,"RHINELANDER, WI - UNITED STATES",Rhinelander-Oneida County Airport,"Rhinelander, Wisconsin, United States",OK
    RHL,////,////,YRYH,UNK,1.0,Roy Hill,"Roy Hill, Australia",OWNO,////,////,????,"Roy Hill, Western Australia, Australia",OK
    RHO,////,////,LGRP,UNK,1.0,Diagoras Airport,"Rhodes, Greece",OWNO,////,////,Diagoras/Maritsa,"Rhodos, Rhodos Island, Notío Aigaío (Southern Aegean), Greece",OK

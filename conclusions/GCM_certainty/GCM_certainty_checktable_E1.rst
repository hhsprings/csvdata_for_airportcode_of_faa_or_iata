
List for checking certainty of Great Circle Mapper (EAA - ELB)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    EAA,EAA,PAEG,PAEG,OK,1.0,Eagle,"Eagle (AK), USA",OWNO,EAGLE,"EAGLE, AK - UNITED STATES",????,"Eagle, Alaska, United States",OK
    EAB,////,////,OYAB,UNK,1.0,Abbse,"Abbse, Yemen",OWNO,////,////,????,"Abbse, Yemen",OK
    EAE,////,////,NVSE,UNK,0.4,Emae,"Emae, Vanuatu",OWNO,////,////,Siwo,"Sangafa, Emae Island, Shéfa, Vanuatu",MAYBE
    EAL,////,////,////,UNK,1.0,Elenak,Elenak,IATA,////,////,Elenak Airport,"Elenak, Kwajalein Atoll, Marshall Islands",MAYBE
    EAM,////,////,OENG,UNK,1.0,Nejran,"Nejran, Saudi Arabia",OWNO,////,////,Nejran Airport,"Nejran, Saudi Arabia",OK
    EAN,EAN,KEAN,KEAN,OK,0.916,Phifer Field,"Wheatland (WY), USA",OWNO,PHIFER AIRFIELD,"WHEATLAND, WY - UNITED STATES",Phifer Airfield,"Wheatland, Wyoming, United States",OK
    EAP,////,////,////,UNK,1.0,Metropolitan Area,Mulhouse/Basel,IATA,////,////,Metropolitan Area,"Basel/Mulhouse, Basel-Stadt, Switzerland",MAYBE
    EAR,EAR,KEAR,KEAR,OK,0.748,Kearney,"Kearney (NE), USA",OWNO,KEARNEY RGNL,"KEARNEY, NE - UNITED STATES",Kearney Regional,"Kearney, Nebraska, United States",OK
    EAS,////,////,LESO,UNK,1.0,San Sebastian,"San Sebastian, Spain",OWNO,////,////,San Sebastián Airport,"San Sebastián, Basque Country, Spain",OK
    EAT,EAT,KEAT,KEAT,OK,0.783,Pangborn Field,"Wenatchee (WA), USA",OWNO,PANGBORN MEMORIAL,"WENATCHEE, WA - UNITED STATES",Pangborn Memorial Airport,"Wenatchee, Washington, United States",OK
    EAU,EAU,KEAU,KEAU,OK,0.445,Eau Claire,"Eau Claire (WI), USA",OWNO,CHIPPEWA VALLEY RGNL,"EAU CLAIRE, WI - UNITED STATES",Chippewa Valley Regional,"Eau Claire, Wisconsin, United States",OK
    EBA,////,////,LIRJ,UNK,1.0,Marina di Campo,"Elba Island, Italy",OWNO,////,////,Marina Di Campo,"Elba Island, Tuscany, Italy",OK
    EBB,////,////,HUEN,UNK,0.737,Entebbe,"Entebbe, Uganda",OWNO,////,////,Entebbe International Airport,"Entebbe, Uganda",OK
    EBD,////,////,HSOB,UNK,1.0,El Obeid,"El Obeid, Sudan",OWNO,////,////,????,"El Obeid, North Kordofan, Sudan",OK
    EBG,////,////,SKEB,UNK,1.0,El Bagre,"El Bagre, Colombia",OWNO,////,////,El Bagre Airport,"El Bagre, Antioquia, Colombia",OK
    EBH,////,////,DAOY,UNK,nan,////,////,////,////,////,El Bayadh Airport,"El Bayadh, El Bayadh, Algeria",UNK
    EBJ,////,////,EKEB,UNK,1.0,Esbjerg,"Esbjerg, Denmark",OWNO,////,////,????,"Esbjerg, Denmark",OK
    EBL,////,////,ORER,UNK,1.0,Erbil International Airport,"Erbil, Iraq",OWNO,////,////,Erbil International Airport,"Erbil, Iraq",OK
    EBM,////,////,DTTR,UNK,1.0,El Borma,"El Borma, Tunisia",OWNO,////,////,????,"El Borma, Tunisia",OK
    EBN,////,////,////,UNK,0.571,Ebadon,"Ebadon, Marshall Islands",OWNO,////,////,Ebadon Airstrip,"Ebadon, Kwajalein Atoll, Marshall Islands",OK
    EBO,////,////,////,UNK,0.571,Ebon Airport,"Ebon, Marshall Islands",OWNO,////,////,????,"Ebon Atoll, Marshall Islands",MAYBE
    EBS,EBS,KEBS,KEBS,OK,1.0,Municipal,"Webster City IA, USA",OWNO,WEBSTER CITY MUNI,"WEBSTER CITY, IA - UNITED STATES",Webster City Municipal Airport,"Webster City, Iowa, United States",OK
    EBU,////,////,LFMH,UNK,1.0,Boutheon,"St Etienne, France",OWNO,////,////,Boutheon,"St-Etienne, Rhône-Alpes, France",OK
    EBW,////,////,FKKW,UNK,1.0,Ebolowa,"Ebolowa, Cameroon",OWNO,////,////,Ebolowa Airport,"Ebolowa, South (Sud), Cameroon",OK
    ECA,6D9,////,////,OK,0.801,Emmet County Airport,"East Tawas (MI), USA",OWNO,IOSCO COUNTY,"EAST TAWAS, MI - UNITED STATES",Iosco County Airport,"East Tawas, Michigan, United States",OK
    ECG,ECG,KECG,KECG,OK,0.857,Elizabeth City,"Elizabeth City (NC), USA",OWNO,ELIZABETH CITY CG AIR STATION/RGNL,"ELIZABETH CITY, NC - UNITED STATES",Elizabeth City Coast Guard Air Station/Regional,"Elizabeth City, North Carolina, United States",OK
    ECH,////,////,YECH,UNK,1.0,Echuca,"Echuca, Australia",OWNO,////,////,????,"Echuca, Victoria, Australia",OK
    ECI,////,////,MNCE,UNK,nan,////,////,////,////,////,Costa Esmeralda Airport,"Rivas, Rivas, Nicaragua",UNK
    ECN,////,////,LCEN,UNK,0.714,Ercan,"Ercan, Cyprus",OWNO,////,////,Ercan International Airport,"Lefkosa, Cyprus",OK
    ECO,////,////,////,UNK,1.0,El Encanto,"El Encanto, Colombia",OWNO,////,////,????,"El Encanto, Amazonas, Colombia",OK
    ECP,ECP,KECP,KECP,OK,1.0,////,////,////,NORTHWEST FLORIDA BEACHES INTL,"PANAMA CITY, FL - UNITED STATES",Northwest Florida Beaches International Airport,"Panama City, Florida, United States",OK
    ECR,////,////,////,UNK,1.0,El Charco,"El Charco, Colombia",OWNO,////,////,????,"El Charco, Nariño, Colombia",OK
    ECS,ECS,KECS,KECS,OK,0.797,Mondell,"Newcastle (WY), USA",OWNO,MONDELL FIELD,"NEWCASTLE, WY - UNITED STATES",Mondell Field,"Newcastle, Wyoming, United States",OK
    EDA,////,////,////,UNK,0.857,Edna Bay,"Edna Bay (AK), USA",OWNO,////,////,Edna Bay Municipal Airport,"Edna Bay, Alaska, United States",OK
    EDB,////,////,HSDB,UNK,0.667,Eldebba,"Eldebba, Sudan",OWNO,////,////,????,"El Debba, Northern, Sudan",OK
    EDD,////,////,YERL,UNK,1.0,Erldunda,"Erldunda, Australia",OWNO,////,////,????,"Erldunda, Northern Territory, Australia",OK
    EDE,EDE,KEDE,KEDE,OK,0.513,Municipal,"Edenton (NC), USA",OWNO,NORTHEASTERN RGNL,"EDENTON, NC - UNITED STATES",Northeastern Regional,"Edenton, North Carolina, United States",OK
    EDF,EDF,PAED,PAED,OK,1.0,Elmendorf AFB,"Anchorage (AK), USA",OWNO,ELMENDORF AFB,"ANCHORAGE, AK - UNITED STATES",Elmendorf AFB,"Anchorage, Alaska, United States",OK
    EDI,////,////,EGPH,UNK,0.71,Turnhouse,"Edinburgh, United Kingdom",OWNO,////,////,????,"Edinburgh, Midlothian, Scotland, United Kingdom",OK
    EDK,EQA,KEQA,KEQA,OK,1.0,El Dorado,"El Dorado (KS), USA",OWNO,EL DORADO/CAPTAIN JACK THOMAS MEMORIAL,"EL DORADO, KS - UNITED STATES",El Dorado/Captain Jack Thomas Memorial Airport,"El Dorado, Kansas, United States",OK
    EDL,////,////,HKEL,UNK,1.0,Eldoret International Airport,"Eldoret, Kenya",OWNO,////,////,Eldoret International Airport,"Eldoret, Kenya",OK
    EDM,////,////,LFRI,UNK,1.0,Les Ajoncs,"La Roche, France",OWNO,////,////,Les Ajoncs,"La Roche-sur-Yon, Pays de la Loire, France",OK
    EDO,////,////,LTFD,UNK,1.0,Edremit/Korfez,"Edremit/Korfez, Turkey",OWNO,////,////,????,"Edremit/Korfez, Balikesir, Turkey",OK
    EDQ,////,////,////,UNK,1.0,Erandique,"Erandique, Honduras",OWNO,////,////,????,"Erandique, Lempira, Honduras",OK
    EDR,////,////,YPMP,UNK,1.0,Edward River,"Edward River, Australia",OWNO,////,////,????,"Edward River, Queensland, Australia",OK
    EDW,EDW,KEDW,KEDW,OK,1.0,Edwards AFB,"Edwards AFB (CA), USA",OWNO,EDWARDS AFB,"EDWARDS, CA - UNITED STATES",Edwards AFB,"Edwards, California, United States",OK
    EED,EED,KEED,KEED,OK,1.0,Needles,"Needles (CA), USA",OWNO,NEEDLES,"NEEDLES, CA - UNITED STATES",????,"Needles, California, United States",OK
    EEK,EEK,PAEE,PAEE,OK,1.0,Eek,"Eek (AK), USA",OWNO,EEK,"EEK, AK - UNITED STATES",????,"Eek, Alaska, United States",OK
    EEN,EEN,KEEN,KEEN,OK,1.0,Dillant-Hopkins,"Keene (NH), USA",OWNO,DILLANT-HOPKINS,"KEENE, NH - UNITED STATES",Dillant-Hopkins Airport,"Keene, New Hampshire, United States",OK
    EFD,EFD,KEFD,KEFD,OK,0.79,Ellington Field,"Houston (TX), USA",OWNO,ELLINGTON,"HOUSTON, TX - UNITED STATES",Ellington Airport,"Houston, Texas, United States",OK
    EFG,////,////,////,UNK,1.0,Efogi,"Efogi, Papua New Guinea",OWNO,////,////,????,"Efogi, Central, Papua-New Guinea",OK
    EFK,EFK,KEFK,KEFK,OK,0.694,Newport,"Newport (VT), USA",OWNO,NEWPORT STATE,"NEWPORT, VT - UNITED STATES",Newport State Airport,"Newport, Vermont, United States",OK
    EFL,////,////,LGKF,UNK,1.0,Argostolion,"Kefallinia, Greece",OWNO,////,////,Argostolion,"Kefallinia Island, Ionía Nísia (Ionian Islands), Greece",OK
    EFW,EFW,KEFW,KEFW,OK,1.0,Municipal,"Jefferson IA, USA",OWNO,JEFFERSON MUNI,"JEFFERSON, IA - UNITED STATES",Jefferson Municipal Airport,"Jefferson, Iowa, United States",OK
    EGA,////,////,////,UNK,1.0,Engati,"Engati, Papua New Guinea",OWNO,////,////,????,"Engati, Morobe, Papua-New Guinea",OK
    EGC,////,////,LFBE,UNK,1.0,Roumanieres,"Bergerac, France",OWNO,////,////,Roumaniere,"Bergerac, Aquitaine, France",OK
    EGE,EGE,KEGE,KEGE,OK,0.799,Eagle County,"Vail/Eagle (CO), USA",OWNO,EAGLE COUNTY RGNL,"EAGLE, CO - UNITED STATES",Eagle County Regional,"Eagle, Colorado, United States",OK
    EGI,EGI,KEGI,KEGI,OK,1.0,Duke Field,"Valparaiso (FL), USA",OWNO,"DUKE FIELD, (EGLIN AF AUX NR 3)","CRESTVIEW, FL - UNITED STATES",Duke Field,"Crestview, Florida, United States",OK
    EGL,////,////,HANG,UNK,1.0,Neghelli,"Neghelli, Ethiopia",OWNO,////,////,Neghelli Airport,"Neghelli, Oromia, Ethiopia",OK
    EGM,////,////,AGGS,UNK,1.0,Sege,"Sege, Solomon Islands",OWNO,////,////,????,"Sege, Solomon Islands",OK
    EGN,////,////,HSGN,UNK,1.0,Geneina,"Geneina, Sudan",OWNO,////,////,????,"Geneina, West Darfur, Sudan",OK
    EGO,////,////,UUOB,UNK,0.8,Belgorod,"Belgorod, Russia",OWNO,////,////,Belgorod International Airport,"Belgorod, Belgorodskaya, Russian Federation (Russia)",OK
    EGP,5T9,////,////,OK,0.573,Maverick Co,"Eagle Pass (TX), USA",OWNO,MAVERICK COUNTY MEMORIAL INTL,"EAGLE PASS, TX - UNITED STATES",Maverick County Memorial International Airport,"Eagle Pass, Texas, United States",OK
    EGS,////,////,BIEG,UNK,1.0,Egilsstadir,"Egilsstadir, Iceland",OWNO,////,////,????,"Egilsstaðir, Iceland",OK
    EGV,EGV,KEGV,KEGV,OK,0.819,Eagle River,"Eagle River (WI), USA",OWNO,EAGLE RIVER UNION,"EAGLE RIVER, WI - UNITED STATES",Eagle River Union Airport,"Eagle River, Wisconsin, United States",OK
    EGX,EII,PAII,PAII,OK,1.0,Egegik,"Egegik (AK), USA",OWNO,EGEGIK,"EGEGIK, AK - UNITED STATES",????,"Egegik, Alaska, United States",OK
    EHL,////,////,SAVB,UNK,1.0,El Bolson,"El Bolson, Argentina",OWNO,////,////,????,"El Bolson, Río Negro, Argentina",OK
    EHM,EHM,PAEH,PAEH,OK,0.826,Cape Newenham,"Cape Newenham (AK), USA",OWNO,CAPE NEWENHAM LRRS,"CAPE NEWENHAM, AK - UNITED STATES",Cape Newenham LRRS Airport,"Cape Newenham, Alaska, United States",OK
    EIA,////,////,////,UNK,1.0,Popondetta,"Eia, Papua New Guinea",OWNO,////,////,Popondetta,"Eia, Northern, Papua-New Guinea",OK
    EIE,////,////,UNII,UNK,0.941,Eniseysk,"Eniseysk, Russia",OWNO,////,////,Yeniseysk Airport,"Yeniseysk, Krasnoyarskiy, Russian Federation (Russia)",TO DO CHECK
    EIH,////,////,YEIN,UNK,1.0,Einasleigh,"Einasleigh, Australia",OWNO,////,////,????,"Einasleigh, Queensland, Australia",OK
    EIL,EIL,PAEI,PAEI,OK,1.0,Eielson AFB,"Fairbanks (AK), USA",OWNO,EIELSON AFB,"FAIRBANKS, AK - UNITED STATES",Eielson AFB,"Fairbanks, Alaska, United States",OK
    EIN,////,////,EHEH,UNK,1.0,Eindhoven,"Eindhoven, Netherlands",OWNO,////,////,????,"Eindhoven, Noord-Brabant (North Brabant), Netherlands",OK
    EIS,////,TUPJ,TUPJ,OK,0.162,Beef Island,"Beef Island, British Virgin Islands",OWNO,TERRANCE B LETTSOME INTL,"ROADTOWN, - VIRGIN ISLANDS, BRITISH",Terrance B. Lettsome International Airport,"Road Town, Beef Island, Tortola, Virgin Islands (British)",TO DO CHECK
    EIY,////,////,LLEY,UNK,1.0,Ein Yahav,"Ein Yahav, Israel",OWNO,////,////,????,"Ein Yahav, Israel",OK
    EJA,////,////,SKEJ,UNK,0.96,Variguies,"Barrancabermeja, Colombia",OWNO,////,////,Yariguíes Airport,"Barrancabermeja, Santander, Colombia",OK
    EJH,////,////,OEWJ,UNK,0.889,Wedjh,"Wedjh, Saudi Arabia",OWNO,////,////,Wejh Airport,"Wejh, Saudi Arabia",TO DO CHECK
    EJN,////,////,////,UNK,0.5,Taolai,Ejina Banner,IATA,////,////,Ejin Banner Taolai Airport,"Ejin Banner, Inner Mongolia, China",MAYBE
    EKA,EKA,KEKA,KEKA,OK,1.0,Murray Field,"Eureka (CA), USA",OWNO,MURRAY FIELD,"EUREKA, CA - UNITED STATES",Murray Field,"Eureka, California, United States",OK
    EKB,////,////,UASB,UNK,1.0,Ekibastuz,"Ekibastuz, Kazakhstan",OWNO,////,////,Ekibastuz Airport,"Ekibastuz, Pavlodar, Kazakhstan",OK
    EKD,////,////,YELK,UNK,1.0,Elkedra,"Elkedra, Australia",OWNO,////,////,????,"Elkedra, Northern Territory, Australia",OK
    EKE,////,////,////,UNK,1.0,Ekereku,"Ekereku, Guyana",OWNO,////,////,????,"Ekereku, Cuyuni-Mazaruni, Guyana",OK
    EKI,EKM,KEKM,KEKM,OK,1.0,Municipal,"Elkhart (IN), USA",OWNO,ELKHART MUNI,"ELKHART, IN - UNITED STATES",Elkhart Municipal Airport,"Elkhart, Indiana, United States",OK
    EKN,EKN,KEKN,KEKN,OK,0.356,Elkins,"Elkins (WV), USA",OWNO,ELKINS-RANDOLPH CO-JENNINGS RANDOLPH FLD,"ELKINS, WV - UNITED STATES",Elkins-Randolph Co-Jennings Randolph Field,"Elkins, West Virginia, United States",OK
    EKO,EKO,KEKO,KEKO,OK,0.664,Elko,"Elko (NV), USA",OWNO,ELKO RGNL,"ELKO, NV - UNITED STATES",Elko Regional,"Elko, Nevada, United States",OK
    EKS,////,////,UHSK,UNK,nan,////,////,////,////,////,????,"Shakhtersk, Sakhalinskaya, Russian Federation (Russia)",UNK
    EKT,////,////,ESSU,UNK,1.0,Eskilstuna,"Eskilstuna, Sweden",OWNO,////,////,????,"Eskilstuna, Södermanlands län, Sweden",OK
    EKX,EKX,KEKX,KEKX,OK,0.537,Elizabethtown,"Elizabethtown (KY), USA",OWNO,ADDINGTON FIELD,"ELIZABETHTOWN, KY - UNITED STATES",Addington Field,"Elizabethtown, Kentucky, United States",OK
    ELA,ELA,KELA,KELA,OK,1.0,Eagle Lake,"Eagle Lake (TX), USA",OWNO,EAGLE LAKE,"EAGLE LAKE, TX - UNITED STATES",????,"Eagle Lake, Texas, United States",OK
    ELB,////,////,SKBC,UNK,0.718,San Bernado,"El Banco, Colombia",OWNO,////,////,Las Flores Airport,"El Banco, Magdalena, Colombia",OK

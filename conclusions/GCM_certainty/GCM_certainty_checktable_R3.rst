
List for checking certainty of Great Circle Mapper (RRS - RZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    RRS,////,////,ENRO,UNK,1.0,Roros,"Roros, Norway",OWNO,////,////,????,"Røros, Norway",OK
    RRT,RRT,KRRT,KRAD,NG,0.501,Warroad,"Warroad (MN), USA",OWNO,WARROAD INTL MEMORIAL,"WARROAD, MN - UNITED STATES",Warroad International Memorial,"Warroad, Minnesota, United States",OK
    RRV,////,////,YRBR,UNK,1.0,Robinson River,"Robinson River, Australia",OWNO,////,////,????,"Robinson River, Northern Territory, Australia",OK
    RSA,////,////,SAZR,UNK,1.0,Santa Rosa,"Santa Rosa, Argentina",OWNO,////,////,????,"Santa Rosa, Argentina",OK
    RSB,////,////,YRSB,UNK,1.0,Roseberth,"Roseberth, Australia",OWNO,////,////,????,"Roseberth, Queensland, Australia",OK
    RSD,////,MYER,MYER,OK,0.632,S Eleuthera,"Rock Sound, Bahamas",OWNO,ROCK SOUND,"ROCK SOUND, - BAHAMAS",????,"Rock Sound, Eleuthera, Bahamas",OK
    RSE,////,////,////,UNK,0.909,Au-Rose Bay,"Sydney, Australia",OWNO,////,////,????,"Rose Bay, New South Wales, Australia",MAYBE
    RSH,RSH,PARS,PARS,OK,0.778,Russian SPB,"Russian Mission (AK), USA",OWNO,RUSSIAN MISSION,"RUSSIAN MISSION, AK - UNITED STATES",????,"Russian Mission, Alaska, United States",OK
    RSI,////,////,////,UNK,1.0,Rio Sidra,"Rio Sidra, Panama",OWNO,////,////,????,"Río Sidra, Kuna Yala (Guna Yala), Panamá",OK
    RSJ,W49,////,////,OK,0.81,Rosario SPB,"Rosario (WA), USA",OWNO,ROSARIO,"ROSARIO, WA - UNITED STATES",Rosario SPB,"Rosario, Washington, United States",OK
    RSK,////,////,WASC,UNK,1.0,Ransiki,"Ransiki, Indonesia",OWNO,////,////,????,"Ransiki, Papua, Indonesia",OK
    RSL,RSL,KRSL,KRSL,OK,0.719,Russell,"Russell (KS), USA",OWNO,RUSSELL MUNI,"RUSSELL, KS - UNITED STATES",Russell Municipal Airport,"Russell, Kansas, United States",OK
    RSN,RSN,KRSN,KRSN,OK,0.748,Ruston,"Ruston (LA), USA",OWNO,RUSTON RGNL,"RUSTON, LA - UNITED STATES",Ruston Regional,"Ruston, Louisiana, United States",OK
    RSS,////,////,////,UNK,1.0,Roseires,"Roseires, Sudan",OWNO,////,////,????,"Roseires, Blue Nile, Sudan",OK
    RST,RST,KRST,KRST,OK,0.627,Municipal,"Rochester (MN), USA",OWNO,ROCHESTER INTL,"ROCHESTER, MN - UNITED STATES",Rochester International Airport,"Rochester, Minnesota, United States",OK
    RSU,////,////,RKJY,UNK,1.0,Yeosu,"Yeosu, South Korea",OWNO,////,////,Yeosu/Suncheon Airport,"Yeosu, Republic of Korea (South Korea)",OK
    RSW,RSW,KRSW,KRSW,OK,0.743,Southwest Florida Reg,"Fort Myers (FL), USA",OWNO,SOUTHWEST FLORIDA INTL,"FORT MYERS, FL - UNITED STATES",Southwest Florida International Airport,"Fort Myers, Florida, United States",OK
    RSX,K21,////,////,OK,0.87,Rouses Point,"Rouses Point (NY), USA",OWNO,ROUSES POINT,"ROUSES POINT, NY - UNITED STATES",Rouses Point SPB,"Rouses Point, New York, United States",OK
    RTA,////,////,NFNR,UNK,1.0,Rotuma Island,"Rotuma Island, Fiji",OWNO,////,////,????,"Elsee, Rotuma Island, Fiji",OK
    RTB,////,////,MHRO,UNK,0.37,Roatan,"Roatan, Honduras",OWNO,////,////,Juan Manuel Gálvez International Airport,"Roatán, Islas de la Bahía, Honduras",OK
    RTC,////,////,VARG,UNK,1.0,Ratnagiri,"Ratnagiri, India",OWNO,////,////,????,"Ratnagiri, Maharashtra, India",OK
    RTG,////,////,WATG,UNK,0.545,Ruteng,"Ruteng, Indonesia",OWNO,////,////,Satar Tacik,"Ruteng, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    RTI,////,////,WRKR,UNK,0.533,Roti,"Roti, Indonesia",OWNO,////,////,????,"Baa, Roti Island, Nusa Tenggara Timur, Indonesia",MAYBE
    RTL,0F3,////,////,OK,0.748,Spirit Lake,"Spirit Lake IA, USA",OWNO,SPIRIT LAKE MUNI,"SPIRIT LAKE, IA - UNITED STATES",Spirit Lake Municipal Airport,"Spirit Lake, Iowa, United States",OK
    RTM,////,////,EHRD,UNK,0.625,Metropolitan Area,"Rotterdam, Netherlands",OWNO,////,////,????,"Rotterdam, Zuid-Holland (South Holland), Netherlands",OK
    RTN,RTN,KRTN,KRTN,OK,1.0,Crews Field,"Raton (NM), USA",OWNO,RATON MUNI/CREWS FIELD,"RATON, NM - UNITED STATES",Ratón Municipal/Crews Field,"Ratón, New Mexico, United States",OK
    RTP,////,////,YRTP,UNK,1.0,Rutland Plains,"Rutland Plains, Australia",OWNO,////,////,????,"Rutland Plains, Queensland, Australia",OK
    RTS,////,////,YRTI,UNK,1.0,Rottnest Island,"Rottnest Island, Australia",OWNO,////,////,????,"Rottnest Island, Western Australia, Australia",OK
    RTW,////,////,UWSS,UNK,0.583,Saratov,"Saratov, Russia",OWNO,////,////,Tsentralny Airport,"Saratov, Saratovskaya, Russian Federation (Russia)",OK
    RTY,////,////,YMYT,UNK,1.0,Merty,"Merty, Australia",OWNO,////,////,????,"Merty, South Australia, Australia",OK
    RUA,////,////,HUAR,UNK,1.0,Arua,"Arua, Uganda",OWNO,////,////,????,"Arua, Uganda",OK
    RUD,////,////,OIMJ,UNK,nan,////,////,////,////,////,Shahroud Airport,"Shahroud, Semnan, Iran",UNK
    RUF,////,////,////,UNK,1.0,Yuruf,"Yuruf, Indonesia",OWNO,////,////,????,"Yuruf, Papua, Indonesia",OK
    RUG,////,////,ZSRG,UNK,0.9,Rugao,"Rugao, PR China",OWNO,////,////,Rugao AB,"Rugao, Jiangsu, China",OK
    RUH,////,////,OERK,UNK,1.0,King Khaled International Airport,"Riyadh, Saudi Arabia",OWNO,////,////,King Khaled International Airport,"Riyadh, Saudi Arabia",OK
    RUI,SRR,KSRR,KSRR,OK,0.326,Municipal,"Ruidoso (NM), USA",OWNO,SIERRA BLANCA RGNL,"RUIDOSO, NM - UNITED STATES",Sierra Blanca Regional,"Ruidoso, New Mexico, United States",OK
    RUK,////,////,VNRK,UNK,1.0,Rukumkot,"Rukumkot, Nepal",OWNO,////,////,????,"Rukumkot, Nepal",OK
    RUM,////,////,VNRT,UNK,1.0,Rumjatar,"Rumjatar, Nepal",OWNO,////,////,????,"Rumjatar, Nepal",OK
    RUN,////,////,FMEE,UNK,0.615,Gillot,"St Denis de la Reunion, Reunion",OWNO,////,////,Roland Garros,"St-Denis, Réunion",TO DO CHECK
    RUP,////,////,VERU,UNK,1.0,Rupsi,"Rupsi, India",OWNO,////,////,????,"Rupsi, Assam, India",OK
    RUR,////,////,NTAR,UNK,1.0,Rurutu,"Rurutu, French Polynesia",OWNO,////,////,????,"Rurutu, Tubuai Islands, Austral Islands, French Polynesia",OK
    RUS,////,////,AGGU,UNK,0.625,Marau Sound,"Marau Sound, Solomon Islands",OWNO,////,////,Paruru Airport,"Marau, Guadalcanal Island, Solomon Islands",MAYBE
    RUT,RUT,KRUT,KRUT,OK,0.521,Rutland,"Rutland (VT), USA",OWNO,RUTLAND - SOUTHERN VERMONT RGNL,"RUTLAND, VT - UNITED STATES",Rutland - Southern Vermont Regional,"Rutland, Vermont, United States",OK
    RUU,////,////,////,UNK,1.0,Ruti,"Ruti, Papua New Guinea",OWNO,////,////,????,"Ruti, Western Highlands, Papua-New Guinea",OK
    RUV,////,////,MGRB,UNK,1.0,Rubelsanto,"Rubelsanto, Guatemala",OWNO,////,////,????,"Rubelsanto, Alta Verapaz, Guatemala",OK
    RUY,////,////,MHRU,UNK,1.0,Copan,"Copan, Honduras",OWNO,////,////,????,"Ruinas de Copán, Copán, Honduras",OK
    RVA,////,////,FMSG,UNK,1.0,Farafangana,"Farafangana, Madagascar",OWNO,////,////,????,"Farafangana, Madagascar",OK
    RVC,////,////,////,UNK,1.0,Rivercess,"Rivercess, Liberia",OWNO,////,////,????,"Rivercess, Liberia",OK
    RVD,////,////,SWLC,UNK,0.429,Rio Verde,"Rio Verde, Brazil",OWNO,////,////,General Leite de Castro,"Rio Verde, Goiás, Brazil",OK
    RVE,////,////,SKSA,UNK,0.483,Saravena,"Saravena, Colombia",OWNO,////,////,Los Colonizadores Airport,"Saravena, Arauca, Colombia",OK
    RVH,////,////,ULSS,UNK,1.0,Rzhevka,"St Petersburg, Russia",OWNO,////,////,Rzhevka,"St. Petersburg, Leningradskaya, Russian Federation (Russia)",OK
    RVK,////,////,ENRM,UNK,1.0,Ryumsjoen Airport,"Roervik, Norway",OWNO,////,////,Ryumsjoen,"Roervik, Norway",OK
    RVN,////,////,EFRO,UNK,1.0,Rovaniemi,"Rovaniemi, Finland",OWNO,////,////,????,"Rovaniemi, Lappi (Lappland (Lapland)), Finland",OK
    RVO,////,////,FARI,UNK,1.0,Reivilo,"Reivilo, South Africa",OWNO,////,////,Reivilo Airport,"Reivilo, North West, South Africa",OK
    RVR,U34,////,////,OK,0.774,Green River,"Green River (UT), USA",OWNO,GREEN RIVER MUNI,"GREEN RIVER, UT - UNITED STATES",Green River Municipal Airport,"Green River, Utah, United States",OK
    RVS,RVS,KRVS,KRVS,OK,0.743,R.Lloyd Jones,"Tulsa (OK), USA",OWNO,RICHARD LLOYD JONES JR,"TULSA, OK - UNITED STATES",Richard Lloyd Jones Jr. Airport,"Tulsa, Oklahoma, United States",OK
    RVT,////,////,YNRV,UNK,nan,////,////,////,////,////,Ravensthorpe Airport,"Ravensthorpe, Western Australia, Australia",UNK
    RVV,////,////,NTAV,UNK,nan,////,////,////,////,////,????,"Raivavae, Tubuai Islands, Austral Islands, French Polynesia",UNK
    RVY,////,////,SURV,UNK,0.222,Rivera,"Rivera, Uruguay",OWNO,////,////,Presidente General Don Óscar D. Gestido International Airport,"Rivera, Rivera, Uruguay",OK
    RWF,RWF,KRWF,KRWF,OK,1.0,Redwood Falls Municipal,"Redwood Falls (MN), USA",OWNO,REDWOOD FALLS MUNI,"REDWOOD FALLS, MN - UNITED STATES",Redwood Falls Municipal Airport,"Redwood Falls, Minnesota, United States",OK
    RWI,RWI,KRWI,KRWI,OK,0.852,Rocky Mount-Wilson,"Rocky Mount (NC), USA",OWNO,ROCKY MOUNT-WILSON RGNL,"ROCKY MOUNT, NC - UNITED STATES",Rocky Mount-Wilson Regional,"Rocky Mount, North Carolina, United States",OK
    RWL,RWL,KRWL,KRWL,OK,0.734,Rawlins,"Rawlins (WY), USA",OWNO,RAWLINS MUNI/HARVEY FIELD,"RAWLINS, WY - UNITED STATES",Rawlins Municipal/Harvey Field,"Rawlins, Wyoming, United States",OK
    RWN,////,////,UKLR,UNK,1.0,Rovno,"Rovno, Ukraine",OWNO,////,////,????,"Rovno, Rivne, Ukraine",OK
    RXA,////,////,////,UNK,1.0,Raudha,"Raudha, Yemen",OWNO,////,////,????,"Raudha, Yemen",OK
    RXS,////,////,RPVR,UNK,1.0,Roxas City,"Roxas City, Philippines",OWNO,////,////,????,"Roxas City, Panay Island, Philippines",OK
    RYB,////,////,UUBK,UNK,0.609,Rybinsk,"Rybinsk, Russia",OWNO,////,////,Staroselye Airport,"Rybinsk, Yaroslavskaya, Russian Federation (Russia)",OK
    RYG,////,////,ENRY,UNK,0.69,Rygge Civilian Airport,"Moss, Norway",OWNO,////,////,Moss Airport Rygge,"Moss, Norway",OK
    RYK,////,////,OPRK,UNK,0.683,Rahim Yar Khan,"Rahim Yar Khan, Pakistan",OWNO,////,////,Shaikh Zayed,"Rahim Yar Khan, Punjab, Pakistan",OK
    RYL,////,////,FLRZ,UNK,0.533,Royal,Lower Zambezi Nat.Park,IATA,////,////,Royal Airstrip,"Lower Zambezi National Park, Lusaka, Zambia",MAYBE
    RYN,////,////,LFCY,UNK,1.0,Medis,"Royan, France",OWNO,////,////,Medis,"Royan, Poitou-Charentes, France",OK
    RYO,////,////,SAWT,UNK,1.0,Rio Turbio,"Rio Turbio, Argentina",OWNO,////,////,????,"Río Turbio, Santa Cruz, Argentina",OK
    RZA,////,////,SAWU,UNK,1.0,Santa Cruz,"Santa Cruz, Argentina",OWNO,////,////,????,"Santa Cruz, Santa Cruz, Argentina",OK
    RZE,////,////,EPRZ,UNK,1.0,Jasionka,"Rzeszow, Poland",OWNO,////,////,Jasionka Airport,"Rzeszow, Podkarpackie, Poland",OK
    RZN,////,////,UUWR,UNK,1.0,Ryazan,"Ryazan, Russia",OWNO,////,////,????,"Ryazan, Ryazanskaya, Russian Federation (Russia)",OK
    RZR,////,////,OINR,UNK,1.0,Ramsar,"Ramsar, Iran",OWNO,////,////,????,"Ramsar, Mazandaran, Iran",OK
    RZS,////,////,OPSW,UNK,nan,////,////,////,////,////,Sawan Airport,"Sawan, Sindh, Pakistan",UNK
    RZZ,RZZ,////,KRZZ,UNK,1.0,Halifax County,"Roanoke Rapids (NC), USA",OWNO,////,////,Halifax County Airport,"Roanoke Rapids, North Carolina, United States",OK

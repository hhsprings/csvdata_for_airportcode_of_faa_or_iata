
List for checking certainty of Great Circle Mapper (BTI - BZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    BTI,BTI,PABA,PABA,OK,0.845,Barter Island,"Barter Island (AK), USA",OWNO,BARTER ISLAND LRRS,"BARTER ISLAND LRRS, AK - UNITED STATES",Barter Island LRRS Airport,"Barter Island, Alaska, United States",OK
    BTJ,////,////,WITT,UNK,0.582,Blang Bintang,"Banda Aceh, Indonesia",OWNO,////,////,Sultan Iskandar Muda,"Banda Aceh, Aceh, Indonesia",OK
    BTK,////,////,UIBB,UNK,1.0,Bratsk,"Bratsk, Russia",OWNO,////,////,Bratsk Airport,"Bratsk, Irkutskaya, Russian Federation (Russia)",OK
    BTL,BTL,KBTL,KBTL,OK,0.799,WK Kellogg Regional,"Battle Creek (MI), USA",OWNO,W K KELLOGG,"BATTLE CREEK, MI - UNITED STATES",W K Kellogg Airport,"Battle Creek, Michigan, United States",OK
    BTM,BTM,KBTM,KBTM,OK,0.518,Butte,"Butte (MT), USA",OWNO,BERT MOONEY,"BUTTE, MT - UNITED STATES",Bert Mooney Airport,"Butte, Montana, United States",OK
    BTN,BBP,KBBP,KBBP,OK,0.354,Bennettsville,"Bennettsville (SC), USA",OWNO,MARLBORO COUNTY JETPORT - H E AVENT FIELD,"BENNETTSVILLE, SC - UNITED STATES",Marlboro County Jetport - H E Avent Field,"Bennettsville, South Carolina, United States",OK
    BTO,////,////,SMBO,UNK,0.636,Botopasie,"Botopasie, Suriname",OWNO,////,////,Botopasi Airstrip,"Botopasi, Sipaliwini, Suriname",OK
    BTP,BTP,KBTP,KBTP,OK,0.656,Graham Field,"Butler (PA), USA",OWNO,BUTLER COUNTY/K W SCHOLTER FIELD,"BUTLER, PA - UNITED STATES",Butler County/K W Scholter Field,"Butler, Pennsylvania, United States",OK
    BTR,BTR,KBTR,KBTR,OK,0.752,Metropolitan Airport,"Baton Rouge (LA), USA",OWNO,"BATON ROUGE METROPOLITAN, RYAN FIELD","BATON ROUGE, LA - UNITED STATES","Baton Rouge Metropolitan, Ryan Field","Baton Rouge, Louisiana, United States",OK
    BTS,////,////,LZIB,UNK,0.667,Ivanka,"Bratislava, Slovakia",OWNO,////,////,Milan Rastislav Stefánik,"Bratislava, Slovakia",OK
    BTT,BTT,PABT,PABT,OK,1.0,Bettles,"Bettles (AK), USA",OWNO,BETTLES,"BETTLES, AK - UNITED STATES",????,"Bettles, Alaska, United States",OK
    BTU,////,////,WBGB,UNK,1.0,Bintulu,"Bintulu, Malaysia",OWNO,////,////,????,"Bintulu, Sarawak, Malaysia",OK
    BTV,BTV,KBTV,KBTV,OK,1.0,International,"Burlington (VT), USA",OWNO,BURLINGTON INTL,"BURLINGTON, VT - UNITED STATES",Burlington International Airport,"Burlington, Vermont, United States",OK
    BTW,////,////,WAOC,UNK,1.0,Batu Licin,"Batu Licin, Indonesia",OWNO,////,////,????,"Batu Licin, Kalimantan Selatan (South Borneo), Indonesia",OK
    BTZ,////,////,LTBE,UNK,1.0,Bursa Airport,"Bursa, Turkey",OWNO,////,////,????,"Bursa, Bursa, Turkey",OK
    BUA,////,////,AYBK,UNK,0.545,Buka,"Buka, Papua New Guinea",OWNO,////,////,????,"Buka Island, Bougainville, Papua-New Guinea",MAYBE
    BUC,////,////,YBKT,UNK,1.0,Burketown,"Burketown, Australia",OWNO,////,////,????,"Burketown, Queensland, Australia",OK
    BUD,////,////,LHBP,UNK,0.692,Ferihegy,"Budapest, Hungary",OWNO,////,////,Budapest International Airport,"Budapest, Budapest, Hungary",OK
    BUF,BUF,KBUF,KBUF,OK,0.524,Greater Buffalo International,"Buffalo (NY), USA",OWNO,BUFFALO NIAGARA INTL,"BUFFALO, NY - UNITED STATES",Buffalo Niagara International Airport,"Buffalo, New York, United States",OK
    BUG,////,////,FNBG,UNK,1.0,Gen. V. Deslandes (Benguela Airport),"Benguela, Angola",OWNO,////,////,Gen V Deslandes,"Benguela, Angola",OK
    BUH,////,////,////,UNK,1.0,Metropolitan Area,"Bucharest, Romania",OWNO,////,////,Metropolitan Area,"Bucharest, Romania",OK
    BUI,////,////,WAJB,UNK,1.0,Bokondini,"Bokondini, Indonesia",OWNO,////,////,????,"Bokondini, Papua, Indonesia",OK
    BUJ,////,////,DAAD,UNK,1.0,Ain Eddis,"Boussaada, Algeria",OWNO,////,////,Ain Eddis,"Bou Saada, Msila, Algeria",OK
    BUN,////,////,SKBU,UNK,0.545,Buenaventura,"Buenaventura, Colombia",OWNO,////,////,Gerardo Tobar López Airport,"Buenaventura, Valle del Cauca, Colombia",OK
    BUO,////,////,HCMV,UNK,1.0,Burao,"Burao, Somalia",OWNO,////,////,????,"Burao, Togdheer, Somalia",OK
    BUP,////,////,VIBT,UNK,1.0,Bhatinda,"Bhatinda, India",OWNO,////,////,????,"Bhatinda, Punjab, India",OK
    BUQ,////,////,FVBU,UNK,0.381,Bulawayo,"Bulawayo, Zimbabwe",OWNO,////,////,Joshua Mqabuko Nkomo International Airport,"Bulawayo, Zimbabwe",OK
    BUR,BUR,KBUR,KBUR,OK,0.766,Burbank,"Burbank (CA), USA",OWNO,BOB HOPE,"BURBANK, CA - UNITED STATES",Bob Hope Airport,"Burbank, California, United States",OK
    BUS,////,////,UGSB,UNK,0.714,Batumi,"Batumi, Georgia",OWNO,////,////,Batumi International Airport,"Batumi, Ajaria, Georgia",OK
    BUU,////,////,////,UNK,1.0,Buyo,"Buyo, Cote d'Ivoire",OWNO,////,////,????,"Buyo, Bas-Sassandra, Côte d'Ivoire (Ivory Coast)",OK
    BUX,////,////,FZKA,UNK,1.0,Bunia,"Bunia, Congo (DRC)",OWNO,////,////,Bunia Airport,"Bunia, Orientale, Democratic Republic of Congo (Zaire)",OK
    BUY,////,////,YBUN,UNK,1.0,Bunbury,"Bunbury, Australia",OWNO,////,////,????,"Bunbury, Western Australia, Australia",OK
    BUZ,////,////,OIBB,UNK,1.0,Bushehr,"Bushehr, Iran",OWNO,////,////,????,"Bushehr, Bushehr, Iran",OK
    BVA,////,////,LFOB,UNK,1.0,Beauvais-Tille Airport,"Paris, France",OWNO,////,////,Tillé,"Beauvais, Picardie (Picardy), France",TO DO CHECK
    BVB,////,////,SBBV,UNK,1.0,Boa Vista,"Boa Vista, Brazil",OWNO,////,////,????,"Boa Vista, Roraima, Brazil",OK
    BVC,////,////,GVBA,UNK,0.88,Rabil,"Boa Vista, Cape Verde",OWNO,////,////,Rabil International Airport,"Rabil, Boa Vista, Cape Verde",OK
    BVE,////,////,LFSL,UNK,0.895,Laroche,"Brive-La-Gaillarde, France",OWNO,////,////,Brive-Souillac Airport,"Brive-la-Gaillarde, Limousin, France",OK
    BVG,////,////,ENBV,UNK,1.0,Berlevag,"Berlevag, Norway",OWNO,////,////,????,"Berlevåg, Finnmark, Norway",OK
    BVH,////,////,SBVH,UNK,1.0,Vilhena,"Vilhena, Brazil",OWNO,////,////,????,"Vilhena, Rondônia, Brazil",OK
    BVI,////,////,YBDV,UNK,1.0,Birdsville,"Birdsville, Australia",OWNO,////,////,????,"Birdsville, Queensland, Australia",OK
    BVK,////,////,SLHJ,UNK,1.0,Huacaraje,"Huacaraje, Bolivia",OWNO,////,////,Huacaraje Airport,"Huacaraje, Iténez, El Beni, Bolivia",OK
    BVO,BVO,KBVO,KBVO,OK,0.805,Bartlesville,"Bartlesville (OK), USA",OWNO,BARTLESVILLE MUNI,"BARTLESVILLE, OK - UNITED STATES",Bartlesville Municipal Airport,"Bartlesville, Oklahoma, United States",OK
    BVS,////,////,SNVS,UNK,1.0,Breves,"Breves, Brazil",OWNO,////,////,????,"Breves, Pará, Brazil",OK
    BVV,////,////,UHSB,UNK,1.0,Burevestnik AFB,Iturup Island,IATA,////,////,Burevestnik AFB,"Burevestnik, Iturup Island, Kuril Islands, Sakhalinskaya, Russian Federation (Russia)",MAYBE
    BVX,BVX,KBVX,KBVX,OK,0.771,Municipal,"Batesville (AR), USA",OWNO,BATESVILLE RGNL,"BATESVILLE, AR - UNITED STATES",Batesville Regional,"Batesville, Arkansas, United States",OK
    BWA,////,////,VNBW,UNK,0.533,Bhairawa,"Bhairawa, Nepal",OWNO,////,////,Gautam Buddha,"Bhairawa, Nepal",OK
    BWB,////,////,YBWX,UNK,1.0,Barrow Island,"Barrow Island, Australia",OWNO,////,////,????,"Barrow Island, Western Australia, Australia",OK
    BWD,BWD,KBWD,KBWD,OK,0.734,Brownwood,"Brownwood (TX), USA",OWNO,BROWNWOOD RGNL,"BROWNWOOD, TX - UNITED STATES",Brownwood Regional,"Brownwood, Texas, United States",OK
    BWE,////,////,EDVE,UNK,0.706,Braunschweig,"Braunschweig, Germany",OWNO,////,////,Braunschweig-Wolfsburg Airport,"Braunschweig, Lower Saxony, Germany",OK
    BWF,////,////,EGNL,UNK,1.0,Walney Island,"Barrow-In-Furness, United Kingdom",OWNO,////,////,????,"Barrow-In-Furness, Walney Island, Cumbria, England, United Kingdom",OK
    BWG,BWG,KBWG,KBWG,OK,0.874,Warren County,"Bowling Green (KY), USA",OWNO,BOWLING GREEN-WARREN COUNTY RGNL,"BOWLING GREEN, KY - UNITED STATES",Bowling Green-Warren County Regional,"Bowling Green, Kentucky, United States",OK
    BWH,////,////,WMKB,UNK,0.833,Butterworth,"Butterworth, Malaysia",OWNO,////,////,RMAF Butterworth,"Butterworth, Penang (Pahang), Malaysia",OK
    BWI,BWI,KBWI,KBWI,OK,1.0,Baltimore/Washington International,"Baltimore (MD), USA",OWNO,BALTIMORE/WASHINGTON INTL THURGOOD MARSHALL,"BALTIMORE, MD - UNITED STATES",Baltimore/Washington International Thurgood Marshall Airport,"Baltimore, Maryland, United States",OK
    BWK,////,////,LDSB,UNK,1.0,Bol,"Bol, Croatia",OWNO,////,////,????,"Bol, Brac, Dalmation Islands, Croatia",OK
    BWN,////,////,WBSB,UNK,0.878,Brunei International,"Bandar Seri Begawan, Brunei Darussalam",OWNO,////,////,Bandar Seri Begwan International Airport,"Bandar Seri Begwan, Brunei Darussalam",OK
    BWO,////,////,UWSB,UNK,1.0,Balakovo,"Balakovo, Russia",OWNO,////,////,????,"Balakovo, Saratovskaya, Russian Federation (Russia)",OK
    BWT,////,////,YWYY,UNK,1.0,Burnie Wynyard,"Burnie, Australia",OWNO,////,////,Wynyard,"Burnie, Tasmania, Australia",OK
    BWU,////,////,YSBK,UNK,1.0,Bankstown,"Bankstown, Australia",OWNO,////,////,????,"Bankstown, New South Wales, Australia",OK
    BXA,BXA,KBXA,KBXA,OK,0.815,George R Carr Air Field,"Bogalusa (LA), USA",OWNO,GEORGE R CARR MEMORIAL AIR FLD,"BOGALUSA, LA - UNITED STATES",George R Carr Memorial Air Field,"Bogalusa, Louisiana, United States",OK
    BXB,////,////,WASO,UNK,1.0,Babo,"Babo, Indonesia",OWNO,////,////,????,"Babo, Papua Barat, Indonesia",OK
    BXE,////,////,GOTB,UNK,1.0,Bakel,"Bakel, Senegal",OWNO,////,////,Bakel Airport,"Bakel, Tambacounda, Senegal",OK
    BXG,////,////,YBDG,UNK,1.0,Bendigo,"Bendigo, Australia",OWNO,////,////,Bendigo Airport,"East Bendigo, Victoria, Australia",OK
    BXH,////,////,UAAH,UNK,0.933,Balhash,"Balhash, Kazakhstan",OWNO,////,////,Balkhash Airport,"Balkhash, Qaraghandy, Kazakhstan",OK
    BXK,BXK,KBXK,KBXK,OK,0.734,Buckeye,"Buckeye (AZ), USA",OWNO,BUCKEYE MUNI,"BUCKEYE, AZ - UNITED STATES",Buckeye Municipal Airport,"Buckeye, Arizona, United States",OK
    BXN,////,////,LTBV,UNK,1.0,Imsik Airport,"Bodrum, Turkey",OWNO,////,////,Imsik,"Bodrum, Mugla, Turkey",OK
    BXO,////,////,LSZC,UNK,nan,////,////,////,////,////,Buochs Airport,"Buochs, Nidwalden, Switzerland",UNK
    BXR,////,////,OIKM,UNK,1.0,Bam,"Bam, Iran",OWNO,////,////,????,"Bam, Kerman, Iran",OK
    BXS,L08,////,////,OK,0.799,Borrego Springs,"Borrego Springs (CA), USA",OWNO,BORREGO VALLEY,"BORREGO SPRINGS, CA - UNITED STATES",Borrego Valley Airport,"Borrego Springs, California, United States",OK
    BXU,////,////,RPME,UNK,1.0,Butuan,"Butuan, Philippines",OWNO,////,////,????,"Butuan, Philippines",OK
    BXX,////,////,////,UNK,1.0,Borama,"Borama, Somalia",OWNO,////,////,Borama Airport,"Borama, Awdal, Somalia",OK
    BYC,////,////,SLYA,UNK,1.0,Yacuiba,"Yacuiba, Bolivia",OWNO,////,////,Yacuiba Airport,"Yacuiba, Gran Chaco, Tarija, Bolivia",OK
    BYG,BYG,KBYG,KBYG,OK,0.599,Municipal,"Buffalo (WY), USA",OWNO,JOHNSON COUNTY,"BUFFALO, WY - UNITED STATES",Johnson County Airport,"Buffalo, Wyoming, United States",OK
    BYH,BYH,KBYH,KBYH,OK,0.674,Blytheville AFB,"Blytheville (AR), USA",OWNO,ARKANSAS INTL,"BLYTHEVILLE, AR - UNITED STATES",Arkansas International Airport,"Blytheville, Arkansas, United States",OK
    BYI,BYI,KBYI,KBYI,OK,1.0,////,////,////,BURLEY MUNI,"BURLEY, ID - UNITED STATES",Burley Municipal Airport,"Burley, Idaho, United States",OK
    BYJ,////,////,LPBJ,UNK,nan,////,////,////,////,////,Beja Airport,"Beja, Beja, Portugal",UNK
    BYK,////,////,DIBK,UNK,1.0,Bouake,"Bouake, Cote d'Ivoire",OWNO,////,////,Bouaké Airport,"Bouaké, Vallée du Bandama, Côte d'Ivoire (Ivory Coast)",OK
    BYM,////,////,MUBY,UNK,0.815,C.M. de Cespedes,"Bayamo, Cuba",OWNO,////,////,Carlos Manuel de Céspedes,"Bayamo, Granma, Cuba",OK
    BYN,////,////,ZMBH,UNK,1.0,Bayankhongor,"Bayankhongor, Mongolia",OWNO,////,////,????,"Bayankhongor, Mongolia",OK
    BYO,////,////,SJDB,UNK,nan,////,////,////,////,////,????,"Bonito, Mato Grosso do Sul, Brazil",UNK
    BYR,////,////,EKLS,UNK,1.0,Laeso Airport,"Laeso Island, Denmark",OWNO,////,////,????,"Læsø, Denmark",MAYBE
    BYS,BYS,KBYS,KBYS,OK,1.0,Bicycle Lake AAF,"Fort Irwin (CA), USA",OWNO,BICYCLE LAKE AAF,"FORT IRWIN/BARSTOW, CA - UNITED STATES",Bicycle Lake AAF Airport,"Fort Irwin/Barstow, California, United States",OK
    BYT,////,////,EIBN,UNK,1.0,Bantry,"Bantry, Ireland",OWNO,////,////,????,"Bantry, County Cork, Munster, Ireland",OK
    BYU,////,////,EDQD,UNK,0.5,Bindlacher-Berg,"Bayreuth, Germany",OWNO,////,////,Bayreuth Airport,"Bayreuth, Bavaria, Germany",OK
    BYW,38WA,////,////,OK,1.0,Blakely Island,"Blakely Island (WA), USA",OWNO,BLAKELY ISLAND,"BLAKELY ISLAND, WA - UNITED STATES",????,"Blakely Island, Washington, United States",OK
    BZA,////,////,MNBZ,UNK,1.0,San Pedro,"Bonanza, Nicaragua",OWNO,////,////,San Pedro Airport,"Bonanza, Atlántico Norte, Nicaragua",OK
    BZB,////,////,////,UNK,1.0,Bazaruto Island,"Bazaruto Island, Mozambique",OWNO,////,////,????,"Bazaruto Island, Bazaruto Archipelago, Mozambique",OK
    BZE,////,////,MZBZ,UNK,1.0,Philip S.W.Goldson International,"Belize City, Belize",OWNO,////,////,Philip S.W. Goldson International Airport,"Belize City, Belize, Belize",OK
    BZG,////,////,EPBY,UNK,0.45,Bydgoszcz,"Bydgoszcz, Poland",OWNO,////,////,Bydgoszcz Ignacy Jan Paderewski Airport,"Bydgoszcz, Kujawsko-pomorskie, Poland",OK
    BZI,////,////,LTBF,UNK,1.0,Balikesir,"Balikesir, Turkey",OWNO,////,////,????,"Balikesir, Balikesir, Turkey",OK
    BZK,////,////,UUBP,UNK,1.0,Briansk,"Briansk, Russia",OWNO,////,////,Bryansk Airport,"Bryansk, Bryanskaya, Russian Federation (Russia)",OK
    BZL,////,////,VGBR,UNK,1.0,Barisal,"Barisal, Bangladesh",OWNO,////,////,????,"Barisal, Bangladesh",OK
    BZM,////,////,EHWO,UNK,0.977,Woensdrecht,"Bergen Op Zoom, Netherlands",OWNO,////,////,Woensdrecht AB,"Bergen op Zoom, Noord-Brabant (North Brabant), Netherlands",OK
    BZN,BZN,KBZN,KBZN,OK,0.734,Gallatin Field,"Bozeman (MT), USA",OWNO,BOZEMAN YELLOWSTONE INTL,"BOZEMAN, MT - UNITED STATES",Bozeman Yellowstone International Airport,"Bozeman, Montana, United States",OK
    BZO,////,////,LIPB,UNK,0.615,Bolzano,"Bolzano, Italy",OWNO,////,////,Dolomiti,"Bolzano, Trentino-Alto Adige, Italy",OK
    BZR,////,////,LFMU,UNK,0.6,Beziers Vias,"Beziers, France",OWNO,////,////,Bíziers Cap d'Agde Airport,"Bíziers, Languedoc-Roussillon, France",OK
    BZU,////,////,FZKJ,UNK,0.615,Buta,"Buta, Congo (DRC)",OWNO,////,////,Buta Zega Airport,"Buta, Orientale, Democratic Republic of Congo (Zaire)",OK
    BZV,////,////,FCBB,UNK,1.0,Maya Maya,"Brazzaville, Congo (ROC)",OWNO,////,////,Maya Maya,"Brazzaville, Congo (Republic of)",OK
    BZY,////,////,LUBL,UNK,1.0,Beltsy,"Beltsy, Moldova",OWNO,////,////,????,"Beltsy, Moldova",OK
    BZZ,////,////,EGVN,UNK,0.919,RAF Station,"Brize Norton, United Kingdom",OWNO,////,////,Brize Norton Station,"Oxford, Oxfordshire, England, United Kingdom",MAYBE


List for checking certainty of Great Circle Mapper (WAA - WMP)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    WAA,IWK,PAIW,PAIW,OK,1.0,Wales,"Wales (AK), USA",OWNO,WALES,"WALES, AK - UNITED STATES",????,"Wales, Alaska, United States",OK
    WAB,////,////,////,UNK,1.0,Wabag,"Wabag, Papua New Guinea",OWNO,////,////,????,"Wabag, Enga, Papua-New Guinea",OK
    WAC,////,////,HAWC,UNK,1.0,Waca,"Waca, Ethiopia",OWNO,////,////,Wacca Airport,"Wacca, SNNPR, Ethiopia",OK
    WAD,////,////,////,UNK,1.0,Andriamena,"Andriamena, Madagascar",OWNO,////,////,????,"Andriamena, Madagascar",OK
    WAE,////,////,OEWD,UNK,0.947,Wadi Ad Dawasir,"Wadi Ad Dawasir, Saudi Arabia",OWNO,////,////,Wadi al-Dawasir Airport,"Wadi al-Dawasir, Saudi Arabia",MAYBE
    WAF,////,////,OPWN,UNK,1.0,Wana,"Wana, Pakistan",OWNO,////,////,????,"Wana, Federally Administered Tribal Areas, Pakistan",OK
    WAG,////,////,NZWU,UNK,1.0,Wanganui,"Wanganui, New Zealand",OWNO,////,////,????,"Wanganui, New Zealand",OK
    WAH,BWP,KBWP,KBWP,OK,0.701,Wahpeton,"Wahpeton (ND), USA",OWNO,HARRY STERN,"WAHPETON, ND - UNITED STATES",Harry Stern Airport,"Wahpeton, North Dakota, United States",OK
    WAI,////,////,FMNW,UNK,0.667,Antsohihy,"Antsohihy, Madagascar",OWNO,////,////,Ambalabe,"Antsohihy, Madagascar",OK
    WAJ,////,////,////,UNK,1.0,Wawoi Falls,"Wawoi Falls, Papua New Guinea",OWNO,////,////,????,"Wawoi Falls, Western, Papua-New Guinea",OK
    WAK,////,////,FMSZ,UNK,1.0,Ankazoabo,"Ankazoabo, Madagascar",OWNO,////,////,????,"Ankazoabo, Madagascar",OK
    WAL,////,////,////,UNK,0.864,Wallops Flight Center,"Chincoteague (VA), USA",OWNO,////,////,Wallops Flight Facility Airport,"Wallops Island, Virginia, United States",OK
    WAM,////,////,FMMZ,UNK,1.0,Ambatondrazaka,"Ambatondrazaka, Madagascar",OWNO,////,////,????,"Ambatondrazaka, Madagascar",OK
    WAN,////,////,////,UNK,1.0,Waverney,"Waverney, Australia",OWNO,////,////,????,"Waverney, Queensland, Australia",OK
    WAO,////,////,////,UNK,1.0,Wabo,"Wabo, Papua New Guinea",OWNO,////,////,????,"Wabo, Gulf, Papua-New Guinea",OK
    WAP,////,////,SCAP,UNK,1.0,Alto Palena,"Alto Palena, Chile",OWNO,////,////,????,"Alto Palena, Los Lagos, Chile",OK
    WAQ,////,////,FMMG,UNK,1.0,Antsalova,"Antsalova, Madagascar",OWNO,////,////,????,"Antsalova, Madagascar",OK
    WAR,////,////,WAJR,UNK,1.0,Waris,"Waris, Indonesia",OWNO,////,////,????,"Waris, Papua, Indonesia",OK
    WAS,////,////,////,UNK,1.0,Metropolitan Area,"Washington, DC, USA",OWNO,////,////,Metropolitan Area,"Washington, District of Columbia, United States",OK
    WAT,////,////,EIWF,UNK,1.0,Waterford,"Waterford, Ireland",OWNO,////,////,????,"Waterford, County Waterford, Munster, Ireland",OK
    WAU,////,////,////,UNK,1.0,Wauchope,"Wauchope, Australia",OWNO,////,////,????,"Wauchope, New South Wales, Australia",OK
    WAV,////,////,YWAV,UNK,1.0,Kalkgurung,"Wave Hill, Australia",OWNO,////,////,Kalkgurung,"Wave Hill, Northern Territory, Australia",OK
    WAW,////,////,EPWA,UNK,0.5,Okecie,"Warsaw, Poland",OWNO,////,////,Fryderyk Chopin International Airport,"Warsaw, Mazowieckie, Poland",OK
    WAX,////,////,HLZW,UNK,1.0,Zwara,"Zwara, Libya",OWNO,////,////,????,"Zwara, Libyan Arab Jamahiriya (Libya)",OK
    WAY,WAY,KWAY,KWAY,OK,1.0,Green County,"Waynesburg (PA), USA",OWNO,GREENE COUNTY,"WAYNESBURG, PA - UNITED STATES",Greene County Airport,"Waynesburg, Pennsylvania, United States",OK
    WAZ,////,////,YWCK,UNK,1.0,Warwick,"Warwick, Australia",OWNO,////,////,????,"Warwick, Queensland, Australia",OK
    WBB,WBB,////,////,OK,1.0,Stebbins,"Stebbins (AK), USA",OWNO,STEBBINS,"STEBBINS, AK - UNITED STATES",????,"Stebbins, Alaska, United States",OK
    WBD,////,////,FMNF,UNK,0.71,Befandriana,"Befandriana, Madagascar",OWNO,////,////,Avaratra Airport,"Befandriana, Madagascar",OK
    WBE,////,////,FNNB,UNK,0.643,Bealanana,"Bealanana, Madagascar",OWNO,////,////,Ankaizina Airport,"Bealanana, Madagascar",OK
    WBG,////,////,ETNS,UNK,0.923,Schleswig-Jagel,"Schleswig-Jagel, Germany",OWNO,////,////,AB,"Schleswig-Jagel, Schleswig-Holstein, Germany",OK
    WBM,////,////,AYWD,UNK,1.0,Wapenamanda,"Wapenamanda, Papua New Guinea",OWNO,////,////,????,"Wapenamanda, Enga, Papua-New Guinea",OK
    WBO,////,////,FMSB,UNK,0.696,Beroroha,"Beroroha, Madagascar",OWNO,////,////,Antsoa,"Beroroha, Madagascar",OK
    WBQ,WBQ,PAWB,PAWB,OK,1.0,Beaver,"Beaver (AK), USA",OWNO,BEAVER,"BEAVER, AK - UNITED STATES",????,"Beaver, Alaska, United States",OK
    WBR,RQB,KRQB,KRQB,OK,0.623,Big Rapids,"Big Rapids (MI), USA",OWNO,ROBEN-HOOD,"BIG RAPIDS, MI - UNITED STATES",Roben-Hood Airport,"Big Rapids, Michigan, United States",OK
    WBU,BDU,KBDU,KBDU,OK,1.0,Municipal,"Boulder (CO), USA",OWNO,BOULDER MUNI,"BOULDER, CO - UNITED STATES",Boulder Municipal Airport,"Boulder, Colorado, United States",OK
    WBW,WBW,KWBW,KWBW,OK,1.0,Wyoming Valle,"Wilkes-Barre (PA), USA",OWNO,WILKES-BARRE WYOMING VALLEY,"WILKES-BARRE, PA - UNITED STATES",Wilkes-Barre Wyoming Valley Airport,"Wilkes-Barre, Pennsylvania, United States",OK
    WCA,////,////,SCST,UNK,1.0,Gamboa,"Castro, Chile",OWNO,////,////,Gamboa Airport,"Castro, Chiloé Island, Los Lagos, Chile",OK
    WCH,////,////,////,UNK,0.625,Chaiten,"Chaiten, Chile",OWNO,////,////,Chaitén Airfield,"Chaitén, Los Lagos, Chile",OK
    WCR,WCR,PALR,PALR,OK,0.815,Chandalar,"Chandalar (AK), USA",OWNO,CHANDALAR LAKE,"CHANDALAR LAKE, - UNITED STATES",????,"Chandalar Lake, Alaska, United States",OK
    WDA,////,////,////,UNK,1.0,Wadi Ain,"Wadi Ain, Yemen",OWNO,////,////,????,"Wadi Ain, Yemen",OK
    WDG,WDG,KWDG,KWDG,OK,0.734,Enid Woodring Mun.,"Enid (OK), USA",OWNO,ENID WOODRING RGNL,"ENID, - UNITED STATES",Enid Woodring Regional,"Enid, Oklahoma, United States",OK
    WDH,////,////,FYWH,UNK,1.0,Hosea Kutako International,"Windhoek, Namibia",OWNO,////,////,Hosea Kutako International Airport,"Windhoek, Namibia",OK
    WDI,////,////,YWND,UNK,1.0,Wondai,"Wondai, Australia",OWNO,////,////,????,"Wondai, Queensland, Australia",OK
    WDR,WDR,KWDR,KWDR,OK,0.445,Winder,"Winder (GA), USA",OWNO,BARROW COUNTY,"WINDER, GA - UNITED STATES",Barrow County Airport,"Winder, Georgia, United States",OK
    WDS,////,////,ZHSY,UNK,nan,////,////,////,////,////,Shiyan Wudangshan Airport,"Shiyan, Hubei, China",UNK
    WEA,WEA,KWEA,KWEA,OK,1.0,Parker County,"Weatherford (TX), USA",OWNO,PARKER COUNTY,"WEATHERFORD, - UNITED STATES",Parker County Airport,"Weatherford, Texas, United States",OK
    WED,////,////,////,UNK,1.0,Wedau,"Wedau, Papua New Guinea",OWNO,////,////,????,"Wedau, Milne Bay, Papua-New Guinea",OK
    WEF,////,////,ZSWF,UNK,1.0,Weifang,"Weifang, PR China",OWNO,////,////,????,"Weifang, Shandong, China",OK
    WEH,////,////,ZSWH,UNK,0.769,Weihai,"Weihai, PR China",OWNO,////,////,Dashuipo Airport,"Weihai, Shandong, China",OK
    WEI,////,////,YBWP,UNK,1.0,Weipa,"Weipa, Australia",OWNO,////,////,????,"Weipa, Queensland, Australia",OK
    WEL,////,////,FAWM,UNK,1.0,Welkom,"Welkom, South Africa",OWNO,////,////,Welkom Airport,"Welkom, Free State, South Africa",OK
    WEP,////,////,////,UNK,1.0,Weam,"Weam, Papua New Guinea",OWNO,////,////,????,"Weam, Western, Papua-New Guinea",OK
    WET,////,////,WABG,UNK,1.0,Wagethe,"Wagethe, Indonesia",OWNO,////,////,????,"Wagethe, Papua, Indonesia",OK
    WEW,////,////,YWWA,UNK,1.0,Wee Waa,"Wee Waa, Australia",OWNO,////,////,????,"Wee Waa, New South Wales, Australia",OK
    WFB,5KE,////,////,OK,0.424,Waterfront SPB,"Ketchikan (AK), USA",OWNO,KETCHIKAN HARBOR,"KETCHIKAN, AK - UNITED STATES",Ketchikan Harbor SPB,"Ketchikan, Alaska, United States",OK
    WFI,////,////,FMSF,UNK,1.0,Fianarantsoa,"Fianarantsoa, Madagascar",OWNO,////,////,????,"Fianarantsoa, Madagascar",OK
    WFK,FVE,KFVE,KFVE,OK,0.402,Frenchville,"Frenchville (ME), USA",OWNO,NORTHERN AROOSTOOK RGNL,"FRENCHVILLE, ME - UNITED STATES",Northern Aroostook Regional,"Frenchville, Maine, United States",OK
    WGA,////,////,YSWG,UNK,0.588,Forrest Hill,"Wagga Wagga, Australia",OWNO,////,////,????,"Wagga Wagga, New South Wales, Australia",OK
    WGC,////,////,VOWA,UNK,1.0,Warrangal,"Warrangal, India",OWNO,////,////,????,"Warrangal, Telangana, India",OK
    WGE,////,////,YWLG,UNK,1.0,Walgett,"Walgett, Australia",OWNO,////,////,????,"Walgett, New South Wales, Australia",OK
    WGO,OKV,KOKV,KOKV,OK,0.766,Municipal,"Winchester (VA), USA",OWNO,WINCHESTER RGNL,"WINCHESTER, VA - UNITED STATES",Winchester Regional,"Winchester, Virginia, United States",OK
    WGP,////,////,WADW,UNK,0.714,Waingapu,"Waingapu, Indonesia",OWNO,////,////,Mau Hau Airport,"Waingapu, Nusa Tenggara Timur, Indonesia",OK
    WGT,////,////,YWGT,UNK,1.0,Wangaratta,"Wangaratta, Australia",OWNO,////,////,????,"Wangaratta, Victoria, Australia",OK
    WGU,////,////,////,UNK,1.0,Wagau,"Wagau, Papua New Guinea",OWNO,////,////,????,"Wagau, Morobe, Papua-New Guinea",OK
    WGY,////,////,////,UNK,0.8,Wagny,"Wagny, Gabon",OWNO,////,////,La Wagny Airport,"La Wagny, Ogooué-Lolo, Gabon",MAYBE
    WHD,4Z7,////,////,OK,0.734,SPB,"Hyder (AK), USA",OWNO,HYDER,"HYDER, AK - UNITED STATES",Hyder SPB,"Hyder, Alaska, United States",OK
    WHF,////,////,HSSW,UNK,1.0,Wadi Halfa,"Wadi Halfa, Sudan",OWNO,////,////,????,"Wadi Halfa, Northern, Sudan",OK
    WHK,////,////,NZWK,UNK,1.0,Whakatane,"Whakatane, New Zealand",OWNO,////,////,????,"Whakatane, New Zealand",OK
    WHL,////,////,////,UNK,1.0,Welshpool,"Welshpool, Australia",OWNO,////,////,????,"Welshpool, Victoria, Australia",OK
    WHO,////,////,NZFJ,UNK,1.0,Franz Josef,"Franz Josef, New Zealand",OWNO,////,////,????,"Franz Josef, New Zealand",OK
    WHP,WHP,KWHP,KWHP,OK,1.0,Whiteman,"Los Angeles (CA), USA",OWNO,WHITEMAN,"LOS ANGELES, - UNITED STATES",Whiteman Airport,"Los Angeles, California, United States",OK
    WHS,////,////,EGEH,UNK,1.0,Whalsay,"Whalsay, United Kingdom",OWNO,////,////,????,"Whalsay, Shetland Islands, Scotland, United Kingdom",OK
    WHT,ARM,KARM,KARM,OK,0.716,Wharton,"Wharton (TX), USA",OWNO,WHARTON RGNL,"WHARTON, TX - UNITED STATES",Wharton Regional,"Wharton, Texas, United States",OK
    WHU,////,////,ZSWU,UNK,0.87,Wuhu,"Wuhu, PR China",OWNO,////,////,Wuhu AB,"Wuhu, Anhui, China",OK
    WIC,////,////,EGPC,UNK,1.0,Wick,"Wick, United Kingdom",OWNO,////,////,????,"Wick, Caithness, Scotland, United Kingdom",OK
    WIE,////,////,ETOU,UNK,0.774,Air Base,"Wiesbaden, Germany",OWNO,////,////,Wiesbaden AAF,"Wiesbaden, Hessen, Germany",OK
    WIK,////,////,NZKE,UNK,1.0,Surfdale,"Surfdale, New Zealand",OWNO,////,////,????,"Surfdale, Waiheke Island, New Zealand",OK
    WIL,////,////,HKNW,UNK,1.0,Wilson,"Nairobi, Kenya",OWNO,////,////,Wilson,"Nairobi, Kenya",OK
    WIN,////,////,YWTN,UNK,1.0,Winton,"Winton, Australia",OWNO,////,////,????,"Winton, Queensland, Australia",OK
    WIO,////,////,YWCA,UNK,1.0,Wilcannia,"Wilcannia, Australia",OWNO,////,////,????,"Wilcannia, New South Wales, Australia",OK
    WIR,////,////,NZWO,UNK,1.0,Wairoa,"Wairoa, New Zealand",OWNO,////,////,????,"Wairoa, New Zealand",OK
    WIT,////,////,YWIT,UNK,1.0,Wittenoom,"Wittenoom, Australia",OWNO,////,////,????,"Wittenoom, Western Australia, Australia",OK
    WIU,////,////,////,UNK,1.0,Witu,"Witu, Papua New Guinea",OWNO,////,////,????,"Witu, West New Britain, Papua-New Guinea",OK
    WJA,////,////,////,UNK,1.0,Woja,"Woja, Marshall Islands",OWNO,////,////,????,"Woja, Marshall Islands",OK
    WJF,WJF,KWJF,KWJF,OK,0.581,William J Fox,"Lancaster (CA), USA",OWNO,GENERAL WM J FOX AIRFIELD,"LANCASTER, CA - UNITED STATES",General William J Fox Airfield,"Lancaster, California, United States",OK
    WJR,////,////,HKWJ,UNK,1.0,Wajir,"Wajir, Kenya",OWNO,////,////,????,"Wajir, Kenya",OK
    WJU,////,////,RKNW,UNK,1.0,WonJu,"WonJu, South Korea",OWNO,////,////,????,"Wonju, Republic of Korea (South Korea)",OK
    WKA,////,////,NZWF,UNK,1.0,Wanaka,"Wanaka, New Zealand",OWNO,////,////,????,"Wanaka, New Zealand",OK
    WKB,////,////,YWKB,UNK,1.0,Warracknabeal,"Warracknabeal, Australia",OWNO,////,////,????,"Warracknabeal, Victoria, Australia",OK
    WKI,////,////,FVWT,UNK,1.0,Hwange,"Hwange, Zimbabwe",OWNO,////,////,????,"Hwange, Zimbabwe",OK
    WKJ,////,////,RJCW,UNK,1.0,Hokkaido,"Wakkanai, Japan",OWNO,////,////,Wakkanai Airport,"Wakkanai, Hokkaido, Japan",OK
    WKK,5A8,////,////,OK,1.0,Aleknagik Airport,"Aleknagik (AK), USA",OWNO,ALEKNAGIK /NEW,"ALEKNAGIK, AK - UNITED STATES",Aleknagik /New Airport,"Aleknagik, Alaska, United States",OK
    WKL,HI07,////,////,OK,0.643,Waikoloa Airport,"Waikoloa (HI), USA",OWNO,WAIKOLOA,"WAIKOLOA VILLAGE, HI - UNITED STATES",Waikoloa Heliport,"Waikoloa Village, Hawaii, Hawaii, United States",OK
    WKN,////,////,////,UNK,1.0,Wakunai,"Wakunai, Papua New Guinea",OWNO,////,////,????,"Wakunai, Bougainville, Papua-New Guinea",OK
    WKR,////,////,MYAW,UNK,1.0,Walker's Cay,"Walker's Cay, Bahamas",OWNO,////,////,????,"Walker's Cay, Abaco Islands, Bahamas",OK
    WLA,////,////,YWAL,UNK,0.714,Wallal,"Wallal, Australia",OWNO,////,////,????,"Wallal Downs, Western Australia, Australia",MAYBE
    WLC,////,////,YWCH,UNK,1.0,Walcha,"Walcha, Australia",OWNO,////,////,????,"Walcha, New South Wales, Australia",OK
    WLD,WLD,KWLD,KWLD,OK,0.694,Arkansas City,"Winfield (KS), USA",OWNO,STROTHER FIELD,"WINFIELD/ARKANSAS CITY, KS - UNITED STATES",Strother Field,"Winfield/Arkansas City, Kansas, United States",OK
    WLE,////,////,YMLS,UNK,nan,////,////,////,////,////,Miles Aerodrome,"Miles, Queensland, Australia",UNK
    WLG,////,////,NZWN,UNK,1.0,International,"Wellington, New Zealand",OWNO,////,////,Wellington International Airport,"Wellington, New Zealand",OK
    WLH,////,////,NVSW,UNK,0.588,Walaha airstrip,"Walaha, Vanuatu",OWNO,////,////,????,"Walaha, Ambae Island, Pénama, Vanuatu",OK
    WLK,WLK,PASK,PASK,OK,1.0,Selawik,"Selawik (AK), USA",OWNO,SELAWIK,"SELAWIK, AK - UNITED STATES",????,"Selawik, Alaska, United States",OK
    WLL,////,////,YWOR,UNK,0.769,Wollogorang airfield,"Wollogorang, Australia",OWNO,////,////,????,"Wollogorang, Northern Territory, Australia",OK
    WLO,////,////,YWTL,UNK,1.0,Waterloo,"Waterloo, Australia",OWNO,////,////,????,"Waterloo, Northern Territory, Australia",OK
    WLP,////,////,YANG,UNK,nan,////,////,////,////,////,West Angelas Airport,"West Angelas, Western Australia, Australia",UNK
    WLR,13Z,////,////,OK,0.79,Loring,"Loring (AK), USA",OWNO,LORING,"LORING, AK - UNITED STATES",Loring SPB,"Loring, Alaska, United States",OK
    WLS,////,////,NLWW,UNK,0.788,Wallis Island,"Wallis Island, Wallis and Futuna Islands",OWNO,////,////,Hihifo,"Wallis Island, Wallis and Futuna Islands",OK
    WLW,WLW,KWLW,KWLW,OK,1.0,Glenn County,"Willows (CA), USA",OWNO,WILLOWS-GLENN COUNTY,"WILLOWS, CA - UNITED STATES",Willows-Glenn County Airport,"Willows, California, United States",OK
    WMA,////,////,FMNX,UNK,1.0,Mandritsara,"Mandritsara, Madagascar",OWNO,////,////,????,"Mandritsara, Madagascar",OK
    WMB,////,////,YWBL,UNK,1.0,Warrnambool,"Warrnambool, Australia",OWNO,////,////,????,"Warrnambool, Victoria, Australia",OK
    WMC,WMC,KWMC,KWMC,OK,0.76,Winnemucca,"Winnemucca (NV), USA",OWNO,WINNEMUCCA MUNI,"WINNEMUCCA, - UNITED STATES",Winnemucca Municipal Airport,"Winnemucca, Nevada, United States",OK
    WMD,////,////,FMSC,UNK,1.0,Mandabe,"Mandabe, Madagascar",OWNO,////,////,????,"Mandabe, Madagascar",OK
    WME,////,////,YMNE,UNK,1.0,Mount Keith,"Mount Keith, Australia",OWNO,////,////,????,"Mount Keith, Western Australia, Australia",OK
    WMH,BPK,KBPK,KBPK,OK,0.605,Mountain Home,"Mountain Home (AR), USA",OWNO,BAXTER COUNTY,"MOUNTAIN HOME, AR - UNITED STATES",Baxter County Airport,"Mountain Home, Arkansas, United States",OK
    WMI,////,////,EPMO,UNK,nan,////,////,////,////,////,Warsaw-Modlin Airport,"Nowy Dwór Mazowiecki, Mazowieckie, Poland",UNK
    WMK,84K,////,////,OK,0.819,SPB,"Meyers Chuck (AK), USA",OWNO,MEYERS CHUCK,"MEYERS CHUCK, AK - UNITED STATES",Meyers Chuck SPB,"Meyers Chuck, Alaska, United States",OK
    WML,////,////,FMMC,UNK,1.0,Malaimbandy,"Malaimbandy, Madagascar",OWNO,////,////,????,"Malaimbandy, Madagascar",OK
    WMN,////,////,FMNR,UNK,1.0,Maroantsetra,"Maroantsetra, Madagascar",OWNO,////,////,????,"Maroantsetra, Madagascar",OK
    WMO,WMO,PAWM,PAWM,OK,1.0,White Mountain,"White Mountain (AK), USA",OWNO,WHITE MOUNTAIN,"WHITE MOUNTAIN, AK - UNITED STATES",????,"White Mountain, Alaska, United States",OK
    WMP,////,////,FMNP,UNK,1.0,Mampikony,"Mampikony, Madagascar",OWNO,////,////,????,"Mampikony, Madagascar",OK

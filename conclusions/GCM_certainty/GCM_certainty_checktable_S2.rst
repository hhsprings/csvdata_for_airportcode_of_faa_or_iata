
List for checking certainty of Great Circle Mapper (SFH - SKF)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    SFH,////,////,MMSF,UNK,0.857,San Felipe,"San Felipe, Mexico",OWNO,////,////,San Felipe International Airport,"San Felipe, Baja California, México",OK
    SFJ,////,////,BGSF,UNK,1.0,Kangerlussuaq,Kangerlussuaq,IATA,////,////,Kangerlussuaq Airport,"Kangerlussuaq, Qeqqata, Greenland",MAYBE
    SFK,////,////,SNSW,UNK,1.0,Soure,"Soure, Brazil",OWNO,////,////,????,"Soure, Pará, Brazil",OK
    SFL,////,////,GVSF,UNK,1.0,Sao Filipe,"Sao Filipe, Cape Verde",OWNO,////,////,????,"São Filipe, Fogo Island, Cape Verde",OK
    SFM,SFM,KSFM,KSFM,OK,0.423,Sanford,"Sanford (ME), USA",OWNO,SANFORD SEACOAST RGNL,"SANFORD, ME - UNITED STATES",Sanford Seacoast Regional,"Sanford, Maine, United States",OK
    SFN,////,////,SAAV,UNK,0.6,Santa Fe,"Santa Fe, Argentina",OWNO,////,////,Sauce Viejo,"Santa Fe, Santa Fe, Argentina",OK
    SFO,SFO,KSFO,KSFO,OK,1.0,International,"San Francisco (CA), USA",OWNO,SAN FRANCISCO INTL,"SAN FRANCISCO, CA - UNITED STATES",San Francisco International Airport,"San Francisco, California, United States",OK
    SFQ,////,////,LTCH,UNK,1.0,Sanliurfa,"Sanliurfa, Turkey",OWNO,////,////,????,"Sanliurfa, Sanliorfa, Turkey",OK
    SFS,////,////,RPLB,UNK,0.786,Subic Bay International Airportt,"Subic Bay, Philippines",OWNO,////,////,Subic Bay International Airport,"Subic Bay, Philippines",OK
    SFT,////,////,ESNS,UNK,1.0,Skelleftea,"Skelleftea, Sweden",OWNO,////,////,????,"Skellefteå, Västerbottens län, Sweden",OK
    SFU,////,////,////,UNK,1.0,Safia,"Safia, Papua New Guinea",OWNO,////,////,????,"Safia, Northern, Papua-New Guinea",OK
    SFV,////,////,SILT,UNK,0.667,Santa Fe Do Sul,"Santa Fe Do Sul, Brazil",OWNO,////,////,Fazenda Três Irmãos,"Santa Fé do Sul, São Paulo, Brazil",OK
    SFX,////,////,////,UNK,1.0,San Felix,"San Felix, Venezuela",OWNO,////,////,????,"San Felix, Monagas, Venezuela",OK
    SFZ,SFZ,KSFZ,KSFZ,OK,0.805,North Central,"Smithfield (RI), USA",OWNO,NORTH CENTRAL STATE,"PAWTUCKET, RI - UNITED STATES",North Central State Airport,"Pawtucket, Rhode Island, United States",OK
    SGA,////,////,OASN,UNK,1.0,Sheghnan,"Sheghnan, Afghanistan",OWNO,////,////,????,"Sheghnan, Badakhshan, Afghanistan",OK
    SGB,////,////,////,UNK,1.0,Singaua,"Singaua, Papua New Guinea",OWNO,////,////,????,"Singaua, Morobe, Papua-New Guinea",OK
    SGC,////,////,USRR,UNK,1.0,Surgut,"Surgut, Russia",OWNO,////,////,Surgut Airport,"Surgut, Tyumenskaya, Russian Federation (Russia)",OK
    SGD,////,////,EKSB,UNK,1.0,Sonderborg,"Sonderborg, Denmark",OWNO,////,////,????,"Sønderborg, Denmark",OK
    SGE,////,////,EDGS,UNK,1.0,Siegerland Airport,"Siegen, Germany",OWNO,////,////,Siegerland Airport,"Siegen, North Rhine-Westphalia, Germany",OK
    SGF,SGF,KSGF,KSGF,OK,0.728,Springfield-Branson Rg,"Springfield (MO), USA",OWNO,SPRINGFIELD-BRANSON NATIONAL,"SPRINGFIELD, MO - UNITED STATES",Springfield-Branson National Airport,"Springfield, Missouri, United States",OK
    SGH,SGH,KSGH,KSGH,OK,0.506,Springfield,"Springfield (OH), USA",OWNO,SPRINGFIELD-BECKLEY MUNI,"SPRINGFIELD, OH - UNITED STATES",Springfield-Beckley Municipal Airport,"Springfield, Ohio, United States",OK
    SGI,////,////,OPSR,UNK,0.667,Sargodha Apt,"Sargodha, Pakistan",OWNO,////,////,Mushaf AB,"Sargodha, Punjab, Pakistan",OK
    SGK,////,////,////,UNK,1.0,Sangapi,"Sangapi, Papua New Guinea",OWNO,////,////,????,"Sangapi, Madang, Papua-New Guinea",OK
    SGM,////,////,////,UNK,1.0,San Ignacio,"San Ignacio, Mexico",OWNO,////,////,????,"San Ignacio, Baja California Sur, México",OK
    SGN,////,////,VVTS,UNK,0.64,Ho Chi Minh City,"Ho Chi Minh City, Viet Nam",OWNO,////,////,Tan Son Nhat International Airport,"Ho Chi Minh City, Vietnam",OK
    SGO,////,////,YSGE,UNK,1.0,St George,"St George, Australia",OWNO,////,////,????,"St. George, Queensland, Australia",OK
    SGP,////,////,YSHG,UNK,1.0,Shay Gap,"Shay Gap, Australia",OWNO,////,////,????,"Shay Gap, Western Australia, Australia",OK
    SGQ,////,////,////,UNK,1.0,Sanggata,"Sanggata, Indonesia",OWNO,////,////,????,"Sanggata, Kalimantan Timur (East Borneo), Indonesia",OK
    SGR,SGR,KSGR,KSGR,OK,0.805,Sugar Land Municipal,"Houston (TX), USA",OWNO,SUGAR LAND RGNL,"HOUSTON, TX - UNITED STATES",Sugar Land Regional,"Houston, Texas, United States",OK
    SGS,////,////,RPMN,UNK,1.0,Sanga Sanga,"Sanga Sanga, Philippines",OWNO,////,////,Sanga-Sanga,"Bongao, Philippines",OK
    SGT,SGT,KSGT,KSGT,OK,0.771,Stuttgart,"Stuttgart (AR), USA",OWNO,STUTTGART MUNI,"STUTTGART, AR - UNITED STATES",Stuttgart Municipal Airport,"Stuttgart, Arkansas, United States",OK
    SGU,SGU,KSGU,KSGU,OK,0.608,Municipal,"Saint George (UT), USA",OWNO,ST GEORGE RGNL,"ST GEORGE, UT - UNITED STATES",St. George Regional,"St. George, Utah, United States",OK
    SGV,////,////,SAVS,UNK,1.0,Sierra Grande,"Sierra Grande, Argentina",OWNO,////,////,????,"Sierra Grande, Río Negro, Argentina",OK
    SGW,A23,////,////,OK,0.81,Saginaw Bay,"Saginaw Bay (AK), USA",OWNO,SAGINAW,"SAGINAW BAY, AK - UNITED STATES",Saginaw SPB,"Saginaw Bay, Alaska, United States",OK
    SGX,////,////,HTSO,UNK,1.0,Songea,"Songea, Tanzania",OWNO,////,////,Songea Airport,"Songea, Ruvuma, Tanzania",OK
    SGY,SGY,PAGY,PAGY,OK,0.734,Municipal,"Skagway (AK), USA",OWNO,SKAGWAY,"SKAGWAY, AK - UNITED STATES",????,"Skagway, Alaska, United States",OK
    SGZ,////,////,VTSH,UNK,1.0,Songkhla,"Songkhla, Thailand",OWNO,////,////,????,"Songkhla, Songkhla, Thailand",OK
    SHA,////,////,ZSSS,UNK,0.872,Hongqiao,"Shanghai, PR China",OWNO,////,////,Shanghai Hongqiao International Airport,"Shanghai, Shanghai, China",OK
    SHB,////,////,RJCN,UNK,1.0,Nakashibetsu,"Nakashibetsu, Japan",OWNO,////,////,Nakashibetsu Airport,"Nakashibetsu, Hokkaido, Japan",OK
    SHC,////,////,////,UNK,0.786,Indaselassie,"Indaselassie, Ethiopia",OWNO,////,////,Shire Airport,"Shire Indasilase, Tigray, Ethiopia",TO DO CHECK
    SHD,SHD,KSHD,KSHD,OK,0.832,Shenandoah Valley,"Staunton (VA), USA",OWNO,SHENANDOAH VALLEY RGNL,"STAUNTON/WAYNESBORO/HARRISONBURG, VA - UNITED STATES",Shenandoah Valley Regional,"Staunton/Waynesboro/Harrisonburg, Virginia, United States",OK
    SHE,////,////,ZYTX,UNK,0.75,Shenyang,"Shenyang, PR China",OWNO,////,////,Taoxian International Airport,"Shenyang, Liaoning, China",OK
    SHF,////,////,ZWHZ,UNK,0.846,Shanhaiguan,"Shanhaiguan, PR China",OWNO,////,////,Shikezi Huayuan Airport,"Shikezi, Xinjiang, China",OK
    SHG,SHG,PAGH,PAGH,OK,1.0,Shungnak,"Shungnak (AK), USA",OWNO,SHUNGNAK,"SHUNGNAK, AK - UNITED STATES",????,"Shungnak, Alaska, United States",OK
    SHH,SHH,PASH,PASH,OK,1.0,Shishmaref,"Shishmaref (AK), USA",OWNO,SHISHMAREF,"SHISHMAREF, AK - UNITED STATES",????,"Shishmaref, Alaska, United States",OK
    SHI,////,////,RORS,UNK,0.667,Shimojishima,"Shimojishima, Japan",OWNO,////,////,????,"Shimojishima (Shimojijima, Miyakojima)) [Shimojishima Airport], Shimoji Island, Okinawa, Japan",MAYBE
    SHJ,////,////,OMSJ,UNK,0.737,Sharjah,"Sharjah, United Arab Emirates",OWNO,////,////,Sharjah International Airport,"Sharjah, Sharjah, United Arab Emirates",OK
    SHK,////,////,FXSH,UNK,1.0,Sehonghong,"Sehonghong, Lesotho",OWNO,////,////,????,"Sehonghong, Lesotho",OK
    SHL,////,////,VEBI,UNK,0.64,Shillong,"Shillong, India",OWNO,////,////,Barapani,"Shillong, Meghalaya, India",OK
    SHM,////,////,RJBD,UNK,0.667,Shirahama,"Shirahama, Japan",OWNO,////,////,Nanki-Shirahana Airport,"Shirahana, Wakayama, Japan",OK
    SHN,SHN,KSHN,KSHN,OK,1.0,Sanderson Field,"Shelton (WA), USA",OWNO,SANDERSON FIELD,"SHELTON, WA - UNITED STATES",Sanderson Field,"Shelton, Washington, United States",OK
    SHO,////,////,FDSK,UNK,1.0,King Mswati III Intl,Manzini,IATA,////,////,King Mswati III International Airport,"Manzini, Lubombo, Swaziland",MAYBE
    SHP,////,////,ZBSH,UNK,0.739,Qinhuangdao,"Qinhuangdao, PR China",OWNO,////,////,Qinhuangdao Shanhaiguan Airport,"Qinhuangdao, Hubei, China",OK
    SHQ,////,////,YSPT,UNK,1.0,Southport,"Southport, Australia",OWNO,////,////,????,"Southport, Queensland, Australia",OK
    SHR,SHR,KSHR,KSHR,OK,0.704,Sheridan,"Sheridan (WY), USA",OWNO,SHERIDAN COUNTY,"SHERIDAN, WY - UNITED STATES",Sheridan County Airport,"Sheridan, Wyoming, United States",OK
    SHS,////,////,ZHSS,UNK,1.0,Shashi,"Shashi, PR China",OWNO,////,////,????,"Shashi, Hubei, China",OK
    SHT,////,////,YSHT,UNK,1.0,Shepparton,"Shepparton, Australia",OWNO,////,////,????,"Shepparton, Victoria, Australia",OK
    SHU,////,////,YSMP,UNK,1.0,Smith Point,"Smith Point, Australia",OWNO,////,////,????,"Smith Point, Northern Territory, Australia",OK
    SHV,SHV,KSHV,KSHV,OK,1.0,Regional,"Shreveport (LA), USA",OWNO,SHREVEPORT RGNL,"SHREVEPORT, LA - UNITED STATES",Shreveport Regional,"Shreveport, Louisiana, United States",OK
    SHW,////,////,OESH,UNK,1.0,Sharurah,"Sharurah, Saudi Arabia",OWNO,////,////,Sharurah Airport,"Sharurah, Saudi Arabia",OK
    SHX,SHX,PAHX,PAHX,OK,1.0,Shageluk,"Shageluk (AK), USA",OWNO,SHAGELUK,"SHAGELUK, AK - UNITED STATES",????,"Shageluk, Alaska, United States",OK
    SHY,////,////,HTSY,UNK,0.643,Shinyanga,"Shinyanga, Tanzania",OWNO,////,////,Ibadakuli Airport,"Shinyanga, Shinyanga, Tanzania",OK
    SHZ,////,////,FXSS,UNK,1.0,Seshutes,"Seshutes, Lesotho",OWNO,////,////,????,"Seshutes, Lesotho",OK
    SIA,////,////,ZLSN,UNK,1.0,Xiguan,"Xi An, PR China",OWNO,////,////,Xiguan,"Xi'an, Shaanxi, China",OK
    SIB,////,////,FCBS,UNK,1.0,Sibiti,"Sibiti, Congo (ROC)",OWNO,////,////,Sibiti Airport,"Sibiti, Congo (Republic of)",OK
    SIC,////,////,////,UNK,1.0,San Jose Island,San Jose Island,IATA,////,////,San José Island Airport,"San José Island, Panamá, Panamá",MAYBE
    SID,////,////,GVAC,UNK,1.0,Amilcar Cabral International,"Sal, Cape Verde",OWNO,////,////,Amílcar Cabral International Airport,"Espargos, Cape Verde",OK
    SIE,////,////,LPSI,UNK,1.0,Sines,"Sines, Portugal",OWNO,////,////,????,"Sines, Setúbal, Portugal",OK
    SIF,////,////,VNSI,UNK,1.0,Simara,"Simara, Nepal",OWNO,////,////,????,"Simara, Nepal",OK
    SIG,SIG,TJIG,TJIG,OK,0.25,Isla Grande,"San Juan, Puerto Rico",OWNO,FERNANDO LUIS RIBAS DOMINICCI,"SAN JUAN, PR - UNITED STATES",Fernando Luis Ribas Dominicci Airport,"San Juan, Puerto Rico, United States",OK
    SIH,////,////,VNDT,UNK,1.0,Silgadi Doti,"Silgadi Doti, Nepal",OWNO,////,////,????,"Silgadi Doti, Nepal",OK
    SII,////,////,GMMF,UNK,1.0,Sidi Ifni,"Sidi Ifni, Morocco",OWNO,////,////,????,"Sidi Ifni, Morocco",OK
    SIJ,////,////,BISI,UNK,1.0,Siglufjordur,"Siglufjordur, Iceland",OWNO,////,////,????,"Siglufjörður, Iceland",OK
    SIK,SIK,KSIK,KSIK,OK,0.834,Memorial,"Sikeston (MO), USA",OWNO,SIKESTON MEMORIAL MUNI,"SIKESTON, MO - UNITED STATES",Sikeston Memorial Municipal Airport,"Sikeston, Missouri, United States",OK
    SIL,////,////,////,UNK,1.0,Sila,"Sila, Papua New Guinea",OWNO,////,////,????,"Sila, Northern, Papua-New Guinea",OK
    SIM,////,////,////,UNK,1.0,Simbai,"Simbai, Papua New Guinea",OWNO,////,////,????,"Simbai, Western Highlands, Papua-New Guinea",OK
    SIN,////,////,WSSS,UNK,0.897,Changi Airport,"Singapore, Singapore",OWNO,////,////,Changi International Airport,"Singapore, Singapore",OK
    SIO,////,////,YSMI,UNK,1.0,Smithton,"Smithton, Australia",OWNO,////,////,????,"Smithton, Tasmania, Australia",OK
    SIP,////,////,UKFF,UNK,0.833,Simferopol,"Simferopol, Ukraine",OWNO,////,////,Simferopol International Airport,"Simferopol, Adygea, Crimea, Ukraine",OK
    SIQ,////,////,WIDS,UNK,1.0,Dabo,"Singkep, Indonesia",OWNO,////,////,Dabo,"Singkep, Natuna, Riau, Indonesia",OK
    SIR,////,////,LSGS,UNK,1.0,Sion,"Sion, Switzerland",OWNO,////,////,????,"Sion, Valais, Switzerland",OK
    SIS,////,////,FASS,UNK,1.0,Sishen,"Sishen, South Africa",OWNO,////,////,Sishen Airport,"Sishen, Northern Cape, South Africa",OK
    SIT,SIT,PASI,PASI,OK,0.518,Sitka,"Sitka (AK), USA",OWNO,SITKA ROCKY GUTIERREZ,"SITKA, AK - UNITED STATES",Sitka Rocky Gutierrez Airport,"Sitka, Alaska, United States",OK
    SIU,////,////,MNSI,UNK,1.0,Siuna,"Siuna, Nicaragua",OWNO,////,////,????,"Siuna, Atlántico Norte, Nicaragua",OK
    SIV,SIV,KSIV,KSIV,OK,1.0,County,"Sullivan (IN), USA",OWNO,SULLIVAN COUNTY,"SULLIVAN, IN - UNITED STATES",Sullivan County Airport,"Sullivan, Indiana, United States",OK
    SIW,////,////,WIMP,UNK,1.0,Sibisa,"Sibisa, Indonesia",OWNO,////,////,Sibisa Airport,"Parapat, Sumatera Utara, Indonesia",OK
    SIX,////,////,YSGT,UNK,1.0,Singleton,"Singleton, Australia",OWNO,////,////,????,"Singleton, New South Wales, Australia",OK
    SIY,SIY,KSIY,KSIY,OK,1.0,Siskiyou County,"Montague (CA), USA",OWNO,SISKIYOU COUNTY,"MONTAGUE, CA - UNITED STATES",Siskiyou County Airport,"Montague, California, United States",OK
    SIZ,////,////,////,UNK,1.0,Sissano,"Sissano, Papua New Guinea",OWNO,////,////,????,"Sissano, Sandaun, Papua-New Guinea",OK
    SJA,////,////,SPJN,UNK,0.6,San Juan,"San Juan, Peru",OWNO,////,////,San Juan de Marcona Airport,"San Juan de Marcona, Ica, Perú",MAYBE
    SJB,////,////,SLJO,UNK,1.0,San Joaquin,"San Joaquin, Bolivia",OWNO,////,////,San Joaquin Airport,"San Joaquin, Mamoré, El Beni, Bolivia",OK
    SJC,SJC,KSJC,KSJC,OK,0.591,Municipal,"San Jose (CA), USA",OWNO,NORMAN Y MINETA SAN JOSE INTL,"SAN JOSE, CA - UNITED STATES",Norman Y. Mineta San José International Airport,"San José, California, United States",OK
    SJD,////,////,MMSD,UNK,0.8,Los Cabos,"San Jose Cabo, Mexico",OWNO,////,////,Los Cabos International Airport,"Los Cabos, Baja California Sur, México",OK
    SJE,////,////,SKSJ,UNK,0.48,San Jose Del Gua,"San Jose Del Gua, Colombia",OWNO,////,////,Jorge Enrique González Torres Airport,"San José del Guaviare, Guaviare, Colombia",MAYBE
    SJF,////,////,////,UNK,0.529,St John Island,"St John Island, U.S. Virgin Islands",OWNO,////,////,Cruz Bay SPB,"Cruz Bay, St. John, Virgin Islands, United States",MAYBE
    SJG,////,////,////,UNK,1.0,San Pedro Jagua,"San Pedro Jagua, Colombia",OWNO,////,////,????,"San Pedro Jagua, Cundinamarca, Colombia",OK
    SJH,////,////,////,UNK,1.0,San Juan Del Cesar,"San Juan Del Cesar, Colombia",OWNO,////,////,????,"San Juan del Cesar, La Guajira, Colombia",OK
    SJI,////,////,RPUH,UNK,1.0,Mcguire Fld,"San Jose, Philippines",OWNO,////,////,McGuire Field,"San José, Philippines",OK
    SJJ,////,////,LQSA,UNK,0.714,Butmir,"Sarajevo, Bosnia and Herzegovina",OWNO,////,////,Sarajevo International Airport,"Sarajevo, Bosnia and Herzegovina",OK
    SJK,////,////,SBSJ,UNK,0.543,Sao Jose Dos Campos,"Sao Jose Dos Campos, Brazil",OWNO,////,////,Professor Urbano Ernesto Stumpf,"São José dos Campos, São Paulo, Brazil",OK
    SJL,////,////,SBUA,UNK,1.0,Da Cachoeira,"Sao Gabriel, Brazil",OWNO,////,////,????,"São Gabriel da Cachoeira, Amazonas, Brazil",MAYBE
    SJN,SJN,KSJN,KSJN,OK,0.404,Municipal,"St Johns (AZ), USA",OWNO,ST JOHNS INDUSTRIAL AIR PARK,"ST JOHNS, AZ - UNITED STATES",St. Johns Industrial Air Park,"St. Johns, Arizona, United States",OK
    SJO,////,////,MROC,UNK,1.0,Juan Santamaria International,"San Jose, Costa Rica",OWNO,////,////,Juan Santamaría International Airport,"San José, Alajuela, Costa Rica",OK
    SJP,////,////,SBSR,UNK,1.0,Sao Jose Do Rio Preto,"Sao Jose Do Rio Preto, Brazil",OWNO,////,////,????,"São José do Rio Preto, São Paulo, Brazil",OK
    SJQ,////,////,FLSS,UNK,1.0,Sesheke,"Sesheke, Zambia",OWNO,////,////,????,"Sesheke, Zambia",OK
    SJR,////,////,////,UNK,0.867,San Juan D Ur,"San Juan D Ur, Colombia",OWNO,////,////,????,"San Juan de Uraba, Antioquia, Colombia",MAYBE
    SJS,////,////,SLJE,UNK,0.609,San Jose,"San Jose, Bolivia",OWNO,////,////,San José de Chiquitos Airport,"San José de Chiquitos, Chiquitos, Santa Cruz, Bolivia",MAYBE
    SJT,SJT,KSJT,KSJT,OK,1.0,Mathis Field,"San Angelo (TX), USA",OWNO,SAN ANGELO RGNL/MATHIS FIELD,"SAN ANGELO, TX - UNITED STATES",San Angelo Regional/Mathis Field,"San Angelo, Texas, United States",OK
    SJU,SJU,TJSJ,TJSJ,OK,1.0,Luis Munoz Marin International,"San Juan, Puerto Rico",OWNO,LUIS MUNOZ MARIN INTL,"SAN JUAN, PR - UNITED STATES",Luis Muñoz Marín International Airport,"San Juan, Puerto Rico, United States",OK
    SJV,////,////,SLJV,UNK,1.0,San Javier,"San Javier, Bolivia",OWNO,////,////,San Javier Airport,"San Javier, Ñuflo de Chávez, Santa Cruz, Bolivia",OK
    SJW,////,////,ZBSJ,UNK,0.6,Daguocun,"Shijiazhuang, PR China",OWNO,////,////,Shijiazhuang Zhengding International Airport,"Shijiazhuang, Hubei, China",OK
    SJX,////,////,////,UNK,1.0,Sartaneja,"Sartaneja, Belize",OWNO,////,////,Sartaneja Airport,"Sartaneja, Corozal, Belize",OK
    SJY,////,////,EFSI,UNK,0.714,Ilmajoki,"Seinajoki, Finland",OWNO,////,////,Seinäjoki Airport,"Seinäjoki, Etelä-Pohjanmaa (Södra Österbotten (Southern Ostrobothnia)), Finland",OK
    SJZ,////,////,LPSJ,UNK,1.0,Sao Jorge Island,"Sao Jorge Island, Portugal",OWNO,////,////,São Jorge Airport,"São Jorge Island, Região Autónoma dos Açores (Azores), Portugal",OK
    SKA,SKA,KSKA,KSKA,OK,1.0,Fairchild AFB,"Spokane (WA), USA",OWNO,FAIRCHILD AFB,"SPOKANE, WA - UNITED STATES",Fairchild AFB,"Spokane, Washington, United States",OK
    SKB,////,////,TKPK,UNK,0.558,Golden Rock,"St Kitts, St. Kitts And Nevis",OWNO,////,////,Robert L. Bradshaw International Airport,"Basseterre, St. Kitts Island, Saint Kitts and Nevis",MAYBE
    SKC,////,////,////,UNK,1.0,Suki,"Suki, Papua New Guinea",OWNO,////,////,????,"Suki, Western, Papua-New Guinea",OK
    SKD,////,////,UTSS,UNK,0.8,Samarkand,"Samarkand, Uzbekistan",OWNO,////,////,Samarkand International Airport,"Samarkand, Samarqand, Uzbekistan",OK
    SKE,////,////,ENSN,UNK,0.471,Skien,"Skien, Norway",OWNO,////,////,Geiteryggen,"Skien, Norway",OK
    SKF,SKF,KSKF,KSKF,OK,0.674,Kelly AFB,"San Antonio (TX), USA",OWNO,KELLY FLD,"SAN ANTONIO, TX - UNITED STATES",Kelly Field,"San Antonio, Texas, United States",OK

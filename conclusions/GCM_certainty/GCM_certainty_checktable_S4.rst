
List for checking certainty of Great Circle Mapper (SPF - SUN)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    SPF,SPF,KSPF,KSPF,OK,0.656,Black Hills,"Spearfish (SD), USA",OWNO,BLACK HILLS-CLYDE ICE FIELD,"SPEARFISH, SD - UNITED STATES",Black Hills-Clyde Ice Field,"Spearfish, South Dakota, United States",OK
    SPG,SPG,KSPG,KSPG,OK,1.0,Albert Whitted Airport,"St Petersburg (FL), USA",OWNO,ALBERT WHITTED,"ST PETERSBURG, FL - UNITED STATES",Albert Whitted Airport,"St. Petersburg, Florida, United States",OK
    SPH,////,////,////,UNK,1.0,Sopu,"Sopu, Papua New Guinea",OWNO,////,////,????,"Sopu, Central, Papua-New Guinea",OK
    SPI,SPI,KSPI,KSPI,OK,0.64,Capital,"Springfield (IL), USA",OWNO,ABRAHAM LINCOLN CAPITAL,"SPRINGFIELD, IL - UNITED STATES",Abraham Lincoln Capital Airport,"Springfield, Illinois, United States",OK
    SPJ,////,////,LGSP,UNK,1.0,Sparta,"Sparta, Greece",OWNO,////,////,????,"Sparta, Peloponnísos (Peloponnese), Greece",OK
    SPK,////,////,////,UNK,0.516,Metropolitan Area,"Sapporo, Japan",OWNO,////,////,????,"Sapporo, Hokkaido, Japan",OK
    SPM,////,////,ETAD,UNK,0.909,Spangdahlem,"Spangdahlem, Germany",OWNO,////,////,Spangdahlem AB,"Binsfeld, Rhineland-Palatinate, Germany",MAYBE
    SPN,GSN,PGSN,PGSN,OK,1.0,International,"Saipan, Northern Mariana Islands",OWNO,FRANCISCO C ADA/SAIPAN INTL,"SAIPAN ISLAND, CQ - UNITED STATES",Saipan International Airport,"Saipan, Northern Mariana Islands, United States",MAYBE
    SPP,////,////,FNME,UNK,0.593,Menongue,"Menongue, Angola",OWNO,////,////,Serp Pinto,"Menongue, Angola",OK
    SPR,////,////,////,UNK,1.0,San Pedro,"San Pedro, Belize",OWNO,////,////,San Pedro Airport,"San Pedro, Belize, Belize",OK
    SPS,SPS,KSPS,KSPS,OK,1.0,Sheppard AFB,"Wichita Falls (TX), USA",OWNO,SHEPPARD AFB/WICHITA FALLS MUNI,"WICHITA FALLS, TX - UNITED STATES",Sheppard AFB/Wichita Falls Municipal Airport,"Wichita Falls, Texas, United States",OK
    SPT,////,////,////,UNK,1.0,Sipitang,"Sipitang, Malaysia",OWNO,////,////,????,"Sipitang, Sarawak, Malaysia",OK
    SPU,////,////,LDSP,UNK,0.625,Split,"Split, Croatia",OWNO,////,////,Kastel,"Split, Croatia",OK
    SPV,////,////,////,UNK,1.0,Sepik Plains,"Sepik Plains, Papua New Guinea",OWNO,////,////,????,"Sepik Plains, East Sepik, Papua-New Guinea",OK
    SPW,SPW,KSPW,KSPW,OK,1.0,Municipal,"Spencer IA, USA",OWNO,SPENCER MUNI,"SPENCER, IA - UNITED STATES",Spencer Municipal Airport,"Spencer, Iowa, United States",OK
    SPY,////,////,DISP,UNK,1.0,San Pedro,"San Pedro, Cote d'Ivoire",OWNO,////,////,San Pédro Airport,"San Pédro, Bas-Sassandra, Côte d'Ivoire (Ivory Coast)",OK
    SPZ,ASG,KASG,KASG,OK,1.0,Springdale Muni,"Springdale (AR), USA",OWNO,SPRINGDALE MUNI,"SPRINGDALE, AR - UNITED STATES",Springdale Municipal Airport,"Springdale, Arkansas, United States",OK
    SQA,IZA,KIZA,KIZA,OK,1.0,Santa Ynez,"Santa Ynez (CA), USA",OWNO,SANTA YNEZ,"SANTA YNEZ, CA - UNITED STATES",????,"Santa Ynez, California, United States",OK
    SQB,////,////,////,UNK,1.0,Santa Ana,"Santa Ana, Colombia",OWNO,////,////,????,"Santa Ana, Valle del Cauca, Colombia",OK
    SQC,////,////,YSCR,UNK,1.0,Southern Cross,"Southern Cross, Australia",OWNO,////,////,????,"Southern Cross, Western Australia, Australia",OK
    SQE,////,////,////,UNK,0.889,San Luis De Pale,"San Luis De Pale, Colombia",OWNO,////,////,????,"San Luis de Palenque, Casanare, Colombia",MAYBE
    SQG,////,////,WIOS,UNK,0.737,Sintang,"Sintang, Indonesia",OWNO,////,////,Susilo Airport,"Sintang, Kalimantan Barat (West Borneo), Indonesia",OK
    SQH,////,////,VVNS,UNK,1.0,Na-San,"Son-La, Viet Nam",OWNO,////,////,Na-San,"Son-La, Vietnam",OK
    SQI,SQI,KSQI,KSQI,OK,0.674,Whiteside County,"Sterling Rockfalls (IL), USA",OWNO,WHITESIDE CO ARPT-JOS H BITTORF FLD,"STERLING/ROCKFALLS, IL - UNITED STATES",Whiteside Co. Airport-Jos H Bittorf Field,"Sterling/Rockfalls, Illinois, United States",OK
    SQJ,////,////,////,UNK,0.636,Shaxian,Sanming,IATA,////,////,Sanming Shaxian Airport,"Sanming, Fujian, China",MAYBE
    SQK,////,////,////,UNK,1.0,Sidi Barani,"Sidi Barani, Egypt",OWNO,////,////,????,"Sidi Barrani, Matruh (Matrouh), Egypt",OK
    SQL,SQL,KSQL,KSQL,OK,1.0,San Carlos,"San Carlos (CA), USA",OWNO,SAN CARLOS,"SAN CARLOS, CA - UNITED STATES",????,"San Carlos, California, United States",OK
    SQM,////,////,SWUA,UNK,0.917,Sao Miguel Araguaia,"Sao Miguel Araguaia, Brazil",OWNO,////,////,????,"São Miguel de Aragao, Goiás, Brazil",MAYBE
    SQN,////,////,WAPN,UNK,0.632,Sanana,"Sanana, Indonesia",OWNO,////,////,????,"Sanana Island, Maluku Utara, Indonesia",MAYBE
    SQO,////,////,ESUD,UNK,0.727,Gunnarn,"Storuman, Sweden",OWNO,////,////,????,"Storuman, Västerbottens län, Sweden",OK
    SQQ,////,////,EYSA,UNK,0.762,Siauliai,"Siauliai, Lithuania",OWNO,////,////,Siauliai International Airport,"Siauliai, Lithuania",OK
    SQR,////,////,WAWS,UNK,1.0,Soroako,"Soroako, Indonesia",OWNO,////,////,????,"Soroako, Sulawesi Selatan, Indonesia",OK
    SQS,////,////,////,UNK,1.0,Matthew Spain,"San Ignacio, Belize",OWNO,////,////,Matthew Spain Airport,"San Ignacio, Cayo, Belize",OK
    SQV,W28,////,////,OK,1.0,Sequim Valley Airport,"Sequim (WA), USA",OWNO,SEQUIM VALLEY,"SEQUIM, WA - UNITED STATES",Sequim Valley Airport,"Sequim, Washington, United States",OK
    SQW,////,////,EKSV,UNK,1.0,Skive Airport,"Skive, Denmark",OWNO,////,////,????,"Skive, Denmark",OK
    SQZ,////,////,EGXP,UNK,0.8,RAF Station,"Scampton, United Kingdom",OWNO,////,////,RAF Scampton,"Scampton, Lincolnshire, England, United Kingdom",OK
    SRA,////,////,SSZR,UNK,1.0,Santa Rosa,"Santa Rosa, Brazil",OWNO,////,////,????,"Santa Rosa, Rio Grande do Sul, Brazil",OK
    SRB,////,////,SLSR,UNK,0.696,Santa Rosa,"Santa Rosa, Bolivia",OWNO,////,////,Santa Rosa de Yacuma Airport,"Santa Rosa de Yacuma, Yacuma, El Beni, Bolivia",MAYBE
    SRC,SRC,KSRC,KSRC,OK,0.734,Searcy,"Searcy (AR), USA",OWNO,SEARCY MUNI,"SEARCY, AR - UNITED STATES",Searcy Municipal Airport,"Searcy, Arkansas, United States",OK
    SRD,////,////,SLRA,UNK,1.0,San Ramon,"San Ramon, Bolivia",OWNO,////,////,San Ramón Airport,"San Ramón, Mamoré, El Beni, Bolivia",OK
    SRE,////,////,SLSU,UNK,0.276,Sucre,"Sucre, Bolivia",OWNO,////,////,Juana Azurduy de Padilla International Airport,"Sucre, Oropeza, Chuquisaca, Bolivia",OK
    SRG,////,////,WARS,UNK,0.889,Achmad Uani,"Semarang, Indonesia",OWNO,////,////,Achmad Yani International Airport,"Semarang, Jawa Tengah, Indonesia",OK
    SRH,////,////,FTTA,UNK,1.0,Sarh,"Sarh, Chad",OWNO,////,////,????,"Sarh, Moyen-Chari, Chad",OK
    SRI,////,////,WALS,UNK,0.643,Samarinda,"Samarinda, Indonesia",OWNO,////,////,Temindung Airport,"Samarinda, Kalimantan Timur (East Borneo), Indonesia",OK
    SRJ,////,////,SLSB,UNK,0.806,Capitan G Q Guardia,"San Borja, Bolivia",OWNO,////,////,Capitán Av. Germán Quiroga Guardia Airport,"San Borja, José Ballivián, El Beni, Bolivia",OK
    SRL,////,////,////,UNK,1.0,Santa Rosalia,"Santa Rosalia, Mexico",OWNO,////,////,????,"Santa Rosalía, Baja California Sur, México",OK
    SRM,////,////,////,UNK,1.0,Sandringham,"Sandringham, Australia",OWNO,////,////,????,"Sandringham, Queensland, Australia",OK
    SRN,////,////,YSRN,UNK,1.0,Strahan,"Strahan, Australia",OWNO,////,////,????,"Strahan, Tasmania, Australia",OK
    SRO,////,////,////,UNK,1.0,Santana Ramos,"Santana Ramos, Colombia",OWNO,////,////,????,"Santana Ramos, Caquetá, Colombia",OK
    SRP,////,////,ENSO,UNK,0.476,Stord Airport,"Stord, Norway",OWNO,////,////,Sørstokken,"Stord, Norway",OK
    SRQ,SRQ,KSRQ,KSRQ,OK,1.0,Sarasota-Bradenton International Airport,"Sarasota (FL), USA",OWNO,SARASOTA/BRADENTON INTL,"SARASOTA/BRADENTON, FL - UNITED STATES",Sarasota/Bradenton International Airport,"Sarasota/Bradenton, Florida, United States",OK
    SRS,////,////,SKSR,UNK,1.0,San Marcos,"San Marcos, Colombia",OWNO,////,////,San Marcos Airport,"San Marcos, Sucre, Colombia",OK
    SRT,////,////,HUSO,UNK,1.0,Soroti,"Soroti, Uganda",OWNO,////,////,????,"Soroti, Uganda",OK
    SRV,SRV,////,////,OK,1.0,Stony River,"Stony River (AK), USA",OWNO,STONY RIVER 2,"STONY RIVER, AK - UNITED STATES",Stony River 2 Airport,"Stony River, Alaska, United States",OK
    SRW,RUQ,KRUQ,KRUQ,OK,1.0,Rowan County,"Salisbury (NC), USA",OWNO,ROWAN COUNTY,"SALISBURY, NC - UNITED STATES",Rowan County Airport,"Salisbury, North Carolina, United States",OK
    SRX,////,////,HLGD,UNK,1.0,Sert,"Sert, Libya",OWNO,////,////,????,"Sert, Libyan Arab Jamahiriya (Libya)",OK
    SRY,////,////,OINZ,UNK,1.0,Dashte Naz,"Sary, Iran",OWNO,////,////,Dashte E. Naz,"Sary, Mazandaran, Iran",OK
    SRZ,////,////,SLET,UNK,1.0,El Trompillo,"Santa Cruz, Bolivia",OWNO,////,////,El Trompillo,"Santa Cruz, Andrés Ibáñez, Santa Cruz, Bolivia",OK
    SSA,////,////,SBSV,UNK,0.889,Luis Eduardo Magalhaes International,"Salvador, Brazil",OWNO,////,////,Deputado Luís Eduardo Magalhães International Airport,"Salvador, Bahia, Brazil",OK
    SSB,VI32,////,////,OK,0.281,SPB,"St Croix Island, U.S. Virgin Islands",OWNO,CHRISTIANSTED HARBOR-SSB,"CHRISTIANSTED ST CROIX, VI - UNITED STATES",Christiansted Harbor-SSB SPB,"Christiansted, St. Croix, Virgin Islands, United States",MAYBE
    SSC,SSC,KSSC,KSSC,OK,1.0,Shaw AFB,"Sumter (SC), USA",OWNO,SHAW AFB,"SUMTER, SC - UNITED STATES",Shaw AFB,"Sumter, South Carolina, United States",OK
    SSD,////,////,SCSF,UNK,0.621,San Felipe,San Felipe,IATA,////,////,Victor Lafón,"San Felipe, Valparaíso, Chile",MAYBE
    SSE,////,////,VASL,UNK,1.0,Solapur Airport,"Sholapur, India",OWNO,////,////,????,"Sholapur, Maharashtra, India",OK
    SSF,SSF,KSSF,KSSF,OK,1.0,Stinson Municipal,"San Antonio (TX), USA",OWNO,STINSON MUNI,"SAN ANTONIO, TX - UNITED STATES",Stinson Municipal Airport,"San Antonio, Texas, United States",OK
    SSG,////,////,FGSL,UNK,0.667,Santa Isabel,"Malabo, Equatorial Guinea",OWNO,////,////,Malabo International Airport,"Malabo, Bioko Island, Bioko Norte, Equatorial Guinea",OK
    SSH,////,////,HESH,UNK,0.81,Ophira,"Sharm El Sheikh, Egypt",OWNO,////,////,Sharm el-Sheikh International Airport,"Sharm el-Sheikh, Janub Sina (South Sinai), Egypt",OK
    SSI,SSI,KSSI,KSSI,OK,0.578,Mckinnon,"Brunswick (GA), USA",OWNO,MCKINNON ST SIMONS ISLAND,"BRUNSWICK, GA - UNITED STATES",McKinnon St. Simons Island Airport,"Brunswick, Georgia, United States",OK
    SSJ,////,////,ENST,UNK,1.0,Stokka,"Sandnessjoen, Norway",OWNO,////,////,Stokka,"Sandnessjøen, Norway",OK
    SSK,////,////,////,UNK,1.0,Sturt Creek,"Sturt Creek, Australia",OWNO,////,////,????,"Sturt Creek, Western Australia, Australia",OK
    SSL,////,////,////,UNK,1.0,Santa Rosalia,"Santa Rosalia, Colombia",OWNO,////,////,????,"Santa Rosalia, Vichada, Colombia",OK
    SSN,////,////,RKSM,UNK,1.0,Seoul Ab,"Seoul, South Korea",OWNO,////,////,Seoul AB,"Seongnam, Republic of Korea (South Korea)",OK
    SSO,////,////,SNLO,UNK,1.0,Sao Lourenco,"Sao Lourenco, Brazil",OWNO,////,////,????,"São Lourenço, Minas Gerais, Brazil",OK
    SSP,////,////,////,UNK,1.0,Silver Plains,"Silver Plains, Australia",OWNO,////,////,????,"Silver Plains, Queensland, Australia",OK
    SSQ,////,////,////,UNK,1.0,La Sarre,"La Sarre, Canada",OWNO,////,////,????,"La Sarre, Québec, Canada",OK
    SSR,////,////,NVSH,UNK,1.0,Sara,"Sara, Vanuatu",OWNO,////,////,????,"Sara, Pentecost (Pentecôte) Island, Pénama, Vanuatu",OK
    SSS,////,////,////,UNK,1.0,Siassi,"Siassi, Papua New Guinea",OWNO,////,////,????,"Siassi, Morobe, Papua-New Guinea",OK
    SST,////,////,SAZL,UNK,1.0,Santa Teresita,"Santa Teresita, Argentina",OWNO,////,////,????,"Santa Teresita, Buenos Aires, Argentina",OK
    SSV,////,////,////,UNK,1.0,Siasi,"Siasi, Philippines",OWNO,////,////,????,"Siasi, Philippines",OK
    SSW,7WA5,////,////,OK,0.734,Stuart Island,"Stuart Island (WA), USA",OWNO,STUART ISLAND AIRPARK,"STUART ISLAND, WA - UNITED STATES",Stuart Island Airpark,"Stuart Island, Washington, United States",OK
    SSY,////,////,FNBC,UNK,1.0,M'Banza Congo,"M'Banza Congo, Angola",OWNO,////,////,????,"M'Banza Congo, Angola",OK
    SSZ,////,////,SBST,UNK,0.8,Santos,"Santos, Brazil",OWNO,////,////,Santos AB,"Santos, São Paulo, Brazil",OK
    STA,////,////,EKVJ,UNK,1.0,Stauning,"Stauning, Denmark",OWNO,////,////,????,"Stauning, Denmark",OK
    STB,////,////,SVSZ,UNK,0.718,L Delicias,"Santa Barbara Ed, Venezuela",OWNO,////,////,????,"Santa Barbara del Zulia, Zulia, Venezuela",MAYBE
    STC,STC,KSTC,KSTC,OK,0.666,Municipal,"Saint Cloud (MN), USA",OWNO,ST CLOUD RGNL,"ST CLOUD, MN - UNITED STATES",St. Cloud Regional,"St. Cloud, Minnesota, United States",OK
    STD,////,////,SVSO,UNK,0.579,Mayo Guerrero,"Santo Domingo, Venezuela",OWNO,////,////,Mayor Buenaventura Vivas,"Santo Domingo, Táchira, Venezuela",OK
    STE,STE,KSTE,KSTE,OK,0.826,Stevens Point,"Stevens Point (WI), USA",OWNO,STEVENS POINT MUNI,"STEVENS POINT, WI - UNITED STATES",Stevens Point Municipal Airport,"Stevens Point, Wisconsin, United States",OK
    STF,////,////,////,UNK,1.0,Stephen Island,"Stephen Island, Australia",OWNO,////,////,????,"Stephens Island, Queensland, Australia",OK
    STG,PBV,PAPB,PAPB,OK,0.704,St George Island,"St George Island (AK), USA",OWNO,ST GEORGE,"ST GEORGE, AK - UNITED STATES",St. George Airport,"St. George, Alaska, United States",OK
    STH,////,////,YSMR,UNK,1.0,Strathmore,"Strathmore, Australia",OWNO,////,////,????,"Strathmore, Queensland, Australia",OK
    STI,////,////,MDST,UNK,0.696,Municipal,"Santiago, Dominican Republic",OWNO,////,////,Cibao International Airport,"Santiago de los Caballeros, Santiago, Dominican Republic",OK
    STJ,STJ,KSTJ,KSTJ,OK,1.0,Rosecrans Memorial,"St Joseph (MO), USA",OWNO,ROSECRANS MEMORIAL,"ST JOSEPH, MO - UNITED STATES",Rosecrans Memorial Airport,"St. Joseph, Missouri, United States",OK
    STK,STK,KSTK,KSTK,OK,0.682,Crosson Field,"Sterling (CO), USA",OWNO,STERLING MUNI,"STERLING, CO - UNITED STATES",Sterling Municipal Airport,"Sterling, Colorado, United States",OK
    STL,STL,KSTL,KSTL,OK,1.0,Lambert-St Louis International,"St Louis (MO), USA",OWNO,LAMBERT-ST LOUIS INTL,"ST LOUIS, MO - UNITED STATES",Lambert-St. Louis International Airport,"St. Louis, Missouri, United States",OK
    STM,////,////,SBSN,UNK,0.871,Maestro Wilson Fonseca Airport,"Santarem (Santarém), Brazil",OWNO,////,////,Santarém - Maestro Wilson Fonseca Airport,"Santarém, Pará, Brazil",MAYBE
    STN,////,////,EGSS,UNK,1.0,Stansted,"London, United Kingdom",OWNO,////,////,Stansted,"London, Essex, England, United Kingdom",OK
    STO,////,////,////,UNK,1.0,Metropolitan Area,"Stockholm, Sweden",OWNO,////,////,Metropolitan Area,"Stockholm, Sweden",OK
    STP,STP,KSTP,KSTP,OK,0.64,St. Paul Downtown,"St Paul (MN), USA",OWNO,ST PAUL DOWNTOWN HOLMAN FLD,"ST PAUL, MN - UNITED STATES",St. Paul Downtown Holman Field,"St. Paul, Minnesota, United States",OK
    STQ,OYM,KOYM,KOYM,OK,0.801,St Marys,"St Marys (PA), USA",OWNO,ST MARYS MUNI,"ST MARYS, PA - UNITED STATES",St. Marys Municipal Airport,"St. Marys, Pennsylvania, United States",OK
    STR,////,////,EDDS,UNK,1.0,Echterdingen,"Stuttgart, Germany",OWNO,////,////,Echterdingen,"Stuttgart, Baden-Württemberg, Germany",OK
    STS,STS,KSTS,KSTS,OK,0.719,Sonoma County,"Santa Rosa (CA), USA",OWNO,CHARLES M SCHULZ - SONOMA COUNTY,"SANTA ROSA, CA - UNITED STATES",Charles M Schulz - Sonoma County Airport,"Santa Rosa, California, United States",OK
    STT,STT,TIST,TIST,OK,0.197,H.S.Truman,"St Thomas Island, U.S. Virgin Islands",OWNO,CYRIL E KING,"CHARLOTTE AMALIE, VI - UNITED STATES",Cyril E King Airport,"Charlotte Amalie, Virgin Islands, United States",TO DO CHECK
    STU,////,////,////,UNK,1.0,Santa Cruz,"Santa Cruz, Belize",OWNO,////,////,Santa Cruz Airport,"Santa Cruz, Corozal, Belize",OK
    STV,////,////,VASU,UNK,1.0,Surat,"Surat, India",OWNO,////,////,????,"Surat, Gujarat, India",OK
    STW,////,////,URMT,UNK,0.643,Stavropol,"Stavropol, Russia",OWNO,////,////,Shpakovskoye Airport,"Stavropol, Stavropol'skiy, Russian Federation (Russia)",OK
    STX,STX,TISX,TISX,OK,0.242,Alex Hamilton,"St Croix Island, U.S. Virgin Islands",OWNO,HENRY E ROHLSEN,"CHRISTIANSTED, VI - UNITED STATES",Henry E Rohlsen Airport,"Christiansted, Virgin Islands, United States",TO DO CHECK
    STY,////,////,SUSO,UNK,0.313,Salto,"Salto, Uruguay",OWNO,////,////,Nueva Hespérides International Airport,"Salto, Salto, Uruguay",OK
    STZ,////,////,SWST,UNK,0.8,Confresa,"Santa Terezinha, Brazil",OWNO,////,////,Aeropuerto Santa Terezinha,"Santa Terezinha, Mato Grosso, Brazil",OK
    SUA,SUA,KSUA,KSUA,OK,1.0,Witham Field,"Stuart (FL), USA",OWNO,WITHAM FIELD,"STUART, FL - UNITED STATES",Witham Field,"Stuart, Florida, United States",OK
    SUB,////,////,WARR,UNK,0.857,Juanda,"Surabaya, Indonesia",OWNO,////,////,Juanda International Airport,"Surabaya, Jawa Timur, Indonesia",OK
    SUD,SUD,KSUD,KSUD,OK,0.734,Stroud,"Stroud (OK), USA",OWNO,STROUD MUNI,"STROUD, OK - UNITED STATES",Stroud Municipal Airport,"Stroud, Oklahoma, United States",OK
    SUE,SUE,KSUE,KSUE,OK,0.752,Door County,"Sturgeon Bay (WI), USA",OWNO,DOOR COUNTY CHERRYLAND,"STURGEON BAY, WI - UNITED STATES",Door County Cherryland Airport,"Sturgeon Bay, Wisconsin, United States",OK
    SUF,////,////,LICA,UNK,0.765,S Eufemia,"Lamezia-Terme, Italy",OWNO,////,////,????,"Lamezia Terme, Calabria, Italy",OK
    SUG,////,////,RPMS,UNK,1.0,Surigao,"Surigao, Philippines",OWNO,////,////,????,"Surigao, Mindanao Island, Philippines",OK
    SUH,////,////,OOSR,UNK,1.0,Sur,"Sur, Oman",OWNO,////,////,????,"Sur, Oman",OK
    SUI,////,////,UGSS,UNK,0.8,Babusheri,"Sukhumi, Georgia",OWNO,////,////,Sukhumi Dranda Airport,"Sukhumi, Abkhazia, Georgia",OK
    SUJ,////,////,LRSM,UNK,1.0,Satu Mare,"Satu Mare, Romania",OWNO,////,////,????,"Satu Mare, Romania",OK
    SUK,////,////,UEBS,UNK,1.0,Sakkyryr,Sakkyryr,IATA,////,////,Sakkyryr Airport,"Batagay-Alyta, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    SUL,////,////,OPSU,UNK,1.0,Sui,"Sui, Pakistan",OWNO,////,////,????,"Sui, Balochistan, Pakistan",OK
    SUM,SMS,KSMS,KSMS,OK,0.81,Municipal,"Sumter (SC), USA",OWNO,SUMTER,"SUMTER, SC - UNITED STATES",????,"Sumter, South Carolina, United States",OK
    SUN,SUN,KSUN,KSUN,OK,0.346,Hailey/Sun Valley Airport,"Sun Valley (ID), USA",OWNO,FRIEDMAN MEMORIAL,"HAILEY, ID - UNITED STATES",Friedman Memorial Airport,"Hailey, Idaho, United States",OK

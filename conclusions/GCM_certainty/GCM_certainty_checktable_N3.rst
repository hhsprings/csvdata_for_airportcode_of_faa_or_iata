
List for checking certainty of Great Circle Mapper (NSE - NZY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    NSE,NSE,KNSE,KNSE,OK,0.835,Whiting Field NAS,"Milton (FL), USA",OWNO,WHITING FIELD NAS NORTH,"MILTON, FL - UNITED STATES",Whiting Field NAS North Airport,"Milton, Florida, United States",OK
    NSH,////,////,OINN,UNK,1.0,Now Shahr,"Now Shahr, Iran",OWNO,////,////,????,"Now Shahr, Mazandaran, Iran",OK
    NSI,////,////,FKYS,UNK,0.865,Nsimalen,"Yaounde, Cameroon",OWNO,////,////,Yaoundé Nsimalen International Airport,"Yaoundé, Centre, Cameroon",OK
    NSK,////,////,UOOO,UNK,0.7,Noril'sk,"Noril'sk, Russia",OWNO,////,////,Alykel Airport,"Norilsk, Krasnoyarskiy, Russian Federation (Russia)",OK
    NSM,////,////,YNSM,UNK,1.0,Norseman,"Norseman, Australia",OWNO,////,////,????,"Norseman, Western Australia, Australia",OK
    NSN,////,////,NZNS,UNK,1.0,Nelson,"Nelson, New Zealand",OWNO,////,////,????,"Nelson, New Zealand",OK
    NSO,////,////,YSCO,UNK,1.0,Scone,"Scone, Australia",OWNO,////,////,????,"Scone, New South Wales, Australia",OK
    NSP,////,////,RPLS,UNK,0.593,NAF,"Sangley Point, Philippines",OWNO,////,////,Major Danilo Atienza AB,"Sangley Point, Luzon Island, Philippines",OK
    NST,////,////,VTSF,UNK,1.0,Nakhon Si Thammarat,"Nakhon Si Thammarat, Thailand",OWNO,////,////,????,"Nakhon Si Thammarat, Nakhon Si Thammarat, Thailand",OK
    NSV,////,////,YNSH,UNK,1.0,Noosaville,"Noosaville, Australia",OWNO,////,////,????,"Noosaville, Queensland, Australia",OK
    NSY,////,////,LICZ,UNK,0.947,Sigonella NAF,"Sigonella, Italy",OWNO,////,////,NAS Sigonella,"Sigonella, Sicily, Italy",OK
    NTA,////,////,////,UNK,1.0,Natadola,"Natadola, Fiji",OWNO,////,////,????,"Natadola, Fiji",OK
    NTB,////,////,ENNO,UNK,1.0,Notodden,"Notodden, Norway",OWNO,////,////,????,"Notodden, Norway",OK
    NTD,NTD,KNTD,KNTD,OK,1.0,Point Mugu NAS,"Port Hueneme (CA), USA",OWNO,POINT MUGU NAS (NAVAL BASE VENTURA CO),"OXNARD, CA - UNITED STATES",NAS Point Mugu,"Oxnard, California, United States",OK
    NTE,////,////,LFRS,UNK,1.0,Nantes Atlantique Airport,"Nantes, France",OWNO,////,////,Atlantique,"Nantes, Pays de la Loire, France",OK
    NTG,////,////,ZSNT,UNK,0.774,Nantong,"Nantong, PR China",OWNO,////,////,Xingdong Airport,"Nantong, Jiangsu, China",OK
    NTI,////,////,WASB,UNK,0.6,Bintuni,"Bintuni, Indonesia",OWNO,////,////,Steenkool Airport,"Bintuni, Papua Barat, Indonesia",OK
    NTJ,41U,////,////,OK,1.0,Manti-Ephraim,"Manti (UT), USA",OWNO,MANTI-EPHRAIM,"MANTI, UT - UNITED STATES",Manti-Ephraim Airport,"Manti, Utah, United States",OK
    NTL,////,////,YWLM,UNK,1.0,Williamtown,"Newcastle, Australia",OWNO,////,////,Williamtown Airport,"Newcastle, New South Wales, Australia",OK
    NTM,////,////,////,UNK,0.75,Miracema Do Norte,"Miracema Do Norte, Brazil",OWNO,////,////,????,"Miracema do Tocantins, Tocantins, Brazil",MAYBE
    NTN,////,////,YNTN,UNK,1.0,Normanton,"Normanton, Australia",OWNO,////,////,????,"Normanton, Queensland, Australia",OK
    NTO,////,////,GVAN,UNK,0.595,Santo Antao,"Santo Antao, Cape Verde",OWNO,////,////,Agostinho Neto,"Ponta do Sol, Ribeira Grande, Santo Antão, Cape Verde",OK
    NTQ,////,////,RJNW,UNK,nan,////,////,////,////,////,Noto Airport,"Wajima, Ishikawa, Japan",UNK
    NTR,////,////,MMAN,UNK,0.923,Aeropuerto Del Norte,"Monterrey, Mexico",OWNO,////,////,Aeropuerto del Norte International Airport,"Monterrey, Nuevo León, México",OK
    NTT,////,////,NFTP,UNK,1.0,Kuini Lavenia,"Niuatoputapu, Tonga",OWNO,////,////,Kuini Lavenia,"Niuatoputapu, Niuafo'ou Island, Tonga",OK
    NTU,NTU,KNTU,KNTU,OK,1.0,NAS,"Oceana (VA), USA",OWNO,OCEANA NAS /APOLLO SOUCEK FIELD/,"VIRGINIA BEACH, VA - UNITED STATES",NAS Oceana,"Virginia Beach, Virginia, United States",OK
    NTX,////,////,WION,UNK,1.0,Natuna Ranai,"Natuna Ranai, Indonesia",OWNO,////,////,????,"Natuna Ranai, Natuna Island, Riau, Indonesia",OK
    NTY,////,////,FAPN,UNK,0.833,Pilansberg,"Sun City, South Africa",OWNO,////,////,Pilansberg International Airport,"Pilansberg, North West, South Africa",OK
    NUA,////,////,////,UNK,0.688,Gregory Lake SPB,Nuwara Eliya,IATA,////,////,Lake Gregory Waterdrome,"Nuwara Eliya, Central Province, Sri Lanka (Ceylon)",MAYBE
    NUB,////,////,YNUM,UNK,1.0,Numbulwar,"Numbulwar, Australia",OWNO,////,////,????,"Numbulwar, Northern Territory, Australia",OK
    NUD,////,////,HSNH,UNK,1.0,En Nahud,"En Nahud, Sudan",OWNO,////,////,????,"En Nahud, West Kordofan, Sudan",OK
    NUE,////,////,EDDN,UNK,0.966,Nuremberg Airport,"Nürnberg, Bavaria, Germany",OWNO,////,////,????,"Nürnberg, Bavaria, Germany",OK
    NUF,////,////,////,UNK,0.704,Castlereigh Resrvr SPB,Hatton,IATA,////,////,Castlereigh Reservoir Waterdrome,"Hatton, Central Province, Sri Lanka (Ceylon)",MAYBE
    NUG,////,////,////,UNK,1.0,Nuguria,"Nuguria, Papua New Guinea",OWNO,////,////,????,"Nuguria, Bougainville, Papua-New Guinea",OK
    NUH,////,////,////,UNK,1.0,Nunchia,"Nunchia, Colombia",OWNO,////,////,????,"Nunchia, Casanare, Colombia",OK
    NUI,AQT,PAQT,PAQT,OK,1.0,Nuiqsut,"Nuiqsut (AK), USA",OWNO,NUIQSUT,"NUIQSUT, AK - UNITED STATES",????,"Nuiqsut, Alaska, United States",OK
    NUK,////,////,NTGW,UNK,1.0,Nukutavake,"Nukutavake, French Polynesia",OWNO,////,////,????,"Nukutavake, French Polynesia",OK
    NUL,NUL,PANU,PANU,OK,1.0,Nulato,"Nulato (AK), USA",OWNO,NULATO,"NULATO, AK - UNITED STATES",????,"Nulato, Alaska, United States",OK
    NUN,NUN,KNUN,KNUN,OK,0.753,Saufley NAS,"Pensacola (FL), USA",OWNO,SAUFLEY FIELD NOLF,"PENSACOLA, FL - UNITED STATES",Saufley Field NOLF Airport,"Pensacola, Florida, United States",OK
    NUP,16A,PPIT,PPIT,OK,1.0,Nunapitchuk,"Nunapitchuk (AK), USA",OWNO,NUNAPITCHUK,"NUNAPITCHUK, AK - UNITED STATES",????,"Nunapitchuk, Alaska, United States",OK
    NUQ,NUQ,KNUQ,KNUQ,OK,0.699,Moffett Field,"Mountain View (CA), USA",OWNO,MOFFETT FEDERAL AFLD,"MOUNTAIN VIEW, CA - UNITED STATES",Moffett Federal Airfield,"Mountain View, California, United States",OK
    NUR,////,////,YNUB,UNK,0.762,Nullarbor,"Nullarbor, Australia",OWNO,////,////,????,"Nullarbor Motel, South Australia, Australia",MAYBE
    NUS,////,////,NVSP,UNK,1.0,Norsup,"Norsup, Vanuatu",OWNO,////,////,????,"Norsup, Malekula Island, Malampa, Vanuatu",OK
    NUT,////,////,////,UNK,1.0,Nutuve,"Nutuve, Papua New Guinea",OWNO,////,////,????,"Nutuve, East New Britain, Papua-New Guinea",OK
    NUU,////,////,HKNK,UNK,1.0,Nakuru,"Nakuru, Kenya",OWNO,////,////,????,"Nakuru, Kenya",OK
    NUW,NUW,KNUW,KNUW,OK,1.0,Ault Field,"Whidbey Island (WA), USA",OWNO,WHIDBEY ISLAND NAS /AULT FIELD,"OAK HARBOR, WA - UNITED STATES",Whidbey Island NAS /Ault Field,"Oak Harbor, Washington, United States",OK
    NUX,////,////,USMU,UNK,1.0,Novy Urengoy,"Novy Urengoy, Russia",OWNO,////,////,Novy Urengoy Airport,"Novy Urengoy, Yamalo-Nenetskiy, Russian Federation (Russia)",OK
    NVA,////,////,SKNV,UNK,0.64,La Marguita,"Neiva, Colombia",OWNO,////,////,Benito Salas Airport,"Neiva, Huila, Colombia",OK
    NVD,NVD,KNVD,KNVD,OK,0.734,Nevada,"Nevada (MO), USA",OWNO,NEVADA MUNI,"NEVADA, MO - UNITED STATES",Nevada Municipal Airport,"Nevada, Missouri, United States",OK
    NVG,////,////,////,UNK,1.0,Nueva Guinea,"Nueva Guinea, Nicaragua",OWNO,////,////,????,"Nueva Guinea, Atlántico Sur, Nicaragua",OK
    NVI,////,////,UTSA,UNK,nan,////,////,////,////,////,Navoi International Airport,"Navoi, Navoiy, Uzbekistan",UNK
    NVK,////,////,ENNK,UNK,1.0,Framnes,"Narvik, Norway",OWNO,////,////,Narvik/Framnes,"Narvik, Norway",OK
    NVP,////,////,SWNA,UNK,1.0,Novo Aripuana,"Novo Aripuana, Brazil",OWNO,////,////,????,"Novo Aripuanã, Amazonas, Brazil",OK
    NVR,////,////,ULNN,UNK,1.0,Novgorod,"Novgorod, Russia",OWNO,////,////,Novgorod Airport,"Novgorod, Novgorodskaya, Russian Federation (Russia)",OK
    NVS,////,////,LFQG,UNK,0.5,Nevers,"Nevers, France",OWNO,////,////,Fourchambault,"Neveras, Bourgogne (Burgundy), France",OK
    NVT,////,////,SBNF,UNK,0.417,Navegantes,"Navegantes, Brazil",OWNO,////,////,Ministro Victor Konder International Airport,"Navegantes, Santa Catarina, Brazil",OK
    NVY,////,////,VONV,UNK,1.0,Neyveli,"Neyveli, India",OWNO,////,////,????,"Neyveli, Tamil Nadu, India",OK
    NWA,////,////,FMCI,UNK,0.429,Moheli,"Moheli, Comoros",OWNO,////,////,Bandar es Salam,"Moheli, Comoros",OK
    NWH,2B3,////,////,OK,1.0,Parlin Field,"Newport (NH), USA",OWNO,PARLIN FIELD,"NEWPORT, NH - UNITED STATES",Parlin Field,"Newport, New Hampshire, United States",OK
    NWI,////,////,EGSH,UNK,0.778,Norwich Airport,"Norwich, United Kingdom",OWNO,////,////,Norwich International Airport,"Norwich, Norfolk, England, United Kingdom",OK
    NWT,////,////,////,UNK,1.0,Nowata,"Nowata, Papua New Guinea",OWNO,////,////,????,"Nowata, Milne Bay, Papua-New Guinea",OK
    NYA,////,////,USHN,UNK,nan,////,////,////,////,////,Nyagan Airport,"Nyagan, Tyumenskaya, Russian Federation (Russia)",UNK
    NYC,////,////,////,UNK,1.0,Metropolitan Area,"New York (NY), USA",OWNO,////,////,Metropolitan Area,"New York, New York, United States",OK
    NYE,////,////,HKNI,UNK,1.0,Nyeri,"Nyeri, Kenya",OWNO,////,////,????,"Nyeri, Kenya",OK
    NYG,NYG,KNYG,KNYG,OK,0.712,Quantanico NAS,"Quantico (VA), USA",OWNO,QUANTICO MCAF /TURNER FIELD,"QUANTICO, VA - UNITED STATES",Quantico McAf /Turner Field,"Quantico, Virginia, United States",OK
    NYI,////,////,DGSN,UNK,1.0,Sunyani,"Sunyani, Ghana",OWNO,////,////,????,"Sunyani, Ghana",OK
    NYK,////,////,HKNY,UNK,1.0,Nanyuki,"Nanyuki, Kenya",OWNO,////,////,????,"Nanyuki, Kenya",OK
    NYM,////,////,USMM,UNK,1.0,Nadym,"Nadym, Russia",OWNO,////,////,Nadym Airport,"Nadym, Yamalo-Nenetskiy, Russian Federation (Russia)",OK
    NYN,////,////,YNYN,UNK,1.0,Nyngan,"Nyngan, Australia",OWNO,////,////,????,"Nyngan, New South Wales, Australia",OK
    NYO,////,////,ESKN,UNK,0.933,Skavsta,"Stockholm, Sweden",OWNO,////,////,Skavsta AB,"Stockholm, Södermanlands län, Sweden",OK
    NYR,////,////,UENN,UNK,nan,////,////,////,////,////,Nyurba Airport,"Nyurba, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    NYT,////,////,VYEL,UNK,0.615,International,Nay Pyi Taw,IATA,////,////,Naypyidaw International Airport,"Naypyidaw, Nay Pyi Taw (Naypyidaw), Myanmar (Burma)",TO DO CHECK
    NYU,////,////,VYBG,UNK,1.0,NyaungU,"Nyaung-u, Myanmar",OWNO,////,////,????,"Nyaung-U (Nyaung Oo) / Bagan, Mandalay, Myanmar (Burma)",MAYBE
    NYW,////,////,VYMY,UNK,nan,////,////,////,////,////,Monywar Airport,"Monywar, Sagaing, Myanmar (Burma)",UNK
    NZC,////,////,SPZA,UNK,0.864,Maria Reiche Neuman,Nazca,IATA,////,////,Nasca - María Reiche Neuman Airport,"Nazca, Ica, Perú",MAYBE
    NZE,////,////,GUNZ,UNK,1.0,Nzerekore,"Nzerekore, Guinea",OWNO,////,////,????,"Nzerekore, Guinea",OK
    NZH,////,////,ZBMZ,UNK,0.545,Xijiao,Manzhouli,IATA,////,////,Manzhouli Xijiao Airport,"Manzhouli, Inner Mongolia, China",MAYBE
    NZY,NZY,KNZY,KNZY,OK,1.0,North Island NAS,"San Diego (CA), USA",OWNO,NORTH ISLAND NAS /HALSEY FIELD/,"SAN DIEGO, CA - UNITED STATES",NAS North Island,"San Diego, California, United States",OK


List for checking certainty of Great Circle Mapper (SUO - SZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    SUO,S21,////,////,OK,0.618,Sun River,"Sun River (OR), USA",OWNO,SUNRIVER,"SUNRIVER, OR - UNITED STATES",????,"Sunriver, Oregon, United States",OK
    SUP,////,////,WART,UNK,1.0,Trunojoyo,"Sumenep, Indonesia",OWNO,////,////,Trunojoyo Airport,"Sumenep, Jawa Timur, Indonesia",OK
    SUQ,////,////,SESC,UNK,1.0,Sucua,"Sucua, Ecuador",OWNO,////,////,????,"Sucúa, Morona-Santiago, Ecuador",OK
    SUR,////,////,////,UNK,1.0,Summer Beaver,"Summer Beaver, Canada",OWNO,////,////,Summer Beaver Airport,"Nibinamik, Ontario, Canada",OK
    SUS,SUS,KSUS,KSUS,OK,1.0,Spirit Of St Louis,"St Louis (MO), USA",OWNO,SPIRIT OF ST LOUIS,"ST LOUIS, MO - UNITED STATES",Spirit of St. Louis Airport,"St. Louis, Missouri, United States",OK
    SUT,////,////,HTSU,UNK,1.0,Sumbawanga Airport,"Sumbawanga, Tanzania",OWNO,////,////,Sumbawanga Airport,"Sumbawanga, Rukwa, Tanzania",OK
    SUU,SUU,KSUU,KSUU,OK,1.0,Travis AFB,"Fairfield (CA), USA",OWNO,TRAVIS AFB,"FAIRFIELD, CA - UNITED STATES",Travis AFB,"Fairfield, California, United States",OK
    SUV,////,////,NFNA,UNK,0.828,Nausori,"Suva, Fiji",OWNO,////,////,Nausori International Airport,"Suva, Fiji",OK
    SUW,SUW,KSUW,KSUW,OK,1.0,Richard I Bong Airport,"Superior (WI), USA",OWNO,RICHARD I BONG,"SUPERIOR, WI - UNITED STATES",Richard I Bong Airport,"Superior, Wisconsin, United States",OK
    SUX,SUX,KSUX,KSUX,OK,1.0,Sioux Gateway,"Sioux City IA, USA",OWNO,SIOUX GATEWAY/COL BUD DAY FIELD,"SIOUX CITY, IA - UNITED STATES",Sioux Gateway/Col Bud Day Field,"Sioux City, Iowa, United States",OK
    SUY,////,////,UENS,UNK,1.0,Suntar,Suntar,IATA,////,////,Suntar Airport,"Suntar, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    SUZ,////,////,////,UNK,1.0,Suria,"Suria, Papua New Guinea",OWNO,////,////,????,"Suria, Central, Papua-New Guinea",OK
    SVA,SVA,PASA,PASA,OK,1.0,Savoonga,"Savoonga (AK), USA",OWNO,SAVOONGA,"SAVOONGA, AK - UNITED STATES",????,"Savoonga, St. Lawrence Island, Alaska, United States",OK
    SVB,////,////,FMNS,UNK,0.778,Sambava,"Sambava, Madagascar",OWNO,////,////,Sud Airport,"Sambava, Madagascar",OK
    SVC,SVC,KSVC,KSVC,OK,1.0,Grant County,"Silver City (NM), USA",OWNO,GRANT COUNTY,"SILVER CITY, NM - UNITED STATES",Grant County Airport,"Silver City, New Mexico, United States",OK
    SVD,////,////,TVSV,UNK,1.0,E.T. Joshua,"St Vincent, Saint Vincent and the Grenadines",OWNO,////,////,E.T. Joshua,"Kingstown, St. Vincent, Saint Vincent and The Grenadines",OK
    SVE,SVE,KSVE,KSVE,OK,0.801,Susanville,"Susanville (CA), USA",OWNO,SUSANVILLE MUNI,"SUSANVILLE, CA - UNITED STATES",Susanville Municipal Airport,"Susanville, California, United States",OK
    SVF,////,////,DBBS,UNK,1.0,Save,"Save, Benin",OWNO,////,////,Savé Airport,"Savé, Collines, Benin",OK
    SVG,////,////,ENZV,UNK,1.0,Sola,"Stavanger, Norway",OWNO,////,////,Sola,"Stavanger, Norway",OK
    SVH,SVH,KSVH,KSVH,OK,0.716,Municipal,"Statesville (NC), USA",OWNO,STATESVILLE RGNL,"STATESVILLE, NC - UNITED STATES",Statesville Regional,"Statesville, North Carolina, United States",OK
    SVI,////,////,SKSV,UNK,0.429,San Vicente,"San Vicente, Colombia",OWNO,////,////,Eduardo Falla Solano Airport,"San Vicente del Caguán, Caquetá, Colombia",MAYBE
    SVJ,////,////,ENSH,UNK,1.0,Helle,"Svolvaer, Norway",OWNO,////,////,Helle,"Svolvær, Nordland, Norway",OK
    SVK,////,////,////,UNK,1.0,Silver Creek,"Silver Creek, Belize",OWNO,////,////,Silver Creek Airport,"Silver Creek, Stann Creek, Belize",OK
    SVL,////,////,EFSA,UNK,1.0,Savonlinna,"Savonlinna, Finland",OWNO,////,////,????,"Savonlinna, Etelä-Savo (Södra Savolax (Southern Savonia)), Finland",OK
    SVM,////,////,////,UNK,1.0,St Paul's Mission,"St Paul's Mission, Australia",OWNO,////,////,????,"St. Paul's Mission, Queensland, Australia",OK
    SVN,SVN,KSVN,KSVN,OK,1.0,Hunter AAF,"Savannah (GA), USA",OWNO,HUNTER AAF,"SAVANNAH, GA - UNITED STATES",Hunter AAF Airport,"Savannah, Georgia, United States",OK
    SVO,////,////,UUEE,UNK,0.884,Sheremetyevo,"Moscow, Russia",OWNO,////,////,Sheremetyevo International Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",OK
    SVP,////,////,FNKU,UNK,0.455,Kuito,"Kuito, Angola",OWNO,////,////,Silva Porto,"Kuito, Bié, Angola",OK
    SVQ,////,////,LEZL,UNK,0.583,Sevilla,"Sevilla, Spain",OWNO,////,////,San Pablo,"Sevilla, Andalusia, Spain",OK
    SVS,SVS,////,////,OK,1.0,Stevens Village,"Stevens Village (AK), USA",OWNO,STEVENS VILLAGE,"STEVENS VILLAGE, AK - UNITED STATES",????,"Stevens Village, Alaska, United States",OK
    SVT,////,////,FBSV,UNK,1.0,Savuti,"Savuti, Botswana",OWNO,////,////,Savuti Airport,"Savuti, North-West, Botswana",OK
    SVU,////,////,NFNS,UNK,1.0,Savusavu,"Savusavu, Fiji",OWNO,////,////,????,"Savusavu, Vanua Levu, Fiji",OK
    SVW,SVW,PASV,PASV,OK,0.719,Sparrevohn AFS,"Sparrevohn (AK), USA",OWNO,SPARREVOHN LRRS,"SPARREVOHN, AK - UNITED STATES",Sparrevohn LRRS Airport,"Sparrevohn, Alaska, United States",OK
    SVX,////,////,USSS,UNK,0.774,Ekaterinburg,"Ekaterinburg, Russia",OWNO,////,////,Koltsovo Airport,"Ekaterinburg, Sverdlovskaya, Russian Federation (Russia)",OK
    SVZ,////,////,SVSA,UNK,0.444,San Antonio,"San Antonio, Venezuela",OWNO,////,////,Juan Vicente Gómez International Airport,"San Antonio del Táchira, Táchira, Venezuela",MAYBE
    SWA,////,////,ZGOW,UNK,0.759,Shantou,"Shantou, PR China",OWNO,////,////,Chaoshan International Airport,"Jieyang, Guangdong, China",OK
    SWD,SWD,PAWD,PAWD,OK,1.0,Seward,"Seward (AK), USA",OWNO,SEWARD,"SEWARD, AK - UNITED STATES",????,"Seward, Alaska, United States",OK
    SWF,SWF,KSWF,KSWF,OK,0.748,Stewart,"Poughkeepsie (NY), USA",OWNO,STEWART INTL,"NEWBURGH, NY - UNITED STATES",Stewart International Airport,"Newburgh, New York, United States",OK
    SWJ,////,////,NVSX,UNK,1.0,South West Bay,"South West Bay, Vanuatu",OWNO,////,////,????,"Southwest Bay, Malekula Island, Malampa, Vanuatu",OK
    SWO,SWO,KSWO,KSWO,OK,0.563,Searcy Fld,"Stillwater (OK), USA",OWNO,STILLWATER RGNL,"STILLWATER, OK - UNITED STATES",Stillwater Regional,"Stillwater, Oklahoma, United States",OK
    SWP,////,////,FYSM,UNK,1.0,Swakopmund,"Swakopmund, Namibia",OWNO,////,////,????,"Swakopmund, Namibia",OK
    SWQ,////,////,WADS,UNK,0.488,Brang Bidji,"Sumbawa, Indonesia",OWNO,////,////,Sultan Muhammad Kaharuddin III Airport,"Sumbawa Besar, Sumbawa Island, Nusa Tenggara Barat, Indonesia",MAYBE
    SWS,////,////,EGFH,UNK,0.714,Fairwood Comm,"Swansea, United Kingdom",OWNO,////,////,????,"Swansea, Glamorgan, Wales, United Kingdom",OK
    SWT,////,////,UNSS,UNK,0.737,Strzhewoi,"Strzhewoi, Russia",OWNO,////,////,Strezhevoy Airport,"Strezhevoy, Tomskaya, Russian Federation (Russia)",TO DO CHECK
    SWU,////,////,RKSW,UNK,1.0,Su Won Airport,"Su Won City, South Korea",OWNO,////,////,????,"Su Won City, Republic of Korea (South Korea)",OK
    SWW,SWW,KSWW,KSWW,OK,0.484,Sweetwater,"Sweetwater (TX), USA",OWNO,AVENGER FIELD,"SWEETWATER, TX - UNITED STATES",Avenger Field,"Sweetwater, Texas, United States",OK
    SWX,////,////,FBSW,UNK,1.0,Shakawe,"Shakawe, Botswana",OWNO,////,////,Shakawe Airport,"Shakawe, North-West, Botswana",OK
    SWY,////,////,WMBA,UNK,1.0,Sitiawan,"Sitiawan, Malaysia",OWNO,////,////,????,"Sitiawan, Perak, Malaysia",OK
    SXB,////,////,LFST,UNK,0.973,Entzheim,"Strasbourg, France",OWNO,////,////,Enzheim,"Strasbourg, Alsace, France",OK
    SXF,////,////,EDDB,UNK,1.0,Schoenefeld,"Berlin, Germany",OWNO,////,////,Berlin Schönefeld Airport,"Berlin, Brandenburg, Germany",OK
    SXI,////,////,OIBS,UNK,1.0,Sirri Island,"Sirri Island, Iran",OWNO,////,////,????,"Sirri Island, Hormozgan, Iran",OK
    SXL,////,////,EISG,UNK,0.741,Collooney,"Sligo, Ireland",OWNO,////,////,????,"Sligo, County Sligo, Connacht, Ireland",OK
    SXM,////,TNCM,TNCM,OK,0.748,Princ. Juliana,"St Maarten, Netherlands Antilles",OWNO,PRINCESS JULIANA INTL,"PHILIPSBURG, - NETHERLANDS ANTILLES",Princess Juliana International Airport,"Philipsburg, St. Maarten, Netherlands Antilles",OK
    SXO,////,////,SWFX,UNK,1.0,Sao Felix Do Araguaia,"Sao Felix Do Araguaia, Brazil",OWNO,////,////,????,"São Felix do Araguaia, Mato Grosso, Brazil",OK
    SXP,SXP,////,////,OK,0.835,Sheldon SPB,"Sheldon Point (AK), USA",OWNO,SHELDON POINT,"SHELDON POINT, AK - UNITED STATES",????,"Sheldon Point, Alaska, United States",OK
    SXQ,SXQ,PASX,PASX,OK,1.0,Soldotna,"Soldotna (AK), USA",OWNO,SOLDOTNA,"SOLDOTNA, AK - UNITED STATES",????,"Soldotna, Alaska, United States",OK
    SXR,////,////,VISR,UNK,1.0,Srinagar,"Srinagar, India",OWNO,////,////,????,"Srinagar, Jammu and Kashmir, India",OK
    SXV,////,////,VOSM,UNK,1.0,Salem,"Salem, India",OWNO,////,////,????,"Salem, Tamil Nadu, India",OK
    SXX,////,////,SNFX,UNK,1.0,Sao Felix Do Xingu,"Sao Felix Do Xingu, Brazil",OWNO,////,////,????,"São Felix de Xingú, Pará, Brazil",OK
    SXZ,////,////,LTCL,UNK,1.0,Siirt,"Siirt, Turkey",OWNO,////,////,????,"Siirt, Siirt, Turkey",OK
    SYA,SYA,PASY,PASY,OK,0.352,Shemya AFB,"Shemya (AK), USA",OWNO,EARECKSON AS,"SHEMYA, AK - UNITED STATES",Eareckson Air Station,"Shemya, Alaska, United States",OK
    SYB,////,////,////,UNK,0.882,Seal Bay,"Seal Bay (AK), USA",OWNO,////,////,Seal Bay SPB,"Seal Bay, Alaska, United States",OK
    SYD,////,////,YSSY,UNK,1.0,Kingsford Smith,"Sydney, Australia",OWNO,////,////,Kingsford Smith,"Sydney, New South Wales, Australia",OK
    SYH,////,////,VNSB,UNK,1.0,Syangboche,"Syangboche, Nepal",OWNO,////,////,????,"Syangboche, Nepal",OK
    SYI,SYI,KSYI,KSYI,OK,0.865,Bomar Field,"Shelbyville (TN), USA",OWNO,BOMAR FIELD-SHELBYVILLE MUNI,"SHELBYVILLE, TN - UNITED STATES",Bomar Field-Shelbyville Municipal Airport,"Shelbyville, Tennessee, United States",OK
    SYJ,////,////,OIKY,UNK,1.0,Sirjan,"Sirjan, Iran",OWNO,////,////,????,"Sirjan, Kerman, Iran",OK
    SYM,////,////,ZPSM,UNK,1.0,Simao,"Simao, PR China",OWNO,////,////,Pu'er Simao Airport,"Simao, Pu'er City, Yunnan, China",OK
    SYN,SYN,KSYN,KSYN,OK,0.701,Carleton,"Stanton (MN), USA",OWNO,STANTON AIRFIELD,"STANTON, MN - UNITED STATES",Stanton Airfield,"Stanton, Minnesota, United States",OK
    SYO,////,////,RJSY,UNK,1.0,Shonai,"Shonai, Japan",OWNO,////,////,Shonai Airport,"Shonai, Yamagata, Japan",OK
    SYP,////,////,MPSA,UNK,0.5,Santiago,"Santiago, Panama",OWNO,////,////,Ruben Cantu Airport,"Santiago de Veraguas, Veraguas, Panamá",MAYBE
    SYQ,////,////,MRPV,UNK,1.0,Tobias Bolanos International,"San Jose, Costa Rica",OWNO,////,////,Tobías Bolaños International Airport,"San José, San José, Costa Rica",OK
    SYR,SYR,KSYR,KSYR,OK,1.0,Hancock International,"Syracuse (NY), USA",OWNO,SYRACUSE HANCOCK INTL,"SYRACUSE, NY - UNITED STATES",Syracuse Hancock International Airport,"Syracuse, New York, United States",OK
    SYS,////,////,UERS,UNK,1.0,Saskylakh,Saskylakh,IATA,////,////,????,"Saskylakh, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    SYT,////,////,LFLN,UNK,1.0,Charolais Bourgogne S.,"Saint Yan, France",OWNO,////,////,Charolais Bourgogne S.,"St-Yan, Bourgogne (Burgundy), France",OK
    SYU,////,////,YWBS,UNK,1.0,Warraber Island,"Sue Island, Australia",OWNO,////,////,????,"Warraber Island, Queensland, Australia",OK
    SYX,////,////,ZJSY,UNK,0.636,Sanya,"Sanya, PR China",OWNO,////,////,Fenghuang International Airport,"Sanya, Hainan, China",OK
    SYY,////,////,EGPO,UNK,1.0,Stornoway,"Stornoway, United Kingdom",OWNO,////,////,????,"Stornoway, Hebrides, Scotland, United Kingdom",OK
    SYZ,////,////,OISS,UNK,0.706,Shiraz,"Shiraz, Iran",OWNO,////,////,Shiraz International Airport,"Shiraz, Fars, Iran",OK
    SZA,////,////,FNSO,UNK,1.0,Soyo,"Soyo, Angola",OWNO,////,////,????,"Soyo, Angola",OK
    SZB,////,////,WMSA,UNK,1.0,Sultan Abdul Aziz Shah,"Kuala Lumpur, Malaysia",OWNO,////,////,Sultan Abdul Aziz Shah Airport,"Kuala Lumpur, Selangor, Malaysia",OK
    SZD,////,////,EGSY,UNK,1.0,Sheffield City Airport,"Sheffield, United Kingdom",OWNO,////,////,Sheffield City,"Sheffield, Yorkshire, England, United Kingdom",OK
    SZF,////,////,LTFH,UNK,1.0,Carsamba,"Samsun, Turkey",OWNO,////,////,Carsamba,"Samsun, Samsun, Turkey",OK
    SZG,////,////,LOWS,UNK,1.0,W.A. Mozart,"Salzburg, Austria",OWNO,////,////,Salzburg W.A. Mozart,"Salzburg, Salzburg, Austria",OK
    SZJ,////,////,MUSN,UNK,1.0,Siguanea,"Siguanea, Cuba",OWNO,////,////,Siguanea Airport,"Nueva Gerona, Isla de la Juventud, Cuba",OK
    SZK,////,////,FASZ,UNK,1.0,Skukuza,"Skukuza, South Africa",OWNO,////,////,Skukuza Airport,"Skukuza, Mpumalanga, South Africa",OK
    SZL,SZL,KSZL,KSZL,OK,1.0,Whiteman AFB,"Warrensburg (MO), USA",OWNO,WHITEMAN AFB,"KNOB NOSTER, MO - UNITED STATES",Whiteman AFB,"Knob Noster, Missouri, United States",OK
    SZR,////,////,LBSZ,UNK,1.0,Stara Zagora,"Stara Zagora, Bulgaria",OWNO,////,////,????,"Stara Zagora, Haskovo, Bulgaria",OK
    SZS,////,////,NZRC,UNK,1.0,Stewart Island,"Stewart Island, New Zealand",OWNO,////,////,????,"Ryan's Creek, Stewart Island, New Zealand",OK
    SZU,////,////,GASG,UNK,1.0,Segou,"Segou, Mali",OWNO,////,////,????,"Segou, Ségou, Mali",OK
    SZW,////,////,EDOP,UNK,1.0,Parchim Airport,"Schwerin, Germany",OWNO,////,////,Parchim Airport,"Schwerin, Mecklenburg-Vorpommern, Germany",OK
    SZX,////,////,ZGSZ,UNK,0.8,Shenzhen,"Shenzhen, PR China",OWNO,////,////,Shenzhen Bao'an International Airport,"Shenzhen, Guangdong, China",OK
    SZY,////,////,EPSY,UNK,0.621,Mazury,"Szymany, Poland",OWNO,////,////,Szczytno-Szymany International Airport,"Szczytno, Warinsko-mazurskie, Poland",TO DO CHECK
    SZZ,////,////,EPSC,UNK,1.0,Goleniow,"Szczecin, Poland",OWNO,////,////,Goleniów,"Szczecin, Zachodniopomorskie, Poland",OK


List for checking certainty of Great Circle Mapper (WMR - WYS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    WMR,////,////,FMNC,UNK,0.64,Mananara,"Mananara, Madagascar",OWNO,////,////,Avaratra Airport,"Mananara, Madagascar",OK
    WMV,////,////,////,UNK,1.0,Madirovalo,"Madirovalo, Madagascar",OWNO,////,////,????,"Madirovalo, Madagascar",OK
    WMX,////,////,WAJW,UNK,1.0,Wamena,"Wamena, Indonesia",OWNO,////,////,Wamena Airport,"Wamena, Papua, Indonesia",OK
    WNA,WNA,PANA,PANA,OK,0.805,Napakiak SPB,"Napakiak (AK), USA",OWNO,NAPAKIAK,"NAPAKIAK, AK - UNITED STATES",????,"Napakiak, Alaska, United States",OK
    WND,////,////,YWDA,UNK,1.0,Windarra,"Windarra, Australia",OWNO,////,////,????,"Windarra, Western Australia, Australia",OK
    WNE,////,////,////,UNK,1.0,Wora Na Ye,"Wora Na Ye, Gabon",OWNO,////,////,????,"Wora Na Ye, Ogooué-Maritime, Gabon",OK
    WNH,////,////,ZPWS,UNK,0.667,Puzhehei,Wenshan,IATA,////,////,Wenshan Puzhehei Airport,"Wenshan, Yunnan, China",MAYBE
    WNN,////,////,////,UNK,1.0,Wunnummin Lake,"Wunnummin Lake, Canada",OWNO,////,////,????,"Wunnumin Lake, Ontario, Canada",OK
    WNP,////,////,RPUN,UNK,1.0,Naga,"Naga, Philippines",OWNO,////,////,????,"Naga, Philippines",OK
    WNR,////,////,YWDH,UNK,0.762,Windorah,"Windorah, Australia",OWNO,////,////,????,"Windorah Park, Queensland, Australia",MAYBE
    WNS,////,////,OPNH,UNK,1.0,Nawabshah,"Nawabshah, Pakistan",OWNO,////,////,????,"Nawab Shah, Sindh, Pakistan",OK
    WNU,////,////,////,UNK,1.0,Wanuma,"Wanuma, Papua New Guinea",OWNO,////,////,????,"Wanuma, Madang, Papua-New Guinea",OK
    WNZ,////,////,ZSWZ,UNK,0.865,Wenzhou,"Wenzhou, PR China",OWNO,////,////,Wenzhou International Airport,"Wenzhou, Zhejiang, China",OK
    WOA,////,////,////,UNK,1.0,Wonenara,"Wonenara, Papua New Guinea",OWNO,////,////,????,"Wonenara, Eastern Highlands, Papua-New Guinea",OK
    WOK,////,////,////,UNK,1.0,Wonken,"Wonken, Venezuela",OWNO,////,////,????,"Wonken, Bolívar, Venezuela",OK
    WOL,////,////,YWOL,UNK,0.667,Wollongong,"Wollongong, Australia",OWNO,////,////,Illawarra Regional Airport,"Wollongong, New South Wales, Australia",OK
    WON,////,////,YWDL,UNK,1.0,Wondoola,"Wondoola, Australia",OWNO,////,////,????,"Wondoola, Queensland, Australia",OK
    WOS,////,////,ZKWS,UNK,nan,////,////,////,////,////,Wonsan Kalma International Airport,"Wonsan, Kangwon, Democratic People's Republic of Korea (North Korea)",UNK
    WOT,////,////,RCWA,UNK,0.545,Wonan,"Wonan, Taiwan",OWNO,////,////,????,"Wang-an, Hope Island, Pescadores Islands, Taiwan",TO DO CHECK
    WOW,UUO,PAUO,PAUO,OK,1.0,Willow,"Willow (AK), USA",OWNO,WILLOW,"WILLOW, AK - UNITED STATES",????,"Willow, Alaska, United States",OK
    WPA,////,////,SCAS,UNK,0.5,Puerto Aisen,"Puerto Aisen, Chile",OWNO,////,////,Cabo 1° Juan Román Airport,"Puerto Aysen, Aysén del General Carlos Ibáñez del Campo, Chile",OK
    WPB,////,////,FMNG,UNK,1.0,Port Berge,"Port Berge, Madagascar",OWNO,////,////,Port Bergé Airport,"Port Bergé, Madagascar",OK
    WPC,////,CZPC,CZPC,OK,1.0,Pincher Creek,"Pincher Creek, Canada",OWNO,PINCHER CREEK,"PINCHER CREEK, - CANADA",????,"Pincher Creek, Alberta, Canada",OK
    WPK,////,////,YWMP,UNK,1.0,Wrotham Park,"Wrotham Park, Australia",OWNO,////,////,????,"Wrotham Park, Queensland, Australia",OK
    WPL,////,////,////,UNK,0.786,Powell Lake,"Powell Lake, Canada",OWNO,////,////,Powell Lake Water Aerodrome,"Powell River, British Columbia, Canada",MAYBE
    WPM,////,////,////,UNK,1.0,Wipim,"Wipim, Papua New Guinea",OWNO,////,////,????,"Wipim, Western, Papua-New Guinea",OK
    WPO,7V2,////,////,OK,1.0,North Fork Valley,"Paonia (CO), USA",OWNO,NORTH FORK VALLEY,"PAONIA, CO - UNITED STATES",North Fork Valley Airport,"Paonia, Colorado, United States",OK
    WPR,////,////,SCFM,UNK,0.444,Porvenir,"Porvenir, Chile",OWNO,////,////,Capitán Fuentes Martinez Airport,"Porvenir, Isla Tierra del Fuego, Magallanes y de la Antártica Chilena, Chile",OK
    WPU,////,////,SCGZ,UNK,0.577,Puerto Williams,"Puerto Williams, Chile",OWNO,////,////,Guardiamarina Zañartu Airport,"Puerto Williams, Magallanes y de la Antártica Chilena, Chile",OK
    WPW,////,////,HKKE,UNK,nan,////,////,////,////,////,????,"Keekorok, Kenya",UNK
    WRA,////,////,////,UNK,1.0,Warder,"Warder, Ethiopia",OWNO,////,////,????,"Warder, Somali, Ethiopia",OK
    WRB,WRB,KWRB,KWRB,OK,1.0,Robins AFB,"Macon (GA), USA",OWNO,ROBINS AFB,"WARNER ROBINS, GA - UNITED STATES",Robins AFB,"Warner Robins, Georgia, United States",OK
    WRE,////,////,NZWR,UNK,1.0,Whangarei,"Whangarei, New Zealand",OWNO,////,////,????,"Whangarei, New Zealand",OK
    WRG,WRG,PAWG,PAWG,OK,0.79,Wrangell SPB,"Wrangell (AK), USA",OWNO,WRANGELL,"WRANGELL, AK - UNITED STATES",????,"Wrangell, Alaska, United States",OK
    WRI,WRI,KWRI,KWRI,OK,0.766,Mc Guire AFB,"Fort Dix (NJ), USA",OWNO,MC GUIRE FLD (JOINT BASE MC GUIRE DIX LAKEHURST),"WRIGHTSTOWN, NJ - UNITED STATES",Mc Guire Field,"Wrightstown, New Jersey, United States",OK
    WRL,WRL,KWRL,KWRL,OK,0.734,Worland,"Worland (WY), USA",OWNO,WORLAND MUNI,"WORLAND, WY - UNITED STATES",Worland Municipal Airport,"Worland, Wyoming, United States",OK
    WRN,////,////,YWDG,UNK,nan,////,////,////,////,////,Windarling Airport,"Windarling Mine, Western Australia, Australia",UNK
    WRO,////,////,EPWR,UNK,0.629,Strachowice,"Wroclaw, Poland",OWNO,////,////,Nicolaus Copernicus Airport Wroclaw,"Wroclaw, Dolnoslaskie, Poland",OK
    WRT,////,////,EGNO,UNK,nan,////,////,////,////,////,Warton Aerodrome,"Warton, Lancashire, England, United Kingdom",UNK
    WRW,////,////,YWWG,UNK,0.733,Warrawagine,"Warrawagine, Australia",OWNO,////,////,????,"Warrawagine Station, Western Australia, Australia",MAYBE
    WRY,////,////,EGEW,UNK,1.0,Westray,"Westray, United Kingdom",OWNO,////,////,Westray Airport,"Westray, Orkney Isles, Scotland, United Kingdom",OK
    WRZ,////,////,VCCW,UNK,nan,////,////,////,////,////,Weerawila International Airport,"Weerawila, Southern Province, Sri Lanka (Ceylon)",UNK
    WSA,////,////,////,UNK,1.0,Wasua,"Wasua, Papua New Guinea",OWNO,////,////,????,"Wasua, Western, Papua-New Guinea",OK
    WSB,WSB,////,////,OK,0.826,SPB,"Steamboat Bay (AK), USA",OWNO,STEAMBOAT BAY,"STEAMBOAT BAY, AK - UNITED STATES",Steamboat Bay SPB,"Steamboat Bay, Alaska, United States",OK
    WSD,WSD,KWSD,KWSD,OK,1.0,Condron AAF,"White Sands (NM), USA",OWNO,CONDRON AAF,"WHITE SANDS, NM - UNITED STATES",Condron AAF Airport,"White Sands, New Mexico, United States",OK
    WSG,AFJ,KAFJ,KAFJ,OK,1.0,County,"Washington (PA), USA",OWNO,WASHINGTON COUNTY,"WASHINGTON, PA - UNITED STATES",Washington County Airport,"Washington, Pennsylvania, United States",OK
    WSH,HWV,KHWV,KHWV,OK,1.0,Brookhaven,"Shirley (NY), USA",OWNO,BROOKHAVEN,"SHIRLEY, NY - UNITED STATES",Brookhaven Airport,"Shirley, New York, United States",OK
    WSM,WSM,////,////,OK,1.0,Wiseman,"Wiseman (AK), USA",OWNO,WISEMAN,"WISEMAN, AK - UNITED STATES",????,"Wiseman, Alaska, United States",OK
    WSN,WSN,PFWS,PFWS,OK,0.674,South Naknek,"South Naknek (AK), USA",OWNO,SOUTH NAKNEK NR 2,"SOUTH NAKNEK, AK - UNITED STATES",South Naknek Number 2 Airport,"South Naknek, Alaska, United States",OK
    WSO,////,////,SMWS,UNK,0.609,Washabo,"Washabo, Suriname",OWNO,////,////,Washabo Airstrip,"Washabo, Sipaliwini, Suriname",OK
    WSP,////,////,MNWP,UNK,1.0,Waspam,"Waspam, Nicaragua",OWNO,////,////,????,"Waspam, Atlántico Norte, Nicaragua",OK
    WSR,////,////,WASW,UNK,1.0,Wasior,"Wasior, Indonesia",OWNO,////,////,Wasior Airport,"Wasior, Papua Barat, Indonesia",OK
    WST,WST,KWST,KWST,OK,1.0,Westerly State,"Westerly (RI), USA",OWNO,WESTERLY STATE,"WESTERLY, - UNITED STATES",Westerly State Airport,"Westerly, Rhode Island, United States",OK
    WSU,////,////,////,UNK,1.0,Wasu,"Wasu, Papua New Guinea",OWNO,////,////,????,"Wasu, Morobe, Papua-New Guinea",OK
    WSX,WA83,////,////,OK,1.0,Westsound,"Westsound (WA), USA",OWNO,WESTSOUND/WSX,"WEST SOUND, WA - UNITED STATES",Westsound/Wsx SPB,"West Sound, Washington, United States",OK
    WSY,////,////,YWHI,UNK,1.0,Whitsunday Airstrip,"Airlie Beach, Australia",OWNO,////,////,Whitsunday Airstrip,"Airlie Beach, Queensland, Australia",OK
    WSZ,////,////,NZWS,UNK,1.0,Westport,"Westport, New Zealand",OWNO,////,////,????,"Westport, New Zealand",OK
    WTA,////,////,FMMU,UNK,1.0,Tambohorano,"Tambohorano, Madagascar",OWNO,////,////,????,"Tambohorano, Madagascar",OK
    WTB,////,////,YBWW,UNK,nan,////,////,////,////,////,Brisbane West Wellcamp Airport,"Toowoomba, Queensland, Australia",UNK
    WTD,////,////,MYGW,UNK,1.0,West End,"West End, Bahamas",OWNO,////,////,????,"West End, Grand Bahama, Bahamas",OK
    WTE,N36,////,////,OK,0.371,Wotje Island,"Wotje Island, Marshall Islands",OWNO,WOTJE,"WOTJE ATOLL, - MARSHALL ISLANDS",????,"Wotje Atoll, Marshall Islands",MAYBE
    WTK,WTK,PAWN,PAWN,OK,1.0,Noatak,"Noatak (AK), USA",OWNO,NOATAK,"NOATAK, AK - UNITED STATES",????,"Noatak, Alaska, United States",OK
    WTL,A61,////,////,OK,1.0,Tuntutuliak,"Tuntutuliak (AK), USA",OWNO,TUNTUTULIAK,"TUNTUTULIAK, AK - UNITED STATES",????,"Tuntutuliak, Alaska, United States",OK
    WTN,////,////,EGXW,UNK,0.8,RAF Station,"Waddington, United Kingdom",OWNO,////,////,RAF Waddington,"Waddington, Lincolnshire, England, United Kingdom",OK
    WTO,////,////,////,UNK,1.0,Wotho Island,"Wotho Island, Marshall Islands",OWNO,////,////,????,"Wotho Island, Marshall Islands",OK
    WTP,////,////,////,UNK,1.0,Woitape,"Woitape, Papua New Guinea",OWNO,////,////,????,"Woitape, Central, Papua-New Guinea",OK
    WTR,E24,////,////,OK,0.686,White River,"White River (AZ), USA",OWNO,WHITERIVER,"WHITERIVER, AZ - UNITED STATES",????,"Whiteriver, Arizona, United States",OK
    WTS,////,////,FMMX,UNK,1.0,Tsiroanomandidy,"Tsiroanomandidy, Madagascar",OWNO,////,////,????,"Tsiroanomandidy, Madagascar",OK
    WTT,////,////,////,UNK,1.0,Wantoat,"Wantoat, Papua New Guinea",OWNO,////,////,????,"Wantoat, Morobe, Papua-New Guinea",OK
    WTZ,////,////,NZWT,UNK,1.0,Whitianga,"Whitianga, New Zealand",OWNO,////,////,????,"Whitianga, New Zealand",OK
    WUA,////,////,ZBUH,UNK,nan,////,////,////,////,////,Wuhai Regional,"Wuhai, Inner Mongolia, China",UNK
    WUD,////,////,YWUD,UNK,1.0,Wudinna,"Wudinna, Australia",OWNO,////,////,????,"Wudinna, South Australia, Australia",OK
    WUG,////,////,////,UNK,1.0,Wau,"Wau, Papua New Guinea",OWNO,////,////,????,"Wau, Morobe, Papua-New Guinea",OK
    WUH,////,////,ZHHH,UNK,0.647,Wuhan,"Wuhan, PR China",OWNO,////,////,Wuhan Tianhe International Airport,"Wuhan, Hubei, China",OK
    WUI,////,////,YMMI,UNK,nan,////,////,////,////,////,Murrin Murrin Airport,"Murrin Murrin, Western Australia, Australia",UNK
    WUN,////,////,YWLU,UNK,1.0,Wiluna,"Wiluna, Australia",OWNO,////,////,????,"Wiluna, Western Australia, Australia",OK
    WUS,////,////,ZSWY,UNK,0.789,Wuyishan,"Wuyishan, PR China",OWNO,////,////,Nanping Wuyishan Airport,"Wuyishan, Fujian, China",OK
    WUT,////,////,ZBXZ,UNK,nan,////,////,////,////,////,Xinzhou Wutaishan Airport,"Xinzhou, Shanxi, China",UNK
    WUU,////,////,HSWW,UNK,1.0,Wau,"Wau, Sudan",OWNO,////,////,Wau Airport,"Wau, Western Bahr el Ghazal, South Sudan",OK
    WUV,////,////,////,UNK,1.0,Wuvulu Island,"Wuvulu Island, Papua New Guinea",OWNO,////,////,????,"Wuvulu Island, Manus, Papua-New Guinea",OK
    WUX,////,////,ZSWX,UNK,0.611,Wuxi,"Wuxi, PR China",OWNO,////,////,Sunan Shuofang International Airport,"Wuxi, Jiangsu, China",OK
    WUZ,////,////,ZGWZ,UNK,1.0,Wuzhou Changzhoudao Airport,"Wuzhou, PR China",OWNO,////,////,Changzhoudao,"Wuzhou, Guangxi, China",OK
    WVB,////,////,FYWB,UNK,1.0,Rooikop,"Walvis Bay, Namibia",OWNO,////,////,Rooikop,"Walvis Bay, Namibia",OK
    WVI,WVI,KWVI,KWVI,OK,0.806,Watsonville,"Watsonville (CA), USA",OWNO,WATSONVILLE MUNI,"WATSONVILLE, - UNITED STATES",Watsonville Municipal Airport,"Watsonville, California, United States",OK
    WVK,////,////,FMSK,UNK,1.0,Manakara,"Manakara, Madagascar",OWNO,////,////,????,"Manakara, Madagascar",OK
    WVL,WVL,KWVL,KWVL,OK,1.0,Robert Lafleur,"Waterville (ME), USA",OWNO,WATERVILLE ROBERT LAFLEUR,"WATERVILLE, ME - UNITED STATES",Waterville Robert Lafleur Airport,"Waterville, Maine, United States",OK
    WVN,////,////,EDWI,UNK,0.759,Wilhelmshaven,"Wilhelmshaven, Germany",OWNO,////,////,JadeWeser,"Wilhelmshaven, Lower Saxony, Germany",OK
    WWA,IYS,PAWS,PAWS,OK,1.0,Wasilla,"Wasilla (AK), USA",OWNO,WASILLA,"WASILLA, AK - UNITED STATES",????,"Wasilla, Alaska, United States",OK
    WWD,WWD,KWWD,KWWD,OK,1.0,Cape May County,"Wildwood (NJ), USA",OWNO,CAPE MAY COUNTY,"WILDWOOD, NJ - UNITED STATES",Cape May County Airport,"Wildwood, New Jersey, United States",OK
    WWI,////,////,YWWI,UNK,1.0,Woodie Woodie,"Woodie Woodie, Australia",OWNO,////,////,????,"Woodie Woodie, Western Australia, Australia",OK
    WWK,////,////,AYWK,UNK,0.625,Boram,"Wewak, Papua New Guinea",OWNO,////,////,????,"Wewak, East Sepik, Papua-New Guinea",OK
    WWP,96Z,////,////,OK,0.621,Whale Pass,"Whale Pass (AK), USA",OWNO,NORTH WHALE,"NORTH WHALE PASS, AK - UNITED STATES",North Whale SPB,"North Whale Pass, Alaska, United States",OK
    WWR,WWR,KWWR,KWWR,OK,1.0,West Woodward,"Woodward (OK), USA",OWNO,WEST WOODWARD,"WOODWARD, OK - UNITED STATES",West Woodward Airport,"Woodward, Oklahoma, United States",OK
    WWT,EWU,PAEW,PAEW,OK,1.0,Newtok,"Newtok (AK), USA",OWNO,NEWTOK,"NEWTOK, AK - UNITED STATES",????,"Newtok, Alaska, United States",OK
    WWY,////,////,YWWL,UNK,1.0,West Wyalong,"West Wyalong, Australia",OWNO,////,////,????,"West Wyalong, New South Wales, Australia",OK
    WXN,////,////,ZUWX,UNK,1.0,Wanxian,"Wanxian, PR China",OWNO,////,////,????,"Wanxian, Sichuan, China",OK
    WYA,////,////,YWHA,UNK,1.0,Whyalla,"Whyalla, Australia",OWNO,////,////,????,"Whyalla, South Australia, Australia",OK
    WYB,78K,////,////,OK,0.615,SPB,"Yes Bay (AK), USA",OWNO,YES BAY LODGE,"YES BAY, AK - UNITED STATES",Yes Bay Lodge SPB,"Yes Bay, Alaska, United States",OK
    WYE,////,////,GFYE,UNK,1.0,Yengema,"Yengema, Sierra Leone",OWNO,////,////,????,"Yengema, Sierra Leone",OK
    WYN,////,////,YWYM,UNK,1.0,Wyndham,"Wyndham, Australia",OWNO,////,////,????,"Wyndham, Western Australia, Australia",OK
    WYS,WYS,KWYS,KWYS,OK,1.0,Yellowstone,"West Yellowstone (MT), USA",OWNO,YELLOWSTONE,"WEST YELLOWSTONE, MT - UNITED STATES",Yellowstone Airport,"West Yellowstone, Montana, United States",OK

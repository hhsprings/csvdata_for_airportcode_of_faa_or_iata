
List for checking certainty of Great Circle Mapper (SAA - SFF)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    SAA,SAA,KSAA,KSAA,OK,0.819,Shively,"Saratoga (WY), USA",OWNO,SHIVELY FIELD,"SARATOGA, WY - UNITED STATES",Shively Field,"Saratoga, Wyoming, United States",OK
    SAB,////,////,TNCS,UNK,0.878,J. Yrausquin,"Saba Island, Netherlands Antilles",OWNO,////,////,Juancho E. Yrausquin,"The Bottom, Saba, Bonaire/Sint Eustatius/Saba",OK
    SAC,SAC,KSAC,KSAC,OK,1.0,Executive,"Sacramento (CA), USA",OWNO,SACRAMENTO EXECUTIVE,"SACRAMENTO, CA - UNITED STATES",Sacramento Executive Airport,"Sacramento, California, United States",OK
    SAD,SAD,KSAD,KSAD,OK,0.734,Safford,"Safford (AZ), USA",OWNO,SAFFORD RGNL,"SAFFORD, AZ - UNITED STATES",Safford Regional,"Safford, Arizona, United States",OK
    SAF,SAF,KSAF,KSAF,OK,0.581,Santa Fe,"Santa Fe (NM), USA",OWNO,SANTA FE MUNI,"SANTA FE, NM - UNITED STATES",Santa Fé Municipal Airport,"Santa Fé, New Mexico, United States",OK
    SAH,////,////,OYSN,UNK,1.0,Sana'a International,"Sana'a, Yemen",OWNO,////,////,Sana'a International Airport,"Sana'a, Yemen",OK
    SAK,////,////,BIKR,UNK,1.0,Saudarkrokur,"Saudarkrokur, Iceland",OWNO,////,////,????,"Sauðárkrókur, Iceland",OK
    SAL,////,////,MSLP,UNK,0.623,Comalapa International,"San Salvador, El Salvador",OWNO,////,////,Monseñor Óscar Arnulfo Romero International Airport,"San Salvador, La Paz, El Salvador",OK
    SAM,////,////,////,UNK,1.0,Salamo,"Salamo, Papua New Guinea",OWNO,////,////,????,"Salamo, Milne Bay, Papua-New Guinea",OK
    SAN,SAN,KSAN,KSAN,OK,0.551,Lindberg Field S.Diego,"San Diego (CA), USA",OWNO,SAN DIEGO INTL,"SAN DIEGO, CA - UNITED STATES",San Diego International Airport,"San Diego, California, United States",OK
    SAO,////,////,////,UNK,1.0,Metropolitan Area,"Sao Paulo, Brazil",OWNO,////,////,Metropolitan Area,"São Paulo, São Paulo, Brazil",OK
    SAP,////,////,MHLM,UNK,0.935,Ramon Villeda Morales,"San Pedro Sula, Honduras",OWNO,////,////,Ramón Villeda Morales International Airport,"San Pedro Sula, Cortés, Honduras",OK
    SAQ,////,MYAN,MYAN,OK,1.0,San Andros,"San Andros, Bahamas",OWNO,SAN ANDROS,"NICHOLLS TOWN, - BAHAMAS",San Andros,"Nicholl's Town, Andros Island, Bahamas",OK
    SAR,SAR,KSAR,KSAR,OK,0.76,Sparta Community,"Sparta (IL), USA",OWNO,SPARTA COMMUNITY-HUNTER FIELD,"SPARTA, IL - UNITED STATES",Sparta Community-Hunter Field,"Sparta, Illinois, United States",OK
    SAS,SAS,KSAS,KSAS,OK,0.949,Salton City,"Salton City (CA), USA",OWNO,SALTON SEA,"SALTON CITY, CA - UNITED STATES",Salton Sea Airport,"Salton City, California, United States",OK
    SAT,SAT,KSAT,KSAT,OK,1.0,San Antonio International,"San Antonio (TX), USA",OWNO,SAN ANTONIO INTL,"SAN ANTONIO, TX - UNITED STATES",San Antonio International Airport,"San Antonio, Texas, United States",OK
    SAU,////,////,WRKS,UNK,0.375,Sawu,"Sawu, Indonesia",OWNO,////,////,Tardamu,"Sabu, Nusa Tenggara Timur, Indonesia",TO DO CHECK
    SAV,SAV,KSAV,KSAV,OK,0.757,Savannah International,"Savannah (GA), USA",OWNO,SAVANNAH/HILTON HEAD INTL,"SAVANNAH, GA - UNITED STATES",Savannah/Hilton Head International Airport,"Savannah, Georgia, United States",OK
    SAW,////,////,LTFJ,UNK,1.0,Sabiha Gökçen International Airport,"Pendik, Istanbul, Turkey",OWNO,////,////,Sabiha Gökçen International Airport,"Istanbul, Istanbul, Turkey",OK
    SAX,////,////,////,UNK,1.0,Sambu,"Sambu, Panama",OWNO,////,////,????,"Sambú, Darién, Panamá",OK
    SAY,////,////,LIQS,UNK,0.5,Siena,"Siena, Italy",OWNO,////,////,Ampugnano,"Siena, Tuscany, Italy",OK
    SAZ,////,////,GLST,UNK,1.0,Sasstown,"Sasstown, Liberia",OWNO,////,////,????,"Sasstown, Liberia",OK
    SBA,SBA,KSBA,KSBA,OK,1.0,Municipal Airport,"Santa Barbara (CA), USA",OWNO,SANTA BARBARA MUNI,"SANTA BARBARA, CA - UNITED STATES",Santa Barbara Municipal Airport,"Santa Barbara, California, United States",OK
    SBB,////,////,SVSB,UNK,0.667,Santa Barbara Ba,"Santa Barbara Ba, Venezuela",OWNO,////,////,????,"Santa Bárbara de Barinas, Barinas, Venezuela",MAYBE
    SBC,////,////,////,UNK,1.0,Selbang,"Selbang, Papua New Guinea",OWNO,////,////,????,"Selbang, Western, Papua-New Guinea",OK
    SBD,SBD,KSBD,KSBD,OK,0.762,Norton AFB,"San Bernardino (CA), USA",OWNO,SAN BERNARDINO INTL,"SAN BERNARDINO, CA - UNITED STATES",San Bernardino International Airport,"San Bernardino, California, United States",OK
    SBE,////,////,////,UNK,1.0,Suabi,"Suabi, Papua New Guinea",OWNO,////,////,????,"Suabi, Western, Papua-New Guinea",OK
    SBF,////,////,OADS,UNK,0.842,Sardeh Band,"Sardeh Band, Afghanistan",OWNO,////,////,Band e Sardeh Dam,"Sardeh Band, Ghazni, Afghanistan",OK
    SBG,////,////,WITB,UNK,0.6,Sabang,"Sabang, Indonesia",OWNO,////,////,Maimun Saleh,"Sabang, Aceh, Indonesia",OK
    SBH,////,////,TFFJ,UNK,0.759,St Barthelemy,"St Barthelemy, Guadeloupe",OWNO,////,////,Gustaf III Airport,"Gustavia, St-Barthélemy, Guadeloupe",OK
    SBI,////,////,GUSB,UNK,1.0,Sambailo,"Koundara, Guinea",OWNO,////,////,Sambailo,"Koundara, Guinea",OK
    SBJ,////,////,SNMX,UNK,1.0,Sao Mateus,"Sao Mateus, Brazil",OWNO,////,////,????,"São Mateus, Espírito Santo, Brazil",OK
    SBK,////,////,LFRT,UNK,0.545,Tremuson,"St Brieuc, France",OWNO,////,////,Armor,"St-Brieuc, Bretagne, France",OK
    SBL,////,////,SLSA,UNK,0.889,Yacuma,"Santa Ana, Bolivia",OWNO,////,////,Santa Ana del Yacuma Yacuma Airport,"Santa Ana del Yacuma, Yacuma, El Beni, Bolivia",OK
    SBM,SBM,KSBM,KSBM,OK,0.79,Memorial,"Sheboygan (WI), USA",OWNO,SHEBOYGAN COUNTY MEMORIAL,"SHEBOYGAN, WI - UNITED STATES",Sheboygan County Memorial Airport,"Sheboygan, Wisconsin, United States",OK
    SBN,SBN,KSBN,KSBN,OK,0.835,South Bend Regional,"South Bend (IN), USA",OWNO,SOUTH BEND INTL,"SOUTH BEND, IN - UNITED STATES",South Bend International Airport,"South Bend, Indiana, United States",OK
    SBO,44U,////,////,OK,0.504,Salina,"Salina (UT), USA",OWNO,SALINA-GUNNISON,"SALINA, UT - UNITED STATES",Salina-Gunnison Airport,"Salina, Utah, United States",OK
    SBP,SBP,KSBP,KSBP,OK,0.865,County Airport,"San Luis Obispo (CA), USA",OWNO,SAN LUIS COUNTY RGNL,"SAN LUIS OBISPO, CA - UNITED STATES",San Luis County Regional,"San Luis Obispo, California, United States",OK
    SBQ,////,////,OPSB,UNK,1.0,Sibi,"Sibi, Pakistan",OWNO,////,////,????,"Sibi, Balochistan, Pakistan",OK
    SBR,////,////,YSII,UNK,1.0,Saibai Island,"Saibai Island, Australia",OWNO,////,////,????,"Saibai Island, Torres Strait Islands, Queensland, Australia",OK
    SBS,SBS,KSBS,KSBS,OK,1.0,Steamboat Springs,"Steamboat Springs (CO), USA",OWNO,STEAMBOAT SPRINGS/BOB ADAMS FIELD,"STEAMBOAT SPRINGS, CO - UNITED STATES",Steamboat Springs/Bob Adams Field,"Steamboat Springs, Colorado, United States",OK
    SBT,////,////,////,UNK,0.769,Tri-City,"San Bernardino (CA), USA",OWNO,////,////,Metropolitan Area,"San Bernardino, California, United States",OK
    SBU,////,////,FASB,UNK,1.0,Springbok,"Springbok, South Africa",OWNO,////,////,Springbok Airport,"Springbok, Northern Cape, South Africa",OK
    SBV,////,////,////,UNK,1.0,Sabah,"Sabah, Papua New Guinea",OWNO,////,////,????,"Sabah, Borneo, Bougainville, Papua-New Guinea",OK
    SBW,////,////,WBGS,UNK,1.0,Sibu,"Sibu, Malaysia",OWNO,////,////,????,"Sibu, Sarawak, Malaysia",OK
    SBX,SBX,KSBX,KSBX,OK,1.0,Shelby,"Shelby (MT), USA",OWNO,SHELBY,"SHELBY, MT - UNITED STATES",????,"Shelby, Montana, United States",OK
    SBY,SBY,KSBY,KSBY,OK,1.0,Wicomico Regional,"Salisbury-Ocean City (MD), USA",OWNO,SALISBURY-OCEAN CITY WICOMICO RGNL,"SALISBURY, MD - UNITED STATES",Salisbury-Ocean City Wicomico Regional,"Salisbury, Maryland, United States",OK
    SBZ,////,////,LRSB,UNK,1.0,Sibiu,"Sibiu, Romania",OWNO,////,////,????,"Sibiu, Romania",OK
    SCA,////,////,////,UNK,1.0,Santa Catalina,"Santa Catalina, Colombia",OWNO,////,////,????,"Santa Catalina, Antioquia, Colombia",OK
    SCB,SCB,KSCB,KSCB,OK,1.0,State,"Scribner (NE), USA",OWNO,SCRIBNER STATE,"SCRIBNER, NE - UNITED STATES",Scribner State Airport,"Scribner, Nebraska, United States",OK
    SCC,SCC,PASC,PASC,OK,1.0,Prudhoe Bay/Deadhorse,"Prudhoe Bay/Deadhorse (AK), USA",OWNO,DEADHORSE,"DEADHORSE, AK - UNITED STATES",????,"Deadhorse, Alaska, United States",OK
    SCD,////,////,////,UNK,1.0,Sulaco,"Sulaco, Honduras",OWNO,////,////,????,"Sulaco, Comayagua, Honduras",OK
    SCE,UNV,KUNV,KUNV,OK,1.0,University Park,"State College (PA), USA",OWNO,UNIVERSITY PARK,"STATE COLLEGE, - UNITED STATES",University Park Airport,"State College, Pennsylvania, United States",OK
    SCF,SDL,KSDL,KSDL,OK,0.778,Scottsdale Municipal,"Phoenix (AZ), USA",OWNO,SCOTTSDALE,"SCOTTSDALE, AZ - UNITED STATES",????,"Scottsdale, Arizona, United States",OK
    SCG,////,////,YSPK,UNK,1.0,Spring Creek,"Spring Creek, Australia",OWNO,////,////,????,"Spring Creek, Queensland, Australia",OK
    SCH,SCH,KSCH,KSCH,OK,1.0,County,"Schenectady (NY), USA",OWNO,SCHENECTADY COUNTY,"SCHENECTADY, NY - UNITED STATES",Schenectady County Airport,"Schenectady, New York, United States",OK
    SCI,////,////,SVPM,UNK,0.75,San Cristobal,"San Cristobal, Venezuela",OWNO,////,////,Paramillo,"San Cristóbal, Táchira, Venezuela",OK
    SCK,SCK,KSCK,KSCK,OK,0.555,Stockton,"Stockton (CA), USA",OWNO,STOCKTON METROPOLITAN,"STOCKTON, CA - UNITED STATES",Stockton Metropolitan Airport,"Stockton, California, United States",OK
    SCL,////,////,SCEL,UNK,0.926,Arturo Merino Benitez,"Santiago, Chile",OWNO,////,////,Arturo Merino Benítez International Airport,"Santiago, Metropolitana de Santiago, Chile",OK
    SCM,SCM,PACM,PACM,OK,0.815,SPB,"Scammon Bay (AK), USA",OWNO,SCAMMON BAY,"SCAMMON BAY, AK - UNITED STATES",????,"Scammon Bay, Alaska, United States",OK
    SCN,////,////,EDDR,UNK,1.0,"Saarbrücken Airport, Ensheim","Saarbruecken, Germany",OWNO,////,////,Ensheim,"Saarbrücken, Saarland, Germany",OK
    SCO,////,////,UATE,UNK,0.667,Aktau,"Aktau, Kazakhstan",OWNO,////,////,Aktau International Airport,"Aktau, Mangghystaü, Kazakhstan",OK
    SCP,////,////,LFNC,UNK,0.667,St Crepin,"St Crepin, France",OWNO,////,////,????,"Mont Dauphin - St-Crepin, Provence-Alpes-Côte d'Azur, France",TO DO CHECK
    SCQ,////,////,LEST,UNK,1.0,Santiago De Compostela,"Santiago De Compostela, Spain",OWNO,////,////,Santiago de Compostela,"Santiago de Compostela, Galicia, Spain",OK
    SCS,////,////,EGPM,UNK,1.0,Scatsta,"Shetland Islands, United Kingdom",OWNO,////,////,Scatsta,"Shetland Islands, Shetland Islands, Scotland, United Kingdom",OK
    SCT,////,////,OYSQ,UNK,0.75,Socotra,"Socotra, Yemen",OWNO,////,////,Socotra International Airport,"Socotra, Yemen",OK
    SCU,////,////,MUCU,UNK,0.857,Antonio Maceo,"Santiago, Cuba",OWNO,////,////,Antonio Maceo International Airport,"Santiago de Cuba, Santiago de Cuba, Cuba",OK
    SCV,////,////,LRSV,UNK,1.0,Salcea,"Suceava, Romania",OWNO,////,////,Salcea,"Suceava, Romania",OK
    SCW,////,////,UUYY,UNK,1.0,Syktyvkar,"Syktyvkar, Russia",OWNO,////,////,Syktyvkar Airport,"Syktyvkar, Komi, Russian Federation (Russia)",OK
    SCX,////,////,////,UNK,0.846,Salina Cruz,"Salina Cruz, Mexico",OWNO,////,////,NAS Salina Cruz,"Salina Cruz, Oaxaca, México",OK
    SCY,////,////,SEST,UNK,1.0,San Cristobal Airport,"San Cristobal, Ecuador",OWNO,////,////,San Cristóbal,"San Cristóbal, San Cristóbal Island, Galápagos Islands, Ecuador",OK
    SCZ,////,////,AGGL,UNK,1.0,Santa Cruz Island,"Santa Cruz Island, Solomon Islands",OWNO,////,////,????,"Santa Cruz/Graciosa Bay/Luova, Santa Cruz Island, Solomon Islands",OK
    SDB,////,////,FALW,UNK,0.88,Langebaanweg,"Saldanha Bay, South Africa",OWNO,////,////,AFB Langebaanweg,"Langebaanweg, Western Cape, South Africa",OK
    SDC,////,////,////,UNK,1.0,Sandcreek,"Sandcreek, Guyana",OWNO,////,////,????,"Sandcreek, Upper Takutu-Upper Essequibo, Guyana",OK
    SDD,////,////,FNUB,UNK,1.0,Lubango,"Lubango, Angola",OWNO,////,////,????,"Lubango, Angola",OK
    SDE,////,////,SANE,UNK,0.5,Santiago Del Estero,"Santiago Del Estero, Argentina",OWNO,////,////,Vicecomodoro Ángel de la Paz Aragonés Airport,"Santiago del Estero, Santiago del Estero, Argentina",OK
    SDF,SDF,KSDF,KSDF,OK,0.874,Standiford Field,"Louisville (KY), USA",OWNO,LOUISVILLE INTL-STANDIFORD FIELD,"LOUISVILLE, KY - UNITED STATES",Louisville International-Standiford Field,"Louisville, Kentucky, United States",OK
    SDG,////,////,OICS,UNK,1.0,Sanandaj,"Sanandaj, Iran",OWNO,////,////,????,"Sanandaj, Kordestan, Iran",OK
    SDH,////,////,MHSR,UNK,0.929,Santa Rosa Copan,"Santa Rosa Copan, Honduras",OWNO,////,////,????,"Santa Rosa de Copán, Copán, Honduras",MAYBE
    SDI,////,////,////,UNK,1.0,Saidor,"Saidor, Papua New Guinea",OWNO,////,////,????,"Saidor, Madang, Papua-New Guinea",OK
    SDJ,////,////,RJSS,UNK,1.0,Sendai,"Sendai, Japan",OWNO,////,////,Sendai Airport,"Sendai, Miyagi, Japan",OK
    SDK,////,////,WBKS,UNK,1.0,Sandakan,"Sandakan, Malaysia",OWNO,////,////,????,"Sandakan, Sabah, Malaysia",OK
    SDL,////,////,ESNN,UNK,0.81,Sundsvall/Harnosand,"Sundsvall, Sweden",OWNO,////,////,Midlanda,"Sundsvall-Härnösand, Västernorrlands län, Sweden",TO DO CHECK
    SDM,SDM,KSDM,KSDM,OK,1.0,Brown Field Municipal,"San Diego (CA), USA",OWNO,BROWN FIELD MUNI,"SAN DIEGO, CA - UNITED STATES",Brown Field Municipal Airport,"San Diego, California, United States",OK
    SDN,////,////,ENSD,UNK,1.0,Sandane,"Sandane, Norway",OWNO,////,////,Sandane,"Anda, Norway",OK
    SDP,SDP,PASD,PASD,OK,0.76,Municipal,"Sand Point (AK), USA",OWNO,SAND POINT,"SAND POINT, AK - UNITED STATES",????,"Sand Point, Alaska, United States",OK
    SDQ,////,////,MDSD,UNK,0.955,Las Americas,"Santo Domingo, Dominican Republic",OWNO,////,////,Las Américas International Airport,"Santo Domingo, Santo Domingo, Dominican Republic",OK
    SDR,////,////,LEXJ,UNK,1.0,Santander,"Santander, Spain",OWNO,////,////,????,"Santander, Cantabria, Spain",OK
    SDS,////,////,RJSD,UNK,0.6,Sado Shima,"Sado Shima, Japan",OWNO,////,////,Sado Airport,"Sado, Sado Island, Niigata, Japan",MAYBE
    SDT,////,////,OPSS,UNK,1.0,Saidu Sharif,"Saidu Sharif, Pakistan",OWNO,////,////,Saidu Sharif Airport,"Saidu Sharif, Khyber Pakhtunkhwa, Pakistan",OK
    SDU,////,////,SBRJ,UNK,0.848,Santos Dumont,"Rio De Janeiro, Brazil",OWNO,////,////,Aeroporto Santos Dumont,"Rio de Janeiro, Rio de Janeiro, Brazil",OK
    SDV,////,////,LLSD,UNK,1.0,Sde Dov,"Tel Aviv Yafo, Israel",OWNO,////,////,Sde Dov,"Tel Aviv, Israel",OK
    SDX,SEZ,KSEZ,KSEZ,OK,1.0,Sedona,"Sedona (AZ), USA",OWNO,SEDONA,"SEDONA, AZ - UNITED STATES",????,"Sedona, Arizona, United States",OK
    SDY,SDY,KSDY,KSDY,OK,1.0,Richland Municipal,"Sidney (MT), USA",OWNO,SIDNEY-RICHLAND MUNI,"SIDNEY, MT - UNITED STATES",Sidney-Richland Municipal Airport,"Sidney, Montana, United States",OK
    SDZ,////,////,////,UNK,1.0,Metropolitan Area,"Shetland Islands, United Kingdom",OWNO,////,////,Metropolitan Area,"Shetland Islands, Scotland, United Kingdom",OK
    SEA,SEA,KSEA,KSEA,OK,1.0,Seattle/Tacoma International,"Seattle (WA), USA",OWNO,SEATTLE-TACOMA INTL,"SEATTLE, WA - UNITED STATES",Seattle-Tacoma International Airport,"Seattle, Washington, United States",OK
    SEB,////,////,HLLS,UNK,1.0,Sebha,"Sebha, Libya",OWNO,////,////,????,"Sebha, Libyan Arab Jamahiriya (Libya)",OK
    SED,////,////,////,UNK,1.0,Min'hat Hashnayim,"Sedom, Israel",OWNO,////,////,Min'hat Hashnayim,"Sedom, Israel",OK
    SEE,SEE,KSEE,KSEE,OK,1.0,Gillespie Field,"San Diego (CA), USA",OWNO,GILLESPIE FIELD,"SAN DIEGO/EL CAJON, CA - UNITED STATES",Gillespie Field,"San Diego/El Cajon, California, United States",OK
    SEF,SEF,KSEF,KSEF,OK,0.445,Air Terminal,"Sebring (FL), USA",OWNO,SEBRING RGNL,"SEBRING, FL - UNITED STATES",Sebring Regional,"Sebring, Florida, United States",OK
    SEG,SEG,KSEG,KSEG,OK,1.0,Penn Valley,"Selinsgrove (PA), USA",OWNO,PENN VALLEY,"SELINSGROVE, PA - UNITED STATES",Penn Valley Airport,"Selinsgrove, Pennsylvania, United States",OK
    SEH,////,////,WAJS,UNK,1.0,Senggeh,"Senggeh, Indonesia",OWNO,////,////,????,"Senggeh, Papua, Indonesia",OK
    SEK,////,////,UESK,UNK,1.0,Srednekolymsk,Srednekolymsk,IATA,////,////,Srednekoly'msk Airport,"Srednekoly'msk, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    SEM,SEM,KSEM,KSEM,OK,0.658,Craig AFB,"Selma (AL), USA",OWNO,CRAIG FIELD,"SELMA, AL - UNITED STATES",Craig Field,"Selma, Alabama, United States",OK
    SEN,////,////,EGMC,UNK,0.8,Municipal,"Southend, United Kingdom",OWNO,////,////,London Southend Airport,"Southend, Essex, England, United Kingdom",OK
    SEO,////,////,DISG,UNK,1.0,Seguela,"Seguela, Cote d'Ivoire",OWNO,////,////,Séguéla Airport,"Séguéla, Worodougou, Côte d'Ivoire (Ivory Coast)",OK
    SEP,SEP,KSEP,KSEP,OK,0.776,Clark Field,"Stephenville (TX), USA",OWNO,STEPHENVILLE CLARK RGNL,"STEPHENVILLE, TX - UNITED STATES",Stephenville Clark Regional,"Stephenville, Texas, United States",OK
    SEQ,////,////,WIBS,UNK,0.737,Sungai Pakning,"Sungai Pakning, Indonesia",OWNO,////,////,????,"Sungai Pakning Bengkalis, Riau, Indonesia",MAYBE
    SER,SER,KSER,KSER,OK,1.0,Freeman Municipal,"Seymour (IN), USA",OWNO,FREEMAN MUNI,"SEYMOUR, IN - UNITED STATES",Freeman Municipal Airport,"Seymour, Indiana, United States",OK
    SEU,////,////,HTSN,UNK,0.667,Seronera,"Seronera, Tanzania",OWNO,////,////,Seronera Airstrip,"Seronera, Serengeti National Park, Mara, Tanzania",OK
    SEV,////,////,UKCS,UNK,0.595,Severodoneck,"Severodoneck, Ukraine",OWNO,////,////,Lisichansk East,"Syeverodonetsk, Luhansk, Ukraine",TO DO CHECK
    SEW,////,////,////,UNK,0.4,Siwa,"Siwa, Egypt",OWNO,////,////,Oasis North,"Siwa, Matruh (Matrouh), Egypt",OK
    SEY,////,////,GQNS,UNK,1.0,Selibaby,"Selibaby, Mauritania",OWNO,////,////,Sélibaby Airport,"Sélibaby, Mauritania",OK
    SEZ,////,////,FSIA,UNK,1.0,Seychelles International,"Mahe Island, Seychelles",OWNO,////,////,Seychelles International Airport,"Mahé, Mahé Island, Seychelles",OK
    SFA,////,////,DTTX,UNK,0.5,Sfax El Maou,"Sfax, Tunisia",OWNO,////,////,Thyna International Airport,"Sfax, Tunisia",OK
    SFB,SFB,KSFB,KSFB,OK,0.382,Central Florida Reg.,"Orlando (FL), USA",OWNO,ORLANDO SANFORD INTL,"ORLANDO, FL - UNITED STATES",Orlando Sanford International Airport,"Orlando, Florida, United States",OK
    SFC,////,////,TFFC,UNK,1.0,St Francois,"St Francois, Guadeloupe",OWNO,////,////,????,"St-François, Guadeloupe",OK
    SFD,////,////,SVSR,UNK,0.8,Las Flecheras,"San Fernando De Apure, Venezuela",OWNO,////,////,????,"San Fernando de Apu, Guárico, Venezuela",MAYBE
    SFE,////,////,RPUS,UNK,1.0,San Fernando,"San Fernando, Philippines",OWNO,////,////,????,"San Fernando, Philippines",OK
    SFF,SFF,KSFF,KSFF,OK,1.0,Felts Field,"Spokane (WA), USA",OWNO,FELTS FIELD,"SPOKANE, WA - UNITED STATES",Felts Field,"Spokane, Washington, United States",OK

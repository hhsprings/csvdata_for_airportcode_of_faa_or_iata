
List for checking certainty of Great Circle Mapper (OSO - OZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    OSO,////,////,YOSB,UNK,nan,////,////,////,////,////,Osborne Mine Airport,"Burnham, Queensland, Australia",UNK
    OSP,////,////,EPSK,UNK,1.0,Redzikowo,"Slupsk, Poland",OWNO,////,////,Redzikowo,"Slupsk, Pomorskie, Poland",OK
    OSR,////,////,LKMT,UNK,0.588,Mosnov,"Ostrava, Czech Republic",OWNO,////,////,Leos Janacek Airport Ostrava,"Ostrava, Moravia-Silesia, Czech Republic",OK
    OSS,////,////,UCFO,UNK,0.545,Osh,"Osh, Kyrgyzstan",OWNO,////,////,Osh International Airport,"Osh, Osh, Kyrgyzstan",OK
    OST,////,////,EBOS,UNK,0.5,Ostend,"Ostend, Belgium",OWNO,////,////,Ostend-Brugge International Airport,"Ostend, West-Vlaanderen (West Flanders), Belgium",OK
    OSU,OSU,KOSU,KOSU,OK,1.0,Ohio State University,"Columbus (OH), USA",OWNO,OHIO STATE UNIVERSITY,"COLUMBUS, OH - UNITED STATES",Ohio State University Airport,"Columbus, Ohio, United States",OK
    OSW,////,////,UWOR,UNK,1.0,Orsk,"Orsk, Russia",OWNO,////,////,Orsk Airport,"Orsk, Orenburgskaya, Russian Federation (Russia)",OK
    OSX,OSX,KOSX,KOSX,OK,1.0,Attala County,"Kosciusko (MS), USA",OWNO,KOSCIUSKO-ATTALA COUNTY,"KOSCIUSKO, MS - UNITED STATES",Kosciusko-Attala County Airport,"Kosciusko, Mississippi, United States",OK
    OSY,////,////,ENNM,UNK,1.0,Namsos,"Namsos, Norway",OWNO,////,////,????,"Namsos, Norway",OK
    OSZ,////,////,EPKO,UNK,0.483,Koszalin,"Koszalin, Poland",OWNO,////,////,Zegrze Pomorskie AB,"Koszalin, Zachodniopomorskie, Poland",OK
    OTA,////,////,HAMO,UNK,1.0,Mota,"Mota, Ethiopia",OWNO,////,////,????,"Mota, Amara, Ethiopia",OK
    OTC,////,////,FTTL,UNK,1.0,Bol,"Bol, Chad",OWNO,////,////,????,"Bol, Lac, Chad",OK
    OTD,////,////,////,UNK,1.0,Contadora,"Contadora, Panama",OWNO,////,////,????,"Contadora, Panamá, Panamá",OK
    OTG,OTG,KOTG,KOTG,OK,0.799,Worthington,"Worthington (MN), USA",OWNO,WORTHINGTON MUNI,"WORTHINGTON, MN - UNITED STATES",Worthington Municipal Airport,"Worthington, Minnesota, United States",OK
    OTH,OTH,KOTH,KOTH,OK,0.49,North Bend,"North Bend (OR), USA",OWNO,SOUTHWEST OREGON RGNL,"NORTH BEND, OR - UNITED STATES",Southwest Oregon Regional,"North Bend, Oregon, United States",OK
    OTI,////,////,WAMR,UNK,1.0,Morotai Island,"Morotai Island, Indonesia",OWNO,////,////,????,"Pitu, Morotai Island, Maluku, Indonesia",OK
    OTL,////,////,GQNB,UNK,1.0,Boutilimit,"Boutilimit, Mauritania",OWNO,////,////,????,"Boutilimit, Mauritania",OK
    OTM,OTM,KOTM,KOTM,OK,0.36,Industrial,"Ottumwa IA, USA",OWNO,OTTUMWA RGNL,"OTTUMWA, IA - UNITED STATES",Ottumwa Regional,"Ottumwa, Iowa, United States",OK
    OTP,////,////,LROP,UNK,0.649,Otopeni International,"Bucharest, Romania",OWNO,////,////,Henri Coanda International Airport,"Bucharest, Romania",OK
    OTR,////,////,MRCC,UNK,1.0,Coto 47,"Coto 47, Costa Rica",OWNO,////,////,????,"Coto 47, Puntarenas, Costa Rica",OK
    OTS,74S,////,////,OK,1.0,Anacortes,"Anacortes (WA), USA",OWNO,ANACORTES,"ANACORTES, WA - UNITED STATES",????,"Anacortes, Washington, United States",OK
    OTU,////,////,SKOT,UNK,1.0,Otu,"Otu, Colombia",OWNO,////,////,Otú Airport,"Remedios, Antioquia, Colombia",TO DO CHECK
    OTY,////,////,////,UNK,1.0,Oria,"Oria, Papua New Guinea",OWNO,////,////,????,"Oria, Bougainville, Papua-New Guinea",OK
    OTZ,OTZ,PAOT,PAOT,OK,0.378,Kotzebue,"Kotzebue (AK), USA",OWNO,RALPH WIEN MEMORIAL,"KOTZEBUE, AK - UNITED STATES",Ralph Wien Memorial Airport,"Kotzebue, Alaska, United States",OK
    OUA,////,////,DFFD,UNK,1.0,Ouagadougou,"Ouagadougou, Burkina Faso",OWNO,////,////,????,"Ouagadougou, Burkina Faso",OK
    OUD,////,////,GMFO,UNK,0.971,Les Angades,"Oujda, Morocco",OWNO,////,////,Les Anglades,"Oujda, Morocco",OK
    OUE,////,////,FCOU,UNK,1.0,Ouesso,"Ouesso, Congo (ROC)",OWNO,////,////,????,"Ouesso, Congo (Republic of)",OK
    OUG,////,////,DFCC,UNK,1.0,Ouahigouya,"Ouahigouya, Burkina Faso",OWNO,////,////,????,"Ouahigouya, Burkina Faso",OK
    OUH,////,////,FAOH,UNK,1.0,Oudtshoorn,"Oudtshoorn, South Africa",OWNO,////,////,Oudtshoorn Airport,"Oudtshoorn, Western Cape, South Africa",OK
    OUI,////,////,VLHS,UNK,0.857,Ban Houei,"Ban Houei, Lao PDR",OWNO,////,////,????,"Ban Houeisai, Lao People's Democratic Republic (Laos)",MAYBE
    OUK,////,////,////,UNK,1.0,Outer Skerries,"Outer Skerries, United Kingdom",OWNO,////,////,????,"Outer Skerries, Shetland Islands, Scotland, United Kingdom",OK
    OUL,////,////,EFOU,UNK,1.0,Oulu,"Oulu, Finland",OWNO,////,////,????,"Oulu, Pohjois-Pohjanmaa (Norra Österbotten (Northern Ostrobothnia)), Finland",OK
    OUM,////,////,////,UNK,1.0,Oum Hadjer,"Oum Hadjer, Chad",OWNO,////,////,Oum-Hadjer Airport,"Oum-Hadjer, Batha, Chad",OK
    OUN,OUN,KOUN,KOUN,OK,0.645,Max Westheimer,"Norman (OK), USA",OWNO,UNIVERSITY OF OKLAHOMA WESTHEIMER,"NORMAN, OK - UNITED STATES",University of Oklahoma / Max Westheimer Airport,"Norman, Oklahoma, United States",OK
    OUR,////,////,FKKI,UNK,1.0,Batouri,"Batouri, Cameroon",OWNO,////,////,Batouri Airport,"Batouri, East (Est), Cameroon",OK
    OUS,////,////,SDOU,UNK,1.0,Ourinhos,"Ourinhos, Brazil",OWNO,////,////,????,"Ourinhos, São Paulo, Brazil",OK
    OUT,////,////,FTTS,UNK,1.0,Bousso,"Bousso, Chad",OWNO,////,////,Bousso Airport,"Bousso, Chari-Baguirmi, Chad",OK
    OUU,////,////,////,UNK,1.0,Ouanga,"Ouanga, Gabon",OWNO,////,////,????,"Ouanga, Nyanga, Gabon",OK
    OUZ,////,////,GQPZ,UNK,0.667,Zouerate,"Zouerate, Mauritania",OWNO,////,////,Tazadit,"Zouérate, Mauritania",OK
    OVA,////,////,FMSL,UNK,1.0,Bekily,"Bekily, Madagascar",OWNO,////,////,Bekily Airport,"Bekily, Madagascar",OK
    OVB,////,////,UNNT,UNK,0.905,Tolmachevo,"Novosibirsk, Russia",OWNO,////,////,Tolmachevo International Airport,"Novosibirsk, Novosibirskaya, Russian Federation (Russia)",OK
    OVD,////,////,LEAS,UNK,1.0,Oviedo,"Asturias, Spain",OWNO,////,////,Asturias,"Oviedo/Aviles, Asturias, Spain",OK
    OVE,OVE,KOVE,KOVE,OK,0.781,Oroville,"Oroville (CA), USA",OWNO,OROVILLE MUNI,"OROVILLE, CA - UNITED STATES",Oroville Municipal Airport,"Oroville, California, United States",OK
    OVL,////,////,SCOV,UNK,0.571,Ovalle,"Ovalle, Chile",OWNO,////,////,El Tuqui Airport,"Ovalle, Coquimbo, Chile",OK
    OVS,////,////,USHS,UNK,nan,////,////,////,////,////,Tyumenskaya Airprot,"Sovetsky, Khanty-Mansiyskiy, Russian Federation (Russia)",UNK
    OWA,OWA,KOWA,KOWA,OK,0.563,Owatonna,"Owatonna (MN), USA",OWNO,OWATONNA DEGNER RGNL,"OWATONNA, MN - UNITED STATES",Owatonna Degner Regional,"Owatonna, Minnesota, United States",OK
    OWB,OWB,KOWB,KOWB,OK,1.0,Daviess County,"Owensboro (KY), USA",OWNO,OWENSBORO-DAVIESS COUNTY,"OWENSBORO, KY - UNITED STATES",Owensboro-Daviess County Airport,"Owensboro, Kentucky, United States",OK
    OWD,OWD,KOWD,KOWD,OK,1.0,Memorial,"Norwood (MA), USA",OWNO,NORWOOD MEMORIAL,"NORWOOD, MA - UNITED STATES",Norwood Memorial Airport,"Norwood, Massachusetts, United States",OK
    OWE,////,////,////,UNK,1.0,Owendo,"Owendo, Gabon",OWNO,////,////,????,"Owendo, Estuaire, Gabon",OK
    OWK,OWK,KOWK,KOWK,OK,0.897,Central Maine,"Norridgewock (ME), USA",OWNO,CENTRAL MAINE ARPT OF NORRIDGEWOCK,"NORRIDGEWOCK, ME - UNITED STATES",Central Maine Airport of Norridgewock,"Norridgewock, Maine, United States",OK
    OXB,////,////,GGOV,UNK,0.851,Osvaldo Vieira,"Bissau, Guinea-Bissau",OWNO,////,////,Oswaldo Vieira International Airport,"Bissau, Guinea-Bissau",OK
    OXC,OXC,KOXC,KOXC,OK,1.0,Waterbury-Oxford,"Oxford (CT), USA",OWNO,WATERBURY-OXFORD,"OXFORD, CT - UNITED STATES",Waterbury-Oxford Airport,"Oxford, Connecticut, United States",OK
    OXD,OXD,KOXD,KOXD,OK,1.0,Miami University,"Oxford (OH), USA",OWNO,MIAMI UNIVERSITY,"OXFORD, OH - UNITED STATES",Miami University Airport,"Oxford, Ohio, United States",OK
    OXF,////,////,EGTK,UNK,0.769,Kidlington,"Oxford, United Kingdom",OWNO,////,////,London Oxford Airport,"Oxford, Oxfordshire, England, United Kingdom",OK
    OXO,////,////,////,UNK,1.0,Orientos,"Orientos, Australia",OWNO,////,////,????,"Orientos, Queensland, Australia",OK
    OXR,OXR,KOXR,KOXR,OK,1.0,Oxnard/Ventura Airport,"Ventura (CA), USA",OWNO,OXNARD,"OXNARD, CA - UNITED STATES",????,"Oxnard, California, United States",OK
    OXY,////,////,YMNY,UNK,1.0,Morney,"Morney, Australia",OWNO,////,////,????,"Morney, Queensland, Australia",OK
    OYA,////,////,SATG,UNK,1.0,Goya,"Goya, Argentina",OWNO,////,////,????,"Goya, Corrientes, Argentina",OK
    OYE,////,////,FOGO,UNK,1.0,Oyem,"Oyem, Gabon",OWNO,////,////,????,"Oyem, Woleu-Ntem, Gabon",OK
    OYG,////,////,////,UNK,1.0,Moyo,"Moyo, Uganda",OWNO,////,////,????,"Moyo, Uganda",OK
    OYK,////,////,SBOI,UNK,1.0,Oiapoque,"Oiapoque, Brazil",OWNO,////,////,????,"Oiapoque, Amapá, Brazil",OK
    OYL,////,////,HKMY,UNK,1.0,Moyale,"Moyale, Kenya",OWNO,////,////,Moyale Airport,"Moyale, Kenya",OK
    OYN,////,////,YOUY,UNK,1.0,Ouyen,"Ouyen, Australia",OWNO,////,////,????,"Ouyen, Victoria, Australia",OK
    OYO,////,////,SAZH,UNK,1.0,Tres Arroyos,"Tres Arroyos, Argentina",OWNO,////,////,????,"Tres Arroyos, Buenos Aires, Argentina",OK
    OYP,////,////,SOOG,UNK,1.0,St-Georges de Loyapock,"St Georges de Loyapock, French Guiana",OWNO,////,////,????,"St-Georges de l'Oyapok, French Guiana",OK
    OZA,OZA,KOZA,KOZA,OK,0.664,Ozona,"Ozona (TX), USA",OWNO,OZONA MUNI,"OZONA, TX - UNITED STATES",Ozona Municipal Airport,"Ozona, Texas, United States",OK
    OZC,////,////,RPMO,UNK,1.0,Labo,"Ozamis City, Philippines",OWNO,////,////,Labo,"Ozamis City, Mindanao Island, Philippines",OK
    OZG,////,////,GMAZ,UNK,nan,////,////,////,////,////,Zagora Airport,"Zagora, Morocco",UNK
    OZH,////,////,UKDE,UNK,0.606,Zaporozhye,"Zaporozhye, Ukraine",OWNO,////,////,Mokraya International Airport,"Zaporozhye, Zaporizhia, Ukraine",OK
    OZP,////,////,LEMO,UNK,0.778,Morón Air Base,"Morón, Spain",OWNO,////,////,Morón AB,"Morón de la Frontera, Andalusia, Spain",MAYBE
    OZR,OZR,KOZR,KOZR,OK,1.0,Cairns AAF,"Ozark (AL), USA",OWNO,CAIRNS AAF (FORT RUCKER),"FORT RUCKER/OZARK, AL - UNITED STATES",Cairns AAF Airport,"Fort Rucker/Ozark, Alabama, United States",OK
    OZZ,////,////,GMMZ,UNK,1.0,Ouarzazate,"Ouarzazate, Morocco",OWNO,////,////,????,"Ouarzazate, Morocco",OK

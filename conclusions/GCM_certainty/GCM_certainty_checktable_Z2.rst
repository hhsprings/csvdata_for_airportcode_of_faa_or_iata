
List for checking certainty of Great Circle Mapper (ZML - ZZV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ZML,////,////,////,UNK,nan,////,////,////,////,////,Railroad Station,"Milwaukee, Wisconsin, United States",UNK
    ZMM,////,////,MMZM,UNK,0.522,Zamora,"Zamora, Mexico",OWNO,////,////,????,"Zamora de Hidalgo, Michoacán de Ocampo, México",MAYBE
    ZMT,////,////,CZMT,UNK,1.0,Masset,"Masset, Canada",OWNO,////,////,Masset Airport,"Masset, Haida Gwaii (Queen Charlotte Islands), British Columbia, Canada",OK
    ZNA,////,////,////,UNK,0.5,Harbour SPB,Nanaimo,IATA,////,////,Nanaimo Harbour Water Airport,"Nanaimo, British Columbia, Canada",MAYBE
    ZNC,ZNC,////,////,OK,1.0,Nyac,"Nyac (AK), USA",OWNO,NYAC,"NYAC, AK - UNITED STATES",????,"Nyac, Alaska, United States",OK
    ZND,////,////,DRZR,UNK,1.0,Zinder,"Zinder, Niger",OWNO,////,////,????,"Zinder, Niger",OK
    ZNE,////,////,YNWN,UNK,1.0,Newman,"Newman, Australia",OWNO,////,////,????,"Newman, Western Australia, Australia",OK
    ZNF,////,////,ETID,UNK,nan,////,////,////,////,////,AAF,"Hanau, Hessen, Germany",UNK
    ZNU,////,////,////,UNK,1.0,Namu,"Namu, Canada",OWNO,////,////,????,"Namu, British Columbia, Canada",OK
    ZNZ,////,////,HTZA,UNK,0.591,Kisauni,"Zanzibar, Tanzania",OWNO,////,////,Abeid Amani Karume International Airport,"Zanzibar City, Zanzibar West (Mjini Magharibi), Tanzania",MAYBE
    ZOF,////,////,////,UNK,1.0,Ocean Falls,"Ocean Falls, Canada",OWNO,////,////,????,"Ocean Falls, British Columbia, Canada",OK
    ZOS,////,////,SCJO,UNK,0.619,Canal Balo,"Osorno, Chile",OWNO,////,////,Cañal Bajo-Carlos Hott Siebert Airport,"Osorno, Los Lagos, Chile",OK
    ZPB,////,////,CZPB,UNK,nan,////,////,////,////,////,????,"Sachigo Lake, Ontario, Canada",UNK
    ZPC,////,////,SCPC,UNK,1.0,Pucon,"Pucon, Chile",OWNO,////,////,Pucón Airport,"Pucón, Araucanía, Chile",OK
    ZPH,ZPH,KZPH,KZPH,OK,1.0,Zephyrhills Municipal Airport,"Zephyrhills (FL), USA",OWNO,ZEPHYRHILLS MUNI,"ZEPHYRHILLS, FL - UNITED STATES",Zephyrhills Municipal Airport,"Zephyrhills, Florida, United States",OK
    ZPO,////,////,CZPO,UNK,nan,////,////,////,////,////,Pinehouse Lake Airport,"Pinehouse, Saskatchewan, Canada",UNK
    ZPQ,////,////,ETHE,UNK,nan,////,////,////,////,////,????,"Rheine Bentlage, North Rhine-Westphalia, Germany",UNK
    ZQN,////,////,NZQN,UNK,0.765,Frankton,"Queenstown, New Zealand",OWNO,////,////,Queenstown International Airport,"Queenstown, New Zealand",OK
    ZQS,////,////,////,UNK,0.9,Queen Charlotte Is,"Queen Charlotte Is, Canada",OWNO,////,////,????,"Queen Charlotte Island, British Columbia, Canada",MAYBE
    ZQV,////,////,EDFV,UNK,nan,////,////,////,////,////,????,"Worms, Rhineland-Palatinate, Germany",UNK
    ZQW,////,////,EDRZ,UNK,1.0,Zweibrücken Airport,"Zweibrücken, Rhineland-Palatinate, Germany",OWNO,////,////,????,"Zweibrücken, Rhineland-Palatinate, Germany",OK
    ZQZ,////,////,ZBZJ,UNK,nan,////,////,////,////,////,Zhangjiakou Ningyuan Airport,"Zhangjiakou, Hubei, China",UNK
    ZRA,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Atlantic City, New Jersey, United States",UNK
    ZRH,////,////,LSZH,UNK,0.667,Zurich International Airport,"Zürich, Switzerland",OWNO,////,////,Zürich-Kloten,"Zürich, Zürich, Switzerland",OK
    ZRI,////,////,WABO,UNK,0.4,Serui,"Serui, Indonesia",OWNO,////,////,S. Condronegoro Airport,"Serui, Papua, Indonesia",OK
    ZRJ,////,////,CZRJ,UNK,0.714,Round Lake,"Round Lake, Canada",OWNO,////,////,Weagamow Lake,"Round Lake, Ontario, Canada",OK
    ZRM,////,////,WAJI,UNK,0.727,Sarmi,"Sarmi, Indonesia",OWNO,////,////,Orai Airport,"Sarmi, Papua, Indonesia",OK
    ZRP,////,////,////,UNK,nan,////,////,////,////,////,Penn Station,"Newark, New Jersey, United States",UNK
    ZSA,////,MYSM,MYSM,OK,0.716,San Salvador,"San Salvador, Bahamas",OWNO,SAN SALVADOR INTL,"COCKBURN TOWN, - BAHAMAS",San Salvador International Airport,"Cockburn Town, San Salvador Island, Bahamas",OK
    ZSE,////,////,FMEP,UNK,0.69,St Pierre dela Reunion,"St Pierre dela Reunion, Reunion",OWNO,////,////,Pierrefonds,"St-Pierre, Réunion",TO DO CHECK
    ZSF,////,////,////,UNK,nan,////,////,////,////,////,Springfield Railway Service,"Springfield, Massachusetts, United States",UNK
    ZSJ,////,////,CZSJ,UNK,nan,////,////,////,////,////,????,"Sandy Lake, Ontario, Canada",UNK
    ZSS,////,////,DISS,UNK,1.0,Sassandra,"Sassandra, Cote d'Ivoire",OWNO,////,////,????,"Sassandra, Bas-Sassandra, Côte d'Ivoire (Ivory Coast)",OK
    ZST,////,CZST,CZST,OK,1.0,////,////,////,STEWART,"STEWART, - CANADA",????,"Stewart, British Columbia, Canada",OK
    ZSW,////,////,////,UNK,1.0,Seal Cove,"Prince Rupert, Canada",OWNO,////,////,Seal Cove,"Prince Rupert, British Columbia, Canada",OK
    ZTA,////,////,NTGY,UNK,0.923,Tureira,"Tureira, French Polynesia",OWNO,////,////,????,"Tureia, French Polynesia",OK
    ZTB,////,////,////,UNK,1.0,Tete-a-La Baleine,"Tete-a-La Baleine, Canada",OWNO,////,////,Tête-à-La Baleine Airport,"Tête-à-La Baleine, Québec, Canada",OK
    ZTF,////,////,////,UNK,nan,////,////,////,////,////,Stamford Transportation Center,"Stamford, Connecticut, United States",UNK
    ZTH,////,////,LGZA,UNK,1.0,"Zakynthos International Airport, ""Dionysios Solomos""","Zakinthos Island, Greece",OWNO,////,////,Dionysios Solomos International Airport,"Zakinthos, Ionía Nísia (Ionian Islands), Greece",MAYBE
    ZTM,////,////,CZTM,UNK,1.0,Shamattawa,"Shamattawa, Canada",OWNO,////,////,????,"Shamattawa, Manitoba, Canada",OK
    ZTR,////,////,UKKO,UNK,0.522,Zhitomir,"Zhitomir, Ukraine",OWNO,////,////,Zhitomyr Ozernoye AB,"Zhytomyr, Zhytomyr, Ukraine",OK
    ZTS,////,////,////,UNK,0.667,Tahsis,"Tahsis, Canada",OWNO,////,////,Tahsis Water Aerodrome,"Tahsis, British Columbia, Canada",OK
    ZTU,////,////,UBBY,UNK,nan,////,////,////,////,////,Zaqatala International Airport,"Zaqatala, Zaqatala, Azerbaijan",UNK
    ZUC,////,////,CZUC,UNK,0.714,Ignace,"Ignace, Canada",OWNO,////,////,Municipal,"Ignace, Ontario, Canada",OK
    ZUD,////,////,SCAC,UNK,0.625,Ancud,"Ancud, Chile",OWNO,////,////,Pupelde,"Ancud, Los Lagos, Chile",OK
    ZUH,////,////,ZGSD,UNK,0.865,Zhuhai Airport,"Zhuhai, PR China",OWNO,////,////,Zhuhai International Airport,"Zhuhai, Guangdong, China",OK
    ZUL,////,////,OEZL,UNK,1.0,Zilfi,"Zilfi, Saudi Arabia",OWNO,////,////,????,"Zilfi, Saudi Arabia",OK
    ZUM,////,////,CZUM,UNK,1.0,Churchill Falls,"Churchill Falls, Canada",OWNO,////,////,????,"Churchill Falls, Newfoundland and Labrador, Canada",OK
    ZUN,////,////,////,UNK,nan,////,////,////,////,////,Union Station,"Chicago, Illinois, United States",UNK
    ZVA,////,////,FMMN,UNK,1.0,Miandrivazo,"Miandrivazo, Madagascar",OWNO,////,////,????,"Miandrivazo, Madagascar",OK
    ZVE,////,////,////,UNK,nan,////,////,////,////,////,Union Station,"New Haven, Connecticut, United States",UNK
    ZVG,////,////,////,UNK,1.0,Springvale,"Springvale, Australia",OWNO,////,////,????,"Springvale, Western Australia, Australia",OK
    ZVK,////,////,VLSK,UNK,1.0,Savannakhet Airport,"Savannakhet, Lao PDR",OWNO,////,////,????,"Savannakhét, Lao People's Democratic Republic (Laos)",OK
    ZWA,////,////,FMND,UNK,1.0,Andapa,"Andapa, Madagascar",OWNO,////,////,????,"Andapa, Madagascar",OK
    ZWI,////,////,////,UNK,nan,////,////,////,////,////,Wilmington Station,"Wilmington, Delaware, United States",UNK
    ZWL,////,////,CZWL,UNK,1.0,Wollaston Lake,"Wollaston Lake, Canada",OWNO,////,////,????,"Wollaston Lake, Saskatchewan, Canada",OK
    ZWS,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Stuttgart, Baden-Württemberg, Germany",UNK
    ZWU,////,////,////,UNK,nan,////,////,////,////,////,Union Station,"Washington, District of Columbia, United States",UNK
    ZXT,////,////,UBTT,UNK,nan,////,////,////,////,////,Zabrat Airport,"Baku, Baki (Baku), Azerbaijan",UNK
    ZYI,////,////,ZUZY,UNK,1.0,Zunyi,"Zunyi, PR China",OWNO,////,////,????,"Zunyi, Guizhou, China",OK
    ZYL,////,////,VGSY,UNK,1.0,Osmany International Airport,"Sylhet, Bangladesh",OWNO,////,////,Osmani International Airport,"Sylhet, Bangladesh",OK
    ZYP,////,////,////,UNK,nan,////,////,////,////,////,Penn Station,"New York, New York, United States",UNK
    ZZU,////,////,FWUU,UNK,nan,////,////,////,////,////,????,"Mzuzu, Malawi",UNK
    ZZV,ZZV,KZZV,KZZV,OK,0.734,Zanesville,"Zanesville (OH), USA",OWNO,ZANESVILLE MUNI,"ZANESVILLE, OH - UNITED STATES",Zanesville Municipal Airport,"Zanesville, Ohio, United States",OK

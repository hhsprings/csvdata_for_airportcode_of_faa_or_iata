
List for checking certainty of Great Circle Mapper (HJT - HSV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    HJT,////,////,ZMHU,UNK,1.0,Khujirt,"Khujirt, Mongolia",OWNO,////,////,????,"Khujirt, Mongolia",OK
    HKA,HKA,KHKA,KHKA,OK,1.0,Municipal,"Blytheville (AR), USA",OWNO,BLYTHEVILLE MUNI,"BLYTHEVILLE, AR - UNITED STATES",Blytheville Municipal Airport,"Blytheville, Arkansas, United States",OK
    HKB,HRR,PAHV,PAHV,OK,0.64,Healy Lake,"Healy Lake (AK), USA",OWNO,HEALY RIVER,"HEALY, AK - UNITED STATES",Healy River Airport,"Healy, Alaska, United States",OK
    HKD,////,////,RJCH,UNK,1.0,Hakodate,"Hakodate, Japan",OWNO,////,////,Hakodate Airport,"Hakodate, Hokkaido, Japan",OK
    HKG,////,////,VHHH,UNK,0.71,Hong Kong International,"Hong Kong, Hong Kong, PR China",OWNO,////,////,Hong Kong International - Chek Lap Kok,"Hong Kong, Hong Kong",OK
    HKK,////,////,NZHK,UNK,1.0,Hokitika Airport,"Hokitika, New Zealand",OWNO,////,////,????,"Hokitika, New Zealand",OK
    HKN,////,////,AYHK,UNK,0.706,Hoskins,"Hoskins, Papua New Guinea",OWNO,////,////,Kimbe,"Hoskins, West New Britain, Papua-New Guinea",OK
    HKS,HKS,KHKS,KHKS,OK,1.0,Hawkins Field,"Jackson (MS), USA",OWNO,HAWKINS FIELD,"JACKSON, MS - UNITED STATES",Hawkins Field,"Jackson, Mississippi, United States",OK
    HKT,////,////,VTSP,UNK,1.0,International,"Phuket, Thailand",OWNO,////,////,Phuket International Airport,"Phuket, Phuket, Thailand",OK
    HKV,////,////,////,UNK,1.0,Haskovo,"Haskovo, Bulgaria",OWNO,////,////,????,"Haskovo, Haskovo, Bulgaria",OK
    HKY,HKY,KHKY,KHKY,OK,0.806,Hickory,"Hickory (NC), USA",OWNO,HICKORY RGNL,"HICKORY, NC - UNITED STATES",Hickory Regional,"Hickory, North Carolina, United States",OK
    HLA,////,////,FALA,UNK,0.8,Lanseria,"Lanseria, South Africa",OWNO,////,////,Lanseria International Airport,"Johannesburg, Gauteng, South Africa",OK
    HLB,HLB,KHLB,KHLB,OK,0.576,Hillenbrand,"Batesville (IN), USA",OWNO,BATESVILLE,"BATESVILLE, IN - UNITED STATES",????,"Batesville, Indiana, United States",OK
    HLC,HLC,KHLC,KHLC,OK,0.748,Hill City,"Hill City (KS), USA",OWNO,HILL CITY MUNI,"HILL CITY, KS - UNITED STATES",Hill City Municipal Airport,"Hill City, Kansas, United States",OK
    HLD,////,////,ZBLA,UNK,0.824,Hailar,"Hailar, PR China",OWNO,////,////,Dongshan Airport,"Hailar, Inner Mongolia, China",OK
    HLF,////,////,ESSF,UNK,0.615,Hultsfred,"Hultsfred, Sweden",OWNO,////,////,Hultsfred-Vimmerby AB,"Hultsfred-Vimmerby, Kalmar län, Sweden",TO DO CHECK
    HLG,HLG,KHLG,KHLG,OK,0.88,Ohio County,"Wheeling (WV), USA",OWNO,WHEELING OHIO CO,"WHEELING, WV - UNITED STATES",Wheeling Ohio County Airport,"Wheeling, West Virginia, United States",OK
    HLH,////,////,ZBUL,UNK,1.0,Ulanhot,"Ulanhot, PR China",OWNO,////,////,????,"Ulanhot, Inner Mongolia, China",OK
    HLI,CVH,KCVH,KCVH,OK,0.826,Hollister,"Hollister (CA), USA",OWNO,HOLLISTER MUNI,"HOLLISTER, CA - UNITED STATES",Hollister Municipal Airport,"Hollister, California, United States",OK
    HLJ,////,////,EYSB,UNK,0.375,Shauliaj,"Shauliaj, Lithuania",OWNO,////,////,????,"Barysiai, Lithuania",TO DO CHECK
    HLL,////,////,YHIL,UNK,1.0,Hillside,"Hillside, Australia",OWNO,////,////,????,"Hillside, Western Australia, Australia",OK
    HLM,HLM,KHLM,KHLM,OK,1.0,Park Township,"Holland (MI), USA",OWNO,PARK TOWNSHIP,"HOLLAND, MI - UNITED STATES",Park Township Airport,"Holland, Michigan, United States",OK
    HLN,HLN,KHLN,KHLN,OK,1.0,Helena Regional,"Helena (MT), USA",OWNO,HELENA RGNL,"HELENA, MT - UNITED STATES",Helena Regional,"Helena, Montana, United States",OK
    HLP,////,////,WIHH,UNK,0.7,Halim Perdana Kusuma,"Jakarta, Indonesia",OWNO,////,////,Halim Perdanakusuma International Airport,"Jakarta, Jawa Barat, Indonesia",OK
    HLR,HLR,KHLR,KHLR,OK,0.938,Fort Hood AAF,"Killeen (TX), USA",OWNO,HOOD AAF,"FORT HOOD(KILLEEN), TX - UNITED STATES",Hood AAF Airport,"Fort Hood, Texas, United States",OK
    HLS,////,////,YSTH,UNK,1.0,St Helens,"St Helens, Australia",OWNO,////,////,????,"St. Helens, Tasmania, Australia",OK
    HLT,////,////,YHML,UNK,1.0,Hamilton,"Hamilton, Australia",OWNO,////,////,????,"Hamilton, Victoria, Australia",OK
    HLV,////,////,////,UNK,1.0,Helenvale,"Helenvale, Australia",OWNO,////,////,????,"Helenvale, Queensland, Australia",OK
    HLW,////,////,FAHL,UNK,1.0,Hluhluwe,"Hluhluwe, South Africa",OWNO,////,////,Hluhluwe Airport,"Hluhluwe, KwaZulu-Natal, South Africa",OK
    HLZ,////,////,NZHN,UNK,1.0,Hamilton,"Hamilton, New Zealand",OWNO,////,////,????,"Hamilton, New Zealand",OK
    HMA,////,////,USHH,UNK,nan,////,////,////,////,////,Khanty-Mansiysk Airport,"Khanty-Mansiysk, Khanty-Mansiyskiy, Russian Federation (Russia)",UNK
    HMB,////,////,HEMK,UNK,nan,////,////,////,////,////,Mubarak International Airport,"Sohag, Al Wadi al Jadid (New Valley), Egypt",UNK
    HME,////,////,DAUH,UNK,0.794,Oued Irara Apt,"Hassi Messaoud, Algeria",OWNO,////,////,Oued Irara-Krim Belkacem Airport,"Hassi Messaoud, Ouargla, Algeria",OK
    HMG,////,////,YHMB,UNK,1.0,Hermannsburg,"Hermannsburg, Australia",OWNO,////,////,????,"Hermannsburg, Northern Territory, Australia",OK
    HMI,////,////,ZWHM,UNK,1.0,Hami,"Hami, PR China",OWNO,////,////,????,"Hami, Xinjiang, China",OK
    HMJ,////,////,UKLH,UNK,1.0,Khmelnitskiy,"Khmelnitskiy, Ukraine",OWNO,////,////,????,"Khmelnitskiy, Khmelnytskyi, Ukraine",OK
    HMN,HMN,KHMN,KHMN,OK,1.0,Holloman AFB,"Alamogordo (NM), USA",OWNO,HOLLOMAN AFB,"ALAMOGORDO, NM - UNITED STATES",Holloman AFB,"Alamogordo, New Mexico, United States",OK
    HMO,////,////,MMHO,UNK,0.786,Gen Pesqueira Garcia,"Hermosillo, Mexico",OWNO,////,////,General Ignacio Pesqueira García International Airport,"Hermosillo, Sonora, México",OK
    HMR,////,////,ENHA,UNK,0.526,Hamar Airport,"Hamar, Norway",OWNO,////,////,Stafsberg,"Hamar, Norway",OK
    HMT,HMT,KHMT,KHMT,OK,0.766,Ryan Field,"Hemet (CA), USA",OWNO,HEMET-RYAN,"HEMET, CA - UNITED STATES",Hemet-Ryan Airport,"Hemet, California, United States",OK
    HMV,////,////,ESUT,UNK,1.0,Hemavan,"Hemavan, Sweden",OWNO,////,////,????,"Hemavan, Västerbottens län, Sweden",OK
    HNA,////,////,RJSI,UNK,1.0,Hanamaki,"Morioka, Japan",OWNO,////,////,Hanamaki Airport,"Morioka, Iwate, Japan",OK
    HNB,HNB,KHNB,KHNB,OK,0.79,Municipal,"Huntingburg (IN), USA",OWNO,HUNTINGBURG,"HUNTINGBURG, IN - UNITED STATES",????,"Huntingburg, Indiana, United States",OK
    HNC,HSE,KHSE,KHSE,OK,0.613,Hatteras,"Hatteras (NC), USA",OWNO,BILLY MITCHELL,"HATTERAS, NC - UNITED STATES",Billy Mitchell Airport,"Hatteras, North Carolina, United States",OK
    HND,////,////,RJTT,UNK,1.0,Haneda,"Tokyo, Japan",OWNO,////,////,Haneda,"Tokyo, Tokyo, Japan",OK
    HNH,HNH,PAOH,PAOH,OK,1.0,Hoonah,"Hoonah (AK), USA",OWNO,HOONAH,"HOONAH, AK - UNITED STATES",????,"Hoonah, Alaska, United States",OK
    HNI,////,////,////,UNK,1.0,Heiweni,"Heiweni, Papua New Guinea",OWNO,////,////,????,"Heiweni, Morobe, Papua-New Guinea",OK
    HNL,HNL,PHNL,PHNL,OK,1.0,International,"Honolulu (HI), USA",OWNO,HONOLULU INTL,"HONOLULU, HI - UNITED STATES",Honolulu International Airport,"Honolulu, Oahu, Hawaii, United States",OK
    HNM,HNM,PHHN,PHHN,OK,1.0,Hana,"Hana (HI), USA",OWNO,HANA,"HANA, HI - UNITED STATES",????,"Hana, Maui, Hawaii, United States",OK
    HNN,////,////,////,UNK,1.0,Honinabi,"Honinabi, Papua New Guinea",OWNO,////,////,????,"Honinabi, Western, Papua-New Guinea",OK
    HNS,HNS,PAHN,PAHN,OK,0.766,Municipal,"Haines (AK), USA",OWNO,HAINES,"HAINES, AK - UNITED STATES",????,"Haines, Alaska, United States",OK
    HNY,////,////,ZGHY,UNK,0.952,Hengyang,"Hengyang, PR China",OWNO,////,////,Hengyang Nanyue Airport,"Hengyang, Hunan, China",OK
    HOA,////,////,HKHO,UNK,1.0,Hola,"Hola, Kenya",OWNO,////,////,????,"Hola, Kenya",OK
    HOB,HOB,KHOB,KHOB,OK,0.748,Lea County,"Hobbs (NM), USA",OWNO,LEA COUNTY RGNL,"HOBBS, NM - UNITED STATES",Lea County Regional,"Hobbs, New Mexico, United States",OK
    HOC,////,////,////,UNK,1.0,Komako,"Komako, Papua New Guinea",OWNO,////,////,????,"Komako, Gulf, Papua-New Guinea",OK
    HOD,////,////,OYHD,UNK,0.714,Hodeidah Airport,"Hodeidah, Yemen",OWNO,////,////,Hodeida International Airport,"Hodeida, Yemen",OK
    HOE,////,////,////,UNK,1.0,Houeisay,"Houeisay, Lao PDR",OWNO,////,////,????,"Houeisay, Lao People's Democratic Republic (Laos)",OK
    HOF,////,////,OEAH,UNK,0.462,Al Hasa,"Hofuf, Saudi Arabia",OWNO,////,////,Al-Ahsa International Airport,"Al-Ahsa, Saudi Arabia",OK
    HOG,////,////,MUHG,UNK,0.878,Frank Pais,"Holguin, Cuba",OWNO,////,////,Frank País International Airport,"Holguín, Holguín, Cuba",OK
    HOH,////,////,LOIH,UNK,0.64,Hohenems,"Hohenems, Austria",OWNO,////,////,Hohenems-Dornbirn,"Hohenems, Vorarlberg, Austria",OK
    HOI,////,////,NTTO,UNK,0.69,Hao Island,"Hao Island, French Polynesia",OWNO,////,////,????,"Hao, Tuamotu Islands, French Polynesia",MAYBE
    HOK,////,////,YHOO,UNK,1.0,Hooker Creek,"Hooker Creek, Australia",OWNO,////,////,Hooker Creek Airport,"Lajamanu, Northern Territory, Australia",OK
    HOM,HOM,PAHO,PAHO,OK,1.0,Homer,"Homer (AK), USA",OWNO,HOMER,"HOMER, AK - UNITED STATES",????,"Homer, Alaska, United States",OK
    HON,HON,KHON,KHON,OK,0.661,Howes,"Huron (SD), USA",OWNO,HURON RGNL,"HURON, SD - UNITED STATES",Huron Regional,"Huron, South Dakota, United States",OK
    HOO,////,////,////,UNK,1.0,Nhon Co,"Quanduc, Viet Nam",OWNO,////,////,Nhon Co,"Quanduc, Vietnam",OK
    HOP,HOP,KHOP,KHOP,OK,1.0,Campbell AAF,"Hopkinsville (KY), USA",OWNO,CAMPBELL AAF (FORT CAMPBELL),"FORT CAMPBELL/HOPKINSVILLE, KY - UNITED STATES",Campbell AAF Airport,"Fort Campbell/Hopkinsville, Kentucky, United States",OK
    HOQ,////,////,EDQM,UNK,0.6,Hof,"Hof, Germany",OWNO,////,////,Plauen,"Hof, Bavaria, Germany",OK
    HOR,////,////,LPHR,UNK,1.0,Horta,"Horta, Portugal",OWNO,////,////,????,"Horta, Região Autónoma dos Açores (Azores), Portugal",OK
    HOS,////,////,SAHC,UNK,0.733,Oscar Reguera,"Chos Malal, Argentina",OWNO,////,////,????,"Chos Malal, Neuquén, Argentina",OK
    HOT,HOT,KHOT,KHOT,OK,1.0,Memorial Field,"Hot Springs (AR), USA",OWNO,MEMORIAL FIELD,"HOT SPRINGS, AR - UNITED STATES",Memorial Field,"Hot Springs, Arkansas, United States",OK
    HOU,HOU,KHOU,KHOU,OK,0.627,Hobby,"Houston (TX), USA",OWNO,WILLIAM P HOBBY,"HOUSTON, TX - UNITED STATES",William P Hobby Airport,"Houston, Texas, United States",OK
    HOV,////,////,ENOV,UNK,1.0,Hovden,"Orsta-Volda, Norway",OWNO,////,////,Hovden,"Orsta-Volda, Norway",OK
    HOX,////,////,VYHL,UNK,1.0,Homalin,"Homalin, Myanmar",OWNO,////,////,????,"Hommalinn, Sagaing, Myanmar (Burma)",OK
    HOY,////,////,////,UNK,0.526,Hoy Island,"Hoy Island, United Kingdom",OWNO,////,////,Longhope Airfield,"Hoy, Hoy Island, Orkney Isles, Scotland, United Kingdom",OK
    HPA,////,////,NFTL,UNK,0.645,Salote Pilolevu,"Ha'Apai, Tonga",OWNO,////,////,Lifuka Island Airport,"Lifuka, Ha'apai, Tonga",OK
    HPB,HPB,PAHP,PAHP,OK,1.0,Hooper Bay,"Hooper Bay (AK), USA",OWNO,HOOPER BAY,"HOOPER BAY, AK - UNITED STATES",????,"Hooper Bay, Alaska, United States",OK
    HPE,////,////,////,UNK,1.0,Hope Vale,"Hope Vale, Australia",OWNO,////,////,????,"Hope Vale, Queensland, Australia",OK
    HPG,////,////,ZHSN,UNK,nan,////,////,////,////,////,Shennongjia Hongping Airport,"Shennongjia, Hubei, China",UNK
    HPH,////,////,VVCI,UNK,0.727,Catbi,"Haiphong, Viet Nam",OWNO,////,////,Cat Bi International Airport,"Hai Phong, Vietnam",OK
    HPN,HPN,KHPN,KHPN,OK,0.867,Westchester County Apt,"Westchester County (NY), USA",OWNO,WESTCHESTER COUNTY,"WHITE PLAINS, NY - UNITED STATES",Westchester County Airport,"White Plains, New York, United States",OK
    HPT,HPT,KHPT,KHPT,OK,1.0,Municipal,"Hampton IA, USA",OWNO,HAMPTON MUNI,"HAMPTON, IA - UNITED STATES",Hampton Municipal Airport,"Hampton, Iowa, United States",OK
    HPV,HI01,////,////,OK,1.0,Princeville,"Kauai Island (HI), USA",OWNO,PRINCEVILLE,"HANALEI, HI - UNITED STATES",Princeville Airport,"Hanalei, Kauai, Hawaii, United States",OK
    HPY,HPY,KHPY,KHPY,OK,1.0,Baytown,"Baytown (TX), USA",OWNO,BAYTOWN,"BAYTOWN, TX - UNITED STATES",????,"Baytown, Texas, United States",OK
    HQM,HQM,KHQM,KHQM,OK,1.0,Bowerman Airport,"Hoquiam (WA), USA",OWNO,BOWERMAN,"HOQUIAM, WA - UNITED STATES",Bowerman Airport,"Hoquiam, Washington, United States",OK
    HRB,////,////,ZYHB,UNK,0.75,Harbin,"Harbin, PR China",OWNO,////,////,Harbin Taiping International Airport,"Harbin, Heilongjiang, China",OK
    HRE,////,////,FVHA,UNK,0.714,Harare,"Harare, Zimbabwe",OWNO,////,////,Harare International Airport,"Harare, Zimbabwe",OK
    HRG,////,////,HEGN,UNK,0.762,Hurghada,"Hurghada, Egypt",OWNO,////,////,Hurghada International Airport,"Hurghada, Al Bahr al Ahmar (Red Sea), Egypt",OK
    HRI,////,////,VCRI,UNK,nan,////,////,////,////,////,Mattala Rajapaksa International Airport,"Mattala, Hambantota, Southern Province, Sri Lanka (Ceylon)",UNK
    HRJ,////,////,VNCJ,UNK,1.0,Chaurjhari,"Chaurjhari, Nepal",OWNO,////,////,????,"Chaurjhari, Nepal",OK
    HRK,////,////,UKHH,UNK,0.571,Kharkov,"Kharkov, Ukraine",OWNO,////,////,Osnova International Airport,"Kharkiv, Kharkiv, Ukraine",OK
    HRL,HRL,KHRL,KHRL,OK,1.0,Valley International,"Harlingen (TX), USA",OWNO,VALLEY INTL,"HARLINGEN, TX - UNITED STATES",Valley International Airport,"Harlingen, Texas, United States",OK
    HRM,////,////,DAFH,UNK,1.0,Tilrempt,"Hassi R'Mel, Algeria",OWNO,////,////,Tilrempt,"Hassi R'Mel, Laghouat, Algeria",OK
    HRN,////,////,////,UNK,1.0,Heliport,"Heron Island, Australia",OWNO,////,////,Heliport,"Heron Island, Queensland, Australia",OK
    HRO,HRO,KHRO,KHRO,OK,1.0,Boone County,"Harrison (AR), USA",OWNO,BOONE COUNTY,"HARRISON, AR - UNITED STATES",Boone County Airport,"Harrison, Arkansas, United States",OK
    HRS,////,////,FAHR,UNK,1.0,Harrismith Airport,"Harrismith, South Africa",OWNO,////,////,????,"Harrismith, Free State, South Africa",OK
    HRT,////,////,EGXU,UNK,0.875,Linton-On-Ouse,"Harrogate, United Kingdom",OWNO,////,////,RAF Linton-on-Ouse,"Linton-on-Ouse, Yorkshire, England, United Kingdom",OK
    HRY,////,////,YHBY,UNK,1.0,Henbury,"Henbury, Australia",OWNO,////,////,????,"Henbury, Northern Territory, Australia",OK
    HSB,HSB,KHSB,KHSB,OK,1.0,Raleigh,"Harrisburg (IL), USA",OWNO,HARRISBURG-RALEIGH,"HARRISBURG, IL - UNITED STATES",Harrisburg-Raleigh Airport,"Harrisburg, Illinois, United States",OK
    HSC,////,////,////,UNK,0.897,Shaoguan,"Shaoguan, PR China",OWNO,////,////,Shaoguan Guitou Airport,"Shaoguan, Guangdong, China",OK
    HSG,////,////,RJFS,UNK,1.0,Saga,"Saga, Japan",OWNO,////,////,Saga Airport,"Saga, Saga, Japan",OK
    HSH,HND,KHND,KHND,OK,0.596,Henderson Sky Harbor,"Las Vegas (NV), USA",OWNO,HENDERSON EXECUTIVE,"LAS VEGAS, NV - UNITED STATES",Henderson Executive Airport,"Las Vegas, Nevada, United States",OK
    HSI,HSI,KHSI,KHSI,OK,0.778,Hastings,"Hastings (NE), USA",OWNO,HASTINGS MUNI,"HASTINGS, NE - UNITED STATES",Hastings Municipal Airport,"Hastings, Nebraska, United States",OK
    HSK,////,////,LEHC,UNK,nan,////,////,////,////,////,Huesca-Pirineos,"Huesca, Aragón, Spain",UNK
    HSL,HLA,PAHL,PAHL,OK,1.0,Huslia,"Huslia (AK), USA",OWNO,HUSLIA,"HUSLIA, AK - UNITED STATES",????,"Huslia, Alaska, United States",OK
    HSM,////,////,YHSM,UNK,1.0,Horsham,"Horsham, Australia",OWNO,////,////,????,"Horsham, Victoria, Australia",OK
    HSN,////,////,ZSZS,UNK,1.0,Zhoushan,"Zhoushan, PR China",OWNO,////,////,????,"Zhoushan, Zhejiang, China",OK
    HSP,HSP,KHSP,KHSP,OK,1.0,Ingalls Field,"Hot Springs (VA), USA",OWNO,INGALLS FIELD,"HOT SPRINGS, VA - UNITED STATES",Ingalls Field,"Hot Springs, Virginia, United States",OK
    HSS,////,////,VIHR,UNK,1.0,Hissar,"Hissar, India",OWNO,////,////,????,"Hissar, Haryana, India",OK
    HST,HST,KHST,KHST,OK,0.906,AFB,"Homestead (FL), USA",OWNO,HOMESTEAD ARB,"HOMESTEAD, FL - UNITED STATES",Homestead ARB,"Homestead, Florida, United States",OK
    HSV,HSV,KHSV,KHSV,OK,0.5,Madison County,"Huntsville (AL), USA",OWNO,HUNTSVILLE INTL-CARL T JONES FIELD,"HUNTSVILLE, AL - UNITED STATES",Huntsville International-Carl T Jones Field,"Huntsville, Alabama, United States",OK


List for checking certainty of Great Circle Mapper (ARZ - AZS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ARZ,////,////,FNZE,UNK,1.0,N'Zeto,"N'Zeto, Angola",OWNO,////,////,????,"N'zeto, Angola",OK
    ASA,////,////,HHSB,UNK,1.0,Assab International Airport,"Assab, Eritrea",OWNO,////,////,Assab International Airport,"Assab, Eritrea",OK
    ASB,////,////,UTAA,UNK,0.778,Ashgabat Airport,"Ashgabat, Turkmenistan",OWNO,////,////,Ashgabat International Airport,"Ashgabat, Ahal, Turkmenistan",OK
    ASC,////,////,SLAS,UNK,0.727,Ascension,"Ascension, Bolivia",OWNO,////,////,Ascensión de Guarayos Airport,"Ascensión de Guarayos, Guarayos, Santa Cruz, Bolivia",MAYBE
    ASD,////,MYAF,MYAF,OK,0.145,Andros Town International Airport,"Andros Town, Bahamas",OWNO,FRESH CREEK,"FRESH CREEK, - BAHAMAS",Andros Town International Airport,"Andros Town, Andros Island, Bahamas",TO DO CHECK
    ASE,ASE,KASE,KASE,OK,0.805,Aspen-Pitkin County Airport,"Aspen (CO), USA",OWNO,ASPEN-PITKIN CO/SARDY FIELD,"ASPEN, CO - UNITED STATES",Aspen-Pitkin Co./Sardy Field,"Aspen, Colorado, United States",OK
    ASF,////,////,URWA,UNK,0.667,Astrakhan,"Astrakhan, Russia",OWNO,////,////,Narimanovo Airport,"Astrakhan, Astrakhanskaya, Russian Federation (Russia)",OK
    ASG,////,////,NZAS,UNK,1.0,Ashburton,"Ashburton, New Zealand",OWNO,////,////,????,"Ashburton, New Zealand",OK
    ASH,ASH,KASH,KASH,OK,1.0,Boire Field,"Nashua (NH), USA",OWNO,BOIRE FIELD,"NASHUA, NH - UNITED STATES",Boire Field,"Nashua, New Hampshire, United States",OK
    ASI,////,////,FHAW,UNK,1.0,Wideawake Field,"Georgetown, St. Helena",OWNO,////,////,Wideawake Field,"Georgetown, Ascension, St. Helena",OK
    ASJ,////,////,RJKA,UNK,1.0,Amami-Oshima Airport,"Amami Oshima Is, Japan",OWNO,////,////,Amami Airport,"Amami, Amami Oshima, Satsunan Islands, Kagoshima, Japan",OK
    ASK,////,////,DIYO,UNK,1.0,Yamoussoukro,"Yamoussoukro, Cote d'Ivoire",OWNO,////,////,????,"Yamoussoukro, Lacs, Côte d'Ivoire (Ivory Coast)",OK
    ASL,ASL,KASL,KASL,OK,1.0,Harrison County,"Marshall (TX), USA",OWNO,HARRISON COUNTY,"MARSHALL, TX - UNITED STATES",Harrison County Airport,"Marshall, Texas, United States",OK
    ASM,////,////,HHAS,UNK,1.0,Asmara International Airport,"Asmara, Eritrea",OWNO,////,////,Asmara International Airport,"Asmara, Eritrea",OK
    ASN,ASN,KASN,KASN,OK,0.766,Talladega,"Talladega (AL), USA",OWNO,TALLADEGA MUNI,"TALLADEGA, AL - UNITED STATES",Talladega Municipal Airport,"Talladega, Alabama, United States",OK
    ASO,////,////,HASO,UNK,1.0,Asosa,"Asosa, Ethiopia",OWNO,////,////,Asosa Airport,"Asosa, Benshangul-Gumaz, Ethiopia",OK
    ASP,////,////,YBAS,UNK,1.0,Alice Springs,"Alice Springs, Australia",OWNO,////,////,????,"Alice Springs, Northern Territory, Australia",OK
    ASQ,TMT,KTMT,KTMT,OK,1.0,Austin,"Austin (NV), USA",OWNO,AUSTIN,"AUSTIN, NV - UNITED STATES",????,"Austin, Nevada, United States",OK
    ASR,////,////,LTAU,UNK,0.56,Kayseri,"Kayseri, Turkey",OWNO,////,////,Erkilet AB,"Kayseri, Kayseri (Kirklareli), Turkey",OK
    AST,AST,KAST,KAST,OK,0.734,Astoria,"Astoria (OR), USA",OWNO,ASTORIA RGNL,"ASTORIA, OR - UNITED STATES",Astoria Regional,"Astoria, Oregon, United States",OK
    ASU,////,////,SGAS,UNK,1.0,Silvio Pettirossi International Airport,"Asuncion, Paraguay",OWNO,////,////,Silvio Pettirossi International Airport,"Asunción, Central, Paraguay",OK
    ASV,////,////,HKAM,UNK,1.0,Amboseli,"Amboseli, Kenya",OWNO,////,////,????,"Amboseli, Kenya",OK
    ASW,////,////,HESN,UNK,1.0,Aswan International Airport,"Aswan, Egypt",OWNO,////,////,Aswan International Airport,"Aswan, Aswan, Egypt",OK
    ASX,ASX,KASX,KASX,OK,0.407,Ashland,"Ashland (WI), USA",OWNO,JOHN F KENNEDY MEMORIAL,"ASHLAND, WI - UNITED STATES",John F Kennedy Memorial Airport,"Ashland, Wisconsin, United States",OK
    ASY,ASY,KASY,KASY,OK,0.781,Ashley,"Ashley (ND), USA",OWNO,ASHLEY MUNI,"ASHLEY, ND - UNITED STATES",Ashley Municipal Airport,"Ashley, North Dakota, United States",OK
    ASZ,////,////,////,UNK,1.0,Asirim,"Asirim, Papua New Guinea",OWNO,////,////,????,"Asirim, West New Britain, Papua-New Guinea",OK
    ATA,////,////,SPHZ,UNK,0.195,Anta,"Anta, Peru",OWNO,////,////,Commandante FAP Germán Arias Graziani Airport,"Anta, Ancash, Perú",OK
    ATB,////,////,HSAT,UNK,1.0,Atbara,"Atbara, Sudan",OWNO,////,////,????,"Atbara, Northern, Sudan",OK
    ATC,////,MYCA,MYCA,OK,1.0,Arthur's Town,"Arthur's Town, Bahamas",OWNO,ARTHURS TOWN,"ARTHURS TOWN, - BAHAMAS",????,"Arthur's Town, Cat Island, Bahamas",OK
    ATD,////,////,AGAT,UNK,0.533,Atoifi,"Atoifi, Solomon Islands",OWNO,////,////,Uru Harbour,"Atoifi, Malaita, Solomon Islands",OK
    ATE,80F,////,////,OK,0.79,Antlers,"Antlers (OK), USA",OWNO,ANTLERS MUNI,"ANTLERS, OK - UNITED STATES",Antlers Municipal Airport,"Antlers, Oklahoma, United States",OK
    ATF,////,////,SEAM,UNK,1.0,Chachoan,"Ambato, Ecuador",OWNO,////,////,Chachoan,"Ambato, Tungurahua, Ecuador",OK
    ATG,////,////,OPMS,UNK,0.615,Attock,"Attock, Pakistan",OWNO,////,////,Minhas AB,"Attock, Punjab, Pakistan",OK
    ATH,////,////,LGAV,UNK,1.0,Eleftherios Venizelos International Airport,"Athens, Greece",OWNO,////,////,Eleftherios Venizelos International Airport,"Athens, Attikí (Attica), Greece",OK
    ATI,////,////,SUAG,UNK,0.737,Artigas,"Artigas, Uruguay",OWNO,////,////,Artigas International Airport,"Artigas, Artigas, Uruguay",OK
    ATJ,////,////,FMME,UNK,1.0,Antsirabe,"Antsirabe, Madagascar",OWNO,////,////,????,"Antsirabe, Madagascar",OK
    ATK,ATK,PATQ,PATQ,OK,0.284,Atqasuk,"Atqasuk (AK), USA",OWNO,ATQASUK EDWARD BURNELL SR MEMORIAL,"ATQASUK, AK - UNITED STATES",Atqasuk Edward Burnell Sr Memorial Airport,"Atqasuk, Alaska, United States",OK
    ATL,ATL,KATL,KATL,OK,1.0,Hartsfield-Jackson Atlanta International Airport,"Atlanta (GA), USA",OWNO,HARTSFIELD - JACKSON ATLANTA INTL,"ATLANTA, GA - UNITED STATES",Hartsfield - Jackson Atlanta International Airport,"Atlanta, Georgia, United States",OK
    ATM,////,////,SBHT,UNK,1.0,Altamira,"Altamira, Brazil",OWNO,////,////,????,"Altamira, Pará, Brazil",OK
    ATN,////,////,AYNX,UNK,1.0,Namatanai,"Namatanai, Papua New Guinea",OWNO,////,////,Namatanai Airport,"Namatanai, New Ireland, Papua-New Guinea",OK
    ATO,UNI,KUNI,KUNI,OK,1.0,Ohio University,"Athens (OH), USA",OWNO,OHIO UNIVERSITY,"ATHENS/ALBANY, OH - UNITED STATES",Ohio University Airport,"Athens/Albany, Ohio, United States",OK
    ATP,////,////,////,UNK,0.692,Metropolitan Area,"Aitape, Papua New Guinea",OWNO,////,////,Airstrip,"Aitape, Sandaun, Papua-New Guinea",OK
    ATQ,////,////,VIAR,UNK,0.772,Sri Guru Ram Dass Jee International Airport,"Amritsar, Punjab, India",OWNO,////,////,Raja Sansi International Airport,"Amritsar, Punjab, India",OK
    ATR,////,////,GQPA,UNK,0.706,Atar Mouakchott,"Atar, Mauritania",OWNO,////,////,Atar International Airport,"Atar, Mauritania",OK
    ATS,ATS,KATS,KATS,OK,0.543,Artesia,"Artesia (NM), USA",OWNO,ARTESIA MUNI,"ARTESIA, NM - UNITED STATES",Artesia Municipal Airport,"Artesia, New Mexico, United States",OK
    ATT,4A2,////,////,OK,1.0,Atmautluak,"Atmautluak (AK), USA",OWNO,ATMAUTLUAK,"ATMAUTLUAK, AK - UNITED STATES",????,"Atmautluak, Alaska, United States",OK
    ATU,ATU,PAAT,PAAT,OK,0.815,Casco Cove,"Attu Island (AK), USA",OWNO,CASCO COVE CGS,"ATTU, AK - UNITED STATES",Casco Cove CGS Airport,"Attu, Alaska, United States",OK
    ATV,////,////,FTTI,UNK,1.0,Ati,"Ati, Chad",OWNO,////,////,????,"Ati, Batha, Chad",OK
    ATW,ATW,KATW,KATW,OK,0.64,Outagamie County,"Appleton (WI), USA",OWNO,APPLETON INTL,"APPLETON, WI - UNITED STATES",Appleton International Airport,"Appleton, Wisconsin, United States",OK
    ATX,////,////,////,UNK,1.0,Atbasar,"Atbasar, Kazakhstan",OWNO,////,////,Atbasar Airport,"Atbasar, Aqmola, Kazakhstan",OK
    ATY,ATY,KATY,KATY,OK,0.81,Watertown,"Watertown (SD), USA",OWNO,WATERTOWN RGNL,"WATERTOWN, SD - UNITED STATES",Watertown Regional,"Watertown, South Dakota, United States",OK
    ATZ,////,////,HEAT,UNK,1.0,Assiut,"Assiut, Egypt",OWNO,////,////,????,"Assiut, Al Wadi al Jadid (New Valley), Egypt",OK
    AUA,////,////,TNCA,UNK,0.944,Queen Beatrix International Airport (Reina Beatrix),"Aruba, Aruba",OWNO,////,////,Reina Beatrix International Airport,"Aruba, Aruba",OK
    AUC,////,////,SKUC,UNK,0.353,Arauca,"Arauca, Colombia",OWNO,////,////,Santiago Pérez Quiroz Airport,"Arauca, Arauca, Colombia",OK
    AUD,////,////,YAGD,UNK,1.0,Augustus Downs,"Augustus Downs, Australia",OWNO,////,////,????,"Augustus Downs, Queensland, Australia",OK
    AUE,////,////,////,UNK,1.0,Abu Rudeis,"Abu Rudeis, Egypt",OWNO,////,////,????,"Abu Rudeis, Janub Sina (South Sinai), Egypt",OK
    AUF,////,////,LFLA,UNK,1.0,Auxerre Branches,"Auxerre, France",OWNO,////,////,Branches,"Auxerre, Bourgogne (Burgundy), France",OK
    AUG,AUG,KAUG,KAUG,OK,0.661,Augusta,"Augusta (ME), USA",OWNO,AUGUSTA STATE,"AUGUSTA, ME - UNITED STATES",Augusta State Airport,"Augusta, Maine, United States",OK
    AUH,////,////,OMAA,UNK,1.0,Abu Dhabi International,"Abu Dhabi, United Arab Emirates",OWNO,////,////,Abu Dhabi International Airport,"Abu Dhabi, Abu Dhabi, United Arab Emirates",OK
    AUI,////,////,////,UNK,1.0,Aua Island,"Aua Island, Papua New Guinea",OWNO,////,////,????,"Aua Island, Manus, Papua-New Guinea",OK
    AUJ,////,////,////,UNK,1.0,Ambunti,"Ambunti, Papua New Guinea",OWNO,////,////,????,"Ambunti, East Sepik, Papua-New Guinea",OK
    AUK,AUK,PAUK,PAUK,OK,1.0,Alakanuk,"Alakanuk (AK), USA",OWNO,ALAKANUK,"ALAKANUK, AK - UNITED STATES",????,"Alakanuk, Alaska, United States",OK
    AUL,////,////,////,UNK,1.0,Aur Island,"Aur Island, Marshall Islands",OWNO,////,////,????,"Aur Island, Marshall Islands",OK
    AUM,AUM,KAUM,KAUM,OK,0.748,Austin,"Austin (MN), USA",OWNO,AUSTIN MUNI,"AUSTIN, MN - UNITED STATES",Austin Municipal Airport,"Austin, Minnesota, United States",OK
    AUN,AUN,KAUN,KAUN,OK,0.76,Auburn,"Auburn (CA), USA",OWNO,AUBURN MUNI,"AUBURN, CA - UNITED STATES",Auburn Municipal Airport,"Auburn, California, United States",OK
    AUO,AUO,KAUO,KAUO,OK,0.428,Auburn-Opelika,"Auburn (AL), USA",OWNO,AUBURN UNIVERSITY RGNL,"AUBURN, AL - UNITED STATES",Auburn University Regional,"Auburn, Alabama, United States",OK
    AUP,////,////,////,UNK,1.0,Agaun,"Agaun, Papua New Guinea",OWNO,////,////,????,"Agaun, Milne Bay, Papua-New Guinea",OK
    AUQ,////,////,////,UNK,1.0,Atuona,"Atuona, French Polynesia",OWNO,////,////,????,"Atuona, Marquesas Islands, French Polynesia",OK
    AUR,////,////,LFLW,UNK,1.0,Aurillac,"Aurillac, France",OWNO,////,////,Aurillac Airport,"Aurillac, Auvergne, France",OK
    AUS,AUS,KAUS,KAUS,OK,1.0,Austin-Bergstrom International,"Austin (TX), USA",OWNO,AUSTIN-BERGSTROM INTL,"AUSTIN, TX - UNITED STATES",Austin-Bergstrom International Airport,"Austin, Texas, United States",OK
    AUT,////,////,WPAT,UNK,0.632,Atauro,"Atauro, Indonesia",OWNO,////,////,????,"Atauro Island, Timor-Leste (East Timor)",MAYBE
    AUU,////,////,YAUR,UNK,1.0,Aurukun Mission,"Aurukun Mission, Australia",OWNO,////,////,????,"Aurukun Mission, Queensland, Australia",OK
    AUW,AUW,KAUW,KAUW,OK,0.64,Municipal,"Wausau (WI), USA",OWNO,WAUSAU DOWNTOWN,"WAUSAU, WI - UNITED STATES",Wausau Downtown Airport,"Wausau, Wisconsin, United States",OK
    AUX,////,////,SWGN,UNK,1.0,Araguaina,"Araguaina, Brazil",OWNO,////,////,????,"Araguaina, Tocantins, Brazil",OK
    AUY,////,////,NVVA,UNK,0.667,Aneityum,"Aneityum, Vanuatu",OWNO,////,////,????,"Anelghowhat, Anatom (Aneityum, Kéamu) Island, Taféa, Vanuatu",MAYBE
    AUZ,ARR,KARR,KARR,OK,1.0,Municipal Airport,"Aurora (IL), USA",OWNO,AURORA MUNI,"CHICAGO/AURORA, IL - UNITED STATES",Aurora Municipal Airport,"Chicago/Aurora, Illinois, United States",OK
    AVA,////,////,ZUAS,UNK,nan,////,////,////,////,////,Anshun Huangguoshu Airport,"Anshun, Guizhou, China",UNK
    AVB,////,////,LIPA,UNK,0.8,Aviano,"Aviano, Italy",OWNO,////,////,Aviano AB,"Aviano, Friuli-Venezia Giulia, Italy",OK
    AVG,////,////,YAUV,UNK,1.0,Auvergne,"Auvergne, Australia",OWNO,////,////,????,"Auvergne, Northern Territory, Australia",OK
    AVI,////,////,MUCA,UNK,1.0,Maximo Gomez,"Ciego De Avila, Cuba",OWNO,////,////,Máximo Gómez,"Ciego de Ávila, Ciego de Ávila, Cuba",OK
    AVK,////,////,ZMAH,UNK,1.0,Arvaikheer,"Arvaikheer, Mongolia",OWNO,////,////,????,"Arvaikheer, Mongolia",OK
    AVL,AVL,KAVL,KAVL,OK,0.529,Hendersonville,"Asheville (NC), USA",OWNO,ASHEVILLE RGNL,"ASHEVILLE, NC - UNITED STATES",Asheville Regional,"Asheville, North Carolina, United States",OK
    AVN,////,////,LFMV,UNK,0.909,Avignon-Caum,"Avignon, France",OWNO,////,////,Caumont,"Avignon, Provence-Alpes-Côte d'Azur, France",OK
    AVO,AVO,KAVO,KAVO,OK,0.529,Municipal,"Avon Park (FL), USA",OWNO,AVON PARK EXECUTIVE,"AVON PARK, FL - UNITED STATES",Avon Park Executive Airport,"Avon Park, Florida, United States",OK
    AVP,AVP,KAVP,KAVP,OK,1.0,Wilkes-Barre International,"Scranton (PA), USA",OWNO,WILKES-BARRE/SCRANTON INTL,"WILKES-BARRE/SCRANTON, PA - UNITED STATES",Wilkes-Barre/Scranton International Airport,"Wilkes-Barre/Scranton, Pennsylvania, United States",OK
    AVV,////,////,YMAV,UNK,1.0,Avalon,"Avalon, Australia",OWNO,////,////,Avalon Airport,"Lara, Victoria, Australia",OK
    AVW,AVQ,KAVQ,KAVQ,OK,0.504,Avra Valley,"Tucson (AZ), USA",OWNO,MARANA RGNL,"MARANA, AZ - UNITED STATES",Marana Regional,"Marana, Arizona, United States",OK
    AWA,////,////,HALA,UNK,1.0,Awassa,"Awassa, Ethiopia",OWNO,////,////,????,"Awassa, SNNPR, Ethiopia",OK
    AWB,////,////,AYAW,UNK,1.0,Awaba,"Awaba, Papua New Guinea",OWNO,////,////,????,"Awaba, Western, Papua-New Guinea",OK
    AWD,////,////,NVVB,UNK,1.0,Aniwa,"Aniwa, Vanuatu",OWNO,////,////,????,"Aniwa, Taféa, Vanuatu",OK
    AWK,AWK,PWAK,PWAK,OK,0.504,Wake Island,"Wake Island, US Minor Outlying Is",OWNO,WAKE ISLAND AIRFIELD,"WAKE ISLAND, WQ - UNITED STATES",Wake Island Airfield,"Wake Island, United States Minor Outlying Islands",OK
    AWM,AWM,KAWM,KAWM,OK,1.0,Municipal,"West Memphis (AR), USA",OWNO,WEST MEMPHIS MUNI,"WEST MEMPHIS, AR - UNITED STATES",West Memphis Municipal Airport,"West Memphis, Arkansas, United States",OK
    AWZ,////,////,OIAW,UNK,1.0,Ahwaz,"Ahwaz, Iran",OWNO,////,////,????,"Ahwaz, Khuzestan, Iran",OK
    AXA,////,TQPF,TQPF,OK,0.206,Wallblake,"Anguilla, Anguilla",OWNO,CLAYTON J LLOYD INTL,"THE VALLEY, - ANGUILLA",Clayton J. Lloyd International Airport,"The Valley, Anguilla, Anguilla",TO DO CHECK
    AXD,////,////,LGAL,UNK,1.0,Demokritos Airport,"Alexandroupolis, Greece",OWNO,////,////,Dimokritos,"Alexandroupolis, Anatolikí Makedonía kai Thráki (Eastern Macedonia and Thrace), Greece",OK
    AXG,AXA,KAXA,KAXA,OK,0.664,Algona,"Algona IA, USA",OWNO,ALGONA MUNI,"ALGONA, IA - UNITED STATES",Algona Municipal Airport,"Algona, Iowa, United States",OK
    AXK,////,////,OYAT,UNK,1.0,Ataq,"Ataq, Yemen",OWNO,////,////,????,"Ataq, Yemen",OK
    AXM,////,////,SKAR,UNK,0.857,El Eden,"Armenia, Colombia",OWNO,////,////,El Edén International Airport,"Armenia, Quindío, Colombia",OK
    AXN,AXN,KAXN,KAXN,OK,0.554,Alexandria,"Alexandria (MN), USA",OWNO,CHANDLER FIELD,"ALEXANDRIA, MN - UNITED STATES",Chandler Field,"Alexandria, Minnesota, United States",OK
    AXP,////,MYAP,MYAP,OK,0.348,Springpoint Airport,"Spring Point, Bahamas",OWNO,SPRING POINT,"SPRING POINT, - BAHAMAS",Spring Point Municipal Airport,"Spring Point, Acklins Island, Bahamas",OK
    AXR,////,////,NTGU,UNK,1.0,Arutua,"Arutua, French Polynesia",OWNO,////,////,????,"Arutua, French Polynesia",OK
    AXS,AXS,KAXS,KAXS,OK,0.757,Municipal,"Altus (OK), USA",OWNO,ALTUS/QUARTZ MOUNTAIN RGNL,"ALTUS, OK - UNITED STATES",Altus/Quartz Mountain Regional,"Altus, Oklahoma, United States",OK
    AXT,////,////,RJSK,UNK,1.0,Akita,"Akita, Japan",OWNO,////,////,Akita Airport,"Akita, Akita, Japan",OK
    AXU,////,////,HAAX,UNK,1.0,Axum,"Axum, Ethiopia",OWNO,////,////,????,"Axum, Tigray, Ethiopia",OK
    AXV,AXV,KAXV,KAXV,OK,1.0,Neil Armstrong,"Wapakoneta (OH), USA",OWNO,NEIL ARMSTRONG,"WAPAKONETA, OH - UNITED STATES",Neil Armstrong Airport,"Wapakoneta, Ohio, United States",OK
    AXX,AXX,KAXX,KAXX,OK,1.0,Angel Fire,"Angel Fire (NM), USA",OWNO,ANGEL FIRE,"ANGEL FIRE, NM - UNITED STATES",????,"Angel Fire, New Mexico, United States",OK
    AYK,////,////,UAUR,UNK,1.0,Arkalyk,"Arkalyk, Kazakhstan",OWNO,////,////,Arkalyk Airport,"Arkalyk, Qostanay, Kazakhstan",OK
    AYO,////,////,SGAY,UNK,0.6,Ayolas,"Ayolas, Paraguay",OWNO,////,////,Juan de Ayolas Airport,"Ayolas, Misiones, Paraguay",OK
    AYP,////,////,SPHO,UNK,0.419,Yanamilla,"Ayacucho, Peru",OWNO,////,////,Coronel FAP Alfredo Mendivil Duarte,"Ayacucho, Ayacucho, Perú",OK
    AYQ,////,////,YAYE,UNK,0.696,Connellan,"Ayers Rock, Australia",OWNO,////,////,Ayers Rock Airport,"Yulara, Northern Territory, Australia",OK
    AYS,AYS,KAYS,KAYS,OK,1.0,Ware County,"Waycross (GA), USA",OWNO,WAYCROSS-WARE COUNTY,"WAYCROSS, GA - UNITED STATES",Waycross-Ware County Airport,"Waycross, Georgia, United States",OK
    AYT,////,////,LTAI,UNK,1.0,Antalya,"Antalya, Turkey",OWNO,////,////,????,"Antalya, Antalya, Turkey",OK
    AZA,IWA,KIWA,KIWA,OK,1.0,////,////,////,PHOENIX-MESA GATEWAY,"PHOENIX, AZ - UNITED STATES",Phoenix-Mesa Gateway Airport,"Phoenix, Arizona, United States",OK
    AZD,////,////,OIYY,UNK,0.462,Yazd,"Yazd, Iran",OWNO,////,////,Shahid Sadooghi,"Yazd, Yazd, Iran",OK
    AZI,////,////,OMAD,UNK,0.792,Al Bateen Airport,"Abu Dhabi, United Arab Emirates",OWNO,////,////,Al Bateen Executive Airport,"Abu Dhabi, Abu Dhabi, United Arab Emirates",OK
    AZN,////,////,UTKA,UNK,1.0,Andizhan,"Andizhan, Uzbekistan",OWNO,////,////,Andizhan Airport,"Andizhan, Andijon, Uzbekistan",OK
    AZO,AZO,KAZO,KAZO,OK,1.0,Battle Creek International,"Kalamazoo (MI), USA",OWNO,KALAMAZOO/BATTLE CREEK INTL,"KALAMAZOO, MI - UNITED STATES",Kalamazoo/Battle Creek International Airport,"Kalamazoo, Michigan, United States",OK
    AZR,////,////,DAUA,UNK,0.294,Adrar,"Adrar, Algeria",OWNO,////,////,Touat-Cheikh Sidi Mohamed Belkebir Airport,"Adrar, Adrar, Algeria",OK
    AZS,////,////,MDCY,UNK,1.0,Samaná El Catey International Airport,"El Catey, Dominican Republic",OWNO,////,////,Samaná El Catey International Airport,"Samaná, María Trinidad Sánchez, Dominican Republic",OK

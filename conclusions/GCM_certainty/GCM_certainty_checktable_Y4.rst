
List for checking certainty of Great Circle Mapper (YUA - YZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    YUA,////,////,ZPYM,UNK,1.0,Yuanmou,"Yuanmou, PR China",OWNO,////,////,Yuanmou Airport,"Yuanmou, Yunnan, China",OK
    YUB,////,////,CYUB,UNK,0.645,Tuktoyaktuk,"Tuktoyaktuk, Canada",OWNO,////,////,James Gruben Airport,"Tuktoyaktuk, Northwest Territories, Canada",OK
    YUD,////,////,CYMU,UNK,1.0,Umiujaq,"Umiujaq, Canada",OWNO,////,////,????,"Umiujaq, Québec, Canada",OK
    YUE,////,////,YYND,UNK,1.0,Yuendumu,"Yuendumu, Australia",OWNO,////,////,????,"Yuendumu, Northern Territory, Australia",OK
    YUL,////,////,CYUL,UNK,0.556,Dorval,"Montreal, Canada",OWNO,////,////,Pierre Elliott Trudeau International Airport,"Montréal, Québec, Canada",OK
    YUM,NYL,KNYL,KNYL,OK,1.0,International,"Yuma (AZ), USA",OWNO,YUMA MCAS/YUMA INTL,"YUMA, AZ - UNITED STATES",Yuma MCAS/Yuma International Airport,"Yuma, Arizona, United States",OK
    YUS,////,////,ZLYS,UNK,nan,////,////,////,////,////,Yushu Batang Airport,"Yushu, Qinghai, China",UNK
    YUT,////,////,CYUT,UNK,1.0,Repulse Bay,"Repulse Bay, Canada",OWNO,////,////,????,"Repulse Bay, Kitikmeot, Northwest Territories, Canada",OK
    YUX,////,////,CYUX,UNK,1.0,Hall Beach,"Hall Beach, Canada",OWNO,////,////,????,"Hall Beach, Nunavut, Canada",OK
    YUY,////,CYUY,CYUY,OK,0.309,Rouyn,"Rouyn, Canada",OWNO,ROUYN-NORANDA,"ROUYN-NORANDA, - CANADA",????,"Rouyn-Noranda, Québec, Canada",TO DO CHECK
    YVA,////,////,FMCN,UNK,1.0,Iconi,"Moroni, Comoros",OWNO,////,////,Iconi,"Moroni, Comoros",OK
    YVB,////,////,CYVB,UNK,1.0,Bonaventure,"Bonaventure, Canada",OWNO,////,////,????,"Bonaventure, Québec, Canada",OK
    YVC,////,////,CYVC,UNK,0.552,La Ronge,"La Ronge, Canada",OWNO,////,////,Barber Field,"La Ronge, Saskatchewan, Canada",OK
    YVD,////,////,////,UNK,1.0,Yeva,"Yeva, Papua New Guinea",OWNO,////,////,????,"Yeva, Gulf, Papua-New Guinea",OK
    YVE,////,////,CYVK,UNK,1.0,Vernon,"Vernon, Canada",OWNO,////,////,????,"Vernon, British Columbia, Canada",OK
    YVG,////,////,CYVG,UNK,1.0,Vermilion,"Vermilion, Canada",OWNO,////,////,????,"Vermilion, Alberta, Canada",OK
    YVM,////,////,CYVM,UNK,1.0,Qikiqtarjuaq,"Qikiqtarjuaq, Canada",OWNO,////,////,????,"Qikiqtarjuaq, Qikiqtaaluk, Nunavut, Canada",OK
    YVO,////,CYVO,CYVO,OK,1.0,Val D'Or,"Val D'Or, Canada",OWNO,VAL-D'OR,"VAL-D'OR, - CANADA",????,"Val d'Or, Québec, Canada",OK
    YVP,////,////,CYVP,UNK,1.0,Kuujjuaq,"Kuujjuaq, Canada",OWNO,////,////,????,"Kuujjuaq, Québec, Canada",OK
    YVQ,////,////,CYVQ,UNK,1.0,Norman Wells,"Norman Wells, Canada",OWNO,////,////,????,"Norman Wells, Northwest Territories, Canada",OK
    YVR,////,CYVR,CYVR,OK,1.0,Vancouver International,"Vancouver, Canada",OWNO,VANCOUVER INTL,"VANCOUVER, - CANADA",Vancouver International Airport,"Vancouver, British Columbia, Canada",OK
    YVT,////,////,CYVT,UNK,1.0,Buffalo Narrows,"Buffalo Narrows, Canada",OWNO,////,////,????,"Buffalo Narrows, Saskatchewan, Canada",OK
    YVV,////,CYVV,CYVV,OK,1.0,Wiarton,"Wiarton, Canada",OWNO,WIARTON,"WIARTON, - CANADA",????,"Wiarton, Ontario, Canada",OK
    YVZ,////,////,CYVZ,UNK,1.0,Deer Lake,"Deer Lake, Canada",OWNO,////,////,????,"Deer Lake, Ontario, Canada",OK
    YWA,////,////,CYWA,UNK,1.0,Petawawa,"Petawawa, Canada",OWNO,////,////,????,"Petawawa, Ontario, Canada",OK
    YWB,////,////,CYKG,UNK,0.759,Kangiqsujuaq,"Kangiqsujuaq, Canada",OWNO,////,////,Wakeham Bay,"Kangiqsujuaq, Québec, Canada",OK
    YWG,////,////,CYWG,UNK,0.778,Winnipeg,"Winnipeg, Canada",OWNO,////,////,Winnipeg International Airport,"Winnipeg, Manitoba, Canada",OK
    YWH,////,////,CYWH,UNK,1.0,Victoria Inner Harbor,"Victoria, Canada",OWNO,////,////,Victoria Inner Harbour Airport,"Victoria, British Columbia, Canada",OK
    YWJ,////,////,CYWJ,UNK,1.0,Deline,"Deline, Canada",OWNO,////,////,????,"Deline, Northwest Territories, Canada",OK
    YWK,////,////,CYWK,UNK,1.0,Wabush,"Wabush, Canada",OWNO,////,////,Wabush Airport,"Wabush, Newfoundland and Labrador, Canada",OK
    YWL,////,////,CYWL,UNK,1.0,Williams Lake,"Williams Lake, Canada",OWNO,////,////,????,"Williams Lake, British Columbia, Canada",OK
    YWM,////,////,////,UNK,1.0,Williams Harbour,"Williams Harbour, Canada",OWNO,////,////,????,"Williams Harbour, Newfoundland and Labrador, Canada",OK
    YWP,////,////,CYWP,UNK,1.0,Webequie,"Webequie, Canada",OWNO,////,////,????,"Webequie, Ontario, Canada",OK
    YWR,////,////,////,UNK,0.786,White River,"White River, Canada",OWNO,////,////,White River Water Aerodrome,"White River, Ontario, Canada",OK
    YWS,////,////,////,UNK,0.56,Whistler,"Whistler, Canada",OWNO,////,////,Green Lake Water Aerodrome,"Whistler, British Columbia, Canada",OK
    YWY,////,////,CYWY,UNK,1.0,Wrigley,"Wrigley, Canada",OWNO,////,////,????,"Wrigley, Northwest Territories, Canada",OK
    YXC,////,////,CYXC,UNK,1.0,Cranbrook,"Cranbrook, Canada",OWNO,////,////,????,"Cranbrook, British Columbia, Canada",OK
    YXE,////,////,CYXE,UNK,0.471,Saskatoon,"Saskatoon, Canada",OWNO,////,////,John G. Diefenbaker International Airport,"Saskatoon, Saskatchewan, Canada",OK
    YXH,////,////,CYXH,UNK,1.0,Medicine Hat,"Medicine Hat, Canada",OWNO,////,////,????,"Medicine Hat, Alberta, Canada",OK
    YXJ,////,////,CYXJ,UNK,0.6,Fort St John,"Fort St John, Canada",OWNO,////,////,North Peace Regional Airport,"Fort St. John, British Columbia, Canada",OK
    YXK,////,////,CYXK,UNK,1.0,Rimouski,"Rimouski, Canada",OWNO,////,////,????,"Rimouski, Québec, Canada",OK
    YXL,////,////,CYXL,UNK,1.0,Sioux Lookout,"Sioux Lookout, Canada",OWNO,////,////,????,"Sioux Lookout, Ontario, Canada",OK
    YXN,////,////,CYXN,UNK,1.0,Whale Cove,"Whale Cove, Canada",OWNO,////,////,Whale Cove Airport,"Whale Cove, Nunavut, Canada",OK
    YXP,////,////,CYXP,UNK,1.0,Pangnirtung,"Pangnirtung, Canada",OWNO,////,////,????,"Pangnirtung, Nunavut, Canada",OK
    YXQ,////,CYXQ,CYXQ,OK,1.0,Beaver Creek,"Beaver Creek, Canada",OWNO,BEAVER CREEK,"BEAVER CREEK, - CANADA",????,"Beaver Creek, Yukon, Canada",OK
    YXR,////,////,CYXR,UNK,0.483,Earlton,"Earlton, Canada",OWNO,////,////,Timiskaming Regional,"Earlton, Ontario, Canada",OK
    YXS,////,////,CYXS,UNK,1.0,Prince George,"Prince George, Canada",OWNO,////,////,????,"Prince George, British Columbia, Canada",OK
    YXT,////,CYXT,CYXT,OK,1.0,Terrace,"Terrace, Canada",OWNO,TERRACE,"TERRACE, - CANADA",????,"Terrace, British Columbia, Canada",OK
    YXU,////,////,CYXU,UNK,0.581,Metropolitan Area,"London, Canada",OWNO,////,////,????,"London, Ontario, Canada",OK
    YXX,////,////,CYXX,UNK,1.0,Abbotsford,"Abbotsford, Canada",OWNO,////,////,????,"Abbotsford, British Columbia, Canada",OK
    YXY,////,CYXY,CYXY,OK,0.526,Whitehorse,"Whitehorse, Canada",OWNO,WHITEHORSE/ERIK NIELSEN INTL,"WHITEHORSE, - CANADA",Erik Nielsen Whitehorse International Airport,"Whitehorse, Yukon, Canada",OK
    YXZ,////,////,CYXZ,UNK,1.0,Wawa,"Wawa, Canada",OWNO,////,////,????,"Wawa, Ontario, Canada",OK
    YYB,////,CYYB,CYYB,OK,1.0,North Bay,"North Bay, Canada",OWNO,NORTH BAY,"NORTH BAY, - CANADA",????,"North Bay, Ontario, Canada",OK
    YYC,////,////,CYYC,UNK,1.0,International,"Calgary, Canada",OWNO,////,////,Calgary International Airport,"Calgary, Alberta, Canada",OK
    YYD,////,CYYD,CYYD,OK,1.0,Smithers,"Smithers, Canada",OWNO,SMITHERS,"SMITHERS, - CANADA",????,"Smithers, British Columbia, Canada",OK
    YYE,////,////,CYYE,UNK,0.588,Fort Nelson,"Fort Nelson, Canada",OWNO,////,////,Northern Rockies Regional Airport,"Fort Nelson, British Columbia, Canada",OK
    YYF,////,CYYF,CYYF,OK,1.0,Penticton,"Penticton, Canada",OWNO,PENTICTON,"PENTICTON, - CANADA",????,"Penticton, British Columbia, Canada",OK
    YYG,////,////,CYYG,UNK,1.0,Charlottetown,"Charlottetown, Canada",OWNO,////,////,????,"Charlottetown, Prince Edward Island, Canada",OK
    YYH,////,////,CYYH,UNK,1.0,Taloyoak,"Taloyoak, Canada",OWNO,////,////,????,"Taloyoak, Kitikmeot, Nunavut, Canada",OK
    YYI,////,////,CYYI,UNK,1.0,Rivers,"Rivers, Canada",OWNO,////,////,????,"Rivers, Manitoba, Canada",OK
    YYJ,////,CYYJ,CYYJ,OK,1.0,Victoria International Airport,"Victoria, Canada",OWNO,VICTORIA INTL,"VICTORIA, - CANADA",Victoria International Airport,"Victoria, British Columbia, Canada",OK
    YYL,////,////,CYYL,UNK,1.0,Lynn Lake,"Lynn Lake, Canada",OWNO,////,////,????,"Lynn Lake, Manitoba, Canada",OK
    YYM,////,////,CYYM,UNK,1.0,Cowley,"Cowley, Canada",OWNO,////,////,Cowley Airport,"Cowley, Alberta, Canada",OK
    YYN,////,////,CYYN,UNK,1.0,Swift Current,"Swift Current, Canada",OWNO,////,////,????,"Swift Current, Saskatchewan, Canada",OK
    YYQ,////,////,CYYQ,UNK,0.647,Metropolitan Area,"Churchill, Canada",OWNO,////,////,????,"Churchill, Manitoba, Canada",OK
    YYR,////,////,CYYR,UNK,0.818,Goose Bay,"Goose Bay, Canada",OWNO,////,////,CFB Goose Bay,"Happy Valley-Goose Bay, Newfoundland and Labrador, Canada",MAYBE
    YYT,////,////,CYYT,UNK,0.727,St Johns,"St Johns, Canada",OWNO,////,////,St. John's International Airport,"St. John's, Newfoundland and Labrador, Canada",OK
    YYU,////,////,CYYU,UNK,1.0,Kapuskasing,"Kapuskasing, Canada",OWNO,////,////,????,"Kapuskasing, Ontario, Canada",OK
    YYW,////,////,CYYW,UNK,1.0,Armstrong,"Armstrong, Canada",OWNO,////,////,????,"Armstrong, Ontario, Canada",OK
    YYY,////,////,CYYY,UNK,1.0,Mont Joli,"Mont Joli, Canada",OWNO,////,////,????,"Mont-Joli, Québec, Canada",OK
    YYZ,////,CYYZ,CYYZ,OK,0.666,Toronto Pearson International Airport,"Toronto, Canada",OWNO,LESTER B PEARSON INTL,"TORONTO, - CANADA",Lester B. Pearson International Airport,"Toronto, Ontario, Canada",OK
    YZA,////,////,////,UNK,0.533,Ashcroft,"Ashcroft, Canada",OWNO,////,////,Cache Creek Airport,"Cache Creek, British Columbia, Canada",TO DO CHECK
    YZD,////,////,CYZD,UNK,1.0,Downsview Airport (Bombardier Aerospace),"Toronto, Canada",OWNO,////,////,Downsview,"Toronto, Ontario, Canada",OK
    YZE,////,////,CYZE,UNK,0.593,Gore Bay,"Gore Bay, Canada",OWNO,////,////,Manitoulin,"Gore Bay, Ontario, Canada",OK
    YZF,////,////,CYZF,UNK,1.0,Yellowknife,"Yellowknife, Canada",OWNO,////,////,????,"Yellowknife, Northwest Territories, Canada",OK
    YZG,////,////,CYZG,UNK,1.0,Salluit,"Salluit, Canada",OWNO,////,////,????,"Salluit, Québec, Canada",OK
    YZH,////,////,CYZH,UNK,1.0,Slave Lake,"Slave Lake, Canada",OWNO,////,////,????,"Slave Lake, Alberta, Canada",OK
    YZP,////,CYZP,CYZP,OK,1.0,Sandspit,"Sandspit, Canada",OWNO,SANDSPIT,"SANDSPIT, - CANADA",????,"Sandspit, British Columbia, Canada",OK
    YZR,////,CYZR,CYZR,OK,0.197,Sarnia,"Sarnia, Canada",OWNO,SARNIA CHRIS HADFIELD,"SARNIA, - CANADA",Chris Hadfield,"Sarnia, Ontario, Canada",OK
    YZS,////,////,CYZS,UNK,1.0,Coral Harbour,"Coral Harbour, Canada",OWNO,////,////,????,"Coral Harbour, Nunavut, Canada",OK
    YZT,////,CYZT,CYZT,OK,1.0,Port Hardy,"Port Hardy, Canada",OWNO,PORT HARDY,"PORT HARDY, - CANADA",????,"Port Hardy, British Columbia, Canada",OK
    YZU,////,////,CYZU,UNK,1.0,Whitecourt,"Whitecourt, Canada",OWNO,////,////,????,"Whitecourt, Alberta, Canada",OK
    YZV,////,////,CYZV,UNK,1.0,Sept-Iles,"Sept-Iles, Canada",OWNO,////,////,????,"Sept-Îles, Québec, Canada",OK
    YZW,////,CYZW,CYZW,OK,1.0,Teslin,"Teslin, Canada",OWNO,TESLIN,"TESLIN, - CANADA",????,"Teslin, Yukon, Canada",OK
    YZX,////,////,CYZX,UNK,1.0,Greenwood,"Greenwood, Canada",OWNO,////,////,????,"Greenwood, Nova Scotia, Canada",OK
    YZY,////,////,ZLZY,UNK,nan,////,////,////,////,////,Zhangye Ganzhou Airport,"Zhangye, Gansu, China",UNK
    YZZ,////,////,////,UNK,0.667,Trail,Trail,IATA,////,////,Trail Regional Airport,"Trail, British Columbia, Canada",MAYBE

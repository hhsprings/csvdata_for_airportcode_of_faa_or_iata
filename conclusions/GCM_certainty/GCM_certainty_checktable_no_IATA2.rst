
List for checking certainty of Great Circle Mapper (FAA LID AKQ - FAA LID DEQ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,AKQ,KAKQ,KAKQ,OK,1.0,////,////,////,WAKEFIELD MUNI,"WAKEFIELD, VA - UNITED STATES",Wakefield Municipal Airport,"Wakefield, Virginia, United States",OK
    ////,AL73,////,////,OK,1.0,////,////,////,SHARPE FIELD,"TUSKEGEE, AL - UNITED STATES",Sharpe Field,"Tuskegee, Alabama, United States",OK
    ////,ALZ,////,////,OK,0.917,////,////,////,ALITAK,"LAZY BAY, AK - UNITED STATES",Alitak SPB,"Lazy Bay, Alaska, United States",OK
    ////,AMG,KAMG,KAMG,OK,1.0,////,////,////,BACON COUNTY,"ALMA, GA - UNITED STATES",Bacon County Airport,"Alma, Georgia, United States",OK
    ////,ANE,KANE,KANE,OK,1.0,////,////,////,ANOKA COUNTY-BLAINE ARPT (JANES FIELD),"MINNEAPOLIS, MN - UNITED STATES",Anoka County-Blaine Airport,"Minneapolis, Minnesota, United States",OK
    ////,ANJ,KANJ,KANJ,OK,1.0,////,////,////,SAULT STE MARIE MUNI/SANDERSON FIELD,"SAULT STE MARIE, MI - UNITED STATES",Sault Ste. Marie Municipal/Sanderson Field,"Sault Ste. Marie, Michigan, United States",OK
    ////,APY,KAPY,KAPY,OK,1.0,////,////,////,ZAPATA COUNTY,"ZAPATA, TX - UNITED STATES",Zapata County Airport,"Zapata, Texas, United States",OK
    ////,AQC,PAQC,PAQC,OK,0.875,////,////,////,KLAWOCK,"KLAWOCK, AK - UNITED STATES",Klawock SPB,"Klawock, Alaska, United States",OK
    ////,AQO,KAQO,KAQO,OK,1.0,////,////,////,LLANO MUNI,"LLANO, TX - UNITED STATES",Llano Municipal Airport,"Llano, Texas, United States",OK
    ////,AQP,KAQP,KAQP,OK,1.0,////,////,////,APPLETON MUNI,"APPLETON, MN - UNITED STATES",Appleton Municipal Airport,"Appleton, Minnesota, United States",OK
    ////,AQR,KAQR,KAQR,OK,1.0,////,////,////,ATOKA MUNI,"ATOKA, OK - UNITED STATES",Atoka Municipal Airport,"Atoka, Oklahoma, United States",OK
    ////,AQW,KAQW,KAQW,OK,1.0,////,////,////,HARRIMAN-AND-WEST,"NORTH ADAMS, MA - UNITED STATES",Harriman-And-West Airport,"North Adams, Massachusetts, United States",OK
    ////,AQX,KAQX,KAQX,OK,1.0,////,////,////,ALLENDALE COUNTY,"ALLENDALE, SC - UNITED STATES",Allendale County Airport,"Allendale, South Carolina, United States",OK
    ////,AQY,////,////,OK,1.0,////,////,////,GIRDWOOD,"GIRDWOOD, AK - UNITED STATES",????,"Girdwood, Alaska, United States",OK
    ////,ASD,KASD,KASD,OK,1.0,////,////,////,SLIDELL,"SLIDELL, LA - UNITED STATES",????,"Slidell, Louisiana, United States",OK
    ////,ASJ,KASJ,KASJ,OK,1.0,////,////,////,TRI-COUNTY,"AHOSKIE, NC - UNITED STATES",Tri-County Airport,"Ahoskie, North Carolina, United States",OK
    ////,ASW,KASW,KASW,OK,1.0,////,////,////,WARSAW MUNI,"WARSAW, IN - UNITED STATES",Warsaw Municipal Airport,"Warsaw, Indiana, United States",OK
    ////,AUH,KAUH,KAUH,OK,1.0,////,////,////,AURORA MUNI - AL POTTER FIELD,"AURORA, NE - UNITED STATES",Aurora Municipal - Al Potter Field,"Aurora, Nebraska, United States",OK
    ////,AVC,KAVC,KAVC,OK,1.0,////,////,////,MECKLENBURG-BRUNSWICK RGNL,"SOUTH HILL, VA - UNITED STATES",Mecklenburg-Brunswick Regional,"South Hill, Virginia, United States",OK
    ////,AVK,KAVK,KAVK,OK,1.0,////,////,////,ALVA RGNL,"ALVA, OK - UNITED STATES",Alva Regional,"Alva, Oklahoma, United States",OK
    ////,AWG,KAWG,KAWG,OK,1.0,////,////,////,WASHINGTON MUNI,"WASHINGTON, IA - UNITED STATES",Washington Municipal Airport,"Washington, Iowa, United States",OK
    ////,AWO,KAWO,KAWO,OK,1.0,////,////,////,ARLINGTON MUNI,"ARLINGTON, WA - UNITED STATES",Arlington Municipal Airport,"Arlington, Washington, United States",OK
    ////,AXH,KAXH,KAXH,OK,1.0,////,////,////,HOUSTON-SOUTHWEST,"HOUSTON, TX - UNITED STATES",Houston-Southwest Airport,"Houston, Texas, United States",OK
    ////,AZC,KAZC,KAZC,OK,1.0,////,////,////,COLORADO CITY MUNI,"COLORADO CITY, AZ - UNITED STATES",Colorado City Municipal Airport,"Colorado City, Arizona, United States",OK
    ////,AZE,KAZE,KAZE,OK,1.0,////,////,////,HAZLEHURST,"HAZLEHURST, GA - UNITED STATES",????,"Hazlehurst, Georgia, United States",OK
    ////,BAC,KBAC,KBAC,OK,1.0,////,////,////,BARNES COUNTY MUNI,"VALLEY CITY, ND - UNITED STATES",Barnes County Municipal Airport,"Valley City, North Dakota, United States",OK
    ////,BAX,KBAX,KBAX,OK,1.0,////,////,////,HURON COUNTY MEMORIAL,"BAD AXE, MI - UNITED STATES",Huron County Memorial Airport,"Bad Axe, Michigan, United States",OK
    ////,BAZ,KBAZ,KBAZ,OK,1.0,////,////,////,NEW BRAUNFELS RGNL,"NEW BRAUNFELS, TX - UNITED STATES",New Braunfels Regional,"New Braunfels, Texas, United States",OK
    ////,BCK,KBCK,KBCK,OK,1.0,////,////,////,BLACK RIVER FALLS AREA,"BLACK RIVER FALLS, WI - UNITED STATES",Black River Falls Area Airport,"Black River Falls, Wisconsin, United States",OK
    ////,BCV,PABV,PABV,OK,1.0,////,////,////,BIRCHWOOD,"BIRCHWOOD, AK - UNITED STATES",????,"Birchwood, Alaska, United States",OK
    ////,BDH,KBDH,KBDH,OK,1.0,////,////,////,WILLMAR MUNI-JOHN L RICE FIELD,"WILLMAR, MN - UNITED STATES",Willmar Municipal-John L Rice Field,"Willmar, Minnesota, United States",OK
    ////,BDN,KBDN,KBDN,OK,1.0,////,////,////,BEND MUNI,"BEND, OR - UNITED STATES",Bend Municipal Airport,"Bend, Oregon, United States",OK
    ////,BEA,KBEA,KBEA,OK,1.0,////,////,////,BEEVILLE MUNI,"BEEVILLE, TX - UNITED STATES",Beeville Municipal Airport,"Beeville, Texas, United States",OK
    ////,BFA,KBFA,KBFA,OK,1.0,////,////,////,BOYNE MOUNTAIN,"BOYNE FALLS, MI - UNITED STATES",Boyne Mountain Airport,"Boyne Falls, Michigan, United States",OK
    ////,BFK,KBFK,KBFK,OK,1.0,////,////,////,BUFFALO MUNI,"BUFFALO, OK - UNITED STATES",Buffalo Municipal Airport,"Buffalo, Oklahoma, United States",OK
    ////,BFW,KBFW,KBFW,OK,1.0,////,////,////,SILVER BAY MUNI,"SILVER BAY, MN - UNITED STATES",Silver Bay Municipal Airport,"Silver Bay, Minnesota, United States",OK
    ////,BGF,KBGF,KBGF,OK,1.0,////,////,////,WINCHESTER MUNI,"WINCHESTER, TN - UNITED STATES",Winchester Municipal Airport,"Winchester, Tennessee, United States",OK
    ////,BHC,KBHC,KBHC,OK,1.0,////,////,////,BAXLEY MUNI,"BAXLEY, GA - UNITED STATES",Baxley Municipal Airport,"Baxley, Georgia, United States",OK
    ////,BHK,KBHK,KBHK,OK,1.0,////,////,////,BAKER MUNI,"BAKER, MT - UNITED STATES",Baker Municipal Airport,"Baker, Montana, United States",OK
    ////,BIG,PABI,PABI,OK,1.0,////,////,////,ALLEN AAF,"DELTA JUNCTION FT GREELY, AK - UNITED STATES",Allen AAF Airport,"Delta Junction/Ft Greeley, Alaska, United States",OK
    ////,BIJ,KBIJ,KBIJ,OK,1.0,////,////,////,EARLY COUNTY,"BLAKELY, GA - UNITED STATES",Early County Airport,"Blakely, Georgia, United States",OK
    ////,BIV,KBIV,KBIV,OK,1.0,////,////,////,WEST MICHIGAN RGNL,"HOLLAND, MI - UNITED STATES",West Michigan Regional,"Holland, Michigan, United States",OK
    ////,BKF,KBKF,KBKF,OK,1.0,////,////,////,BUCKLEY AFB,"AURORA, CO - UNITED STATES",Buckley AFB,"Aurora, Colorado, United States",OK
    ////,BKN,KBKN,KBKN,OK,1.0,////,////,////,BLACKWELL-TONKAWA MUNI,"BLACKWELL, OK - UNITED STATES",Blackwell-Tonkawa Municipal Airport,"Blackwell, Oklahoma, United States",OK
    ////,BKS,KBKS,KBKS,OK,1.0,////,////,////,BROOKS COUNTY,"FALFURRIAS, TX - UNITED STATES",Brooks County Airport,"Falfurrias, Texas, United States",OK
    ////,BKV,KBKV,KBKV,OK,1.0,////,////,////,BROOKSVILLE-TAMPA BAY RGNL,"BROOKSVILLE, FL - UNITED STATES",Brooksville-Tampa Bay Regional,"Brooksville, Florida, United States",OK
    ////,BMQ,KBMQ,KBMQ,OK,1.0,////,////,////,BURNET MUNI KATE CRADDOCK FIELD,"BURNET, TX - UNITED STATES",Burnet Municipal Kate Craddock Field,"Burnet, Texas, United States",OK
    ////,BPG,KBPG,KBPG,OK,1.0,////,////,////,BIG SPRING MC MAHON-WRINKLE,"BIG SPRING, TX - UNITED STATES",Big Spring Mc Mahon-Wrinkle Airport,"Big Spring, Texas, United States",OK
    ////,BQP,KBQP,KBQP,OK,1.0,////,////,////,MOREHOUSE MEMORIAL,"BASTROP, LA - UNITED STATES",Morehouse Memorial Airport,"Bastrop, Louisiana, United States",OK
    ////,BTA,KBTA,KBTA,OK,1.0,////,////,////,BLAIR MUNI,"BLAIR, NE - UNITED STATES",Blair Municipal Airport,"Blair, Nebraska, United States",OK
    ////,BUU,KBUU,KBUU,OK,1.0,////,////,////,BURLINGTON MUNI,"BURLINGTON, WI - UNITED STATES",Burlington Municipal Airport,"Burlington, Wisconsin, United States",OK
    ////,BUY,KBUY,KBUY,OK,1.0,////,////,////,BURLINGTON-ALAMANCE RGNL,"BURLINGTON, NC - UNITED STATES",Burlington-Alamance Regional,"Burlington, North Carolina, United States",OK
    ////,BVN,KBVN,KBVN,OK,1.0,////,////,////,ALBION MUNI,"ALBION, NE - UNITED STATES",Albion Municipal Airport,"Albion, Nebraska, United States",OK
    ////,BVY,KBVY,KBVY,OK,1.0,////,////,////,BEVERLY MUNI,"BEVERLY, MA - UNITED STATES",Beverly Municipal Airport,"Beverly, Massachusetts, United States",OK
    ////,BWW,KBWW,KBWW,OK,1.0,////,////,////,BOWMAN RGNL,"BOWMAN, ND - UNITED STATES",Bowman Regional,"Bowman, North Dakota, United States",OK
    ////,BXM,KBXM,KBXM,OK,1.0,////,////,////,BRUNSWICK EXECUTIVE,"BRUNSWICK, ME - UNITED STATES",Brunswick Executive Airport,"Brunswick, Maine, United States",OK
    ////,BYL,KBYL,KBYL,OK,1.0,////,////,////,WILLIAMSBURG-WHITLEY COUNTY,"WILLIAMSBURG, KY - UNITED STATES",Williamsburg-Whitley County Airport,"Williamsburg, Kentucky, United States",OK
    ////,C16,////,////,OK,1.0,////,////,////,FRASCA FIELD,"URBANA, IL - UNITED STATES",Frasca Field,"Urbana, Illinois, United States",OK
    ////,C23,////,////,OK,1.0,////,////,////,PELELIU,"BABELTHUAP ISLAND, - PALAU",Peleliu Airport,"Babelthuap Island, Palau",OK
    ////,C47,////,////,OK,1.0,////,////,////,PORTAGE MUNI,"PORTAGE, WI - UNITED STATES",Portage Municipal Airport,"Portage, Wisconsin, United States",OK
    ////,C89,////,////,OK,1.0,////,////,////,SYLVANIA,"STURTEVANT, WI - UNITED STATES",Sylvania Airport,"Sturtevant, Wisconsin, United States",OK
    ////,C91,////,////,OK,1.0,////,////,////,DOWAGIAC MUNI,"DOWAGIAC, MI - UNITED STATES",Dowagiac Municipal Airport,"Dowagiac, Michigan, United States",OK
    ////,CA92,////,////,OK,1.0,////,////,////,PARADISE SKYPARK,"PARADISE, CA - UNITED STATES",Paradise Skypark Airport,"Paradise, California, United States",OK
    ////,CAV,KCAV,KCAV,OK,1.0,////,////,////,CLARION MUNI,"CLARION, IA - UNITED STATES",Clarion Municipal Airport,"Clarion, Iowa, United States",OK
    ////,CBG,KCBG,KCBG,OK,1.0,////,////,////,CAMBRIDGE MUNI,"CAMBRIDGE, MN - UNITED STATES",Cambridge Municipal Airport,"Cambridge, Minnesota, United States",OK
    ////,CCA,KCCA,KCCA,OK,1.0,////,////,////,CLINTON MUNI,"CLINTON, AR - UNITED STATES",Clinton Municipal Airport,"Clinton, Arkansas, United States",OK
    ////,CCO,KCCO,KCCO,OK,1.0,////,////,////,NEWNAN COWETA COUNTY,"ATLANTA, GA - UNITED STATES",Newnan Coweta County Airport,"Atlanta, Georgia, United States",OK
    ////,CDD,KCDD,KCDD,OK,0.933,////,////,////,SCOTTS,"CRANE LAKE, MN - UNITED STATES",Scotts SPB,"Crane Lake, Minnesota, United States",OK
    ////,CDI,KCDI,KCDI,OK,1.0,////,////,////,CAMBRIDGE MUNI,"CAMBRIDGE, OH - UNITED STATES",Cambridge Municipal Airport,"Cambridge, Ohio, United States",OK
    ////,CFE,KCFE,KCFE,OK,1.0,////,////,////,BUFFALO MUNI,"BUFFALO, MN - UNITED STATES",Buffalo Municipal Airport,"Buffalo, Minnesota, United States",OK
    ////,CFJ,KCFJ,KCFJ,OK,1.0,////,////,////,CRAWFORDSVILLE MUNI,"CRAWFORDSVILLE, IN - UNITED STATES",Crawfordsville Municipal Airport,"Crawfordsville, Indiana, United States",OK
    ////,CFS,KCFS,KCFS,OK,1.0,////,////,////,TUSCOLA AREA,"CARO, MI - UNITED STATES",Tuscola Area Airport,"Caro, Michigan, United States",OK
    ////,CGC,KCGC,KCGC,OK,1.0,////,////,////,CRYSTAL RIVER-CAPTAIN TOM DAVIS FLD,"CRYSTAL RIVER, FL - UNITED STATES",Crystal River-Captain Tom Davis Field,"Crystal River, Florida, United States",OK
    ////,CHD,KCHD,KCHD,OK,1.0,////,////,////,CHANDLER MUNI,"CHANDLER, AZ - UNITED STATES",Chandler Municipal Airport,"Chandler, Arizona, United States",OK
    ////,CHU,KCHU,KCHU,OK,1.0,////,////,////,HOUSTON COUNTY,"CALEDONIA, MN - UNITED STATES",Houston County Airport,"Caledonia, Minnesota, United States",OK
    ////,CII,KCII,KCII,OK,1.0,////,////,////,CHOTEAU,"CHOTEAU, MT - UNITED STATES",????,"Choteau, Montana, United States",OK
    ////,CJR,KCJR,KCJR,OK,1.0,////,////,////,CULPEPER RGNL,"CULPEPER, VA - UNITED STATES",Culpeper Regional,"Culpeper, Virginia, United States",OK
    ////,CKF,KCKF,KCKF,OK,1.0,////,////,////,CRISP COUNTY-CORDELE,"CORDELE, GA - UNITED STATES",Crisp County-Cordele Airport,"Cordele, Georgia, United States",OK
    ////,CKI,KCKI,KCKI,OK,1.0,////,////,////,WILLIAMSBURG RGNL,"KINGSTREE, SC - UNITED STATES",Williamsburg Regional,"Kingstree, South Carolina, United States",OK
    ////,CKP,KCKP,KCKP,OK,1.0,////,////,////,CHEROKEE COUNTY RGNL,"CHEROKEE, IA - UNITED STATES",Cherokee County Regional,"Cherokee, Iowa, United States",OK
    ////,CKZ,KCKZ,KCKZ,OK,1.0,////,////,////,PENNRIDGE,"PERKASIE, PA - UNITED STATES",Pennridge Airport,"Perkasie, Pennsylvania, United States",OK
    ////,CMA,KCMA,KCMA,OK,1.0,////,////,////,CAMARILLO,"CAMARILLO, CA - UNITED STATES",????,"Camarillo, California, United States",OK
    ////,CMD,KCMD,KCMD,OK,1.0,////,////,////,CULLMAN RGNL-FOLSOM FIELD,"CULLMAN, AL - UNITED STATES",Cullman Regional-Folsom Field,"Cullman, Alabama, United States",OK
    ////,CNB,KCNB,KCNB,OK,1.0,////,////,////,MYERS FIELD,"CANBY, MN - UNITED STATES",Myers Field,"Canby, Minnesota, United States",OK
    ////,CNC,KCNC,KCNC,OK,1.0,////,////,////,CHARITON MUNI,"CHARITON, IA - UNITED STATES",Chariton Municipal Airport,"Chariton, Iowa, United States",OK
    ////,CNI,KCNI,KCNI,OK,1.0,////,////,////,CHEROKEE COUNTY,"CANTON, GA - UNITED STATES",Cherokee County Airport,"Canton, Georgia, United States",OK
    ////,COQ,KCOQ,KCOQ,OK,1.0,////,////,////,CLOQUET CARLTON COUNTY,"CLOQUET, MN - UNITED STATES",Cloquet Carlton County Airport,"Cloquet, Minnesota, United States",OK
    ////,CPC,KCPC,KCPC,OK,1.0,////,////,////,COLUMBUS COUNTY MUNI,"WHITEVILLE, NC - UNITED STATES",Columbus County Municipal Airport,"Whiteville, North Carolina, United States",OK
    ////,CPK,KCPK,KCPK,OK,1.0,////,////,////,CHESAPEAKE RGNL,"NORFOLK, VA - UNITED STATES",Chesapeake Regional,"Norfolk, Virginia, United States",OK
    ////,CPT,KCPT,KCPT,OK,1.0,////,////,////,CLEBURNE RGNL,"CLEBURNE, TX - UNITED STATES",Cleburne Regional,"Cleburne, Texas, United States",OK
    ////,CPU,KCPU,KCPU,OK,1.0,////,////,////,CALAVERAS CO-MAURY RASMUSSEN FIELD,"SAN ANDREAS, CA - UNITED STATES",Calaveras Co-Maury Rasmussen Field,"San Andreas, California, United States",OK
    ////,CQA,KCQA,KCQA,OK,1.0,////,////,////,LAKEFIELD,"CELINA, OH - UNITED STATES",Lakefield Airport,"Celina, Ohio, United States",OK
    ////,CQB,KCQB,KCQB,OK,1.0,////,////,////,CHANDLER RGNL,"CHANDLER, OK - UNITED STATES",Chandler Regional,"Chandler, Oklahoma, United States",OK
    ////,CQF,KCQF,KCQF,OK,1.0,////,////,////,H L SONNY CALLAHAN,"FAIRHOPE, AL - UNITED STATES",H L Sonny Callahan Airport,"Fairhope, Alabama, United States",OK
    ////,CQM,KCQM,KCQM,OK,1.0,////,////,////,COOK MUNI,"COOK, MN - UNITED STATES",Cook Municipal Airport,"Cook, Minnesota, United States",OK
    ////,CQX,KCQX,KCQX,OK,1.0,////,////,////,CHATHAM MUNI,"CHATHAM, MA - UNITED STATES",Chatham Municipal Airport,"Chatham, Massachusetts, United States",OK
    ////,CRO,KCRO,KCRO,OK,1.0,////,////,////,CORCORAN,"CORCORAN, CA - UNITED STATES",????,"Corcoran, California, United States",OK
    ////,CRZ,KCRZ,KCRZ,OK,1.0,////,////,////,CORNING MUNI,"CORNING, IA - UNITED STATES",Corning Municipal Airport,"Corning, Iowa, United States",OK
    ////,CSL,KCSL,KCSL,OK,0.863,////,////,////,O'SULLIVAN AHP,"CAMP SAN LUIS OBISPO, CA - UNITED STATES",O'Sullivan Army Heliport,"Camp San Luis Obispo, California, United States",OK
    ////,CSR,////,////,OK,1.0,////,////,////,CAMPBELL AIRSTRIP,"ANCHORAGE, AK - UNITED STATES",Campbell Airstrip,"Anchorage, Alaska, United States",OK
    ////,CTJ,KCTJ,KCTJ,OK,1.0,////,////,////,WEST GEORGIA RGNL - O V GRAY FIELD,"CARROLLTON, GA - UNITED STATES",West Georgia Regional - O V Gray Field,"Carrollton, Georgia, United States",OK
    ////,CTK,KCTK,KCTK,OK,1.0,////,////,////,INGERSOLL,"CANTON, IL - UNITED STATES",Ingersoll Airport,"Canton, Illinois, United States",OK
    ////,CUL,KCUL,KCUL,OK,1.0,////,////,////,CARMI MUNI,"CARMI, - UNITED STATES",Carmi Municipal Airport,"Carmi, Illinois, United States",OK
    ////,CUT,KCUT,KCUT,OK,1.0,////,////,////,CUSTER COUNTY,"CUSTER, SD - UNITED STATES",Custer County Airport,"Custer, South Dakota, United States",OK
    ////,CVB,KCVB,KCVB,OK,1.0,////,////,////,CASTROVILLE MUNI,"CASTROVILLE, TX - UNITED STATES",Castroville Municipal Airport,"Castroville, Texas, United States",OK
    ////,CVC,KCVC,KCVC,OK,1.0,////,////,////,COVINGTON MUNI,"ATLANTA, GA - UNITED STATES",Covington Municipal Airport,"Atlanta, Georgia, United States",OK
    ////,CVK,KCVK,KCVK,OK,1.0,////,////,////,SHARP COUNTY RGNL,"ASH FLAT, AR - UNITED STATES",Sharp County Regional,"Ash Flat, Arkansas, United States",OK
    ////,CVX,KCVX,KCVX,OK,1.0,////,////,////,CHARLEVOIX MUNI,"CHARLEVOIX, MI - UNITED STATES",Charlevoix Municipal Airport,"Charlevoix, Michigan, United States",OK
    ////,CWF,KCWF,KCWF,OK,1.0,////,////,////,CHENNAULT INTL,"LAKE CHARLES, LA - UNITED STATES",Chennault International Airport,"Lake Charles, Louisiana, United States",OK
    ////,CWV,KCWV,KCWV,OK,1.0,////,////,////,CLAXTON-EVANS COUNTY,"CLAXTON, GA - UNITED STATES",Claxton-Evans County Airport,"Claxton, Georgia, United States",OK
    ////,CXE,KCXE,KCXE,OK,1.0,////,////,////,CHASE CITY MUNI,"CHASE CITY, VA - UNITED STATES",Chase City Municipal Airport,"Chase City, Virginia, United States",OK
    ////,CZG,KCZG,KCZG,OK,1.0,////,////,////,TRI-CITIES,"ENDICOTT, NY - UNITED STATES",Tri-Cities Airport,"Endicott, New York, United States",OK
    ////,CZL,KCZL,KCZL,OK,1.0,////,////,////,TOM B DAVID FLD,"CALHOUN, GA - UNITED STATES",Tom B David Field,"Calhoun, Georgia, United States",OK
    ////,DAF,KDAF,KDAF,OK,1.0,////,////,////,NECEDAH,"NECEDAH, WI - UNITED STATES",????,"Necedah, Wisconsin, United States",OK
    ////,DAW,KDAW,KDAW,OK,1.0,////,////,////,SKYHAVEN,"ROCHESTER, NH - UNITED STATES",Skyhaven Airport,"Rochester, New Hampshire, United States",OK
    ////,DCM,KDCM,KDCM,OK,1.0,////,////,////,CHESTER CATAWBA RGNL,"CHESTER, SC - UNITED STATES",Chester Catawba Regional,"Chester, South Carolina, United States",OK
    ////,DCY,KDCY,KDCY,OK,1.0,////,////,////,DAVIESS COUNTY,"WASHINGTON, IN - UNITED STATES",Daviess County Airport,"Washington, Indiana, United States",OK
    ////,DDH,KDDH,KDDH,OK,1.0,////,////,////,WILLIAM H MORSE STATE,"BENNINGTON, VT - UNITED STATES",William H Morse State Airport,"Bennington, Vermont, United States",OK
    ////,DED,KDED,KDED,OK,1.0,////,////,////,DELAND MUNI-SIDNEY H TAYLOR FIELD,"DELAND, FL - UNITED STATES",Deland Municipal-Sidney H Taylor Field,"Deland, Florida, United States",OK
    ////,DEQ,KDEQ,KDEQ,OK,1.0,////,////,////,J LYNN HELMS SEVIER COUNTY,"DE QUEEN, AR - UNITED STATES",J Lynn Helms Sevier County Airport,"De Queen, Arkansas, United States",OK

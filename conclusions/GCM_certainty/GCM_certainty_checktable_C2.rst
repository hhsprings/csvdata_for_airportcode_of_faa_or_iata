
List for checking certainty of Great Circle Mapper (CEG - CIT)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    CEG,////,////,EGNR,UNK,1.0,Chester,"Chester, United Kingdom",OWNO,////,////,????,"Chester, Flintshire, England, United Kingdom",OK
    CEI,////,////,VTCT,UNK,0.8,Chiang Rai,"Chiang Rai, Thailand",OWNO,////,////,Chiang Rai International Airport,"Chiang Rai, Chiang Rai, Thailand",OK
    CEJ,////,////,UKKL,UNK,0.609,Chernigov,"Chernigov, Ukraine",OWNO,////,////,Shestovitsa,"Chernihiv, Chernihiv, Ukraine",TO DO CHECK
    CEK,////,////,USCC,UNK,0.688,Chelyabinsk,"Chelyabinsk, Russia",OWNO,////,////,Balandino Airport,"Chelyabinsk, Chelyabinskaya, Russian Federation (Russia)",OK
    CEM,CEM,PACE,PACE,OK,1.0,Central,"Central (AK), USA",OWNO,CENTRAL,"CENTRAL, AK - UNITED STATES",????,"Central, Alaska, United States",OK
    CEN,////,////,MMCN,UNK,0.857,Ciudad Obregon,"Ciudad Obregon, Mexico",OWNO,////,////,Ciudad Obregón International Airport,"Ciudad Obregón, Sonora, México",OK
    CEO,////,////,FNWK,UNK,1.0,Waco Kungo,"Waco Kungo, Angola",OWNO,////,////,????,"Waku-Kungo, Angola",OK
    CEP,////,////,SLCP,UNK,1.0,Concepcion,"Concepción, Bolivia",OWNO,////,////,Concepción Airport,"Concepción, Ñuflo de Chávez, Santa Cruz, Bolivia",OK
    CEQ,////,////,LFMD,UNK,1.0,Cannes - Mandelieu,"Cannes, France",OWNO,////,////,Mandelieu,"Cannes, Provence-Alpes-Côte d'Azur, France",OK
    CER,////,////,LFRC,UNK,1.0,Maupertus,"Cherbourg, France",OWNO,////,////,Maupertus,"Cherbourg, Basse-Normandie (Lower Normandy), France",OK
    CES,////,////,YCNK,UNK,1.0,Cessnock,"Cessnock, Australia",OWNO,////,////,????,"Cessnock, New South Wales, Australia",OK
    CET,////,////,LFOU,UNK,1.0,Le Pontreau,"Cholet, France",OWNO,////,////,Le Pontreau,"Cholet, Pays de la Loire, France",OK
    CEU,CEU,KCEU,KCEU,OK,0.874,Oconee County,"Clemson (SC), USA",OWNO,OCONEE COUNTY RGNL,"CLEMSON, SC - UNITED STATES",Oconee County Regional,"Clemson, South Carolina, United States",OK
    CEV,CEV,KCEV,KCEV,OK,1.0,Mettle Field,"Connersville (IN), USA",OWNO,METTEL FIELD,"CONNERSVILLE, IN - UNITED STATES",Mettel Field,"Connersville, Indiana, United States",OK
    CEW,CEW,KCEW,KCEW,OK,1.0,Bob Sikes,"Crestview (FL), USA",OWNO,BOB SIKES,"CRESTVIEW, FL - UNITED STATES",Bob Sikes Airport,"Crestview, Florida, United States",OK
    CEX,AK13,////,////,OK,1.0,Chena Hot Springs,"Chena Hot Springs (AK), USA",OWNO,CHENA HOT SPRINGS,"CHENA HOT SPRINGS, AK - UNITED STATES",????,"Chena Hot Springs, Alaska, United States",OK
    CEY,CEY,KCEY,KCEY,OK,0.471,Calloway County,"Murray (KY), USA",OWNO,KYLE-OAKLEY FIELD,"MURRAY, KY - UNITED STATES",Kyle-Oakley Field,"Murray, Kentucky, United States",OK
    CEZ,CEZ,KCEZ,KCEZ,OK,0.623,Montezuma County,"Cortez (CO), USA",OWNO,CORTEZ MUNI,"CORTEZ, CO - UNITED STATES",Cortez Municipal Airport,"Cortez, Colorado, United States",OK
    CFB,////,////,SBCB,UNK,0.783,Cabo Frio,"Cabo Frio, Brazil",OWNO,////,////,Cabo Frio International Airport,"Cabo Frio, Rio de Janeiro, Brazil",OK
    CFC,////,////,SBCD,UNK,0.378,Cacador,"Cacador, Brazil",OWNO,////,////,Carlos Alberto da Costa Neves Airport,"Caçador, Santa Catarina, Brazil",OK
    CFD,CFD,KCFD,KCFD,OK,1.0,Coulter Field,"Bryan (TX), USA",OWNO,COULTER FIELD,"BRYAN, TX - UNITED STATES",Coulter Field,"Bryan, Texas, United States",OK
    CFE,////,////,LFLC,UNK,0.842,Aulnat,"Clermont-Ferrand, France",OWNO,////,////,Clermont/Ferrand Auvergne International Airport,"Clermont/Ferrand Auvergne, Auvergne, France",TO DO CHECK
    CFF,////,////,FNCF,UNK,1.0,Cafunfo,"Cafunfo, Angola",OWNO,////,////,????,"Cafunfo, Angola",OK
    CFG,////,////,MUCF,UNK,1.0,Jaime González Airport,"Cienfuegos, Cuba",OWNO,////,////,Jaime González,"Cienfuegos, Cienfuegos, Cuba",OK
    CFH,////,////,YCFH,UNK,1.0,Clifton Hills,"Clifton Hills, Australia",OWNO,////,////,????,"Clifton Hills, South Australia, Australia",OK
    CFI,////,////,YCFD,UNK,0.7,Camfield,"Camfield, Australia",OWNO,////,////,Camfield Station Airport,"Camfield Station, Northern Territory, Australia",MAYBE
    CFK,////,////,DAOI,UNK,nan,////,////,////,////,////,Chlef International Airport,"Chlef, Chlef, Algeria",UNK
    CFN,////,////,EIDL,UNK,1.0,Donegal,"Donegal, Ireland",OWNO,////,////,Donegal,"Carrickfinn, County Donegal, Ulster, Ireland",OK
    CFO,////,////,SJHG,UNK,0.636,Confreza,"Confreza, Brazil",OWNO,////,////,Aeropuerto Confresa,"Confresa, Mato Grosso, Brazil",OK
    CFP,////,////,YCPN,UNK,1.0,Carpentaria Downs,"Carpentaria Downs, Australia",OWNO,////,////,????,"Carpentaria Downs, Queensland, Australia",OK
    CFR,////,////,LFRK,UNK,1.0,Carpiquet,"Caen, France",OWNO,////,////,Carpiquet,"Caen, Basse-Normandie (Lower Normandy), France",OK
    CFS,////,////,YCFS,UNK,0.857,Coffs Harbour,"Coffs Harbour, Australia",OWNO,////,////,Coffs Harbour Regional Airport,"Coffs Harbour, New South Wales, Australia",OK
    CFT,CFT,KCFT,KCFT,OK,0.588,Morenci,"Clifton (AZ), USA",OWNO,GREENLEE COUNTY,"CLIFTON/MORENCI, AZ - UNITED STATES",Greenlee County Airport,"Clifton/Morenci, Arizona, United States",OK
    CFU,////,////,LGKR,UNK,0.837,I. Kapodistrias,"Kerkyra, Greece",OWNO,////,////,Ioannis Kapodistrias International Airport,"Kerkyra, Corfu, Ionía Nísia (Ionian Islands), Greece",OK
    CFV,CFV,KCFV,KCFV,OK,1.0,Municipal,"Coffeyville (KS), USA",OWNO,COFFEYVILLE MUNI,"COFFEYVILLE, KS - UNITED STATES",Coffeyville Municipal Airport,"Coffeyville, Kansas, United States",OK
    CGA,CGA,////,////,OK,0.757,Craig SPB,"Craig (AK), USA",OWNO,CRAIG,"CRAIG, AK - UNITED STATES",Craig SPB,"Craig, Alaska, United States",OK
    CGB,////,////,SBCY,UNK,1.0,Marechal Rondon International Airport,"Cuiaba, Brazil",OWNO,////,////,Marechal Rondon International Airport,"Cuiabá (Várzea Grande)), Mato Grosso, Brazil",OK
    CGC,////,////,////,UNK,1.0,Cape Gloucester,"Cape Gloucester, Papua New Guinea",OWNO,////,////,????,"Cape Gloucester, West New Britain, Papua-New Guinea",OK
    CGD,////,////,ZGCD,UNK,1.0,Changde,"Changde, PR China",OWNO,////,////,????,"Changde, Hunan, China",OK
    CGE,CGE,KCGE,KCGE,OK,0.479,Cambridge,"Cambridge (MD), USA",OWNO,CAMBRIDGE-DORCHESTER RGNL,"CAMBRIDGE, MD - UNITED STATES",Cambridge-Dorchester Regional,"Cambridge, Maryland, United States",OK
    CGF,CGF,KCGF,KCGF,OK,1.0,Cuyahoga County,"Cleveland (OH), USA",OWNO,CUYAHOGA COUNTY,"CLEVELAND, OH - UNITED STATES",Cuyahoga County Airport,"Cleveland, Ohio, United States",OK
    CGH,////,////,SBSP,UNK,0.884,Congonhas,"Sao Paulo, Brazil",OWNO,////,////,Congonhas International Airport,"São Paulo, São Paulo, Brazil",OK
    CGI,CGI,KCGI,KCGI,OK,0.814,Cape Girardeau,"Cape Girardeau (MO), USA",OWNO,CAPE GIRARDEAU RGNL,"CAPE GIRARDEAU, MO - UNITED STATES",Cape Girardeau Regional,"Cape Girardeau, Missouri, United States",OK
    CGJ,////,////,FLKE,UNK,0.667,Chingola,"Chingola, Zambia",OWNO,////,////,Kasompe,"Chingola, Copperbelt, Zambia",OK
    CGK,////,////,WIII,UNK,1.0,Soekarno-Hatta International,"Jakarta, Indonesia",OWNO,////,////,Soekarno-Hatta International Airport,"Jakarta, Banten, Indonesia",OK
    CGM,////,////,RPMH,UNK,0.737,Mambajao,"Camiguin, Philippines",OWNO,////,////,????,"Camiguin, Philippines",OK
    CGN,////,////,EDDK,UNK,0.6,Cologne/Bonn,"Cologne, Germany",OWNO,////,////,Konrad Adenauer,"Cologne-Bonn, North Rhine-Westphalia, Germany",TO DO CHECK
    CGO,////,////,ZHCC,UNK,0.818,Zhengzhou,"Zhengzhou, PR China",OWNO,////,////,Xinzheng Airport,"Zhengzhou, Henan, China",OK
    CGP,////,////,VGEG,UNK,0.606,Patenga,"Chittagong, Bangladesh",OWNO,////,////,Shah Amanat International Airport,"Chittagong, Bangladesh",OK
    CGQ,////,////,ZYCC,UNK,0.815,Changchun,"Changchun, PR China",OWNO,////,////,Longjia International Airport,"Changchun, Jilin, China",OK
    CGR,////,////,SBCG,UNK,1.0,Campo Grande International Airport,"Campo Grande, Brazil",OWNO,////,////,Campo Grande International Airport,"Campo Grande, Mato Grosso do Sul, Brazil",OK
    CGS,CGS,KCGS,KCGS,OK,1.0,College Park,"College Park (MD), USA",OWNO,COLLEGE PARK,"COLLEGE PARK, MD - UNITED STATES",????,"College Park, Maryland, United States",OK
    CGT,////,////,GQPC,UNK,1.0,Chinguitti,"Chinguitti, Mauritania",OWNO,////,////,Chinguetti Airport,"Chinguetti, Mauritania",OK
    CGU,////,////,////,UNK,1.0,Ciudad Guayana,"Ciudad Guayana, Venezuela",OWNO,////,////,????,"Ciudad Guayana, Venezuela",OK
    CGV,////,////,YCAG,UNK,1.0,Caiguna,"Caiguna, Australia",OWNO,////,////,????,"Caiguna, Western Australia, Australia",OK
    CGY,////,////,RPML,UNK,1.0,Lumbia,"Cagayan De Oro, Philippines",OWNO,////,////,Lumbia,"Cagayan de Oro, Mindanao Island, Philippines",OK
    CGZ,CGZ,KCGZ,KCGZ,OK,1.0,Municipal,"Casa Grande (AZ), USA",OWNO,CASA GRANDE MUNI,"CASA GRANDE, AZ - UNITED STATES",Casa Grande Municipal Airport,"Casa Grande, Arizona, United States",OK
    CHA,CHA,KCHA,KCHA,OK,1.0,Lovell Field,"Chattanooga (TN), USA",OWNO,LOVELL FIELD,"CHATTANOOGA, TN - UNITED STATES",Lovell Field,"Chattanooga, Tennessee, United States",OK
    CHB,////,////,OPCL,UNK,1.0,Chilas,"Chilas, Pakistan",OWNO,////,////,Chilas Airport,"Chilas, Gilgit-Baltistan, Pakistan",OK
    CHC,////,////,NZCH,UNK,1.0,International,"Christchurch, New Zealand",OWNO,////,////,Christchurch International Airport,"Christchurch, New Zealand",OK
    CHE,////,////,////,UNK,1.0,Reenroe,"Caherciveen, Ireland",OWNO,////,////,Reenroe,"Caherciveen, County Kerry, Munster, Ireland",OK
    CHF,////,////,RKPE,UNK,1.0,Chinhae,"Chinhae, South Korea",OWNO,////,////,????,"Chinhae, Republic of Korea (South Korea)",OK
    CHG,////,////,ZYCY,UNK,1.0,Chaoyang Airport,"Chaoyang, PR China",OWNO,////,////,????,"Chaoyang, Liaoning, China",OK
    CHH,////,////,SPPY,UNK,1.0,Chachapoyas,"Chachapoyas, Peru",OWNO,////,////,Chachapoyas Airport,"Chachapoyas, Amazonas, Perú",OK
    CHI,////,////,////,UNK,0.606,Chicago FSS,"Chicago (IL), USA",OWNO,////,////,Metropolitan Area,"Chicago, Illinois, United States",OK
    CHJ,////,////,FVCH,UNK,1.0,Chipinge,"Chipinge, Zimbabwe",OWNO,////,////,????,"Chipinge, Zimbabwe",OK
    CHK,CHK,KCHK,KCHK,OK,1.0,Municipal,"Chickasha (OK), USA",OWNO,CHICKASHA MUNI,"CHICKASHA, OK - UNITED STATES",Chickasha Municipal Airport,"Chickasha, Oklahoma, United States",OK
    CHL,LLJ,KLLJ,KLLJ,OK,1.0,Challis,"Challis (ID), USA",OWNO,CHALLIS,"CHALLIS, ID - UNITED STATES",????,"Challis, Idaho, United States",OK
    CHM,////,////,SPEO,UNK,0.276,Chimbote,"Chimbote, Peru",OWNO,////,////,Teniente FAP Jaime A. de Montrevil Morales,"Chimbote, Ancash, Perú",OK
    CHN,////,////,RKJU,UNK,1.0,Chonju,"Chonju, South Korea",OWNO,////,////,????,"Chonju, Republic of Korea (South Korea)",OK
    CHO,CHO,KCHO,KCHO,OK,1.0,Albemarle,"Charlottesville (VA), USA",OWNO,CHARLOTTESVILLE-ALBEMARLE,"CHARLOTTESVILLE, VA - UNITED STATES",Charlottesville-Albemarle Airport,"Charlottesville, Virginia, United States",OK
    CHP,CHP,////,////,OK,1.0,Circle Hot Springs,"Circle Hot Springs (AK), USA",OWNO,CIRCLE HOT SPRINGS,"CIRCLE HOT SPRINGS, AK - UNITED STATES",????,"Circle Hot Springs, Alaska, United States",OK
    CHQ,////,////,LGSA,UNK,0.48,Souda,"Chania, Greece",OWNO,////,////,Ioannis Daskalogiannis International Airport,"Chania, Krítí (Crete), Greece",OK
    CHR,////,////,LFLX,UNK,0.579,Chateauroux,"Chateauroux, France",OWNO,////,////,Marcel Dassault,"Châteauroux/Déols, Centre, France",OK
    CHS,CHS,KCHS,KCHS,OK,0.885,AFB Municipal,"Charleston (SC), USA",OWNO,CHARLESTON AFB/INTL,"CHARLESTON, SC - UNITED STATES",Charleston AFB/International,"Charleston, South Carolina, United States",OK
    CHT,////,////,NZCI,UNK,0.828,Karewa,"Chatham Island, New Zealand",OWNO,////,////,Tuuta,"Waitangi, Chatham Islands, New Zealand",OK
    CHU,9A3,PACH,PACH,OK,1.0,Chuathbaluk,"Chuathbaluk (AK), USA",OWNO,CHUATHBALUK,"CHUATHBALUK, AK - UNITED STATES",????,"Chuathbaluk, Alaska, United States",OK
    CHV,////,////,LPCH,UNK,1.0,Chaves,"Chaves, Portugal",OWNO,////,////,????,"Chaves, Vila Real, Portugal",OK
    CHX,////,////,MPCH,UNK,0.538,Changuinola,"Changuinola, Panama",OWNO,////,////,Captain Manuel Nino International Airport,"Changuinola, Bocas del Toro, Panamá",OK
    CHY,////,////,AGGC,UNK,1.0,Choiseul Bay,"Choiseul Bay, Solomon Islands",OWNO,////,////,????,"Choiseul Bay, Taro Island, Solomon Islands",OK
    CHZ,2S7,////,////,OK,1.0,State,"Chiloquin (OR), USA",OWNO,CHILOQUIN STATE,"CHILOQUIN, OR - UNITED STATES",Chiloquin State Airport,"Chiloquin, Oregon, United States",OK
    CIA,////,////,LIRA,UNK,0.473,Ciampino,"Rome, Italy",OWNO,////,////,Ciampino-Giovan Battista Pastine International Airport,"Rome, Lazio, Italy",OK
    CIB,AVX,KAVX,KAVX,OK,0.563,Ap In The Sky,"Catalina Island (CA), USA",OWNO,CATALINA,"AVALON, CA - UNITED STATES",Catalina Airport,"Avalon, California, United States",OK
    CIC,CIC,KCIC,KCIC,OK,0.748,Chico,"Chico (CA), USA",OWNO,CHICO MUNI,"CHICO, CA - UNITED STATES",Chico Municipal Airport,"Chico, California, United States",OK
    CID,CID,KCID,KCID,OK,0.546,Cedar Rapids,"Cedar Rapids IA, USA",OWNO,THE EASTERN IOWA,"CEDAR RAPIDS, IA - UNITED STATES",The Eastern Iowa Airport,"Cedar Rapids, Iowa, United States",OK
    CIE,////,////,YCOI,UNK,1.0,Collie,"Collie, Australia",OWNO,////,////,????,"Collie, Western Australia, Australia",OK
    CIF,////,////,ZBCF,UNK,1.0,Chifeng,"Chifeng, PR China",OWNO,////,////,????,"Chifeng, Inner Mongolia, China",OK
    CIG,CAG,KCAG,KCAG,OK,1.0,Craig-Moffat,"Craig (CO), USA",OWNO,CRAIG-MOFFAT,"CRAIG, CO - UNITED STATES",Craig-Moffat Airport,"Craig, Colorado, United States",OK
    CIH,////,////,ZBCZ,UNK,0.8,Changzhi,"Changzhi, PR China",OWNO,////,////,Changzhi Wangcun Airport,"Changzhi, Shaanxi, China",OK
    CIJ,////,////,SLCO,UNK,0.647,E. Beltram,"Cobija, Bolivia",OWNO,////,////,Capitan Anibal Arab,"Cobija, Nicolás Suárez, Pando, Bolivia",OK
    CIK,CIK,PACI,PACI,OK,1.0,Chalkyitsik,"Chalkyitsik (AK), USA",OWNO,CHALKYITSIK,"CHALKYITSIK, AK - UNITED STATES",????,"Chalkyitsik, Alaska, United States",OK
    CIL,K29,////,////,OK,0.521,Melsing Creek,"Council (AK), USA",OWNO,COUNCIL,"COUNCIL, AK - UNITED STATES",????,"Council, Alaska, United States",OK
    CIM,////,////,SKCM,UNK,1.0,Cimitarra,"Cimitarra, Colombia",OWNO,////,////,Cimitarra Airport,"Cimitarra, Santander, Colombia",OK
    CIN,CIN,KCIN,KCIN,OK,0.421,Carroll,"Carroll IA, USA",OWNO,ARTHUR N NEU,"CARROLL, IA - UNITED STATES",Arthur N Neu Airport,"Carroll, Iowa, United States",OK
    CIO,////,////,SGCO,UNK,0.533,MCAL Lopez,"Concepcion, Paraguay",OWNO,////,////,Teniente Coronel Carmelo Peralta Airport,"Concepción, Concepción, Paraguay",OK
    CIP,////,////,FLCP,UNK,1.0,Chipata,"Chipata, Zambia",OWNO,////,////,????,"Chipata, Zambia",OK
    CIQ,////,////,////,UNK,1.0,Chiquimula,"Chiquimula, Guatemala",OWNO,////,////,????,"Chiquimula, Chiquimula, Guatemala",OK
    CIR,CIR,KCIR,KCIR,OK,0.719,Cairo,"Cairo (IL), USA",OWNO,CAIRO RGNL,"CAIRO, IL - UNITED STATES",Cairo Regional,"Cairo, Illinois, United States",OK
    CIS,////,////,PCIS,UNK,1.0,Canton Island,"Canton Island, Kiribati",OWNO,////,////,????,"Canton Island, Kiribati",OK
    CIT,////,////,UAII,UNK,0.75,Shimkent,"Shimkent, Kazakhstan",OWNO,////,////,Shymkent International Airport,"Shymkent, Ongtüstik Qazaqstan, Kazakhstan",OK


List for checking certainty of Great Circle Mapper (CNS - CST)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    CNS,////,////,YBCS,UNK,0.706,Cairns,"Cairns, Australia",OWNO,////,////,Cairns International Airport,"Cairns, Queensland, Australia",OK
    CNT,////,////,////,UNK,1.0,Charata,"Charata, Argentina",OWNO,////,////,????,"Charata, Chubut, Argentina",OK
    CNU,CNU,KCNU,KCNU,OK,1.0,Martin Johnson,"Chanute (KS), USA",OWNO,CHANUTE MARTIN JOHNSON,"CHANUTE, KS - UNITED STATES",Chanute Martin Johnson Airport,"Chanute, Kansas, United States",OK
    CNV,////,////,SNED,UNK,1.0,Canavieiras,"Canavieiras, Brazil",OWNO,////,////,????,"Canavieiras, Bahia, Brazil",OK
    CNW,CNW,KCNW,KCNW,OK,0.284,James Connall,"Waco (TX), USA",OWNO,TSTC WACO,"WACO, TX - UNITED STATES",TSTC Waco Airport,"Waco, Texas, United States",OK
    CNX,////,////,VTCC,UNK,1.0,International,"Chiang Mai, Thailand",OWNO,////,////,Chiang Mai International Airport,"Chiang Mai, Chiang Mai, Thailand",OK
    CNY,CNY,KCNY,KCNY,OK,1.0,Canyonlands Field,"Moab (UT), USA",OWNO,CANYONLANDS FIELD,"MOAB, UT - UNITED STATES",Canyonlands Field,"Moab, Utah, United States",OK
    CNZ,////,////,////,UNK,1.0,Cangamba,"Cangamba, Angola",OWNO,////,////,????,"Cangamba, Angola",OK
    COA,O22,////,////,OK,1.0,Columbia,"Columbia (CA), USA",OWNO,COLUMBIA,"COLUMBIA, CA - UNITED STATES",????,"Columbia, California, United States",OK
    COB,////,////,////,UNK,1.0,Coolibah,"Coolibah, Australia",OWNO,////,////,????,"Coolibah, Northern Territory, Australia",OK
    COC,////,////,SAAC,UNK,0.452,Concordia,"Concordia, Argentina",OWNO,////,////,Comodoro Pierrestegui,"Concordia, Entre Ríos, Argentina",OK
    COD,COD,KCOD,KCOD,OK,1.0,Yellowstone Regional,"Cody/Yellowstone (WY), USA",OWNO,YELLOWSTONE RGNL,"CODY, WY - UNITED STATES",Yellowstone Regional,"Cody, Wyoming, United States",OK
    COE,COE,KCOE,KCOE,OK,0.401,Coeur D'Alene,"Coeur D'Alene (ID), USA",OWNO,COEUR D'ALENE - PAPPY BOYINGTON FIELD,"COEUR D'ALENE, ID - UNITED STATES",Coeur d'Alene - Pappy Boyington Field,"Coeur d'Alene, Idaho, United States",OK
    COF,COF,KCOF,KCOF,OK,1.0,Patrick AFB,"Cocoa (FL), USA",OWNO,PATRICK AFB,"COCOA BEACH, FL - UNITED STATES",Patrick AFB,"Cocoa Beach, Florida, United States",OK
    COG,////,////,SKCD,UNK,1.0,Mandinga,"Condoto, Colombia",OWNO,////,////,Mandinga Airport,"Condoto, Chocó, Colombia",OK
    COH,////,////,VECO,UNK,1.0,Cooch Behar,"Cooch Behar, India",OWNO,////,////,????,"Cooch Behar, West Bengal, India",OK
    COI,COI,KCOI,KCOI,OK,1.0,Merritt Island,"Cocoa (FL), USA",OWNO,MERRITT ISLAND,"MERRITT ISLAND, FL - UNITED STATES",????,"Merritt Island, Florida, United States",OK
    COJ,////,////,YCBB,UNK,1.0,Coonabarabran,"Coonabarabran, Australia",OWNO,////,////,????,"Coonabarabran, New South Wales, Australia",OK
    COK,////,////,VOCI,UNK,0.5,Cochin,"Cochin, India",OWNO,////,////,Kochi International Airport,"Kochi, Kerala, India",TO DO CHECK
    COL,////,////,EGEL,UNK,1.0,Coll Island,"Coll Island, United Kingdom",OWNO,////,////,????,"Coll Island, Scotland, United Kingdom",OK
    COM,COM,KCOM,KCOM,OK,0.766,Coleman,"Coleman (TX), USA",OWNO,COLEMAN MUNI,"COLEMAN, TX - UNITED STATES",Coleman Municipal Airport,"Coleman, Texas, United States",OK
    CON,CON,KCON,KCON,OK,1.0,Concord Municipal Airport,"Concord (NH), USA",OWNO,CONCORD MUNI,"CONCORD, NH - UNITED STATES",Concord Municipal Airport,"Concord, New Hampshire, United States",OK
    COO,////,////,DBBB,UNK,0.313,Cotonou,"Cotonou, Benin",OWNO,////,////,Cardinal Bernardin Gantin International Airport,"Cotonou, Littoral, Benin",OK
    COP,K23,////,////,OK,0.64,Cooperstown,"Cooperstown (NY), USA",OWNO,COOPERSTOWN-WESTVILLE,"COOPERSTOWN, NY - UNITED STATES",Cooperstown-Westville Airport,"Cooperstown, New York, United States",OK
    COQ,////,////,ZMCD,UNK,1.0,Choibalsan,"Choibalsan, Mongolia",OWNO,////,////,????,"Choibalsan, Mongolia",OK
    COR,////,////,SACO,UNK,0.448,Pajas Blancas,"Cordoba, Argentina",OWNO,////,////,Ingeniero Aeronáutico Ambrosio L.V. Taravella International Airport,"Córdoba, Córdoba, Argentina",OK
    COS,COS,KCOS,KCOS,OK,0.426,Peterson Field,"Colorado Springs (CO), USA",OWNO,CITY OF COLORADO SPRINGS MUNI,"COLORADO SPRINGS, CO - UNITED STATES",City of Colorado Springs Municipal Airport,"Colorado Springs, Colorado, United States",OK
    COT,COT,KCOT,KCOT,OK,0.41,Cotulla,"Cotulla (TX), USA",OWNO,COTULLA-LA SALLE COUNTY,"COTULLA, TX - UNITED STATES",Cotulla-La Salle County Airport,"Cotulla, Texas, United States",OK
    COU,COU,KCOU,KCOU,OK,1.0,Columbia Regional,"Columbia (MO), USA",OWNO,COLUMBIA RGNL,"COLUMBIA, MO - UNITED STATES",Columbia Regional,"Columbia, Missouri, United States",OK
    COV,////,////,LPCV,UNK,1.0,Covilha,"Covilha, Portugal",OWNO,////,////,????,"Covilhã, Castelo Branco, Portugal",OK
    COW,////,////,SCQB,UNK,0.706,Coquimbo,"Coquimbo, Chile",OWNO,////,////,Ciudad,"Coquimbo, Coquimbo, Chile",OK
    COX,////,MYAK,MYAK,OK,1.0,Congo Town,"Congo Town, Bahamas",OWNO,CONGO TOWN,"CONGO TOWN, - BAHAMAS",????,"Congo Town, Andros Island, Bahamas",OK
    COY,////,////,YCWY,UNK,0.733,Coolawanyah,"Coolawanyah, Australia",OWNO,////,////,????,"Coolawanyah Station, Western Australia, Australia",MAYBE
    COZ,////,////,MDCZ,UNK,0.667,Constanza Airport,"Constanza, Dominican Republic",OWNO,////,////,Constanza National Airport,"Constanza, La Vega, Dominican Republic",OK
    CPA,////,////,GLCP,UNK,1.0,A. Tubman,"Cape Palmas, Liberia",OWNO,////,////,A. Tubman,"Cape Palmas, Liberia",OK
    CPB,////,////,SKCA,UNK,1.0,Capurgana,"Capurgana, Colombia",OWNO,////,////,Capurganá Airport,"Capurganá, Chocó, Colombia",OK
    CPC,////,////,SAZY,UNK,1.0,Chapelco,"San Martin DeLos Andes, Argentina",OWNO,////,////,Chapelco,"San Martin de los Andes, Neuquén, Argentina",OK
    CPD,////,////,YCBP,UNK,1.0,Coober Pedy,"Coober Pedy, Australia",OWNO,////,////,????,"Coober Pedy, South Australia, Australia",OK
    CPE,////,////,MMCP,UNK,0.476,Campeche International,"Campeche, Mexico",OWNO,////,////,Ingeniero Alberto Acuña Ongay International Airport,"Campeche, Campeche, México",OK
    CPF,////,////,WRSC,UNK,1.0,Cepu,"Cepu, Indonesia",OWNO,////,////,????,"Cepu, Jawa Tengah, Indonesia",OK
    CPG,////,////,////,UNK,1.0,Carmen De Patagones,"Carmen De Patagones, Argentina",OWNO,////,////,????,"Carmen de Patagones, Buenos Aires, Argentina",OK
    CPH,////,////,EKCH,UNK,0.583,Copenhagen Airport,"Copenhagen, Denmark",OWNO,////,////,København Airport - Kastrup,"Copenhagen, Denmark",OK
    CPL,////,////,SKHA,UNK,0.474,Chaparral,"Chaparral, Colombia",OWNO,////,////,General Navas Pardo Airport,"Chaparral, Tolima, Colombia",OK
    CPM,CPM,KCPM,KCPM,OK,1.0,Compton,"Compton (CA), USA",OWNO,COMPTON/WOODLEY,"COMPTON, CA - UNITED STATES",Compton/Woodley Airport,"Compton, California, United States",OK
    CPN,////,////,////,UNK,1.0,Cape Rodney,"Cape Rodney, Papua New Guinea",OWNO,////,////,????,"Cape Rodney, Central, Papua-New Guinea",OK
    CPO,////,////,SCAT,UNK,0.545,Chamonate,"Copiapo, Chile",OWNO,////,////,Desierto de Atacama Airport,"Copiapó, Atacama, Chile",OK
    CPQ,////,////,SDAM,UNK,0.645,International,"Campinas, Brazil",OWNO,////,////,Amarais,"Campinas, São Paulo, Brazil",OK
    CPR,CPR,KCPR,KCPR,OK,1.0,Casper,"Casper (WY), USA",OWNO,CASPER/NATRONA COUNTY INTL,"CASPER, WY - UNITED STATES",Casper/Natrona County International Airport,"Casper, Wyoming, United States",OK
    CPS,CPS,KCPS,KCPS,OK,1.0,Bi-State Parks (Downtown Airport),"St. Louis (MO), USA",OWNO,ST LOUIS DOWNTOWN,"CAHOKIA/ST LOUIS, IL - UNITED STATES",St. Louis Downtown Airport,"Cahokia/St. Louis, Illinois, United States",OK
    CPT,////,////,FACT,UNK,1.0,Cape Town International,"Cape Town, South Africa",OWNO,////,////,Cape Town International Airport,"Cape Town, Western Cape, South Africa",OK
    CPU,////,////,SNCU,UNK,0.636,Cururupu,"Cururupu, Brazil",OWNO,////,////,Aeropuerto Cururupu,"Cururupu, Maranhão, Brazil",OK
    CPV,////,////,SBKG,UNK,1.0,Joao Suassuna,"Campina Grande, Brazil",OWNO,////,////,Joao Suassuna,"Campina Grande, Paraíba, Brazil",OK
    CPX,CPX,TJCP,TJCP,OK,0.105,Culebra,"Culebra, Puerto Rico",OWNO,BENJAMIN RIVERA NORIEGA,"ISLA DE CULEBRA, PR - UNITED STATES",Benjamin Rivera Noriega Airport,"Isla De Culebra, Puerto Rico, United States",MAYBE
    CQD,////,////,OIFS,UNK,1.0,Shahre-Kord,"Shahre-Kord, Iran",OWNO,////,////,????,"Shahre-Kord, Chahar Mahall va Bakhtiari, Iran",OK
    CQF,////,////,LFAC,UNK,0.545,Calais,"Calais, France",OWNO,////,////,????,"Calais-Dunkerque, Nord-Pas-de-Calais, France",TO DO CHECK
    CQM,////,////,LERL,UNK,nan,////,////,////,////,////,Ciudad Real Central Airport,"Ciudad Real, Castille-La Mancha, Spain",UNK
    CQP,////,////,YCFL,UNK,1.0,Cape Flattery,"Cape Flattery, Australia",OWNO,////,////,????,"Cape Flattery, Queensland, Australia",OK
    CQS,////,////,SWCQ,UNK,1.0,Costa Marques,"Costa Marques, Brazil",OWNO,////,////,????,"Costa Marques, Rondônia, Brazil",OK
    CQT,////,////,////,UNK,1.0,Caquetania,"Caquetania, Colombia",OWNO,////,////,????,"Caquetania, Caquetá, Colombia",OK
    CRA,////,////,LRCV,UNK,1.0,Craiova,"Craiova, Romania",OWNO,////,////,????,"Craiova, Romania",OK
    CRB,////,////,YCBR,UNK,1.0,Collarenebri,"Collarenebri, Australia",OWNO,////,////,????,"Collarenebri, New South Wales, Australia",OK
    CRC,////,////,SKGO,UNK,0.6,Cartago,"Cartago, Colombia",OWNO,////,////,Santa Ana,"Cartago, Valle del Cauca, Colombia",OK
    CRD,////,////,SAVC,UNK,0.682,Comodoro Rivadavia,"Comodoro Rivadavia, Argentina",OWNO,////,////,General E. Mosconi,"Comodoro Rivadavia, Chubut, Argentina",OK
    CRE,CRE,KCRE,KCRE,OK,1.0,Grand Strand Airport,"Myrtle Beach (SC), USA",OWNO,GRAND STRAND,"NORTH MYRTLE BEACH, SC - UNITED STATES",Grand Strand Airport,"North Myrtle Beach, South Carolina, United States",OK
    CRF,////,////,FEFC,UNK,1.0,Carnot,"Carnot, Central African Republic",OWNO,////,////,????,"Carnot, Haute-Sangha/Mambéré-Kadéï (Tö-Sangä/Mbaere-Kadeï), Central African Republic",OK
    CRG,CRG,KCRG,KCRG,OK,0.552,Craig Municipal,"Jacksonville (FL), USA",OWNO,JACKSONVILLE EXECUTIVE AT CRAIG,"JACKSONVILLE, FL - UNITED STATES",Jacksonville Executive Airport at Craig,"Jacksonville, Florida, United States",OK
    CRH,////,////,////,UNK,1.0,Cherribah,"Cherribah, Australia",OWNO,////,////,????,"Cherribah, Queensland, Australia",OK
    CRI,////,MYCI,MYCI,OK,0.4,Crooked Island,"Crooked Island, Bahamas",OWNO,COLONEL HILL,"COLONEL HILL, - BAHAMAS",????,"Colonel Hill, Crooked Island, Bahamas",TO DO CHECK
    CRJ,////,////,////,UNK,1.0,Coorabie,"Coorabie, Australia",OWNO,////,////,????,"Coorabie, South Australia, Australia",OK
    CRK,////,////,RPLC,UNK,0.778,Clark Field,"Luzon Island, Philippines",OWNO,////,////,Clark International Airport,"Luzon, Philippines",MAYBE
    CRL,////,////,EBCI,UNK,0.857,Charleroi,"Brussels, Belgium",OWNO,////,////,Brussels South Charleroi Airport,"Charleroi, Hainaut, Belgium",OK
    CRM,////,////,RPVF,UNK,0.727,National,"Catarman, Philippines",OWNO,////,////,????,"Catarman, Samar Island, Philippines",OK
    CRP,CRP,KCRP,KCRP,OK,1.0,International,"Corpus Christi (TX), USA",OWNO,CORPUS CHRISTI INTL,"CORPUS CHRISTI, TX - UNITED STATES",Corpus Christi International Airport,"Corpus Christi, Texas, United States",OK
    CRQ,////,////,SBCV,UNK,1.0,Caravelas,"Caravelas, Brazil",OWNO,////,////,????,"Caravelas, Bahia, Brazil",OK
    CRR,////,////,SANW,UNK,1.0,Ceres,"Ceres, Argentina",OWNO,////,////,Ceres Airport,"Ceres, Santa Fe, Argentina",OK
    CRS,CRS,KCRS,KCRS,OK,0.297,Corsicana,"Corsicana (TX), USA",OWNO,C DAVID CAMPBELL FIELD-CORSICANA MUNI,"CORSICANA, TX - UNITED STATES",C David Campbell Field-Corsicana Municipal Airport,"Corsicana, Texas, United States",OK
    CRT,CRT,KCRT,KCRT,OK,0.554,Municipal,"Crossett (AR), USA",OWNO,Z M JACK STELL FIELD,"CROSSETT, AR - UNITED STATES",Z M Jack Stell Field,"Crossett, Arkansas, United States",OK
    CRU,////,////,TGPZ,UNK,0.743,Carriacou Island,"Carriacou Island, Grenada",OWNO,////,////,Lauriston,"Hillsborough, Carriacou, Grenada",MAYBE
    CRV,////,////,LIBC,UNK,1.0,Crotone,"Crotone, Italy",OWNO,////,////,????,"Crotone, Calabria, Italy",OK
    CRW,CRW,KCRW,KCRW,OK,1.0,Yeager Airport,"Charleston (WV), USA",OWNO,YEAGER,"CHARLESTON, WV - UNITED STATES",Yeager Airport,"Charleston, West Virginia, United States",OK
    CRX,CRX,KCRX,KCRX,OK,1.0,Roscoe Turner,"Corinth (MS), USA",OWNO,ROSCOE TURNER,"CORINTH, MS - UNITED STATES",Roscoe Turner Airport,"Corinth, Mississippi, United States",OK
    CRY,////,////,////,UNK,1.0,Carlton Hill,"Carlton Hill, Australia",OWNO,////,////,????,"Carlton Hill, Western Australia, Australia",OK
    CRZ,////,////,UTAV,UNK,0.909,Turkmenabad,"Turkmenabad, Turkmenistan",OWNO,////,////,Türkmenabat Airport,"Türkmenabat, Lebap, Turkmenistan",OK
    CSA,////,////,EGEY,UNK,1.0,Colonsay Island,"Colonsay Island, United Kingdom",OWNO,////,////,Colonsay Island Airport,"Colonsay, Colonsay Island, Hebrides, Scotland, United Kingdom",OK
    CSB,////,////,LRCS,UNK,1.0,Caransebes,"Caransebes, Romania",OWNO,////,////,????,"Caransebes, Romania",OK
    CSC,////,////,MRCA,UNK,0.5,Canas,"Canas, Costa Rica",OWNO,////,////,Codela,"Guapiles, Limón, Costa Rica",TO DO CHECK
    CSD,////,////,////,UNK,1.0,Cresswell Downs,"Cresswell Downs, Australia",OWNO,////,////,????,"Cresswell Downs, Northern Territory, Australia",OK
    CSE,0CO2,////,////,OK,0.602,Crested Butte,"Crested Butte (CO), USA",OWNO,BUCKHORN RANCH,"CRESTED BUTTE, CO - UNITED STATES",Buckhorn Ranch Airport,"Crested Butte, Colorado, United States",OK
    CSF,////,////,LFPC,UNK,1.0,Creil,"Creil, France",OWNO,////,////,Creil Airport,"Creil, Picardie (Picardy), France",OK
    CSG,CSG,KCSG,KCSG,OK,1.0,////,////,////,COLUMBUS,"COLUMBUS, GA - UNITED STATES",????,"Columbus, Georgia, United States",OK
    CSH,////,////,ULAS,UNK,nan,////,////,////,////,////,Solovki Airport,"Solovetsky, Solovetsky Islands, Arkhangel'skaya, Russian Federation (Russia)",UNK
    CSI,////,////,YCAS,UNK,1.0,Casino,"Casino, Australia",OWNO,////,////,????,"Casino, New South Wales, Australia",OK
    CSK,////,////,GOGS,UNK,1.0,Cap Skirring,"Cap Skirring, Senegal",OWNO,////,////,Cap Skirring Airport,"Cap Skirring, Ziguinchor, Senegal",OK
    CSM,CSM,KCSM,KCSM,OK,0.534,Sherman,"Clinton (OK), USA",OWNO,CLINTON-SHERMAN,"CLINTON, OK - UNITED STATES",Clinton-Sherman Industrial Airpark,"Burns Flat, Oklahoma, United States",OK
    CSN,CXP,KCXP,KCXP,OK,1.0,Carson City,"Carson City (NV), USA",OWNO,CARSON,"CARSON CITY, NV - UNITED STATES",Carson Airport,"Carson City, Nevada, United States",OK
    CSO,////,////,EDBC,UNK,0.9,Magdeburg-Cochstedt Airport,"Magdeburg, Germany",OWNO,////,////,Magdeburg-Cochstedt International Airport,"Cochstedt, Saxony-Anhalt, Germany",OK
    CSQ,CSQ,KCSQ,KCSQ,OK,1.0,Municipal,"Creston IA, USA",OWNO,CRESTON MUNI,"CRESTON, IA - UNITED STATES",Creston Municipal Airport,"Creston, Iowa, United States",OK
    CSR,////,////,////,UNK,1.0,Casuarito,"Casuarito, Colombia",OWNO,////,////,????,"Casuarito, Vichada, Colombia",OK
    CSS,////,////,SSCL,UNK,1.0,Cassilandia,"Cassilandia, Brazil",OWNO,////,////,Cassilândia,"Cassilândia, Mato Grosso do Sul, Brazil",OK
    CST,////,////,NFCS,UNK,0.533,Castaway,"Castaway, Fiji",OWNO,////,////,????,"Castaway Island Resort, Qalito Island, Fiji",MAYBE

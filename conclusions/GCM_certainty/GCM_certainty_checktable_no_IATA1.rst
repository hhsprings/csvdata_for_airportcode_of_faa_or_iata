
List for checking certainty of Great Circle Mapper (ICAO ZYJZ - FAA LID AKH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ////,////,////,ZYJZ,UNK,nan,////,////,////,////,////,Jinzhou Xiaolingzi Airport,"Jinzhou, Liaoning, China",UNK
    ////,04G,////,////,OK,1.0,////,////,////,LANSDOWNE,"YOUNGSTOWN, OH - UNITED STATES",Lansdowne Airport,"Youngstown, Ohio, United States",OK
    ////,06C,////,////,OK,1.0,////,////,////,SCHAUMBURG RGNL,"CHICAGO/SCHAUMBURG, IL - UNITED STATES",Schaumburg Regional,"Chicago/Schaumburg, Illinois, United States",OK
    ////,06N,////,////,OK,1.0,////,////,////,RANDALL,"MIDDLETOWN, NY - UNITED STATES",Randall Airport,"Middletown, New York, United States",OK
    ////,07MT,////,////,OK,1.0,////,////,////,GLASGOW INDUSTRIAL,"GLASGOW, MT - UNITED STATES",Glasgow Industrial Airport,"Glasgow, Montana, United States",OK
    ////,09J,////,////,OK,1.0,////,////,////,JEKYLL ISLAND,"JEKYLL ISLAND, GA - UNITED STATES",????,"Jekyll Island, Georgia, United States",OK
    ////,0A9,////,////,OK,1.0,////,////,////,ELIZABETHTON MUNI,"ELIZABETHTON, TN - UNITED STATES",Elizabethton Municipal Airport,"Elizabethton, Tennessee, United States",OK
    ////,0G6,////,////,OK,1.0,////,////,////,WILLIAMS COUNTY,"BRYAN, OH - UNITED STATES",Williams County Airport,"Bryan, Ohio, United States",OK
    ////,0G7,////,////,OK,1.0,////,////,////,FINGER LAKES RGNL,"SENECA FALLS, NY - UNITED STATES",Finger Lakes Regional,"Seneca Falls, New York, United States",OK
    ////,0P2,////,////,OK,1.0,////,////,////,SHOESTRING AVIATION AIRFIELD,"STEWARTSTOWN, PA - UNITED STATES",Shoestring Aviation Airfield,"Stewartstown, Pennsylvania, United States",OK
    ////,0W3,////,////,OK,1.0,////,////,////,HARFORD COUNTY,"CHURCHVILLE, MD - UNITED STATES",Harford County Airport,"Churchville, Maryland, United States",OK
    ////,0WI8,////,////,OK,1.0,////,////,////,OCONOMOWOC,"OCONOMOWOC, WI - UNITED STATES",????,"Oconomowoc, Wisconsin, United States",OK
    ////,10C,////,////,OK,1.0,////,////,////,GALT FIELD,"GREENWOOD/WONDER LAKE, IL - UNITED STATES",Galt Field,"Greenwood/Wonder Lake, Illinois, United States",OK
    ////,17G,////,////,OK,1.0,////,////,////,PORT BUCYRUS-CRAWFORD COUNTY,"BUCYRUS, OH - UNITED STATES",Port Bucyrus-Crawford County Airport,"Bucyrus, Ohio, United States",OK
    ////,18AZ,////,////,OK,1.0,////,////,////,SKY RANCH AT CAREFREE,"CAREFREE, AZ - UNITED STATES",Sky Ranch Airport at Carefree,"Carefree, Arizona, United States",OK
    ////,1A3,////,////,OK,1.0,////,////,////,MARTIN CAMPBELL FIELD,"COPPERHILL, TN - UNITED STATES",Martin Campbell Field,"Copperhill, Tennessee, United States",OK
    ////,1AZ0,////,////,OK,1.0,////,////,////,MOBILE,"MOBILE, AZ - UNITED STATES",????,"Mobile, Arizona, United States",OK
    ////,1B9,////,////,OK,1.0,////,////,////,MANSFIELD MUNI,"MANSFIELD, MA - UNITED STATES",Mansfield Municipal Airport,"Mansfield, Massachusetts, United States",OK
    ////,1C9,////,////,OK,1.0,////,////,////,FRAZIER LAKE AIRPARK,"HOLLISTER, CA - UNITED STATES",Frazier Lake Airpark,"Hollister, California, United States",OK
    ////,1G3,////,////,OK,0.87,////,////,////,KENT STATE UNIV,"KENT, OH - UNITED STATES",Kent State University Airport,"Kent, Ohio, United States",OK
    ////,1H2,////,////,OK,1.0,////,////,////,EFFINGHAM COUNTY MEMORIAL,"EFFINGHAM, IL - UNITED STATES",Effingham County Memorial Airport,"Effingham, Illinois, United States",OK
    ////,1O2,////,////,OK,1.0,////,////,////,LAMPSON FIELD,"LAKEPORT, CA - UNITED STATES",Lampson Field,"Lakeport, California, United States",OK
    ////,1O6,////,////,OK,1.0,////,////,////,DUNSMUIR MUNI-MOTT,"DUNSMUIR, CA - UNITED STATES",Dunsmuir Municipal-Mott,"Dunsmuir, California, United States",OK
    ////,1RL,////,////,OK,1.0,////,////,////,POINT ROBERTS AIRPARK,"POINT ROBERTS, WA - UNITED STATES",Point Roberts Airpark,"Point Roberts, Washington, United States",OK
    ////,20GA,////,////,OK,1.0,////,////,////,EARL L SMALL JR FIELD/STOCKMAR,"VILLA RICA, GA - UNITED STATES",Earl L Small Jr. Field/Stockmar Airport,"Villa Rica, Georgia, United States",OK
    ////,23M,////,////,OK,1.0,////,////,////,CLARKE COUNTY,"QUITMAN, MS - UNITED STATES",Clarke County Airport,"Quitman, Mississippi, United States",OK
    ////,24C,////,////,OK,1.0,////,////,////,LOWELL CITY,"LOWELL, MI - UNITED STATES",Lowell City Airport,"Lowell, Michigan, United States",OK
    ////,24J,////,////,OK,1.0,////,////,////,SUWANNEE COUNTY,"LIVE OAK, FL - UNITED STATES",Suwannee County Airport,"Live Oak, Florida, United States",OK
    ////,25D,////,////,OK,1.0,////,////,////,FOREST LAKE,"FOREST LAKE, MN - UNITED STATES",????,"Forest Lake, Minnesota, United States",OK
    ////,26AK,PACS,PACS,OK,1.0,////,////,////,CAPE SARICHEF,"CAPE SARICHEF, AK - UNITED STATES",????,"Cape Sarichef, Alaska, United States",OK
    ////,29D,////,////,OK,1.0,////,////,////,GROVE CITY,"GROVE CITY, PA - UNITED STATES",????,"Grove City, Pennsylvania, United States",OK
    ////,2A0,////,////,OK,1.0,////,////,////,MARK ANTON,"DAYTON, TN - UNITED STATES",Mark Anton Airport,"Dayton, Tennessee, United States",OK
    ////,2B2,////,////,OK,1.0,////,////,////,PLUM ISLAND,"NEWBURYPORT, MA - UNITED STATES",Plum Island Airport,"Newburyport, Massachusetts, United States",OK
    ////,2G2,////,////,OK,1.0,////,////,////,JEFFERSON COUNTY AIRPARK,"STEUBENVILLE, OH - UNITED STATES",Jefferson County Airpark,"Steubenville, Ohio, United States",OK
    ////,2G4,////,////,OK,1.0,////,////,////,GARRETT COUNTY,"OAKLAND, MD - UNITED STATES",Garrett County Airport,"Oakland, Maryland, United States",OK
    ////,2G9,////,////,OK,1.0,////,////,////,SOMERSET COUNTY,"SOMERSET, PA - UNITED STATES",Somerset County Airport,"Somerset, Pennsylvania, United States",OK
    ////,2H0,////,////,OK,1.0,////,////,////,SHELBY COUNTY,"SHELBYVILLE, IL - UNITED STATES",Shelby County Airport,"Shelbyville, Illinois, United States",OK
    ////,2J9,////,////,OK,1.0,////,////,////,QUINCY MUNI,"QUINCY, FL - UNITED STATES",Quincy Municipal Airport,"Quincy, Florida, United States",OK
    ////,2O1,////,////,OK,1.0,////,////,////,GANSNER FIELD,"QUINCY, CA - UNITED STATES",Gansner Field,"Quincy, California, United States",OK
    ////,2XS8,////,////,OK,1.0,////,////,////,BENSON AIRSTRIP,"UVALDE, TX - UNITED STATES",Benson Airstrip,"Uvalde, Texas, United States",OK
    ////,36U,////,////,OK,1.0,////,////,////,HEBER CITY MUNI - RUSS MCDONALD FIELD,"HEBER, UT - UNITED STATES",Heber City Municipal - Russ McDonald Field,"Heber City, Utah, United States",OK
    ////,37AA,PAAD,PAAD,OK,1.0,////,////,////,POINT THOMSON AIRSTRIP,"DEADHORSE, AK - UNITED STATES",Point Thomson Airstrip,"Deadhorse, Alaska, United States",OK
    ////,38W,////,////,OK,1.0,////,////,////,LYNDEN MUNICIPAL AIRPORT JANSEN FIELD,"LYNDEN, WA - UNITED STATES",Lynden Municipal Jansen Field,"Lynden, Washington, United States",OK
    ////,3D2,////,////,OK,1.0,////,////,////,EPHRAIM-GIBRALTAR,"EPHRAIM, WI - UNITED STATES",Ephraim-Gibraltar Airport,"Ephraim, Wisconsin, United States",OK
    ////,3G3,////,////,OK,1.0,////,////,////,WADSWORTH MUNI,"WADSWORTH, OH - UNITED STATES",Wadsworth Municipal Airport,"Wadsworth, Ohio, United States",OK
    ////,3G4,////,////,OK,1.0,////,////,////,ASHLAND COUNTY,"ASHLAND, OH - UNITED STATES",Ashland County Airport,"Ashland, Ohio, United States",OK
    ////,3J1,////,////,OK,1.0,////,////,////,RIDGELAND,"RIDGELAND, SC - UNITED STATES",????,"Ridgeland, South Carolina, United States",OK
    ////,3O9,////,////,OK,1.0,////,////,////,GRAND LAKE RGNL,"AFTON, OK - UNITED STATES",Grand Lake Regional,"Afton, Oklahoma, United States",OK
    ////,3T4,////,////,OK,1.0,////,////,////,TETLIN,"TETLIN, AK - UNITED STATES",????,"Tetlin, Alaska, United States",OK
    ////,3W2,////,////,OK,1.0,////,////,////,PUT IN BAY,"PUT IN BAY, OH - UNITED STATES",????,"Put In Bay, Ohio, United States",OK
    ////,3W7,////,////,OK,1.0,////,////,////,GRAND COULEE DAM,"ELECTRIC CITY, WA - UNITED STATES",Grand Coulee Dam Airport,"Electric City, Washington, United States",OK
    ////,41N,////,////,OK,1.0,////,////,////,BRACEVILLE,"NEWTON FALLS, OH - UNITED STATES",Braceville Airport,"Newton Falls, Ohio, United States",OK
    ////,49A,////,////,OK,1.0,////,////,////,GILMER COUNTY,"ELLIJAY, GA - UNITED STATES",Gilmer County Airport,"Ellijay, Georgia, United States",OK
    ////,49X,////,////,OK,1.0,////,////,////,CHEMEHUEVI VALLEY,"CHEMEHUEVI VALLEY, CA - UNITED STATES",????,"Chemehuevi Valley, California, United States",OK
    ////,4A4,////,////,OK,1.0,////,////,////,POLK COUNTY AIRPORT- CORNELIUS MOORE FIELD,"CEDARTOWN, GA - UNITED STATES",Polk County Airport- Cornelius Moore Field,"Cedartown, Georgia, United States",OK
    ////,4A7,////,////,OK,1.0,////,////,////,ATLANTA SOUTH RGNL,"ATLANTA, GA - UNITED STATES",Atlanta South Regional,"Atlanta, Georgia, United States",OK
    ////,4A9,////,////,OK,1.0,////,////,////,ISBELL FIELD,"FORT PAYNE, AL - UNITED STATES",Isbell Field,"Fort Payne, Alabama, United States",OK
    ////,4B8,////,////,OK,1.0,////,////,////,ROBERTSON FIELD,"PLAINVILLE, CT - UNITED STATES",Robertson Field,"Plainville, Connecticut, United States",OK
    ////,4G0,////,////,OK,1.0,////,////,////,PITTSBURGH-MONROEVILLE,"MONROEVILLE, PA - UNITED STATES",Pittsburgh-Monroeville Airport,"Monroeville, Pennsylvania, United States",OK
    ////,4G2,////,////,OK,1.0,////,////,////,HAMBURG INC,"HAMBURG, NY - UNITED STATES",Hamburg Inc. Airport,"Hamburg, New York, United States",OK
    ////,4G4,////,////,OK,1.0,////,////,////,YOUNGSTOWN ELSER METRO,"YOUNGSTOWN, OH - UNITED STATES",Youngstown Elser Metro Airport,"Youngstown, Ohio, United States",OK
    ////,4I7,////,////,OK,1.0,////,////,////,PUTNAM COUNTY RGNL,"GREENCASTLE, IN - UNITED STATES",Putnam County Regional,"Greencastle, Indiana, United States",OK
    ////,4U9,////,////,OK,1.0,////,////,////,DELL FLIGHT STRIP,"DELL, MT - UNITED STATES",Dell Flight Strip,"Dell, Montana, United States",OK
    ////,4WA4,////,////,OK,1.0,////,////,////,WINDSOCK,"LOPEZ, WA - UNITED STATES",Windsock Airport,"Lopez Island, Washington, United States",OK
    ////,52A,////,////,OK,1.0,////,////,////,MADISON MUNI,"MADISON, GA - UNITED STATES",Madison Municipal Airport,"Madison, Georgia, United States",OK
    ////,54J,////,////,OK,1.0,////,////,////,DEFUNIAK SPRINGS,"DEFUNIAK SPRINGS, FL - UNITED STATES",????,"Defuniak Springs, Florida, United States",OK
    ////,55S,////,////,OK,1.0,////,////,////,PACKWOOD,"PACKWOOD, WA - UNITED STATES",????,"Packwood, Washington, United States",OK
    ////,57C,////,////,OK,1.0,////,////,////,EAST TROY MUNI,"EAST TROY, WI - UNITED STATES",East Troy Municipal Airport,"East Troy, Wisconsin, United States",OK
    ////,5B2,////,////,OK,1.0,////,////,////,SARATOGA COUNTY,"SARATOGA SPRINGS, NY - UNITED STATES",Saratoga County Airport,"Saratoga Springs, New York, United States",OK
    ////,60J,////,////,OK,1.0,////,////,////,ODELL WILLIAMSON MUNI,"OCEAN ISLE BEACH, NC - UNITED STATES",Odell Williamson Municipal Airport,"Ocean Isle Beach, North Carolina, United States",OK
    ////,66CA,////,////,OK,1.0,////,////,////,RANCHO SAN SIMEON,"CAMBRIA, CA - UNITED STATES",Rancho San Simeon Airport,"Cambria, California, United States",OK
    ////,6A2,////,////,OK,1.0,////,////,////,GRIFFIN-SPALDING COUNTY,"GRIFFIN, GA - UNITED STATES",Griffin-Spalding County Airport,"Griffin, Georgia, United States",OK
    ////,6J4,////,////,OK,1.0,////,////,////,SALUDA COUNTY,"SALUDA, SC - UNITED STATES",Saluda County Airport,"Saluda, South Carolina, United States",OK
    ////,6K8,PFTO,PFTO,OK,1.0,////,////,////,TOK JUNCTION,"TOK, AK - UNITED STATES",Tok Junction Airport,"Tok, Alaska, United States",OK
    ////,6S0,////,////,OK,1.0,////,////,////,BIG TIMBER,"BIG TIMBER, MT - UNITED STATES",????,"Big Timber, Montana, United States",OK
    ////,6S2,////,////,OK,1.0,////,////,////,FLORENCE MUNI,"FLORENCE, OR - UNITED STATES",Florence Municipal Airport,"Florence, Oregon, United States",OK
    ////,6Y8,////,////,OK,1.0,////,////,////,WELKE,"BEAVER ISLAND, MI - UNITED STATES",Welke Airport,"Beaver Island, Michigan, United States",OK
    ////,70J,////,////,OK,1.0,////,////,////,CAIRO-GRADY COUNTY,"CAIRO, GA - UNITED STATES",Cairo-Grady County Airport,"Cairo, Georgia, United States",OK
    ////,70N,////,////,OK,1.0,////,////,////,SPRING HILL,"STERLING, PA - UNITED STATES",Spring Hill Airport,"Sterling, Pennsylvania, United States",OK
    ////,7AK,PAUT,PAUT,OK,1.0,////,////,////,AKUTAN,"AKUTAN, AK - UNITED STATES",????,"Akutan, Alaska, United States",OK
    ////,7D9,////,////,OK,1.0,////,////,////,GERMACK,"GENEVA, OH - UNITED STATES",Germack Airport,"Geneva, Ohio, United States",OK
    ////,7FA1,////,////,OK,1.0,////,////,////,SUGAR LOAF SHORES,"KEY WEST, FL - UNITED STATES",Sugar Loaf Shores Airport,"Key West, Florida, United States",OK
    ////,7FL4,////,////,OK,1.0,////,////,////,HALLER AIRPARK,"GREEN COVE SPRINGS, FL - UNITED STATES",Haller Airpark,"Green Cove Springs, Florida, United States",OK
    ////,7N7,////,////,OK,1.0,////,////,////,SPITFIRE AERODROME,"PEDRICKTOWN, NJ - UNITED STATES",Spitfire Aerodrome,"Pedricktown, New Jersey, United States",OK
    ////,8M8,////,////,OK,1.0,////,////,////,EAGLE II,"LEWISTON, MI - UNITED STATES",Eagle II Airport,"Lewiston, Michigan, United States",OK
    ////,8XS8,////,////,OK,1.0,////,////,////,REESE AIRPARK,"LUBBOCK, TX - UNITED STATES",Reese Airpark,"Lubbock, Texas, United States",OK
    ////,90WA,////,////,OK,1.0,////,////,////,WALDRONAIRE,"EAST SOUND, WA - UNITED STATES",Waldronaire Airport,"East Sound, Washington, United States",OK
    ////,93C,////,////,OK,1.0,////,////,////,RICHLAND,"RICHLAND CENTER, WI - UNITED STATES",Richland Airport,"Richland Center, Wisconsin, United States",OK
    ////,99N,////,////,OK,1.0,////,////,////,BAMBERG COUNTY,"BAMBERG, SC - UNITED STATES",Bamberg County Airport,"Bamberg, South Carolina, United States",OK
    ////,9A5,////,////,OK,1.0,////,////,////,BARWICK LAFAYETTE,"LAFAYETTE, GA - UNITED STATES",Barwick Lafayette Airport,"Lafayette, Georgia, United States",OK
    ////,9G1,////,////,OK,1.0,////,////,////,PITTSBURGH NORTHEAST,"PITTSBURGH, PA - UNITED STATES",Pittsburgh Northeast Airport,"Pittsburgh, Pennsylvania, United States",OK
    ////,A39,////,////,OK,1.0,////,////,////,AK-CHIN RGNL,"MARICOPA, AZ - UNITED STATES",Ak-Chin Regional,"Maricopa, Arizona, United States",OK
    ////,A79,////,////,OK,1.0,////,////,////,CHIGNIK LAKE,"CHIGNIK LAKE, AK - UNITED STATES",????,"Chignik Lake, Alaska, United States",OK
    ////,AAA,KAAA,KAAA,OK,1.0,////,////,////,LOGAN COUNTY,"LINCOLN, IL - UNITED STATES",Logan County Airport,"Lincoln, Illinois, United States",OK
    ////,AAO,KAAO,KAAO,OK,1.0,////,////,////,COLONEL JAMES JABARA,"WICHITA, KS - UNITED STATES",Colonel James Jabara Airport,"Wichita, Kansas, United States",OK
    ////,AAS,KAAS,KAAS,OK,1.0,////,////,////,TAYLOR COUNTY,"CAMPBELLSVILLE, KY - UNITED STATES",Taylor County Airport,"Campbellsville, Kentucky, United States",OK
    ////,AAT,KAAT,KAAT,OK,1.0,////,////,////,ALTURAS MUNI,"ALTURAS, CA - UNITED STATES",Alturas Municipal Airport,"Alturas, California, United States",OK
    ////,ACJ,KACJ,KACJ,OK,1.0,////,////,////,JIMMY CARTER RGNL,"AMERICUS, GA - UNITED STATES",Jimmy Carter Regional,"Americus, Georgia, United States",OK
    ////,ACP,KACP,KACP,OK,1.0,////,////,////,ALLEN PARISH,"OAKDALE, LA - UNITED STATES",Allen Parish Airport,"Oakdale, Louisiana, United States",OK
    ////,ACQ,KACQ,KACQ,OK,1.0,////,////,////,WASECA MUNI,"WASECA, MN - UNITED STATES",Waseca Municipal Airport,"Waseca, Minnesota, United States",OK
    ////,ADC,KADC,KADC,OK,1.0,////,////,////,WADENA MUNI,"WADENA, MN - UNITED STATES",Wadena Municipal Airport,"Wadena, Minnesota, United States",OK
    ////,ADF,KADF,KADF,OK,1.0,////,////,////,DEXTER B FLORENCE MEMORIAL FIELD,"ARKADELPHIA, AR - UNITED STATES",Dexter B Florence Memorial Field,"Arkadelphia, Arkansas, United States",OK
    ////,ADU,KADU,KADU,OK,1.0,////,////,////,AUDUBON COUNTY,"AUDUBON, IA - UNITED STATES",Audubon County Airport,"Audubon, Iowa, United States",OK
    ////,AEG,KAEG,KAEG,OK,1.0,////,////,////,DOUBLE EAGLE II,"ALBUQUERQUE, NM - UNITED STATES",Double Eagle II Airport,"Albuquerque, New Mexico, United States",OK
    ////,AEJ,KAEJ,KAEJ,OK,1.0,////,////,////,CENTRAL COLORADO RGNL,"BUENA VISTA, CO - UNITED STATES",Central Colorado Regional,"Buena Vista, Colorado, United States",OK
    ////,AFK,KAFK,KAFK,OK,1.0,////,////,////,NEBRASKA CITY MUNI,"NEBRASKA CITY, NE - UNITED STATES",Nebraska City Municipal Airport,"Nebraska City, Nebraska, United States",OK
    ////,AFP,KAFP,KAFP,OK,1.0,////,////,////,ANSON COUNTY - JEFF CLOUD FIELD,"WADESBORO, NC - UNITED STATES",Anson County - Jeff Cloud Field,"Wadesboro, North Carolina, United States",OK
    ////,AHQ,KAHQ,KAHQ,OK,1.0,////,////,////,WAHOO MUNI,"WAHOO, NE - UNITED STATES",Wahoo Municipal Airport,"Wahoo, Nebraska, United States",OK
    ////,AIB,KAIB,KAIB,OK,1.0,////,////,////,HOPKINS FIELD,"NUCLA, CO - UNITED STATES",Hopkins Field,"Nucla, Colorado, United States",OK
    ////,AIG,KAIG,KAIG,OK,1.0,////,////,////,LANGLADE COUNTY,"ANTIGO, WI - UNITED STATES",Langlade County Airport,"Antigo, Wisconsin, United States",OK
    ////,AIT,KAIT,KAIT,OK,1.0,////,////,////,AITKIN MUNI-STEVE KURTZ FIELD,"AITKIN, MN - UNITED STATES",Aitkin Municipal-Steve Kurtz Field,"Aitkin, Minnesota, United States",OK
    ////,AJG,KAJG,KAJG,OK,1.0,////,////,////,MOUNT CARMEL MUNI,"MOUNT CARMEL, IL - UNITED STATES",Mount Carmel Municipal Airport,"Mount Carmel, Illinois, United States",OK
    ////,AJO,KAJO,KAJO,OK,1.0,////,////,////,CORONA MUNI,"CORONA, CA - UNITED STATES",Corona Municipal Airport,"Corona, California, United States",OK
    ////,AJR,KAJR,KAJR,OK,1.0,////,////,////,HABERSHAM COUNTY,"CORNELIA, GA - UNITED STATES",Habersham County Airport,"Cornelia, Georgia, United States",OK
    ////,AJZ,KAJZ,KAJZ,OK,1.0,////,////,////,BLAKE FIELD,"DELTA, CO - UNITED STATES",Blake Field,"Delta, Colorado, United States",OK
    ////,AK03,PAWT,PAWT,OK,0.8,////,////,////,WAINWRIGHT AS,"WAINWRIGHT, AK - UNITED STATES",Wainwright Air Station,"Wainwright, Alaska, United States",OK
    ////,AK04,////,PAZK,UNK,nan,////,////,////,////,////,Skelton Airport,"Eureka, Alaska, United States",UNK
    ////,AK15,PALP,PALP,OK,1.0,////,////,////,ALPINE AIRSTRIP,"DEADHORSE, AK - UNITED STATES",Alpine Airstrip,"Deadhorse, Alaska, United States",OK
    ////,AK26,////,////,OK,1.0,////,////,////,SOLOMON STATE FIELD,"SOLOMON, AK - UNITED STATES",Solomon State Field,"Solomon, Alaska, United States",OK
    ////,AKH,KAKH,KAKH,OK,1.0,////,////,////,GASTONIA MUNI,"GASTONIA, NC - UNITED STATES",Gastonia Municipal Airport,"Gastonia, North Carolina, United States",OK


List for checking certainty of Great Circle Mapper (ERZ - EZV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    ERZ,////,////,LTCE,UNK,1.0,Erzurum,"Erzurum, Turkey",OWNO,////,////,????,"Erzurum, Erzurum, Turkey",OK
    ESA,////,////,////,UNK,1.0,Esa'Ala,"Esa'Ala, Papua New Guinea",OWNO,////,////,????,"Esa'Ala, Milne Bay, Papua-New Guinea",OK
    ESB,////,////,LTAC,UNK,0.857,Esenboga,"Ankara, Turkey",OWNO,////,////,Esenboga International Airport,"Ankara, Turkey",OK
    ESC,ESC,KESC,KESC,OK,1.0,Delta County Airport,"Escanaba (MI), USA",OWNO,DELTA COUNTY,"ESCANABA, MI - UNITED STATES",Delta County Airport,"Escanaba, Michigan, United States",OK
    ESD,ORS,KORS,KORS,OK,1.0,Orcas Island,"Eastsound (WA), USA",OWNO,ORCAS ISLAND,"EASTSOUND, WA - UNITED STATES",Orcas Island Airport,"Eastsound, Washington, United States",OK
    ESE,////,////,MMES,UNK,0.327,Ensenada,"Ensenada, Mexico",OWNO,////,////,General Alberto L. Salinas C. International Airport,"Ensenada, Baja California, México",OK
    ESF,ESF,KESF,KESF,OK,0.719,Esler Field,"Alexandria (LA), USA",OWNO,ESLER RGNL,"ALEXANDRIA, LA - UNITED STATES",Esler Regional,"Alexandria, Louisiana, United States",OK
    ESG,////,////,SGME,UNK,0.642,Mariscal Estigarribia,"Mariscal Estigarribia, Paraguay",OWNO,////,////,Dr. Luis Maria Argaña International Airport,"Mariscal Estigarribia, Boquerón, Paraguay",OK
    ESH,////,////,////,UNK,1.0,Shoreham,"Shoreham By Sea, United Kingdom",OWNO,////,////,Shoreham,"Shoreham By Sea, England, United Kingdom",OK
    ESI,////,////,////,UNK,1.0,Espinosa,"Espinosa, Brazil",OWNO,////,////,????,"Espinosa, Minas Gerais, Brazil",OK
    ESK,////,////,LTBI,UNK,1.0,Eskisehir,"Eskisehir, Turkey",OWNO,////,////,????,"Eskisehir, Eskisehir, Turkey",OK
    ESL,////,////,URWI,UNK,0.714,Elista,"Elista, Russia",OWNO,////,////,Elista International Airport,"Elista, Kalmykiya, Russian Federation (Russia)",OK
    ESM,////,////,SETN,UNK,0.435,Esmeraldas,"Esmeraldas, Ecuador",OWNO,////,////,Carlos Concha Torres International Airport,"Tachina, Esmeraldas, Ecuador",OK
    ESN,ESN,KESN,KESN,OK,1.0,Easton,"Easton (MD), USA",OWNO,EASTON/NEWNAM FIELD,"EASTON, MD - UNITED STATES",Easton/Newnam Field,"Easton, Maryland, United States",OK
    ESO,E14,////,////,OK,0.49,Espanola,"Espanola (NM), USA",OWNO,OHKAY OWINGEH,"ESPANOLA, NM - UNITED STATES",Ohkay Owingeh Airport,"Española, New Mexico, United States",OK
    ESP,N53,////,////,OK,0.771,Birchwood-Pocono,"East Stroudsburg (PA), USA",OWNO,STROUDSBURG-POCONO,"EAST STROUDSBURG, PA - UNITED STATES",Stroudsburg-Pocono Airport,"East Stroudsburg, Pennsylvania, United States",OK
    ESR,////,////,SCES,UNK,0.526,El Salvador,"El Salvador, Chile",OWNO,////,////,Ricardo García Posada Airport,"El Salvador, Atacama, Chile",OK
    ESS,////,////,EDLE,UNK,0.667,Essen,"Essen, Germany",OWNO,////,////,????,"Essen-Mulheim, North Rhine-Westphalia, Germany",TO DO CHECK
    EST,EST,KEST,KEST,OK,1.0,Municipal,"Estherville IA, USA",OWNO,ESTHERVILLE MUNI,"ESTHERVILLE, IA - UNITED STATES",Estherville Municipal Airport,"Estherville, Iowa, United States",OK
    ESU,////,////,GMMI,UNK,0.692,Essaouira,"Essaouira, Morocco",OWNO,////,////,Mogador,"Essaouira, Morocco",OK
    ESW,ESW,KESW,KESW,OK,1.0,State,"Easton (WA), USA",OWNO,EASTON STATE,"EASTON, WA - UNITED STATES",Easton State Airport,"Easton, Washington, United States",OK
    ETB,ETB,KETB,KETB,OK,0.79,West Bend,"West Bend (WI), USA",OWNO,WEST BEND MUNI,"WEST BEND, WI - UNITED STATES",West Bend Municipal Airport,"West Bend, Wisconsin, United States",OK
    ETD,////,////,YEDA,UNK,1.0,Etadunna,"Etadunna, Australia",OWNO,////,////,????,"Etadunna, South Australia, Australia",OK
    ETE,////,////,HAMM,UNK,1.0,Genda Wuha,"Genda Wuha, Ethiopia",OWNO,////,////,Genda Wuha Airport,"Metema, Amara, Ethiopia",OK
    ETH,////,////,LLET,UNK,0.889,Elat,"Elat, Israel",OWNO,////,////,Eilat Airport,"Eilat, Israel",OK
    ETN,ETN,KETN,KETN,OK,1.0,Municipal,"Eastland (TX), USA",OWNO,EASTLAND MUNI,"EASTLAND, TX - UNITED STATES",Eastland Municipal Airport,"Eastland, Texas, United States",OK
    ETR,////,////,SERO,UNK,nan,////,////,////,////,////,Coronel Artilleria Victor Larrea Airport,"Santa Rosa, El Oro, Ecuador",UNK
    ETS,EDN,KEDN,KEDN,OK,1.0,Municipal,"Enterprise (AL), USA",OWNO,ENTERPRISE MUNI,"ENTERPRISE, AL - UNITED STATES",Enterprise Municipal Airport,"Enterprise, Alabama, United States",OK
    ETZ,////,////,LFJL,UNK,1.0,Metz-Nancy-Lorraine,"Metz/Nancy, France",OWNO,////,////,????,"Metz/Nancy/Lorraine, Lorraine, France",OK
    EUA,////,////,NFTE,UNK,1.0,Kaufana,"Eua, Tonga",OWNO,////,////,Kaufana,"Eua, Tonga",OK
    EUC,////,////,YECL,UNK,1.0,Eucla,"Eucla, Australia",OWNO,////,////,????,"Eucla, Western Australia, Australia",OK
    EUE,05U,////,////,OK,1.0,Eureka,"Eureka (NV), USA",OWNO,EUREKA,"EUREKA, NV - UNITED STATES",????,"Eureka, Nevada, United States",OK
    EUF,EUF,KEUF,KEUF,OK,1.0,Weedon Field,"Eufaula (AL), USA",OWNO,WEEDON FIELD,"EUFAULA, AL - UNITED STATES",Weedon Field,"Eufaula, Alabama, United States",OK
    EUG,EUG,KEUG,KEUG,OK,0.373,Eugene,"Eugene (OR), USA",OWNO,MAHLON SWEET FIELD,"EUGENE, OR - UNITED STATES",Mahlon Sweet Field,"Eugene, Oregon, United States",OK
    EUM,////,////,EDHN,UNK,0.75,Neumuenster,"Neumuenster, Germany",OWNO,////,////,Wasbek,"Neumünster, Schleswig-Holstein, Germany",OK
    EUN,////,////,GMML,UNK,0.919,Hassan I,"Laayoune, Morocco",OWNO,////,////,Hassan International Airport,"El Aaiún, Western Sahara",OK
    EUQ,////,////,RPVS,UNK,nan,////,////,////,////,////,Evelio Javier,"San Jose de Buenavista, Antique, Philippines",UNK
    EUX,////,TNCE,TNCE,OK,1.0,F D Roosevelt,"St Eustatius, Netherlands Antilles",OWNO,F D ROOSEVELT,"ORANJESTAD, - NETHERLANDS ANTILLES",F.D. Roosevelt,"Oranjestad, Sint Eustatius, Bonaire/Sint Eustatius/Saba",OK
    EVA,4TE8,////,////,OK,0.25,Landing Strip,"Evadale (TX), USA",OWNO,BEN BRUCE MEMORIAL AIRPARK,"EVADALE, TX - UNITED STATES",Ben Bruce Memorial Airpark,"Evadale, Texas, United States",OK
    EVD,////,////,YEVA,UNK,1.0,Eva Downs,"Eva Downs, Australia",OWNO,////,////,????,"Eva Downs, Northern Territory, Australia",OK
    EVE,////,////,ENEV,UNK,0.8,Evenes,"Harstad-Narvik, Norway",OWNO,////,////,Harstad/Narvik,"Evenes, Norway",OK
    EVG,////,////,ESND,UNK,1.0,Sveg,"Sveg, Sweden",OWNO,////,////,????,"Sveg, Jämtlands län, Sweden",OK
    EVH,////,////,YEVD,UNK,1.0,Evans Head,"Evans Head, Australia",OWNO,////,////,????,"Evans Head, New South Wales, Australia",OK
    EVM,EVM,KEVM,KEVM,OK,0.599,Eveleth,"Eveleth (MN), USA",OWNO,EVELETH-VIRGINIA MUNI,"EVELETH, MN - UNITED STATES",Eveleth-Virginia Municipal Airport,"Eveleth, Minnesota, United States",OK
    EVN,////,////,UDYZ,UNK,0.483,Yerevan,"Yerevan, Armenia",OWNO,////,////,Zvartnots International Airport,"Yerevan, Armenia",OK
    EVV,EVV,KEVV,KEVV,OK,0.783,Dress Regional,"Evansville (IN), USA",OWNO,EVANSVILLE RGNL,"EVANSVILLE, IN - UNITED STATES",Evansville Regional,"Evansville, Indiana, United States",OK
    EVW,EVW,KEVW,KEVW,OK,0.315,Evanston,"Evanston (WY), USA",OWNO,EVANSTON-UINTA COUNTY BURNS FIELD,"EVANSTON, WY - UNITED STATES",Evanston-Uinta County Burns Field,"Evanston, Wyoming, United States",OK
    EVX,////,////,LFOE,UNK,0.5,Evreux,"Evreux, France",OWNO,////,////,Évreux-Fauville AB,"Évreux, Haute-Normandie (Upper Normandy), France",OK
    EWB,EWB,KEWB,KEWB,OK,0.826,New Bedford,"New Bedford (MA), USA",OWNO,NEW BEDFORD RGNL,"NEW BEDFORD, MA - UNITED STATES",New Bedford Regional,"New Bedford, Massachusetts, United States",OK
    EWE,////,////,////,UNK,1.0,Ewer,"Ewer, Indonesia",OWNO,////,////,????,"Ewer, Papua, Indonesia",OK
    EWI,////,////,WABT,UNK,1.0,Enarotali,"Enarotali, Indonesia",OWNO,////,////,????,"Enarotali, Papua, Indonesia",OK
    EWK,EWK,KEWK,KEWK,OK,1.0,City-County,"Newton (KS), USA",OWNO,NEWTON-CITY-COUNTY,"NEWTON, KS - UNITED STATES",Newton-City-County Airport,"Newton, Kansas, United States",OK
    EWN,EWN,KEWN,KEWN,OK,0.51,Simmons Nott,"New Bern (NC), USA",OWNO,COASTAL CAROLINA REGIONAL,"NEW BERN, NC - UNITED STATES",Coastal Carolina Regional,"New Bern, North Carolina, United States",OK
    EWO,////,////,FCOE,UNK,1.0,Ewo,"Ewo, Congo (ROC)",OWNO,////,////,????,"Ewo, Congo (Republic of)",OK
    EWR,EWR,KEWR,KEWR,OK,0.716,Newark International,"Newark (NJ), USA",OWNO,NEWARK LIBERTY INTL,"NEWARK, NJ - UNITED STATES",Newark Liberty International Airport,"Newark, New Jersey, United States",OK
    EXI,EXI,////,////,OK,0.852,SPB,"Excursion Inlet (AK), USA",OWNO,EXCURSION INLET,"EXCURSION INLET, AK - UNITED STATES",Excursion Inlet SPB,"Excursion Inlet, Alaska, United States",OK
    EXM,////,////,YEXM,UNK,1.0,Exmouth Gulf,"Exmouth Gulf, Australia",OWNO,////,////,????,"Exmouth Gulf, Western Australia, Australia",OK
    EXT,////,////,EGTE,UNK,0.75,Exeter,"Exeter, United Kingdom",OWNO,////,////,Exeter International Airport,"Exeter, Devonshire, England, United Kingdom",OK
    EYK,////,////,USHQ,UNK,nan,////,////,////,////,////,Beloyarskiy Airport,"Beloyarskiy, Khanty-Mansiyskiy, Russian Federation (Russia)",UNK
    EYL,////,////,GAYE,UNK,1.0,Yelimane,"Yelimane, Mali",OWNO,////,////,????,"Yélimané, Kayes, Mali",OK
    EYP,////,////,SKYP,UNK,0.615,El Yopal,"El Yopal, Colombia",OWNO,////,////,El Alcaraván Airport,"El Yopal, Casanare, Colombia",OK
    EYS,////,////,HKES,UNK,1.0,Eliye Springs,"Eliye Springs, Kenya",OWNO,////,////,Eliye Springs Airport,"Eliye Springs, Kenya",OK
    EYW,EYW,KEYW,KEYW,OK,1.0,International,"Key West (FL), USA",OWNO,KEY WEST INTL,"KEY WEST, FL - UNITED STATES",Key West International Airport,"Key West, Florida, United States",OK
    EZE,////,////,SAEZ,UNK,1.0,Ministro Pistarini,"Buenos Aires, Argentina",OWNO,////,////,Aeropuerto Internacional Ezeiza/Ministro Pistarini,"Buenos Aires, Distrito Federal, Argentina",OK
    EZS,////,////,LTCA,UNK,1.0,Elazig,"Elazig, Turkey",OWNO,////,////,????,"Elazig, Elazig, Turkey",OK
    EZV,////,////,USHB,UNK,1.0,Berezovo,Berezovo,IATA,////,////,Berezovo Airport,"Berezovo, Khanty-Mansiyskiy, Russian Federation (Russia)",MAYBE

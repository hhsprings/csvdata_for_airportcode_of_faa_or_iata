
List for checking certainty of Great Circle Mapper (RHP - RRR)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    RHP,////,////,VNRC,UNK,1.0,Ramechhap,"Ramechhap, Nepal",OWNO,////,////,????,"Ramechhap, Nepal",OK
    RHR,////,////,////,UNK,1.0,Al Hamra SPB,Ras al Khaimah,IATA,////,////,Al Hamra SPB,"Ras al Khaimah, Ras al-Khaimah, United Arab Emirates",MAYBE
    RHT,////,////,////,UNK,0.756,Alxa Right Banner,Alxa Right Banner,IATA,////,////,Alxa Right Banner Badanjilin Airport,"Badanjilin, Inner Mongolia, China",MAYBE
    RHV,RHV,KRHV,KRHV,OK,0.577,Reid-Hillview,"San Jose (CA), USA",OWNO,REID-HILLVIEW OF SANTA CLARA COUNTY,"SAN JOSE, CA - UNITED STATES",Reid-Hillview Airport of Santa Clara County,"San José, California, United States",OK
    RIA,////,////,SBSM,UNK,0.667,Base Aerea,"Santa Maria, Brazil",OWNO,////,////,????,"Santa Maria, Rio Grande do Sul, Brazil",OK
    RIB,////,////,SLRI,UNK,1.0,Gen Buech,"Riberalta, Bolivia",OWNO,////,////,Gen. Buech Airport,"Riberalta, Vaca Díez, El Beni, Bolivia",OK
    RIC,RIC,KRIC,KRIC,OK,1.0,International (Byrd Field),"Richmond (VA), USA",OWNO,RICHMOND INTL,"RICHMOND, VA - UNITED STATES",Richmond International Airport,"Richmond, Virginia, United States",OK
    RID,RID,KRID,KRID,OK,0.752,Richmond,"Richmond (IN), USA",OWNO,RICHMOND MUNI,"RICHMOND, IN - UNITED STATES",Richmond Municipal Airport,"Richmond, Indiana, United States",OK
    RIE,RPD,KRPD,KRPD,OK,0.487,Rice Lake,"Rice Lake (WI), USA",OWNO,RICE LAKE RGNL - CARL'S FIELD,"RICE LAKE, WI - UNITED STATES",Rice Lake Regional - Carl's Field,"Rice Lake, Wisconsin, United States",OK
    RIF,RIF,KRIF,KRIF,OK,0.599,Reynolds,"Richfield (UT), USA",OWNO,RICHFIELD MUNI,"RICHFIELD, UT - UNITED STATES",Richfield Municipal Airport,"Richfield, Utah, United States",OK
    RIG,////,////,SBRG,UNK,1.0,Rio Grande,"Rio Grande, Brazil",OWNO,////,////,????,"Rio Grande, Rio Grande do Sul, Brazil",OK
    RIH,////,////,MPSM,UNK,nan,////,////,////,////,////,Scarlett Martinez International Airport,"Río Hato, Coclé, Panamá",UNK
    RIJ,////,////,SPJA,UNK,0.37,Rioja,"Rioja, Peru",OWNO,////,////,Juan Simons Vela Airport,"Rioja, San Martín, Perú",OK
    RIK,////,////,MRCR,UNK,1.0,Carrillo,"Carrillo, Costa Rica",OWNO,////,////,????,"Playa Sámara/Carrillo, Guanacaste, Costa Rica",OK
    RIL,RIL,KRIL,KRIL,OK,0.852,Garfield County,"Rifle (CO), USA",OWNO,GARFIELD COUNTY RGNL,"RIFLE, CO - UNITED STATES",Garfield County Regional,"Rifle, Colorado, United States",OK
    RIM,////,////,SPLN,UNK,0.889,Rodriguez De Men,"Rodriguez De Men, Peru",OWNO,////,////,????,"Rodríguez de Mendoza, Amazonas, Perú",MAYBE
    RIN,////,////,AGRC,UNK,1.0,Ringi Cove,"Ringi Cove, Solomon Islands",OWNO,////,////,????,"Ringi Cove, Kolombangara Island, Solomon Islands",OK
    RIO,////,////,////,UNK,1.0,Metropolitan Area,"Rio De Janeiro, Brazil",OWNO,////,////,Metropolitan Area,"Rio de Janeiro, Rio de Janeiro, Brazil",OK
    RIR,RIR,KRIR,KRIR,OK,0.688,Riverside Fla-Bob,"Riverside (CA), USA",OWNO,FLABOB,"RIVERSIDE/RUBIDOUX/, CA - UNITED STATES",Flabob Airport,"Riverside, California, United States",OK
    RIS,////,////,RJER,UNK,1.0,Rishiri,"Rishiri, Japan",OWNO,////,////,Rishiri Airport,"Rishirifuji, Rishiri Island, Hokkaido, Japan",OK
    RIV,RIV,KRIV,KRIV,OK,0.935,March AFB,"Riverside (CA), USA",OWNO,MARCH ARB,"RIVERSIDE, CA - UNITED STATES",March ARB,"Riverside, California, United States",OK
    RIW,RIW,KRIW,KRIW,OK,0.766,Riverton,"Riverton (WY), USA",OWNO,RIVERTON RGNL,"RIVERTON, WY - UNITED STATES",Riverton Regional,"Riverton, Wyoming, United States",OK
    RIX,////,////,EVRA,UNK,0.615,Riga,"Riga, Latvia",OWNO,////,////,Riga International Airport,"Riga, Latvia",OK
    RIY,////,////,OYRN,UNK,0.571,Riyan Mukalla,"Riyan Mukalla, Yemen",OWNO,////,////,????,"Riyan, Yemen",MAYBE
    RIZ,////,////,////,UNK,0.696,Shanzihe,Rizhao,IATA,////,////,Rizhao Shanzihe Airport,"Rizhao, Shandong, China",MAYBE
    RJA,////,////,VORY,UNK,1.0,Rajahmundry,"Rajahmundry, India",OWNO,////,////,????,"Rajahmundry, Andhra Pradesh, India",OK
    RJB,////,////,VNRB,UNK,1.0,Rajbiraj,"Rajbiraj, Nepal",OWNO,////,////,????,"Rajbiraj, Nepal",OK
    RJH,////,////,VGRJ,UNK,0.552,Rajshahi,"Rajshahi, Bangladesh",OWNO,////,////,Shah Mokhdum,"Rajshahi, Bangladesh",OK
    RJI,////,////,////,UNK,1.0,Rajouri,"Rajouri, India",OWNO,////,////,????,"Rajouri, Jammu and Kashmir, India",OK
    RJK,////,////,LDRI,UNK,1.0,Rijeka,"Rijeka, Croatia",OWNO,////,////,????,"Rijeka, Croatia",OK
    RJL,////,////,LELO,UNK,1.0,Logroño-Agoncillo Airport,"Logroño, Spain",OWNO,////,////,Logroño-Agoncillo,"Logroño, La Rioja, Spain",OK
    RJN,////,////,OIKR,UNK,1.0,Rafsanjan,"Rafsanjan, Iran",OWNO,////,////,????,"Rafsanjan, Kerman, Iran",OK
    RKA,////,////,NTKK,UNK,nan,////,////,////,////,////,Aratika Nord Airport,"Aratika, French Polynesia",UNK
    RKC,1O5,////,////,OK,0.352,Yreka,"Yreka (CA), USA",OWNO,MONTAGUE-YREKA ROHRER FIELD,"MONTAGUE, CA - UNITED STATES",Montague-Yreka Rohrer Field,"Montague, California, United States",OK
    RKD,RKD,KRKD,KRKD,OK,1.0,Knox County Regional,"Rockland (ME), USA",OWNO,KNOX COUNTY RGNL,"ROCKLAND, ME - UNITED STATES",Knox County Regional,"Rockland, Maine, United States",OK
    RKE,////,////,EKRK,UNK,1.0,Roskilde Airport,"Copenhagen, Denmark",OWNO,////,////,Roskilde,"København, Denmark",OK
    RKH,UZA,KUZA,KUZA,OK,1.0,Rock Hill,"Rock Hill (SC), USA",OWNO,ROCK HILL/YORK CO/BRYANT FIELD,"ROCK HILL, SC - UNITED STATES",Rock Hill/York Co./Bryant Field,"Rock Hill, South Carolina, United States",OK
    RKI,////,////,////,UNK,1.0,Rokot,"Rokot, Indonesia",OWNO,////,////,????,"Rokot, Sumatera Barat, Indonesia",OK
    RKO,////,////,WIBR,UNK,1.0,Sipora,"Sipora, Indonesia",OWNO,////,////,????,"Sipora, Sumatera Barat, Indonesia",OK
    RKP,RKP,KRKP,KRKP,OK,0.857,Aransas County,"Rockport (TX), USA",OWNO,ARANSAS CO,"ROCKPORT, TX - UNITED STATES",Aransas County Airport,"Rockport, Texas, United States",OK
    RKR,RKR,KRKR,KRKR,OK,1.0,Robert S Kerr,"Poteau (OK), USA",OWNO,ROBERT S KERR,"POTEAU, OK - UNITED STATES",Robert S Kerr Airport,"Poteau, Oklahoma, United States",OK
    RKS,RKS,KRKS,KRKS,OK,1.0,Sweetwater County,"Rock Springs (WY), USA",OWNO,ROCK SPRINGS-SWEETWATER COUNTY,"ROCK SPRINGS, WY - UNITED STATES",Rock Springs-Sweetwater County Airport,"Rock Springs, Wyoming, United States",OK
    RKT,////,////,OMRK,UNK,0.848,Ras Al Khaimah,"Ras Al Khaimah, United Arab Emirates",OWNO,////,////,Ras al Khaimah International Airport,"Ras al Khaimah, Ras al-Khaimah, United Arab Emirates",OK
    RKU,////,////,////,UNK,1.0,Kairuku,"Yule Island, Papua New Guinea",OWNO,////,////,Kairuku,"Yule Island, Central, Papua-New Guinea",OK
    RKV,////,////,BIRK,UNK,0.667,Reykjavik Domestic,"Reykjavik, Iceland",OWNO,////,////,????,"Reykjavík, Iceland",OK
    RKW,RKW,KRKW,KRKW,OK,1.0,Municipal,"Rockwood (TN), USA",OWNO,ROCKWOOD MUNI,"ROCKWOOD, TN - UNITED STATES",Rockwood Municipal Airport,"Rockwood, Tennessee, United States",OK
    RKY,////,////,////,UNK,1.0,Rokeby,"Rokeby, Australia",OWNO,////,////,????,"Rokeby, Queensland, Australia",OK
    RKZ,////,////,ZURK,UNK,nan,////,////,////,////,////,Shigatse Peace Airport,"Xigazê, Tibet, China",UNK
    RLD,RLD,KRLD,KRLD,OK,1.0,Richland,"Richland (WA), USA",OWNO,RICHLAND,"RICHLAND, WA - UNITED STATES",????,"Richland, Washington, United States",OK
    RLG,////,////,ETNL,UNK,0.909,Rostock-Laage Airport,"Rostock, Germany",OWNO,////,////,Rostock-Laage AB,"Laage, Mecklenburg-Vorpommern, Germany",MAYBE
    RLK,////,////,ZBYZ,UNK,0.667,Tianjitai,Bayannur,IATA,////,////,Bayannur Tianjitai Airport,"Bayannur, Inner Mongolia, China",MAYBE
    RLP,////,////,////,UNK,1.0,Rosella Plains,"Rosella Plains, Australia",OWNO,////,////,????,"Rosella Plains, Queensland, Australia",OK
    RLT,////,////,DRZL,UNK,1.0,Arlit,"Arlit, Niger",OWNO,////,////,????,"Arlit, Niger",OK
    RMA,////,////,YROM,UNK,1.0,Roma,"Roma, Australia",OWNO,////,////,????,"Roma, Queensland, Australia",OK
    RMB,////,////,OOBR,UNK,1.0,Buraimi,"Buraimi, Oman",OWNO,////,////,????,"Buraimi, Oman",OK
    RMD,////,////,VORG,UNK,1.0,Ramagundam,"Ramagundam, India",OWNO,////,////,????,"Ramagundam, Telangana, India",OK
    RME,RME,KRME,KRME,OK,0.689,Griffiss AFB,"Rome (NY), USA",OWNO,GRIFFISS INTL,"ROME, NY - UNITED STATES",Griffiss International Airport,"Rome, New York, United States",OK
    RMF,////,////,HEMA,UNK,nan,////,////,////,////,////,Marsa Alam International Airport,"Marsa Alam, Al Bahr al Ahmar (Red Sea), Egypt",UNK
    RMG,RMG,KRMG,KRMG,OK,0.627,Richard B Russell,"Rome (GA), USA",OWNO,RICHARD B RUSSELL REGIONAL - J H TOWERS FIELD,"ROME, GA - UNITED STATES",Richard B Russell Regional - J H Towers Field,"Rome, Georgia, United States",OK
    RMI,////,////,LIPR,UNK,0.6,Miramare,"Rimini, Italy",OWNO,////,////,Federico Fellini International Airport,"Rimini, Emilia-Romagna, Italy",OK
    RMK,////,////,YREN,UNK,1.0,Renmark,"Renmark, Australia",OWNO,////,////,????,"Renmark, South Australia, Australia",OK
    RML,////,////,VCCC,UNK,1.0,Ratmalana,"Colombo, Sri Lanka",OWNO,////,////,Ratmalana Airport,"Colombo, Western Province, Sri Lanka (Ceylon)",OK
    RMN,////,////,////,UNK,1.0,Rumginae,"Rumginae, Papua New Guinea",OWNO,////,////,????,"Rumginae, Western, Papua-New Guinea",OK
    RMP,RMP,////,////,OK,1.0,Rampart,"Rampart (AK), USA",OWNO,RAMPART,"RAMPART, AK - UNITED STATES",????,"Rampart, Alaska, United States",OK
    RMQ,////,////,RCMQ,UNK,nan,////,////,////,////,////,Taichung Ching Chuan Kang,"Taichung, Taiwan",UNK
    RMS,////,////,ETAR,UNK,0.783,Ramstein Air Base,"Ramstein, Germany",OWNO,////,////,Ramstein AFB,"Ramstein, Rhineland-Palatinate, Germany",OK
    RMT,////,////,////,UNK,nan,////,////,////,////,////,Rimatara Airport,"Rimatara, French Polynesia",UNK
    RNA,////,////,AGAR,UNK,0.8,Ulawa Airport,"Arona, Solomon Islands",OWNO,////,////,Arona,"Ulawa Island, Solomon Islands",OK
    RNB,////,////,ESDF,UNK,0.688,Kallinge,"Ronneby, Sweden",OWNO,////,////,"Ronneby AB, F17","Ronneby, Blekinge län, Sweden",OK
    RNC,RNC,KRNC,KRNC,OK,0.766,Warren County,"Mcminnville (TN), USA",OWNO,WARREN COUNTY MEMORIAL,"MC MINNVILLE, TN - UNITED STATES",Warren County Memorial Airport,"Mc Minnville, Tennessee, United States",OK
    RND,RND,KRND,KRND,OK,1.0,Randolph AFB,"San Antonio (TX), USA",OWNO,RANDOLPH AFB,"UNIVERSAL CITY, TX - UNITED STATES",Randolph AFB,"Universal City, Texas, United States",OK
    RNE,////,////,LFLO,UNK,1.0,Renaison,"Roanne, France",OWNO,////,////,Renaison,"Roanne, Rhône-Alpes, France",OK
    RNG,4V0,////,////,OK,1.0,Rangely,"Rangely (CO), USA",OWNO,RANGELY,"RANGELY, CO - UNITED STATES",????,"Rangely, Colorado, United States",OK
    RNH,RNH,KRNH,KRNH,OK,0.757,Municipal,"New Richmond (WI), USA",OWNO,NEW RICHMOND RGNL,"NEW RICHMOND, WI - UNITED STATES",New Richmond Regional,"New Richmond, Wisconsin, United States",OK
    RNI,////,////,MNCI,UNK,0.818,Corn Island,"Corn Island, Nicaragua",OWNO,////,////,Corn Island International Airport,"Corn Island, Atlántico Sur, Nicaragua",OK
    RNJ,////,////,RORY,UNK,0.769,Yoronjima,"Yoronjima, Japan",OWNO,////,////,Yoron Airport,"Yoron, Yoron Island, Satsunan Islands, Kagoshima, Japan",TO DO CHECK
    RNL,////,////,AGGR,UNK,1.0,Rennell,"Rennell, Solomon Islands",OWNO,////,////,????,"Rennell/Tingoa, Rennell Island, Solomon Islands",OK
    RNN,////,////,EKRN,UNK,0.824,Bornholm Airport,"Bornholm island, Denmark",OWNO,////,////,Rønne,"Bornholm, Denmark",OK
    RNO,RNO,KRNO,KRNO,OK,1.0,Reno/Tahoe International Airport,"Reno (NV), USA",OWNO,RENO/TAHOE INTL,"RENO, NV - UNITED STATES",Reno/Tahoe International Airport,"Reno, Nevada, United States",OK
    RNP,////,////,////,UNK,1.0,Rongelap Island,"Rongelap Island, Marshall Islands",OWNO,////,////,????,"Rongelap Island, Marshall Islands",OK
    RNR,////,////,////,UNK,1.0,Robinson River,"Robinson River, Papua New Guinea",OWNO,////,////,????,"Robinson River, Central, Papua-New Guinea",OK
    RNS,////,////,LFRN,UNK,1.0,St Jacques,"Rennes, France",OWNO,////,////,St-Jacques,"Rennes, Bretagne, France",OK
    RNT,RNT,KRNT,KRNT,OK,0.778,Renton,"Renton (WA), USA",OWNO,RENTON MUNI,"RENTON, WA - UNITED STATES",Renton Municipal Airport,"Renton, Washington, United States",OK
    RNU,////,////,WBKR,UNK,1.0,Ranau,"Ranau, Malaysia",OWNO,////,////,????,"Ranau, Sabah, Malaysia",OK
    RNZ,RZL,KRZL,KRZL,OK,0.518,Rensselaer,"Rensselaer (IN), USA",OWNO,JASPER COUNTY,"RENSSELAER, IN - UNITED STATES",Jasper County Airport,"Rensselaer, Indiana, United States",OK
    ROA,ROA,KROA,KROA,OK,0.514,Municipal,"Roanoke (VA), USA",OWNO,ROANOKE-BLACKSBURG RGNL/WOODRUM FIELD,"ROANOKE, VA - UNITED STATES",Roanoke-Blacksburg Regional/Woodrum Field,"Roanoke, Virginia, United States",OK
    ROB,////,////,GLRB,UNK,0.884,Roberts International Airport,"Monrovia, Liberia",OWNO,////,////,Roberts Field,"Monrovia, Liberia",OK
    ROC,ROC,KROC,KROC,OK,0.484,Monroe County,"Rochester (NY), USA",OWNO,GREATER ROCHESTER INTL,"ROCHESTER, NY - UNITED STATES",Greater Rochester International Airport,"Rochester, New York, United States",OK
    ROD,////,////,FARS,UNK,0.75,Robertson,"Robertson, South Africa",OWNO,////,////,Robertson Airfield,"Robertson, Western Cape, South Africa",OK
    ROG,ROG,KROG,KROG,OK,0.334,Rogers,"Rogers (AR), USA",OWNO,ROGERS EXECUTIVE - CARTER FIELD,"ROGERS, AR - UNITED STATES",Rogers Executive - Carter Field,"Rogers, Arkansas, United States",OK
    ROH,////,////,YROB,UNK,1.0,Robinhood,"Robinhood, Australia",OWNO,////,////,????,"Robinhood, Queensland, Australia",OK
    ROI,////,////,VTUV,UNK,1.0,Roi Et Airport,"Roi Et, Thailand",OWNO,////,////,????,"Roi Et, Roi Et, Thailand",OK
    ROK,////,////,YBRK,UNK,1.0,Rockhampton,"Rockhampton, Australia",OWNO,////,////,????,"Rockhampton, Queensland, Australia",OK
    ROM,////,////,////,UNK,1.0,Metropolitan Area,"Rome, Italy",OWNO,////,////,Metropolitan Area,"Rome, Lazio, Italy",OK
    RON,////,////,SKPA,UNK,0.632,Rondon,"Rondon, Colombia",OWNO,////,////,Juan José Rondón Airport,"Paipa, Boyacá, Colombia",TO DO CHECK
    ROO,////,////,SWRD,UNK,1.0,Rondonopolis,"Rondonopolis, Brazil",OWNO,////,////,????,"Rondonópolis, Mato Grosso, Brazil",OK
    ROP,GRO,PGRO,PGRO,OK,0.029,Rota,"Rota, Northern Mariana Islands",OWNO,BENJAMIN TAISACAN MANGLONA INTL,"ROTA ISLAND, CQ - UNITED STATES",Benjamin Taisacan Manglona International Airport,"Rota Island, Northern Mariana Islands, United States",MAYBE
    ROR,ROR,PTRO,PTRO,OK,0.591,Airai,"Koror, Palau",OWNO,BABELTHUAP/KOROR,"BABELTHUAP ISLAND, - PALAU",Babelthuap/Koror Airport,"Babelthuap Island, Palau",OK
    ROS,////,////,SAAR,UNK,0.714,Fisherton,"Rosario, Argentina",OWNO,////,////,????,"Rosario, Santa Fe, Argentina",OK
    ROT,////,////,NZRO,UNK,1.0,Rotorua,"Rotorua, New Zealand",OWNO,////,////,????,"Rotorua, New Zealand",OK
    ROU,////,////,LBRS,UNK,1.0,Rousse,"Rousse, Bulgaria",OWNO,////,////,????,"Ruse, Razgrad, Bulgaria",OK
    ROV,////,////,URRR,UNK,0.706,Rostov,"Rostov, Russia",OWNO,////,////,Rostov-on-Don Airport,"Rostov-on-Don, Rostovskaya, Russian Federation (Russia)",TO DO CHECK
    ROW,ROW,KROW,KROW,OK,0.445,Industrial,"Roswell (NM), USA",OWNO,ROSWELL INTL AIR CENTER,"ROSWELL, NM - UNITED STATES",Roswell International Air Center,"Roswell, New Mexico, United States",OK
    ROX,ROX,KROX,KROX,OK,1.0,Municipal,"Roseau (MN), USA",OWNO,ROSEAU MUNI/RUDY BILLBERG FIELD,"ROSEAU, MN - UNITED STATES",Roseau Municipal/Rudy Billberg Field,"Roseau, Minnesota, United States",OK
    ROY,////,////,SAWM,UNK,1.0,Rio Mayo,"Rio Mayo, Argentina",OWNO,////,////,????,"Río Mayo, Chubut, Argentina",OK
    ROZ,////,////,LERT,UNK,nan,////,////,////,////,////,Naval Station Rota,"Rota, Andalusia, Spain",UNK
    RPA,////,////,////,UNK,1.0,Rolpa,"Rolpa, Nepal",OWNO,////,////,????,"Rolpa, Nepal",OK
    RPB,////,////,YRRB,UNK,1.0,Roper Bar,"Roper Bar, Australia",OWNO,////,////,????,"Roper Bar, Northern Territory, Australia",OK
    RPM,////,////,YNGU,UNK,1.0,Ngukurr,"Ngukurr, Australia",OWNO,////,////,????,"Ngukurr, Northern Territory, Australia",OK
    RPN,////,////,LLIB,UNK,1.0,Rosh Pina,"Rosh Pina, Israel",OWNO,////,////,Rosh Pina,"Rosh Pina, Israel",OK
    RPR,////,////,VARP,UNK,1.0,Raipur,"Raipur, India",OWNO,////,////,????,"Raipur, Chhattisgarh, India",OK
    RPV,////,////,////,UNK,1.0,Roper Valley,"Roper Valley, Australia",OWNO,////,////,????,"Roper Valley, Northern Territory, Australia",OK
    RPX,RPX,KRPX,KRPX,OK,1.0,Roundup,"Roundup (MT), USA",OWNO,ROUNDUP,"ROUNDUP, MT - UNITED STATES",????,"Roundup, Montana, United States",OK
    RRE,////,////,YMRE,UNK,1.0,Marree,"Marree, Australia",OWNO,////,////,????,"Marree, South Australia, Australia",OK
    RRG,////,////,FIMR,UNK,1.0,Rodrigues Island,"Rodrigues Island, Mauritius",OWNO,////,////,????,"Plaine Corail, Rodrigues Island, Mauritius",OK
    RRI,////,////,////,UNK,1.0,Barora,"Barora, Solomon Islands",OWNO,////,////,????,"Barora, Solomon Islands",OK
    RRK,////,////,VERK,UNK,1.0,Rourkela,"Rourkela, India",OWNO,////,////,????,"Rourkela, Odisha, India",OK
    RRL,RRL,KRRL,KRRL,OK,1.0,Municipal,"Merrill (WI), USA",OWNO,MERRILL MUNI,"MERRILL, WI - UNITED STATES",Merrill Municipal Airport,"Merrill, Wisconsin, United States",OK
    RRM,////,////,////,UNK,1.0,Marromeu,"Marromeu, Mozambique",OWNO,////,////,????,"Marromeu, Mozambique",OK
    RRR,////,////,NTKO,UNK,nan,////,////,////,////,////,Raroia Airpor,"Raroia, French Polynesia",UNK

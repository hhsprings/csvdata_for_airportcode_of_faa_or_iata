
List for checking certainty of Great Circle Mapper (BNQ - BTH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    BNQ,////,////,////,UNK,1.0,Baganga,"Baganga, Philippines",OWNO,////,////,????,"Baganga, Philippines",OK
    BNR,////,////,DFOB,UNK,1.0,Banfora,"Banfora, Burkina Faso",OWNO,////,////,Banfora Airport,"Banfora, Burkina Faso",OK
    BNS,////,////,SVBI,UNK,1.0,Barinas,"Barinas, Venezuela",OWNO,////,////,????,"Barinas, Barinas, Venezuela",OK
    BNT,////,////,////,UNK,1.0,Bundi,"Bundi, Papua New Guinea",OWNO,////,////,????,"Bundi, Madang, Papua-New Guinea",OK
    BNU,////,////,SSBL,UNK,1.0,Blumenau,"Blumenau, Brazil",OWNO,////,////,????,"Blumenau, Santa Catarina, Brazil",OK
    BNV,////,////,////,UNK,1.0,Boana,"Boana, Papua New Guinea",OWNO,////,////,????,"Boana, Morobe, Papua-New Guinea",OK
    BNW,BNW,KBNW,KBNW,OK,0.64,Boone,"Boone IA, USA",OWNO,BOONE MUNI,"BOONE, IA - UNITED STATES",Boone Municipal Airport,"Boone, Iowa, United States",OK
    BNX,////,////,LQBK,UNK,0.889,Banja Luka,"Banja Luka, Bosnia and Herzegovina",OWNO,////,////,Banja Luka International Airport,"Banja Luka, Bosnia and Herzegovina",OK
    BNY,////,////,AGGB,UNK,1.0,Bellona,"Bellona, Solomon Islands",OWNO,////,////,????,"Bellona/Anua, Solomon Islands",OK
    BNZ,////,////,////,UNK,1.0,Banz,"Banz, Papua New Guinea",OWNO,////,////,????,"Banz, Western Highlands, Papua-New Guinea",OK
    BOA,////,////,FZAJ,UNK,1.0,Boma,"Boma, Congo (DRC)",OWNO,////,////,Boma Airport,"Boma, Bas-Congo, Democratic Republic of Congo (Zaire)",OK
    BOB,////,////,NTTB,UNK,1.0,Motu-mute,"Bora Bora, French Polynesia",OWNO,////,////,Motu Mute,"Bora Bora, Society Islands, French Polynesia",OK
    BOC,////,////,MPBO,UNK,1.0,Bocas Del Toro,"Bocas Del Toro, Panama",OWNO,////,////,????,"Bocas del Toro, Bocas del Toro, Panamá",OK
    BOD,////,////,LFBD,UNK,0.64,Bordeaux Airport,"Bordeaux, France",OWNO,////,////,Mérignac,"Bordeaux, Aquitaine, France",OK
    BOE,////,////,FCOB,UNK,1.0,Boundji,"Boundji, Congo (ROC)",OWNO,////,////,Boundji Airport,"Boundji, Congo (Republic of)",OK
    BOG,////,////,SKBO,UNK,0.688,Eldorado,"Bogota, Colombia",OWNO,////,////,El Nuevo Dorado International Airport,"Bogotá, Distrito Capital de Bogotá, Colombia",OK
    BOH,////,////,EGHH,UNK,0.815,International,"Bournemouth, United Kingdom",OWNO,////,////,????,"Bournemouth, Hampshire, England, United Kingdom",OK
    BOI,BOI,KBOI,KBOI,OK,0.694,Air Terminal Gowen Field,"Boise (ID), USA",OWNO,BOISE AIR TERMINAL/GOWEN FLD,"BOISE, ID - UNITED STATES",Boise Air Terminal/Gowen Field,"Boise, Idaho, United States",OK
    BOJ,////,////,LBBG,UNK,0.667,Bourgas,"Bourgas, Bulgaria",OWNO,////,////,Burgas International Airport,"Burgas, Burgas, Bulgaria",OK
    BOK,BOK,KBOK,KBOK,OK,0.766,Brookings State,"Brookings (OR), USA",OWNO,BROOKINGS,"BROOKINGS, OR - UNITED STATES",????,"Brookings, Oregon, United States",OK
    BOL,////,////,EGQB,UNK,1.0,Bally Kelly,"Bally Kelly, United Kingdom",OWNO,////,////,????,"Bally Kelly, County Londonderry, Northern Ireland, United Kingdom",OK
    BOM,////,////,VABB,UNK,0.93,Chhatrapati Shivaji,"Mumbai, India",OWNO,////,////,Chhatrapati Shivaji International Airport,"Mumbai, Maharashtra, India",OK
    BON,////,////,TNCB,UNK,0.941,Flamingo International,"Bonaire, Netherlands Antilles",OWNO,////,////,Flamingo Airport,"Kralendijk, Bonaire, Bonaire/Sint Eustatius/Saba",OK
    BOO,////,////,ENBO,UNK,1.0,Bodo,"Bodo, Norway",OWNO,////,////,????,"Bodø, Norway",OK
    BOP,////,////,FEFO,UNK,1.0,Bouar,"Bouar, Central African Republic",OWNO,////,////,????,"Bouar, Nana-Mambéré (Nanä-Mbaere), Central African Republic",OK
    BOQ,////,////,////,UNK,1.0,Boku,"Boku, Papua New Guinea",OWNO,////,////,????,"Boku, Bougainville, Papua-New Guinea",OK
    BOR,////,////,LFSQ,UNK,1.0,Fontaine,"Belfort, France",OWNO,////,////,Fontaine,"Belfort, Franche-Comté, France",OK
    BOS,BOS,KBOS,KBOS,OK,0.52,Logan International,"Boston (MA), USA",OWNO,GENERAL EDWARD LAWRENCE LOGAN INTL,"BOSTON, MA - UNITED STATES",General Edward Lawrence Logan International Airport,"Boston, Massachusetts, United States",OK
    BOT,////,////,AYET,UNK,1.0,Boset,"Boset, Papua New Guinea",OWNO,////,////,????,"Boset, Western, Papua-New Guinea",OK
    BOU,////,////,LFLD,UNK,1.0,Bourges,"Bourges, France",OWNO,////,////,Bourges Airport,"Bourges, Centre, France",OK
    BOV,////,////,////,UNK,1.0,Boang,"Boang, Papua New Guinea",OWNO,////,////,????,"Boang, New Ireland, Papua-New Guinea",OK
    BOW,BOW,KBOW,KBOW,OK,1.0,Bartow Municipal Airport,"Bartow (FL), USA",OWNO,BARTOW MUNI,"BARTOW, FL - UNITED STATES",Bartow Municipal Airport,"Bartow, Florida, United States",OK
    BOX,////,////,YBRL,UNK,1.0,Borroloola,"Borroloola, Australia",OWNO,////,////,????,"Borroloola, Northern Territory, Australia",OK
    BOY,////,////,DFOO,UNK,0.824,Borgo,"Bobo Dioulasso, Burkina Faso",OWNO,////,////,????,"Bobo/Dioulasso, Burkina Faso",MAYBE
    BOZ,////,////,FEGZ,UNK,1.0,Bozoum,"Bozoum, Central African Republic",OWNO,////,////,????,"Bozoum, Ouham-Pendé (Wâmo-Pendë), Central African Republic",OK
    BPB,////,////,////,UNK,1.0,Boridi,"Boridi, Papua New Guinea",OWNO,////,////,????,"Boridi, Central, Papua-New Guinea",OK
    BPC,////,////,FKKV,UNK,1.0,Bamenda,"Bamenda, Cameroon",OWNO,////,////,Bamenda Airport,"Bamenda, North-West (Nord-Ouest), Cameroon",OK
    BPD,////,////,////,UNK,1.0,Bapi,"Bapi, Papua New Guinea",OWNO,////,////,????,"Bapi, Morobe, Papua-New Guinea",OK
    BPE,////,////,ZBDH,UNK,0.571,Beidaihe,Qinhuangdao,IATA,////,////,Qinhuangdao Beidaihe Airport,"Qinhuangdao, Hubei, China",MAYBE
    BPF,////,////,AGBT,UNK,1.0,Batuna Aerodrome,"Batuna, Solomon Islands",OWNO,////,////,????,"Batuna, Solomon Islands",OK
    BPG,////,////,SBBW,UNK,1.0,Barra Do Garcas,"Barra Do Garcas, Brazil",OWNO,////,////,????,"Barra do Garças, Mato Grosso, Brazil",OK
    BPH,////,////,RPMF,UNK,1.0,Bislig,"Bislig, Philippines",OWNO,////,////,????,"Bislig, Mindanao Island, Philippines",OK
    BPI,BPI,KBPI,KBPI,OK,0.518,Big Piney-Marbleton,"Big Piney (WY), USA",OWNO,MILEY MEMORIAL FIELD,"BIG PINEY, WY - UNITED STATES",Miley Memorial Field,"Big Piney, Wyoming, United States",OK
    BPL,////,////,ZWBL,UNK,nan,////,////,////,////,////,Bole Alashankou Airport,"Bole, Xinjiang, China",UNK
    BPM,////,////,////,UNK,1.0,Begumpet (Hyderabad Old Airport),"Hyderabad, Andhra Pradesh, India",OWNO,////,////,Begumpet,"Hyderabad, Telangana, India",OK
    BPN,////,////,WALL,UNK,0.667,Sepingan,"Balikpapan, Indonesia",OWNO,////,////,Sultan Aji Muhammad Sulaiman Airport,"Balikpapan, Kalimantan Timur (East Borneo), Indonesia",OK
    BPS,////,////,SBPS,UNK,1.0,Porto Seguro,"Porto Seguro, Brazil",OWNO,////,////,????,"Porto Seguro, Bahia, Brazil",OK
    BPT,BPT,KBPT,KBPT,OK,0.367,Southeast Texas Regional Airport,"Beaumont (TX), USA",OWNO,JACK BROOKS RGNL,"BEAUMONT/PORT ARTHUR, TX - UNITED STATES",Jack Brooks Regional,"Beaumont/Port Arthur, Texas, United States",OK
    BPU,////,////,////,UNK,1.0,Beppu,"Beppu, Japan",OWNO,////,////,????,"Beppu, Oita, Japan",OK
    BPX,////,////,ZUBD,UNK,0.966,Bangda,"Bangda, PR China",OWNO,////,////,Qamdo Bamda Airport,"Bangda, Qamdo (Chamdo), Tibet, China",OK
    BPY,////,////,FMNQ,UNK,1.0,Besalampy,"Besalampy, Madagascar",OWNO,////,////,????,"Besalampy, Madagascar",OK
    BQA,////,////,RPUR,UNK,1.0,Baler,"Baler, Philippines",OWNO,////,////,????,"Baler, Luzon Island, Philippines",OK
    BQB,////,////,YBLN,UNK,0.8,Bussellton Airport,"Bussellton, Australia",OWNO,////,////,Busselton Regional Airport,"Busselton, Western Australia, Australia",OK
    BQE,////,////,GGBU,UNK,1.0,Bubaque,"Bubaque, Guinea-Bissau",OWNO,////,////,????,"Bubaque, Bijagós Islands, Guinea-Bissau",OK
    BQG,////,////,UHNB,UNK,nan,////,////,////,////,////,Bogorodskoe Airport,"Bogorodskoe, Khabarovskiy, Russian Federation (Russia)",UNK
    BQH,////,////,EGKB,UNK,1.0,Biggin Hill,"London, United Kingdom",OWNO,////,////,London Biggin Hill,"Bromley, Kent, England, United Kingdom",MAYBE
    BQJ,////,////,UEBB,UNK,1.0,Batagay,Batagay,IATA,////,////,Batagay Airport,"Batagay, Sakha (Yakutiya), Russian Federation (Russia)",MAYBE
    BQK,BQK,KBQK,KBQK,OK,0.587,Glynco Jetport,"Brunswick (GA), USA",OWNO,BRUNSWICK GOLDEN ISLES,"BRUNSWICK, GA - UNITED STATES",Brunswick Golden Isles Airport,"Brunswick, Georgia, United States",OK
    BQL,////,////,YBOU,UNK,1.0,Boulia,"Boulia, Australia",OWNO,////,////,????,"Boulia, Queensland, Australia",OK
    BQN,BQN,TJBQ,TJBQ,OK,0.419,Borinquen,"Aguadilla, Puerto Rico",OWNO,RAFAEL HERNANDEZ,"AGUADILLA, PR - UNITED STATES",Rafael Hernández Airport,"Aguadilla, Puerto Rico, United States",OK
    BQO,////,////,DIBN,UNK,1.0,Bouna,"Bouna, Cote d'Ivoire",OWNO,////,////,????,"Bouna, Zanzan, Côte d'Ivoire (Ivory Coast)",OK
    BQQ,////,////,SNBX,UNK,1.0,Barra,"Barra, Brazil",OWNO,////,////,????,"Barra, Bahia, Brazil",OK
    BQS,////,////,UHBB,UNK,0.75,Blagoveschensk,"Blagoveschensk, Russia",OWNO,////,////,Ignatyevo Airport,"Blagoveshchensk, Amurskaya, Russian Federation (Russia)",OK
    BQT,////,////,UMBB,UNK,1.0,Brest,"Brest, Belarus",OWNO,////,////,Brest Airport,"Brest, Brestskaya (Brest), Belarus",OK
    BQU,////,////,TVSB,UNK,0.857,Bequia Airport (James F. Mitchell),"Port Elizabeth, Saint Vincent and the Grenadines",OWNO,////,////,J.F. Mitchell,"Bequia, Saint Vincent and The Grenadines",MAYBE
    BQV,BQV,////,////,OK,0.663,Bartlett SPB,"Gustavus (AK), USA",OWNO,BARTLETT COVE,"BARTLETT COVE, AK - UNITED STATES",Bartlett Cove SPB,"Bartlett Cove, Alaska, United States",OK
    BQW,////,////,YBGO,UNK,1.0,Balgo Hills,"Balgo Hills, Australia",OWNO,////,////,????,"Balgo Hill, Western Australia, Australia",OK
    BRA,////,////,SNBR,UNK,1.0,Barreiras,"Barreiras, Brazil",OWNO,////,////,Barreiras Airport,"Barreiras, Bahia, Brazil",OK
    BRB,////,////,SBRR,UNK,1.0,Barreirinhas,"Barreirinhas, Brazil",OWNO,////,////,????,"Barreirinhas, Maranhão, Brazil",OK
    BRC,////,////,SAZS,UNK,0.6,International,"San Carlos DeBariloche, Argentina",OWNO,////,////,????,"San Carlos de Bariloche, Río Negro, Argentina",OK
    BRD,BRD,KBRD,KBRD,OK,0.57,Crow Wing County,"Brainerd (MN), USA",OWNO,BRAINERD LAKES RGNL,"BRAINERD, MN - UNITED STATES",Brainerd Lakes Regional,"Brainerd, Minnesota, United States",OK
    BRE,////,////,EDDW,UNK,0.706,Bremen,"Bremen, Germany",OWNO,////,////,Neuenland,"Bremen, Bremen, Germany",OK
    BRH,////,////,////,UNK,1.0,Brahman,"Brahman, Papua New Guinea",OWNO,////,////,????,"Brahman, Madang, Papua-New Guinea",OK
    BRI,////,////,LIBD,UNK,0.519,Palese,"Bari, Italy",OWNO,////,////,Bari Karol Wojtyla International Airport,"Bari, Apulia, Italy",OK
    BRJ,////,////,////,UNK,1.0,Bright,"Bright, Australia",OWNO,////,////,????,"Bright, Victoria, Australia",OK
    BRK,////,////,YBKE,UNK,1.0,Bourke,"Bourke, Australia",OWNO,////,////,????,"Bourke, New South Wales, Australia",OK
    BRL,BRL,KBRL,KBRL,OK,0.445,Burlington,"Burlington IA, USA",OWNO,SOUTHEAST IOWA RGNL,"BURLINGTON, IA - UNITED STATES",Southeast Iowa Regional,"Burlington, Iowa, United States",OK
    BRM,////,////,SVBM,UNK,0.588,Barquisimeto,"Barquisimeto, Venezuela",OWNO,////,////,Jacinto Lara International Airport,"Barquisimeto, Lara, Venezuela",OK
    BRN,////,////,LSZB,UNK,1.0,Belp,"Berne, Switzerland",OWNO,////,////,Bern-Belp,"Bern, Bern, Switzerland",OK
    BRO,BRO,KBRO,KBRO,OK,0.901,South Padre Is. International,"Brownsville (TX), USA",OWNO,BROWNSVILLE/SOUTH PADRE ISLAND INTL,"BROWNSVILLE, TX - UNITED STATES",Brownsville/South Padre Island International Airport,"Brownsville, Texas, United States",OK
    BRP,////,////,////,UNK,1.0,Biaru,"Biaru, Papua New Guinea",OWNO,////,////,????,"Biaru, Gulf, Papua-New Guinea",OK
    BRQ,////,////,LKTB,UNK,1.0,Turany,"Brno, Czech Republic",OWNO,////,////,Brno-Turany Airport,"Brno, South Moravia, Czech Republic",OK
    BRR,////,////,EGPR,UNK,1.0,North Bay,"Barra, United Kingdom",OWNO,////,////,Barra,"North Bay, Isle of Barra, Hebrides, Scotland, United Kingdom",OK
    BRS,////,////,EGGD,UNK,0.778,Bristol,"Bristol, United Kingdom",OWNO,////,////,Bristol International Airport,"Bristol, Somerset, England, United Kingdom",OK
    BRT,////,////,YBTI,UNK,1.0,Bathurst Island,"Bathurst Island, Australia",OWNO,////,////,Bathurst Island,"Nguiu, Bathurst Island, Northern Territory, Australia",OK
    BRU,////,////,EBBR,UNK,0.64,National,"Brussels, Belgium",OWNO,////,////,Brussels Airport,"Brussels, Vlaams-Brabant (Flemish Brabant), Belgium",OK
    BRV,////,////,EDWB,UNK,1.0,Bremerhaven,"Bremerhaven, Germany",OWNO,////,////,????,"Bremerhaven, Bremen, Germany",OK
    BRW,BRW,PABR,PABR,OK,0.501,Wiley Post/W.Rogers M,"Barrow (AK), USA",OWNO,WILEY POST-WILL ROGERS MEMORIAL,"BARROW, AK - UNITED STATES",Wiley Post-Will Rogers Memorial Airport,"Barrow, Alaska, United States",OK
    BRX,////,////,MDBH,UNK,0.471,Barahona,"Barahona, Dominican Republic",OWNO,////,////,María Montez International Airport,"Barahona, Barahona, Dominican Republic",OK
    BRY,BRY,KBRY,KBRY,OK,1.0,Samuels Field,"Bardstown (KY), USA",OWNO,SAMUELS FIELD,"BARDSTOWN, KY - UNITED STATES",Samuels Field,"Bardstown, Kentucky, United States",OK
    BSA,////,////,HCMF,UNK,0.4,Bossaso,"Bossaso, Somalia",OWNO,////,////,Bender Qassim International Airport,"Boosaaso, Bari, Somalia",OK
    BSB,////,////,SBBR,UNK,0.448,International,"Brasilia, Brazil",OWNO,////,////,Presidente Juscelino Kubitschek International Airport,"Brasília, Distrito Federal, Brazil",OK
    BSC,////,////,SKBS,UNK,0.533,Bahia Solano,"Bahia Solano, Colombia",OWNO,////,////,José Celestino Mutis Airport,"Bahía Solano, Chocó, Colombia",OK
    BSD,////,////,ZPBS,UNK,1.0,Baoshan,"Baoshan, PR China",OWNO,////,////,????,"Baoshan, Yunnan, China",OK
    BSE,////,////,WBGN,UNK,1.0,Sematan,"Sematan, Malaysia",OWNO,////,////,????,"Sematan, Sarawak, Malaysia",OK
    BSF,BSF,PHSF,PHSF,OK,0.649,Bradshaw AAF,"Pohakuloa (HI), USA",OWNO,BRADSHAW ARMY AIRFIELD,"CAMP POHAKULOA, HI - UNITED STATES",Bradshaw AAF,"Camp Pohakuloa, Hawaii, Hawaii, United States",OK
    BSG,////,////,FGBT,UNK,1.0,Bata,"Bata, Equatorial Guinea",OWNO,////,////,Bata Airport,"Bata, Litoral, Equatorial Guinea",OK
    BSH,////,////,EGKA,UNK,0.5,Brighton,"Brighton, United Kingdom",OWNO,////,////,Shoreham,"Shoreham-by-Sea, Sussex, England, United Kingdom",TO DO CHECK
    BSJ,////,////,YBNS,UNK,1.0,Bairnsdale,"Bairnsdale, Australia",OWNO,////,////,????,"Bairnsdale, Victoria, Australia",OK
    BSK,////,////,DAUB,UNK,0.476,Biskra,"Biskra, Algeria",OWNO,////,////,Mohamed Khider Airport,"Biskra, Biskra, Algeria",OK
    BSM,////,////,OINJ,UNK,nan,////,////,////,////,////,Bishe Kola AB,"Amol, Mazandaran, Iran",UNK
    BSN,////,////,FEFS,UNK,1.0,Bossangoa,"Bossangoa, Central African Republic",OWNO,////,////,????,"Bossangoa, Ouham (Wâmo), Central African Republic",OK
    BSO,////,////,RPUO,UNK,1.0,Basco,"Basco, Philippines",OWNO,////,////,????,"Basco, Philippines",OK
    BSR,////,////,ORMM,UNK,1.0,International,"Basra, Iraq",OWNO,////,////,Basrah International Airport,"Basrah, Iraq",OK
    BST,////,////,OABT,UNK,1.0,Bost,"Bost, Afghanistan",OWNO,////,////,Bost,"Lashkar Gah, Helmand, Afghanistan",OK
    BSU,////,////,FZEN,UNK,1.0,Basankusu,"Basankusu, Congo (DRC)",OWNO,////,////,Basankusu Airport,"Basankusu, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    BSX,////,////,VYPN,UNK,1.0,Bassein,"Bassein, Myanmar",OWNO,////,////,????,"Bassein, Ayeyawady, Myanmar (Burma)",OK
    BTC,////,////,VCCB,UNK,1.0,Batticaloa,"Batticaloa, Sri Lanka",OWNO,////,////,Batticaloa Airport,"Batticaloa, Eastern Province, Sri Lanka (Ceylon)",OK
    BTE,////,////,GFBN,UNK,0.48,Bonthe,"Bonthe, Sierra Leone",OWNO,////,////,Sherbro International Airport,"Bonthe, Sierra Leone",OK
    BTH,////,////,WIDD,UNK,1.0,Hang Nadim,"Batam, Indonesia",OWNO,////,////,Hang Nadim,"Batam, Batu Besar, Riau, Indonesia",OK

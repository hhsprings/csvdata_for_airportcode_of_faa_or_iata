
List for checking certainty of Great Circle Mapper (HAA - HJR)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    HAA,////,////,ENHK,UNK,1.0,Hasvik Airport,"Hasvik, Norway",OWNO,////,////,????,"Hasvik, Norway",OK
    HAB,HAB,KHAB,KHAB,OK,1.0,Marion County - Rankin Fite Airport,"Hamilton (AL), USA",OWNO,MARION COUNTY-RANKIN FITE,"HAMILTON, AL - UNITED STATES",Marion County-Rankin Fite Airport,"Hamilton, Alabama, United States",OK
    HAC,////,////,RJTH,UNK,1.0,Hachijo Jima,"Hachijo Jima, Japan",OWNO,////,////,Hachijojima Airport,"Hachijojima, Tokyo, Japan",OK
    HAD,////,////,ESMT,UNK,0.842,Halmstad,"Halmstad, Sweden",OWNO,////,////,Halmstad AB,"Halmstad, Hallands län, Sweden",OK
    HAE,AZ15,////,////,OK,0.374,Havasupai,"Havasupai (AZ), USA",OWNO,LAVA FALLS,"SUPAI VILLAGE, AZ - UNITED STATES","Lava Falls Heliport, Havasupai Airport","Supai Village, Arizona, United States",OK
    HAF,HAF,KHAF,KHAF,OK,0.87,Half Moon,"Half Moon (CA), USA",OWNO,HALF MOON BAY,"HALF MOON BAY, CA - UNITED STATES",????,"Half Moon Bay, California, United States",OK
    HAH,////,////,FMCH,UNK,0.967,Prince Said Ibrahim In,"Moroni, Comoros",OWNO,////,////,Prince Saïd Ibrahim International Airport,"Moroni, Comoros",OK
    HAI,HAI,KHAI,KHAI,OK,0.882,Dr Haines,"Three Rivers (MI), USA",OWNO,THREE RIVERS MUNI DR HAINES,"THREE RIVERS, MI - UNITED STATES",Three Rivers Municipal Dr Haines,"Three Rivers, Michigan, United States",OK
    HAJ,////,////,EDDV,UNK,0.667,Hannover-Langenhagen Airport,"Hanover, Germany",OWNO,////,////,????,"Hannover, Lower Saxony, Germany",OK
    HAK,////,////,ZJHK,UNK,0.737,Haikou,"Haikou, PR China",OWNO,////,////,Meilan International Airport,"Haikou, Hainan, China",OK
    HAL,////,////,FYHI,UNK,1.0,Halali,"Halali, Namibia",OWNO,////,////,????,"Halali, Namibia",OK
    HAM,////,////,EDDH,UNK,1.0,Fuhlsbuettel,"Hamburg, Germany",OWNO,////,////,Fuhlsbüttel,"Hamburg, Hamburg, Germany",OK
    HAN,////,////,VVNB,UNK,0.6,Noibai,"Hanoi, Viet Nam",OWNO,////,////,Noi Bai International Airport,"Hanoi, Vietnam",OK
    HAO,HAO,KHAO,KHAO,OK,0.241,Hamilton,"Hamilton (OH), USA",OWNO,BUTLER CO RGNL-HOGAN FIELD,"HAMILTON, OH - UNITED STATES",Butler Co. Regional-Hogan Field,"Hamilton, Ohio, United States",OK
    HAQ,////,////,VRMH,UNK,0.815,Hanimaadhoo,"Hanimaadhoo, Maldives",OWNO,////,////,Hanimaadhoo International Airport,"Hanimaadhoo, Hanimaadhoo Island, Maldives",OK
    HAR,CXY,KCXY,KCXY,OK,0.472,Harrisburg Skyport,"Harrisburg (PA), USA",OWNO,CAPITAL CITY,"HARRISBURG, PA - UNITED STATES",Capital City Airport,"Harrisburg, Pennsylvania, United States",OK
    HAS,////,////,OEHL,UNK,1.0,Hail,"Hail, Saudi Arabia",OWNO,////,////,Ha'il Airport,"Ha'il, Saudi Arabia",OK
    HAT,////,////,YHTL,UNK,1.0,Heathlands,"Heathlands, Australia",OWNO,////,////,????,"Heathlands, Queensland, Australia",OK
    HAU,////,////,ENHD,UNK,0.72,Haugesund,"Haugesund, Norway",OWNO,////,////,Karmøy,"Haugesund, Norway",OK
    HAV,////,////,MUHA,UNK,1.0,Jose Marti International,"Havana, Cuba",OWNO,////,////,José Martí International Airport,"Havana, La Habana, Cuba",OK
    HAW,////,////,EGFE,UNK,1.0,Haverfordwest,"Haverfordwest, United Kingdom",OWNO,////,////,????,"Haverfordwest, Pembrokeshire, Wales, United Kingdom",OK
    HAZ,////,////,////,UNK,1.0,Hatzfeldthaven,"Hatzfeldthaven, Papua New Guinea",OWNO,////,////,????,"Hatzfeldthaven, Madang, Papua-New Guinea",OK
    HBA,////,////,YMHB,UNK,1.0,Hobart,"Hobart, Australia",OWNO,////,////,????,"Hobart, Tasmania, Australia",OK
    HBB,NM83,////,////,OK,1.0,Industrial Airpark,"Hobbs (NM), USA",OWNO,INDUSTRIAL AIRPARK,"HOBBS, NM - UNITED STATES",Industrial Airpark,"Hobbs, New Mexico, United States",OK
    HBE,////,////,HEBA,UNK,1.0,Borg El Arab Airport,"Borg El Arab, Egypt",OWNO,////,////,Borg el Arab,"Alexandria, Al Iskandariyah (Alexandria), Egypt",OK
    HBG,HBG,KHBG,KHBG,OK,1.0,Bobby L. Chain Mun.,"Hattiesburg (MS), USA",OWNO,HATTIESBURG BOBBY L CHAIN MUNI,"HATTIESBURG, MS - UNITED STATES",Hattiesburg Bobby L Chain Municipal Airport,"Hattiesburg, Mississippi, United States",OK
    HBH,2Z1,////,////,OK,0.241,Hobart Bay,"Hobart Bay (AK), USA",OWNO,ENTRANCE ISLAND,"ENTRANCE ISLAND, AK - UNITED STATES",Entrance Island SPB,"Entrance Island, Alaska, United States",OK
    HBR,HBR,KHBR,KHBR,OK,0.734,Hobart,"Hobart (OK), USA",OWNO,HOBART RGNL,"HOBART, OK - UNITED STATES",Hobart Regional,"Hobart, Oklahoma, United States",OK
    HBT,////,////,////,UNK,0.1,SPB,Hambantota,IATA,////,////,Hambantota Waterdrome,"Hambantota, Southern Province, Sri Lanka (Ceylon)",MAYBE
    HBX,////,////,VAHB,UNK,1.0,Hubli,"Hubli, India",OWNO,////,////,????,"Hubli, Karnataka, India",OK
    HCC,1B1,////,////,OK,1.0,Columbia County,"Hudson (NY), USA",OWNO,COLUMBIA COUNTY,"HUDSON, NY - UNITED STATES",Columbia County Airport,"Hudson, New York, United States",OK
    HCJ,////,////,ZGHC,UNK,nan,////,////,////,////,////,Hechi Jinchengjiang Airport,"Hechi, Guangxi, China",UNK
    HCM,////,////,HCME,UNK,1.0,Eil,"Eil, Somalia",OWNO,////,////,Eil Airport,"Eil, Nugaal, Somalia",OK
    HCN,////,////,RCKW,UNK,1.0,Hengchun,"Hengchun, Taiwan",OWNO,////,////,????,"Hengchun, Taiwan",OK
    HCQ,////,////,YHLC,UNK,1.0,Halls Creek,"Halls Creek, Australia",OWNO,////,////,????,"Halls Creek, Western Australia, Australia",OK
    HCR,HCA,PAHC,PAHC,OK,1.0,Holy Cross,"Holy Cross (AK), USA",OWNO,HOLY CROSS,"HOLY CROSS, AK - UNITED STATES",????,"Holy Cross, Alaska, United States",OK
    HCW,CQW,KCQW,KCQW,OK,0.799,Cheraw,"Cheraw (SC), USA",OWNO,CHERAW MUNI/LYNCH BELLINGER FIELD,"CHERAW, SC - UNITED STATES",Cheraw Municipal/Lynch Bellinger Field,"Cheraw, South Carolina, United States",OK
    HDB,////,////,EDIU,UNK,1.0,Heidelberg,"Heidelberg, Germany",OWNO,////,////,????,"Heidelberg, Baden-Württemberg, Germany",OK
    HDD,////,////,OPKD,UNK,1.0,Hyderabad,"Hyderabad, Pakistan",OWNO,////,////,????,"Hyderabad, Sindh, Pakistan",OK
    HDE,HDE,KHDE,KHDE,OK,1.0,Brewster Field,"Holdrege (NE), USA",OWNO,BREWSTER FIELD,"HOLDREGE, NE - UNITED STATES",Brewster Field,"Holdrege, Nebraska, United States",OK
    HDF,////,////,EDAH,UNK,1.0,Heringsdorf Airport,"Heringsdorf, Germany",OWNO,////,////,????,"Heringsdorf, Mecklenburg-Vorpommern, Germany",OK
    HDG,////,////,ZBHD,UNK,nan,////,////,////,////,////,????,"Handan, Hebei, China",UNK
    HDH,HDH,PHDH,PHDH,OK,1.0,Dillingham Airfield,"Oahu (HI), USA",OWNO,DILLINGHAM AIRFIELD,"MOKULEIA, HI - UNITED STATES",Dillingham Airfield,"Mokuleia, Oahu, Hawaii, United States",OK
    HDM,////,////,OIHH,UNK,1.0,Hamadan,"Hamadan, Iran",OWNO,////,////,????,"Hamadan, Hamadan, Iran",OK
    HDN,HDN,KHDN,KHDN,OK,1.0,Yampa Valley,"Hayden (CO), USA",OWNO,YAMPA VALLEY,"HAYDEN, CO - UNITED STATES",Yampa Valley Airport,"Hayden, Colorado, United States",OK
    HDR,////,////,OIKP,UNK,nan,////,////,////,////,////,Havadarya,"Bandar Abbas, Hormozgan, Iran",UNK
    HDS,////,////,FAHS,UNK,1.0,Hoedspruit Airport,"Hoedspruit, South Africa",OWNO,////,////,Hoedspruit Airport,"Hoedspruit, Limpopo, South Africa",OK
    HDY,////,////,VTSS,UNK,0.737,Hat Yai,"Hat Yai, Thailand",OWNO,////,////,Hat Yai International Airport,"Hat Yai, Songkhla, Thailand",OK
    HEA,////,////,OAHR,UNK,0.714,Herat,"Herat, Afghanistan",OWNO,////,////,Herat International Airport,"Herat, Herat, Afghanistan",OK
    HEB,////,////,VBHD,UNK,1.0,Henzada,"Henzada, Myanmar",OWNO,////,////,????,"Henzada, Ayeyawady, Myanmar (Burma)",OK
    HED,AK33,////,////,OK,0.79,Herendeen,"Herendeen (AK), USA",OWNO,HERENDEEN BAY,"HERENDEEN BAY, AK - UNITED STATES",????,"Herendeen Bay, Alaska, United States",OK
    HEE,HEE,KHEE,KHEE,OK,1.0,Thompson-Robbins,"Helena (AR), USA",OWNO,THOMPSON-ROBBINS,"HELENA/WEST HELENA, AR - UNITED STATES",Thompson-Robbins Airport,"Helena/West Helena, Arkansas, United States",OK
    HEH,////,////,VYHH,UNK,1.0,Heho,"Heho, Myanmar",OWNO,////,////,????,"Heho, Shan, Myanmar (Burma)",OK
    HEI,////,////,EDXB,UNK,1.0,Heide-Büsum Airport,"Heide/Buesum, Germany",OWNO,////,////,????,"Heide-Büsum, Schleswig-Holstein, Germany",OK
    HEK,////,////,ZYHE,UNK,1.0,Heihe,"Heihe, PR China",OWNO,////,////,????,"Heihe, Heilongjiang, China",OK
    HEL,////,////,EFHK,UNK,1.0,Helsinki-Vantaa,"Helsinki, Finland",OWNO,////,////,Vantaa,"Helsinki, Uusimaa (Nyland (Uusimaa)), Finland",OK
    HEM,////,////,EFHF,UNK,1.0,Helsinki-Malmi,"Helsinki, Finland",OWNO,////,////,Malmi,"Helsinki, Uusimaa (Nyland (Uusimaa)), Finland",OK
    HEO,////,////,////,UNK,1.0,Haelogo,"Haelogo, Papua New Guinea",OWNO,////,////,????,"Haelogo, Central, Papua-New Guinea",OK
    HER,////,////,LGIR,UNK,0.689,Heraklion International “Nikos Kazantzakis”,"Heraklion, Crete, Greece",OWNO,////,////,Nikos Kazantzakis,"Heraklion, Krítí (Crete), Greece",OK
    HES,HRI,KHRI,KHRI,OK,1.0,Hermiston Municipal Airport,"Hermiston (OR), USA",OWNO,HERMISTON MUNI,"HERMISTON, OR - UNITED STATES",Hermiston Municipal Airport,"Hermiston, Oregon, United States",OK
    HET,////,////,ZBHH,UNK,0.8,Hohhot,"Hohhot, PR China",OWNO,////,////,Baita International Airport,"Hohhot, Inner Mongolia, China",OK
    HEY,HEY,KHEY,KHEY,OK,0.524,Hanchey Army Heliport,"Ozark (AL), USA",OWNO,HANCHEY AHP (FORT RUCKER),"FORT RUCKER OZARK, AL - UNITED STATES",Hanchey Army Heliport,"Fort Rucker, Alabama, United States",OK
    HEZ,HEZ,KHEZ,KHEZ,OK,0.594,Natchez-Adams County Airport,"Natchez (MS), USA",OWNO,HARDY-ANDERS FIELD NATCHEZ-ADAMS COUNTY,"NATCHEZ, MS - UNITED STATES",Hardy-Anders Field Natchez-Adams County Airport,"Natchez, Mississippi, United States",OK
    HFA,////,////,LLHA,UNK,0.667,Haifa,"Haifa, Israel",OWNO,////,////,Haifa International Airport,"Haifa, Israel",OK
    HFD,HFD,KHFD,KHFD,OK,1.0,Brainard,"Hartford (CT), USA",OWNO,HARTFORD-BRAINARD,"HARTFORD, CT - UNITED STATES",Hartford-Brainard Airport,"Hartford, Connecticut, United States",OK
    HFE,////,////,ZSOF,UNK,0.629,Hefei,"Hefei, PR China",OWNO,////,////,Hefei Xinqiao International Airport,"Hefei, Anhui, China",OK
    HFF,HFF,KHFF,KHFF,OK,1.0,Mackall AAF,"Hoffman (NC), USA",OWNO,MACKALL AAF,"CAMP MACKALL, NC - UNITED STATES",Mackall AAF Airport,"Camp Mackall, North Carolina, United States",OK
    HFN,////,////,BIHN,UNK,1.0,Hornafjordur,"Hornafjordur, Iceland",OWNO,////,////,Hornafjörður,"Höfn, Iceland",OK
    HFS,////,////,ESOH,UNK,1.0,Hagfors,"Hagfors, Sweden",OWNO,////,////,????,"Hagfors, Värmlands län, Sweden",OK
    HFT,////,////,ENHF,UNK,1.0,Hammerfest,"Hammerfest, Norway",OWNO,////,////,????,"Hammerfest, Norway",OK
    HGA,////,////,HCMH,UNK,0.762,Hargeisa,"Hargeisa, Somalia",OWNO,////,////,Hargeisa International Airport,"Hargeisa, Woqooyi Galbeed, Somalia",OK
    HGD,////,////,YHUG,UNK,1.0,Hughenden,"Hughenden, Australia",OWNO,////,////,????,"Hughenden, Queensland, Australia",OK
    HGE,////,////,SVHG,UNK,nan,////,////,////,////,////,Higuerote Airport,"Higuerote, Miranda, Venezuela",UNK
    HGH,////,////,ZSHC,UNK,0.75,Hangzhou,"Hangzhou, PR China",OWNO,////,////,Xiaoshan International Airport,"Hangzhou, Zhejiang, China",OK
    HGI,////,////,HSFA,UNK,0.5,Higlieg,Higlieg,IATA,////,////,Paloich Airport,"Paloich, Upper Nile, South Sudan",TO DO CHECK
    HGL,////,////,EDXH,UNK,1.0,Heligoland Airport (Helgoland-Düne Airport),"Helgoland, Schleswig-Holstein, Germany",OWNO,////,////,Düne,"Helgoland, Schleswig-Holstein, Germany",OK
    HGN,////,////,VTCH,UNK,1.0,Mae Hong Son,"Mae Hong Son, Thailand",OWNO,////,////,????,"Mae Hong Son, Mae Hong Son, Thailand",OK
    HGO,////,////,DIKO,UNK,1.0,Korhogo,"Korhogo, Cote d'Ivoire",OWNO,////,////,????,"Korhogo, Savanes, Côte d'Ivoire (Ivory Coast)",OK
    HGR,HGR,KHGR,KHGR,OK,0.49,Wash. County Regional,"Hagerstown (MD), USA",OWNO,HAGERSTOWN RGNL-RICHARD A HENSON FLD,"HAGERSTOWN, MD - UNITED STATES",Hagerstown Regional-Richard A Henson Field,"Hagerstown, Maryland, United States",OK
    HGS,////,////,GFHA,UNK,1.0,Hastings,"Freetown, Sierra Leone",OWNO,////,////,Hastings,"Freetown, Sierra Leone",OK
    HGU,////,////,AYMH,UNK,1.0,Kagamuga,"Mount Hagen, Papua New Guinea",OWNO,////,////,Kagamuga,"Mount Hagen, Southern Highlands, Papua-New Guinea",OK
    HGZ,2AK6,////,////,OK,0.543,Hogatza,"Hogatza (AK), USA",OWNO,HOG RIVER,"HOGATZA, AK - UNITED STATES",Hog River Airport,"Hogatza, Alaska, United States",OK
    HHA,////,////,ZGHA,UNK,0.902,Changsha Huanghua,"Huanghua, PR China",OWNO,////,////,Huanghua International Airport,"Changsha, Hunan, China",OK
    HHE,////,////,RJSH,UNK,0.857,Hachinohe,"Hachinohe, Japan",OWNO,////,////,Hachinohe AB,"Hachinohe, Aomori, Japan",OK
    HHH,HXD,KHXD,KHXD,OK,1.0,Hilton Head,"Hilton Head (SC), USA",OWNO,HILTON HEAD,"HILTON HEAD ISLAND, SC - UNITED STATES",Hilton Head Airport,"Hilton Head Island, South Carolina, United States",OK
    HHI,HHI,PHHI,PHHI,OK,0.911,Wheeler AFB,"Wahiawa (HI), USA",OWNO,WHEELER AAF,"WAHIAWA, HI - UNITED STATES",Wheeler AAF Airport,"Wahiawa, Oahu, Hawaii, United States",OK
    HHN,////,////,EDFH,UNK,1.0,Hahn,"Frankfurt/Main, Germany",OWNO,////,////,Frankfurt-Hahn,"Hahn, Rhineland-Palatinate, Germany",OK
    HHQ,////,////,VTPH,UNK,1.0,Hua Hin Airport,"Hua Hin, Thailand",OWNO,////,////,????,"Hua Hin, Phetchaburi, Thailand",OK
    HHR,HHR,KHHR,KHHR,OK,0.79,Hawthorne,"Hawthorne (CA), USA",OWNO,JACK NORTHROP FIELD/HAWTHORNE MUNI,"HAWTHORNE, CA - UNITED STATES",Jack Northrop Field/Hawthorne Municipal Airport,"Hawthorne, California, United States",OK
    HHZ,////,////,NTGH,UNK,0.7,Hikueru,"Hikueru, French Polynesia",OWNO,////,////,????,"Hikueru Atoll, French Polynesia",MAYBE
    HIA,////,////,ZSSH,UNK,nan,////,////,////,////,////,Huai'an Lianshui Airport,"Huai'an, Jiangsu, China",UNK
    HIB,HIB,KHIB,KHIB,OK,0.397,Chisholm Airport,"Hibbing (MN), USA",OWNO,RANGE RGNL,"HIBBING, MN - UNITED STATES",Range Regional,"Hibbing, Minnesota, United States",OK
    HIE,HIE,KHIE,KHIE,OK,0.65,Regional,"Whitefield (NH), USA",OWNO,MOUNT WASHINGTON RGNL,"WHITEFIELD, NH - UNITED STATES",Mount Washington Regional,"Whitefield, New Hampshire, United States",OK
    HIF,HIF,KHIF,KHIF,OK,1.0,Hill AFB,"Ogden (UT), USA",OWNO,HILL AFB,"OGDEN, UT - UNITED STATES",Hill AFB,"Ogden, Utah, United States",OK
    HIG,////,////,YHHY,UNK,1.0,Highbury,"Highbury, Australia",OWNO,////,////,????,"Highbury, Queensland, Australia",OK
    HII,HII,KHII,KHII,OK,0.821,Municipal,"Lake Havasu City (AZ), USA",OWNO,LAKE HAVASU CITY,"LAKE HAVASU CITY, AZ - UNITED STATES",????,"Lake Havasu City, Arizona, United States",OK
    HIJ,////,////,RJOA,UNK,0.783,International,"Hiroshima, Japan",OWNO,////,////,Hiroshima Airport,"Hiroshima, Hiroshima, Japan",OK
    HIK,////,////,PHIK,UNK,1.0,Hickam AFB,"Honolulu (HI), USA",OWNO,////,////,Hickam AFB,"Honolulu, Oahu, Hawaii, United States",OK
    HIL,////,////,////,UNK,0.8,Shillavo,"Shillavo, Ethiopia",OWNO,////,////,????,"Shilabo, Somali, Ethiopia",OK
    HIM,////,////,VCCH,UNK,nan,////,////,////,////,////,Hingurakgoda Airport,"Minneriya, North Central Province, Sri Lanka (Ceylon)",UNK
    HIN,////,////,RKPS,UNK,0.909,Sacheon,"Chinju, South Korea",OWNO,////,////,Sacheon AB,"Chinju, Republic of Korea (South Korea)",OK
    HIO,HIO,KHIO,KHIO,OK,1.0,Portland,"Hillsboro (OR), USA",OWNO,PORTLAND-HILLSBORO,"PORTLAND, OR - UNITED STATES",Portland-Hillsboro Airport,"Portland, Oregon, United States",OK
    HIP,////,////,YHDY,UNK,1.0,Headingly,"Headingly, Australia",OWNO,////,////,????,"Headingly, Queensland, Australia",OK
    HIR,////,////,AGGH,UNK,0.818,Henderson International,"Honiara, Solomon Islands",OWNO,////,////,Honiara International Airport,"Honiara, Guadalcanal Island, Solomon Islands",OK
    HIS,////,////,////,UNK,0.703,Hayman Island,"Hayman Island, Australia",OWNO,////,////,Hayman Island Resort SPB,"Hayman Island, Queensland, Australia",OK
    HIT,////,////,////,UNK,1.0,Hivaro,"Hivaro, Papua New Guinea",OWNO,////,////,????,"Hivaro, Gulf, Papua-New Guinea",OK
    HIW,////,////,RJBH,UNK,0.857,Hiroshima West,"Hiroshima, Japan",OWNO,////,////,Hiroshima-Nishi Airport,"Hiroshima, Hiroshima, Japan",OK
    HIX,////,////,NTMN,UNK,0.667,Hiva Oa,"Hiva Oa, French Polynesia",OWNO,////,////,Atuona,"Hiva Oa, Marquesas Islands, French Polynesia",OK
    HJJ,////,////,ZGCJ,UNK,1.0,Zhijiang,Huaihua,IATA,////,////,Zhijiang Airport,"Huaihua, Hunan, China",MAYBE
    HJR,////,////,VAKJ,UNK,1.0,Khajuraho,"Khajuraho, India",OWNO,////,////,????,"Khajuraho, Madhya Pradesh, India",OK

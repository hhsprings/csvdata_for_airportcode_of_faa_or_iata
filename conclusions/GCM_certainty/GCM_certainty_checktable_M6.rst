
List for checking certainty of Great Circle Mapper (MSN - MWC)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MSN,MSN,KMSN,KMSN,OK,0.773,Dane County Regional,"Madison (WI), USA",OWNO,DANE COUNTY RGNL-TRUAX FIELD,"MADISON, WI - UNITED STATES",Dane County Regional-Truax Field,"Madison, Wisconsin, United States",OK
    MSO,MSO,KMSO,KMSO,OK,0.441,Johnson-Bell Field,"Missoula (MT), USA",OWNO,MISSOULA INTL,"MISSOULA, MT - UNITED STATES",Missoula International Airport,"Missoula, Montana, United States",OK
    MSP,MSP,KMSP,KMSP,OK,1.0,St Paul International,"Minneapolis, Saint Paul (MN), USA",OWNO,MINNEAPOLIS-ST PAUL INTL/WOLD-CHAMBERLAIN,"MINNEAPOLIS, MN - UNITED STATES",Minneapolis-St. Paul International/Wold-Chamberlain,"Minneapolis, Minnesota, United States",OK
    MSQ,////,////,UMMS,UNK,0.385,Minsk International 2,"Minsk, Belarus",OWNO,////,////,Minsk National Airport,"Minsk, Minskaya (Minsk), Belarus",OK
    MSR,////,////,LTCK,UNK,1.0,Mus,"Mus, Turkey",OWNO,////,////,????,"Mus, Mus, Turkey",OK
    MSS,MSS,KMSS,KMSS,OK,0.856,Richards Field,"Massena (NY), USA",OWNO,MASSENA INTL-RICHARDS FIELD,"MASSENA, NY - UNITED STATES",Massena International-Richards Field,"Massena, New York, United States",OK
    MST,////,////,EHBK,UNK,0.71,Maastricht/Aachen,"Maastricht, Netherlands",OWNO,////,////,Zuid-Limburg,"Maastricht, Limburg, Netherlands",OK
    MSU,////,////,FXMM,UNK,1.0,Moshoeshoe International,"Maseru, Lesotho",OWNO,////,////,Moshoeshoe International Airport,"Maseru, Lesotho",OK
    MSV,MSV,KMSV,KMSV,OK,1.0,Sullivan County International,"Monticello (NY), USA",OWNO,SULLIVAN COUNTY INTL,"MONTICELLO, NY - UNITED STATES",Sullivan County International Airport,"Monticello, New York, United States",OK
    MSW,////,////,HHMS,UNK,0.737,Massawa,"Massawa, Eritrea",OWNO,////,////,Massawa International Airport,"Massawa, Eritrea",OK
    MSX,////,////,FCMM,UNK,1.0,Mossendjo,"Mossendjo, Congo (ROC)",OWNO,////,////,????,"Mossendjo, Congo (Republic of)",OK
    MSY,MSY,KMSY,KMSY,OK,0.585,International,"New Orleans (LA), USA",OWNO,LOUIS ARMSTRONG NEW ORLEANS INTL,"NEW ORLEANS, LA - UNITED STATES",Louis Armstrong New Orléans International Airport,"New Orléans, Louisiana, United States",OK
    MSZ,////,////,FNMO,UNK,1.0,Namibe,"Namibe, Angola",OWNO,////,////,????,"Namibe, Angola",OK
    MTA,////,////,NZMA,UNK,1.0,Matamata,"Matamata, New Zealand",OWNO,////,////,????,"Matamata, New Zealand",OK
    MTB,////,////,SKML,UNK,0.593,Monte Libano,"Monte Libano, Colombia",OWNO,////,////,Montelíbano Airport,"Montelíbano, Córdoba, Colombia",OK
    MTC,MTC,KMTC,KMTC,OK,1.0,Selfridge ANGB,"Mt Clemens (MI), USA",OWNO,SELFRIDGE ANGB,"MOUNT CLEMENS, MI - UNITED STATES",Selfridge Angb Airport,"Mount Clemens, Michigan, United States",OK
    MTD,////,////,YMSF,UNK,0.69,Mt Sandford,"Mt Sandford, Australia",OWNO,////,////,????,"Mount Sanford Station, Northern Territory, Australia",TO DO CHECK
    MTE,////,////,SNMA,UNK,1.0,Monte Alegre,"Monte Alegre, Brazil",OWNO,////,////,????,"Monte Alegre, Pará, Brazil",OK
    MTF,////,////,HAMT,UNK,1.0,Mizan Teferi,"Mizan Teferi, Ethiopia",OWNO,////,////,Mizan Teferi Airport,"Mizan Teferi, SNNPR, Ethiopia",OK
    MTG,////,////,////,UNK,1.0,Mato Grosso,"Mato Grosso, Brazil",OWNO,////,////,????,"Mato Grosso, Mato Grosso, Brazil",OK
    MTH,MTH,KMTH,KMTH,OK,0.445,Marathon Flight Strip,"Marathon (FL), USA",OWNO,THE FLORIDA KEYS MARATHON INTL,"MARATHON, FL - UNITED STATES",The Florida Keys Marathon International Airport,"Marathon, Florida, United States",OK
    MTI,////,////,GVMT,UNK,1.0,Mosteiros,"Mosteiros, Cape Verde",OWNO,////,////,????,"Mosteiros, Fogo Island, Cape Verde",OK
    MTJ,MTJ,KMTJ,KMTJ,OK,0.563,Montrose County,"Montrose (CO), USA",OWNO,MONTROSE RGNL,"MONTROSE, CO - UNITED STATES",Montrose Regional,"Montrose, Colorado, United States",OK
    MTK,////,////,NGMN,UNK,1.0,Makin Island,"Makin Island, Kiribati",OWNO,////,////,????,"Makin Island, Kiribati",OK
    MTL,////,////,YMND,UNK,1.0,Maitland,"Maitland, Australia",OWNO,////,////,????,"Maitland, New South Wales, Australia",OK
    MTM,MTM,PAMM,PAMM,OK,0.801,SPB,"Metlakatla (AK), USA",OWNO,METLAKATLA,"METLAKATLA, AK - UNITED STATES",Metlakatla SPB,"Metlakatla, Alaska, United States",OK
    MTN,MTN,KMTN,KMTN,OK,0.653,Glenn L Martin,"Baltimore (MD), USA",OWNO,MARTIN STATE,"BALTIMORE, MD - UNITED STATES",Martin State Airport,"Baltimore, Maryland, United States",OK
    MTO,MTO,KMTO,KMTO,OK,1.0,Coles County Memorial,"Mattoon (IL), USA",OWNO,COLES COUNTY MEMORIAL,"MATTOON/CHARLESTON, IL - UNITED STATES",Coles County Memorial Airport,"Mattoon/Charleston, Illinois, United States",OK
    MTP,MTP,KMTP,KMTP,OK,0.554,Sky Portal,"Montauk (NY), USA",OWNO,MONTAUK,"MONTAUK, NY - UNITED STATES",????,"Montauk, New York, United States",OK
    MTQ,////,////,YMIT,UNK,1.0,Mitchell,"Mitchell, Australia",OWNO,////,////,????,"Mitchell, Queensland, Australia",OK
    MTR,////,////,SKMR,UNK,0.774,S. Jeronimo,"Monteria, Colombia",OWNO,////,////,Los Garzones Airport,"Montería, Córdoba, Colombia",OK
    MTS,////,////,FDMS,UNK,0.733,Matsapha International,"Manzini (Mbabane), Swaziland",OWNO,////,////,Matasapha Airport,"Manzini, Manzini, Swaziland",OK
    MTT,////,////,MMMT,UNK,1.0,Minatitlan,"Minatitlan, Mexico",OWNO,////,////,Minatitlán/Coatzacoalcos National,"Minatitlán, Veracruz, México",OK
    MTU,////,////,////,UNK,1.0,Montepuez,"Montepuez, Mozambique",OWNO,////,////,????,"Montepuez, Mozambique",OK
    MTV,////,////,NVSA,UNK,1.0,Mota Lava,"Mota Lava, Vanuatu",OWNO,////,////,Mota Lava,"Ablow, Mota Lava Island, Torba, Vanuatu",OK
    MTW,MTW,KMTW,KMTW,OK,0.578,Municipal,"Manitowoc (WI), USA",OWNO,MANITOWOC COUNTY,"MANITOWOC, WI - UNITED STATES",Manitowoc County Airport,"Manitowoc, Wisconsin, United States",OK
    MTX,MTF,////,////,OK,1.0,Metro Field,"Fairbanks (AK), USA",OWNO,METRO FLD,"FAIRBANKS, AK - UNITED STATES",Metro Field,"Fairbanks, Alaska, United States",OK
    MTY,////,////,MMMY,UNK,0.87,Gen Mariano Escobedo,"Monterrey, Mexico",OWNO,////,////,General Mariano Escobedo International Airport,"Monterrey, Nuevo León, México",OK
    MTZ,////,////,LLMZ,UNK,0.545,Masada,"Masada, Israel",OWNO,////,////,I Bar Yehuda,"Masada, Israel",OK
    MUA,////,////,AGGM,UNK,1.0,Munda,"Munda, Solomon Islands",OWNO,////,////,????,"Munda, New Georgia Island, Solomon Islands",OK
    MUB,////,////,FBMN,UNK,1.0,Maun,"Maun, Botswana",OWNO,////,////,Maun Airport,"Maun, North-West, Botswana",OK
    MUC,////,////,EDDM,UNK,0.612,Metropolitan Area,"Munich, Germany",OWNO,////,////,Franz Josef Strauss International Airport,"Munich, Bavaria, Germany",OK
    MUD,////,////,FQMD,UNK,1.0,Mueda,"Mueda, Mozambique",OWNO,////,////,????,"Mueda, Mozambique",OK
    MUE,MUE,PHMU,PHMU,OK,0.51,Kamuela,"Kamuela (HI), USA",OWNO,WAIMEA-KOHALA,"KAMUELA, HI - UNITED STATES",Waimea-Kohala Airport,"Kamuela, Hawaii, Hawaii, United States",OK
    MUF,////,////,////,UNK,1.0,Muting,"Muting, Indonesia",OWNO,////,////,????,"Muting, Papua, Indonesia",OK
    MUG,////,////,MMMG,UNK,1.0,Mulege,"Mulege, Mexico",OWNO,////,////,????,"Mulegé, Baja California Sur, México",OK
    MUH,////,////,HEMM,UNK,1.0,Mersa Matruh,"Mersa Matruh, Egypt",OWNO,////,////,????,"Mersa Matruh, Matruh (Matrouh), Egypt",OK
    MUI,MUI,KMUI,KMUI,OK,1.0,Muir AAF,"Fort Indiantown (PA), USA",OWNO,MUIR AAF (FORT INDIANTOWN GAP),"FORT INDIANTOWN GAP(ANNVILLE), PA - UNITED STATES",Muir AAF Airport,"Fort Indiantown Gap, Pennsylvania, United States",OK
    MUJ,////,////,HAMR,UNK,1.0,Mui,"Mui, Ethiopia",OWNO,////,////,Mui Airport,"Mui, SNNPR, Ethiopia",OK
    MUK,////,////,NCMK,UNK,0.615,Mauke Island,"Mauke Island, Cook Islands",OWNO,////,////,Akatoka Manava,"Mauke Island, Cook Islands",OK
    MUL,MUL,KMUL,KMUL,OK,1.0,Spence,"Moultrie (GA), USA",OWNO,SPENCE,"MOULTRIE, GA - UNITED STATES",Spence Airport,"Moultrie, Georgia, United States",OK
    MUN,////,////,SVMT,UNK,0.8,Quiriquire,"Maturin, Venezuela",OWNO,////,////,Maturín Airport,"Maturín, Monagas, Venezuela",OK
    MUO,MUO,KMUO,KMUO,OK,1.0,AFB,"Mountain Home (ID), USA",OWNO,MOUNTAIN HOME AFB,"MOUNTAIN HOME, ID - UNITED STATES",Mountain Home AFB,"Mountain Home, Idaho, United States",OK
    MUP,////,////,YMUP,UNK,1.0,Mulga Park,"Mulga Park, Australia",OWNO,////,////,????,"Mulga Park, Northern Territory, Australia",OK
    MUQ,////,////,YMUC,UNK,0.625,Muccan,"Muccan, Australia",OWNO,////,////,????,"Muccan Station, Western Australia, Australia",MAYBE
    MUR,////,////,WBGM,UNK,1.0,Marudi,"Marudi, Malaysia",OWNO,////,////,????,"Marudi, Sarawak, Malaysia",OK
    MUS,////,////,RJAM,UNK,0.435,Marcus Island,"Marcus Island, Japan",OWNO,////,////,Minami Torishima Airport,"Minami Torishima, Tokyo, Japan",TO DO CHECK
    MUT,MUT,KMUT,KMUT,OK,0.719,Muscatine,"Muscatine IA, USA",OWNO,MUSCATINE MUNI,"MUSCATINE, IA - UNITED STATES",Muscatine Municipal Airport,"Muscatine, Iowa, United States",OK
    MUW,////,////,DAOV,UNK,0.667,Mascara,"Mascara, Algeria",OWNO,////,////,Ghriss Airport,"Ghriss/Mascara, Mascara, Algeria",OK
    MUX,////,////,OPMT,UNK,0.706,Multan,"Multan, Pakistan",OWNO,////,////,Multan International Airport,"Multan, Punjab, Pakistan",OK
    MUY,////,////,FCBM,UNK,1.0,Mouyondzi,"Mouyondzi, Congo (ROC)",OWNO,////,////,????,"Mouyondzi, Congo (Republic of)",OK
    MUZ,////,////,HTMU,UNK,1.0,Musoma,"Musoma, Tanzania",OWNO,////,////,Musoma Airport,"Musoma, Mara, Tanzania",OK
    MVA,////,////,BIRL,UNK,0.963,Reykiahlid,"Myvatn, Iceland",OWNO,////,////,Reykjahlíð,"Myvatn, Iceland",OK
    MVB,////,////,FOON,UNK,1.0,Franceville/Mvengue,"Franceville, Gabon",OWNO,////,////,????,"Franceville/Mvengue, Haut-Ogooué, Gabon",OK
    MVC,MVC,KMVC,KMVC,OK,1.0,Monroe County,"Monroeville (AL), USA",OWNO,MONROE COUNTY,"MONROEVILLE, AL - UNITED STATES",Monroe County Airport,"Monroeville, Alabama, United States",OK
    MVD,////,////,SUMU,UNK,1.0,Carrasco,"Montevideo, Uruguay",OWNO,////,////,Carrasco/General Cesáreo L. Berisso International Airport,"Montevideo, Canelones, Uruguay",OK
    MVE,MVE,KMVE,KMVE,OK,0.445,Municipal,"Montevideo (MN), USA",OWNO,MONTEVIDEO-CHIPPEWA COUNTY,"MONTEVIDEO, MN - UNITED STATES",Montevideo-Chippewa County Airport,"Montevideo, Minnesota, United States",OK
    MVF,////,////,SBMS,UNK,0.8,Dixsept Rosado,"Mossoro, Brazil",OWNO,////,////,Dix Sept Rosado,"Mossoró, Rio Grande do Norte, Brazil",OK
    MVG,////,////,////,UNK,1.0,Mevang,"Mevang, Gabon",OWNO,////,////,????,"Mevang, Moyen-Ogooué, Gabon",OK
    MVH,////,////,////,UNK,1.0,Macksville,"Macksville, Australia",OWNO,////,////,????,"Macksville, New South Wales, Australia",OK
    MVI,////,////,////,UNK,1.0,Manetai,"Manetai, Papua New Guinea",OWNO,////,////,????,"Manetai, Bougainville, Papua-New Guinea",OK
    MVK,////,////,YMUK,UNK,1.0,Mulka,"Mulka, Australia",OWNO,////,////,????,"Mulka, South Australia, Australia",OK
    MVL,MVL,KMVL,KMVL,OK,0.797,Morrisville-Stowe,"Stowe (VT), USA",OWNO,MORRISVILLE-STOWE STATE,"MORRISVILLE, VT - UNITED STATES",Morrisville-Stowe State Airport,"Morrisville, Vermont, United States",OK
    MVM,0V7,////,////,OK,0.425,Monument Valley,"Kayenta (AZ), USA",OWNO,KAYENTA,"KAYENTA, AZ - UNITED STATES",????,"Kayenta, Arizona, United States",OK
    MVN,MVN,KMVN,KMVN,OK,0.679,Mt Vernon-Outland,"Mt Vernon (IL), USA",OWNO,MOUNT VERNON,"MOUNT VERNON, IL - UNITED STATES",????,"Mount Vernon, Illinois, United States",OK
    MVO,////,////,FTTM,UNK,1.0,Mongo,"Mongo, Chad",OWNO,////,////,????,"Mongo, Guéra, Chad",OK
    MVP,////,////,SKMU,UNK,0.229,Mitu,"Mitu, Colombia",OWNO,////,////,Fabio Alberto León Bentley Airport,"Mitú, Vaupés, Colombia",OK
    MVQ,////,////,UMOO,UNK,1.0,Mogilev,"Mogilev, Belarus",OWNO,////,////,Mogilev Airport,"Mogilev, Mahilyowskaya (Mogilev), Belarus",OK
    MVR,////,////,FKKL,UNK,0.917,Salam,"Maroua, Cameroon",OWNO,////,////,Maroua Salak Airport,"Maroua, Far North (Extrême-Nord), Cameroon",OK
    MVS,////,////,SNMU,UNK,1.0,Mucuri,"Mucuri, Brazil",OWNO,////,////,????,"Mucuri, Bahia, Brazil",OK
    MVT,////,////,NTGV,UNK,1.0,Mataiva,"Mataiva, French Polynesia",OWNO,////,////,????,"Mataiva, French Polynesia",OK
    MVU,////,////,YMGV,UNK,1.0,Musgrave,"Musgrave, Australia",OWNO,////,////,????,"Musgrave, Queensland, Australia",OK
    MVV,////,////,LFHM,UNK,1.0,Megeve,"Megeve, France",OWNO,////,////,????,"Megève, Rhône-Alpes, France",OK
    MVW,BVS,KBVS,KBVS,OK,1.0,Skagit Regional,"Mount Vernon (WA), USA",OWNO,SKAGIT RGNL,"BURLINGTON/MOUNT VERNON, WA - UNITED STATES",Skagit Regional,"Burlington/Mount Vernon, Washington, United States",OK
    MVX,////,////,FOGV,UNK,1.0,Minvoul,"Minvoul, Gabon",OWNO,////,////,Minvoul Airport,"Minvoul, Woleu-Ntem, Gabon",OK
    MVY,MVY,KMVY,KMVY,OK,1.0,Martha's Vineyard,"Martha's Vineyard (MA), USA",OWNO,MARTHA'S VINEYARD,"VINEYARD HAVEN, MA - UNITED STATES",Martha's Vineyard Airport,"Vineyard Haven, Massachusetts, United States",OK
    MVZ,////,////,FVMV,UNK,0.762,Masvingo,"Masvingo, Zimbabwe",OWNO,////,////,Masvingo International Airport,"Masvingo, Zimbabwe",OK
    MWA,MWA,KMWA,KMWA,OK,0.865,Williamson County,"Marion (IL), USA",OWNO,WILLIAMSON COUNTY RGNL,"MARION, IL - UNITED STATES",Williamson County Regional,"Marion, Illinois, United States",OK
    MWB,////,////,YMRW,UNK,1.0,Morawa,"Morawa, Australia",OWNO,////,////,????,"Morawa, Western Australia, Australia",OK
    MWC,MWC,KMWC,KMWC,OK,1.0,Lawrence J Timmerman,"Milwaukee (WI), USA",OWNO,LAWRENCE J TIMMERMAN,"MILWAUKEE, WI - UNITED STATES",Lawrence J Timmerman Airport,"Milwaukee, Wisconsin, United States",OK

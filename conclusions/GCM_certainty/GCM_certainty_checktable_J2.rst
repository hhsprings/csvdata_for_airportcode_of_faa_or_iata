
List for checking certainty of Great Circle Mapper (JNB - JZH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    JNB,////,////,FAOR,UNK,0.813,Johannesburg International,"Johannesburg, South Africa",OWNO,////,////,O.R. Tambo International Airport,"Johannesburg, Gauteng, South Africa",OK
    JNG,////,////,ZLJN,UNK,1.0,Jining,"Jining, PR China",OWNO,////,////,????,"Jining, Shandong, China",OK
    JNI,////,////,SAAJ,UNK,1.0,Junin,"Junin, Argentina",OWNO,////,////,????,"Junin, Buenos Aires, Argentina",OK
    JNN,////,////,BGNN,UNK,0.714,Nanortalik,"Nanortalik, Greenland",OWNO,////,////,Nanortalik Heliport,"Nanortalik, Kujalleq, Greenland",OK
    JNS,////,////,BGNS,UNK,1.0,Heliport,"Narsaq, Greenland",OWNO,////,////,Narsaq Helipport,"Narsaq, Kujalleq, Greenland",OK
    JNU,JNU,PAJN,PAJN,OK,1.0,Juneau International,"Juneau (AK), USA",OWNO,JUNEAU INTL,"JUNEAU, AK - UNITED STATES",Juneau International Airport,"Juneau, Alaska, United States",OK
    JNX,////,////,LGNX,UNK,1.0,Naxos Airport,"Naxos, Greece",OWNO,////,////,????,"Naxos, Cyclades Islands, Notío Aigaío (Southern Aegean), Greece",OK
    JNZ,////,////,////,UNK,0.917,Jinzhou,"Jinzhou, PR China",OWNO,////,////,Jinzhou Bay Airport,"Jinzhou, Liaoning, China",OK
    JOE,////,////,EFJO,UNK,1.0,Joensuu,"Joensuu, Finland",OWNO,////,////,????,"Joensuu, Pohjois-Karjala (Norra Karelen (North Karelia)), Finland",OK
    JOG,////,////,WARJ,UNK,0.833,Adisutjipto,"Yogyakarta, Indonesia",OWNO,////,////,Adisucipto International Airport,"Yogyakarta, Yogyakarta, Indonesia",OK
    JOH,////,////,FAPJ,UNK,0.897,Port Saint Johns,"Port Saint Johns, South Africa",OWNO,////,////,Port St. Johns Airport,"Port St. Johns, Eastern Cape, South Africa",MAYBE
    JOI,////,////,SBJV,UNK,0.556,Cubatao,"Joinville, Brazil",OWNO,////,////,Lauro Carneiro de Loyola,"Joinville, Santa Catarina, Brazil",OK
    JOK,////,////,UWKJ,UNK,0.625,Joshkar-Ola,"Joshkar-Ola, Russia",OWNO,////,////,Yoshkar-Ola Airport,"Yoshkar-Ola, Mariy El, Russian Federation (Russia)",TO DO CHECK
    JOL,////,////,RPMJ,UNK,1.0,Jolo,"Jolo, Philippines",OWNO,////,////,????,"Jolo, Philippines",OK
    JOM,////,////,HTNJ,UNK,1.0,Njombe,"Njombe, Tanzania",OWNO,////,////,Njombe Airport,"Njombe, Iringa, Tanzania",OK
    JOP,////,////,////,UNK,1.0,Josephstaal,"Josephstaal, Papua New Guinea",OWNO,////,////,????,"Josephstaal, Madang, Papua-New Guinea",OK
    JOS,////,////,DNJO,UNK,0.316,Jos,"Jos, Nigeria",OWNO,////,////,Yakubu Gowon Airport,"Jos, Plateau, Nigeria",OK
    JOT,JOT,KJOT,KJOT,OK,0.722,Municipal,"Joliet (IL), USA",OWNO,JOLIET RGNL,"JOLIET, IL - UNITED STATES",Joliet Regional,"Joliet, Illinois, United States",OK
    JPA,////,////,SBJP,UNK,0.75,Castro Pinto Intl.,"Joao Pessoa, Brazil",OWNO,////,////,Presidente Castro Pinto,"Joao Pessoa, Paraíba, Brazil",OK
    JPR,////,////,SWJI,UNK,0.6,Ji-Parana,"Ji-Parana, Brazil",OWNO,////,////,José Coleto Airport,"Ji-Paraná, Rondônia, Brazil",OK
    JPU,////,////,////,UNK,1.0,La Defense Heliport,"Paris, France",OWNO,////,////,La Défense Heliport,"Paris, Île-de-France, France",OK
    JQA,////,////,BGUQ,UNK,1.0,Qaarsut,"Qaarsut, Greenland",OWNO,////,////,Qaarsut,"Uummannaq, Qaasuitsup, Greenland",OK
    JQE,////,////,MPJE,UNK,1.0,Jaque,"Jaque, Panama",OWNO,////,////,????,"Jaqué, Darién, Panamá",OK
    JRA,JRA,KJRA,KJRA,OK,0.679,West 30th St Heliport,"New York (NY), USA",OWNO,WEST 30TH ST,"NEW YORK, NY - UNITED STATES",West 30th St. Heliport,"New York, New York, United States",OK
    JRB,JRB,KJRB,KJRB,OK,0.929,Downtown Manhattan H/P,"New York (NY), USA",OWNO,DOWNTOWN MANHATTAN/WALL ST,"NEW YORK, NY - UNITED STATES",Downtown Manhattan/Wall St. Heliport,"New York, New York, United States",OK
    JRF,JRF,PHJR,PHJR,OK,1.0,////,////,////,KALAELOA (JOHN RODGERS FIELD),"KAPOLEI, HI - UNITED STATES",Kalaeloa Airport,"Kapolei, Oahu, Hawaii, United States",OK
    JRH,////,////,VEJT,UNK,0.769,Rowriah,"Jorhat, India",OWNO,////,////,????,"Jorhat, Assam, India",OK
    JRN,////,////,SWJU,UNK,0.56,Juruena,"Juruena, Brazil",OWNO,////,////,Aeropuerto Juruena,"Juruena, Mato Grosso, Brazil",OK
    JRO,////,////,HTKJ,UNK,0.833,Kilimanjaro,"Kilimanjaro, Tanzania",OWNO,////,////,Kilimanjaro International Airport,"Kilimanjaro, Kilimanjaro, Tanzania",OK
    JRS,////,////,OJJR,UNK,1.0,Atarot,"Jerusalem, Israel",OWNO,////,////,Atarot,"Jerusalem, Israel",OK
    JSA,////,////,VIJR,UNK,1.0,Jaisalmer,"Jaisalmer, India",OWNO,////,////,????,"Jaisalmer, Rajasthan, India",OK
    JSH,////,////,LGST,UNK,1.0,Sitia,"Sitia, Greece",OWNO,////,////,????,"Sitia, Crete, Krítí (Crete), Greece",OK
    JSI,////,////,LGSK,UNK,1.0,Skiathos,"Skiathos, Greece",OWNO,////,////,????,"Skiathos, Thessalía (Thessaly), Greece",OK
    JSM,////,////,SAWS,UNK,1.0,Jose De San Martin,"Jose De San Martin, Argentina",OWNO,////,////,????,"José de San Martín, Chubut, Argentina",OK
    JSR,////,////,VGJR,UNK,1.0,Jessore,"Jessore, Bangladesh",OWNO,////,////,????,"Jessore, Bangladesh",OK
    JST,JST,KJST,KJST,OK,0.73,Cambria County,"Johnstown (PA), USA",OWNO,JOHN MURTHA JOHNSTOWN-CAMBRIA CO,"JOHNSTOWN, PA - UNITED STATES",John Murtha Johnstown-Cambria County Airport,"Johnstown, Pennsylvania, United States",OK
    JSU,////,////,BGMQ,UNK,0.471,Heliport,"Maniitsoq, Greenland",OWNO,////,////,????,"Sukkertoppen, Qeqqata, Greenland",TO DO CHECK
    JSY,////,////,LGSO,UNK,1.0,Syros Island,"Syros Island, Greece",OWNO,////,////,????,"Syros Island, Notío Aigaío (Southern Aegean), Greece",OK
    JTC,////,////,SBAE,UNK,0.893,Moussa Nakhl Tobias (Bauru-Arealva Apt.),"Bauru, Brazil",OWNO,////,////,Moussa Nakhl Tobias State Airport,"Bauru, São Paulo, Brazil",OK
    JTR,////,////,LGSR,UNK,0.588,Thira,"Thira, Greece",OWNO,////,////,????,"Santorini, Thira Island, Notío Aigaío (Southern Aegean), Greece",MAYBE
    JTY,////,////,LGPL,UNK,1.0,Astypalaia,"Astypalaia Island, Greece",OWNO,////,////,Astypalaia,"Astypalaia Island, Notío Aigaío (Southern Aegean), Greece",OK
    JUA,////,////,SIZX,UNK,0.278,Juara,"Juara, Brazil",OWNO,////,////,Inácio Luís do Nascimento Airport,"Juara, Mato Grosso, Brazil",OK
    JUB,////,////,HSSJ,UNK,1.0,Juba International Airport,"Juba, South Sudan",OWNO,////,////,Juba International Airport,"Juba, Central Equatoria, South Sudan",OK
    JUH,////,////,ZSJH,UNK,0.714,Jiuhuashan,Chizhou,IATA,////,////,Chizhou Jiuhuashan Airport,"Chizhou, Anhui, China",MAYBE
    JUI,////,////,EDWJ,UNK,0.588, ,"Juist island, Germany",OWNO,////,////,????,"Juist, Lower Saxony, Germany",MAYBE
    JUJ,////,////,SASJ,UNK,0.471,El Cadillal,"Jujuy, Argentina",OWNO,////,////,????,"Jujuy, Jujuy, Argentina",OK
    JUL,////,////,SPJL,UNK,0.389,Juliaca,"Juliaca, Peru",OWNO,////,////,Inca Manco Cápac International Airport,"Juliaca, Puno, Perú",OK
    JUM,////,////,VNJL,UNK,1.0,Jumla,"Jumla, Nepal",OWNO,////,////,????,"Jumla, Nepal",OK
    JUN,////,////,YJDA,UNK,1.0,Jundah,"Jundah, Australia",OWNO,////,////,????,"Jundah, Queensland, Australia",OK
    JUO,////,////,////,UNK,1.0,Jurado,"Jurado, Colombia",OWNO,////,////,????,"Jurado, Antioquia, Colombia",OK
    JUR,////,////,YJNB,UNK,1.0,Jurien Bay,"Jurien Bay, Australia",OWNO,////,////,????,"Jurien Bay, Western Australia, Australia",OK
    JUT,////,////,MHJU,UNK,1.0,Juticalpa,"Juticalpa, Honduras",OWNO,////,////,????,"Juticalpa, Olancho, Honduras",OK
    JUV,////,////,BGUK,UNK,0.706,Heliport,"Upernavik, Greenland",OWNO,////,////,????,"Upernavik, Qaasuitsup, Greenland",OK
    JUZ,////,////,ZSJU,UNK,0.933,Juzhou,"Juzhou, PR China",OWNO,////,////,????,"Quzhou, Zhejiang, China",OK
    JVA,////,////,FMMK,UNK,1.0,Ankavandra,"Ankavandra, Madagascar",OWNO,////,////,????,"Ankavandra, Madagascar",OK
    JVI,47N,////,////,OK,0.429,Kupper,"Manville (NJ), USA",OWNO,CENTRAL JERSEY RGNL,"MANVILLE, NJ - UNITED STATES",Central Jersey Regional,"Manville, New Jersey, United States",OK
    JVL,JVL,KJVL,KJVL,OK,0.549,Rock County,"Janesville (WI), USA",OWNO,SOUTHERN WISCONSIN RGNL,"JANESVILLE, WI - UNITED STATES",Southern Wisconsin Regional,"Janesville, Wisconsin, United States",OK
    JWA,////,////,FBJW,UNK,1.0,Jwaneng,"Jwaneng, Botswana",OWNO,////,////,Jwaneng Airport,"Jwaneng, Southern, Botswana",OK
    JWN,////,////,OITZ,UNK,1.0,Zanjan,"Zanjan, Iran",OWNO,////,////,????,"Zanjan, Zanjan, Iran",OK
    JXA,////,////,ZYJX,UNK,nan,////,////,////,////,////,Jixi Xingkaihu Airport,"Jixi, Heilongjiang, China",UNK
    JXN,JXN,KJXN,KJXN,OK,0.563,Reynolds Municipal,"Jackson (MI), USA",OWNO,JACKSON COUNTY-REYNOLDS FIELD,"JACKSON, MI - UNITED STATES",Jackson County-Reynolds Field,"Jackson, Michigan, United States",OK
    JYR,////,////,OIKJ,UNK,nan,////,////,////,////,////,Jiroft,"Jiroft, Kerman, Iran",UNK
    JYV,////,////,EFJY,UNK,1.0,Jyvaskyla,"Jyvaskyla, Finland",OWNO,////,////,????,"Jyväskylä, Keski-Suomi (Mellersta Finland (Central Finland)), Finland",OK
    JZH,////,////,ZUJZ,UNK,nan,////,////,////,////,////,Jiuzhaigou Huanglong,"Jiuzhaigou, Sichuan, China",UNK

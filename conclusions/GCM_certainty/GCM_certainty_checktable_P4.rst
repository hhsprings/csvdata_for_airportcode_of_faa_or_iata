
List for checking certainty of Great Circle Mapper (PSU - PZY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    PSU,////,////,WIOP,UNK,0.69,Putussibau,"Putussibau, Indonesia",OWNO,////,////,Pangsuma Airport,"Putussibau, Kalimantan Barat (West Borneo), Indonesia",OK
    PSV,////,////,////,UNK,1.0,Papa Stour,"Papa Stour, United Kingdom",OWNO,////,////,????,"Papa Stour, Shetland Islands, Scotland, United Kingdom",OK
    PSW,////,////,SNOS,UNK,1.0,Passos,"Passos, Brazil",OWNO,////,////,????,"Passos, Minas Gerais, Brazil",OK
    PSX,PSX,KPSX,KPSX,OK,0.719,Palacios,"Palacios (TX), USA",OWNO,PALACIOS MUNI,"PALACIOS, TX - UNITED STATES",Palacios Municipal Airport,"Palacios, Texas, United States",OK
    PSY,////,////,SFAL,UNK,1.0,Port Stanley,"Port Stanley, Falkland Islands",OWNO,////,////,Port Stanley,"Stanley, Falkland Islands",OK
    PSZ,////,////,SLPS,UNK,0.5,Puerto Suarez,"Puerto Suarez, Bolivia",OWNO,////,////,Capitan Av. Salvador Ogaya,"Puerto Suárez, Germán Busch, Potosí, Bolivia",OK
    PTA,TPO,////,PALJ,NG,1.0,Port Alsworth,"Port Alsworth (AK), USA",OWNO,PORT ALSWORTH,"PORT ALSWORTH, AK - UNITED STATES",????,"Port Alsworth, Alaska, United States",OK
    PTB,PTB,KPTB,KPTB,OK,0.627,Municipal,"Petersburg (VA), USA",OWNO,DINWIDDIE COUNTY,"PETERSBURG, VA - UNITED STATES",Dinwiddie County Airport,"Petersburg, Virginia, United States",OK
    PTC,16K,////,////,OK,0.801,Port Alice,"Port Alice (AK), USA",OWNO,PORT ALICE,"PORT ALICE, AK - UNITED STATES",Port Alice SPB,"Port Alice, Alaska, United States",OK
    PTD,AHP,PAAP,PAAP,OK,0.834,Port Alexander,"Port Alexander (AK), USA",OWNO,PORT ALEXANDER,"PORT ALEXANDER, AK - UNITED STATES",Port Alexander SPB,"Port Alexander, Alaska, United States",OK
    PTF,////,////,NFFO,UNK,0.375,Malololailai,"Malololailai, Fiji",OWNO,////,////,????,"Malolo Lailai Island, Fiji",TO DO CHECK
    PTG,////,////,FAPP,UNK,0.364,Pietersburg,"Pietersburg, South Africa",OWNO,////,////,Polokwane International Airport,"Polokwane, Limpopo, South Africa",TO DO CHECK
    PTH,PTH,PAPH,PAPH,OK,1.0,Port Heiden,"Port Heiden (AK), USA",OWNO,PORT HEIDEN,"PORT HEIDEN, AK - UNITED STATES",????,"Port Heiden, Alaska, United States",OK
    PTJ,////,////,YPOD,UNK,1.0,Portland,"Portland, Australia",OWNO,////,////,????,"Portland, Victoria, Australia",OK
    PTK,PTK,KPTK,KPTK,OK,0.378,Pontiac,"Pontiac (MI), USA",OWNO,OAKLAND COUNTY INTL,"PONTIAC, MI - UNITED STATES",Oakland County International Airport,"Pontiac, Michigan, United States",OK
    PTM,////,////,SVPT,UNK,1.0,Palmarito,"Palmarito, Venezuela",OWNO,////,////,????,"Palmarito, Apure, Venezuela",OK
    PTN,PTN,KPTN,KPTN,OK,0.863,Williams Memorial,"Patterson (LA), USA",OWNO,HARRY P WILLIAMS MEMORIAL,"PATTERSON, LA - UNITED STATES",Harry P Williams Memorial Airport,"Patterson, Louisiana, United States",OK
    PTO,////,////,SSPB,UNK,0.857,Municipal,"Pato Branco, Brazil",OWNO,////,////,????,"Pato Branco, Paraná, Brazil",OK
    PTP,////,////,TFFR,UNK,1.0,Le Raizet,"Pointe-a-Pitre, Guadeloupe",OWNO,////,////,Le Raizet,"Pointe-à-Pitre, Guadeloupe",OK
    PTQ,////,////,SNMZ,UNK,1.0,Porto de Moz,"Porto de Moz, Brazil",OWNO,////,////,????,"Pôrto de Moz, Pará, Brazil",OK
    PTS,PTS,KPTS,KPTS,OK,0.845,Municipal,"Pittsburg (KS), USA",OWNO,ATKINSON MUNI,"PITTSBURG, KS - UNITED STATES",Atkinson Municipal Airport,"Pittsburg, Kansas, United States",OK
    PTT,PTT,KPTT,KPTT,OK,0.694,Pratt,"Pratt (KS), USA",OWNO,PRATT RGNL,"PRATT, KS - UNITED STATES",Pratt Regional,"Pratt, Kansas, United States",OK
    PTU,PTU,PAPM,PAPM,OK,1.0,Platinum,"Platinum (AK), USA",OWNO,PLATINUM,"PLATINUM, AK - UNITED STATES",????,"Platinum, Alaska, United States",OK
    PTV,PTV,KPTV,KPTV,OK,0.81,Porterville,"Porterville (CA), USA",OWNO,PORTERVILLE MUNI,"PORTERVILLE, CA - UNITED STATES",Porterville Municipal Airport,"Porterville, California, United States",OK
    PTW,PTW,KPTW,KPTW,OK,0.653,Pottstown/Limerick,"Pottstown (PA), USA",OWNO,HERITAGE FIELD,"POTTSTOWN, PA - UNITED STATES",Heritage Field,"Pottstown, Pennsylvania, United States",OK
    PTX,////,////,SKPI,UNK,1.0,Pitalito,"Pitalito, Colombia",OWNO,////,////,Pitalito Airport,"Pitalito, Huila, Colombia",OK
    PTY,////,////,MPTO,UNK,1.0,Tocumen International,"Panama City, Panama",OWNO,////,////,Tocumen International Airport,"Panamá City, Panamá, Panamá",OK
    PTZ,////,////,SESM,UNK,0.545,Pastaza,"Pastaza, Ecuador",OWNO,////,////,Rio Amazonas,"Shell, Pastaza, Ecuador",OK
    PUA,////,////,////,UNK,1.0,Puas,"Puas, Papua New Guinea",OWNO,////,////,????,"Puas, New Ireland, Papua-New Guinea",OK
    PUB,PUB,KPUB,KPUB,OK,1.0,Memorial,"Pueblo (CO), USA",OWNO,PUEBLO MEMORIAL,"PUEBLO, CO - UNITED STATES",Pueblo Memorial Airport,"Pueblo, Colorado, United States",OK
    PUC,PUC,KPUC,KPUC,OK,0.821,Carbon County,"Price (UT), USA",OWNO,CARBON COUNTY RGNL/BUCK DAVIS FIELD,"PRICE, UT - UNITED STATES",Carbon County Regional/Buck Davis Field,"Price, Utah, United States",OK
    PUD,////,////,SAWD,UNK,1.0,Puerto Deseado,"Puerto Deseado, Argentina",OWNO,////,////,????,"Puerto Deseado, Santa Cruz, Argentina",OK
    PUE,////,////,MPOA,UNK,1.0,Puerto Obaldia,"Puerto Obaldia, Panama",OWNO,////,////,Puerto Obaldia Airport,"Puerto Obaldia, Kuna Yala (Guna Yala), Panamá",OK
    PUF,////,////,LFBP,UNK,0.615,Uzein,"Pau, France",OWNO,////,////,????,"Pau Pyrénées, Aquitaine, France",MAYBE
    PUG,////,////,YPAG,UNK,1.0,Port Augusta,"Port Augusta, Australia",OWNO,////,////,????,"Port Augusta, South Australia, Australia",OK
    PUI,////,////,////,UNK,1.0,Pureni,"Pureni, Papua New Guinea",OWNO,////,////,????,"Pureni, Southern Highlands, Papua-New Guinea",OK
    PUJ,////,////,MDPC,UNK,0.842,Punta Cana,"Punta Cana, Dominican Republic",OWNO,////,////,Punta Cana International Airport,"Punta Cana, La Altagracia, Dominican Republic",OK
    PUK,////,////,NTGQ,UNK,1.0,Pukarua,"Pukarua, French Polynesia",OWNO,////,////,????,"Pukarua, French Polynesia",OK
    PUL,83Q,////,////,OK,0.332,Poulsbo,"Poulsbo (WA), USA",OWNO,PORT OF POULSBO MARINA MOORAGE,"POULSBO, WA - UNITED STATES",Port of Poulsbo Marina Moorage SPB,"Poulsbo, Washington, United States",OK
    PUM,////,////,WAWA,UNK,0.417,Pomala,"Pomala, Indonesia",OWNO,////,////,Sangia Nibandera Airport,"Pomala, Kolaka, Sulawesi Tenggara, Indonesia",OK
    PUN,////,////,FZOP,UNK,1.0,Punia,"Punia, Congo (DRC)",OWNO,////,////,Punia Airport,"Punia, Maniema, Democratic Republic of Congo (Zaire)",OK
    PUP,////,////,DFCP,UNK,1.0,Po,"Po, Burkina Faso",OWNO,////,////,????,"Po, Burkina Faso",OK
    PUQ,////,////,SCCI,UNK,0.642,Pres Ibanez,"Punta Arenas, Chile",OWNO,////,////,Presidente Carlos Ibáñez del Campo International Airport,"Punta Arenas, Magallanes y de la Antártica Chilena, Chile",OK
    PUR,////,////,SLPR,UNK,1.0,Puerto Rico,"Puerto Rico, Bolivia",OWNO,////,////,Puerto Rico Airport,"Puerto Rico, Manuripi, Pando, Bolivia",OK
    PUS,////,////,RKPK,UNK,0.833,Kimhae,"Pusan, South Korea",OWNO,////,////,Gimhae,"Busan, Republic of Korea (South Korea)",TO DO CHECK
    PUT,////,////,VOPN,UNK,0.743,Puttaprathe,"Puttaparthi, India",OWNO,////,////,Sri Sathya Sai,"Puttaparthi, Andhra Pradesh, India",OK
    PUU,////,////,SKAS,UNK,0.667,Puerto Asis,"Puerto Asis, Colombia",OWNO,////,////,Tres de Mayo Airport,"Puerto Asís, Putumayo, Colombia",OK
    PUV,////,////,NWWP,UNK,1.0,Poum,"Poum, New Caledonia",OWNO,////,////,????,"Poum, New Caledonia",OK
    PUW,PUW,KPUW,KPUW,OK,1.0,Moscow Regional,"Pullman (WA), USA",OWNO,PULLMAN/MOSCOW RGNL,"PULLMAN/MOSCOW, WA - UNITED STATES",Pullman/Moscow Regional,"Pullman/Moscow, Washington, United States",OK
    PUX,////,////,SCPV,UNK,0.686,Puerto Varas,"Puerto Varas, Chile",OWNO,////,////,El Mirador Airport,"Puerto Varas, Los Lagos, Chile",OK
    PUY,////,////,LDPL,UNK,1.0,Pula,"Pula, Croatia",OWNO,////,////,????,"Pula, Croatia",OK
    PUZ,////,////,MNPC,UNK,1.0,Puerto Cabezas,"Puerto Cabezas, Nicaragua",OWNO,////,////,????,"Puerto Cabezas, Atlántico Norte, Nicaragua",OK
    PVA,////,////,SKPV,UNK,0.667,Providencia,"Providencia, Colombia",OWNO,////,////,El Embrujo,"Providencia, Providencia Island, San Andrés y Providencia, Colombia",OK
    PVC,PVC,KPVC,KPVC,OK,0.834,Provincetown,"Provincetown (MA), USA",OWNO,PROVINCETOWN MUNI,"PROVINCETOWN, MA - UNITED STATES",Provincetown Municipal Airport,"Provincetown, Massachusetts, United States",OK
    PVD,PVD,KPVD,KPVD,OK,0.769,Theodore Francis,"Providence (RI), USA",OWNO,THEODORE FRANCIS GREEN STATE,"PROVIDENCE, RI - UNITED STATES",Theodore Francis Green State Airport,"Providence, Rhode Island, United States",OK
    PVE,////,////,MPVR,UNK,1.0,El Porvenir,"El Porvenir, Panama",OWNO,////,////,????,"El Porvenir, Kuna Yala (Guna Yala), Panamá",OK
    PVF,PVF,KPVF,KPVF,OK,1.0,Placerville,"Placerville (CA), USA",OWNO,PLACERVILLE,"PLACERVILLE, CA - UNITED STATES",????,"Placerville, California, United States",OK
    PVG,////,////,ZSPD,UNK,0.722,Pu Dong,"Shanghai, PR China",OWNO,////,////,Pudong International Airport,"Shanghai, Shanghai, China",OK
    PVH,////,////,SBPV,UNK,0.408,Porto Velho International,"Porto Velho, Brazil",OWNO,////,////,Governador Jorge Teixeira de Oliveira,"Porto Velho, Rondônia, Brazil",OK
    PVI,////,////,SSPI,UNK,1.0,Paranavai,"Paranavai, Brazil",OWNO,////,////,????,"Paranavai, Paraná, Brazil",OK
    PVK,////,////,LGPZ,UNK,0.786,Aktion,"Preveza/Lefkas, Greece",OWNO,////,////,Aktion National,"Preveza/Lefkada, Dytikí Elláda (Western Greece), Greece",OK
    PVO,////,////,SEPV,UNK,0.533,Portoviejo,"Portoviejo, Ecuador",OWNO,////,////,Reales Tamarindos,"Portoviejo, Manabí, Ecuador",OK
    PVR,////,////,MMPR,UNK,0.592,Ordaz,"Puerto Vallarta, Mexico",OWNO,////,////,Licenciado Gustavo Díaz Ordaz International Airport,"Puerto Vallarta, Jalisco, México",OK
    PVS,////,////,UHMD,UNK,0.9,Provideniya,"Provideniya, Russia",OWNO,////,////,Provideniya Bay Airport,"Provideniya Bay, Chukotskiy, Russian Federation (Russia)",MAYBE
    PVU,PVU,KPVU,KPVU,OK,0.64,Provo,"Provo (UT), USA",OWNO,PROVO MUNI,"PROVO, UT - UNITED STATES",Provo Municipal Airport,"Provo, Utah, United States",OK
    PVW,PVW,KPVW,KPVW,OK,1.0,Hale County,"Plainview (TX), USA",OWNO,HALE COUNTY,"PLAINVIEW, TX - UNITED STATES",Hale County Airport,"Plainview, Texas, United States",OK
    PWA,PWA,KPWA,KPWA,OK,1.0,Wiley Post,"Oklahoma City (OK), USA",OWNO,WILEY POST,"OKLAHOMA CITY, OK - UNITED STATES",Wiley Post Airport,"Oklahoma City, Oklahoma, United States",OK
    PWD,PWD,KPWD,KPWD,OK,0.766,herwood,"Plentywood (MT), USA",OWNO,SHER-WOOD,"PLENTYWOOD, MT - UNITED STATES",Sher-Wood Airport,"Plentywood, Montana, United States",OK
    PWE,////,////,UHMP,UNK,1.0,Pevek,"Pevek, Russia",OWNO,////,////,Pevek Airport,"Pevek, Magadanskaya, Russian Federation (Russia)",OK
    PWI,////,////,////,UNK,1.0,Beles,"Pawi, Ethiopia",OWNO,////,////,Beles Airport,"Pawi, Amara, Ethiopia",OK
    PWK,PWK,KPWK,KPWK,OK,0.378,Pal-Waukee,"Chicago (IL), USA",OWNO,CHICAGO EXECUTIVE,"CHICAGO/PROSPECT HEIGHTS/WHEELING, IL - UNITED STATES",Chicago Executive Airport,"Chicago/Prospect Heights/Wheeling, Illinois, United States",OK
    PWL,////,////,////,UNK,1.0,Purwokerto,"Purwokerto, Indonesia",OWNO,////,////,????,"Purwokerto, Jawa Tengah, Indonesia",OK
    PWM,PWM,KPWM,KPWM,OK,1.0,International Jetport,"Portland (ME), USA",OWNO,PORTLAND INTL JETPORT,"PORTLAND, ME - UNITED STATES",Portland International Jetport,"Portland, Maine, United States",OK
    PWN,////,////,MYCP,UNK,1.0,Pitts Town,"Pitts Town, Bahamas",OWNO,////,////,????,"Pitts Town, Crooked Island, Bahamas",OK
    PWO,////,////,FZQC,UNK,1.0,Pweto,"Pweto, Congo (DRC)",OWNO,////,////,????,"Pweto, Katanga, Democratic Republic of Congo (Zaire)",OK
    PWQ,////,////,UASP,UNK,1.0,Pavlodar,"Pavlodar, Kazakhstan",OWNO,////,////,Pavlodar Airport,"Pavlodar, Pavlodar, Kazakhstan",OK
    PWR,PWR,////,////,OK,0.835,Port Walter,"Port Walter (AK), USA",OWNO,PORT WALTER,"PORT WALTER, AK - UNITED STATES",Port Walter SPB,"Port Walter, Alaska, United States",OK
    PWT,PWT,KPWT,KPWT,OK,0.734,Bremerton,"Bremerton (WA), USA",OWNO,BREMERTON NATIONAL,"BREMERTON, WA - UNITED STATES",Bremerton National Airport,"Bremerton, Washington, United States",OK
    PXH,////,////,YPMH,UNK,nan,////,////,////,////,////,????,"Prominent Hill, South Australia, Australia",UNK
    PXL,P10,////,////,OK,1.0,Polacca,"Polacca (AZ), USA",OWNO,POLACCA,"POLACCA, AZ - UNITED STATES",????,"Polacca, Arizona, United States",OK
    PXM,////,////,MMPS,UNK,0.897,Puerto Escondido,"Puerto Escondido, Mexico",OWNO,////,////,Puerto Escondido International Airport,"Puerto Escondido, Oaxaca, México",OK
    PXO,////,////,LPPS,UNK,1.0,Porto Santo,"Porto Santo, Portugal",OWNO,////,////,????,"Porto Santo, Região Autónoma da Madeira (Madeira), Portugal",OK
    PXR,////,////,VTUJ,UNK,nan,////,////,////,////,////,????,"Surin, Surin, Thailand",UNK
    PXU,////,////,VVPK,UNK,1.0,Pleiku,"Pleiku, Viet Nam",OWNO,////,////,????,"Pleiku, Vietnam",OK
    PYA,////,////,SKVL,UNK,0.722,Puerto Boyaca,"Puerto Boyaca, Colombia",OWNO,////,////,Velasquez Airport,"Puerto Boyacá, Boyacá, Colombia",OK
    PYB,////,////,VEJP,UNK,1.0,Jeypore,"Jeypore, India",OWNO,////,////,????,"Jeypore, Odisha, India",OK
    PYC,////,////,////,UNK,1.0,Playon Chico,"Playon Chico, Panama",OWNO,////,////,????,"Playón Chico, Kuna Yala (Guna Yala), Panamá",OK
    PYE,////,////,NCPY,UNK,1.0,Penrhyn Island,"Penrhyn Island, Cook Islands",OWNO,////,////,????,"Penrhyn Island, Cook Islands",OK
    PYH,////,////,SVPA,UNK,0.652,Puerto Ayacucho,"Puerto Ayacucho, Venezuela",OWNO,////,////,Casique Aramare,"Puerto Ayacucho, Amazonas, Venezuela",OK
    PYJ,////,////,UERP,UNK,0.941,Polyarnyj,"Polyarnyj, Russia",OWNO,////,////,Polyarny Airport,"Udachny, Sakha (Yakutiya), Russian Federation (Russia)",TO DO CHECK
    PYL,PYL,////,////,OK,0.819,Perry SPB,"Perry Island (AK), USA",OWNO,PERRY ISLAND,"PERRY ISLAND, AK - UNITED STATES",Perry Island SPB,"Perry Island, Alaska, United States",OK
    PYM,PYM,KPYM,KPYM,OK,0.806,Plymouth,"Plymouth (MA), USA",OWNO,PLYMOUTH MUNI,"PLYMOUTH, MA - UNITED STATES",Plymouth Municipal Airport,"Plymouth, Massachusetts, United States",OK
    PYN,////,////,////,UNK,1.0,Payan,"Payan, Colombia",OWNO,////,////,????,"Payan, Nariño, Colombia",OK
    PYO,////,////,SEPT,UNK,1.0,Putumayo,"Putumayo, Ecuador",OWNO,////,////,????,"Putumayo, Sucumbíos, Ecuador",OK
    PYR,////,////,LGAD,UNK,1.0,Andravida,"Pyrgos, Greece",OWNO,////,////,Andravida,"Pyrgos, Dytikí Elláda (Western Greece), Greece",OK
    PYV,////,////,////,UNK,1.0,Yaviza,"Yaviza, Panama",OWNO,////,////,????,"Yaviza, Darién, Panamá",OK
    PYX,////,////,VTBF,UNK,1.0,Pattaya,"Pattaya, Thailand",OWNO,////,////,????,"Pattaya, Chon Buri, Thailand",OK
    PYY,////,////,VTCI,UNK,1.0,Pai,Pai,IATA,////,////,Pai Airport,"Pai, Mae Hong Son, Thailand",MAYBE
    PZA,////,////,SKPZ,UNK,1.0,Casanare,"Paz De Ariporo, Colombia",OWNO,////,////,Paz de Ariporo Airport,"Paz de Ariporo, Casanare, Colombia",OK
    PZB,////,////,FAPM,UNK,1.0,Pietermaritzburg,"Pietermaritzburg, South Africa",OWNO,////,////,Pietermaritzburg Airport,"Pietermaritzburg, KwaZulu-Natal, South Africa",OK
    PZE,////,////,EGHK,UNK,0.64,Penzance,"Penzance, United Kingdom",OWNO,////,////,Penzance Heliport,"Penzance, Cornwall, England, United Kingdom",OK
    PZH,////,////,OPZB,UNK,1.0,Zhob,"Zhob, Pakistan",OWNO,////,////,Zhob Airport,"Zhob, Balochistan, Pakistan",OK
    PZI,////,////,ZUZH,UNK,nan,////,////,////,////,////,????,"Panzhihua, Sichuan, China",UNK
    PZL,////,////,FADQ,UNK,1.0,Zulu Inyala,"Phinda, South Africa",OWNO,////,////,Zulu Inyala,"Phinda, KwaZulu-Natal, South Africa",OK
    PZO,////,////,SVPR,UNK,0.407,Puerto Ordaz,"Puerto Ordaz, Venezuela",OWNO,////,////,General Manuel Carlos Piar Guayana Airport,"Puerto Ordaz, Bolívar, Venezuela",OK
    PZS,////,////,SCTC,UNK,nan,////,////,////,////,////,Maquehue Airport,"Temuco, Araucanía, Chile",UNK
    PZU,////,////,HSPN,UNK,1.0,Port Sudan New International Airport,"Port Sudan, Sudan",OWNO,////,////,New International Airport,"Port Sudan, Red Sea, Sudan",OK
    PZY,////,////,LZPP,UNK,1.0,Piestany,"Piestany, Slovakia",OWNO,////,////,????,"Piestany, Slovakia",OK


List for checking certainty of Great Circle Mapper (FNJ - FYV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    FNJ,////,////,ZKPY,UNK,0.857,Sunan,"Pyongyang, Korea, DPR",OWNO,////,////,Sunan International Airport,"Pyongyang, Pyongyang, Democratic People's Republic of Korea (North Korea)",OK
    FNL,FNL,KFNL,KFNL,OK,1.0,Municipal Airport,"Fort Collins/Loveland (CO), USA",OWNO,FORT COLLINS-LOVELAND MUNI,"FORT COLLINS/LOVELAND, CO - UNITED STATES",Fort Collins-Loveland Municipal Airport,"Fort Collins/Loveland, Colorado, United States",OK
    FNR,FNR,PANR,PANR,OK,0.815,SPB,"Funter Bay (AK), USA",OWNO,FUNTER BAY,"FUNTER BAY, AK - UNITED STATES",Funter Bay SPB,"Funter Bay, Alaska, United States",OK
    FNT,FNT,KFNT,KFNT,OK,0.859,Bishop,"Flint (MI), USA",OWNO,BISHOP INTL,"FLINT, MI - UNITED STATES",Bishop International Airport,"Flint, Michigan, United States",OK
    FOA,////,////,////,UNK,1.0,Foula,"Foula, United Kingdom",OWNO,////,////,????,"Foula, Shetland Islands, Scotland, United Kingdom",OK
    FOB,82CL,////,////,OK,1.0,Fort Bragg,"Fort Bragg (CA), USA",OWNO,FORT BRAGG,"FORT BRAGG, CA - UNITED STATES",????,"Fort Bragg, California, United States",OK
    FOC,////,////,ZSFZ,UNK,0.765,Fuzhou,"Fuzhou, PR China",OWNO,////,////,Changle Airport,"Fuzhou, Fujian, China",OK
    FOD,FOD,KFOD,KFOD,OK,0.734,Fort Dodge,"Fort Dodge IA, USA",OWNO,FORT DODGE RGNL,"FORT DODGE, IA - UNITED STATES",Fort Dodge Regional,"Fort Dodge, Iowa, United States",OK
    FOE,FOE,KFOE,KFOE,OK,0.396,Forbes AFB,"Topeka (KS), USA",OWNO,TOPEKA RGNL,"TOPEKA, KS - UNITED STATES",Topeka Regional,"Topeka, Kansas, United States",OK
    FOG,////,////,LIBF,UNK,1.0,Gino Lisa,"Foggia, Italy",OWNO,////,////,Gino Lisa Airport,"Foggia, Apulia, Italy",OK
    FOK,FOK,KFOK,KFOK,OK,0.441,Suffolk County,"Westhampton (NY), USA",OWNO,FRANCIS S GABRESKI,"WESTHAMPTON BEACH, NY - UNITED STATES",Francis S Gabreski Airport,"Westhampton Beach, New York, United States",OK
    FOM,////,////,FKKM,UNK,0.75,Foumban,"Foumban, Cameroon",OWNO,////,////,Foumban Nkounja Airport,"Foumban, West (Ouest), Cameroon",OK
    FON,////,////,MRAN,UNK,0.667,Fortuna,"Fortuna, Costa Rica",OWNO,////,////,Arenal,"La Fortuna, Alajuela, Costa Rica",MAYBE
    FOO,////,////,////,UNK,0.571,Numfoor,"Numfoor, Indonesia",OWNO,////,////,Kornasoren,"Numfoor, Irian Jaya Island, Papua, Indonesia",OK
    FOR,////,////,SBFZ,UNK,0.93,Pinto Martins Airport,"Fortaleza, Brazil",OWNO,////,////,Pinto Martins International Airport,"Fortaleza, Ceará, Brazil",OK
    FOS,////,////,YFRT,UNK,1.0,Forrest,"Forrest, Australia",OWNO,////,////,????,"Forrest, Western Australia, Australia",OK
    FOT,////,////,YFST,UNK,1.0,Forster,"Forster, Australia",OWNO,////,////,????,"Forster, New South Wales, Australia",OK
    FOU,////,////,FOGF,UNK,1.0,Fougamou,"Fougamou, Gabon",OWNO,////,////,Fougamou Airport,"Fougamou, Ngounié, Gabon",OK
    FOY,////,////,////,UNK,1.0,Foya,"Foya, Liberia",OWNO,////,////,????,"Foya, Liberia",OK
    FPO,////,MYGF,MYGF,OK,0.962,Grand Bahama International,"Freeport, Bahamas",OWNO,GRAND BAHAMA INTL,"FREEPORT, - BAHAMAS",Grand Bahamas International Airport,"Freeport, Grand Bahama, Bahamas",OK
    FPR,FPR,KFPR,KFPR,OK,0.543,St Lucie County,"Fort Pierce (FL), USA",OWNO,TREASURE COAST INTL,"FORT PIERCE, FL - UNITED STATES",Treasure Coast International Airport,"Fort Pierce, Florida, United States",OK
    FPY,40J,////,////,OK,1.0,Perry-Foley,"Perry (FL), USA",OWNO,PERRY-FOLEY,"PERRY, FL - UNITED STATES",Perry-Foley Airport,"Perry, Florida, United States",OK
    FRA,////,////,EDDF,UNK,0.786,International,"Frankfurt/Main, Germany",OWNO,////,////,Rhein-Main,"Frankfurt, Hessen, Germany",OK
    FRB,////,////,YFBS,UNK,1.0,Forbes,"Forbes, Australia",OWNO,////,////,????,"Forbes, New South Wales, Australia",OK
    FRC,////,////,SIMK,UNK,1.0,Franca,"Franca, Brazil",OWNO,////,////,????,"Franca, São Paulo, Brazil",OK
    FRD,FHR,KFHR,KFHR,OK,1.0,Friday Harbor,"Friday Harbor, USA (WA)",OWNO,FRIDAY HARBOR,"FRIDAY HARBOR, WA - UNITED STATES",????,"Friday Harbor, Washington, United States",OK
    FRE,////,////,AGGF,UNK,1.0,Fera Island,"Fera Island, Solomon Islands",OWNO,////,////,????,"Fera/Maringe, Fera Island, Santa Isabel Islands, Solomon Islands",OK
    FRG,FRG,KFRG,KFRG,OK,0.852,Republic Field,"Farmingdale (NY), USA",OWNO,REPUBLIC,"FARMINGDALE, NY - UNITED STATES",Republic Airport,"Farmingdale, New York, United States",OK
    FRH,FRH,KFRH,KFRH,OK,1.0,Municipal,"French Lick (IN), USA",OWNO,FRENCH LICK MUNI,"FRENCH LICK, IN - UNITED STATES",French Lick Municipal Airport,"French Lick, Indiana, United States",OK
    FRI,FRI,KFRI,KFRI,OK,1.0,Marshall AAF,"Fort Riley (KS), USA",OWNO,MARSHALL AAF,"FORT RILEY(JUNCTION CITY), KS - UNITED STATES",Marshall AAF Airport,"Fort Riley, Kansas, United States",OK
    FRK,////,////,FSSF,UNK,1.0,Fregate Island,"Fregate Island, Seychelles",OWNO,////,////,????,"Frégate Island, Seychelles",OK
    FRL,////,////,LIPK,UNK,0.462,Luigi Ridolfi,"Forli, Italy",OWNO,////,////,Forlì Airport,"Forlì, Emilia-Romagna, Italy",OK
    FRM,FRM,KFRM,KFRM,OK,0.771,Fairmont,"Fairmont (MN), USA",OWNO,FAIRMONT MUNI,"FAIRMONT, MN - UNITED STATES",Fairmont Municipal Airport,"Fairmont, Minnesota, United States",OK
    FRN,FRN,PAFR,PAFR,OK,1.0,Bryant AAF,"Fort Richardson (AK), USA",OWNO,BRYANT AAF,"FORT RICHARDSON(ANCHORAGE), AK - UNITED STATES",Bryant AAF Airport,"Fort Richardson, Alaska, United States",OK
    FRO,////,////,ENFL,UNK,1.0,Flora,"Floro, Norway",OWNO,////,////,????,"Florø, Sogn og Fjordane, Norway",OK
    FRQ,////,////,////,UNK,1.0,Feramin,"Feramin, Papua New Guinea",OWNO,////,////,????,"Feramin, Western, Papua-New Guinea",OK
    FRR,FRR,KFRR,KFRR,OK,1.0,Warren County,"Front Royal (VA), USA",OWNO,FRONT ROYAL-WARREN COUNTY,"FRONT ROYAL, VA - UNITED STATES",Front Royal-Warren County Airport,"Front Royal, Virginia, United States",OK
    FRS,////,////,MGMM,UNK,0.581,Santa Elena,"Flores, Guatemala",OWNO,////,////,Mundo Maya International Airport,"Flores, Petén, Guatemala",OK
    FRT,////,////,SCEV,UNK,0.667,Frutillar,"Frutillar, Chile",OWNO,////,////,El Avellano Airport,"Frutillar, Los Lagos, Chile",OK
    FRU,////,////,UCFM,UNK,1.0,Manas International,"Bishkek, Kyrgyzstan",OWNO,////,////,Manas International Airport,"Bishkek, Chuy (Chü), Kyrgyzstan",OK
    FRW,////,////,FBFT,UNK,0.833,Francistown,"Francistown, Botswana",OWNO,////,////,Francistown International Airport,"Francistown, North-East, Botswana",OK
    FRY,IZG,KIZG,KIZG,OK,0.34,Fryeburg,"Fryeburg (ME), USA",OWNO,EASTERN SLOPES RGNL,"FRYEBURG, ME - UNITED STATES",Eastern Slopes Regional,"Fryeburg, Maine, United States",OK
    FRZ,////,////,ETHF,UNK,0.833,Fritzlar Airbase,"Fritzlar, Germany",OWNO,////,////,Fritzlar AB,"Fritzlar, Hessen, Germany",OK
    FSC,////,////,LFKF,UNK,1.0,Sud Corse,"Figari, France",OWNO,////,////,Sud Corse,"Figari, Corse (Corsica), France",OK
    FSD,FSD,KFSD,KFSD,OK,1.0,Regional(Jo Foss Fld),"Sioux Falls (SD), USA",OWNO,JOE FOSS FIELD,"SIOUX FALLS, SD - UNITED STATES",Joe Foss Field,"Sioux Falls, South Dakota, United States",OK
    FSI,FSI,KFSI,KFSI,OK,1.0,Henry Post AAF,"Fort Sill (OK), USA",OWNO,HENRY POST AAF (FORT SILL),"FORT SILL, OK - UNITED STATES",Henry Post AAF Airport,"Fort Sill, Oklahoma, United States",OK
    FSK,FSK,KFSK,KFSK,OK,1.0,Municipal,"Fort Scott (KS), USA",OWNO,FORT SCOTT MUNI,"FORT SCOTT, KS - UNITED STATES",Fort Scott Municipal Airport,"Fort Scott, Kansas, United States",OK
    FSL,////,////,////,UNK,1.0,Fossil Downs,"Fossil Downs, Australia",OWNO,////,////,????,"Fossil Downs, Western Australia, Australia",OK
    FSM,FSM,KFSM,KFSM,OK,0.766,Municipal,"Fort Smith (AR), USA",OWNO,FORT SMITH RGNL,"FORT SMITH, AR - UNITED STATES",Fort Smith Regional,"Fort Smith, Arkansas, United States",OK
    FSP,////,////,LFVP,UNK,1.0,St Pierre,"St Pierre, St. Pierre and Miquelon",OWNO,////,////,????,"St-Pierre, St. Pierre and Miquelon",OK
    FSS,////,////,EGQK,UNK,0.875,Kinloss,"Forres, United Kingdom",OWNO,////,////,RAF Kinloss,"Forres, Morayshire, Scotland, United Kingdom",OK
    FST,FST,KFST,KFST,OK,1.0,Pecos County,"Fort Stockton (TX), USA",OWNO,FORT STOCKTON-PECOS COUNTY,"FORT STOCKTON, TX - UNITED STATES",Fort Stockton-Pecos County Airport,"Fort Stockton, Texas, United States",OK
    FSU,FSU,KFSU,KFSU,OK,0.669,Fort Sumner,"Fort Sumner (NM), USA",OWNO,FORT SUMNER MUNI,"FORT SUMNER, NM - UNITED STATES",Fort Sumner Municipal Airport,"Fort Sumner, New Mexico, United States",OK
    FSZ,////,////,RJNS,UNK,nan,////,////,////,////,////,Mt. Fuji Shizuoka Airport,"Shizuoka, Shizuoka, Japan",UNK
    FTA,////,////,NVVF,UNK,1.0,Futuna Airport,"Futuna Island, Vanuatu",OWNO,////,////,Futuna Airport,"Futuna Island, Taféa, Vanuatu",OK
    FTE,////,////,SAWC,UNK,0.431,El Calafate,"El Calafate, Argentina",OWNO,////,////,Comandante Armando Tola International Airport,"El Calafate, Santa Cruz, Argentina",OK
    FTI,FAQ,NSFQ,NSFQ,OK,0.636,Fitiuta,"Fitiuta, American Samoa",OWNO,FITIUTA,"FITIUTA VILLAGE, AS - UNITED STATES",????,"Fitiuta Village, American Samoa",MAYBE
    FTK,FTK,KFTK,KFTK,OK,1.0,Godman AAF,"Fort Knox (KY), USA",OWNO,GODMAN AAF,"FORT KNOX, KY - UNITED STATES",Godman AAF Airport,"Fort Knox, Kentucky, United States",OK
    FTU,////,////,FMSD,UNK,1.0,Marillac,"Fort Dauphin, Madagascar",OWNO,////,////,Maurillac Airport,"Tolagnaro, Madagascar",OK
    FTW,FTW,KFTW,KFTW,OK,0.605,Meacham Field,"Fort Worth/Dallas (TX), USA",OWNO,FORT WORTH MEACHAM INTL,"FORT WORTH, TX - UNITED STATES",Fort Worth Meacham International Airport,"Fort Worth, Texas, United States",OK
    FTX,////,////,FCOO,UNK,1.0,Owando,"Owando, Congo (ROC)",OWNO,////,////,????,"Owando, Congo (Republic of)",OK
    FTY,FTY,KFTY,KFTY,OK,0.716,Fulton County,"Atlanta (GA), USA",OWNO,FULTON COUNTY AIRPORT-BROWN FIELD,"ATLANTA, GA - UNITED STATES",Fulton County Airport-Brown Field,"Atlanta, Georgia, United States",OK
    FUB,////,////,////,UNK,1.0,Fulleborn,"Fulleborn, Papua New Guinea",OWNO,////,////,????,"Fulleborn, West New Britain, Papua-New Guinea",OK
    FUE,////,////,GCFV,UNK,1.0,Fuerteventura,"Fuerteventura, Canary Islands, Spain",OWNO,////,////,Fuerteventura,"Puerto del Rosario, Fuerteventura Island, Canary Islands, Spain",OK
    FUG,////,////,ZSFY,UNK,1.0,Fuyang,"Fuyang, PR China",OWNO,////,////,????,"Fuyang, Anhui, China",OK
    FUJ,////,////,RJFE,UNK,1.0,Fukue,"Fukue, Japan",OWNO,////,////,Fukue Airport,"Goto, Fukue Island, Nagasaki, Japan",OK
    FUK,////,////,RJFF,UNK,1.0,Fukuoka Airport,"Fukuoka, Japan",OWNO,////,////,Fukuoka Airport,"Fukuoka, Fukuoka, Japan",OK
    FUL,FUL,KFUL,KFUL,OK,1.0,Municipal,"Fullerton (CA), USA",OWNO,FULLERTON MUNI,"FULLERTON, CA - UNITED STATES",Fullerton Municipal Airport,"Fullerton, California, United States",OK
    FUM,////,////,////,UNK,1.0,Fuma,"Fuma, Papua New Guinea",OWNO,////,////,????,"Fuma, Western, Papua-New Guinea",OK
    FUN,////,////,NGFU,UNK,1.0,International,"Funafuti Atol, Tuvalu",OWNO,////,////,Funafuti Atoll International Airport,"Funafuti Atoll, Tuvalu",OK
    FUO,////,////,ZGFS,UNK,1.0,Fuoshan,"Fuoshan, PR China",OWNO,////,////,????,"Foshan, Guangdong, China",OK
    FUT,////,////,NLWF,UNK,0.69,Futuna Island,"Futuna Island, Wallis and Futuna",OWNO,////,////,Pointe Vele,"Futuna Island, Wallis and Futuna Islands",OK
    FVL,////,////,YFLO,UNK,1.0,Flora Valey,"Flora Valley, Australia",OWNO,////,////,Flora Valley Airport,"Ord River, Western Australia, Australia",OK
    FVM,////,////,VRMR,UNK,nan,////,////,////,////,////,Fuvahmulah Airport,"Fuvahmulah, Gnaviyani Atoll, Maldives",UNK
    FVR,////,////,////,UNK,0.815,Forrest River Airport,"Forrest River, Australia",OWNO,////,////,Forrest River Mission Airport,"Oombulgurri, Western Australia, Australia",MAYBE
    FWA,FWA,KFWA,KFWA,OK,0.757,Municipal/Baer Field,"Fort Wayne (IN), USA",OWNO,FORT WAYNE INTL,"FORT WAYNE, IN - UNITED STATES",Fort Wayne International Airport,"Fort Wayne, Indiana, United States",OK
    FWH,NFW,KNFW,KNFW,OK,0.459,Carswell AFB,"Fort Worth/Dallas (TX), USA",OWNO,FORT WORTH NAS JRB (CARSWELL FLD),"FORT WORTH, TX - UNITED STATES",Fort Worth NAS JRB Airport,"Fort Worth, Texas, United States",OK
    FWL,0AA4,////,PAFW,NG,1.0,Farewell,"Farewell (AK), USA",OWNO,FAREWELL,"FAREWELL, AK - UNITED STATES",????,"Farewell, Alaska, United States",OK
    FWM,////,////,////,UNK,1.0,Heliport,"Fort William, United Kingdom",OWNO,////,////,Heliport,"Fort William, Scotland, United Kingdom",OK
    FXE,FXE,KFXE,KFXE,OK,1.0,Executive,"Fort Lauderdale (FL), USA",OWNO,FORT LAUDERDALE EXECUTIVE,"FORT LAUDERDALE, FL - UNITED STATES",Fort Lauderdale Executive Airport,"Fort Lauderdale, Florida, United States",OK
    FXO,////,////,FQCB,UNK,1.0,Cuamba,"Cuamba, Mozambique",OWNO,////,////,????,"Cuamba, Mozambique",OK
    FXY,FXY,KFXY,KFXY,OK,1.0,Municipal,"Forest City IA, USA",OWNO,FOREST CITY MUNI,"FOREST CITY, IA - UNITED STATES",Forest City Municipal Airport,"Forest City, Iowa, United States",OK
    FYJ,////,////,ZYFY,UNK,nan,////,////,////,////,////,Fuyuan Dongji Airport,"Fuyuan, Heilongjiang, China",UNK
    FYM,FYM,KFYM,KFYM,OK,1.0,Municipal,"Fayetteville (TN), USA",OWNO,FAYETTEVILLE MUNI,"FAYETTEVILLE, TN - UNITED STATES",Fayetteville Municipal Airport,"Fayetteville, Tennessee, United States",OK
    FYN,////,////,ZWFY,UNK,0.759,Fuyun,"Fuyun, PR China",OWNO,////,////,Fuyun Koktokay Airport,"Fuyun, Xinjiang, China",OK
    FYT,////,////,FTTY,UNK,0.5,Faya,"Faya, Chad",OWNO,////,////,????,"Faya-Largeau, Borkou, Chad",TO DO CHECK
    FYU,FYU,PFYU,PFYU,OK,1.0,Fort Yukon,"Fort Yukon (AK), USA",OWNO,FORT YUKON,"FORT YUKON, AK - UNITED STATES",????,"Fort Yukon, Alaska, United States",OK
    FYV,FYV,KFYV,KFYV,OK,1.0,Municipal (Drake Fld),"Fayetteville (AR), USA",OWNO,DRAKE FIELD,"FAYETTEVILLE, AR - UNITED STATES",Drake Field,"Fayetteville, Arkansas, United States",OK

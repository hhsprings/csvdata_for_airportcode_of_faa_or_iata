
List for checking certainty of Great Circle Mapper (IAA - ILH)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    IAA,////,////,UOII,UNK,1.0,Igarka,"Igarka, Russia",OWNO,////,////,Igarka Airport,"Igarka, Krasnoyarskiy, Russian Federation (Russia)",OK
    IAB,IAB,KIAB,KIAB,OK,0.805,Mcconnell AFB,"Wichita (KS), USA",OWNO,MC CONNELL AFB,"WICHITA, KS - UNITED STATES",Mc Connell AFB,"Wichita, Kansas, United States",OK
    IAD,IAD,KIAD,KIAD,OK,1.0,Dulles International,"Washington, DC, USA",OWNO,WASHINGTON DULLES INTL,"WASHINGTON, DC - UNITED STATES",Washington Dulles International Airport,"Washington, District of Columbia, United States",OK
    IAG,IAG,KIAG,KIAG,OK,1.0,International,"Niagara Falls (NY), USA",OWNO,NIAGARA FALLS INTL,"NIAGARA FALLS, NY - UNITED STATES",Niagara Falls International Airport,"Niagara Falls, New York, United States",OK
    IAH,IAH,KIAH,KIAH,OK,1.0,George Bush Intercontinental,"Houston (TX), USA",OWNO,GEORGE BUSH INTERCONTINENTAL/HOUSTON,"HOUSTON, TX - UNITED STATES",George Bush Intercontinental/Houston Airport,"Houston, Texas, United States",OK
    IAM,////,////,DAUZ,UNK,1.0,In Amenas,"In Amenas, Algeria",OWNO,////,////,In Aménas Airport,"In Aménas, Algeria",OK
    IAN,IAN,PAIK,PAIK,OK,1.0,Bob Barker Memorial,"Kiana (AK), USA",OWNO,BOB BAKER MEMORIAL,"KIANA, AK - UNITED STATES",Bob Baker Memorial Airport,"Kiana, Alaska, United States",OK
    IAO,////,////,RPNS,UNK,nan,////,////,////,////,////,Sayak Airport,"Del Carmen, Siargao Island, Philippines",UNK
    IAQ,////,////,////,UNK,1.0,Bahregan,"Bahregan, Iran",OWNO,////,////,????,"Bahregan, Bushehr, Iran",OK
    IAR,////,////,UUDL,UNK,0.75,Yaroslavl,"Yaroslavl, Russia",OWNO,////,////,Tunoshna Airport,"Yaroslavl, Yaroslavskaya, Russian Federation (Russia)",OK
    IAS,////,////,LRIA,UNK,0.615,Iasi,"Iasi, Romania",OWNO,////,////,Iasi International Airport,"Iasi, Romania",OK
    IAU,////,////,////,UNK,1.0,Iaura,"Iaura, Papua New Guinea",OWNO,////,////,????,"Iaura, Central, Papua-New Guinea",OK
    IBA,////,////,DNIB,UNK,1.0,Ibadan,"Ibadan, Nigeria",OWNO,////,////,Ibadan Airport,"Ibadan, Oyo, Nigeria",OK
    IBE,////,////,SKIB,UNK,0.6,Ibague,"Ibague, Colombia",OWNO,////,////,Perales Airport,"Ibagué, Tolima, Colombia",OK
    IBI,////,////,////,UNK,1.0,Iboki,"Iboki, Papua New Guinea",OWNO,////,////,????,"Iboki, West New Britain, Papua-New Guinea",OK
    IBL,////,////,////,UNK,1.0,Indigo Bay Lodge,Indigo Bay Lodge,IATA,////,////,Indigo Bay Lodge,"Indigo Bay, Bazaruto Island, Bazaruto Archipelago, Mozambique",MAYBE
    IBO,////,////,////,UNK,1.0,Ibo,"Ibo, Mozambique",OWNO,////,////,????,"Ibo, Mozambique",OK
    IBP,////,////,SPBR,UNK,1.0,Iberia,"Iberia, Peru",OWNO,////,////,Iberia Airport,"Iberia, Madre de Dios, Perú",OK
    IBR,////,////,RJAH,UNK,nan,////,////,////,////,////,Ibaraki Airport,"Omitama, Ibaraki, Japan",UNK
    IBZ,////,////,LEIB,UNK,0.938,Ibiza,"Ibiza, Balearic Islands, Spain",OWNO,////,////,????,"Eivissa, Eivissa, Balearic Islands, Spain",OK
    ICA,////,////,SVIC,UNK,1.0,Icabaru,"Icabaru, Venezuela",OWNO,////,////,????,"Icabaru, Bolívar, Venezuela",OK
    ICI,////,////,NFCI,UNK,1.0,Cicia,"Cicia, Fiji",OWNO,////,////,????,"Cicia, Fiji",OK
    ICK,////,////,SMNI,UNK,1.0,Nieuw Nickerie,"Nieuw Nickerie, Suriname",OWNO,////,////,????,"Nieuw Nickerie, Nickerie, Suriname",OK
    ICL,ICL,KICL,KICL,OK,0.549,Municipal,"Clarinda IA, USA",OWNO,SCHENCK FIELD,"CLARINDA, IA - UNITED STATES",Schenck Field,"Clarinda, Iowa, United States",OK
    ICN,////,////,RKSI,UNK,1.0,Incheon International Airport,"Seoul, South Korea",OWNO,////,////,Incheon International Airport,"Seoul, Republic of Korea (South Korea)",OK
    ICO,////,////,////,UNK,1.0,Sicogon Island,"Sicogon Island, Philippines",OWNO,////,////,????,"Sicogon Island, Philippines",OK
    ICT,ICT,KICT,KICT,OK,0.445,Mid-Continent,"Wichita (KS), USA",OWNO,WICHITA DWIGHT D EISENHOWER NATIONAL,"WICHITA, KS - UNITED STATES",Wichita Dwight D Eisenhower National Airport,"Wichita, Kansas, United States",OK
    ICY,19AK,////,////,OK,1.0,Icy Bay,"Icy Bay (AK), USA",OWNO,ICY BAY,"ICY BAY, AK - UNITED STATES",????,"Icy Bay, Alaska, United States",OK
    IDA,IDA,KIDA,KIDA,OK,0.288,Fanning Field,"Idaho Falls (ID), USA",OWNO,IDAHO FALLS RGNL,"IDAHO FALLS, ID - UNITED STATES",Idaho Falls Regional,"Idaho Falls, Idaho, United States",OK
    IDB,////,////,ESUE,UNK,1.0,Idre,"Idre, Sweden",OWNO,////,////,????,"Idre, Sweden",OK
    IDF,////,////,FZCB,UNK,1.0,Idiofa,"Idiofa, Congo (DRC)",OWNO,////,////,????,"Idiofa, Bandundu, Democratic Republic of Congo (Zaire)",OK
    IDG,IDG,KIDG,KIDG,OK,1.0,Municipal,"Ida Grove IA, USA",OWNO,IDA GROVE MUNI,"IDA GROVE, IA - UNITED STATES",Ida Grove Municipal Airport,"Ida Grove, Iowa, United States",OK
    IDI,IDI,KIDI,KIDI,OK,0.851,Indiana,"Indiana (PA), USA",OWNO,INDIANA COUNTY/JIMMY STEWART FLD/,"INDIANA, PA - UNITED STATES",Indiana County Airport,"Indiana, Pennsylvania, United States",OK
    IDK,////,////,YIDK,UNK,1.0,Indulkana,"Indulkana, Australia",OWNO,////,////,????,"Indulkana, South Australia, Australia",OK
    IDN,////,////,////,UNK,1.0,Indagen,"Indagen, Papua New Guinea",OWNO,////,////,????,"Indagen, Morobe, Papua-New Guinea",OK
    IDO,////,////,SWIY,UNK,1.0,Santa Isabel Do Morro,"Santa Isabel do Morro, Brazil",OWNO,////,////,????,"Santa Isabel do Morro, Tocantins, Brazil",OK
    IDP,IDP,KIDP,KIDP,OK,0.815,Independence,"Independence (KS), USA",OWNO,INDEPENDENCE MUNI,"INDEPENDENCE, KS - UNITED STATES",Independence Municipal Airport,"Independence, Kansas, United States",OK
    IDR,////,////,VAID,UNK,0.308,Indore,"Indore, India",OWNO,////,////,Devi Ahilyabai Holkar International Airport,"Indore, Madhya Pradesh, India",OK
    IDY,////,////,LFEY,UNK,1.0,Ile d'Yeu,"Ile d'Yeu, France",OWNO,////,////,????,"Île d'Yeu, Pays de la Loire, France",OK
    IEG,////,////,EPZG,UNK,1.0,Babimost,"Zielona Gora, Poland",OWNO,////,////,Zielona Góra-Babimost Airport,"Zielona Góra, Lubuskie, Poland",OK
    IEJ,////,////,RORE,UNK,1.0,Iejima,"Iejima, Japan",OWNO,////,////,Iejima Airport,"Iejima, Ie-Shima Island, Okinawa, Japan",OK
    IEV,////,////,UKKK,UNK,1.0,Zhulhany,"Kiev, Ukraine",OWNO,////,////,Zhulyany,"Kiev, Kiev, Ukraine",OK
    IFA,IFA,KIFA,KIFA,OK,0.64,Iowa Falls,"Iowa Falls IA, USA",OWNO,IOWA FALLS MUNI,"IOWA FALLS, IA - UNITED STATES",Iowa Falls Municipal Airport,"Iowa Falls, Iowa, United States",OK
    IFF,////,////,YIFY,UNK,1.0,Iffley,"Iffley, Australia",OWNO,////,////,????,"Iffley, Queensland, Australia",OK
    IFH,////,////,OIFE,UNK,nan,////,////,////,////,////,Hesa AB,"Isfahan, Esfahan, Iran",UNK
    IFJ,////,////,BIIS,UNK,1.0,Isafjordur,"Isafjordur, Iceland",OWNO,////,////,????,"Ísafjörður, Iceland",OK
    IFL,////,////,YIFL,UNK,1.0,Innisfail,"Innisfail, Australia",OWNO,////,////,????,"Innisfail, Queensland, Australia",OK
    IFN,////,////,OIFM,UNK,0.737,Isfahan,"Isfahan, Iran",OWNO,////,////,Isfahan International Airport,"Isfahan, Esfahan, Iran",OK
    IFO,////,////,UKLI,UNK,1.0,Ivano-Frankovsk,"Ivano-Frankovsk, Ukraine",OWNO,////,////,????,"Ivano-Frankovsk, Ivano-Frankivsk, Ukraine",OK
    IFP,IFP,KIFP,KIFP,OK,0.918,Laughlin Bullhead International,"Bullhead City (AZ), USA",OWNO,LAUGHLIN/BULLHEAD INTL,"BULLHEAD CITY, AZ - UNITED STATES",Laughlin/Bullhead International Airport,"Bullhead City, Arizona, United States",OK
    IFU,////,////,VREI,UNK,nan,////,////,////,////,////,Ifuru Airport,"Ifuru, Raa Atoll, North (Uthuru), Maldives",UNK
    IGA,////,MYIG,MYIG,OK,0.08,Inagua,"Inagua, Bahamas",OWNO,MATTHEW TOWN,"MATTHEW TOWN, - BAHAMAS",????,"Matthew Town, Great Inagua Island, Bahamas",TO DO CHECK
    IGB,////,////,SAVJ,UNK,0.7,Ingeniero Jacobacci,"Ingeniero Jacobacci, Argentina",OWNO,////,////,C Faa H R Borden,"Ingeniero Jacobacci, Río Negro, Argentina",OK
    IGD,////,////,LTCT,UNK,nan,////,////,////,////,////,Igdir Airport,"Igdir, Igdir, Turkey",UNK
    IGE,////,////,FOOI,UNK,1.0,Iguela,"Iguela, Gabon",OWNO,////,////,????,"Iguela, Ogooué-Maritime, Gabon",OK
    IGG,IGG,PAIG,PAIG,OK,1.0,Igiugig,"Igiugig (AK), USA",OWNO,IGIUGIG,"IGIUGIG, AK - UNITED STATES",????,"Igiugig, Alaska, United States",OK
    IGH,////,////,YIGM,UNK,1.0,Ingham,"Ingham, Australia",OWNO,////,////,????,"Ingham, Queensland, Australia",OK
    IGL,////,////,LTBL,UNK,1.0,Cigli Military Airport,"Izmir, Turkey",OWNO,////,////,Cigli Military,"Izmir, Izmir, Turkey",OK
    IGM,IGM,KIGM,KIGM,OK,1.0,Kingman,"Kingman (AZ), USA",OWNO,KINGMAN,"KINGMAN, AZ - UNITED STATES",????,"Kingman, Arizona, United States",OK
    IGN,////,////,RPMI,UNK,0.476,Maria Cristina Airport,"Iligan, Philippines",OWNO,////,////,????,"Iligan, Mindanao Island, Philippines",OK
    IGO,////,////,SKIG,UNK,0.483,Chigorodo,"Chigorodo, Colombia",OWNO,////,////,Jaime Ortiz Betancur Airport,"Chigorodó, Antioquia, Colombia",OK
    IGR,////,////,SARI,UNK,0.833,Cataratas,"Iguazu, Argentina",OWNO,////,////,Cataratas del Iguaçu,"Iguaçu, Misiones, Argentina",OK
    IGS,////,////,ETSI,UNK,nan,////,////,////,////,////,Manching,"Ingolstadt, Bavaria, Germany",UNK
    IGT,////,////,URMS,UNK,nan,////,////,////,////,////,Magas Airport,"Sleptsovskaya, Ingushetiya, Russian Federation (Russia)",UNK
    IGU,////,////,SBFI,UNK,0.842,Cataratas,"Iguassu Falls, Brazil",OWNO,////,////,Cataratas International Airport,"Iguaçu Falls, Paraná, Brazil",OK
    IHN,////,////,OYQN,UNK,1.0,Qishn,"Qishn, Yemen",OWNO,////,////,????,"Qishn, Yemen",OK
    IHO,////,////,FMSI,UNK,1.0,Ihosy,"Ihosy, Madagascar",OWNO,////,////,????,"Ihosy, Madagascar",OK
    IHR,////,////,OIZI,UNK,nan,////,////,////,////,////,????,"Iranshahr, Sistan va Baluchestan, Iran",UNK
    IHU,////,////,AYIH,UNK,1.0,Ihu,"Ihu, Papua New Guinea",OWNO,////,////,????,"Ihu, Gulf, Papua-New Guinea",OK
    IIA,////,////,EIMN,UNK,1.0,Inishmaan,"Inishmaan, Ireland",OWNO,////,////,????,"Inishmaan, Aran Islands, Connacht, Ireland",OK
    IIL,////,////,OICI,UNK,nan,////,////,////,////,////,????,"Ilam, Ilam, Iran",UNK
    IIS,////,////,////,UNK,1.0,Nissan Island,"Nissan Island, Papua New Guinea",OWNO,////,////,????,"Nissan Island, Bougainville, Papua-New Guinea",OK
    IJK,////,////,USII,UNK,1.0,Izhevsk,"Izhevsk, Russia",OWNO,////,////,Izhevsk Airport,"Izhevsk, Udmurtskaya, Russian Federation (Russia)",OK
    IJU,////,////,SSIJ,UNK,0.357,J.Batista Bos Filho,"Ijui, Brazil",OWNO,////,////,????,"Ijuí, Rio Grande do Sul, Brazil",OK
    IJX,IJX,KIJX,KIJX,OK,1.0,Municipal,"Jacksonville (IL), USA",OWNO,JACKSONVILLE MUNI,"JACKSONVILLE, - UNITED STATES",Jacksonville Municipal Airport,"Jacksonville, Illinois, United States",OK
    IKA,////,////,OIIE,UNK,1.0,Imam Khomeini International Airport,"Tehran, Iran",OWNO,////,////,Imam Khomeini International Airport,"Tehran, Tehran, Iran",OK
    IKB,UKF,KUKF,KUKF,OK,1.0,Wilkes County,"Wilkesboro (NC), USA",OWNO,WILKES COUNTY,"NORTH WILKESBORO, NC - UNITED STATES",Wilkes County Airport,"North Wilkesboro, North Carolina, United States",OK
    IKI,////,////,RJDB,UNK,1.0,Iki,"Iki, Japan",OWNO,////,////,Iki Airport,"Iki, Iki Island, Nagasaki, Japan",OK
    IKK,IKK,KIKK,KIKK,OK,1.0,Greater Kankakee,"Kankakee (IL), USA",OWNO,GREATER KANKAKEE,"KANKAKEE, IL - UNITED STATES",Greater Kankakee Airport,"Kankakee, Illinois, United States",OK
    IKL,////,////,FZGV,UNK,1.0,Ikela,"Ikela, Congo (DRC)",OWNO,////,////,????,"Ikela, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    IKO,IKO,PAKO,PAKO,OK,0.511,Nikolski AFS,"Nikolski (AK), USA",OWNO,NIKOLSKI AS,"NIKOLSKI, AK - UNITED STATES",Nikolski Air Station,"Nikolski, Alaska, United States",OK
    IKP,////,////,YIKM,UNK,1.0,Inkerman,"Inkerman, Australia",OWNO,////,////,????,"Inkerman, Queensland, Australia",OK
    IKS,////,////,UEST,UNK,1.0,Tiksi,"Tiksi, Russia",OWNO,////,////,Tiksi Airport,"Tiksi, Sakha (Yakutiya), Russian Federation (Russia)",OK
    IKT,////,////,UIII,UNK,0.778,Irkutsk,"Irkutsk, Russia",OWNO,////,////,Irkutsk International Airport,"Irkutsk, Irkutskaya, Russian Federation (Russia)",OK
    ILA,////,////,////,UNK,1.0,Illaga,"Illaga, Indonesia",OWNO,////,////,????,"Illaga, Papua, Indonesia",OK
    ILD,////,////,LEDA,UNK,nan,////,////,////,////,////,Lleida-Alguaire Airport,"Lleida, Catalonia, Spain",UNK
    ILE,ILE,KILE,KILE,OK,0.445,Municipal,"Killeen (TX), USA",OWNO,SKYLARK FIELD,"KILLEEN, - UNITED STATES",Skylark Field,"Killeen, Texas, United States",OK
    ILF,////,////,CZBD,UNK,1.0,Ilford,"Ilford, Canada",OWNO,////,////,Ilford Airport,"Ilford, Manitoba, Canada",OK
    ILG,ILG,KILG,KILG,OK,0.585,Greater Wilmington,"Wilmington (DE), USA",OWNO,NEW CASTLE,"WILMINGTON, DE - UNITED STATES",New Castle Airport,"Wilmington, Delaware, United States",OK
    ILH,////,////,ETIK,UNK,0.667,Illis Airbase,"Illisheim, Germany",OWNO,////,////,AAF,"Illisheim, Bavaria, Germany",OK

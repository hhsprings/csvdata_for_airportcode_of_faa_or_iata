
List for checking certainty of Great Circle Mapper (BAA - BEL)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    BAA,////,////,////,UNK,1.0,Bialla,"Bialla, Papua New Guinea",OWNO,////,////,????,"Bialla, West New Britain, Papua-New Guinea",OK
    BAB,BAB,KBAB,KBAB,OK,1.0,Beale AFB,"Marysville (CA), USA",OWNO,BEALE AFB,"MARYSVILLE, CA - UNITED STATES",Beale AFB,"Marysville, California, United States",OK
    BAC,////,////,////,UNK,0.762,Barranca De Upia,"Barranca De Upia, Colombia",OWNO,////,////,Guadalito Airport,"Barranca de Upía, Meta, Colombia",OK
    BAD,BAD,KBAD,KBAD,OK,1.0,Barksdale AFB,"Shreveport (LA), USA",OWNO,BARKSDALE AFB,"BOSSIER CITY, LA - UNITED STATES",Barksdale AFB,"Bossier City, Louisiana, United States",OK
    BAE,////,////,LFMR,UNK,0.769,Barcelonnette,"Barcelonnette, France",OWNO,////,////,St-Pons,"Barcelonnette, Provence-Alpes-Côte d'Azur, France",OK
    BAF,BAF,KBAF,KBAF,OK,0.852,Barnes,"Westfield (MA), USA",OWNO,WESTFIELD-BARNES RGNL,"WESTFIELD/SPRINGFIELD, MA - UNITED STATES",Westfield-Barnes Regional,"Westfield/Springfield, Massachusetts, United States",OK
    BAG,////,////,RPUB,UNK,1.0,Loakan,"Baguio, Philippines",OWNO,////,////,Loakan Airport,"Baguio City, Philippines",OK
    BAH,////,////,OBBI,UNK,1.0,Bahrain International,"Bahrain, Bahrain",OWNO,////,////,Bahrain International Airport,"Manama, Bahrain",OK
    BAI,////,////,MRBA,UNK,1.0,Buenos Aires,"Buenos Aires, Costa Rica",OWNO,////,////,????,"Buenos Aires, Puntarenas, Costa Rica",OK
    BAJ,////,////,////,UNK,1.0,Bali,"Bali, Papua New Guinea",OWNO,////,////,????,"Bali, West New Britain, Papua-New Guinea",OK
    BAL,////,////,LTCJ,UNK,0.48,Batman,"Batman, Turkey",OWNO,////,////,Batman Military Air Base,"Batman, Batman, Turkey",OK
    BAM,BAM,KBAM,KBAM,OK,0.576,Lander County,"Battle Mountain (NV), USA",OWNO,BATTLE MOUNTAIN,"BATTLE MOUNTAIN, NV - UNITED STATES",????,"Battle Mountain, Nevada, United States",OK
    BAN,////,////,FZVR,UNK,1.0,Basongo,"Basongo, Congo (DRC)",OWNO,////,////,????,"Basongo, Kasai-Occidental (West Kasai), Democratic Republic of Congo (Zaire)",OK
    BAP,////,////,////,UNK,1.0,Baibara,"Baibara, Papua New Guinea",OWNO,////,////,????,"Baibara, Central, Papua-New Guinea",OK
    BAQ,////,////,SKBQ,UNK,0.814,E Cortissoz,"Barranquilla, Colombia",OWNO,////,////,Ernesto Cortissoz International Airport,"Barranquilla, Atlántico, Colombia",OK
    BAS,////,////,AGGE,UNK,1.0,Balalae,"Balalae, Solomon Islands",OWNO,////,////,????,"Ballalae, Solomon Islands",OK
    BAT,////,////,SBBT,UNK,0.556,Barretos,"Barretos, Brazil",OWNO,////,////,Chafei Amsei,"Barretos, São Paulo, Brazil",OK
    BAU,////,////,SBBU,UNK,1.0,Bauru,"Bauru, Brazil",OWNO,////,////,????,"Bauru, São Paulo, Brazil",OK
    BAV,////,////,ZBOW,UNK,0.84,Baotou,"Baotou, PR China",OWNO,////,////,Baotou Erliban Airport,"Baotou, Inner Mongolia, China",OK
    BAW,////,////,////,UNK,1.0,Biawonque,"Biawonque, Gabon",OWNO,////,////,????,"Biawonque, Ogooué-Maritime, Gabon",OK
    BAX,////,////,UNBB,UNK,0.6,Barnaul,"Barnaul, Russia",OWNO,////,////,Mikhaylovka Airport,"Barnaul, Altayskiy, Russian Federation (Russia)",OK
    BAY,////,////,LRBM,UNK,0.514,Baia Mare,"Baia Mare, Romania",OWNO,////,////,Tautii Magheraus,"Baia Mare, Romania",OK
    BAZ,////,////,SWBC,UNK,0.875,Barbelos,"Barbelos, Brazil",OWNO,////,////,????,"Barcelos, Amazonas, Brazil",TO DO CHECK
    BBA,////,////,SCBA,UNK,0.552,Teniente Vidal,"Balmaceda, Chile",OWNO,////,////,????,"Balmaceda, Aysén del General Carlos Ibáñez del Campo, Chile",OK
    BBB,BBB,KBBB,KBBB,OK,1.0,Municipal,"Benson (MN), USA",OWNO,BENSON MUNI,"BENSON, MN - UNITED STATES",Benson Municipal Airport,"Benson, Minnesota, United States",OK
    BBC,BYY,KBYY,KBYY,OK,0.719,Bay City,"Bay City (TX), USA",OWNO,BAY CITY MUNI,"BAY CITY, TX - UNITED STATES",Bay City Municipal Airport,"Bay City, Texas, United States",OK
    BBD,BBD,KBBD,KBBD,OK,1.0,Curtis Field,"Brady (TX), USA",OWNO,CURTIS FIELD,"BRADY, TX - UNITED STATES",Curtis Field,"Brady, Texas, United States",OK
    BBG,////,////,NGTU,UNK,0.818,Butaritari,"Butaritari, Kiribati",OWNO,////,////,????,"Butaritari Atoll, Kiribati",MAYBE
    BBH,////,////,EDBH,UNK,1.0,Barth,"Barth, Germany",OWNO,////,////,????,"Barth, Mecklenburg-Vorpommern, Germany",OK
    BBI,////,////,VEBS,UNK,1.0,Bhubaneswar,"Bhubaneswar, India",OWNO,////,////,????,"Bhubaneswar, Odisha, India",OK
    BBJ,////,////,EDRB,UNK,0.5,Bitburg Air Base,"Bitburg, Germany",OWNO,////,////,Flugplatz Bitburg,"Bitburg, Rhineland-Palatinate, Germany",OK
    BBK,////,////,FBKE,UNK,1.0,Kasane,"Kasane, Botswana",OWNO,////,////,????,"Kasane, North-West, Botswana",OK
    BBL,////,////,YLLE,UNK,1.0,Ballera,Ballera,IATA,////,////,Ballera Airport,"Ballera, Queensland, Australia",MAYBE
    BBM,////,////,VDBG,UNK,1.0,Battambang,"Battambang, Cambodia",OWNO,////,////,????,"Battambang, Cambodia",OK
    BBN,////,////,WBGZ,UNK,1.0,Bario,"Bario, Malaysia",OWNO,////,////,????,"Bario, Sarawak, Malaysia",OK
    BBO,////,////,HCMI,UNK,1.0,Berbera,"Berbera, Somalia",OWNO,////,////,Berbera Airport,"Berbera, Woqooyi Galbeed, Somalia",OK
    BBP,////,////,EGHJ,UNK,1.0,Bembridge,"Bembridge, United Kingdom",OWNO,////,////,????,"Bembridge, Isle of Wight, England, United Kingdom",OK
    BBQ,////,////,TAPH,UNK,0.56,Barbuda,"Barbuda, Antigua and Barbuda",OWNO,////,////,Codrington,"Barbuda, Antigua and Barbuda",OK
    BBR,////,////,TFFB,UNK,1.0,Baillif,"Basse Terre, Guadeloupe",OWNO,////,////,Baillif,"Basse Terre, Guadeloupe",OK
    BBS,////,////,EGLK,UNK,1.0,Blackbush,"Blackbush, United Kingdom",OWNO,////,////,Blackbushe Airport,"Blackbushe, Hampshire, England, United Kingdom",OK
    BBT,////,////,FEFT,UNK,1.0,Berberati,"Berberati, Central African Republic",OWNO,////,////,????,"Berberati, Haute-Sangha/Mambéré-Kadéï (Tö-Sangä/Mbaere-Kadeï), Central African Republic",OK
    BBU,////,////,LRBS,UNK,0.588,Baneasa,"Bucharest, Romania",OWNO,////,////,Aurel Vlaicu International Airport,"Bucharest, Romania",OK
    BBV,////,////,DIGN,UNK,0.387,Bereby,"Bereby, Cote d'Ivoire",OWNO,////,////,Néro-sur-Mer Airport,"Grand Béréby, Bas-Sassandra, Côte d'Ivoire (Ivory Coast)",MAYBE
    BBW,BBW,KBBW,KBBW,OK,0.79,Broken Bow,"Broken Bow (NE), USA",OWNO,BROKEN BOW MUNI/KEITH GLAZE FLD,"BROKEN BOW, NE - UNITED STATES",Broken Bow Municipal/Keith Glaze Field,"Broken Bow, Nebraska, United States",OK
    BBX,LOM,KLOM,KLOM,OK,1.0,Wings Field,"Blue Bell (PA), USA",OWNO,WINGS FIELD,"PHILADELPHIA, PA - UNITED STATES",Wings Field,"Philadelphia, Pennsylvania, United States",OK
    BBY,////,////,FEFM,UNK,1.0,Bambari,"Bambari, Central African Republic",OWNO,////,////,????,"Bambari, Ouaka (Wäkä), Central African Republic",OK
    BBZ,////,////,FLZB,UNK,1.0,Zambezi,"Zambezi, Zambia",OWNO,////,////,????,"Zambezi, Zambia",OK
    BCA,////,////,MUBA,UNK,0.519,Baracoa,"Baracoa, Cuba",OWNO,////,////,Gustavo Rizo,"Baracoa, Las Tunas, Cuba",OK
    BCB,BCB,KBCB,KBCB,OK,1.0,Virginia Tech,"Blacksburg (VA), USA",OWNO,VIRGINIA TECH/MONTGOMERY EXECUTIVE,"BLACKSBURG, VA - UNITED STATES",Virginia Tech / Montgomery Executive Airport,"Blacksburg, Virginia, United States",OK
    BCC,Z48,////,////,OK,1.0,Bear Creek,"Bear Creek (AK), USA",OWNO,BEAR CREEK 3,"BEAR CREEK, AK - UNITED STATES",Bear Creek 3 Airport,"Bear Creek, Alaska, United States",OK
    BCD,////,////,RPVB,UNK,0.636,Bacolod-Silay International Airport,"Bacolod City, Negros Occidental, Philippines",OWNO,////,////,????,"Bacolod, Philippines",MAYBE
    BCE,BCE,KBCE,KBCE,OK,0.549,Bryce,"Bryce (UT), USA",OWNO,BRYCE CANYON,"BRYCE CANYON, UT - UNITED STATES",????,"Bryce Canyon, Utah, United States",OK
    BCF,////,////,FEGU,UNK,1.0,Bouca,"Bouca, Central African Republic",OWNO,////,////,Bouca Airport,"Bouca, Ouham (Wâmo), Central African Republic",OK
    BCG,////,////,////,UNK,1.0,Bemichi,"Bemichi, Guyana",OWNO,////,////,????,"Bemichi, Barima-Waini, Guyana",OK
    BCH,////,////,WPEC,UNK,0.5,English Madeira,"Baucau, Indonesia",OWNO,////,////,Cakung International Airport,"Baucau, Timor-Leste (East Timor)",OK
    BCI,////,////,YBAR,UNK,1.0,Barcaldine,"Barcaldine, Australia",OWNO,////,////,????,"Barcaldine, Queensland, Australia",OK
    BCK,////,////,YBWR,UNK,1.0,Bolwarra,"Bolwarra, Australia",OWNO,////,////,????,"Bolwarra, Queensland, Australia",OK
    BCL,////,////,MRBC,UNK,0.875,Barra Colorado,"Barra Colorado, Costa Rica",OWNO,////,////,????,"Barra del Colorado, Limón, Costa Rica",MAYBE
    BCM,////,////,LRBC,UNK,0.345,Bacau,"Bacau, Romania",OWNO,////,////,George Enescu International Airport,"Bacau, Romania",OK
    BCN,////,////,LEBL,UNK,0.727,Barcelona,"Barcelona, Spain",OWNO,////,////,El Prat,"Barcelona, Catalonia, Spain",OK
    BCO,////,////,HABC,UNK,0.667,Jinka,"Jinka, Ethiopia",OWNO,////,////,Baco Airport,"Jinka, SNNPR, Ethiopia",OK
    BCP,////,////,////,UNK,1.0,Bambu,"Bambu, Papua New Guinea",OWNO,////,////,????,"Bambu, Madang, Papua-New Guinea",OK
    BCQ,////,////,////,UNK,1.0,Brack,"Brack, Libya",OWNO,////,////,????,"Brak, Libyan Arab Jamahiriya (Libya)",OK
    BCR,////,////,SWNK,UNK,0.686,Boca Do Acre,"Boca Do Acre, Brazil",OWNO,////,////,Novo Campo,"Boca do Acre, Amazonas, Brazil",OK
    BCS,65LA,////,////,OK,1.0,Southern Seaplane,"Belle Chasse (LA), USA",OWNO,SOUTHERN SEAPLANE,"BELLE CHASSE, LA - UNITED STATES",Southern Seaplane Airport,"Belle Chasse, Louisiana, United States",OK
    BCT,BCT,KBCT,KBCT,OK,0.709,Public,"Boca Raton (FL), USA",OWNO,BOCA RATON,"BOCA RATON, FL - UNITED STATES",????,"Boca Raton, Florida, United States",OK
    BCU,////,////,DNBA,UNK,0.522,Bauchi,"Bauchi, Nigeria",OWNO,////,////,Bauchi State International Airport,"Bauchi, Bauchi, Nigeria",OK
    BCV,////,////,////,UNK,0.5,Belmopan,"Belmopan, Belize",OWNO,////,////,Hector Silva Airstrip,"Belmopan, Cayo, Belize",OK
    BCW,////,////,////,UNK,1.0,Benguera Island,"Benguera Island, Mozambique",OWNO,////,////,????,"Benguéra Island, Bazaruto Archipelago, Mozambique",OK
    BCX,////,////,UWUB,UNK,0.875,Beloreck,"Beloreck, Russia",OWNO,////,////,????,"Beloretsk, Bashkortostan, Russian Federation (Russia)",TO DO CHECK
    BCY,////,////,HABU,UNK,1.0,Bulchi,"Bulchi, Ethiopia",OWNO,////,////,Bulchi Airport,"Bulchi (Bulki, SNNPR, Ethiopia",OK
    BCZ,////,////,////,UNK,1.0,Bickerton Island,"Bickerton Island, Australia",OWNO,////,////,????,"Milyakburra, Bickerton Island, Northern Territory, Australia",OK
    BDA,////,TXKF,TXKF,OK,0.448,Bermuda International,"Bermuda, Bermuda",OWNO,L F WADE INTL,"HAMILTON, - BERMUDA",L.F. Wade International Airport,"Bermuda, Bermuda",TO DO CHECK
    BDB,////,////,YBUD,UNK,1.0,Bundaberg,"Bundaberg, Australia",OWNO,////,////,????,"Bundaberg, Queensland, Australia",OK
    BDC,////,////,SNBC,UNK,0.718,Barra Do Corda,"Barra Do Corda, Brazil",OWNO,////,////,Aeropuerto Barra do Corda,"Barra do Corda, Maranhão, Brazil",OK
    BDD,////,////,YBAU,UNK,1.0,Badu Island,"Badu Island, Australia",OWNO,////,////,????,"Badu Island, Queensland, Australia",OK
    BDE,BDE,KBDE,KBDE,OK,0.805,Baudette,"Baudette (MN), USA",OWNO,BAUDETTE INTL,"BAUDETTE, MN - UNITED STATES",Baudette International Airport,"Baudette, Minnesota, United States",OK
    BDF,3IS8,////,////,OK,0.962,Rinkenberger,"Bradford (IL), USA",OWNO,RINKENBERGER RLA,"BRADFORD, IL - UNITED STATES",Rinkenberger RLA Airport,"Bradford, Illinois, United States",OK
    BDG,BDG,KBDG,KBDG,OK,0.757,Blanding,"Blanding (UT), USA",OWNO,BLANDING MUNI,"BLANDING, UT - UNITED STATES",Blanding Municipal Airport,"Blanding, Utah, United States",OK
    BDH,////,////,OIBL,UNK,1.0,Bandar Lengeh,"Bandar Lengeh, Iran",OWNO,////,////,????,"Bandar Lengeh, Hormozgan, Iran",OK
    BDI,////,////,FSSB,UNK,1.0,Bird Island,"Bird Island, Seychelles",OWNO,////,////,????,"Bird Island, Seychelles",OK
    BDJ,////,////,WAOO,UNK,0.957,Sjamsudin Noor,"Banjarmasin, Indonesia",OWNO,////,////,Syamsudin Noor Airport,"Banjarmarsin, Kalimantan Selatan (South Borneo), Indonesia",MAYBE
    BDK,////,////,DIBU,UNK,0.783,Bondoukou,"Bondoukou, Cote d'Ivoire",OWNO,////,////,Soko,"Bondoukou, Zanzan, Côte d'Ivoire (Ivory Coast)",OK
    BDL,BDL,KBDL,KBDL,OK,0.896,Hartford Bradley International,"Hartford, Windsor Locks (CT), USA",OWNO,BRADLEY INTL,"WINDSOR LOCKS, CT - UNITED STATES",Bradley International Airport,"Hartford/Springfield, Connecticut, United States",OK
    BDM,////,////,LTBG,UNK,1.0,Bandirma,"Bandirma, Turkey",OWNO,////,////,????,"Bandirma, Balikesir, Turkey",OK
    BDN,////,////,OPTH,UNK,1.0,Talhar,"Badin, Pakistan",OWNO,////,////,Talhar,"Badin, Sindh, Pakistan",OK
    BDO,////,////,WICC,UNK,0.917,Husein Sastranegara,"Bandung, Indonesia",OWNO,////,////,Bandung-Husein Sastranegara International Airport,"Bandung, Jawa Barat, Indonesia",OK
    BDP,////,////,VNCG,UNK,0.588,Bhadrapur,"Bhadrapur, Nepal",OWNO,////,////,????,"Chandragadhi, Nepal",TO DO CHECK
    BDQ,////,////,VABO,UNK,1.0,Vadodara,"Vadodara, India",OWNO,////,////,????,"Vadodara, Gujarat, India",OK
    BDR,BDR,KBDR,KBDR,OK,0.891,Igor I. Sikorsky Mem.,"Bridgeport (CT), USA",OWNO,IGOR I SIKORSKY MEMORIAL,"BRIDGEPORT, CT - UNITED STATES",Igor I Sikorsky Memorial Airport,"Bridgeport, Connecticut, United States",OK
    BDS,////,////,LIBR,UNK,0.813,Papola Casale,"Brindisi, Italy",OWNO,////,////,Casale,"Brindisi, Apulia, Italy",OK
    BDT,////,////,FZFD,UNK,1.0,Gbadolite,"Gbadolite, Congo (DRC)",OWNO,////,////,Gbadolite Airport,"Gbadolite, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    BDU,////,////,ENDU,UNK,1.0,Bardufoss,"Bardufoss, Norway",OWNO,////,////,????,"Bardufoss, Norway",OK
    BDV,////,////,FZRB,UNK,1.0,Moba,"Moba, Congo (DRC)",OWNO,////,////,Moba Airport,"Moba, Katanga, Democratic Republic of Congo (Zaire)",OK
    BDW,////,////,YBDF,UNK,1.0,Bedford Downs,"Bedford Downs, Australia",OWNO,////,////,????,"Bedford Downs, Western Australia, Australia",OK
    BDX,BDX,////,KBDX,UNK,1.0,Broadus Airport,"Broadus (MT), USA",OWNO,////,////,????,"Broadus, Montana, United States",OK
    BDY,S05,////,////,OK,1.0,State,"Bandon (OR), USA",OWNO,BANDON STATE,"BANDON, OR - UNITED STATES",Bandon State Airport,"Bandon, Oregon, United States",OK
    BDZ,////,////,////,UNK,1.0,Baindoung,"Baindoung, Papua New Guinea",OWNO,////,////,????,"Baindoung, Morobe, Papua-New Guinea",OK
    BEA,////,////,////,UNK,1.0,Bereina,"Bereina, Papua New Guinea",OWNO,////,////,????,"Bereina, Central, Papua-New Guinea",OK
    BEB,////,////,EGPL,UNK,1.0,Benbecula,"Benbecula, United Kingdom",OWNO,////,////,????,"Benbecula, Hebrides, Scotland, United Kingdom",OK
    BEC,BEC,KBEC,KBEC,OK,0.694,Beech,"Wichita (KS), USA",OWNO,BEECH FACTORY,"WICHITA, KS - UNITED STATES",Beech Factory Airport,"Wichita, Kansas, United States",OK
    BED,BED,KBED,KBED,OK,0.746,Hanscom Field,"Bedford (MA), USA",OWNO,LAURENCE G HANSCOM FLD,"BEDFORD, MA - UNITED STATES",Laurence G Hanscom Field,"Bedford, Massachusetts, United States",OK
    BEE,////,////,YBGB,UNK,1.0,Beagle Bay,"Beagle Bay, Australia",OWNO,////,////,????,"Beagle Bay, Western Australia, Australia",OK
    BEF,////,////,MNBL,UNK,1.0,Bluefields,"Bluefields, Nicaragua",OWNO,////,////,????,"Bluefields, Atlántico Sur, Nicaragua",OK
    BEG,////,////,LYBE,UNK,0.513,Aerodrom Beograd,"Belgrade, Serbia",OWNO,////,////,Belgrade Nikola Tesla,"Belgrade, Serbia",OK
    BEH,BEH,KBEH,KBEH,OK,0.552,Ross Field,"Benton Harbor (MI), USA",OWNO,SOUTHWEST MICHIGAN RGNL,"BENTON HARBOR, MI - UNITED STATES",Southwest Michigan Regional,"Benton Harbor, Michigan, United States",OK
    BEI,////,////,HABE,UNK,1.0,Beica,"Beica, Ethiopia",OWNO,////,////,Beica Airport,"Beica, Oromia, Ethiopia",OK
    BEJ,////,////,WALK,UNK,0.526,Berau,"Berau, Indonesia",OWNO,////,////,Kalimaru,"Tanjung Redep, Berau, Kalimantan (Borneo), Kalimantan Timur (East Borneo), Indonesia",OK
    BEK,////,////,////,UNK,1.0,Bareli,"Bareli, India",OWNO,////,////,????,"Bareli, Uttar Pradesh, India",OK
    BEL,////,////,SBBE,UNK,0.97,Val de Cans International Airport,"Belem, Brazil",OWNO,////,////,Belém International - Val-de-Cães,"Belém, Pará, Brazil",OK

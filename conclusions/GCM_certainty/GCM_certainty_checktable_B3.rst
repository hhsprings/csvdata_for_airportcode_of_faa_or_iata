
List for checking certainty of Great Circle Mapper (BJC - BNP)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    BJC,BJC,KBJC,KBJC,OK,0.238,Jeffco,"Broomfield (CO), USA",OWNO,ROCKY MOUNTAIN METROPOLITAN,"DENVER, CO - UNITED STATES",Rocky Mountain Metropolitan Airport,"Denver, Colorado, United States",OK
    BJD,////,////,BIBK,UNK,1.0,Bakkafjordur,"Bakkafjordur, Iceland",OWNO,////,////,????,"Bakkafjörður, Iceland",OK
    BJF,////,////,ENBS,UNK,1.0,Båtsfjord Airport,"Batsfjord, Norway",OWNO,////,////,????,"Båtsfjord, Norway",OK
    BJG,////,////,////,UNK,1.0,Bolaang,"Bolaang, Indonesia",OWNO,////,////,????,"Bolaang, Sulawesi Utara, Indonesia",OK
    BJH,////,////,VNBG,UNK,1.0,Bajhang,"Bajhang, Nepal",OWNO,////,////,????,"Bajhang, Nepal",OK
    BJI,BJI,KBJI,KBJI,OK,0.76,Bemidji,"Bemidji (MN), USA",OWNO,BEMIDJI RGNL,"BEMIDJI, MN - UNITED STATES",Bemidji Regional,"Bemidji, Minnesota, United States",OK
    BJJ,BJJ,KBJJ,KBJJ,OK,1.0,Wayne County,"Wooster (OH), USA",OWNO,WAYNE COUNTY,"WOOSTER, OH - UNITED STATES",Wayne County Airport,"Wooster, Ohio, United States",OK
    BJK,////,////,WAPK,UNK,0.5,Benjina,"Benjina, Indonesia",OWNO,////,////,Nangasuri Aru Airport,"Benjina, Aru Island, Maluku, Indonesia",OK
    BJL,////,////,GBYD,UNK,0.8,Yundum International,"Banjul, Gambia",OWNO,////,////,Banjul International Airport,"Banjul, Gambia",OK
    BJM,////,////,HBBA,UNK,1.0,Bujumbura International,"Bujumbura, Burundi",OWNO,////,////,Bujumbura International Airport,"Bujumbura, Burundi",OK
    BJN,////,////,////,UNK,1.0,Bajone,"Bajone, Mozambique",OWNO,////,////,????,"Bajone, Mozambique",OK
    BJO,////,////,SLBJ,UNK,1.0,Bermejo,"Bermejo, Bolivia",OWNO,////,////,Bermejo Airport,"Bermejo, Aniceto Arce, Tarija, Bolivia",OK
    BJR,////,////,HABD,UNK,1.0,Bahar Dar,"Bahar Dar, Ethiopia",OWNO,////,////,????,"Bahar Dar, Amara, Ethiopia",OK
    BJS,////,////,////,UNK,1.0,Metropolitan Area,"Beijing, PR China",OWNO,////,////,Metropolitan Area,"Beijing, Beijing, China",OK
    BJT,////,////,////,UNK,0.889,Bentota River SPB,Bentota,IATA,////,////,Bentota River Airport,"Bentota, Southern Province, Sri Lanka (Ceylon)",MAYBE
    BJU,////,////,VNBR,UNK,1.0,Bajura Airport,"Bajura, Nepal",OWNO,////,////,????,"Bajura, Nepal",OK
    BJV,////,////,LTFE,UNK,1.0,Milas Airport,"Bodrum., Turkey",OWNO,////,////,Milas,"Bodrum, Mugla, Turkey",OK
    BJW,////,////,WRKB,UNK,1.0,Bajawa,"Bajawa, Indonesia",OWNO,////,////,????,"Bajawa, Flores Island, Nusa Tenggara Timur, Indonesia",OK
    BJX,////,////,MMLO,UNK,0.71,Del Bajio,"Leon/Guanajuato, Mexico",OWNO,////,////,Guanajuato International Airport,"León, Guanajuato, México",OK
    BJY,////,////,LYBT,UNK,0.8,Batajnica,"Belgrade, Serbia",OWNO,////,////,Batajnia AB,"Batajnica, Serbia",OK
    BJZ,////,////,LEBZ,UNK,0.98,Talaveral La Real,"Badajoz, Spain",OWNO,////,////,Talavera La Real,"Badajoz, Extremadura, Spain",OK
    BKA,////,////,UUBB,UNK,1.0,Bykovo,"Moscow, Russia",OWNO,////,////,Bykovo Airport,"Moscow, Moskovskaya, Russian Federation (Russia)",OK
    BKB,////,////,VIBK,UNK,0.824,Bikaner,"Bikaner, India",OWNO,////,////,Nal,"Bikaner, Rajasthan, India",OK
    BKC,BVK,PABL,PABL,OK,1.0,Buckland,"Buckland (AK), USA",OWNO,BUCKLAND,"BUCKLAND, AK - UNITED STATES",????,"Buckland, Alaska, United States",OK
    BKD,BKD,KBKD,KBKD,OK,1.0,Stephens County,"Breckenridge (TX), USA",OWNO,STEPHENS COUNTY,"BRECKENRIDGE, TX - UNITED STATES",Stephens County Airport,"Breckenridge, Texas, United States",OK
    BKE,BKE,KBKE,KBKE,OK,0.576,Baker,"Baker (OR), USA",OWNO,BAKER CITY MUNI,"BAKER CITY, OR - UNITED STATES",Baker City Municipal Airport,"Baker City, Oregon, United States",OK
    BKG,BBG,KBBG,KBBG,OK,1.0,////,////,////,BRANSON,"BRANSON, MO - UNITED STATES",????,"Branson, Missouri, United States",OK
    BKH,BKH,PHBK,PHBK,OK,0.425,Barking Sands,"Kekaha (HI), USA",OWNO,BARKING SANDS PMRF,"KEKAHA, KAUAI, HI - UNITED STATES",Barking Sands Pacific Missile Range Facility (PMRF) Airport,"Kekaha, Kauai, Hawaii, United States",OK
    BKI,////,////,WBKK,UNK,0.846,Kota Kinabalu,"Kota Kinabalu, Malaysia",OWNO,////,////,Kota-Kinabalu International Airport,"Kota-Kinabalu, Sabah, Malaysia",OK
    BKJ,////,////,GUOK,UNK,1.0,Boke,"Boke, Guinea",OWNO,////,////,????,"Boke, Guinea",OK
    BKK,////,////,VTBS,UNK,0.519,Metropolitan Area,"Bangkok, Thailand",OWNO,////,////,Suvarnabhumi Bangkok International Airport,"Bangkok, Samut Prakan, Thailand",OK
    BKL,BKL,KBKL,KBKL,OK,1.0,Burke Lakefront,"Cleveland (OH), USA",OWNO,BURKE LAKEFRONT,"CLEVELAND, OH - UNITED STATES",Burke Lakefront Airport,"Cleveland, Ohio, United States",OK
    BKM,////,////,WBGQ,UNK,1.0,Bakalalan,"Bakalalan, Malaysia",OWNO,////,////,????,"Ba'kelalan, Sarawak, Malaysia",OK
    BKN,////,////,////,UNK,1.0,Balkanabat,Balkanabat,IATA,////,////,Balkanabat Airport,"Balkanabat, Balkan, Turkmenistan",MAYBE
    BKO,////,////,GABS,UNK,0.435,Bamako,"Bamako, Mali",OWNO,////,////,Modibo Keita International Airport,"Bamako, Bamako, Mali",OK
    BKP,////,////,YBAW,UNK,1.0,Barkly Downs,"Barkly Downs, Australia",OWNO,////,////,????,"Barkly Downs, Queensland, Australia",OK
    BKQ,////,////,YBCK,UNK,1.0,Blackall,"Blackall, Australia",OWNO,////,////,????,"Blackall, Queensland, Australia",OK
    BKR,////,////,FTTK,UNK,1.0,Bokoro,"Bokoro, Chad",OWNO,////,////,????,"Bokoro, Chari-Baguirmi, Chad",OK
    BKS,////,////,WIPL,UNK,0.605,Padangkemiling,"Bengkulu, Indonesia",OWNO,////,////,Fatmawati Soekarno Airport,"Bengkulu, Bengkulu, Indonesia",OK
    BKT,BKT,KBKT,KBKT,OK,0.517,Blackstone AAF,"Blackstone (VA), USA",OWNO,ALLEN C PERKINSON BLACKSTONE AAF,"BLACKSTONE, VA - UNITED STATES",Allen C Perkinson Blackstone AAF Airport,"Blackstone, Virginia, United States",OK
    BKU,////,////,FMSV,UNK,1.0,Betioky,"Betioky, Madagascar",OWNO,////,////,Betioky Airport,"Betioky, Madagascar",OK
    BKW,BKW,KBKW,KBKW,OK,0.404,Beckley,"Beckley (WV), USA",OWNO,RALEIGH COUNTY MEMORIAL,"BECKLEY, WV - UNITED STATES",Raleigh County Memorial Airport,"Beckley, West Virginia, United States",OK
    BKX,BKX,KBKX,KBKX,OK,0.81,Brookings,"Brookings (SD), USA",OWNO,BROOKINGS RGNL,"BROOKINGS, SD - UNITED STATES",Brookings Regional,"Brookings, South Dakota, United States",OK
    BKY,////,////,FZMA,UNK,0.87,Kamenbe,"Bukavu, Congo (DRC)",OWNO,////,////,Kuvumu Airport,"Bukavu, Sud-Kivu (South Kivu), Democratic Republic of Congo (Zaire)",OK
    BKZ,////,////,HTBU,UNK,1.0,Bukoba,"Bukoba, Tanzania",OWNO,////,////,Bukoba Airport,"Bukoba, Kagera, Tanzania",OK
    BLA,////,////,SVBC,UNK,0.757,Gen. J A Anzoategui,"Barcelona, Venezuela",OWNO,////,////,Generál José Antonio Anzoátegui International Airport,"Barcelona, Anzoátegui, Venezuela",OK
    BLB,////,////,MPHO,UNK,0.667,Balboa,"Balboa, Panama",OWNO,////,////,Howard AFB,"Balboa, Panamá, Panamá",OK
    BLC,////,////,FKKG,UNK,1.0,Bali,"Bali, Cameroon",OWNO,////,////,Bali Airport,"Bali, North-West (Nord-Ouest), Cameroon",OK
    BLD,BVU,KBVU,KBVU,OK,0.781,Boulder City,"Boulder City (NV), USA",OWNO,BOULDER CITY MUNI,"BOULDER CITY, NV - UNITED STATES",Boulder City Municipal Airport,"Boulder City, Nevada, United States",OK
    BLE,////,////,ESSD,UNK,0.865,Dala,"Borlange/Falun, Sweden",OWNO,////,////,Borlänge AB,"Borlänge, Dalarnas län, Sweden",OK
    BLF,BLF,KBLF,KBLF,OK,0.653,Bluefield/Princeton Airport,"Princeton (WV), USA",OWNO,MERCER COUNTY,"BLUEFIELD, WV - UNITED STATES",Mercer County Airport,"Bluefield, West Virginia, United States",OK
    BLG,////,////,WBGC,UNK,1.0,Belaga,"Belaga, Malaysia",OWNO,////,////,????,"Belaga, Sarawak, Malaysia",OK
    BLH,BLH,KBLH,KBLH,OK,1.0,Blythe,"Blythe (CA), USA",OWNO,BLYTHE,"BLYTHE, CA - UNITED STATES",????,"Blythe, California, United States",OK
    BLI,BLI,KBLI,KBLI,OK,0.801,Bellingham,"Bellingham (WA), USA",OWNO,BELLINGHAM INTL,"BELLINGHAM, WA - UNITED STATES",Bellingham International Airport,"Bellingham, Washington, United States",OK
    BLJ,////,////,DABT,UNK,0.348,Batna,"Batna, Algeria",OWNO,////,////,Mostepha Ben Boulaid Airport,"Batna, Batna, Algeria",OK
    BLK,////,////,EGNH,UNK,0.636,Blackpool,"Blackpool, United Kingdom",OWNO,////,////,Squire's Gate Airport,"Blackpool, Lancashire, England, United Kingdom",OK
    BLL,////,////,EKBI,UNK,1.0,Billund,"Billund, Denmark",OWNO,////,////,????,"Billund, Denmark",OK
    BLM,BLM,KBLM,KBLM,OK,0.709,Monmouth County,"Belmar (NJ), USA",OWNO,MONMOUTH EXECUTIVE,"BELMAR/FARMINGDALE, NJ - UNITED STATES",Monmouth Executive Airport,"Belmar/Farmingdale, New Jersey, United States",OK
    BLN,////,////,YBLA,UNK,1.0,Benalla,"Benalla, Australia",OWNO,////,////,????,"Benalla, Victoria, Australia",OK
    BLO,////,////,BIBL,UNK,0.571,Blonduos,"Blonduos, Iceland",OWNO,////,////,Hjaltabakki,"Blönduós, Iceland",OK
    BLP,////,////,////,UNK,0.8,Bellavista,"Bellavista, Peru",OWNO,////,////,Huallaga Airport,"Bellavista, San Martín, Perú",OK
    BLQ,////,////,LIPE,UNK,1.0,Guglielmo Marconi,"Bologna, Italy",OWNO,////,////,Guglielmo Marconi,"Bologna, Emilia-Romagna, Italy",OK
    BLR,////,////,VOBL,UNK,0.75,Bengaluru International Airport,"Bangalore, India",OWNO,////,////,Kempegowda International Airport,"Bangalore, Karnataka, India",OK
    BLS,////,////,YBLL,UNK,1.0,Bollon,"Bollon, Australia",OWNO,////,////,????,"Bollon, Queensland, Australia",OK
    BLT,////,////,YBTR,UNK,1.0,Blackwater,"Blackwater, Australia",OWNO,////,////,????,"Blackwater, Queensland, Australia",OK
    BLU,BLU,KBLU,KBLU,OK,0.891,Blue Canyon,"Blue Canyon (CA), USA",OWNO,BLUE CANYON - NYACK,"EMIGRANT GAP, CA - UNITED STATES",Blue Canyon - Nyack Airport,"Emigrant Gap, California, United States",OK
    BLV,BLV,KBLV,KBLV,OK,1.0,Scott AFB/Mid America,"Belleville (IL), USA",OWNO,SCOTT AFB/MIDAMERICA,"BELLEVILLE, IL - UNITED STATES",Scott AFB/Midamerica Airport,"Belleville, Illinois, United States",OK
    BLX,////,////,LIDB,UNK,0.467,Belluno,"Belluno, Italy",OWNO,////,////,Arturo dell'Oro,"Belluno, Veneto, Italy",OK
    BLY,////,////,EIBT,UNK,1.0,Belmullet,"Belmullet, Ireland",OWNO,////,////,????,"Belmullet, County Mayo, Connacht, Ireland",OK
    BLZ,////,////,FWCL,UNK,0.867,Chileka,"Blantyre, Malawi",OWNO,////,////,Chileka International Airport,"Blantyre, Malawi",OK
    BMA,////,////,ESSB,UNK,1.0,Bromma,"Stockholm, Sweden",OWNO,////,////,Bromma,"Stockholm, Stockholms län, Sweden",OK
    BMB,////,////,FZFU,UNK,1.0,Bumba,"Bumba, Congo (DRC)",OWNO,////,////,Bumba Airport,"Bumba, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    BMC,BMC,KBMC,KBMC,OK,1.0,Brigham City,"Brigham City (UT), USA",OWNO,BRIGHAM CITY,"BRIGHAM CITY, UT - UNITED STATES",????,"Brigham City, Utah, United States",OK
    BMD,////,////,FMML,UNK,0.333,Belo,"Belo, Madagascar",OWNO,////,////,????,"Belo sur Tsiribihina, Madagascar",MAYBE
    BME,////,////,YBRM,UNK,1.0,Broome,"Broome, Australia",OWNO,////,////,????,"Broome, Western Australia, Australia",OK
    BMF,////,////,FEGM,UNK,1.0,Bakouma,"Bakouma, Central African Republic",OWNO,////,////,????,"Bakouma, Mbomou (Mbömü), Central African Republic",OK
    BMG,BMG,KBMG,KBMG,OK,0.679,Bloomington,"Bloomington (IN), USA",OWNO,MONROE COUNTY,"BLOOMINGTON, IN - UNITED STATES",Monroe County Airport,"Bloomington, Indiana, United States",OK
    BMH,////,////,////,UNK,1.0,Bomai,"Bomai, Papua New Guinea",OWNO,////,////,????,"Bomai, Chimbu, Papua-New Guinea",OK
    BMI,BMI,KBMI,KBMI,OK,0.557,Bloomington-Normal,"Bloomington-Normal (IL), USA",OWNO,CENTRAL IL RGNL ARPT AT BLOOMINGTON-NORMAL,"BLOOMINGTON/NORMAL, IL - UNITED STATES",Central IL Regional Airport at Bloomington-Normal,"Bloomington/Normal, Illinois, United States",OK
    BMJ,////,////,SYBR,UNK,1.0,Baramita,"Baramita, Guyana",OWNO,////,////,????,"Baramita, Barima-Waini, Guyana",OK
    BMK,////,////,EDWR,UNK,1.0,Borkum,"Borkum, Germany",OWNO,////,////,????,"Borkum, Lower Saxony, Germany",OK
    BML,BML,KBML,KBML,OK,0.79,Berlin,"Berlin (NH), USA",OWNO,BERLIN RGNL,"BERLIN, NH - UNITED STATES",Berlin Regional,"Berlin, New Hampshire, United States",OK
    BMM,////,////,FOOB,UNK,1.0,Bitam,"Bitam, Gabon",OWNO,////,////,????,"Bitam, Woleu-Ntem, Gabon",OK
    BMN,////,////,ORBB,UNK,0.923,Bamerny,"Bamerny, Iraq",OWNO,////,////,????,"Bamarni, Iraq",OK
    BMO,////,////,VYBM,UNK,1.0,Bhamo,"Bhamo, Myanmar",OWNO,////,////,????,"Bhamo, Kachin, Myanmar (Burma)",OK
    BMP,////,////,YBPI,UNK,1.0,Brampton Island,"Brampton Island, Australia",OWNO,////,////,????,"Brampton Island, Queensland, Australia",OK
    BMR,////,////,EDWZ,UNK,1.0,Baltrum,"Baltrum, Germany",OWNO,////,////,????,"Baltrum, Lower Saxony, Germany",OK
    BMS,////,////,SNBU,UNK,0.37,Brumado,"Brumado, Brazil",OWNO,////,////,Socrates Mariani Bittencourt,"Brumado, Bahia, Brazil",OK
    BMT,BMT,KBMT,KBMT,OK,1.0,Beaumont Muni,"Beaumont (TX), USA",OWNO,BEAUMONT MUNI,"BEAUMONT, TX - UNITED STATES",Beaumont Municipal Airport,"Beaumont, Texas, United States",OK
    BMU,////,////,WADB,UNK,0.214,Bima,"Bima, Indonesia",OWNO,////,////,Sultan Muhammad Salahuddin International Airport,"Bima, Nusa Tenggara Barat, Indonesia",OK
    BMV,////,////,VVBM,UNK,1.0,Phung-Duc,"Banmethuot, Viet Nam",OWNO,////,////,Phung-Duc,"Buon Me Thuot, Vietnam",OK
    BMW,////,////,DATM,UNK,0.882,Bordj Badji Mokhtar,"Bordj Badji Mokhtar, Algeria",OWNO,////,////,Bordj Mokhtar Airport,"Bordj Mokhtar, Adrar, Algeria",MAYBE
    BMX,37AK,PABM,PABM,OK,1.0,Big Mountain,"Big Mountain (AK), USA",OWNO,BIG MOUNTAIN,"BIG MOUNTAIN, AK - UNITED STATES",????,"Big Mountain, Alaska, United States",OK
    BMY,////,////,NWWC,UNK,1.0,Belep Island,"Belep Island, New Caledonia",OWNO,////,////,Belep Islands Airport,"Waala, Île Art, Belep Islands, New Caledonia",OK
    BMZ,////,////,////,UNK,1.0,Bamu,"Bamu, Papua New Guinea",OWNO,////,////,????,"Bamu, Western, Papua-New Guinea",OK
    BNA,BNA,KBNA,KBNA,OK,0.618,Metropolitan,"Nashville (TN), USA",OWNO,NASHVILLE INTL,"NASHVILLE, TN - UNITED STATES",Nashville International Airport,"Nashville, Tennessee, United States",OK
    BNB,////,////,FZGN,UNK,1.0,Boende,"Boende, Congo (DRC)",OWNO,////,////,Boende Airport,"Boende, Équateur (Equator), Democratic Republic of Congo (Zaire)",OK
    BNC,////,////,FZNP,UNK,1.0,Beni,"Beni, Congo (DRC)",OWNO,////,////,Beni Airport,"Beni, Nord-Kivu (North Kivu), Democratic Republic of Congo (Zaire)",OK
    BND,////,////,OIKB,UNK,0.833,Bandar Abbas,"Bandar Abbas, Iran",OWNO,////,////,Bandar Abbas International Airport,"Bandar Abbas, Hormozgan, Iran",OK
    BNE,////,////,YBBN,UNK,1.0,International,"Brisbane, Australia",OWNO,////,////,Brisbane International Airport,"Brisbane, Queensland, Australia",OK
    BNF,BNF,////,////,OK,0.889,Warm Spring Bay SPB,"Baranof (AK), USA",OWNO,WARM SPRING BAY,"BARANOF, AK - UNITED STATES",Warm Spring Bay SPB,"Baranof, Alaska, United States",OK
    BNG,BNG,KBNG,KBNG,OK,0.771,Banning,"Banning (CA), USA",OWNO,BANNING MUNI,"BANNING, CA - UNITED STATES",Banning Municipal Airport,"Banning, California, United States",OK
    BNI,////,////,DNBE,UNK,1.0,Benin City,"Benin City, Nigeria",OWNO,////,////,Benin Airport,"Benin City, Edo, Nigeria",OK
    BNJ,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Bonn, North Rhine-Westphalia, Germany",UNK
    BNK,////,////,YBNA,UNK,1.0,Ballina,"Ballina, Australia",OWNO,////,////,????,"Ballina, New South Wales, Australia",OK
    BNL,BNL,KBNL,KBNL,OK,0.743,Barnwell County,"Barnwell (SC), USA",OWNO,BARNWELL RGNL,"BARNWELL, SC - UNITED STATES",Barnwell Regional,"Barnwell, South Carolina, United States",OK
    BNM,////,////,////,UNK,1.0,Bodinumu,"Bodinumu, Papua New Guinea",OWNO,////,////,????,"Bodinumu, Central, Papua-New Guinea",OK
    BNN,////,////,ENBN,UNK,1.0,Bronnoy,"Bronnoysund, Norway",OWNO,////,////,Brønnøy,"Brønnøysund, Norway",OK
    BNO,BNO,KBNO,KBNO,OK,0.774,Burns,"Burns (OR), USA",OWNO,BURNS MUNI,"BURNS, OR - UNITED STATES",Burns Municipal Airport,"Burns, Oregon, United States",OK
    BNP,////,////,OPBN,UNK,1.0,Bannu,"Bannu, Pakistan",OWNO,////,////,Bannu Airport,"Bannu, Khyber Pakhtunkhwa, Pakistan",OK

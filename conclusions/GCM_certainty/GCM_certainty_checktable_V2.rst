
List for checking certainty of Great Circle Mapper (VME - VYS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    VME,////,////,SAOR,UNK,0.757,Villa Mercedes,"Villa Mercedes, Argentina",OWNO,////,////,Pringles,"Villa Mercedes, San Luis, Argentina",OK
    VMI,////,////,////,UNK,0.462,INC,"Vallemi, Paraguay",OWNO,////,////,Dr. Juan Plate Airport,"Puerto Vallemi, Concepción, Paraguay",MAYBE
    VMU,////,////,AYBA,UNK,1.0,Baimuru,"Baimuru, Papua New Guinea",OWNO,////,////,????,"Baimuru, Gulf, Papua-New Guinea",OK
    VNA,////,////,VLSV,UNK,1.0,Saravane,"Saravane, Lao PDR",OWNO,////,////,????,"Saravane, Lao People's Democratic Republic (Laos)",OK
    VNC,VNC,KVNC,KVNC,OK,1.0,Venice Municipal Airport,"Venice (FL), USA",OWNO,VENICE MUNI,"VENICE, FL - UNITED STATES",Venice Municipal Airport,"Venice, Florida, United States",OK
    VNE,////,////,LFRV,UNK,1.0,Meucon,"Vannes, France",OWNO,////,////,Meucon,"Vannes, Bretagne, France",OK
    VNO,////,////,EYVI,UNK,0.737,Vilnius,"Vilnius, Lithuania",OWNO,////,////,Vilnius International Airport,"Vilnius, Lithuania",OK
    VNR,////,////,YVRS,UNK,0.632,Vanrook,"Vanrook, Australia",OWNO,////,////,????,"Vanrook Stations, Queensland, Australia",MAYBE
    VNS,////,////,VIBN,UNK,1.0,Varanasi,"Varanasi, India",OWNO,////,////,????,"Varanasi, Uttar Pradesh, India",OK
    VNX,////,////,FQVL,UNK,1.0,Vilanculos,"Vilanculos, Mozambique",OWNO,////,////,????,"Vilankulo, Mozambique",OK
    VNY,VNY,KVNY,KVNY,OK,1.0,Van Nuys,"Los Angeles (CA), USA",OWNO,VAN NUYS,"VAN NUYS, CA - UNITED STATES",????,"Van Nuys, California, United States",OK
    VOG,////,////,URWW,UNK,0.818,Volgograd,"Volgograd, Russia",OWNO,////,////,Volgograd International Airport,"Volgograd, Volgogradskaya, Russian Federation (Russia)",OK
    VOH,////,////,FMNV,UNK,0.833,Vohemar,"Vohemar, Madagascar",OWNO,////,////,????,"Vohimarina, Madagascar",TO DO CHECK
    VOI,////,////,GLVA,UNK,1.0,Voinjama,"Voinjama, Liberia",OWNO,////,////,????,"Voinjama, Liberia",OK
    VOK,VOK,KVOK,KVOK,OK,1.0,Volk Field,"Camp Douglas (WI), USA",OWNO,VOLK FIELD,"CAMP DOUGLAS, WI - UNITED STATES",Volk Field,"Camp Douglas, Wisconsin, United States",OK
    VOL,////,////,LGBL,UNK,0.815,Nea Anchialos National Airport,"Volos, Greece",OWNO,////,////,Nea Anchialos,"Volos, Thessalía (Thessaly), Greece",OK
    VOT,////,////,SDVG,UNK,0.541,Votuporanga,"Votuporanga, Brazil",OWNO,////,////,Domingos Pignatari,"Votuporanga, São Paulo, Brazil",OK
    VOZ,////,////,UUOO,UNK,0.571,Voronezh,"Voronezh, Russia",OWNO,////,////,Chertovitskoye Airport,"Voronezh, Voronezhskaya, Russian Federation (Russia)",OK
    VPE,////,////,FNGI,UNK,1.0,Ongiva,"Ongiva, Angola",OWNO,////,////,????,"Ongiva, Angola",OK
    VPG,////,////,////,UNK,0.467,Vipingo,Vipingo,IATA,////,////,Vipingo Estate Airstrip,"Vipingo, Kenya",MAYBE
    VPN,////,////,BIVO,UNK,1.0,Vopnafjordur,"Vopnafjordur, Iceland",OWNO,////,////,????,"Vopnafjörður, Iceland",OK
    VPS,VPS,KVPS,KVPS,OK,0.629,Ft. Walton Beach,"Valparaiso (FL), USA",OWNO,EGLIN AFB/DESTIN-FT WALTON BEACH,"VALPARAISO/DESTIN-FT WALTON BEACH, FL - UNITED STATES",Eglin AFB,"Valparaiso, Florida, United States",OK
    VPY,////,////,FQCH,UNK,1.0,Chimoio,"Chimoio, Mozambique",OWNO,////,////,????,"Chimoio, Mozambique",OK
    VPZ,VPZ,KVPZ,KVPZ,OK,0.885,Porter County,"Valparaiso (IN), USA",OWNO,PORTER COUNTY RGNL,"VALPARAISO, IN - UNITED STATES",Porter County Regional,"Valparaiso, Indiana, United States",OK
    VQS,VQS,TJVQ,TJVQ,OK,0.089,Vieques,"Vieques, Puerto Rico",OWNO,ANTONIO RIVERA RODRIGUEZ,"ISLA DE VIEQUES, PR - UNITED STATES",Antonio Rivera Rodriguez Airport,"Isla De Vieques, Puerto Rico, United States",MAYBE
    VRA,////,////,MUVR,UNK,0.939,Juan Gualberto Gomez,"Varadero, Cuba",OWNO,////,////,Juan Gualberto Gómez International Airport,"Varadero, Matanzas, Cuba",OK
    VRB,VRB,KVRB,KVRB,OK,1.0,Municipal,"Vero Beach (FL), USA",OWNO,VERO BEACH MUNI,"VERO BEACH, FL - UNITED STATES",Vero Beach Municipal Airport,"Vero Beach, Florida, United States",OK
    VRC,////,////,RPUV,UNK,1.0,Virac,"Virac, Philippines",OWNO,////,////,????,"Virac, Catanduanes Island, Philippines",OK
    VRE,////,////,FAVR,UNK,1.0,Vredendal,"Vredendal, South Africa",OWNO,////,////,Vredendal Airport,"Vredendal, Western Cape, South Africa",OK
    VRI,////,////,ULDW,UNK,nan,////,////,////,////,////,Varandey Airport,"Varandey, Nenetskiy, Russian Federation (Russia)",UNK
    VRK,////,////,EFVR,UNK,1.0,Varkaus,"Varkaus, Finland",OWNO,////,////,????,"Varkaus, Etelä-Savo (Södra Savolax (Southern Savonia)), Finland",OK
    VRL,////,////,LPVR,UNK,1.0,Vila Real,"Vila Real, Portugal",OWNO,////,////,????,"Vila Real, Vila Real, Portugal",OK
    VRN,////,////,LIPX,UNK,0.714,Verona,"Verona, Italy",OWNO,////,////,Villafranca,"Verona, Veneto, Italy",OK
    VRS,3VS,////,////,OK,0.332,Versailles,"Versailles (MO), USA",OWNO,ROY OTTEN MEMORIAL AIRFIELD,"VERSAILLES, MO - UNITED STATES",Roy Otten Memorial Airfield,"Versailles, Missouri, United States",OK
    VRU,////,////,FAVB,UNK,1.0,Vryburg,"Vryburg, South Africa",OWNO,////,////,Vryburg Airport,"Vryburg, North West, South Africa",OK
    VRY,////,////,ENVR,UNK,1.0,Stolport,"Vaeroy, Norway",OWNO,////,////,STOLport,"Værøy, Norway",OK
    VSA,////,////,MMVA,UNK,0.694,Capitan Carlos Perez,"Villahermosa, Mexico",OWNO,////,////,C.P.A. Carlos Rovirosa Pérez International Airport,"Villahermosa, Tabasco, México",OK
    VSE,////,////,LPVZ,UNK,0.37,Viseu,"Viseu, Portugal",OWNO,////,////,Gonçalves Lobato,"Viseu, Viseu, Portugal",OK
    VSF,VSF,KVSF,KVSF,OK,0.766,State,"Springfield (VT), USA",OWNO,HARTNESS STATE (SPRINGFIELD),"SPRINGFIELD, VT - UNITED STATES",Hartness State Airport,"Springfield, Vermont, United States",OK
    VSG,////,////,UKCW,UNK,1.0,Lugansk,"Lugansk, Ukraine",OWNO,////,////,????,"Lugansk, Luhansk, Ukraine",OK
    VST,////,////,ESOW,UNK,0.706,Hasslo,"Vasteras, Sweden",OWNO,////,////,Västerås,"Stockholm, Västmanlands län, Sweden",OK
    VTB,////,////,UMII,UNK,0.609,Vitebsk,"Vitebsk, Belarus",OWNO,////,////,Vitebsk Vostochny Airport,"Vitebsk, Vitsyebskaya (Vitebsk), Belarus",OK
    VTE,////,////,VLVT,UNK,0.865,Wattay,"Vientiane, Lao PDR",OWNO,////,////,Wattay International Airport,"Vientiane, Lao People's Democratic Republic (Laos)",OK
    VTF,////,////,NFVL,UNK,1.0,Vatulele,"Vatulele, Fiji",OWNO,////,////,????,"Vatulele, Fiji",OK
    VTG,////,////,VVVT,UNK,1.0,Vung Tau,"Vung Tau, Viet Nam",OWNO,////,////,????,"Vung Tau, Vietnam",OK
    VTL,////,////,LFSZ,UNK,0.414,Vittel,"Vittel, France",OWNO,////,////,Champ de Courses,"Vittel, Lorraine, France",OK
    VTN,VTN,KVTN,KVTN,OK,1.0,Miller Field,"Valentine (NE), USA",OWNO,MILLER FIELD,"VALENTINE, NE - UNITED STATES",Miller Field,"Valentine, Nebraska, United States",OK
    VTS,////,////,EVVA,UNK,nan,////,////,////,////,////,Ventspils International Airport,"Ventspils, Latvia",UNK
    VTU,////,////,MUVT,UNK,0.474,Las Tunas,"Las Tunas, Cuba",OWNO,////,////,Hermanos Ameijeiras,"Las Tunas, Las Tunas, Cuba",OK
    VTZ,////,////,VEVZ,UNK,1.0,Vishakhapatnam,"Vishakhapatnam, India",OWNO,////,////,????,"Vishakhapatnam, Andhra Pradesh, India",OK
    VUP,////,////,SKVP,UNK,0.474,Valledupar,"Valledupar, Colombia",OWNO,////,////,Alfonso López Pumarejo Airport,"Valledupar, Cesar, Colombia",OK
    VUS,////,////,ULWU,UNK,0.889,Velikij Ustyug,"Velikij Ustyug, Russia",OWNO,////,////,Veliky Ustyug Airport,"Veliky Ustyug, Vologodskaya, Russian Federation (Russia)",OK
    VUU,////,////,////,UNK,1.0,Mvuu Camp,Liwonde,IATA,////,////,Mvuu Camp Airport,"Liwonde, Malawi",MAYBE
    VVB,////,////,FMMH,UNK,1.0,Mahanoro,"Mahanoro, Madagascar",OWNO,////,////,Mahanoro Airport,"Mahanoro, Madagascar",OK
    VVC,////,////,SKVV,UNK,0.947,La Vanguardia,"Villavicencio, Colombia",OWNO,////,////,Vanguardia Airport,"Villavicencio, Meta, Colombia",OK
    VVI,////,////,SLVR,UNK,1.0,Viru Viru International,"Santa Cruz, Bolivia",OWNO,////,////,Viru Viru International Airport,"Santa Cruz, Andrés Ibáñez, Santa Cruz, Bolivia",OK
    VVK,////,////,ESSW,UNK,1.0,Vastervik,"Vastervik, Sweden",OWNO,////,////,????,"Västervik, Sweden",OK
    VVO,////,////,UHWW,UNK,0.786,Vladivostok,"Vladivostok, Russia",OWNO,////,////,Knevichi Airport,"Vladivostok, Primorskiy, Russian Federation (Russia)",OK
    VVZ,////,////,DAAP,UNK,1.0,Illizi,"Illizi, Algeria",OWNO,////,////,????,"Illizi, Illizi, Algeria",OK
    VXC,////,////,FQLC,UNK,1.0,Lichinga,"Lichinga, Mozambique",OWNO,////,////,????,"Lichinga, Mozambique",OK
    VXE,////,////,GVSV,UNK,0.897,San Pedro,"Sao Vicente, Cape Verde",OWNO,////,////,São Pedro,"São Pedro, São Vicente, Cape Verde",OK
    VXO,////,////,ESMX,UNK,0.556,Vaxjo,"Vaxjo, Sweden",OWNO,////,////,Växjö-Smaland Airport,"Växjö, Småland, Kronobergs län, Sweden",OK
    VYD,////,////,FAVY,UNK,1.0,Vryheid,"Vryheid, South Africa",OWNO,////,////,????,"Vryheid, KwaZulu-Natal, South Africa",OK
    VYI,////,////,UENW,UNK,nan,////,////,////,////,////,Vilyuisk Airport,"Vilyuisk, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    VYS,VYS,KVYS,KVYS,OK,0.482,Illinois Valley Regnl,"Peru (IL), USA",OWNO,ILLINOIS VALLEY RGNL-WALTER A DUNCAN FIELD,"PERU, IL - UNITED STATES",Illinois Valley Regional-Walter A Duncan Field,"Peru, Illinois, United States",OK

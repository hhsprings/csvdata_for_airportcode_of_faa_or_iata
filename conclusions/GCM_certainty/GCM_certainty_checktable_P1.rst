
List for checking certainty of Great Circle Mapper (PAA - PGO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    PAA,////,////,VYPA,UNK,1.0,Pa-an,"Pa-an, Myanmar",OWNO,////,////,????,"Pa-an, Kayin, Myanmar (Burma)",OK
    PAB,////,////,VABI,UNK,1.0,Bilaspur,"Bilaspur, India",OWNO,////,////,????,"Bilaspur, Chhattisgarh, India",OK
    PAC,////,////,MPMG,UNK,0.69,Paitilla,"Panama City, Panama",OWNO,////,////,Paitilla-Marcos A. Gelabert,"Panamá City, Panamá, Panamá",OK
    PAD,////,////,EDLP,UNK,1.0,Paderborn Lippstadt Airport,"Paderborn, Germany",OWNO,////,////,????,"Paderborn/Lippstadt, North Rhine-Westphalia, Germany",OK
    PAE,PAE,KPAE,KPAE,OK,1.0,Snohomish County,"Everett (WA), USA",OWNO,SNOHOMISH COUNTY (PAINE FLD),"EVERETT, WA - UNITED STATES",Snohomish County Airport,"Everett, Washington, United States",OK
    PAF,////,////,HUPA,UNK,1.0,Pakuba,"Pakuba, Uganda",OWNO,////,////,????,"Pakuba, Uganda",OK
    PAG,////,////,RPMP,UNK,1.0,Pagadian,"Pagadian, Philippines",OWNO,////,////,????,"Pagadian, Mindanao, Philippines",OK
    PAH,PAH,KPAH,KPAH,OK,1.0,Barkley Regional,"Paducah (KY), USA",OWNO,BARKLEY RGNL,"PADUCAH, KY - UNITED STATES",Barkley Regional,"Paducah, Kentucky, United States",OK
    PAJ,////,////,OPPC,UNK,0.588,Para Chinar,"Para Chinar, Pakistan",OWNO,////,////,????,"Parachinar, Federally Administered Tribal Areas, Pakistan",OK
    PAK,PAK,PHPA,PHPA,OK,1.0,Port Allen,"Hanapepe (HI), USA",OWNO,PORT ALLEN,"HANAPEPE, HI - UNITED STATES",Port Allen Airport,"Hanapepe, Kauai, Hawaii, United States",OK
    PAL,////,////,SKPQ,UNK,0.485,Palanquero,"Palanquero, Colombia",OWNO,////,////,Germán Olano Moreno AB,"Palanquero, Cundinamarca, Colombia",OK
    PAM,PAM,KPAM,KPAM,OK,1.0,Tyndall AFB,"Panama City (FL), USA",OWNO,TYNDALL AFB,"PANAMA CITY, FL - UNITED STATES",Tyndall AFB,"Panama City, Florida, United States",OK
    PAN,////,////,VTSK,UNK,1.0,Pattani,"Pattani, Thailand",OWNO,////,////,????,"Pattani, Pattani, Thailand",OK
    PAO,PAO,KPAO,KPAO,OK,1.0,Palo Alto,"Palo Alto (CA), USA",OWNO,PALO ALTO,"PALO ALTO, CA - UNITED STATES",????,"Palo Alto, California, United States",OK
    PAP,////,////,MTPP,UNK,0.636,Mais Gate,"Port Au Prince, Haiti",OWNO,////,////,Toussaint Louverture International Airport,"Port-au-Prince, Haiti",OK
    PAQ,PAQ,PAAQ,PAAQ,OK,1.0,Municipal,"Palmer (AK), USA",OWNO,PALMER MUNI,"PALMER, AK - UNITED STATES",Palmer Municipal Airport,"Palmer, Alaska, United States",OK
    PAR,////,////,////,UNK,1.0,Metropolitan Area,"Paris, France",OWNO,////,////,Metropolitan Area,"Paris, Île-de-France, France",OK
    PAS,////,////,LGPA,UNK,1.0,Paros,"Paros, Greece",OWNO,////,////,????,"Paros, Notío Aigaío (Southern Aegean), Greece",OK
    PAT,////,////,VEPT,UNK,0.276,Patna,"Patna, India",OWNO,////,////,Lok Nayak Jaya Prakash Narayan,"Patna, Bihar, India",OK
    PAU,////,////,VYPK,UNK,1.0,Pauk,"Pauk, Myanmar",OWNO,////,////,????,"Pauk, Magway, Myanmar (Burma)",OK
    PAV,////,////,SBUF,UNK,1.0,Paulo Afonso,"Paulo Afonso, Brazil",OWNO,////,////,????,"Paulo Afonso, Bahia, Brazil",OK
    PAW,////,////,////,UNK,1.0,Pambwa,"Pambwa, Papua New Guinea",OWNO,////,////,????,"Pambwa, Milne Bay, Papua-New Guinea",OK
    PAX,////,////,MTPX,UNK,1.0,Port De Paix,"Port De Paix, Haiti",OWNO,////,////,????,"Port-de-Paix, Haiti",OK
    PAY,////,////,WBKP,UNK,1.0,Pamol,"Pamol, Malaysia",OWNO,////,////,????,"Pamol, Sabah, Malaysia",OK
    PAZ,////,////,MMPA,UNK,0.743,Tajin,"Poza Rica, Mexico",OWNO,////,////,El Tajín National,"Poza Rica, Veracruz, México",OK
    PBB,////,////,SSPN,UNK,1.0,Paranaiba,"Paranaiba, Brazil",OWNO,////,////,????,"Paranaiba, Mato Grosso do Sul, Brazil",OK
    PBC,////,////,MMPB,UNK,0.578,Huejotsingo,"Puebla, Mexico",OWNO,////,////,Hermanos Serdán International Airport,"Puebla, Puebla, México",OK
    PBD,////,////,VAPR,UNK,1.0,Porbandar,"Porbandar, India",OWNO,////,////,????,"Porbandar, Gujarat, India",OK
    PBE,////,////,SKPR,UNK,1.0,Puerto Berrio,"Puerto Berrio, Colombia",OWNO,////,////,Puerto Berrio Airport,"Puerto Berrio, Antioquia, Colombia",OK
    PBF,PBF,KPBF,KPBF,OK,1.0,Grider Field,"Pine Bluff (AR), USA",OWNO,GRIDER FIELD,"PINE BLUFF, AR - UNITED STATES",Grider Field,"Pine Bluff, Arkansas, United States",OK
    PBG,PBG,KPBG,KPBG,OK,0.734,AFB,"Plattsburgh (NY), USA",OWNO,PLATTSBURGH INTL,"PLATTSBURGH, NY - UNITED STATES",Plattsburgh International Airport,"Plattsburgh, New York, United States",OK
    PBH,////,////,VQPR,UNK,0.615,Paro,"Paro, Bhutan",OWNO,////,////,Paro International Airport,"Paro, Bhutan",OK
    PBI,PBI,KPBI,KPBI,OK,1.0,International,"West Palm Beach (FL), USA",OWNO,PALM BEACH INTL,"WEST PALM BEACH, FL - UNITED STATES",Palm Beach International Airport,"West Palm Beach, Florida, United States",OK
    PBJ,////,////,NVSI,UNK,0.588,Paama,"Paama, Vanuatu",OWNO,////,////,????,"Tavie, Paama Island, Malampa, Vanuatu",MAYBE
    PBL,////,////,SVPC,UNK,0.571,Puerto Cabello,"Puerto Cabello, Venezuela",OWNO,////,////,Bartolome Salom International Airport,"Puerto Cabello, Carabobo, Venezuela",OK
    PBM,////,////,SMJP,UNK,0.64,Zanderij International,"Paramaribo, Suriname",OWNO,////,////,Johan Adolf Pengel International Airport,"Paramaribo, Para, Suriname",OK
    PBN,////,////,FNPA,UNK,1.0,Porto Amboim,"Porto Amboim, Angola",OWNO,////,////,????,"Porto Amboim, Angola",OK
    PBO,////,////,YPBO,UNK,1.0,Paraburdoo,"Paraburdoo, Australia",OWNO,////,////,????,"Paraburdoo, Western Australia, Australia",OK
    PBP,////,////,MRIA,UNK,1.0,Punta Islita,"Punta Islita, Costa Rica",OWNO,////,////,????,"Punta Islita, Guanacaste, Costa Rica",OK
    PBQ,////,////,SWPM,UNK,1.0,Pimenta Bueno,"Pimenta Bueno, Brazil",OWNO,////,////,????,"Pimenta Bueno, Rondônia, Brazil",OK
    PBR,////,////,MGPB,UNK,1.0,Puerto Barrios,"Puerto Barrios, Guatemala",OWNO,////,////,????,"Puerto Barrios, Izabal, Guatemala",OK
    PBU,////,////,VYPT,UNK,1.0,Putao,"Putao, Myanmar",OWNO,////,////,????,"Putao, Kachin, Myanmar (Burma)",OK
    PBZ,////,////,FAPG,UNK,1.0,Plettenberg Bay,"Plettenberg Bay, South Africa",OWNO,////,////,Plettenberg Bay Airport,"Plettenberg Bay, Western Cape, South Africa",OK
    PCA,A14,PAOC,PAOC,OK,1.0,Portage Creek,"Portage Creek (AK), USA",OWNO,PORTAGE CREEK,"PORTAGE CREEK, AK - UNITED STATES",????,"Portage Creek, Alaska, United States",OK
    PCB,////,////,WIHP,UNK,0.48,Pondok Cabe,"Pondok Cabe, Indonesia",OWNO,////,////,????,"Depok (Tangerang, Jakarta)) [Pondok Cabe Airport], Jawa Barat, Indonesia",MAYBE
    PCC,////,////,////,UNK,1.0,Puerto Rico,"Puerto Rico, Colombia",OWNO,////,////,????,"Puerto Rico, Caquetá, Colombia",OK
    PCD,PDC,KPDC,KPDC,OK,1.0,Municipal,"Prairie Du Chien (WI), USA",OWNO,PRAIRIE DU CHIEN MUNI,"PRAIRIE DU CHIEN, WI - UNITED STATES",Prairie Du Chien Municipal Airport,"Prairie Du Chien, Wisconsin, United States",OK
    PCH,////,////,////,UNK,1.0,Palacios,"Palacios, Honduras",OWNO,////,////,????,"Palacios, Copán, Honduras",OK
    PCL,////,////,SPCL,UNK,0.515,Capitan Rolden,"Pucallpa, Peru",OWNO,////,////,Capitán FAP David Armando Abensur Rengifo International Airport,"Pucallpa, Ucayali, Perú",OK
    PCM,////,////,////,UNK,1.0,Playa del Carmen,"Playa del Carmen, Mexico",OWNO,////,////,????,"Playa del Carmen, Quintana Roo, México",OK
    PCN,////,////,NZPN,UNK,0.6,Koromiko,"Picton, New Zealand",OWNO,////,////,????,"Picton, New Zealand",OK
    PCO,////,////,////,UNK,1.0,Punta Colorada,"Punta Colorada, Mexico",OWNO,////,////,Punta Colorada Airport,"La Ribera, Baja California Sur, México",OK
    PCP,////,////,FPPR,UNK,1.0,Principe,"Principe, Sao Tome and Principe",OWNO,////,////,Príncipe Airport,"Santo António, Príncipe, São Tomé and Príncipe",OK
    PCR,////,////,SKPC,UNK,0.683,Puerto Carreno,"Puerto Carreno, Colombia",OWNO,////,////,German Olano Airport,"Puerto Carreño, Vichada, Colombia",OK
    PCS,////,////,SNPC,UNK,1.0,Picos,"Picos, Brazil",OWNO,////,////,????,"Picos, Piauí, Brazil",OK
    PCT,39N,////,////,OK,1.0,Princeton,"Princeton (NJ), USA",OWNO,PRINCETON,"PRINCETON/ROCKY HILL, NJ - UNITED STATES",Princeton Airport,"Princeton/Rocky Hill, New Jersey, United States",OK
    PCV,////,////,////,UNK,1.0,Punta Chivato,"Punta Chivato, Mexico",OWNO,////,////,????,"Punta Chivato, Baja California Sur, México",OK
    PDA,////,////,SKPD,UNK,0.815,Puerto Inirida,"Puerto Inirida, Colombia",OWNO,////,////,Obando Airport,"Puerto Inirida, Guainía, Colombia",OK
    PDB,4K0,////,////,OK,1.0,Pedro Bay,"Pedro Bay (AK), USA",OWNO,PEDRO BAY,"PEDRO BAY, AK - UNITED STATES",????,"Pedro Bay, Alaska, United States",OK
    PDC,////,////,NWWQ,UNK,1.0,Mueo,"Mueo, New Caledonia",OWNO,////,////,????,"Mueo, New Caledonia",OK
    PDE,////,////,YPDI,UNK,1.0,Pandie Pandie,"Pandie Pandie, Australia",OWNO,////,////,????,"Pandie Pandie, South Australia, Australia",OK
    PDF,////,////,SNRD,UNK,1.0,Prado,"Prado, Brazil",OWNO,////,////,????,"Prado, Bahia, Brazil",OK
    PDG,////,////,WIPT,UNK,1.0,Minangkabau International Airport,"Padang, West Sumatra, Indonesia",OWNO,////,////,Minangkabau International Airport,"Padang, Sumatera Barat, Indonesia",OK
    PDI,////,////,////,UNK,1.0,Pindiu,"Pindiu, Papua New Guinea",OWNO,////,////,????,"Pindiu, Morobe, Papua-New Guinea",OK
    PDK,PDK,KPDK,KPDK,OK,0.769,De Kalb/Peachtree,"Atlanta (GA), USA",OWNO,DEKALB-PEACHTREE,"ATLANTA, GA - UNITED STATES",Dekalb-Peachtree Airport,"Atlanta, Georgia, United States",OK
    PDL,////,////,LPPD,UNK,0.462,Nordela,"Ponta Delgada, Portugal",OWNO,////,////,João Paulo II Airport,"São Miguel Island, Região Autónoma dos Açores (Azores), Portugal",TO DO CHECK
    PDM,////,////,////,UNK,0.739,Capt. J. Montenegro,Pedasi,IATA,////,////,Captain Justiniano Montenegro,"Pedasi, Los Santos, Panamá",MAYBE
    PDN,////,////,////,UNK,1.0,Parndana,"Parndana, Australia",OWNO,////,////,????,"Parndana, Kangaroo Island, South Australia, Australia",OK
    PDO,////,////,WIPQ,UNK,1.0,Pendopo,"Pendopo, Indonesia",OWNO,////,////,????,"Pendopo, Sumatera Selatan, Indonesia",OK
    PDP,////,////,SULS,UNK,0.318,Punta Del Este,"Punta Del Este, Uruguay",OWNO,////,////,Capitán Corbeta C.A. Curbelo International Airport,"Maldonado, Maldonado, Uruguay",TO DO CHECK
    PDR,////,////,SJKE,UNK,0.762,Municipal,"Presidente Dutra, Brazil",OWNO,////,////,Presidente José Sarney,"Presidente Dutra, Maranhão, Brazil",OK
    PDS,////,////,MMPG,UNK,0.848,Piedras Negras,"Piedras Negras, Mexico",OWNO,////,////,Piedras Negras International Airport,"Piedras Negras, Coahuila, México",OK
    PDT,PDT,KPDT,KPDT,OK,0.445,Pendleton,"Pendleton (OR), USA",OWNO,EASTERN OREGON RGNL AT PENDLETON,"PENDLETON, OR - UNITED STATES",Eastern Oregon Regional at Pendleton,"Pendleton, Oregon, United States",OK
    PDU,////,////,SUPU,UNK,0.414,Paysandu,"Paysandu, Uruguay",OWNO,////,////,Tydeo Larre Borges International Airport,"Paysandú, Paysandú, Uruguay",OK
    PDV,////,////,LBPD,UNK,0.778,Plovdiv,"Plovdiv, Bulgaria",OWNO,////,////,Plovdiv International Airport,"Plovdiv, Plovdiv, Bulgaria",OK
    PDX,PDX,KPDX,KPDX,OK,0.891,Portland OR International,"Portland (OR), USA",OWNO,PORTLAND INTL,"PORTLAND, OR - UNITED STATES",Portland International Airport,"Portland, Oregon, United States",OK
    PDZ,////,////,SVPE,UNK,1.0,Pedernales,"Pedernales, Venezuela",OWNO,////,////,????,"Pedernales, Delta Amacuro, Venezuela",OK
    PEA,////,////,YPSH,UNK,1.0,Penneshaw,"Penneshaw, Australia",OWNO,////,////,????,"Penneshaw, Kangaroo Island, South Australia, Australia",OK
    PEB,////,////,////,UNK,1.0,Pebane,"Pebane, Mozambique",OWNO,////,////,????,"Pebane, Mozambique",OK
    PEC,PEC,////,////,OK,0.805,Pelican SPB,"Pelican (AK), USA",OWNO,PELICAN,"PELICAN, AK - UNITED STATES",Pelican SPB,"Pelican, Alaska, United States",OK
    PED,////,////,LKPD,UNK,1.0,Pardubice,"Pardubice, Czech Republic",OWNO,////,////,Pardubice Airport,"Pardubice, Pardubice, Czech Republic",OK
    PEE,////,////,USPP,UNK,0.364,Perm,"Perm, Russia",OWNO,////,////,Bolshoye Savino International Airport,"Perm, Permskiy, Russian Federation (Russia)",OK
    PEF,////,////,EDCP,UNK,1.0,Peenemuende,"Peenemuende, Germany",OWNO,////,////,????,"Peenemünde, Mecklenburg-Vorpommern, Germany",OK
    PEG,////,////,LIRZ,UNK,0.571,Sant Egidio,"Perugia, Italy",OWNO,////,////,Perugia San Francesco d'Assisi,"Perugia, Umbria, Italy",OK
    PEH,////,////,SAZP,UNK,0.452,Pehuajo,"Pehuajo, Argentina",OWNO,////,////,Comodoro P. Zanni,"Pehuajo, Argentina",OK
    PEI,////,////,SKPE,UNK,0.865,Matecana,"Pereira, Colombia",OWNO,////,////,Matecaña International Airport,"Pereira, Risaralda, Colombia",OK
    PEK,////,////,ZBAA,UNK,0.867,Beijing Capital International,"Beijing, PR China",OWNO,////,////,Capital Airport,"Beijing, Beijing, China",OK
    PEL,////,////,FXPG,UNK,1.0,Pelaneng,"Pelaneng, Lesotho",OWNO,////,////,????,"Pelaneng, Lesotho",OK
    PEM,////,////,SPTU,UNK,0.714,Puerto Maldonado,"Puerto Maldonado, Peru",OWNO,////,////,Padre Aldamiz International Airport,"Puerto Maldonado, Madre de Dios, Perú",OK
    PEN,////,////,WMKP,UNK,1.0,Penang International,"Penang, Malaysia",OWNO,////,////,Penang International Airport,"George Town, Penang (Pahang), Malaysia",OK
    PEP,////,////,////,UNK,1.0,Peppimenarti,"Peppimenarti, Australia",OWNO,////,////,????,"Peppimenarti, Northern Territory, Australia",OK
    PEQ,PEQ,KPEQ,KPEQ,OK,0.694,Pecos City,"Pecos City (TX), USA",OWNO,PECOS MUNI,"PECOS, TX - UNITED STATES",Pecos Municipal Airport,"Pecos, Texas, United States",OK
    PER,////,////,YPPH,UNK,0.714,Perth,"Perth, Australia",OWNO,////,////,Perth International Airport,"Perth, Western Australia, Australia",OK
    PES,////,////,ULPB,UNK,0.75,Petrozavodsk,"Petrozavodsk, Russia",OWNO,////,////,Besovets Airport,"Petrozavodsk, Kareliya, Russian Federation (Russia)",OK
    PET,////,////,SBPK,UNK,0.667,Federal,"Pelotas, Brazil",OWNO,////,////,Pelotas International Airport,"Pelotas, Rio Grande do Sul, Brazil",OK
    PEU,////,////,MHPL,UNK,1.0,Puerto Lempira,"Puerto Lempira, Honduras",OWNO,////,////,????,"Puerto Lenpira, Gracias a Dios, Honduras",OK
    PEV,////,////,LHPP,UNK,nan,////,////,////,////,////,Pécs-Pogány International Airport,"Pécs, Baranya, Hungary",UNK
    PEW,////,////,OPPS,UNK,0.5,Peshawar,"Peshawar, Pakistan",OWNO,////,////,Bacha Khan International Airport,"Peshawar, Federally Administered Tribal Areas, Pakistan",OK
    PEX,////,////,UUYP,UNK,1.0,Pechora,"Pechora, Russia",OWNO,////,////,Pechora Airport,"Pechora, Komi, Russian Federation (Russia)",OK
    PEY,////,////,YPNG,UNK,1.0,Penong,"Penong, Australia",OWNO,////,////,????,"Penong, South Australia, Australia",OK
    PEZ,////,////,UWPP,UNK,1.0,Penza,"Penza, Russia",OWNO,////,////,Penza Airport,"Penza, Penzenskaya, Russian Federation (Russia)",OK
    PFB,////,////,SBPF,UNK,0.647,Passo Fundo,"Passo Fundo, Brazil",OWNO,////,////,Lauro Kurtz,"Passo Fundo, Rio Grande do Sul, Brazil",OK
    PFC,PFC,KPFC,KPFC,OK,1.0,tate,"Pacific City (OR), USA",OWNO,PACIFIC CITY STATE,"PACIFIC CITY, OR - UNITED STATES",Pacific City State Airport,"Pacific City, Oregon, United States",OK
    PFJ,////,////,BIPA,UNK,1.0,Patreksfjordur,"Patreksfjordur, Iceland",OWNO,////,////,????,"Patreksfjörður, Iceland",OK
    PFO,////,////,LCPH,UNK,1.0,International,"Paphos, Cyprus",OWNO,////,////,Paphos International Airport,"Paphos, Cyprus",OK
    PFQ,////,////,OITP,UNK,nan,////,////,////,////,////,Parsabad-Moghan,"Parsabad, Azarbayjan-e Sharqi, Iran",UNK
    PFR,////,////,FZVS,UNK,1.0,Ilebo,"Ilebo, Congo (DRC)",OWNO,////,////,????,"Ilebo, Kasai-Occidental (West Kasai), Democratic Republic of Congo (Zaire)",OK
    PGA,PGA,KPGA,KPGA,OK,0.694,Page,"Page (AZ), USA",OWNO,PAGE MUNI,"PAGE, AZ - UNITED STATES",Page Municipal Airport,"Page, Arizona, United States",OK
    PGB,////,////,////,UNK,1.0,Pangoa,"Pangoa, Papua New Guinea",OWNO,////,////,????,"Pangoa, Western, Papua-New Guinea",OK
    PGC,W99,////,////,OK,1.0,Grant County,"Petersburg (WV), USA",OWNO,GRANT COUNTY,"PETERSBURG, WV - UNITED STATES",Grant County Airport,"Petersburg, West Virginia, United States",OK
    PGD,PGD,KPGD,KPGD,OK,0.529,Charlotte County,"Punta Gorda (FL), USA",OWNO,PUNTA GORDA,"PUNTA GORDA, FL - UNITED STATES",????,"Punta Gorda, Florida, United States",OK
    PGE,////,////,////,UNK,1.0,Yegepa,"Yegepa, Papua New Guinea",OWNO,////,////,????,"Yegepa, Morobe, Papua-New Guinea",OK
    PGF,////,////,LFMP,UNK,1.0,Llabanere,"Perpignan, France",OWNO,////,////,Llabanere,"Perpignan/Rivesaltes, Languedoc-Roussillon, France",OK
    PGH,////,////,VIPT,UNK,1.0,Pantnagar,"Pantnagar, India",OWNO,////,////,????,"Pantnagar, Uttarakhand, India",OK
    PGI,////,////,FNCH,UNK,1.0,Chitato,"Chitato, Angola",OWNO,////,////,????,"Chitato, Angola",OK
    PGK,////,////,WIPK,UNK,0.722,Pangkalpinang,"Pangkalpinang, Indonesia",OWNO,////,////,Depati Amir Airport,"Pangkal Pinang, Sumatera Selatan, Indonesia",OK
    PGL,PQL,KPQL,KPQL,OK,0.518,Jackson County,"Pascagoula (MS), USA",OWNO,TRENT LOTT INTL,"PASCAGOULA, MS - UNITED STATES",Trent Lott International Airport,"Pascagoula, Mississippi, United States",OK
    PGM,PGM,////,////,OK,1.0,Port Graham,"Port Graham (AK), USA",OWNO,PORT GRAHAM,"PORT GRAHAM, AK - UNITED STATES",????,"Port Graham, Alaska, United States",OK
    PGN,////,////,////,UNK,1.0,Pangia,"Pangia, Papua New Guinea",OWNO,////,////,????,"Pangia, Southern Highlands, Papua-New Guinea",OK
    PGO,PSO,KPSO,KPSO,OK,1.0,tevens Field,"Pagosa Springs (CO), USA",OWNO,STEVENS FIELD,"PAGOSA SPRINGS, CO - UNITED STATES",Stevens Field,"Pagosa Springs, Colorado, United States",OK

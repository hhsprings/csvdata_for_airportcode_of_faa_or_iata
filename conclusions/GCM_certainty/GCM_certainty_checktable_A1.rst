
List for checking certainty of Great Circle Mapper (AAA - AFT)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    AAA,////,////,NTGA,UNK,1.0,Anaa,"Anaa, French Polynesia",OWNO,////,////,????,"Anaa, French Polynesia",OK
    AAB,////,////,YARY,UNK,1.0,Arrabury,"Arrabury, Australia",OWNO,////,////,????,"Arrabury, Queensland, Australia",OK
    AAC,////,////,HEAR,UNK,1.0,El Arish International Airport,"Arish, Egypt",OWNO,////,////,El Arish International Airport,"El Arish, Shamal Sina (North Sinai), Egypt",OK
    AAE,////,////,DABB,UNK,0.64,Les Salines,"Annaba, Algeria",OWNO,////,////,Rabah Bitat,"Annaba, El Tarf, Algeria",OK
    AAF,AAF,KAAF,KAAF,OK,0.479,Municipal,"Apalachicola (FL), USA",OWNO,APALACHICOLA RGNL-CLEVE RANDOLPH FIELD,"APALACHICOLA, FL - UNITED STATES",Apalachicola Regional-Cleve Randolph Field,"Apalachicola, Florida, United States",OK
    AAG,////,////,SSYA,UNK,1.0,Arapoti,"Arapoti, Brazil",OWNO,////,////,Arapoti Airport,"Arapoti, Paraná, Brazil",OK
    AAH,////,////,EDKA,UNK,1.0,Aachen/Merzbrück,"Aachen, Germany",OWNO,////,////,Merzbrück,"Aachen, North Rhine-Westphalia, Germany",OK
    AAI,////,////,SWRA,UNK,1.0,Arraias,"Arraias, Brazil",OWNO,////,////,????,"Arraias, Tocantins, Brazil",OK
    AAJ,////,////,SMCA,UNK,0.958,Cayana Airstrip,"Awaradam, Suriname",OWNO,////,////,Cajana Airstrip,"Awaradam, Sipaliwini, Suriname",OK
    AAK,////,////,NGUK,UNK,1.0,Aranuka,"Aranuka, Kiribati",OWNO,////,////,????,"Buariki, Aranuka, Kiribati",OK
    AAL,////,////,EKYT,UNK,1.0,Aalborg Airport (Aalborg Lufthavn),"Aalborg, Denmark",OWNO,////,////,????,"Aalborg, Denmark",OK
    AAM,////,////,FAMD,UNK,1.0,Mala Mala,"Mala Mala, South Africa",OWNO,////,////,Mala Mala Airport,"Mala Mala, Mpumalanga, South Africa",OK
    AAN,////,////,OMAL,UNK,1.0,Al Ain International Airport,"Al Ain, United Arab Emirates",OWNO,////,////,Al Ain International Airport,"Al Ain, Abu Dhabi, United Arab Emirates",OK
    AAO,////,////,SVAN,UNK,0.909,Anzualegui,"Anaco, Venezuela",OWNO,////,////,????,"Anaco, Anzoátegui, Venezuela",OK
    AAQ,////,////,URKA,UNK,1.0,Vityazevo Airport,"Anapa, Russia",OWNO,////,////,Vityazevo Airport,"Anapa, Krasnodarskiy, Russian Federation (Russia)",OK
    AAR,////,////,EKAH,UNK,1.0,Tirstrup,"Aarhus, Denmark",OWNO,////,////,Tirstrup,"Århus, Denmark",OK
    AAS,////,////,////,UNK,1.0,Apalapsili,"Apalapsili, Indonesia",OWNO,////,////,????,"Apalapsili, Papua, Indonesia",OK
    AAT,////,////,ZWAT,UNK,1.0,Altay Airport,"Altay, PR China",OWNO,////,////,????,"Altay, Xinjiang, China",OK
    AAU,////,////,NSAU,UNK,1.0,Asau,"Asau, Samoa",OWNO,////,////,????,"Asau, Samoa (Western Samoa)",OK
    AAV,////,////,RPMA,UNK,0.5,Alah,"Alah, Philippines",OWNO,////,////,????,"Allah Valley, Mindanao, Philippines",MAYBE
    AAX,////,////,SBAX,UNK,1.0,Araxa,"Araxa, Brazil",OWNO,////,////,????,"Araxá, Minas Gerais, Brazil",OK
    AAY,////,////,OYGD,UNK,0.72,Al Ghaydah,"Al Ghaydah, Yemen",OWNO,////,////,Al Ghaidah International Airport,"Al Ghaidah, Yemen",OK
    AAZ,////,////,MGQZ,UNK,1.0,Quetzaltenango Airport,"Quetzaltenango, Guatemala",OWNO,////,////,????,"Quezaltenango, Quetzaltenango, Guatemala",OK
    ABA,////,////,UNAA,UNK,1.0,Abakan,"Abakan, Russia",OWNO,////,////,Abakan Airport,"Abakan, Khakasiya, Russian Federation (Russia)",OK
    ABB,////,////,DNAS,UNK,0.571,International,Asaba,IATA,////,////,Asaba International Airport,"Asaba, Delta, Nigeria",MAYBE
    ABC,////,////,LEAB,UNK,0.686,Los Llanos,"Albacete, Spain",OWNO,////,////,Albacete Airport,"Albacete, Castille-La Mancha, Spain",OK
    ABD,////,////,OIAA,UNK,1.0,Abadan,"Abadan, Iran",OWNO,////,////,????,"Abadan, Khuzestan, Iran",OK
    ABE,ABE,KABE,KABE,OK,1.0,Lehigh Valley International,"Easton (PA), USA",OWNO,LEHIGH VALLEY INTL,"ALLENTOWN, PA - UNITED STATES",Lehigh Valley International Airport,"Allentown, Pennsylvania, United States",OK
    ABF,////,////,NGAB,UNK,1.0,Abaiang,"Abaiang, Kiribati",OWNO,////,////,????,"Abaiang, Kiribati",OK
    ABG,////,////,YABI,UNK,0.842,Abingdon,"Abingdon, Australia",OWNO,////,////,????,"Abingdon Downs, Queensland, Australia",MAYBE
    ABH,////,////,YAPH,UNK,1.0,Alpha,"Alpha, Australia",OWNO,////,////,????,"Alpha, Queensland, Australia",OK
    ABI,ABI,KABI,KABI,OK,0.694,Municipal,"Abilene, USA",OWNO,ABILENE RGNL,"ABILENE, TX - UNITED STATES",Abilene Regional,"Abilene, Texas, United States",OK
    ABJ,////,////,DIAP,UNK,1.0,Felix Houphouet Boigny International,"Abidjan, Cote d'Ivoire",OWNO,////,////,Félix Houphouët-Boigny International Airport,"Abidjan, Lagunes, Côte d'Ivoire (Ivory Coast)",OK
    ABK,////,////,HAKD,UNK,1.0,Kabri Dar,"Kabri Dar, Ethiopia",OWNO,////,////,Kabri Dar Airport,"Kabri Dar, Somali, Ethiopia",OK
    ABL,AFM,PAFM,PAFM,OK,1.0,Ambler,"Ambler (AK), USA",OWNO,AMBLER,"AMBLER, AK - UNITED STATES",????,"Ambler, Alaska, United States",OK
    ABM,////,////,YNPE,UNK,0.387,Bamaga,"Bamaga, Australia",OWNO,////,////,Northern Peninsula Airport,"Bamaga, Queensland, Australia",OK
    ABN,////,////,SMBN,UNK,1.0,Albina,"Albina, Suriname",OWNO,////,////,????,"Albina, Marowijne, Suriname",OK
    ABO,////,////,DIAO,UNK,1.0,Aboisso,"Aboisso, Cote d'Ivoire",OWNO,////,////,????,"Aboisso, Sud-Comoé, Côte d'Ivoire (Ivory Coast)",OK
    ABP,////,////,////,UNK,1.0,Atkamba,"Atkamba, Papua New Guinea",OWNO,////,////,????,"Atkamba, Western, Papua-New Guinea",OK
    ABQ,ABQ,KABQ,KABQ,OK,1.0,Albuquerque International Sunport,"Albuquerque (NM), USA",OWNO,ALBUQUERQUE INTL SUNPORT,"ALBUQUERQUE, NM - UNITED STATES",Albuquerque International Sunport,"Albuquerque, New Mexico, United States",OK
    ABR,ABR,KABR,KABR,OK,0.783,Municipal,"Aberdeen (SD), USA",OWNO,ABERDEEN RGNL,"ABERDEEN, SD - UNITED STATES",Aberdeen Regional,"Aberdeen, South Dakota, United States",OK
    ABS,////,////,HEBL,UNK,1.0,Abu Simbel Airport,"Abu Simbel, Egypt",OWNO,////,////,????,"Abu Simbel, Aswan, Egypt",OK
    ABT,////,////,OEBA,UNK,0.737,Al-Aqiq,"Al-Baha, Saudi Arabia",OWNO,////,////,Al-Baha Airport,"Al-Baha, Saudi Arabia",OK
    ABU,////,////,WATA,UNK,0.714,Atambua,"Atambua, Indonesia",OWNO,////,////,Haliwen,"Atambua, Nusa Tenggara Timur, Indonesia",OK
    ABV,////,////,DNAA,UNK,1.0,Nnamdi Azikiwe International Airport,"Abuja, Nigeria",OWNO,////,////,Nnamdi Azikiwe International Airport,"Abuja, Abuja Federal Capital Territory, Nigeria",OK
    ABW,////,////,////,UNK,1.0,Abau,"Abau, Papua New Guinea",OWNO,////,////,????,"Abau, Central, Papua-New Guinea",OK
    ABX,////,////,YMAY,UNK,1.0,Albury,"Albury, Australia",OWNO,////,////,????,"Albury, New South Wales, Australia",OK
    ABY,ABY,KABY,KABY,OK,0.25,Dougherty County,"Albany (GA), USA",OWNO,SOUTHWEST GEORGIA RGNL,"ALBANY, GA - UNITED STATES",Southwest Georgia Regional,"Albany, Georgia, United States",OK
    ABZ,////,////,EGPD,UNK,1.0,Dyce,"Aberdeen, United Kingdom",OWNO,////,////,Dyce,"Aberdeen, Aberdeenshire, Scotland, United Kingdom",OK
    ACA,////,////,MMAA,UNK,0.564,Acapulco International Airport,"Acapulco, Mexico",OWNO,////,////,General Juan N. Álvarez International Airport,"Acapulco, Guerrero, México",OK
    ACB,ACB,KACB,KACB,OK,1.0,Antrim County,"Bellaire, USA (MI)",OWNO,ANTRIM COUNTY,"BELLAIRE, MI - UNITED STATES",Antrim County Airport,"Bellaire, Michigan, United States",OK
    ACC,////,////,DGAA,UNK,1.0,Kotoka International Airport,"Accra, Ghana",OWNO,////,////,Kotoka International Airport,"Accra, Ghana",OK
    ACD,////,////,SKAD,UNK,0.526,Acandi,"Acandi, Colombia",OWNO,////,////,Alcides Fernández Airport,"Acandí, Chocó, Colombia",OK
    ACE,////,////,GCRR,UNK,1.0,Lanzarote,"Lanzarote, Canary Islands, Spain",OWNO,////,////,Lanzarote Airport,"Arrecife, Lanzarote Island, Canary Islands, Spain",OK
    ACH,////,////,LSZR,UNK,0.696,Altenrhein,"Altenrhein, Switzerland",OWNO,////,////,St. Gallen-Altenrhein Airport,"St. Gallen / Altenrhein, Sankt Gallen, Switzerland",OK
    ACI,////,////,EGJA,UNK,1.0,Alderney Airport - The Blaye,"Alderney (one of the Channel Islands), United Kingdom",OWNO,////,////,The Blaye,"Alderney, Guernsey",OK
    ACJ,////,////,VCCA,UNK,nan,////,////,////,////,////,Anuradhapura Airport,"Anuradhapura, North Central Province, Sri Lanka (Ceylon)",UNK
    ACK,ACK,KACK,KACK,OK,1.0,Memorial,"Nantucket (MA), USA",OWNO,NANTUCKET MEMORIAL,"NANTUCKET, MA - UNITED STATES",Nantucket Memorial Airport,"Nantucket, Massachusetts, United States",OK
    ACL,////,////,////,UNK,1.0,Aguaclara,"Aguaclara, Colombia",OWNO,////,////,????,"Aguaclara, Casanare, Colombia",OK
    ACM,////,////,////,UNK,1.0,Arica,"Arica, Colombia",OWNO,////,////,????,"Arica, Amazonas, Colombia",OK
    ACN,////,////,MMCC,UNK,1.0,International,"Ciudad Acuna, Mexico",OWNO,////,////,Ciudad Acuña International Airport,"Ciudad Acuña, Coahuila, México",OK
    ACP,////,////,OITM,UNK,nan,////,////,////,////,////,Sahand,"Maragheh, Azarbayjan-e Sharqi, Iran",UNK
    ACR,////,////,SKAC,UNK,1.0,Araracuara,"Araracuara, Colombia",OWNO,////,////,Araracuara Airport,"Araracuara, Caquetá, Colombia",OK
    ACS,////,////,UNKS,UNK,1.0,Achinsk,"Achinsk, Russia",OWNO,////,////,????,"Achinsk, Krasnoyarskiy, Russian Federation (Russia)",OK
    ACT,ACT,KACT,KACT,OK,0.64,Municipal,"Waco (TX), USA",OWNO,WACO RGNL,"WACO, TX - UNITED STATES",Waco Regional,"Waco, Texas, United States",OK
    ACU,////,////,////,UNK,1.0,Achutupo,"Achutupo, Panama",OWNO,////,////,????,"Achutupo, Kuna Yala (Guna Yala), Panamá",OK
    ACV,ACV,KACV,KACV,OK,1.0,Arcata,"Arcata (CA), USA",OWNO,ARCATA,"ARCATA/EUREKA, CA - UNITED STATES",Arcata Airport,"Arcata/Eureka, California, United States",OK
    ACX,////,////,ZUYI,UNK,0.522,Xingyi,Xingyi,IATA,////,////,Xingyi Wanfenglin Airport,"Xingyi, Guizhou, China",MAYBE
    ACY,ACY,KACY,KACY,OK,1.0,Atlantic City International,"Atlantic City (NJ), USA",OWNO,ATLANTIC CITY INTL,"ATLANTIC CITY, NJ - UNITED STATES",Atlantic City International Airport,"Atlantic City, New Jersey, United States",OK
    ACZ,////,////,OIZB,UNK,0.833,Zabol A/P,"Zabol, Iran",OWNO,////,////,????,"Zabol, Sistan va Baluchestan, Iran",OK
    ADA,////,////,LTAF,UNK,1.0,Adana,"Adana, Turkey",OWNO,////,////,????,"Adana, Adana, Turkey",OK
    ADB,////,////,LTBJ,UNK,1.0,Adnan Menderes Airport,"Izmir, Turkey",OWNO,////,////,Adnan Menderes,"Izmir, Izmir, Turkey",OK
    ADD,////,////,HAAB,UNK,1.0,Bole International Airport,"Addis Ababa, Ethiopia",OWNO,////,////,Bole International Airport,"Addis Ababa, Addis Ababa, Ethiopia",OK
    ADE,////,////,OYAA,UNK,1.0,Aden International Airport,"Aden, Yemen",OWNO,////,////,Aden International Airport,"Aden, Yemen",OK
    ADF,////,////,LTCP,UNK,1.0,Adiyaman,"Adiyaman, Turkey",OWNO,////,////,????,"Adiyaman, Adiyaman, Turkey",OK
    ADG,ADG,KADG,KADG,OK,1.0,Lenawee County,"Adrian (MI), USA",OWNO,LENAWEE COUNTY,"ADRIAN, MI - UNITED STATES",Lenawee County Airport,"Adrian, Michigan, United States",OK
    ADH,////,////,UEEA,UNK,1.0,Aldan,"Aldan, Russia",OWNO,////,////,Aldan Airport,"Aldan, Sakha (Yakutiya), Russian Federation (Russia)",OK
    ADI,////,////,FYAR,UNK,1.0,Arandis,"Arandis, Namibia",OWNO,////,////,????,"Arandis, Namibia",OK
    ADJ,////,////,OJAM,UNK,1.0,Marka International Airport,"Amman, Jordan",OWNO,////,////,Marka International Airport,"Amman, Jordan",OK
    ADK,ADK,PADK,PADK,OK,0.87,Adak Island NS,"Adak Island (AK), USA",OWNO,ADAK,"ADAK ISLAND, AK - UNITED STATES",Adak Airport,"Adak Island, Alaska, United States",OK
    ADL,////,////,YPAD,UNK,1.0,Adelaide International Airport,"Adelaide, Australia",OWNO,////,////,Adelaide International Airport,"Adelaide, South Australia, Australia",OK
    ADM,ADM,KADM,KADM,OK,1.0,Municipal Airport,"Ardmore (OK), USA",OWNO,ARDMORE MUNI,"ARDMORE, OK - UNITED STATES",Ardmore Municipal Airport,"Ardmore, Oklahoma, United States",OK
    ADN,////,////,SKAN,UNK,1.0,Andes,"Andes, Colombia",OWNO,////,////,????,"Andes, Antioquia, Colombia",OK
    ADO,////,////,YAMK,UNK,1.0,Andamooka,"Andamooka, Australia",OWNO,////,////,????,"Andamooka, South Australia, Australia",OK
    ADP,////,////,VCCG,UNK,0.6,Anuradhapura,"Anuradhapura, Sri Lanka",OWNO,////,////,Gal Oya Airport,"Ampara, Eastern Province, Sri Lanka (Ceylon)",TO DO CHECK
    ADQ,ADQ,PADQ,PADQ,OK,1.0,Kodiak Airport,"Kodiak (AK), USA",OWNO,KODIAK,"KODIAK, AK - UNITED STATES",????,"Kodiak, Alaska, United States",OK
    ADR,PHH,KPHH,KPHH,OK,0.563,Andrews,"Andrews (SC), USA",OWNO,ROBERT F SWINNIE,"ANDREWS, SC - UNITED STATES",Robert F Swinnie Airport,"Andrews, South Carolina, United States",OK
    ADS,ADS,KADS,KADS,OK,1.0,Addison Airport,"Dallas, Addison (TX), USA",OWNO,ADDISON,"DALLAS, TX - UNITED STATES",Addison Airport,"Dallas, Texas, United States",OK
    ADT,ADH,KADH,KADH,OK,0.686,Ada,"Ada (OK), USA",OWNO,ADA MUNI,"ADA, OK - UNITED STATES",Ada Municipal Airport,"Ada, Oklahoma, United States",OK
    ADU,////,////,OITL,UNK,1.0,Ardabil,"Ardabil, Iran",OWNO,////,////,????,"Ardabil, Azarbayjan-e Sharqi, Iran",OK
    ADV,////,////,EGWA,UNK,1.0,Andover,"Andover, United Kingdom",OWNO,////,////,????,"Andover, Hampshire, England, United Kingdom",OK
    ADW,ADW,KADW,KADW,OK,0.663,Andrews AFB,"Camp Springs (MD), USA",OWNO,JOINT BASE ANDREWS,"CAMP SPRINGS, MD - UNITED STATES",Joint Base Andrews Airport,"Camp Springs, Maryland, United States",OK
    ADX,////,////,EGQL,UNK,0.905,RAF Leuchars,"St Andrews, Scotland, United Kingdom",OWNO,////,////,Leuchars,"St. Andrews, Fife, Scotland, United Kingdom",OK
    ADY,////,////,FAAL,UNK,1.0,Alldays,"Alldays, South Africa",OWNO,////,////,????,"Alldays, Limpopo, South Africa",OK
    ADZ,////,////,SKSP,UNK,0.567,Sesquicentenario International Airport,"San Andres Island, Colombia",OWNO,////,////,Gustavo Rojas Pinilla International Airport,"San Andrés Island, San Andrés y Providencia, Colombia",OK
    AEA,////,////,NGTB,UNK,1.0,Abemama Atoll,"Abemama Atoll, Kiribati",OWNO,////,////,????,"Abemama Atoll, Kiribati",OK
    AEB,////,////,ZGBS,UNK,0.462,Youjiang,Baise,IATA,////,////,Baise Bama Airport,"Baise, Guangxi, China",MAYBE
    AEG,////,////,WIME,UNK,1.0,Aek Godang,"Aek Godang, Indonesia",OWNO,////,////,Aek Godang Airport,"Padang Sidempuan, Sumatera Utara, Indonesia",OK
    AEH,////,////,FTTC,UNK,0.923,Abecher,"Abecher, Chad",OWNO,////,////,????,"Abéché, Ouaddaï, Chad",TO DO CHECK
    AEK,////,////,////,UNK,1.0,Aseki,"Aseki, Papua New Guinea",OWNO,////,////,????,"Aseki, Morobe, Papua-New Guinea",OK
    AEL,AEL,KAEL,KAEL,OK,0.79,Albert Lea,"Albert Lea (MN), USA",OWNO,ALBERT LEA MUNI,"ALBERT LEA, MN - UNITED STATES",Albert Lea Municipal Airport,"Albert Lea, Minnesota, United States",OK
    AEO,////,////,GQNA,UNK,1.0,Aioun El Atrouss,"Aioun El Atrouss, Mauritania",OWNO,////,////,????,"Aioun el Atrouss, Mauritania",OK
    AEP,////,////,SABE,UNK,0.851,Jorge Newbery Airport,"Buenos Aires, Argentina",OWNO,////,////,Aeroparque Jorge Newbery,"Buenos Aires, Distrito Federal, Argentina",OK
    AER,////,////,URSS,UNK,0.571,Adler-Sochi International Airport,"Adler/Sochi, Russia",OWNO,////,////,Sochi Airport,"Sochi, Krasnodarskiy, Russian Federation (Russia)",OK
    AES,////,////,ENAL,UNK,1.0,Vigra,"Aalesund, Norway",OWNO,////,////,Vigra,"Ålesund, Norway",OK
    AET,6A8,PFAL,PFAL,OK,1.0,Allakaket Airport,"Allakaket (AK), USA",OWNO,ALLAKAKET,"ALLAKAKET, AK - UNITED STATES",????,"Allakaket, Alaska, United States",OK
    AEU,////,////,OIBA,UNK,nan,////,////,////,////,////,????,"Abu Musa Island, Hormozgan, Iran",UNK
    AEX,AEX,KAEX,KAEX,OK,1.0,Alexandria International Airport,"Alexandria (LA), USA",OWNO,ALEXANDRIA INTL,"ALEXANDRIA, LA - UNITED STATES",Alexandria International Airport,"Alexandria, Louisiana, United States",OK
    AEY,////,////,BIAR,UNK,1.0,Akureyri,"Akureyri, Iceland",OWNO,////,////,Akureyri Airport,"Akureyri, Iceland",OK
    AFA,////,////,SAMR,UNK,1.0,San Rafael,"San Rafael, Argentina",OWNO,////,////,????,"San Rafael, Mendoza, Argentina",OK
    AFD,////,////,FAPA,UNK,1.0,Port Alfred,"Port Alfred, South Africa",OWNO,////,////,????,"Port Alfred, Eastern Cape, South Africa",OK
    AFF,AFF,KAFF,KAFF,OK,0.833,USAF Academy,"Colorado Springs (CO), USA",OWNO,USAF ACADEMY AFLD,"COLORADO SPRINGS, CO - UNITED STATES",USAF Academy Airfield,"Colorado Springs, Colorado, United States",OK
    AFI,////,////,SKAM,UNK,1.0,Amalfi,"Amalfi, Colombia",OWNO,////,////,Amalfi Airport,"Amalfi, Antioquia, Colombia",OK
    AFK,////,////,////,UNK,nan,////,////,////,////,////,Kondavattavan Tank Waterdrome,"Ampara, Eastern Province, Sri Lanka (Ceylon)",UNK
    AFL,////,////,SBAT,UNK,0.5,Alta Floresta,"Alta Floresta, Brazil",OWNO,////,////,Piloto Oswaldo Marques Dias Airport,"Alta Floresta, Mato Grosso, Brazil",OK
    AFN,AFN,KAFN,KAFN,OK,0.598,Municipal,"Jaffrey (NH), USA",OWNO,JAFFREY AIRPORT-SILVER RANCH,"JAFFREY, NH - UNITED STATES",Jaffrey Airport-Silver Ranch,"Jaffrey, New Hampshire, United States",OK
    AFO,AFO,KAFO,KAFO,OK,1.0,Municipal,"Afton (WY), USA",OWNO,AFTON MUNI,"AFTON, WY - UNITED STATES",Afton Municipal Airport,"Afton, Wyoming, United States",OK
    AFR,////,////,////,UNK,1.0,Afore,"Afore, Papua New Guinea",OWNO,////,////,????,"Afore, Northern, Papua-New Guinea",OK
    AFS,////,////,UTSN,UNK,nan,////,////,////,////,////,Zarafshan Airport,"Zarafshan, Navoiy, Uzbekistan",UNK
    AFT,////,////,AGAF,UNK,1.0,Afutara Aerodrome,"Afutara, Solomon Islands",OWNO,////,////,????,"Afutara, Solomon Islands",OK


List for checking certainty of Great Circle Mapper (AFW - ALX)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    AFW,AFW,KAFW,KAFW,OK,1.0,Fort Worth Alliance,"Fort Worth (TX), USA",OWNO,FORT WORTH ALLIANCE,"FORT WORTH, TX - UNITED STATES",Fort Worth Alliance Airport,"Fort Worth, Texas, United States",OK
    AFY,////,////,LTAH,UNK,1.0,Afyon,"Afyon, Turkey",OWNO,////,////,????,"Afyon, Afyonkarahisar, Turkey",OK
    AFZ,////,////,OIMS,UNK,nan,////,////,////,////,////,????,"Sabzevar, Khorasan-e Razavi, Iran",UNK
    AGA,////,////,GMAD,UNK,1.0,Agadir-Al Massira Airport,"Agadir, Morocco",OWNO,////,////,Al Massira,"Agadir, Morocco",OK
    AGB,////,////,EDMA,UNK,1.0,Augsburg Airport (Flughafen Augsburg),"Augsburg, Germany",OWNO,////,////,????,"Augsburg, Bavaria, Germany",OK
    AGC,AGC,KAGC,KAGC,OK,1.0,Allegheny County Airport,"Pittsburgh (PA), USA",OWNO,ALLEGHENY COUNTY,"PITTSBURGH, PA - UNITED STATES",Allegheny County Airport,"Pittsburgh, Pennsylvania, United States",OK
    AGD,////,////,WASG,UNK,1.0,Anggi,"Anggi, Indonesia",OWNO,////,////,????,"Anggi, Papua, Indonesia",OK
    AGE,////,////,EDWG,UNK,0.667,Flugplatz Wangerooge,"Wangerooge, Germany",OWNO,////,////,????,"Wangerooge, Lower Saxony, Germany",OK
    AGF,////,////,LFBA,UNK,1.0,Agen La Garenne Airport,"Agen, France",OWNO,////,////,La Garenne,"Agen, Aquitaine, France",OK
    AGG,////,////,////,UNK,1.0,Angoram,"Angoram, Papua New Guinea",OWNO,////,////,????,"Angoram, East Sepik, Papua-New Guinea",OK
    AGH,////,////,ESTA,UNK,1.0,Angelholm,"Angelholm/Helsingborg, Sweden",OWNO,////,////,????,"Ängelholm, Sweden",OK
    AGI,////,////,SMWA,UNK,0.741,Wageningen,"Wageningen, Suriname",OWNO,////,////,Wageningen Airstrip,"Wageningen, Nickerie, Suriname",OK
    AGJ,////,////,RORA,UNK,1.0,Aguni,"Aguni, Japan",OWNO,////,////,Aguni Airport,"Aguni, Aguni Island, Okinawa, Japan",OK
    AGK,////,////,////,UNK,1.0,Kagua,"Kagua, Papua New Guinea",OWNO,////,////,????,"Kagua, Southern Highlands, Papua-New Guinea",OK
    AGL,////,////,////,UNK,1.0,Wanigela,"Wanigela, Papua New Guinea",OWNO,////,////,????,"Wanigela, Northern, Papua-New Guinea",OK
    AGM,////,////,BGAM,UNK,1.0,Tasiilaq,"Tasiilaq, Greenland",OWNO,////,////,????,"Tasiilaq, Sermersooq, Greenland",OK
    AGN,AGN,PAGN,PAGN,OK,0.396,Angoon Seaplane Base,"Angoon (AK), USA",OWNO,ANGOON,"ANGOON, AK - UNITED STATES",Angoon SPB,"Angoon, Alaska, United States",OK
    AGO,AGO,KAGO,KAGO,OK,1.0,Magnolia Municipal Airport,"Magnolia (AR), USA",OWNO,MAGNOLIA MUNI,"MAGNOLIA, AR - UNITED STATES",Magnolia Municipal Airport,"Magnolia, Arkansas, United States",OK
    AGP,////,////,LEMG,UNK,1.0,Málaga-Costa del Sol Airport[,"Málaga, Spain",OWNO,////,////,Costa del Sol,"Málaga, Andalusia, Spain",OK
    AGQ,////,////,LGAG,UNK,1.0,Agrinion,"Agrinion, Greece",OWNO,////,////,????,"Agrinion, Dytikí Elláda (Western Greece), Greece",OK
    AGR,////,////,VIAG,UNK,1.0,Kheria,"Agra, India",OWNO,////,////,Kheria,"Agra, Uttar Pradesh, India",OK
    AGS,AGS,KAGS,KAGS,OK,0.752,Bush Field,"Augusta (GA), USA",OWNO,AUGUSTA RGNL AT BUSH FIELD,"AUGUSTA, GA - UNITED STATES",Augusta Regional at Bush Field,"Augusta, Georgia, United States",OK
    AGT,////,////,SGES,UNK,1.0,Guarani International Airport (Alejo Garcia),"Ciudad del Este, Paraguay",OWNO,////,////,Guaraní International Airport,"Ciudad del Este, Alto Paraná, Paraguay",OK
    AGU,////,////,MMAS,UNK,0.877,Jesús Terán Peredo International Airport,"Aguascalientes, Mexico",OWNO,////,////,Licenciado Jesús Terán Peredo International Airport,"Aguascalientes, Aguascalientes, México",OK
    AGV,////,////,SVAC,UNK,1.0,Oswaldo Guevara Mujica Airport,"Acarigua, Venezuela",OWNO,////,////,Oswaldo Guevara Mujica,"Acarigua, Portuguesa, Venezuela",OK
    AGW,////,////,////,UNK,1.0,Agnew Airport,"Agnew, Australia",OWNO,////,////,????,"Agnew, Queensland, Australia",OK
    AGX,////,////,VOAT,UNK,1.0,Agatti Island,"Agatti Island, India",OWNO,////,////,Agatti Aerodrome,"Agatti Island, Lakshadweep, India",OK
    AGZ,////,////,FAAG,UNK,1.0,Aggeneys Airport,"Aggeneys, South Africa",OWNO,////,////,Aggeneys Aerodrome,"Aggeneys, Northern Cape, South Africa",OK
    AHB,////,////,OEAB,UNK,0.615,Abha Regional Airport,"Abha, Saudi Arabia",OWNO,////,////,Abha Airport,"Abha, Saudi Arabia",OK
    AHC,AHC,KAHC,KAHC,OK,1.0,Amedee AAF,"Herlong (CA), USA",OWNO,AMEDEE AAF,"HERLONG, CA - UNITED STATES",Amedee AAF Airport,"Herlong, California, United States",OK
    AHD,1F0,////,////,OK,0.694,Downtown,"Ardmore (OK), USA",OWNO,ARDMORE DOWNTOWN EXECUTIVE,"ARDMORE, OK - UNITED STATES",Ardmore Downtown Executive Airport,"Ardmore, Oklahoma, United States",OK
    AHE,////,////,NTHE,UNK,1.0,Ahe Airport,"Ahe, French Polynesia",OWNO,////,////,????,"Ahe, French Polynesia",OK
    AHF,37V,////,////,OK,1.0,Municipal,"Arapahoe (NE), USA",OWNO,ARAPAHOE MUNI,"ARAPAHOE, NE - UNITED STATES",Arapahoe Municipal Airport,"Arapahoe, Nebraska, United States",OK
    AHH,AHH,KAHH,KAHH,OK,1.0,Municipal,"Amery (WI), USA",OWNO,AMERY MUNI,"AMERY, WI - UNITED STATES",Amery Municipal Airport,"Amery, Wisconsin, United States",OK
    AHI,////,////,WAPA,UNK,1.0,Amahai,"Amahai, Indonesia",OWNO,////,////,Amahai,"Pulau Seram, Maluku, Indonesia",OK
    AHJ,////,////,ZUHY,UNK,nan,////,////,////,////,////,Hongyuan Airport,"Hongyuan, Aba, Sichuan, China",UNK
    AHL,////,////,SYAH,UNK,1.0,Aishalton,"Aishalton, Guyana",OWNO,////,////,????,"Aishalton, Upper Takutu-Upper Essequibo, Guyana",OK
    AHN,AHN,KAHN,KAHN,OK,1.0,Athens-Ben Epps Airport,"Athens (GA), USA",OWNO,ATHENS/BEN EPPS,"ATHENS, GA - UNITED STATES",Athens/Ben Epps Airport,"Athens, Georgia, United States",OK
    AHO,////,////,LIEA,UNK,1.0,Fertilia,"Alghero, Italy",OWNO,////,////,Fertilia,"Alghero, Sardinia, Italy",OK
    AHS,////,////,////,UNK,1.0,Ahuas,"Ahuas, Honduras",OWNO,////,////,????,"Ahuás, Gracias a Dios, Honduras",OK
    AHU,////,////,GMTA,UNK,1.0,Charif Al Idrissi,"Al Hoceima, Morocco",OWNO,////,////,Cherif al Idrissi,"Al Hoceima, Morocco",OK
    AHY,////,////,////,UNK,1.0,Ambatolahy,"Ambatolahy, Madagascar",OWNO,////,////,????,"Ambatolahy, Madagascar",OK
    AHZ,////,////,LFHU,UNK,0.636,Alpe D Huez,"Alpe D Huez, France",OWNO,////,////,????,"L'Alpe d'Huez, Rhône-Alpes, France",MAYBE
    AIA,AIA,KAIA,KAIA,OK,0.76,Alliance,"Alliance (NE), USA",OWNO,ALLIANCE MUNI,"ALLIANCE, NE - UNITED STATES",Alliance Municipal Airport,"Alliance, Nebraska, United States",OK
    AIC,////,////,////,UNK,1.0,Airok,"Airok, Marshall Islands",OWNO,////,////,????,"Airok, Marshall Islands",OK
    AID,AID,KAID,KAID,OK,0.507,Municipal,"Anderson (IN), USA",OWNO,ANDERSON MUNI-DARLINGTON FIELD,"ANDERSON, IN - UNITED STATES",Anderson Municipal-Darlington Field,"Anderson, Indiana, United States",OK
    AIE,////,////,////,UNK,1.0,Aiome,"Aiome, Papua New Guinea",OWNO,////,////,????,"Aiome, Madang, Papua-New Guinea",OK
    AIF,////,////,SBAS,UNK,1.0,Assis,"Assis, Brazil",OWNO,////,////,????,"Assis, São Paulo, Brazil",OK
    AIG,////,////,FEFY,UNK,1.0,Yalinga,"Yalinga, Central African Republic",OWNO,////,////,????,"Yalinga, Haute-Kotto (Tö-Kötö), Central African Republic",OK
    AII,////,////,HDAS,UNK,1.0,Alisabieh,"Alisabieh, Djibouti",OWNO,////,////,????,"Alisabieh, Djibouti",OK
    AIK,AIK,KAIK,KAIK,OK,1.0,Municipal,"Aiken (SC), USA",OWNO,AIKEN MUNI,"AIKEN, SC - UNITED STATES",Aiken Municipal Airport,"Aiken, South Carolina, United States",OK
    AIL,////,////,////,UNK,1.0,Ailigandi,"Ailigandi, Panama",OWNO,////,////,????,"Ailigandi, Kuna Yala (Guna Yala), Panamá",OK
    AIM,////,////,////,UNK,1.0,Ailuk Island,"Ailuk Island, Marshall Islands",OWNO,////,////,????,"Ailuk Island, Marshall Islands",OK
    AIN,AWI,PAWI,PAWI,OK,1.0,Wainwright,"Wainwright (AK), USA",OWNO,WAINWRIGHT,"WAINWRIGHT, AK - UNITED STATES",????,"Wainwright, Alaska, United States",OK
    AIO,AIO,KAIO,KAIO,OK,1.0,Municipal,"Atlantic IA, USA",OWNO,ATLANTIC MUNI,"ATLANTIC, IA - UNITED STATES",Atlantic Municipal Airport,"Atlantic, Iowa, United States",OK
    AIR,////,////,////,UNK,1.0,Aripuana,"Aripuana, Brazil",OWNO,////,////,????,"Aripuanã, Mato Grosso, Brazil",OK
    AIS,////,////,NGTR,UNK,1.0,Arorae Island,"Arorae Island, Kiribati",OWNO,////,////,????,"Arorae Island, Kiribati",OK
    AIT,////,////,NCAI,UNK,1.0,Aitutaki,"Aitutaki, Cook Islands",OWNO,////,////,????,"Aitutaki, Cook Islands",OK
    AIU,////,////,NCAT,UNK,1.0,Atiu Island,"Atiu Island, Cook Islands",OWNO,////,////,????,"Atiu Island, Cook Islands",OK
    AIV,AIV,KAIV,KAIV,OK,1.0,George Downer,"Aliceville (AL), USA",OWNO,GEORGE DOWNER,"ALICEVILLE, AL - UNITED STATES",George Downer Airport,"Aliceville, Alabama, United States",OK
    AIZ,AIZ,KAIZ,KAIZ,OK,1.0,Lee C Fine Memorial,"Kaiser/Lake Ozark (MO), USA",OWNO,LEE C FINE MEMORIAL,"KAISER/LAKE OZARK, MO - UNITED STATES",Lee C Fine Memorial Airport,"Kaiser/Lake Ozark, Missouri, United States",OK
    AJA,////,////,LFKJ,UNK,0.667,Campo Dell Oro,"Ajaccio, France",OWNO,////,////,Ajaccio Napoleon Bonaparte Airport,"Ajaccio, Corse (Corsica), France",OK
    AJF,////,////,OESK,UNK,0.75,Jouf,"Jouf, Saudi Arabia",OWNO,////,////,Al-Jouf Airport,"Al-Jouf, Saudi Arabia",TO DO CHECK
    AJI,////,////,LTCO,UNK,1.0,Agri,"Agri, Turkey",OWNO,////,////,????,"Agri, Agri, Turkey",OK
    AJJ,////,////,GQNJ,UNK,1.0,Akjoujt,"Akjoujt, Mauritania",OWNO,////,////,????,"Akjoujt, Mauritania",OK
    AJK,////,////,OIHR,UNK,nan,////,////,////,////,////,????,"Arak, Markazi, Iran",UNK
    AJL,////,////,VEAZ,UNK,1.0,Aizawl,"Aizawl, India",OWNO,////,////,????,"Aizawl, Mizoram, India",OK
    AJN,////,////,FMCV,UNK,1.0,Ouani,"Anjouan, Nzwani Island, Comoros",OWNO,////,////,Ouani,"Anjouan, Comoros",OK
    AJR,////,////,ESNX,UNK,1.0,Arvidsjaur,"Arvidsjaur, Sweden",OWNO,////,////,????,"Arvidsjaur, Norrbottens län, Sweden",OK
    AJS,////,////,////,UNK,0.727,Abreojos,"Abreojos, Mexico",OWNO,////,////,????,"Punta Abreojos, Baja California Sur, México",MAYBE
    AJU,////,////,SBAR,UNK,1.0,Aracaju - Santa Maria Airport,"Aracaju, Brazil",OWNO,////,////,Santa Maria,"Aracaju, Sergipe, Brazil",OK
    AJY,////,////,DRZA,UNK,1.0,Mano Dayak International Airport,"Agades, Niger",OWNO,////,////,Manu Dayak International Airport,"Agades, Niger",OK
    AKA,////,////,ZLAK,UNK,1.0,Ankang,"Ankang, PR China",OWNO,////,////,????,"Ankang, Shaanxi, China",OK
    AKB,AKA,PAAK,PAAK,OK,1.0,Atka,"Atka (AK), USA",OWNO,ATKA,"ATKA, AK - UNITED STATES",????,"Atka, Alaska, United States",OK
    AKC,AKR,KAKR,KAKR,OK,1.0,Akron Fulton International Airport,"Akron/Canton (OH), USA",OWNO,AKRON FULTON INTL,"AKRON, OH - UNITED STATES",Akron Fulton International Airport,"Akron, Ohio, United States",OK
    AKD,////,////,VAAK,UNK,1.0,Akola,"Akola, India",OWNO,////,////,????,"Akola, Maharashtra, India",OK
    AKE,////,////,////,UNK,1.0,Akieni,"Akieni, Gabon",OWNO,////,////,????,"Akieni, Haut-Ogooué, Gabon",OK
    AKF,////,////,HLKF,UNK,1.0,Kufrah,"Kufrah, Libya",OWNO,////,////,????,"Kufrah, Libyan Arab Jamahiriya (Libya)",OK
    AKG,////,////,////,UNK,1.0,Anguganak,"Anguganak, Papua New Guinea",OWNO,////,////,????,"Anguganak, Sandaun, Papua-New Guinea",OK
    AKH,////,////,OEPS,UNK,nan,////,////,////,////,////,Prince Sultan AB,"Al-Kharj, Saudi Arabia",UNK
    AKI,AKI,PFAK,PFAK,OK,1.0,Akiak,"Akiak (AK), USA",OWNO,AKIAK,"AKIAK, AK - UNITED STATES",????,"Akiak, Alaska, United States",OK
    AKJ,////,////,RJEC,UNK,1.0,Asahikawa,"Asahikawa, Japan",OWNO,////,////,????,"Asahikawa, Hokkaido, Japan",OK
    AKK,AKK,PAKH,PAKH,OK,0.752,Akhiok SPB,"Akhiok, USAAK",OWNO,AKHIOK,"AKHIOK, AK - UNITED STATES",????,"Akhiok, Alaska, United States",OK
    AKL,////,////,NZAA,UNK,0.564,Metropolitan Area,"Auckland, New Zealand",OWNO,////,////,Auckland International Airport,"Auckland, New Zealand",OK
    AKM,////,////,////,UNK,1.0,Zakouma,"Zakouma, Chad",OWNO,////,////,Zakouma Airport,"Zakouma, Salamat, Chad",OK
    AKN,AKN,PAKN,PAKN,OK,1.0,King Salmon,"King Salmon (AK), USA",OWNO,KING SALMON,"KING SALMON, AK - UNITED STATES",????,"King Salmon, Alaska, United States",OK
    AKO,AKO,KAKO,KAKO,OK,1.0,Colorado Plains Regional Airport,"Akron (CO), USA",OWNO,COLORADO PLAINS RGNL,"AKRON, CO - UNITED STATES",Colorado Plains Regional,"Akron, Colorado, United States",OK
    AKP,AKP,PAKP,PAKP,OK,0.933,Anaktuvuk,"Anaktuvuk (AK), USA",OWNO,ANAKTUVUK PASS,"ANAKTUVUK PASS, AK - UNITED STATES",????,"Anaktuvuk Pass, Alaska, United States",OK
    AKQ,////,////,WIAG,UNK,1.0,Gunung Batin,"Astraksetra, Indonesia",OWNO,////,////,Gunung Batin,"Astraksetra, Lampung, Indonesia",OK
    AKR,////,////,DNAK,UNK,1.0,Akure,"Akure, Nigeria",OWNO,////,////,Akure Airport,"Akure, Ondo, Nigeria",OK
    AKS,////,////,AGGA,UNK,0.421,Gwaunaru'u,"Auki, Solomon Islands",OWNO,////,////,????,"Auki, Solomon Islands",OK
    AKT,////,////,LCRA,UNK,1.0,Akrotiri RAF,"Akrotiri, Cyprus",OWNO,////,////,RAF Akrotiri,"Akrotiri, Cyprus",OK
    AKU,////,////,ZWAK,UNK,1.0,Aksu,"Aksu, PR China",OWNO,////,////,????,"Aksu, Xinjiang, China",OK
    AKV,////,////,CYKO,UNK,1.0,Akulivik,"Akulivik, Canada",OWNO,////,////,????,"Akulivik, Québec, Canada",OK
    AKW,////,////,OIAG,UNK,nan,////,////,////,////,////,????,"Aghajari, Khuzestan, Iran",UNK
    AKX,////,////,UATT,UNK,0.714,Aktyubinsk,"Aktyubinsk, Kazakhstan",OWNO,////,////,Aktobe Airport,"Aktobe, Aqtöbe, Kazakhstan",TO DO CHECK
    AKY,////,////,VYSW,UNK,1.0,Sittwe Airport,"Sittwe, Myanmar",OWNO,////,////,????,"Sittwe, Rakhine, Myanmar (Burma)",OK
    ALA,////,////,UAAA,UNK,1.0,Almaty International Airport,"Almaty, Kazakhstan",OWNO,////,////,Almaty International Airport,"Almaty, Almaty, Kazakhstan",OK
    ALB,ALB,KALB,KALB,OK,1.0,Albany International Airport,"Albany (NY), USA",OWNO,ALBANY INTL,"ALBANY, NY - UNITED STATES",Albany International Airport,"Albany, New York, United States",OK
    ALC,////,////,LEAL,UNK,0.667,El Altet Airport,"Alicante, Spain",OWNO,////,////,Alicante,"Alicante, Valenciana, Spain",OK
    ALD,////,////,SPAR,UNK,1.0,Alerta,"Alerta, Peru",OWNO,////,////,????,"Alerta, Madre de Dios, Perú",OK
    ALE,E38,////,////,OK,0.399,Alpine,"Alpine (TX), USA",OWNO,ALPINE-CASPARIS MUNI,"ALPINE, TX - UNITED STATES",Alpine-Casparis Municipal Airport,"Alpine, Texas, United States",OK
    ALF,////,////,ENAT,UNK,1.0,Alta,"Alta, Norway",OWNO,////,////,????,"Alta, Norway",OK
    ALG,////,////,DAAG,UNK,1.0,Houari Boumedienne Airport,"Algiers, Algeria",OWNO,////,////,Houari Boumediene Airport,"Algiers, Boumerdès, Algeria",OK
    ALH,////,////,YABA,UNK,1.0,Albany,"Albany, Australia",OWNO,////,////,????,"Albany, Western Australia, Australia",OK
    ALI,ALI,KALI,KALI,OK,1.0,Alice International Airport,"Alice, USA",OWNO,ALICE INTL,"ALICE, TX - UNITED STATES",Alice International Airport,"Alice, Texas, United States",OK
    ALJ,////,////,FAAB,UNK,0.878,Kortdoorn,"Alexander Bay, South Africa",OWNO,////,////,Alexander Bay Airport,"Alexander Bay, Northern Cape, South Africa",OK
    ALK,////,////,////,UNK,1.0,Asela,"Asela, Ethiopia",OWNO,////,////,????,"Asela, Oromia, Ethiopia",OK
    ALL,////,////,LIMG,UNK,1.0,Albenga,"Albenga, Italy",OWNO,////,////,????,"Albenga, Liguria, Italy",OK
    ALM,ALM,KALM,KALM,OK,0.552,Municipal,"Alamogordo (NM), USA",OWNO,ALAMOGORDO-WHITE SANDS RGNL,"ALAMOGORDO, NM - UNITED STATES",Alamogordo-White Sands Regional,"Alamogordo, New Mexico, United States",OK
    ALN,ALN,KALN,KALN,OK,0.484,Alton,"Alton (IL), USA",OWNO,ST LOUIS RGNL,"ALTON/ST LOUIS, IL - UNITED STATES",St. Louis Regional,"Alton/St. Louis, Illinois, United States",OK
    ALO,ALO,KALO,KALO,OK,0.704,Waterloo,"Waterloo IA, USA",OWNO,WATERLOO RGNL,"WATERLOO, IA - UNITED STATES",Waterloo Regional,"Waterloo, Iowa, United States",OK
    ALP,////,////,OSAP,UNK,1.0,Aleppo International Airport (Nejrab Airport),"Aleppo, Syria",OWNO,////,////,Aleppo International Airport,"Aleppo, Syrian Arab Republic (Syria)",OK
    ALQ,////,////,SSLT,UNK,0.667,Federal,"Alegrete, Brazil",OWNO,////,////,Gaudêncio Machado Ramos Airport,"Alegrete, Rio Grande do Sul, Brazil",OK
    ALR,////,////,NZLX,UNK,1.0,Alexandra,"Alexandra, New Zealand",OWNO,////,////,????,"Alexandra, New Zealand",OK
    ALS,ALS,KALS,KALS,OK,0.587,Municipal,"Alamosa (CO), USA",OWNO,SAN LUIS VALLEY RGNL/BERGMAN FIELD,"ALAMOSA, CO - UNITED STATES",San Luis Valley Regional/Bergman Field,"Alamosa, Colorado, United States",OK
    ALT,////,////,SDWQ,UNK,0.667,Alenquer,"Alenquer, Brazil",OWNO,////,////,Aeropuerto Alenquer,"Alenquer, Pará, Brazil",OK
    ALU,////,////,HCMA,UNK,1.0,Alula,"Alula, Somalia",OWNO,////,////,Alula Airport,"Alula, Bari, Somalia",OK
    ALW,ALW,KALW,KALW,OK,0.748,Walla Walla,"Walla Walla (WA), USA",OWNO,WALLA WALLA RGNL,"WALLA WALLA, WA - UNITED STATES",Walla Walla Regional,"Walla Walla, Washington, United States",OK
    ALX,ALX,KALX,KALX,OK,1.0,Thomas C. Russell Field,"Alexander City (AL), USA",OWNO,THOMAS C RUSSELL FLD,"ALEXANDER CITY, AL - UNITED STATES",Thomas C Russell Field,"Alexander City, Alabama, United States",OK

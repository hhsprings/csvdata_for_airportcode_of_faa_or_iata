
List for checking certainty of Great Circle Mapper (KSE - KZS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    KSE,////,////,HUKS,UNK,1.0,Kasese,"Kasese, Uganda",OWNO,////,////,????,"Kasese, Uganda",OK
    KSF,////,////,EDVK,UNK,1.0,Kassel-Calden Airport,"Kassel, Germany",OWNO,////,////,Kassel-Calden,"Kassel, Hessen, Germany",OK
    KSG,////,////,////,UNK,1.0,Kisengan,"Kisengan, Papua New Guinea",OWNO,////,////,????,"Kisengan, Morobe, Papua-New Guinea",OK
    KSH,////,////,OICC,UNK,0.483,Kermanshah,"Kermanshah, Iran",OWNO,////,////,Shahid Ashrafi Esfahani,"Kermanshah, Kermanshah, Iran",OK
    KSI,////,////,GUKU,UNK,1.0,Kissidougou,"Kissidougou, Guinea",OWNO,////,////,????,"Kissidougou, Guinea",OK
    KSJ,////,////,LGKS,UNK,1.0,Kasos Island,"Kasos Island, Greece",OWNO,////,////,????,"Kasos Island, Notío Aigaío (Southern Aegean), Greece",OK
    KSK,////,////,ESKK,UNK,1.0,Karlskoga,"Karlskoga, Sweden",OWNO,////,////,????,"Karlskoga, Örebro län, Sweden",OK
    KSL,////,////,HSKA,UNK,1.0,Kassala,"Kassala, Sudan",OWNO,////,////,????,"Kassala, Kassala, Sudan",OK
    KSM,KSM,PASM,PASM,OK,0.778,Saint Marys,"Saint Marys (AK), USA",OWNO,ST MARY'S,"ST MARY'S, AK - UNITED STATES",St. Mary's Airport,"St. Mary's, Alaska, United States",OK
    KSN,////,////,UAUU,UNK,0.778,Kostanay,"Kostanay, Kazakhstan",OWNO,////,////,Kostanay International Airport,"Kostanay, Qostanay, Kazakhstan",OK
    KSO,////,////,LGKA,UNK,1.0,Aristoteles Airport,"Kastoria, Greece",OWNO,////,////,Aristotelis,"Kastoria, Dytikí Makedonía (Western Macedonia), Greece",OK
    KSP,////,////,////,UNK,1.0,Kosipe,"Kosipe, Papua New Guinea",OWNO,////,////,????,"Kosipe, Central, Papua-New Guinea",OK
    KSQ,////,////,UTSL,UNK,1.0,Karshi,"Karshi, Uzbekistan",OWNO,////,////,Karshi Airport,"Karshi-Khanabad, Qashqadaryo, Uzbekistan",OK
    KSS,////,////,GASK,UNK,1.0,Sikasso,"Sikasso, Mali",OWNO,////,////,????,"Sikasso, Sikasso, Mali",OK
    KST,////,////,HSKI,UNK,1.0,Kosti,"Kosti, Sudan",OWNO,////,////,????,"Kosti, White Nile, Sudan",OK
    KSU,////,////,ENKB,UNK,1.0,Kvernberget,"Kristiansund, Norway",OWNO,////,////,Kvernberget,"Kristiansund, Norway",OK
    KSV,////,////,YSPV,UNK,1.0,Springvale,"Springvale, Australia",OWNO,////,////,????,"Springvale, Queensland, Australia",OK
    KSW,////,////,LLKS,UNK,1.0,Kiryat Shmona,"Kiryat Shmona, Israel",OWNO,////,////,????,"Kiryat Shmona, Israel",OK
    KSX,////,////,////,UNK,1.0,Yasuru,"Yasuru, Papua New Guinea",OWNO,////,////,????,"Yasuru, Morobe, Papua-New Guinea",OK
    KSY,////,////,LTCF,UNK,1.0,Kars,"Kars, Turkey",OWNO,////,////,????,"Kars, Kars, Turkey",OK
    KSZ,////,////,ULKK,UNK,1.0,Kotlas,"Kotlas, Russia",OWNO,////,////,????,"Kotlas, Arkhangel'skaya, Russian Federation (Russia)",OK
    KTA,////,////,YPKA,UNK,1.0,Karratha,"Karratha, Australia",OWNO,////,////,Karratha,"Dampier, Western Australia, Australia",OK
    KTB,KTB,////,////,OK,0.801,Thorne Bay,"Thorne Bay (AK), USA",OWNO,THORNE BAY,"THORNE BAY, AK - UNITED STATES",Thorne Bay SPB,"Thorne Bay, Alaska, United States",OK
    KTC,////,////,////,UNK,1.0,Katiola,"Katiola, Cote d'Ivoire",OWNO,////,////,????,"Katiola, Vallée du Bandama, Côte d'Ivoire (Ivory Coast)",OK
    KTD,////,////,RORK,UNK,1.0,Kitadaito,"Kitadaito, Japan",OWNO,////,////,Kitadaito Airport,"Kitadaito, Kitadaito Island, Okinawa, Japan",OK
    KTE,////,////,WMKE,UNK,1.0,Kerteh,"Kerteh, Malaysia",OWNO,////,////,????,"Kerteh, Terengganu, Malaysia",OK
    KTF,////,////,NZTK,UNK,1.0,Takaka,"Takaka, New Zealand",OWNO,////,////,????,"Takaka, New Zealand",OK
    KTG,////,////,WIOK,UNK,0.667,Ketapang,"Ketapang, Indonesia",OWNO,////,////,Rahadi Oesman Airport,"Ketapang, Kalimantan Barat (West Borneo), Indonesia",OK
    KTH,AK56,////,////,OK,0.628,SPB,"Tikchik (AK), USA",OWNO,TIKCHIK LODGE,"TIKCHIK, AK - UNITED STATES",Tikchik Lodge SPB,"Tikchik, Alaska, United States",OK
    KTI,////,////,VDKT,UNK,1.0,Kratie,"Kratie, Cambodia",OWNO,////,////,????,"Kratie, Cambodia",OK
    KTK,////,////,////,UNK,1.0,Kanua,"Kanua, Papua New Guinea",OWNO,////,////,????,"Kanua, Bougainville, Papua-New Guinea",OK
    KTL,////,////,HKKT,UNK,1.0,Kitale,"Kitale, Kenya",OWNO,////,////,????,"Kitale, Kenya",OK
    KTM,////,////,VNKT,UNK,0.895,Tribhuvan,"Kathmandu, Nepal",OWNO,////,////,Tribhuvan International Airport,"Kathmandu, Nepal",OK
    KTN,KTN,PAKT,PAKT,OK,1.0,International,"Ketchikan (AK), USA",OWNO,KETCHIKAN INTL,"KETCHIKAN, AK - UNITED STATES",Ketchikan International Airport,"Ketchikan, Alaska, United States",OK
    KTO,////,////,SYKT,UNK,1.0,Kato,"Kato, Guyana",OWNO,////,////,????,"Kato, Potaro-Siparuni, Guyana",OK
    KTP,////,////,MKTP,UNK,1.0,Tinson Pen Aerodrome,"Kingston, Jamaica",OWNO,////,////,Tinson Pen,"Kingston, Jamaica",OK
    KTQ,////,////,EFIT,UNK,1.0,Kitee,"Kitee, Finland",OWNO,////,////,????,"Kitee, Pohjois-Karjala (Norra Karelen (North Karelia)), Finland",OK
    KTR,////,////,YPTN,UNK,1.0,Tindal,"Katherine, Australia",OWNO,////,////,Tindal,"Katherine, Northern Territory, Australia",OK
    KTS,KTS,PFKT,PFKT,OK,1.0,Brevig Mission,"Teller Mission (AK), USA",OWNO,BREVIG MISSION,"BREVIG MISSION, AK - UNITED STATES",Brevig Mission Airport,"Brevig Mission, Alaska, United States",OK
    KTT,////,////,EFKT,UNK,1.0,Kittila,"Kittila, Finland",OWNO,////,////,Kittilä Airport,"Kittilä, Lappi (Lappland (Lapland)), Finland",OK
    KTU,////,////,VIKO,UNK,1.0,Kota,"Kota, India",OWNO,////,////,????,"Kota, Rajasthan, India",OK
    KTV,////,////,SVKM,UNK,1.0,Kamarata,"Kamarata, Venezuela",OWNO,////,////,????,"Kamarata, Bolívar, Venezuela",OK
    KTW,////,////,EPKT,UNK,0.643,Pyrzowice,"Katowice, Poland",OWNO,////,////,Katowice International Airport,"Katowice, Slaskie, Poland",OK
    KTX,////,////,GAKO,UNK,1.0,Koutiala,"Koutiala, Mali",OWNO,////,////,????,"Koutiala, Sikasso, Mali",OK
    KTY,////,////,VCCN,UNK,nan,////,////,////,////,////,Katukurunda Airport,"Kalutara, Western Province, Sri Lanka (Ceylon)",UNK
    KUA,////,////,WMKD,UNK,0.526,Kuantan,"Kuantan, Malaysia",OWNO,////,////,Sultan Abmad Shah Airport,"Kuantan, Penang (Pahang), Malaysia",OK
    KUC,////,////,NGKT,UNK,1.0,Kuria,"Kuria, Kiribati",OWNO,////,////,????,"Kuria, Kiribati",OK
    KUD,////,////,WBKT,UNK,1.0,Kudat,"Kudat, Malaysia",OWNO,////,////,????,"Kudat, Sabah, Malaysia",OK
    KUE,////,////,AGKU,UNK,1.0,Kukundu,"Kukundu, Solomon Islands",OWNO,////,////,????,"Kukundu, Kolombangara Island, Solomon Islands",OK
    KUF,////,////,UWWW,UNK,0.571,Samara,"Samara, Russia",OWNO,////,////,Kurumoch Airport,"Samara, Samarskaya, Russian Federation (Russia)",OK
    KUG,////,////,YKUB,UNK,1.0,Kubin Island,"Kubin Island, Australia",OWNO,////,////,????,"Kubin Island, Queensland, Australia",OK
    KUH,////,////,RJCK,UNK,1.0,Kushiro,"Kushiro, Japan",OWNO,////,////,Kushiro Airport,"Kushiro, Hokkaido, Japan",OK
    KUK,Z09,PFKA,PFKA,OK,1.0,Kasigluk,"Kasigluk (AK), USA",OWNO,KASIGLUK,"KASIGLUK, AK - UNITED STATES",????,"Kasigluk, Alaska, United States",OK
    KUL,////,////,WMKK,UNK,1.0,Kuala Lumpur International,"Kuala Lumpur, Malaysia",OWNO,////,////,Kuala Lumpur International Airport,"Kuala Lumpur, Selangor, Malaysia",OK
    KUM,////,////,RJFC,UNK,1.0,Yakushima,"Yakushima, Japan",OWNO,////,////,Yakushima Airport,"Yakushima, Yakushima Island, Satsunan Islands, Kagoshima, Japan",OK
    KUN,////,////,EYKA,UNK,0.706,Kaunas,"Kaunas, Lithuania",OWNO,////,////,Kaunas International Airport,"Kaunas, Lithuania",OK
    KUO,////,////,EFKU,UNK,1.0,Kuopio,"Kuopio, Finland",OWNO,////,////,Kuopio Airport,"Kuopio, Pohjois-Savo (Norra Savolax (Northern Savonia)), Finland",OK
    KUP,////,////,////,UNK,1.0,Kupiano,"Kupiano, Papua New Guinea",OWNO,////,////,????,"Kupiano, Central, Papua-New Guinea",OK
    KUQ,////,////,////,UNK,1.0,Kuri,"Kuri, Papua New Guinea",OWNO,////,////,????,"Kuri, Gulf, Papua-New Guinea",OK
    KUR,////,////,OARZ,UNK,0.741,Kuran-O-Munjan,"Kuran-O-Munjan, Afghanistan",OWNO,////,////,Razer,"Karanomunjan, Badakhshan, Afghanistan",OK
    KUS,////,////,BGKK,UNK,1.0,Kulusuk,"Kulusuk, Greenland",OWNO,////,////,????,"Kulusuk, Sermersooq, Greenland",OK
    KUT,////,////,UGKO,UNK,0.333,Kutaisi,"Kutaisi, Georgia",OWNO,////,////,King David the Builder Kutaisi International Airport,"Kutaisi, Imereti, Georgia",OK
    KUU,////,////,VIBR,UNK,0.88,Kullu Manali Airport,"Bhuntar, Himachal Pradesh, India",OWNO,////,////,Bhuntar,"Kulu, Himachal Pradesh, India",OK
    KUV,////,////,RKJK,UNK,0.833,Kunsan,"Kunsan, South Korea",OWNO,////,////,Gunsan,"Kunsan, Republic of Korea (South Korea)",OK
    KVA,////,////,LGKV,UNK,0.905,Megas Alexandros Airport,"Kavala, Greece",OWNO,////,////,Megas Alexandros International Airport,"Kavala, Anatolikí Makedonía kai Thráki (Eastern Macedonia and Thrace), Greece",OK
    KVB,////,////,ESGR,UNK,1.0,Skovde,"Skovde, Sweden",OWNO,////,////,????,"Skövde, Västra Götalands län, Sweden",OK
    KVC,KVC,PAVC,PAVC,OK,1.0,King Cove,"King Cove (AK), USA",OWNO,KING COVE,"KING COVE, AK - UNITED STATES",????,"King Cove, Alaska, United States",OK
    KVD,////,////,UBBG,UNK,0.462,Gyandzha,"Gyandzha, Azerbaijan",OWNO,////,////,Ganja International Airport,"Ganja, Göygöl, Azerbaijan",TO DO CHECK
    KVE,////,////,////,UNK,1.0,Kitava,"Kitava, Papua New Guinea",OWNO,////,////,????,"Kitava, Milne Bay, Papua-New Guinea",OK
    KVG,////,////,AYKV,UNK,1.0,Kavieng,"Kavieng, Papua New Guinea",OWNO,////,////,????,"Kavieng, New Ireland, Papua-New Guinea",OK
    KVK,////,////,ULMK,UNK,0.545,Kirovsk,"Kirovsk, Russia",OWNO,////,////,Khibiny Airport,"Apatity, Murmanskaya, Russian Federation (Russia)",TO DO CHECK
    KVL,KVL,PAVL,PAVL,OK,1.0,Kivalina,"Kivalina (AK), USA",OWNO,KIVALINA,"KIVALINA, AK - UNITED STATES",????,"Kivalina, Alaska, United States",OK
    KVM,////,////,UHMO,UNK,nan,////,////,////,////,////,????,"Markovo, Chukotskiy, Russian Federation (Russia)",UNK
    KVR,////,////,UHWK,UNK,nan,////,////,////,////,////,????,"Kavalerovo, Primorskiy, Russian Federation (Russia)",UNK
    KVX,////,////,USKK,UNK,0.526,Kirov,"Kirov, Russia",OWNO,////,////,Pobedilovo Airport,"Kirov, Kirovskaya, Russian Federation (Russia)",OK
    KWA,KWA,PKWA,PKWA,OK,0.397,Kwajalein,"Kwajalein, Marshall Islands",OWNO,BUCHOLZ AAF(KWAJALEIN KMR)(ATOL),"KWAJALEIN, - MARSHALL ISLANDS",Bucholz AAF Airport,"Kwajalein Atoll, Marshall Islands",MAYBE
    KWE,////,////,ZUGY,UNK,0.638,Guiyang,"Guiyang, PR China",OWNO,////,////,Guiyang Longdongbao International Airport,"Guiyang, Guizhou, China",OK
    KWG,////,////,UKDR,UNK,0.4,Krivoy Rog,"Krivoy Rog, Ukraine",OWNO,////,////,Lozuvatka International Airport,"Krivyi Rih, Dnipropetrovsk, Ukraine",TO DO CHECK
    KWI,////,////,OKBK,UNK,1.0,International,"Kuwait, Kuwait",OWNO,////,////,Kuwait International Airport,"Kuwait City, Kuwait",MAYBE
    KWJ,////,////,RKJJ,UNK,0.857,Kwangju,"Kwangju, South Korea",OWNO,////,////,????,"Gwangju, Republic of Korea (South Korea)",TO DO CHECK
    KWK,GGV,PAGG,PAGG,OK,1.0,Kwigillingok,"Kwigillingok (AK), USA",OWNO,KWIGILLINGOK,"KWIGILLINGOK, AK - UNITED STATES",????,"Kwigillingok, Alaska, United States",OK
    KWL,////,////,ZGKL,UNK,0.667,Guilin,"Guilin, PR China",OWNO,////,////,Liangjiang International Airport,"Guilin, Guangxi, China",OK
    KWM,////,////,YKOW,UNK,1.0,Kowanyama,"Kowanyama, Australia",OWNO,////,////,????,"Kowanyama, Queensland, Australia",OK
    KWN,AQH,PAQH,PAQH,OK,0.79,Kwinhagak,"Quinhagak (AK), USA",OWNO,QUINHAGAK,"QUINHAGAK, AK - UNITED STATES",????,"Quinhagak, Alaska, United States",OK
    KWP,KWP,////,////,OK,0.857,Village SPB,"West Point (AK), USA",OWNO,WEST POINT VILLAGE,"WEST POINT, AK - UNITED STATES",West Point Village SPB,"West Point, Alaska, United States",OK
    KWY,////,////,////,UNK,1.0,Kiwayu,"Kiwayu, Kenya",OWNO,////,////,????,"Kiwayu, Kenya",OK
    KWZ,////,////,FZQM,UNK,1.0,Kolwezi,"Kolwezi, Congo (DRC)",OWNO,////,////,Kolwezi Airport,"Kolwezi, Katanga, Democratic Republic of Congo (Zaire)",OK
    KXE,////,////,FAKD,UNK,1.0,Klerksdorp,"Klerksdorp, South Africa",OWNO,////,////,Klerksdorp Airport,"Klerksdorp, North West, South Africa",OK
    KXF,////,////,NFNO,UNK,1.0,Koro Island,"Koro Island, Fiji",OWNO,////,////,????,"Koro Island, Fiji",OK
    KXK,////,////,UHKK,UNK,0.882,Komsomolsk Na Amure,"Komsomolsk Na Amure, Russia",OWNO,////,////,Khurba,"Komsomolsk-na-Amur, Khabarovskiy, Russian Federation (Russia)",OK
    KYA,////,////,LTAN,UNK,1.0,Konya,"Konya, Turkey",OWNO,////,////,????,"Konya, Konya, Turkey",OK
    KYD,////,////,RCLY,UNK,1.0,Lanyu Airport,"Orchid Island, Taiwan",OWNO,////,////,????,"Lanyu, Taiwan",TO DO CHECK
    KYE,////,////,OLKA,UNK,0.727,Kleyate Airport (AFB?),"Tripoli, Lebanon",OWNO,////,////,Rene Mouawad AB,"Kleyate/Tripoli, Lebanon",OK
    KYI,////,////,YYTA,UNK,1.0,Yalata Mission Airport,"Yalata Mission, Australia",OWNO,////,////,????,"Yalata Mission, South Australia, Australia",OK
    KYK,KYK,PAKY,PAKY,OK,1.0,Karluk Airport,"Karluk (AK), USA",OWNO,KARLUK,"KARLUK, AK - UNITED STATES",????,"Karluk, Alaska, United States",OK
    KYP,////,////,VYKP,UNK,1.0,Kyaukpyu Airport,"Kyaukpyu, Myanmar",OWNO,////,////,????,"Kyaukpyu, Rakhine, Myanmar (Burma)",OK
    KYS,////,////,GAKY,UNK,1.0,Dag-Dag,Kayes,IATA,////,////,Dag Dag,"Kayes, Kayes, Mali",MAYBE
    KYU,KYU,PFKU,PFKU,OK,1.0,Koyukuk,"Koyukuk (AK), USA",OWNO,KOYUKUK,"KOYUKUK, AK - UNITED STATES",????,"Koyukuk, Alaska, United States",OK
    KYZ,////,////,UNKY,UNK,1.0,Kyzyl,"Kyzyl, Russia",OWNO,////,////,Kyzyl Airport,"Kyzyl, Tyva (Tuva), Russian Federation (Russia)",OK
    KZB,////,////,////,UNK,1.0,Zachar Bay SPB,"Zachar Bay (AK), USA",OWNO,////,////,Zachar Bay SPB,"Zachar Bay, Alaska, United States",OK
    KZC,////,////,VDKH,UNK,1.0,Kampong Chhnang Airport,"Kampong Chhnang, Cambodia",OWNO,////,////,????,"Kompong-Chhnang, Cambodia",OK
    KZF,////,////,////,UNK,1.0,Kaintiba,"Kaintiba, Papua New Guinea",OWNO,////,////,????,"Kaintiba, Gulf, Papua-New Guinea",OK
    KZI,////,////,LGKZ,UNK,1.0,Philippos Airport,"Kozani, Greece",OWNO,////,////,Filippos,"Kozani, Dytikí Makedonía (Western Macedonia), Greece",OK
    KZN,////,////,UWKD,UNK,1.0,Kazan,"Kazan, Russia",OWNO,////,////,Kazan Airport,"Kazan, Tatarstan, Russian Federation (Russia)",OK
    KZO,////,////,UAOO,UNK,0.933,Kzyl-Orda,"Kzyl-Orda, Kazakhstan",OWNO,////,////,Kyzylorda Airport,"Kyzylorda, Qyzylorda, Kazakhstan",TO DO CHECK
    KZR,////,////,LTBZ,UNK,1.0,Zafer Airport,"Kütahya, Turkey",OWNO,////,////,Zafer Airport,"Kütahya, Turkey",OK
    KZS,////,////,LGKJ,UNK,0.611,Kastelorizo Island Public Airport,"Kastelorizo, Greece",OWNO,////,////,????,"Kastelorizo, Notío Aigaío (Southern Aegean), Greece",OK


List for checking certainty of Great Circle Mapper (MWD - MZZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MWD,////,////,OPMI,UNK,0.842,Mianwali,"Mianwali, Pakistan",OWNO,////,////,Mianwali AB,"Mianwali, Punjab, Pakistan",OK
    MWE,////,////,HSMR,UNK,1.0,Merowe,"Merowe, Sudan",OWNO,////,////,????,"Merowe, Northern, Sudan",OK
    MWF,////,////,NVSN,UNK,0.588,Maewo,"Maewo, Vanuatu",OWNO,////,////,????,"Naone, Maewo Island, Pénama, Vanuatu",MAYBE
    MWG,////,////,////,UNK,1.0,Marawaka,"Marawaka, Papua New Guinea",OWNO,////,////,????,"Marawaka, Eastern Highlands, Papua-New Guinea",OK
    MWH,MWH,KMWH,KMWH,OK,0.797,Grant County,"Moses Lake (WA), USA",OWNO,GRANT CO INTL,"MOSES LAKE, WA - UNITED STATES",Grant Co. International Airport,"Moses Lake, Washington, United States",OK
    MWI,////,////,////,UNK,1.0,Maramuni,"Maramuni, Papua New Guinea",OWNO,////,////,????,"Maramuni, Enga, Papua-New Guinea",OK
    MWJ,////,////,SYMR,UNK,1.0,Matthews Ridge,"Matthews Ridge, Guyana",OWNO,////,////,????,"Matthews Ridge, Barima-Waini, Guyana",OK
    MWK,////,////,WIOM,UNK,0.625,Matak,"Matak, Indonesia",OWNO,////,////,Tarempa,"Matak, Riau, Indonesia",OK
    MWL,MWL,KMWL,KMWL,OK,1.0,Mineral Wells Airport,"Mineral Wells (TX), USA",OWNO,MINERAL WELLS,"MINERAL WELLS, TX - UNITED STATES",????,"Mineral Wells, Texas, United States",OK
    MWM,MWM,KMWM,KMWM,OK,1.0,Municipal,"Windom (MN), USA",OWNO,WINDOM MUNI,"WINDOM, MN - UNITED STATES",Windom Municipal Airport,"Windom, Minnesota, United States",OK
    MWN,////,////,HTMD,UNK,1.0,Mwadui,"Mwadui, Tanzania",OWNO,////,////,Mwadui Airport,"Mwadui, Shinyanga, Tanzania",OK
    MWO,MWO,KMWO,KMWO,OK,1.0,Hook Field,"Middletown (OH), USA",OWNO,MIDDLETOWN REGIONAL/HOOK FIELD,"MIDDLETOWN, OH - UNITED STATES",Middletown Regional/Hook Field,"Middletown, Ohio, United States",OK
    MWP,////,////,////,UNK,1.0,Mountain,"Mountain, Nepal",OWNO,////,////,????,"Mountain, Nepal",OK
    MWQ,////,////,VYMW,UNK,1.0,Magwe,"Magwe, Myanmar",OWNO,////,////,????,"Magwe, Magway, Myanmar (Burma)",OK
    MWX,////,////,RKJB,UNK,nan,////,////,////,////,////,Muan International Airport,"Muan, Republic of Korea (South Korea)",UNK
    MWZ,////,////,HTMW,UNK,1.0,Mwanza,"Mwanza, Tanzania",OWNO,////,////,Mwanza Airport,"Mwanza, Mwanza, Tanzania",OK
    MXB,////,////,WAWM,UNK,0.56,Masamba,"Masamba, Indonesia",OWNO,////,////,Andi Jemma,"Masamba, Sulawesi Selatan, Indonesia",OK
    MXE,MEB,KMEB,KMEB,OK,0.64,Maxton,"Maxton (NC), USA",OWNO,LAURINBURG-MAXTON,"MAXTON, NC - UNITED STATES",Laurinburg-Maxton Airport,"Maxton, North Carolina, United States",OK
    MXF,MXF,KMXF,KMXF,OK,1.0,Maxwell AFB,"Montgomery (AL), USA",OWNO,MAXWELL AFB,"MONTGOMERY, AL - UNITED STATES",Maxwell AFB,"Montgomery, Alabama, United States",OK
    MXH,////,////,AYMR,UNK,1.0,Moro,"Moro, Papua New Guinea",OWNO,////,////,????,"Moro, Southern Highlands, Papua-New Guinea",OK
    MXJ,////,////,DNMN,UNK,1.0,Minna,"Minna, Nigeria",OWNO,////,////,Minna Airport,"Minna, Niger, Nigeria",OK
    MXL,////,////,MMML,UNK,0.302,Mexicali,"Mexicali, Mexico",OWNO,////,////,General Rodolfo Sánchez Taboada International Airport,"Mexicali, Baja California, México",OK
    MXM,////,////,FMSR,UNK,1.0,Morombe,"Morombe, Madagascar",OWNO,////,////,????,"Morombe, Madagascar",OK
    MXN,////,////,LFRU,UNK,0.609,Morlaix,"Morlaix, France",OWNO,////,////,Ploujean,"Morlaix, Bretagne, France",OK
    MXO,MXO,KMXO,KMXO,OK,0.722,Municipal,"Monticello IA, USA",OWNO,MONTICELLO RGNL,"MONTICELLO, IA - UNITED STATES",Monticello Regional,"Monticello, Iowa, United States",OK
    MXP,////,////,LIMC,UNK,1.0,Malpensa,"Milan, Italy",OWNO,////,////,Malpensa,"Milan, Lombardy, Italy",OK
    MXS,////,////,NSMA,UNK,0.857,Maota Savaii Is,"Maota Savaii Is, Samoa",OWNO,////,////,????,"Maota, Savai'i Island, Samoa (Western Samoa)",MAYBE
    MXT,////,////,FMMO,UNK,1.0,Maintirano,"Maintirano, Madagascar",OWNO,////,////,????,"Maintirano, Madagascar",OK
    MXV,////,////,ZMMN,UNK,1.0,Moron,"Moron, Mongolia",OWNO,////,////,Mörön Airport,"Mörön, Mongolia",OK
    MXX,////,////,ESKM,UNK,0.533,Mora,"Mora, Sweden",OWNO,////,////,Siljan,"Mora, Dalarnas län, Sweden",OK
    MXY,15Z,PAMX,PAMX,OK,1.0,Mccarthy,"Mccarthy (AK), USA",OWNO,MCCARTHY,"MCCARTHY, AK - UNITED STATES",????,"McCarthy, Alaska, United States",OK
    MXZ,////,////,ZGMX,UNK,1.0,Meixian,"Meixian, PR China",OWNO,////,////,Meixian,"Meizhou, Guangdong, China",OK
    MYA,////,////,YMRY,UNK,1.0,Moruya,"Moruya, Australia",OWNO,////,////,????,"Moruya, New South Wales, Australia",OK
    MYB,////,////,FOOY,UNK,1.0,Mayoumba,"Mayoumba, Gabon",OWNO,////,////,????,"Mayumba, Gabon",OK
    MYC,////,////,SVBS,UNK,0.556,Maracay,"Maracay, Venezuela",OWNO,////,////,Mariscal Sucre Airport,"Maracay, Carabobo, Venezuela",OK
    MYD,////,////,HKML,UNK,1.0,Malindi,"Malindi, Kenya",OWNO,////,////,????,"Malindi, Kenya",OK
    MYE,////,////,RJTQ,UNK,0.6,Miyake Jima,"Miyake Jima, Japan",OWNO,////,////,Miyakejima Airport,"Miyakejima, Miyakejima, Tokyo, Japan",OK
    MYF,MYF,KMYF,KMYF,OK,0.643,Montgomery Field,"San Diego (CA), USA",OWNO,MONTGOMERY-GIBBS EXECUTIVE,"SAN DIEGO, CA - UNITED STATES",Montgomery-Gibbs Executive Airport,"San Diego, California, United States",OK
    MYG,////,MYMM,MYMM,OK,0.609,Mayaguana,"Mayaguana, Bahamas",OWNO,MAYAGUANA,"ABRAHAMS BAY, - BAHAMAS",????,"Abraham's Bay, Mayaguana Island, Bahamas",MAYBE
    MYI,////,////,YMUI,UNK,1.0,Murray Island,"Murray Island, Australia",OWNO,////,////,????,"Murray Island, Torres Strait Islands, Queensland, Australia",OK
    MYJ,////,////,RJOM,UNK,1.0,Matsuyama,"Matsuyama, Japan",OWNO,////,////,Matsuyama Airport,"Matsuyama, Skikoku, Ehime, Japan",OK
    MYL,MYL,KMYL,KMYL,OK,0.309,Mccall,"Mccall (ID), USA",OWNO,MC CALL MUNI,"MC CALL, ID - UNITED STATES",Mc Call Municipal Airport,"Mc Call, Idaho, United States",OK
    MYP,////,////,UTAM,UNK,0.615,Mary,"Mary, Turkmenistan",OWNO,////,////,Mary International Airport,"Mary, Mary, Turkmenistan",OK
    MYQ,////,////,VOMY,UNK,1.0,Mysore,"Mysore, India",OWNO,////,////,????,"Mysore, Karnataka, India",OK
    MYR,MYR,KMYR,KMYR,OK,0.806,Myrtle Beach AFB,"Myrtle Beach (SC), USA",OWNO,MYRTLE BEACH INTL,"MYRTLE BEACH, SC - UNITED STATES",Myrtle Beach International Airport,"Myrtle Beach, South Carolina, United States",OK
    MYT,////,////,VYMK,UNK,0.75,Myitkyina,"Myitkyina, Myanmar",OWNO,////,////,Pamti,"Myitkyina, Kachin, Myanmar (Burma)",OK
    MYU,MYU,PAMY,PAMY,OK,0.621,Ellis Field,"Mekoryuk (AK), USA",OWNO,MEKORYUK,"MEKORYUK, AK - UNITED STATES",????,"Mekoryuk, Alaska, United States",OK
    MYV,MYV,KMYV,KMYV,OK,1.0,Yuba County,"Marysville (CA), USA",OWNO,YUBA COUNTY,"MARYSVILLE, CA - UNITED STATES",Yuba County Airport,"Marysville, California, United States",OK
    MYW,////,////,HTMT,UNK,1.0,Mtwara,"Mtwara, Tanzania",OWNO,////,////,Mtwara Airport,"Mtwara, Mtwara, Tanzania",OK
    MYY,////,////,WBGR,UNK,1.0,Miri,"Miri, Malaysia",OWNO,////,////,????,"Miri, Sarawak, Malaysia",OK
    MZB,////,////,FQMP,UNK,0.903,Mocimboa Praia,"Mocimboa Praia, Mozambique",OWNO,////,////,????,"Moçimboa de Praia, Mozambique",MAYBE
    MZG,////,////,RCQC,UNK,0.833,Makung,"Makung, Taiwan",OWNO,////,////,????,"Magong, Taiwan",OK
    MZH,////,////,LTAP,UNK,1.0,Merzifon,"Merzifon, Turkey",OWNO,////,////,????,"Merzifon, Amasya, Turkey",OK
    MZI,////,////,GAMB,UNK,0.5,Mopti,"Mopti, Mali",OWNO,////,////,Sevare AB,"Mopti Ambodedjo, Mopti, Mali",OK
    MZJ,MZJ,KMZJ,KMZJ,OK,0.445,Marana,"Marana (AZ), USA",OWNO,PINAL AIRPARK,"MARANA, AZ - UNITED STATES",Pinal Airpark,"Marana, Arizona, United States",OK
    MZK,////,////,NGMK,UNK,1.0,Marakei,"Marakei, Kiribati",OWNO,////,////,????,"Marakei, Kiribati",OK
    MZL,////,////,SKMZ,UNK,0.6,Santaguida,"Manizales, Colombia",OWNO,////,////,La Nubia Airport,"Manizales, Caldas, Colombia",OK
    MZM,////,////,LFSF,UNK,1.0,Frescaty,"Metz, France",OWNO,////,////,Frescaty,"Metz, Lorraine, France",OK
    MZO,////,////,MUMZ,UNK,1.0,Sierra Maestra,"Manzanillo, Cuba",OWNO,////,////,Sierra Maestra,"Manzanillo, Granma, Cuba",OK
    MZP,////,////,NZMK,UNK,1.0,Motueka,"Motueka, New Zealand",OWNO,////,////,????,"Motueka, New Zealand",OK
    MZQ,////,////,FAMU,UNK,1.0,Mkuze,"Mkuze, South Africa",OWNO,////,////,Mkuze Airport,"Mkuze, KwaZulu-Natal, South Africa",OK
    MZR,////,////,OAMS,UNK,0.848,Mazar-I-Sharif,"Mazar I Sharif, Afghanistan",OWNO,////,////,Mazar-e Sharif International Airport,"Mazar-i-Sharif, Balkh, Afghanistan",OK
    MZT,////,////,MMMZ,UNK,0.878,Gen. Rafael Buelna,"Mazatlan, Mexico",OWNO,////,////,General Rafael Buelna International Airport,"Mazatlán, Sinaloa, México",OK
    MZU,////,////,VEMZ,UNK,1.0,Muzaffarpur,"Muzaffarpur, India",OWNO,////,////,????,"Muzaffarpur, Bihar, India",OK
    MZV,////,////,WBMU,UNK,1.0,Mulu,"Mulu, Malaysia",OWNO,////,////,????,"Mulu, Sarawak, Malaysia",OK
    MZW,////,////,DAAY,UNK,nan,////,////,////,////,////,Mécheria Airport,"Mécheria, Naama, Algeria",UNK
    MZZ,MZZ,KMZZ,KMZZ,OK,0.841,Marion,"Marion (IN), USA",OWNO,MARION MUNI,"MARION, IN - UNITED STATES",Marion Municipal Airport,"Marion, Indiana, United States",OK

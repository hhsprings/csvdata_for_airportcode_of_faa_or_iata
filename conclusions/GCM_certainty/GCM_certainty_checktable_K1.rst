
List for checking certainty of Great Circle Mapper (KAA - KFS)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    KAA,////,////,FLKS,UNK,1.0,Kasama,"Kasama, Zambia",OWNO,////,////,????,"Kasama, Zambia",OK
    KAB,////,////,FVKB,UNK,0.714,Kariba,"Kariba, Zimbabwe",OWNO,////,////,Kariba International Airport,"Kariba, Zimbabwe",OK
    KAC,////,////,OSKL,UNK,1.0,Kameshli,"Kameshli, Syria",OWNO,////,////,????,"Kameshli, Syrian Arab Republic (Syria)",OK
    KAD,////,////,DNKA,UNK,1.0,Kaduna,"Kaduna, Nigeria",OWNO,////,////,Kaduna Airport,"Kaduna, Kaduna, Nigeria",OK
    KAE,AFE,PAFE,PAFE,OK,0.716,SPB,"Kake (AK), USA",OWNO,KAKE,"KAKE, AK - UNITED STATES",????,"Kake, Alaska, United States",OK
    KAF,////,////,////,UNK,1.0,Karato,"Karato, Papua New Guinea",OWNO,////,////,????,"Karato, Bougainville, Papua-New Guinea",OK
    KAG,////,////,RKNN,UNK,1.0,Kangnung,"Kangnung, South Korea",OWNO,////,////,????,"Kangnung, Republic of Korea (South Korea)",OK
    KAI,////,////,SYKA,UNK,1.0,Kaieteur,"Kaieteur, Guyana",OWNO,////,////,????,"Kaieteur, Potaro-Siparuni, Guyana",OK
    KAJ,////,////,EFKI,UNK,1.0,Kajaani,"Kajaani, Finland",OWNO,////,////,????,"Kajaani, Kainuu (Kajanaland (Kainuu)), Finland",OK
    KAK,////,////,////,UNK,1.0,Kar,"Kar, Papua New Guinea",OWNO,////,////,????,"Kar, Southern Highlands, Papua-New Guinea",OK
    KAL,KAL,PAKV,PAKV,OK,1.0,Kaltag,"Kaltag (AK), USA",OWNO,KALTAG,"KALTAG, AK - UNITED STATES",????,"Kaltag, Alaska, United States",OK
    KAM,////,////,////,UNK,0.7,Kamaran Island,"Kamaran Island, Yemen",OWNO,////,////,????,"Kamaran, Yemen",MAYBE
    KAN,////,////,DNKN,UNK,0.732,Aminu Kano International Apt,"Kano, Nigeria",OWNO,////,////,Mallam Aminu Kano International Airport,"Kano, Kano, Nigeria",OK
    KAO,////,////,EFKS,UNK,1.0,Kuusamo,"Kuusamo, Finland",OWNO,////,////,????,"Kuusamo, Pohjois-Pohjanmaa (Norra Österbotten (Northern Ostrobothnia)), Finland",OK
    KAP,////,////,FZSK,UNK,1.0,Kapanga,"Kapanga, Congo (DRC)",OWNO,////,////,Kapanga Airport,"Kapanga, Katanga, Democratic Republic of Congo (Zaire)",OK
    KAQ,////,////,////,UNK,1.0,Kamulai,"Kamulai, Papua New Guinea",OWNO,////,////,????,"Kamulai, Central, Papua-New Guinea",OK
    KAR,////,////,SYKM,UNK,1.0,Kamarang,"Kamarang, Guyana",OWNO,////,////,????,"Kamarang, Cuyuni-Mazaruni, Guyana",OK
    KAS,////,////,FYKB,UNK,1.0,Karasburg,"Karasburg, Namibia",OWNO,////,////,????,"Karasburg, Namibia",OK
    KAT,////,////,NZKT,UNK,1.0,Kaitaia,"Kaitaia, New Zealand",OWNO,////,////,????,"Kaitaia, New Zealand",OK
    KAU,////,////,EFKA,UNK,1.0,Kauhava,"Kauhava, Finland",OWNO,////,////,????,"Kauhava, Etelä-Pohjanmaa (Södra Österbotten (Southern Ostrobothnia)), Finland",OK
    KAV,////,////,SVKA,UNK,1.0,Kavanayen,"Kavanayen, Venezuela",OWNO,////,////,????,"Kavanayén, Bolívar, Bolívar, Venezuela",OK
    KAW,////,////,VYKT,UNK,1.0,Kawthaung,"Kawthaung, Myanmar",OWNO,////,////,????,"Kawthaung, Taninthayi, Myanmar (Burma)",OK
    KAX,////,////,YKBR,UNK,1.0,Kalbarri,"Kalbarri, Australia",OWNO,////,////,????,"Kalbarri, Western Australia, Australia",OK
    KAY,////,////,NFNW,UNK,1.0,Wakaya Island,"Wakaya Island, Fiji",OWNO,////,////,????,"Wakaya Island, Fiji",OK
    KAZ,////,////,WAMK,UNK,1.0,Kau,"Kau, Indonesia",OWNO,////,////,????,"Kao, Maluku Utara, Indonesia",OK
    KBA,////,////,GFKB,UNK,1.0,Kabala,"Kabala, Sierra Leone",OWNO,////,////,Kabala Airport,"Kabala, Sierra Leone",OK
    KBB,////,////,////,UNK,1.0,Kirkimbie,"Kirkimbie, Australia",OWNO,////,////,????,"Kirkimbie, Northern Territory, Australia",OK
    KBC,Z91,////,////,OK,1.0,Birch Creek,"Birch Creek (AK), USA",OWNO,BIRCH CREEK,"BIRCH CREEK, AK - UNITED STATES",????,"Birch Creek, Alaska, United States",OK
    KBD,////,////,////,UNK,1.0,Kimberley Downs,"Kimberley Downs, Australia",OWNO,////,////,????,"Kimberley Downs, Western Australia, Australia",OK
    KBE,KBE,////,////,OK,0.882,Hot Springs SPB,"Bell Island (AK), USA",OWNO,BELL ISLAND HOT SPRINGS,"BELL ISLAND, AK - UNITED STATES",Bell Island Hot Springs SPB,"Bell Island, Alaska, United States",OK
    KBF,////,////,////,UNK,1.0,Karubaga,"Karubaga, Indonesia",OWNO,////,////,????,"Karubaga, Papua, Indonesia",OK
    KBG,////,////,HUKF,UNK,1.0,Kabalega Falls,"Kabalega Falls, Uganda",OWNO,////,////,????,"Kabalega Falls, Uganda",OK
    KBI,////,////,FKKB,UNK,1.0,Kribi,"Kribi, Cameroon",OWNO,////,////,Kribi Airport,"Kribi, South (Sud), Cameroon",OK
    KBJ,////,////,YKCA,UNK,1.0,King Canyon,"Kings Canyon, Australia",OWNO,////,////,????,"Kings Canyon, Northern Territory, Australia",OK
    KBL,////,////,OAKB,UNK,0.563,Khwaja Rawash,"Kabul, Afghanistan",OWNO,////,////,Hamid Karzai International Airport,"Kabul, Kabul, Afghanistan",OK
    KBM,////,////,////,UNK,1.0,Kabwum,"Kabwum, Papua New Guinea",OWNO,////,////,????,"Kabwum, Morobe, Papua-New Guinea",OK
    KBN,////,////,FZWT,UNK,0.706,Kabinda,"Kabinda, Congo (DRC)",OWNO,////,////,Tunta Airport,"Kabinda, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    KBO,////,////,FZRM,UNK,1.0,Kabalo,"Kabalo, Congo (DRC)",OWNO,////,////,????,"Kabalo, Katanga, Democratic Republic of Congo (Zaire)",OK
    KBP,////,////,UKBB,UNK,0.846,Borispol,"Kiev, Ukraine",OWNO,////,////,Boryspil International Airport,"Kiev, Kiev, Ukraine",OK
    KBQ,////,////,FWKG,UNK,1.0,Kasungu,"Kasungu, Malawi",OWNO,////,////,????,"Kasungu, Malawi",OK
    KBR,////,////,WMKC,UNK,0.682,Pengkalan Chepa,"Kota Bharu, Malaysia",OWNO,////,////,Sultan Ismail Petra,"Kota Bharu, Kelantan, Malaysia",OK
    KBS,////,////,GFBO,UNK,1.0,Bo,"Bo, Sierra Leone",OWNO,////,////,????,"Bo, Sierra Leone",OK
    KBT,////,////,////,UNK,1.0,Kaben,"Kaben, Marshall Islands",OWNO,////,////,????,"Kaben, Marshall Islands",OK
    KBU,////,////,WAOK,UNK,0.435,Kotabaru,"Kotabaru, Indonesia",OWNO,////,////,Gusti Sjamsir Alam Airport,"Stagen, Laut Island, Kalimantan Selatan (South Borneo), Indonesia",TO DO CHECK
    KBV,////,////,VTSG,UNK,1.0,Krabi,"Krabi, Thailand",OWNO,////,////,????,"Krabi, Krabi, Thailand",OK
    KBW,Z78,////,////,OK,0.815,Chignik Bay,"Chignik (AK), USA",OWNO,CHIGNIK BAY,"CHIGNIK, AK - UNITED STATES",Chignik Bay SPB,"Chignik, Alaska, United States",OK
    KBX,////,////,////,UNK,1.0,Kambuaya,"Kambuaya, Indonesia",OWNO,////,////,????,"Kambuaya, Papua, Indonesia",OK
    KBY,////,////,YKBY,UNK,1.0,Streaky Bay,"Streaky Bay, Australia",OWNO,////,////,????,"Streaky Bay, South Australia, Australia",OK
    KBZ,////,////,NZKI,UNK,1.0,Kaikoura,"Kaikoura, New Zealand",OWNO,////,////,????,"Kaikoura, New Zealand",OK
    KCA,////,////,ZWKC,UNK,0.846,Kuqa Qiuci Airport,"Kuqa, Xinjiang, China",OWNO,////,////,????,"Kuqa, Xinjiang, China",OK
    KCB,////,////,SMTP,UNK,1.0,Tepoe Airstrip,"Kasikasima, Suriname",OWNO,////,////,Tepoe Airstrip,"Tepoe, Sipaliwini, Suriname",OK
    KCC,KCC,////,////,OK,0.819,Coffman Cove SPB,"Coffman Cove (AK), USA",OWNO,COFFMAN COVE,"COFFMAN COVE, AK - UNITED STATES",Coffman Cove SPB,"Coffman Cove, Alaska, United States",OK
    KCE,////,////,YCSV,UNK,1.0,Collinsville,"Collinsville, Australia",OWNO,////,////,????,"Collinsville, Queensland, Australia",OK
    KCF,////,////,OPKW,UNK,0.8,Kadanwari,"Kadanwari, Pakistan",OWNO,////,////,Thar Airport,"Kadanwari, Sindh, Pakistan",OK
    KCG,KCG,////,////,UNK,1.0,Fisheries,"Chignik (AK), USA",OWNO,////,////,Chignik Fisheries Airport,"Chignik, Alaska, United States",OK
    KCH,////,////,WBGG,UNK,1.0,Kuching,"Kuching, Malaysia",OWNO,////,////,????,"Kuching, Sarawak, Malaysia",OK
    KCK,////,////,UIKK,UNK,1.0,Kirensk,Kirensk,IATA,////,////,Kirensk Airport,"Kirensk, Irkutskaya, Russian Federation (Russia)",MAYBE
    KCL,KCL,////,////,OK,1.0,Lagoon,"Chignik (AK), USA",OWNO,CHIGNIK LAGOON,"CHIGNIK LAGOON, AK - UNITED STATES",????,"Chignik Lagoon, Alaska, United States",OK
    KCM,////,////,LTCN,UNK,1.0,Kahramanmaras,"Kahramanmaras, Turkey",OWNO,////,////,????,"Kahramanmaras, Kahramanmaras, Turkey",OK
    KCN,KCN,////,////,UNK,0.9,SPB,"Chernofski (AK), USA",OWNO,////,////,Chernofski Harbor SPB,"Chernofski Harbor, Alaska, United States",OK
    KCO,////,////,LTBQ,UNK,0.919,Cengiz Topel,"Kocaeli, Turkey",OWNO,////,////,Cengiz Topel NAS,"Köseköy, Kocaeli, Turkey",OK
    KCP,////,////,////,UNK,1.0,Kamenets-Podolskiy,"Kamenets-Podolskiy, Ukraine",OWNO,////,////,????,"Kamenets-Podolskiy, Khmelnytskyi, Ukraine",OK
    KCQ,AJC,PAJC,PAJC,OK,1.0,Chignik,"Chignik (AK), USA",OWNO,CHIGNIK,"CHIGNIK, AK - UNITED STATES",????,"Chignik, Alaska, United States",OK
    KCR,KCR,////,////,OK,1.0,Colorado Creek,"Colorado Creek (AK), USA",OWNO,COLORADO CREEK,"COLORADO CREEK, AK - UNITED STATES",????,"Colorado Creek, Alaska, United States",OK
    KCS,////,////,YKCS,UNK,0.733,Kings Creek Station,"Kings Creek Station, Australia",OWNO,////,////,????,"Kings Creek, Northern Territory, Australia",MAYBE
    KCT,////,////,VCCK,UNK,nan,////,////,////,////,////,Koggala Airpot,"Galle, Southern Province, Sri Lanka (Ceylon)",UNK
    KCU,////,////,HUMI,UNK,1.0,Masindi,"Masindi, Uganda",OWNO,////,////,????,"Masindi, Uganda",OK
    KCZ,////,////,RJOK,UNK,1.0,Kochi,"Kochi, Japan",OWNO,////,////,Kochi Airport,"Kochi, Kochi, Japan",OK
    KDA,////,////,GODK,UNK,0.625,Kolda,"Kolda, Senegal",OWNO,////,////,Kolda North Airport,"Kolda, Kolda, Senegal",OK
    KDB,////,////,YKBL,UNK,1.0,Kambalda,"Kambalda, Australia",OWNO,////,////,????,"Kambalda, Western Australia, Australia",OK
    KDC,////,////,DBBK,UNK,1.0,Kandi,"Kandi, Benin",OWNO,////,////,Kandi Airport,"Kandi, Alibori, Benin",OK
    KDD,////,////,OPKH,UNK,1.0,Khuzdar,"Khuzdar, Pakistan",OWNO,////,////,????,"Khuzdar, Balochistan, Pakistan",OK
    KDE,////,////,////,UNK,1.0,Koroba,"Koroba, Papua New Guinea",OWNO,////,////,????,"Koroba, Southern Highlands, Papua-New Guinea",OK
    KDH,////,////,OAKN,UNK,0.762,Kandahar,"Kandahar, Afghanistan",OWNO,////,////,Kandahar International Airport,"Kandahar, Kandahar, Afghanistan",OK
    KDI,////,////,WAWW,UNK,1.0,Wolter Monginsidi,"Kendari, Indonesia",OWNO,////,////,Wolter Monginsidi,"Kendari, Sulawesi Tenggara, Indonesia",OK
    KDJ,////,////,FOGJ,UNK,0.714,N'Djole,"N'Djole, Gabon",OWNO,////,////,Ville Airport,"N'Djolé, Gabon",OK
    KDK,KDK,PAKD,PAKD,OK,1.0,Municipal,"Kodiak (AK), USA",OWNO,KODIAK MUNI,"KODIAK, AK - UNITED STATES",Kodiak Municipal Airport,"Kodiak, Alaska, United States",OK
    KDL,////,////,EEKA,UNK,1.0,Kardla,"Kardla, Estonia",OWNO,////,////,????,"Kärdla, Estonia",OK
    KDM,////,////,VRMT,UNK,1.0,Kaadedhdhoo,"Kaadedhdhoo, Maldives",OWNO,////,////,Kaadedhdhoo Airport,"Kaadedhdhoo, Kaadedhdhoo Island, Maldives",OK
    KDN,////,////,FOGE,UNK,1.0,Ndende,"Ndende, Gabon",OWNO,////,////,????,"Ndende, Ngounié, Gabon",OK
    KDO,////,////,VRMK,UNK,1.0,Kadhdhoo,"Kadhdhoo, Maldives",OWNO,////,////,Kadhdhoo Airport,"Kadhdhoo, Kadhdhoo Island, Maldives",OK
    KDP,////,////,////,UNK,1.0,Kandep,"Kandep, Papua New Guinea",OWNO,////,////,????,"Kandep, Enga, Papua-New Guinea",OK
    KDR,////,////,////,UNK,1.0,Kandrian,"Kandrian, Papua New Guinea",OWNO,////,////,????,"Kandrian, West New Britain, Papua-New Guinea",OK
    KDS,////,////,////,UNK,1.0,Kamaran Downs,"Kamaran Downs, Australia",OWNO,////,////,????,"Kamaran Downs, Queensland, Australia",OK
    KDT,////,////,VTBK,UNK,nan,////,////,////,////,////,Kamphaeng Saen,"Nakhon Pathom, Suphan Buri, Thailand",UNK
    KDU,////,////,OPSD,UNK,1.0,Skardu,"Skardu, Pakistan",OWNO,////,////,Skardu Airport,"Skardu, Gilgit-Baltistan, Pakistan",OK
    KDV,////,////,NFKD,UNK,0.6,Kandavu,"Kandavu, Fiji",OWNO,////,////,????,"Vunisea, Kadavu Island , Fiji",TO DO CHECK
    KDW,////,////,////,UNK,0.718,Victoria Reservoir SPB,Kandy,IATA,////,////,Victoria Reservoir Waterdrome,"Kandy, Central Province, Sri Lanka (Ceylon)",MAYBE
    KDX,////,////,HSLI,UNK,nan,////,////,////,////,////,Kadugli Airport,"Kadugli, South Kordofan, Sudan",UNK
    KDY,////,////,UEMH,UNK,nan,////,////,////,////,////,Teply'j Klyuch Airport,"Teply'j Klyuch, Sakha (Yakutiya), Russian Federation (Russia)",UNK
    KDZ,////,////,////,UNK,0.745,Polgolla Reservoir SPB,Kandy,IATA,////,////,Polgolla Reservoir Waterdrome,"Kandy, Central Province, Sri Lanka (Ceylon)",MAYBE
    KEB,KEB,////,////,OK,1.0,Nanwalek,"Nanwalek (AK), USA",OWNO,NANWALEK,"NANWALEK, AK - UNITED STATES",????,"Nanwalek, Alaska, United States",OK
    KEC,////,////,FZQG,UNK,1.0,Kasenga,"Kasenga, Congo (DRC)",OWNO,////,////,Kasenga Airport,"Kasenga, Katanga, Democratic Republic of Congo (Zaire)",OK
    KED,////,////,GQNK,UNK,1.0,Kaedi,"Kaedi, Mauritania",OWNO,////,////,Kaédi Airport,"Kaédi, Mauritania",OK
    KEE,////,////,FCOK,UNK,1.0,Kelle,"Kelle, Congo (ROC)",OWNO,////,////,????,"Kelle, Congo (Republic of)",OK
    KEF,////,////,BIKF,UNK,0.889,Keflavik International,"Reykjavik, Iceland",OWNO,////,////,Keflavík,"Reykjavík, Iceland",OK
    KEG,////,////,////,UNK,1.0,Keglsugl,"Keglsugl, Papua New Guinea",OWNO,////,////,????,"Keglsugl, Chimbu, Papua-New Guinea",OK
    KEH,S60,////,////,OK,0.789,Kenmore Air Harbor,"Kenmore Air Harbor (WA), USA",OWNO,KENMORE AIR HARBOR INC,"KENMORE, WA - UNITED STATES",Kenmore Air Harbor Inc. SPB,"Kenmore, Washington, United States",OK
    KEI,////,////,WAKP,UNK,1.0,Kepi,"Kepi, Indonesia",OWNO,////,////,????,"Kepi, Papua, Indonesia",OK
    KEJ,////,////,UNEE,UNK,0.778,Kemerovo,"Kemerovo, Russia",OWNO,////,////,Kemerovo International Airport,"Kemerovo, Kemerovskaya, Russian Federation (Russia)",OK
    KEK,KEK,////,////,OK,1.0,Ekwok,"Ekwok (AK), USA",OWNO,EKWOK,"EKWOK, AK - UNITED STATES",????,"Ekwok, Alaska, United States",OK
    KEL,////,////,EDHK,UNK,1.0,Kiel Airport (Kiel Holtenau Airport),"Kiel, Schleswig-Holstein, Germany",OWNO,////,////,Holtenau,"Kiel, Schleswig-Holstein, Germany",OK
    KEM,////,////,EFKE,UNK,1.0,Kemi/Tornio,"Kemi/Tornio, Finland",OWNO,////,////,????,"Kemi-Tornio, Lappi (Lappland (Lapland)), Finland",TO DO CHECK
    KEN,////,////,GFKE,UNK,1.0,Kenema,"Kenema, Sierra Leone",OWNO,////,////,????,"Kenema, Sierra Leone",OK
    KEO,////,////,DIOD,UNK,1.0,Odienne,"Odienne, Cote d'Ivoire",OWNO,////,////,Odienné Airport,"Odienné, Denguélé, Côte d'Ivoire (Ivory Coast)",OK
    KEP,////,////,VNNG,UNK,1.0,Nepalganj,"Nepalganj, Nepal",OWNO,////,////,????,"Nepalgunj, Nepal",OK
    KEQ,////,////,WASE,UNK,1.0,Kebar,"Kebar, Indonesia",OWNO,////,////,????,"Kebar, Papua, Indonesia",OK
    KER,////,////,OIKK,UNK,1.0,Kerman,"Kerman, Iran",OWNO,////,////,????,"Kerman, Kerman, Iran",OK
    KES,////,////,CZEE,UNK,1.0,Kelsey,"Kelsey, Canada",OWNO,////,////,Kelsey Airport,"Kelesy, Manitoba, Canada",OK
    KET,////,////,VYKG,UNK,1.0,Keng Tung,"Keng Tung, Myanmar",OWNO,////,////,????,"Kengtung, Shan, Myanmar (Burma)",OK
    KEV,////,////,EFHA,UNK,1.0,Halli,"Kuorevesi, Finland",OWNO,////,////,Halli Airport,"Jämsä, Keski-Suomi (Mellersta Finland (Central Finland)), Finland",OK
    KEW,////,////,////,UNK,1.0,Keewaywin,"Keewaywin, Canada",OWNO,////,////,????,"Keewaywin, Ontario, Canada",OK
    KEX,////,////,////,UNK,1.0,Kanabea,"Kanabea, Papua New Guinea",OWNO,////,////,????,"Kanabea, Gulf, Papua-New Guinea",OK
    KEY,////,////,HKKR,UNK,1.0,Kericho,"Kericho, Kenya",OWNO,////,////,????,"Kericho, Kenya",OK
    KEZ,////,////,////,UNK,0.655,Kelani-Peliyagoda SPB,Colombo,IATA,////,////,Kelani River-Peliyagoda Waterdrome,"Colombo, Western Province, Sri Lanka (Ceylon)",MAYBE
    KFA,////,////,GQNF,UNK,1.0,Kiffa,"Kiffa, Mauritania",OWNO,////,////,????,"Kiffa, Mauritania",OK
    KFE,////,////,YFDF,UNK,nan,////,////,////,////,////,Fortescue Dave Forrest,"Cloudbreak, Western Australia, Australia",UNK
    KFG,////,////,YKKG,UNK,1.0,Kalkurung,"Kalkurung, Australia",OWNO,////,////,????,"Kalkurung, Northern Territory, Australia",OK
    KFP,KFP,PAKF,PAKF,OK,1.0,False Pass,"False Pass (AK), USA",OWNO,FALSE PASS,"FALSE PASS, AK - UNITED STATES",????,"False Pass, Alaska, United States",OK
    KFS,////,////,LTAL,UNK,1.0,Kastamonu,"Kastamonu, Turkey",OWNO,////,////,Kastamonu Airport,"Kastamonu, Turkey",OK

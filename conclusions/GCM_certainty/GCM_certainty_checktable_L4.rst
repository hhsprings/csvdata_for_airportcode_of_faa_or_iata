
List for checking certainty of Great Circle Mapper (LTS - LZY)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    LTS,LTS,KLTS,KLTS,OK,1.0,Altus AFB,"Altus (OK), USA",OWNO,ALTUS AFB,"ALTUS, OK - UNITED STATES",Altus AFB,"Altus, Oklahoma, United States",OK
    LTT,////,////,LFTZ,UNK,0.919,La Mole,"Saint Tropez, France",OWNO,////,////,La Môle - St-Tropez Airport,"St-Tropez, Provence-Alpes-Côte d'Azur, France",MAYBE
    LTV,////,////,YLOV,UNK,0.762,Lotusvale,"Lotusvale, Australia",OWNO,////,////,????,"Lotus Vale Station, Queensland, Australia",TO DO CHECK
    LTW,2W6,////,////,OK,0.81,St Marys County,"Leonardtown (MD), USA",OWNO,ST MARY'S COUNTY RGNL,"LEONARDTOWN, MD - UNITED STATES",St. Mary's County Regional,"Leonardtown, Maryland, United States",OK
    LTX,////,////,SELT,UNK,1.0,Cotapaxi International,Latacunga,IATA,////,////,Cotopaxi International Airport,"Latacunga, Cotopaxi, Ecuador",MAYBE
    LUA,////,////,VNLK,UNK,0.385,Lukla,"Lukla, Nepal",OWNO,////,////,Tenzing-Hillary Airport,"Lukla, Nepal",OK
    LUB,////,////,SYLP,UNK,1.0,Lumid Pau,"Lumid Pau, Guyana",OWNO,////,////,????,"Lumid Pau, Upper Takutu-Upper Essequibo, Guyana",OK
    LUC,////,////,NFNH,UNK,0.833,Laucala Is,"Laucala Is, Fiji",OWNO,////,////,????,"Laucala Island, Fiji",MAYBE
    LUD,////,////,FYLZ,UNK,0.667,Luderitz,"Luderitz, Namibia",OWNO,////,////,Diaz Point,"Luderitz, Namibia",OK
    LUE,////,////,LZLU,UNK,1.0,Lucenec,"Lucenec, Slovakia",OWNO,////,////,????,"Lucenec, Slovakia",OK
    LUF,LUF,KLUF,KLUF,OK,1.0,Luke AFB,"Phoenix (AZ), USA",OWNO,LUKE AFB,"GLENDALE, AZ - UNITED STATES",Luke AFB,"Glendale, Arizona, United States",OK
    LUG,////,////,LSZA,UNK,0.75,Lugano,"Lugano, Switzerland",OWNO,////,////,Agno,"Lugano, Ticino, Switzerland",OK
    LUH,////,////,VILD,UNK,1.0,Ludhiana,"Ludhiana, India",OWNO,////,////,????,"Ludhiana, Punjab, India",OK
    LUI,////,////,////,UNK,1.0,La Union,"La Union, Honduras",OWNO,////,////,????,"La Unión, Olancho, Honduras",OK
    LUK,LUK,KLUK,KLUK,OK,0.57,Municipal,"Cincinnati (OH), USA",OWNO,CINCINNATI MUNI AIRPORT LUNKEN FIELD,"CINCINNATI, OH - UNITED STATES",Cincinnati Municipal Lunken Field,"Cincinnati, Ohio, United States",OK
    LUL,LUL,KLUL,KLUL,OK,1.0,Hesler-Noble Field,"Laurel (MS), USA",OWNO,HESLER-NOBLE FIELD,"LAUREL, MS - UNITED STATES",Hesler-Noble Field,"Laurel, Mississippi, United States",OK
    LUM,////,////,ZPMS,UNK,1.0,Mangshi,"Luxi, PR China",OWNO,////,////,Dehong Mangshi Airport,"Mang City, Dehong, Yunnan, China",OK
    LUN,////,////,FLKK,UNK,0.609,Lusaka International Airport,"Lusaka, Zambia",OWNO,////,////,Kenneth Kaunda International Airport,"Lusaka, Lusaka, Zambia",OK
    LUO,////,////,FNUE,UNK,1.0,Luena,"Luena, Angola",OWNO,////,////,????,"Luena, Angola",OK
    LUP,LUP,PHLU,PHLU,OK,1.0,Kalaupapa,"Kalaupapa (HI), USA",OWNO,KALAUPAPA,"KALAUPAPA, HI - UNITED STATES",????,"Kalaupapa, Molokai, Hawaii, United States",OK
    LUQ,////,////,SAOU,UNK,1.0,San Luis,"San Luis, Argentina",OWNO,////,////,????,"San Luis, Argentina",OK
    LUR,LUR,PALU,PALU,OK,0.845,Cape Lisburne,"Cape Lisburne (AK), USA",OWNO,CAPE LISBURNE LRRS,"CAPE LISBURNE, AK - UNITED STATES",Cape Lisburne LRRS Airport,"Cape Lisburne, Alaska, United States",OK
    LUS,////,////,FZCE,UNK,1.0,Lusanga,"Lusanga, Congo (DRC)",OWNO,////,////,????,"Lusanga, Bandundu, Democratic Republic of Congo (Zaire)",OK
    LUT,////,////,YLRS,UNK,1.0,Laura Station,"Laura Station, Australia",OWNO,////,////,????,"Laura Station, Queensland, Australia",OK
    LUU,////,////,YLRA,UNK,1.0,Laura,"Laura, Australia",OWNO,////,////,????,"Laura, Queensland, Australia",OK
    LUV,////,////,WAPL,UNK,0.583,Langgur,"Langgur, Indonesia",OWNO,////,////,Dumatubin,"Langgur, Kai Island, Maluku, Indonesia",OK
    LUW,////,////,WAMW,UNK,0.588,Luwuk,"Luwuk, Indonesia",OWNO,////,////,Bubung,"Luwuk, Sulawesi Tengah, Indonesia",OK
    LUX,////,////,ELLX,UNK,0.667,Luxembourg,"Luxembourg, Luxembourg",OWNO,////,////,Luxembourg-Findel International Airport,"Luxembourg, Luxembourg",OK
    LUZ,////,////,EPLB,UNK,1.0,Lublin Airport,"Lublin, Poland",OWNO,////,////,Lublin Airport,"Lublin, Lubelskie, Poland",OK
    LVA,////,////,LFOV,UNK,1.0,Entrammes,"Laval, France",OWNO,////,////,Entrammes,"Laval, Pays de la Loire, France",OK
    LVB,////,////,SSLI,UNK,1.0,Dos Galpoes,"Livramento, Brazil",OWNO,////,////,Dos Galpoes,"Santana do Livramento, Rio Grande do Sul, Brazil",OK
    LVD,2AK,////,////,OK,1.0,Lime Village,"Lime Village (AK), USA",OWNO,LIME VILLAGE,"LIME VILLAGE, AK - UNITED STATES",????,"Lime Village, Alaska, United States",OK
    LVI,////,////,FLHN,UNK,0.5,Livingstone,"Livingstone, Zambia",OWNO,////,////,Harry Mwanga Nkumbula International Airport,"Livingstone, Southern, Zambia",OK
    LVK,LVK,KLVK,KLVK,OK,0.801,Livermore,"Livermore (CA), USA",OWNO,LIVERMORE MUNI,"LIVERMORE, CA - UNITED STATES",Livermore Municipal Airport,"Livermore, California, United States",OK
    LVL,LVL,KLVL,KLVL,OK,1.0,Lawrenceville,"Lawrenceville (VA), USA",OWNO,LAWRENCEVILLE/BRUNSWICK MUNI,"LAWRENCEVILLE, VA - UNITED STATES",Lawrenceville/Brunswick Municipal Airport,"Lawrenceville, Virginia, United States",OK
    LVM,LVM,KLVM,KLVM,OK,1.0,Mission Field,"Livingston (MT), USA",OWNO,MISSION FIELD,"LIVINGSTON, MT - UNITED STATES",Mission Field,"Livingston, Montana, United States",OK
    LVO,////,////,YLTN,UNK,1.0,Laverton,"Laverton, Australia",OWNO,////,////,????,"Laverton, Western Australia, Australia",OK
    LVP,////,////,OIBV,UNK,1.0,Lavan,"Lavan, Iran",OWNO,////,////,Lavan Airport,"Lavan Island, Hormozgan, Iran",OK
    LVS,LVS,KLVS,KLVS,OK,0.613,Las Vegas,"Las Vegas (NM), USA",OWNO,LAS VEGAS MUNI,"LAS VEGAS, NM - UNITED STATES",Las Vegas Municipal Airport,"Las Vegas, New Mexico, United States",OK
    LWB,LWB,KLWB,KLWB,OK,1.0,Greenbrier Valley,"Lewisburg (WV), USA",OWNO,GREENBRIER VALLEY,"LEWISBURG, WV - UNITED STATES",Greenbrier Valley Airport,"Lewisburg, West Virginia, United States",OK
    LWC,LWC,KLWC,KLWC,OK,0.734,Lawrence,"Lawrence (KS), USA",OWNO,LAWRENCE MUNI,"LAWRENCE, KS - UNITED STATES",Lawrence Municipal Airport,"Lawrence, Kansas, United States",OK
    LWE,////,////,WATW,UNK,0.64,Lewoleba,"Lewoleba, Indonesia",OWNO,////,////,Wonopito Airport,"Lewoleba, Nusa Tenggara Timur, Indonesia",OK
    LWH,////,////,YLAH,UNK,1.0,Lawn Hill,"Lawn Hill, Australia",OWNO,////,////,????,"Lawn Hill, Queensland, Australia",OK
    LWI,////,////,////,UNK,1.0,Lowai,"Lowai, Papua New Guinea",OWNO,////,////,????,"Lowai, Morobe, Papua-New Guinea",OK
    LWK,////,////,EGET,UNK,1.0,Lerwick/Tingwall,"Shetland Islands, United Kingdom",OWNO,////,////,Tingwall,"Lerwick, Shetland Islands, Scotland, United Kingdom",OK
    LWL,LWL,KLWL,KLWL,OK,1.0,Harriet Field,"Wells (NV), USA",OWNO,WELLS MUNI/HARRIET FIELD,"WELLS, NV - UNITED STATES",Wells Municipal/Harriet Field,"Wells, Nevada, United States",OK
    LWM,LWM,KLWM,KLWM,OK,0.806,Lawrence,"Lawrence (MA), USA",OWNO,LAWRENCE MUNI,"LAWRENCE, MA - UNITED STATES",Lawrence Municipal Airport,"Lawrence, Massachusetts, United States",OK
    LWN,////,////,UDSG,UNK,0.538,Gyoumri,"Gyoumri, Armenia",OWNO,////,////,Shirak International Airport,"Gyoumri, Armenia",OK
    LWO,////,////,UKLL,UNK,1.0,Snilow,"Lvov, Ukraine",OWNO,////,////,Snilow,"Lvov, Lviv, Ukraine",OK
    LWR,////,////,EHLW,UNK,0.87,Leeuwarden,"Leeuwarden, Netherlands",OWNO,////,////,Leeuwarden AB,"Leeuwarden, Fryslân (Friesland), Netherlands",OK
    LWS,LWS,KLWS,KLWS,OK,0.856,Nez Perce County Rgnl,"Lewiston (ID), USA",OWNO,LEWISTON-NEZ PERCE COUNTY,"LEWISTON, ID - UNITED STATES",Lewiston-Nez Perce County Airport,"Lewiston, Idaho, United States",OK
    LWT,LWT,KLWT,KLWT,OK,1.0,Municipal,"Lewistown (MT), USA",OWNO,LEWISTOWN MUNI,"LEWISTOWN, MT - UNITED STATES",Lewistown Municipal Airport,"Lewistown, Montana, United States",OK
    LWV,LWV,KLWV,KLWV,OK,0.682,Municipal,"Lawrenceville (IL), USA",OWNO,LAWRENCEVILLE-VINCENNES INTL,"LAWRENCEVILLE, IL - UNITED STATES",Lawrenceville-Vincennes International Airport,"Lawrenceville, Illinois, United States",OK
    LWY,////,////,WBGW,UNK,1.0,Lawas,"Lawas, Malaysia",OWNO,////,////,????,"Lawas, Sarawak, Malaysia",OK
    LXA,////,////,ZULS,UNK,0.75,Lhasa,"Lhasa, PR China",OWNO,////,////,Gonggar,"Lhasa, Tibet, China",OK
    LXG,////,////,VLLN,UNK,1.0,Luang Namtha,"Luang Namtha, Lao PDR",OWNO,////,////,????,"Luang Namtha, Lao People's Democratic Republic (Laos)",OK
    LXN,LXN,KLXN,KLXN,OK,0.536,Lexington,"Lexington (NE), USA",OWNO,JIM KELLY FIELD,"LEXINGTON, NE - UNITED STATES",Jim Kelly Field,"Lexington, Nebraska, United States",OK
    LXR,////,////,HELX,UNK,0.769,Luxor,"Luxor, Egypt",OWNO,////,////,Luxor International Airport,"Luxor, Qina (Qena), Egypt",OK
    LXS,////,////,LGLM,UNK,1.0,Lemnos,"Lemnos, Greece",OWNO,////,////,????,"Lemnos, Voreío Aigaío (Northern Aegean), Greece",OK
    LXU,////,////,FLLK,UNK,1.0,Lukulu,"Lukulu, Zambia",OWNO,////,////,????,"Lukulu, Zambia",OK
    LXV,LXV,KLXV,KLXV,OK,0.576,Leadville,"Leadville (CO), USA",OWNO,LAKE COUNTY,"LEADVILLE, CO - UNITED STATES",Lake County Airport,"Leadville, Colorado, United States",OK
    LYA,////,////,ZHLY,UNK,1.0,Luoyang,"Luoyang, PR China",OWNO,////,////,????,"Luoyang, Henan, China",OK
    LYB,////,////,MWCL,UNK,0.531,Little Cayman,"Little Cayman, Cayman Islands",OWNO,////,////,Edward Bodden Airfield,"Little Cayman, Cayman Islands",OK
    LYC,////,////,ESNL,UNK,1.0,Lycksele,"Lycksele, Sweden",OWNO,////,////,????,"Lycksele, Västerbottens län, Sweden",OK
    LYE,////,////,EGDL,UNK,0.733,RAF Station,"Lyneham, United Kingdom",OWNO,////,////,RAF Lyneham,"Lyneham, Wiltshire, England, United Kingdom",OK
    LYG,////,////,ZSLG,UNK,1.0,Lianyungang,"Lianyungang, PR China",OWNO,////,////,????,"Lianyungang, Jiangsu, China",OK
    LYH,LYH,KLYH,KLYH,OK,1.0,Preston-Glenn Field,"Lynchburg (VA), USA",OWNO,LYNCHBURG RGNL/PRESTON GLENN FLD,"LYNCHBURG, VA - UNITED STATES",Lynchburg Regional/Preston Glenn Field,"Lynchburg, Virginia, United States",OK
    LYI,////,////,ZSLY,UNK,0.783,Linyi,"Linyi, PR China",OWNO,////,////,Shubuling,"Linyi, Shandong, China",OK
    LYN,////,////,LFLY,UNK,1.0,Bron (general aviation),"Lyon, France",OWNO,////,////,Bron,"Lyon, Rhône-Alpes, France",OK
    LYP,////,////,OPFA,UNK,0.818,Faisalabad,"Faisalabad, Pakistan",OWNO,////,////,Faisalabad International Airport,"Faisalabad, Punjab, Pakistan",OK
    LYR,////,////,ENSB,UNK,1.0,Svalbard,"Longyearbyen, Svalbard (Norway)",OWNO,////,////,Svalbard,"Longyearbyen, Norway",OK
    LYS,////,////,LFLL,UNK,0.909,Lyon-Saint Exupéry Airport,"Lyon, France",OWNO,////,////,St-Exupéry,"Lyon, Rhône-Alpes, France",OK
    LYT,////,////,////,UNK,1.0,Lady Elliot Island,"Lady Elliot Island, Australia",OWNO,////,////,????,"Lady Elliot Island, Queensland, Australia",OK
    LYU,ELO,KELO,KELO,OK,0.704,Ely,"Ely (MN), USA",OWNO,ELY MUNI,"ELY, MN - UNITED STATES",Ely Municipal Airport,"Ely, Minnesota, United States",OK
    LYX,////,////,EGMD,UNK,0.545,Lydd International Airport,"Lydd, United Kingdom",OWNO,////,////,London Ashford International Airport,"Lydd, Kent, England, United Kingdom",OK
    LZC,////,////,MMLC,UNK,1.0,Lazaro Cardenas,"Lazaro Cardenas, Mexico",OWNO,////,////,????,"Lázaro Cárdenas, Michoacán de Ocampo, México",OK
    LZH,////,////,ZGZH,UNK,0.789,Liuzhou,"Liuzhou, PR China",OWNO,////,////,Bailian,"Liuzhou, Guangxi, China",OK
    LZN,////,////,RCFG,UNK,nan,////,////,////,////,////,Matsu Nangan,"Nangan, MatsuTao Island, Taiwan",UNK
    LZO,////,////,ZULZ,UNK,1.0,Luzhou,"Luzhou, PR China",OWNO,////,////,????,"Luzhou, Sichuan, China",OK
    LZR,////,////,YLZI,UNK,1.0,Lizard Island,"Lizard Island, Australia",OWNO,////,////,????,"Lizard Island, Queensland, Australia",OK
    LZY,////,////,ZUNZ,UNK,nan,////,////,////,////,////,Mainling,"Nyingchi, Tibet, China",UNK

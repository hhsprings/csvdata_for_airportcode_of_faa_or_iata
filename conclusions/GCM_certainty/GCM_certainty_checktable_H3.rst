
List for checking certainty of Great Circle Mapper (HTA - HZV)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    HTA,////,////,UIAA,UNK,0.588,Chita,"Chita, Russia",OWNO,////,////,Kadala Airport,"Chita, Zabaykal'skiy, Russian Federation (Russia)",OK
    HTG,////,////,UOHH,UNK,0.933,Hatanga,"Hatanga, Russia",OWNO,////,////,Khatanga Airport,"Khatanga, Krasnoyarskiy, Russian Federation (Russia)",TO DO CHECK
    HTH,HTH,KHTH,KHTH,OK,0.563,Hawthorne,"Hawthorne (NV), USA",OWNO,HAWTHORNE INDUSTRIAL,"HAWTHORNE, NV - UNITED STATES",Hawthorne Industrial Airport,"Hawthorne, Nevada, United States",OK
    HTI,////,////,YBHM,UNK,1.0,Hamilton Island,"Hamilton Island, Australia",OWNO,////,////,????,"Hamilton Island, Queensland, Australia",OK
    HTL,HTL,KHTL,KHTL,OK,0.596,Roscommon County,"Houghton (MI), USA",OWNO,ROSCOMMON COUNTY - BLODGETT MEMORIAL,"HOUGHTON LAKE, MI - UNITED STATES",Roscommon County - Blodgett Memorial Airport,"Houghton Lake, Michigan, United States",OK
    HTN,////,////,ZWTN,UNK,1.0,Hotan,"Hotan, PR China",OWNO,////,////,????,"Hotan, Xinjiang, China",OK
    HTO,HTO,KHTO,KHTO,OK,1.0,East Hampton,"East Hampton (NY), USA",OWNO,EAST HAMPTON,"EAST HAMPTON, NY - UNITED STATES",????,"East Hampton, New York, United States",OK
    HTR,////,////,RORH,UNK,1.0,Hateruma,"Hateruma, Japan",OWNO,////,////,Hateruma Airport,"Hateruma, Okinawa, Japan",OK
    HTS,HTS,KHTS,KHTS,OK,1.0,Tri-State/Milton,"Huntington (WV), USA",OWNO,TRI-STATE/MILTON J FERGUSON FIELD,"HUNTINGTON, WV - UNITED STATES",Tri-State/Milton J Ferguson Field,"Huntington, West Virginia, United States",OK
    HTT,////,////,ZLHX,UNK,1.0,Huatugou,Huatugou,IATA,////,////,Huatugou Airport,"Mangnai, Qinghai, China",MAYBE
    HTU,////,////,YHPN,UNK,1.0,Hopetoun,"Hopetoun, Australia",OWNO,////,////,????,"Hopetown, Victoria, Australia",OK
    HTV,UTS,KUTS,KUTS,OK,0.748,Huntsville,"Huntsville (TX), USA",OWNO,HUNTSVILLE MUNI,"HUNTSVILLE, TX - UNITED STATES",Huntsville Municipal Airport,"Huntsville, Texas, United States",OK
    HTY,////,////,LTDA,UNK,nan,////,////,////,////,////,Hatay Airport,"Antakya, Turkey",UNK
    HTZ,////,////,SKHC,UNK,1.0,Hato Corozal,"Hato Corozal, Colombia",OWNO,////,////,Hato Corozal Airport,"Hato Corozal, Casanare, Colombia",OK
    HUA,HUA,KHUA,KHUA,OK,1.0,Redstone AAF,"Huntsville (AL), USA",OWNO,REDSTONE AAF,"REDSTONE ARSNL HUNTSVILLE, AL - UNITED STATES",Redstone AAF Airport,"Redstone Arsenal, Alabama, United States",OK
    HUB,////,////,YHBR,UNK,1.0,Humbert River,"Humbert River, Australia",OWNO,////,////,????,"Humbert River, Northern Territory, Australia",OK
    HUC,////,////,////,UNK,0.438,Humacao Airport,"Humacao, Puerto Rico",OWNO,////,////,Metropolitan Area,"Humacao, Puerto Rico, United States",OK
    HUD,0K7,////,////,OK,0.709,Humboldt,"Humboldt IA, USA",OWNO,HUMBOLDT MUNI,"HUMBOLDT, IA - UNITED STATES",Humboldt Municipal Airport,"Humboldt, Iowa, United States",OK
    HUE,////,////,HAHU,UNK,1.0,Humera,"Humera, Ethiopia",OWNO,////,////,Humera Airport,"Humera, Tigray, Ethiopia",OK
    HUF,HUF,KHUF,KHUF,OK,0.865,Hulman Field,"Terre Haute (IN), USA",OWNO,TERRE HAUTE INTL-HULMAN FIELD,"TERRE HAUTE, IN - UNITED STATES",Terre Haute International-Hulman Field,"Terre Haute, Indiana, United States",OK
    HUG,////,////,MGHT,UNK,1.0,Huehuetenango,"Huehuetenango, Guatemala",OWNO,////,////,????,"Huehuetenango, Huehuetenango, Guatemala",OK
    HUH,////,////,NTTH,UNK,0.737,Huahine,"Huahine, French Polynesia",OWNO,////,////,Fare,"Huahine, Society Islands, French Polynesia",OK
    HUI,////,////,VVPB,UNK,0.786,Phu Bai,"Hue, Viet Nam",OWNO,////,////,Phu Bai International Airport,"Huê', Vietnam",OK
    HUJ,HHW,KHHW,KHHW,OK,0.349,Hugo,"Hugo (OK), USA",OWNO,STAN STAMPER MUNI,"HUGO, OK - UNITED STATES",Stan Stamper Municipal Airport,"Hugo, Oklahoma, United States",OK
    HUK,////,////,////,UNK,1.0,Hukuntsi,"Hukuntsi, Botswana",OWNO,////,////,Hukuntsi Airport,"Hukuntsi, Kgalagadi, Botswana",OK
    HUL,HUL,KHUL,KHUL,OK,1.0,International,"Houlton (ME), USA",OWNO,HOULTON INTL,"HOULTON, ME - UNITED STATES",Houlton International Airport,"Houlton, Maine, United States",OK
    HUM,HUM,KHUM,KHUM,OK,1.0,Terrebonne,"Houma (LA), USA",OWNO,HOUMA-TERREBONNE,"HOUMA, LA - UNITED STATES",Houma-Terrebonne Airport,"Houma, Louisiana, United States",OK
    HUN,////,////,RCYU,UNK,1.0,Hualien,"Hualien, Taiwan",OWNO,////,////,????,"Hualien, Taiwan",OK
    HUQ,////,////,HLON,UNK,1.0,Houn,"Houn, Libya",OWNO,////,////,????,"Houn, Libyan Arab Jamahiriya (Libya)",OK
    HUS,HUS,PAHU,PAHU,OK,0.704,Municipal,"Hughes (AK), USA",OWNO,HUGHES,"HUGHES, AK - UNITED STATES",????,"Hughes, Alaska, United States",OK
    HUT,HUT,KHUT,KHUT,OK,0.76,Hutchinson,"Hutchinson (KS), USA",OWNO,HUTCHINSON RGNL,"HUTCHINSON, KS - UNITED STATES",Hutchinson Regional,"Hutchinson, Kansas, United States",OK
    HUU,////,////,SPNC,UNK,0.269,Huanuco,"Huanuco, Peru",OWNO,////,////,Alférez FAP David Figueroa Fernandini,"Huánuco, Huánuco, Perú",OK
    HUV,////,////,ESNH,UNK,1.0,Hudiksvall,"Hudiksvall, Sweden",OWNO,////,////,????,"Hudiksvall, Sweden",OK
    HUW,////,////,SWHT,UNK,nan,////,////,////,////,////,????,"Humaitá, Amazonas, Brazil",UNK
    HUX,////,////,MMBT,UNK,0.526,Huatulco,"Huatulco, Mexico",OWNO,////,////,Bahías de Huatulco International Airport,"Huatulco, Oaxaca, México",OK
    HUY,////,////,EGNJ,UNK,1.0,Humberside Airport,"Humberside, United Kingdom",OWNO,////,////,????,"Humberside, Lincolnshire, England, United Kingdom",OK
    HUZ,////,////,ZGHZ,UNK,0.81,Huizhou,"Huizhou, PR China",OWNO,////,////,Huizhou Pingtan Airport,"Huizhou, Guangdong, China",OK
    HVA,////,////,FMNL,UNK,1.0,Analalava,"Analalava, Madagascar",OWNO,////,////,????,"Analalava, Madagascar",OK
    HVB,////,////,YHBA,UNK,1.0,Hervey Bay,"Hervey Bay, Australia",OWNO,////,////,????,"Hervey Bay, Queensland, Australia",OK
    HVD,////,////,ZMKD,UNK,1.0,Khovd,"Khovd, Mongolia",OWNO,////,////,????,"Khovd, Mongolia",OK
    HVE,HVE,KHVE,KHVE,OK,0.51,Intermediate,"Hanksville (UT), USA",OWNO,HANKSVILLE,"HANKSVILLE, UT - UNITED STATES",????,"Hanksville, Utah, United States",OK
    HVG,////,////,ENHV,UNK,1.0,Valan,"Honningsvag, Norway",OWNO,////,////,Valan,"Honningsvåg, Finnmark, Norway",OK
    HVK,////,////,BIHK,UNK,1.0,Holmavik,"Holmavik, Iceland",OWNO,////,////,????,"Hólmavík, Iceland",OK
    HVN,HVN,KHVN,KHVN,OK,1.0,Tweed New Haven Airport,"New Haven (CT), USA",OWNO,TWEED-NEW HAVEN,"NEW HAVEN, CT - UNITED STATES",Tweed-New Haven Airport,"New Haven, Connecticut, United States",OK
    HVR,HVR,KHVR,KHVR,OK,1.0,City County,"Havre (MT), USA",OWNO,HAVRE CITY-COUNTY,"HAVRE, MT - UNITED STATES",Havre City-County Airport,"Havre, Montana, United States",OK
    HVS,HVS,KHVS,KHVS,OK,0.81,Municipal,"Hartsville (SC), USA",OWNO,HARTSVILLE RGNL,"HARTSVILLE, SC - UNITED STATES",Hartsville Regional,"Hartsville, South Carolina, United States",OK
    HWA,////,////,////,UNK,1.0,Hawabango,"Hawabango, Papua New Guinea",OWNO,////,////,????,"Hawabango, Gulf, Papua-New Guinea",OK
    HWD,HWD,KHWD,KHWD,OK,0.423,Air Terminal,"Hayward (CA), USA",OWNO,HAYWARD EXECUTIVE,"HAYWARD, CA - UNITED STATES",Hayward Executive Airport,"Hayward, California, United States",OK
    HWI,HWI,////,////,OK,0.801,SPB,"Hawk Inlet (AK), USA",OWNO,HAWK INLET,"HAWK INLET, AK - UNITED STATES",Hawk Inlet SPB,"Hawk Inlet, Alaska, United States",OK
    HWK,////,////,YHAW,UNK,1.0,Wilpena Pound,"Hawker, Australia",OWNO,////,////,Wilpena Pound,"Hawker, South Australia, Australia",OK
    HWN,////,////,FVWN,UNK,0.867,Hwange Nat Park,"Hwange Nat Park, Zimbabwe",OWNO,////,////,????,"Hwange National Park, Zimbabwe",MAYBE
    HWO,HWO,KHWO,KHWO,OK,1.0,North Perry,"Hollywood (FL), USA",OWNO,NORTH PERRY,"HOLLYWOOD, FL - UNITED STATES",North Perry Airport,"Hollywood, Florida, United States",OK
    HXD,////,////,ZLDL,UNK,nan,////,////,////,////,////,Delingha Airport,"Delingha, Qinghai, China",UNK
    HXX,////,////,YHAY,UNK,1.0,Hay,"Hay, Australia",OWNO,////,////,????,"Hay, New South Wales, Australia",OK
    HYA,HYA,KHYA,KHYA,OK,0.679,Barnstable,"Hyannis (MA), USA",OWNO,BARNSTABLE MUNI-BOARDMAN/POLANDO FIELD,"HYANNIS, MA - UNITED STATES",Barnstable Municipal-Boardman/Polando Field,"Hyannis, Massachusetts, United States",OK
    HYC,////,////,EGUH,UNK,0.87,High Wycombe,"High Wycombe, United Kingdom",OWNO,////,////,RAF,"High Wycombe, Buckinghamshire, England, United Kingdom",OK
    HYD,////,////,VOHS,UNK,1.0,Rajiv Gandhi International Airport,"Hyderabad, Andhra Pradesh, India",OWNO,////,////,Rajiv Gandhi International Airport,"Hyderabad, Telangana, India",OK
    HYF,////,////,////,UNK,1.0,Hayfields,"Hayfields, Papua New Guinea",OWNO,////,////,????,"Hayfields, East Sepik, Papua-New Guinea",OK
    HYG,HYG,PAHY,PAHY,OK,0.79,SPB,"Hydaburg (AK), USA",OWNO,HYDABURG,"HYDABURG, AK - UNITED STATES",Hydaburg SPB,"Hydaburg, Alaska, United States",OK
    HYL,HYL,////,////,OK,0.498,SPB,"Hollis (AK), USA",OWNO,HOLLIS CLARK BAY,"HOLLIS, AK - UNITED STATES",Hollis Clark Bay SPB,"Hollis, Alaska, United States",OK
    HYN,////,////,ZSLQ,UNK,0.889,Taizhou Luqiao Airport,"Taizhou, Zhejiang, PR China",OWNO,////,////,Luqiao,"Huangyan, Zhejiang, China",OK
    HYR,HYR,KHYR,KHYR,OK,0.51,Municipal,"Hayward (WI), USA",OWNO,SAWYER COUNTY,"HAYWARD, WI - UNITED STATES",Sawyer County Airport,"Hayward, Wisconsin, United States",OK
    HYS,HYS,KHYS,KHYS,OK,0.661,Municipal,"Hays (KS), USA",OWNO,HAYS RGNL,"HAYS, KS - UNITED STATES",Hays Regional,"Hays, Kansas, United States",OK
    HYV,////,////,EFHV,UNK,1.0,Hyvinkaa,"Hyvinkaa, Finland",OWNO,////,////,????,"Hyvinkää, Uusimaa (Nyland (Uusimaa)), Finland",OK
    HZB,////,////,LFQT,UNK,1.0,Merville/Calonne,"Hazebrouck, France",OWNO,////,////,Calonne,"Merville, Nord-Pas-de-Calais, France",OK
    HZG,////,////,ZLHZ,UNK,1.0,Hanzhong Chenggu Airport,"Hanzhong, Shaanxi, China",OWNO,////,////,Hanzhong Chenggu Airport,"Hanzhong, Shaanxi, China",OK
    HZH,////,////,ZUNP,UNK,1.0,Liping,Liping,IATA,////,////,Liping,"Liping City, Guizhou, China",MAYBE
    HZK,////,////,BIHU,UNK,1.0,Husavik,"Husavik, Iceland",OWNO,////,////,????,"Húsavík, Iceland",OK
    HZL,HZL,KHZL,KHZL,OK,0.799,Hazleton,"Hazleton (PA), USA",OWNO,HAZLETON RGNL,"HAZLETON, PA - UNITED STATES",Hazleton Regional,"Hazleton, Pennsylvania, United States",OK
    HZV,////,////,////,UNK,1.0,Hazyview,"Hazyview, South Africa",OWNO,////,////,????,"Hazyview, Mpumalanga, South Africa",OK


List for checking certainty of Great Circle Mapper (XAB - XZM)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    XAB,////,////,LFOI,UNK,1.0,Abbeville,"Abbeville, France",OWNO,////,////,????,"Abbeville, Picardie (Picardy), France",OK
    XAC,////,////,LFCH,UNK,1.0,La Teste-de-Buch,"Arcachon, France",OWNO,////,////,La Teste de Buch,"Arcachon, Aquitaine, France",OK
    XAD,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Churchill, Manitoba, Canada",UNK
    XAL,////,////,////,UNK,1.0,Alamos,"Alamos, Mexico",OWNO,////,////,????,"Álamos, Sonora, México",OK
    XAP,////,////,SBCH,UNK,1.0,Chapeco,"Chapeco, Brazil",OWNO,////,////,????,"Chapecó, Santa Catarina, Brazil",OK
    XAR,////,////,DFOY,UNK,1.0,Aribinda,"Aribinda, Burkina Faso",OWNO,////,////,Aribinda Airport,"Aribinda, Burkina Faso",OK
    XAU,////,////,SOOS,UNK,1.0,Saul,"Saul, French Guiana",OWNO,////,////,????,"Saül, French Guiana",OK
    XAX,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Dorval, Québec, Canada",UNK
    XBB,////,////,////,UNK,1.0,Blubber Bay,"Blubber Bay, Canada",OWNO,////,////,????,"Blubber Bay, British Columbia, Canada",OK
    XBE,////,////,////,UNK,1.0,Bearskin Lake,"Bearskin Lake, Canada",OWNO,////,////,????,"Bearskin Lake, Ontario, Canada",OK
    XBG,////,////,DFEB,UNK,1.0,Bogande,"Bogande, Burkina Faso",OWNO,////,////,????,"Bogande, Burkina Faso",OK
    XBJ,////,////,OIMB,UNK,1.0,Birjand,"Birjand, Iran",OWNO,////,////,????,"Birjand, Khorasan-e Janubi, Iran",OK
    XBL,////,////,HABB,UNK,0.714,Buno Bedelle,"Buno Bedelle, Ethiopia",OWNO,////,////,????,"Bedele, Gambela Peoples', Ethiopia",MAYBE
    XBN,////,////,////,UNK,1.0,Biniguni,"Biniguni, Papua New Guinea",OWNO,////,////,????,"Biniguni, Northern, Papua-New Guinea",OK
    XBO,////,////,DFEA,UNK,1.0,Boulsa,"Boulsa, Burkina Faso",OWNO,////,////,????,"Boulsa, Burkina Faso",OK
    XBR,////,////,////,UNK,0.476,Brockville,"Brockville, Canada",OWNO,////,////,Thousand Islands Regional,"Brockville, Ontario, Canada",OK
    XCB,////,////,LFQI,UNK,nan,////,////,////,////,////,Epinoy,"Cambrai, Nord-Pas-de-Calais, France",UNK
    XCH,////,////,YPXM,UNK,1.0,Christmas Island,"Christmas Island, Christmas Island",OWNO,////,////,????,"Christmas Island, Christmas Island",OK
    XCL,////,////,////,UNK,1.0,Cluff Lake,"Cluff Lake, Canada",OWNO,////,////,Cluff Lake Airport,"Cluff Lake mine, Saskatchewan, Canada",OK
    XCM,////,////,////,UNK,0.769,Chatham,"Chatham, Canada",OWNO,////,////,????,"Chatham-Kent, Ontario, Canada",TO DO CHECK
    XCN,////,////,////,UNK,1.0,Coron,"Coron, Philippines",OWNO,////,////,????,"Coron, Philippines",OK
    XCO,////,////,YOLA,UNK,1.0,Colac,"Colac, Australia",OWNO,////,////,????,"Colac, Victoria, Australia",OK
    XCR,////,////,LFOK,UNK,0.667,Paris Vatry Airport (Aircraft training and cargo airport),"Vatry, France",OWNO,////,////,Vatry International Airport,"Chalons-sur-Marne, Champagne-Ardenne, France",OK
    XCZ,////,////,LFQV,UNK,nan,////,////,////,////,////,????,"Charleville Mezieres, Champagne-Ardenne, France",UNK
    XDA,////,////,LFBY,UNK,nan,////,////,////,////,////,????,"Dax/Seyresse, Aquitaine, France",UNK
    XDE,////,////,DFOU,UNK,1.0,Diebougou,"Diebougou, Burkina Faso",OWNO,////,////,Diebougou Airport,"Diebougou, Burkina Faso",OK
    XDJ,////,////,DFCJ,UNK,1.0,Djibo,"Djibo, Burkina Faso",OWNO,////,////,Djibo Airport,"Djibo, Burkina Faso",OK
    XDS,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Ottawa, Ontario, Canada",UNK
    XEF,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Winnipeg, Manitoba, Canada",UNK
    XEG,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Kingston, Ontario, Canada",UNK
    XEN,////,////,ZYXC,UNK,1.0,Xingcheng,"Xingcheng, PR China",OWNO,////,////,????,"Xingcheng, Liaoning, China",OK
    XES,C02,////,////,OK,0.555,Municipal,"Lake Geneva (WI), USA",OWNO,GRAND GENEVA RESORT,"LAKE GENEVA, WI - UNITED STATES",Grand Geneva Resort Airport,"Lake Geneva, Wisconsin, United States",OK
    XFL,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Shawinigan, Québec, Canada",UNK
    XFN,////,////,ZHXF,UNK,0.788,Xiangfan,"Xiangfan, PR China",OWNO,////,////,Xiangyang Liuji,"Xiangyang, Hubei, China",OK
    XFW,////,////,EDHI,UNK,1.0,Hamburg Finkenwerder Airport (Airbus),"Hamburg/Finkenwerder, Germany",OWNO,////,////,Finkenwerder,"Hamburg, Hamburg, Germany",OK
    XGA,////,////,DFOG,UNK,1.0,Gaoua,"Gaoua, Burkina Faso",OWNO,////,////,????,"Gaoua, Burkina Faso",OK
    XGG,////,////,DFEG,UNK,1.0,Gorom-Gorom,"Gorom-Gorom, Burkina Faso",OWNO,////,////,????,"Gorom-Gorom, Burkina Faso",OK
    XGL,////,////,////,UNK,1.0,Granville Lake,Granville Lake,IATA,////,////,????,"Granville Lake, Manitoba, Canada",MAYBE
    XGN,////,////,FNXA,UNK,1.0,Xangongo,"Xangongo, Angola",OWNO,////,////,????,"Xangongo, Angola",OK
    XGR,////,////,CYLU,UNK,0.703,Kangiqsualujjuaq,"Kangiqsualujjuaq, Canada",OWNO,////,////,Georges River,"Kangiqsualujjuaq, Québec, Canada",OK
    XIC,////,////,ZUXC,UNK,0.774,Xichang,"Xichang, PR China",OWNO,////,////,Qingshan Airport,"Xichang, Sichuan, China",OK
    XIE,////,////,VLXL,UNK,1.0,Xienglom,"Xienglom, Lao PDR",OWNO,////,////,????,"Xienglom, Lao People's Democratic Republic (Laos)",OK
    XIG,////,////,////,UNK,0.8,Municipal,"Xinguara, Brazil",OWNO,////,////,????,"Xinguara, Pará, Brazil",OK
    XIJ,////,////,OKAJ,UNK,nan,////,////,////,////,////,Ahmed al-Jaber AB,"Ahmed al-Jaber, Kuwait",UNK
    XIL,////,////,ZBXH,UNK,1.0,Xilinhot,"Xilinhot, PR China",OWNO,////,////,????,"Xilinhot, Inner Mongolia, China",OK
    XIN,////,////,ZGXN,UNK,1.0,Xingning,"Xingning, PR China",OWNO,////,////,????,"Xingning, Guangdong, China",OK
    XIY,////,////,ZLXY,UNK,0.898,Xianyang,"Xi An, PR China",OWNO,////,////,Xianyang International Airport,"Xi'an, Shaanxi, China",OK
    XJD,////,////,OTBH,UNK,nan,////,////,////,////,////,Al Udeid AB,"Doha, Qatar",UNK
    XJM,////,////,OPMA,UNK,nan,////,////,////,////,////,Mangla Airport,"Mangla, Punjab, Pakistan",UNK
    XKA,////,////,DFEL,UNK,1.0,Kantchari,"Kantchari, Burkina Faso",OWNO,////,////,????,"Kantchari, Burkina Faso",OK
    XKH,////,////,VLXK,UNK,1.0,Xieng Khouang,"Xieng Khouang, Lao PDR",OWNO,////,////,????,"Xieng Khouang, Lao People's Democratic Republic (Laos)",OK
    XKS,////,////,CYAQ,UNK,1.0,Kasabonika,"Kasabonika, Canada",OWNO,////,////,????,"Kasabonika, Ontario, Canada",OK
    XKY,////,////,DFCA,UNK,1.0,Kaya,"Kaya, Burkina Faso",OWNO,////,////,????,"Kaya, Burkina Faso",OK
    XLB,////,////,CZWH,UNK,1.0,Lac Brochet,"Lac Brochet, Canada",OWNO,////,////,????,"Lac Brochet, Manitoba, Canada",OK
    XLO,////,////,////,UNK,1.0,Long Xuyen,"Long Xuyen, Viet Nam",OWNO,////,////,????,"Long Xuyen, Vietnam",OK
    XLR,////,////,LFDI,UNK,nan,////,////,////,////,////,Artigues de Lussac,"Libourne, Aquitaine, France",UNK
    XLS,////,////,GOSS,UNK,0.842,St Louis,"St Louis, Senegal",OWNO,////,////,Saint-Louis Airport,"Saint-Louis, Saint-Louis, Senegal",TO DO CHECK
    XLU,////,////,DFCL,UNK,1.0,Leo,"Leo, Burkina Faso",OWNO,////,////,Leo Airport,"Leo, Burkina Faso",OK
    XLW,////,////,EDWD,UNK,1.0,Lemwerder,"Lemwerder, Germany",OWNO,////,////,????,"Lemwerder, Lower Saxony, Germany",OK
    XMA,////,////,////,UNK,1.0,Maramag,"Maramag, Philippines",OWNO,////,////,????,"Maramag, Philippines",OK
    XMC,////,////,YMCO,UNK,1.0,Mallacoota,"Mallacoota, Australia",OWNO,////,////,????,"Mallacoota, Victoria, Australia",OK
    XMD,MDS,KMDS,KMDS,OK,0.845,Madison,"Madison (SD), USA",OWNO,MADISON MUNI,"MADISON, SD - UNITED STATES",Madison Municipal Airport,"Madison, South Dakota, United States",OK
    XMG,////,////,VNMN,UNK,1.0,Mahendranagar,"Mahendranagar, Nepal",OWNO,////,////,????,"Mahendranagar, Nepal",OK
    XMH,////,////,NTGI,UNK,1.0,Manihi,"Manihi, French Polynesia",OWNO,////,////,????,"Manihi, French Polynesia",OK
    XMI,////,////,HTMI,UNK,1.0,Masasi,"Masasi, Tanzania",OWNO,////,////,Masasi Airport,"Masasi, Mtwara, Tanzania",OK
    XMJ,////,////,LFBM,UNK,nan,////,////,////,////,////,????,"Mont de Marsan, Aquitaine, France",UNK
    XML,////,////,YMIN,UNK,1.0,Minlaton,"Minlaton, Australia",OWNO,////,////,????,"Minlaton, South Australia, Australia",OK
    XMN,////,////,ZSAM,UNK,0.741,Xiamen,"Xiamen, PR China",OWNO,////,////,Gaoqi International Airport,"Xiamen, Fujian, China",OK
    XMP,////,////,////,UNK,1.0,Macmillan Pass,"Macmillan Pass, Canada",OWNO,////,////,Macmillan Pass Airport,"Macmillan Pass, Yukon, Canada",OK
    XMS,////,////,SEMC,UNK,0.286,Macas,"Macas, Ecuador",OWNO,////,////,Coronel Edmundo Carvajal,"Macas, Morona-Santiago, Ecuador",OK
    XMW,////,////,LFDB,UNK,nan,////,////,////,////,////,????,"Montauban, Midi-Pyrénées, France",UNK
    XMY,////,////,YYMI,UNK,1.0,Yam Island,"Yam Island, Australia",OWNO,////,////,????,"Yam Island, Queensland, Australia",OK
    XNA,XNA,KXNA,KXNA,OK,1.0,Northwest Arkansas Rgn,"Fayetteville (AR), USA",OWNO,NORTHWEST ARKANSAS RGNL,"FAYETTEVILLE/SPRINGDALE/, AR - UNITED STATES",Northwest Arkansas Regional,"Fayetteville, Arkansas, United States",OK
    XNG,////,////,////,UNK,1.0,Quang Ngai,"Quang Ngai, Viet Nam",OWNO,////,////,????,"Quang Ngai, Vietnam",OK
    XNN,////,////,ZLXN,UNK,0.757,Xining,"Xining, PR China",OWNO,////,////,Caojiabu,"Xining, Qinghai, China",OK
    XNT,////,////,ZBXT,UNK,0.788,Xingtai,"Xingtai, PR China",OWNO,////,////,Xingtai Dalian Airport,"Xingtai, Hubei, China",OK
    XNU,////,////,DFON,UNK,1.0,Nouna,"Nouna, Burkina Faso",OWNO,////,////,Nouna Airport,"Nouna, Burkina Faso",OK
    XOG,////,////,LFMO,UNK,nan,////,////,////,////,////,Caritat,"Orange, Provence-Alpes-Côte d'Azur, France",UNK
    XPA,////,////,DFEP,UNK,1.0,Pama,"Pama, Burkina Faso",OWNO,////,////,Pama Airport,"Pama, Burkina Faso",OK
    XPK,////,////,CZFG,UNK,1.0,Pukatawagan,"Pukatawagan, Canada",OWNO,////,////,Pukatawagan Airport,"Pukatawagan, Manitoba, Canada",OK
    XPL,////,////,MHSC,UNK,1.0,Palmerola Air Base,"Comayagua, Honduras",OWNO,////,////,Palmerola Air Base,"Comayagua, Comayagua, Honduras",OK
    XPP,////,////,CZNG,UNK,1.0,Poplar River,"Poplar River, Canada",OWNO,////,////,Poplar River Airport,"Poplar River, Manitoba, Canada",OK
    XPR,IEN,KIEN,KIEN,OK,1.0,Pine Ridge,"Pine Ridge (SD), USA",OWNO,PINE RIDGE,"PINE RIDGE, SD - UNITED STATES",????,"Pine Ridge, South Dakota, United States",OK
    XQC,////,////,ORBD,UNK,nan,////,////,////,////,////,Balad Southeast,"Al Bakr, Iraq",UNK
    XQP,////,////,MRQP,UNK,0.522,Quepos,"Quepos, Costa Rica",OWNO,////,////,La Managua,"Quepos, Puntarenas, Costa Rica",OK
    XQU,////,////,////,UNK,0.727,Qualicum,"Qualicum, Canada",OWNO,////,////,????,"Qualicum Beach, British Columbia, Canada",MAYBE
    XRH,////,////,YSRI,UNK,0.667,RAAF Base,Richmond,IATA,////,////,RAAF Base Richmond,"Richmond, New South Wales, Australia",MAYBE
    XRR,////,CYDM,CYDM,OK,1.0,Ross River,"Ross River, Canada",OWNO,ROSS RIVER,"ROSS RIVER, - CANADA",????,"Ross River, Yukon, Canada",OK
    XRY,////,////,LEJR,UNK,1.0,La Parra,"Jerez de la Frontera, Spain",OWNO,////,////,La Parra,"Jerez de la Frontera, Andalusia, Spain",OK
    XSB,////,////,OMBY,UNK,1.0,Sir Bani Yas Island,Sir Bani Yas Island,IATA,////,////,Sir Bani Yas Airport,"Sir Bani Yas Island, Abu Dhabi, United Arab Emirates",MAYBE
    XSC,////,MBSC,MBSC,OK,0.694,International,"South Caicos, Turks and Caicos Islands",OWNO,SOUTH CAICOS INTL,"COCKBURN HARBOUR, - TURKS AND CAICOS ISLANDS",????,"South Caicos Island, Turks and Caicos Islands",MAYBE
    XSD,TNX,KTNX,KTNX,OK,1.0,Test Range,"Tonopah (NV), USA",OWNO,TONOPAH TEST RANGE,"TONOPAH, NV - UNITED STATES",Tonopah Test Range Airport,"Tonopah, Nevada, United States",OK
    XSE,////,////,DFES,UNK,1.0,Sebba,"Sebba, Burkina Faso",OWNO,////,////,Sebba Airport,"Sebba, Burkina Faso",OK
    XSI,////,////,CZSN,UNK,1.0,South Indian Lake,"South Indian Lake, Canada",OWNO,////,////,South Indian Lake Airport,"South Indian Lake, Manitoba, Canada",OK
    XSJ,////,////,LFAG,UNK,nan,////,////,////,////,////,????,"Peronne / St-Quentin, Picardie (Picardy), France",UNK
    XSO,////,////,RPNO,UNK,1.0,Siocon,"Siocon, Philippines",OWNO,////,////,????,"Siocon, Zamboanga del Norte, Philippines",OK
    XSP,////,////,WSSL,UNK,1.0,Seletar,"Singapore, Singapore",OWNO,////,////,Seletar Airport,"Singapore, Singapore",OK
    XSU,////,////,LFOD,UNK,nan,////,////,////,////,////,????,"Saumur / St-Florent / St-Hilaire, Pays de la Loire, France",UNK
    XTG,////,////,YTGM,UNK,1.0,Thargomindah,"Thargomindah, Australia",OWNO,////,////,????,"Thargomindah, Queensland, Australia",OK
    XTL,////,////,CYBQ,UNK,1.0,Tadoule Lake,"Tadoule Lake, Canada",OWNO,////,////,????,"Tadoule Lake, Manitoba, Canada",OK
    XTO,////,////,YTAM,UNK,1.0,Taroom,"Taroom, Australia",OWNO,////,////,????,"Taroom, Queensland, Australia",OK
    XTR,////,////,YTAA,UNK,1.0,Tara,"Tara, Australia",OWNO,////,////,????,"Tara, Queensland, Australia",OK
    XUZ,////,////,ZSXZ,UNK,0.683,Xuzhou,"Xuzhou, PR China",OWNO,////,////,Xuzhou Guanyin International Airport,"Xuzhou, Jiangsu, China",OK
    XVL,////,////,////,UNK,1.0,Vinh Long,"Vinh Long, Viet Nam",OWNO,////,////,????,"Vinh Long, Vietnam",OK
    XVS,////,////,LFAV,UNK,nan,////,////,////,////,////,Denain,"Valenciennes, Nord-Pas-de-Calais, France",UNK
    XVV,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Belleville, Ontario, Canada",UNK
    XXB,////,////,EGCD,UNK,nan,////,////,////,////,////,Woodford Aerodrome,"Manchester, Cheshire, England, United Kingdom",UNK
    XYA,////,////,AGGY,UNK,1.0,Yandina Airport,"Yandina, Solomon Islands",OWNO,////,////,????,"Yandina, Solomon Islands",OK
    XYE,////,////,VYYE,UNK,1.0,Ye Airport,"Ye, Myanmar",OWNO,////,////,????,"Ye, Mon, Myanmar (Burma)",OK
    XYR,////,////,////,UNK,0.774,Edwaki,"Yellow River, Papua New Guinea",OWNO,////,////,????,"Yellow River, Sandaun, Papua-New Guinea",OK
    XZA,////,////,DFEZ,UNK,1.0,Zabre,"Zabre, Burkina Faso",OWNO,////,////,Zabré Airport,"Zabré, Burkina Faso",OK
    XZL,////,////,////,UNK,nan,////,////,////,////,////,Railway Station,"Edmonton, Alberta, Canada",UNK
    XZM,////,////,////,UNK,nan,////,////,////,////,////,Macau Heliport at Outer Harbour Ferry Terminal,"Macau, Macau",UNK

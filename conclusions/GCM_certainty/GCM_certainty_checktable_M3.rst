
List for checking certainty of Great Circle Mapper (MHG - MLC)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    MHG,////,////,EDFM,UNK,0.762,Mannheim Airport,"Mannheim, Germany",OWNO,////,////,City,"Mannheim, Baden-Württemberg, Germany",OK
    MHH,////,MYAM,MYAM,OK,0.704,International,"Marsh Harbour, Bahamas",OWNO,MARSH HARBOUR INTL,"MARSH HARBOUR, - BAHAMAS",????,"Marsh Harbour, Abaco Islands, Bahamas",OK
    MHI,////,////,HDMO,UNK,1.0,Musha,"Musha, Djibouti",OWNO,////,////,????,"Musha, Djibouti",OK
    MHK,MHK,KMHK,KMHK,OK,1.0,Manhattan Regional Airport,"Manhattan (KS), USA",OWNO,MANHATTAN RGNL,"MANHATTAN, KS - UNITED STATES",Manhattan Regional,"Manhattan, Kansas, United States",OK
    MHL,MHL,KMHL,KMHL,OK,1.0,Memorial Municipal,"Marshall (MO), USA",OWNO,MARSHALL MEMORIAL MUNI,"MARSHALL, MO - UNITED STATES",Marshall Memorial Municipal Airport,"Marshall, Missouri, United States",OK
    MHM,////,////,PAMH,UNK,0.737,Intermediate,"Minchumina (AK), USA",OWNO,////,////,????,"Minchumina, Alaska, United States",OK
    MHN,MHN,////,KMHN,UNK,0.692,Mullen,"Mullen (NE), USA",OWNO,////,////,Hooker County Airport,"Mullen, Nebraska, United States",OK
    MHO,////,////,YMHO,UNK,1.0,Mount House,"Mount House, Australia",OWNO,////,////,????,"Mount House, Western Australia, Australia",OK
    MHP,////,////,UMMM,UNK,0.737,Minsk International 1,"Minsk, Belarus",OWNO,////,////,Minsk-1,"Minsk, Minskaya (Minsk), Belarus",OK
    MHQ,////,////,EFMA,UNK,1.0,Mariehamn,"Mariehamn, Finland",OWNO,////,////,Mariehamn Airport,"Mariehamn, Åland Islands, Ahvenanmaan maakunta (Landskapet Åland (Åland)), Finland",OK
    MHR,MHR,KMHR,KMHR,OK,0.882,Mather AFB,"Sacramento (CA), USA",OWNO,SACRAMENTO MATHER,"SACRAMENTO, CA - UNITED STATES",Sacramento Mather Airport,"Sacramento, California, United States",OK
    MHT,MHT,KMHT,KMHT,OK,0.821,Municipal,"Manchester (NH), USA",OWNO,MANCHESTER,"MANCHESTER, NH - UNITED STATES",????,"Manchester, New Hampshire, United States",OK
    MHU,////,////,YHOT,UNK,1.0,Mount Hotham,"Mount Hotham, Australia",OWNO,////,////,????,"Mount Hotham, Victoria, Australia",OK
    MHV,MHV,KMHV,KMHV,OK,0.319,Kern County,"Mojave (CA), USA",OWNO,MOJAVE AIR AND SPACE PORT,"MOJAVE, CA - UNITED STATES",Mojave Air and Space Port Airport,"Mojave, California, United States",OK
    MHW,////,////,SLAG,UNK,1.0,Monteagudo,"Monteagudo, Bolivia",OWNO,////,////,Monteagudo Airport,"Monteagudo, Hernando Siles, Chuquisaca, Bolivia",OK
    MHX,////,////,NCMH,UNK,1.0,Manihiki Island,"Manihiki Island, Cook Islands",OWNO,////,////,????,"Manihiki Island, Cook Islands",OK
    MHY,////,////,////,UNK,1.0,Morehead,"Morehead, Papua New Guinea",OWNO,////,////,????,"Morehead, Western, Papua-New Guinea",OK
    MHZ,////,////,EGUN,UNK,0.842,Mildenhall Airport,"Mildenhall, United Kingdom",OWNO,////,////,RAF Mildenhall,"Mildenhall, Suffolk, England, United Kingdom",OK
    MIA,MIA,KMIA,KMIA,OK,1.0,International,"Miami (FL), USA",OWNO,MIAMI INTL,"MIAMI, FL - UNITED STATES",Miami International Airport,"Miami, Florida, United States",OK
    MIB,MIB,KMIB,KMIB,OK,1.0,Minot AFB,"Minot (ND), USA",OWNO,MINOT AFB,"MINOT, ND - UNITED STATES",Minot AFB,"Minot, North Dakota, United States",OK
    MIC,MIC,KMIC,KMIC,OK,1.0,Crystal,"Minneapolis/St Paul (MN), USA",OWNO,CRYSTAL,"MINNEAPOLIS, MN - UNITED STATES",Crystal Airport,"Minneapolis, Minnesota, United States",OK
    MID,////,////,MMMD,UNK,0.45,Rejon,"Merida, Mexico",OWNO,////,////,Licenciado Manuel Crecencio Rejón International Airport,"Mérida, Yucatán, México",OK
    MIE,MIE,KMIE,KMIE,OK,0.852,Delaware County,"Muncie (IN), USA",OWNO,DELAWARE COUNTY RGNL,"MUNCIE, IN - UNITED STATES",Delaware County Regional,"Muncie, Indiana, United States",OK
    MIF,E01,////,////,OK,1.0,Roy Hurd Memorial,"Monahans (TX), USA",OWNO,ROY HURD MEMORIAL,"MONAHANS, TX - UNITED STATES",Roy Hurd Memorial Airport,"Monahans, Texas, United States",OK
    MIG,////,////,ZUMY,UNK,1.0,Mian Yang,"Mian Yang, PR China",OWNO,////,////,????,"Mianyang, Sichuan, China",OK
    MIH,////,////,YMIP,UNK,1.0,Mitchell Plateau,"Mitchell Plateau, Australia",OWNO,////,////,????,"Mitchell Plateau, Western Australia, Australia",OK
    MII,////,////,SBML,UNK,0.476,Dr Gastao Vidigal,"Marilia, Brazil",OWNO,////,////,????,"Marília, São Paulo, Brazil",OK
    MIJ,1Q9,////,MLIP,NG,1.0,Mili Island,"Mili Island, Marshall Islands",OWNO,MILI,"MILI ISLAND, - MARSHALL ISLANDS",????,"Mili Island, Marshall Islands",OK
    MIK,////,////,EFMI,UNK,1.0,Mikkeli,"Mikkeli, Finland",OWNO,////,////,Mikkeli Airport,"Mikkeli, Etelä-Savo (Södra Savolax (Southern Savonia)), Finland",OK
    MIL,////,////,////,UNK,1.0,Metropolitan Area,Milan,IATA,////,////,Metropolitan Area,"Milan, Lombardy, Italy",MAYBE
    MIM,////,////,YMER,UNK,1.0,Merimbula,"Merimbula, Australia",OWNO,////,////,????,"Merimbula, New South Wales, Australia",OK
    MIN,////,////,YMPA,UNK,1.0,Minnipa,"Minnipa, Australia",OWNO,////,////,????,"Minnipa, South Australia, Australia",OK
    MIO,MIO,KMIO,KMIO,OK,0.719,Miami,"Miami (OK), USA",OWNO,MIAMI REGIONAL,"MIAMI, OK - UNITED STATES",Miami Regional,"Miami, Oklahoma, United States",OK
    MIP,////,////,LLRM,UNK,0.833,Mitspeh Ramon,"Mitspeh Ramon, Israel",OWNO,////,////,Ramon AB,"Mitzpeh Ramon, Israel",OK
    MIQ,MLE,KMLE,KMLE,OK,1.0,Millard,"Omaha (NE), USA",OWNO,MILLARD,"OMAHA, NE - UNITED STATES",Millard Airport,"Omaha, Nebraska, United States",OK
    MIR,////,////,DTMB,UNK,1.0,Habib Bourguiba International,"Monastir, Tunisia",OWNO,////,////,Habib Bourguiba International Airport,"Monastir, Tunisia",OK
    MIS,////,////,AYMS,UNK,1.0,Misima Island,"Misima Island, Papua New Guinea",OWNO,////,////,????,"Misima Island, Milne Bay, Papua-New Guinea",OK
    MIT,MIT,KMIT,KMIT,OK,0.666,Kern County,"Shafter (CA), USA",OWNO,SHAFTER-MINTER FIELD,"SHAFTER, CA - UNITED STATES",Shafter-Minter Field,"Shafter, California, United States",OK
    MIU,////,////,DNMA,UNK,0.783,Maiduguri,"Maiduguri, Nigeria",OWNO,////,////,Maiduguri International Airport,"Maiduguri, Borno, Nigeria",OK
    MIV,MIV,KMIV,KMIV,OK,0.79,Millville,"Millville (NJ), USA",OWNO,MILLVILLE MUNI,"MILLVILLE, NJ - UNITED STATES",Millville Municipal Airport,"Millville, New Jersey, United States",OK
    MIW,MIW,KMIW,KMIW,OK,1.0,Municipal,"Marshalltown IA, USA",OWNO,MARSHALLTOWN MUNI,"MARSHALLTOWN, IA - UNITED STATES",Marshalltown Municipal Airport,"Marshalltown, Iowa, United States",OK
    MIX,////,////,////,UNK,1.0,Miriti,"Miriti, Colombia",OWNO,////,////,????,"Miriti, Amazonas, Colombia",OK
    MIY,////,////,YMTA,UNK,1.0,Mittiebah,"Mittiebah, Australia",OWNO,////,////,????,"Mittiebah, Northern Territory, Australia",OK
    MIZ,////,////,////,UNK,1.0,Mainoru,"Mainoru, Australia",OWNO,////,////,????,"Mainoru, Northern Territory, Australia",OK
    MJA,////,////,FMSJ,UNK,1.0,Manja,"Manja, Madagascar",OWNO,////,////,????,"Manja, Madagascar",OK
    MJB,Q30,////,////,OK,0.371,Mejit Island,"Mejit Island, Marshall Islands",OWNO,MEJIT,"MEJIT ATOLL, - MARSHALL ISLANDS",????,"Mejit Atoll, Marshall Islands",MAYBE
    MJC,////,////,DIMN,UNK,1.0,Man,"Man, Cote d'Ivoire",OWNO,////,////,????,"Man, 18 Montagnes (Dix-Huit Montagnes), Côte d'Ivoire (Ivory Coast)",OK
    MJD,////,////,OPMJ,UNK,1.0,Mohenjodaro,"Mohenjodaro, Pakistan",OWNO,////,////,????,"Mohenjodaro, Sindh, Pakistan",OK
    MJE,////,////,////,UNK,1.0,Majkin,"Majkin, Marshall Islands",OWNO,////,////,????,"Majkin, Marshall Islands",OK
    MJF,////,////,ENMS,UNK,1.0,Kjaerstad,"Mosjoen, Norway",OWNO,////,////,Kjaerstad,"Mosjøen, Norway",OK
    MJI,////,////,HLLM,UNK,1.0,Mitiga,"Mitiga, Libya",OWNO,////,////,Mitiga,"Tripoli, Libyan Arab Jamahiriya (Libya)",OK
    MJJ,////,////,////,UNK,1.0,Moki,"Moki, Papua New Guinea",OWNO,////,////,????,"Moki, Madang, Papua-New Guinea",OK
    MJK,////,////,YSHK,UNK,1.0,Shark Bay,"Monkey Mia, Australia",OWNO,////,////,Shark Bay,"Monkey Mia, Western Australia, Australia",OK
    MJL,////,////,FOGM,UNK,0.667,Mouila,"Mouila, Gabon",OWNO,////,////,Ville Airport,"Mouila, Ngounié, Gabon",OK
    MJM,////,////,FZWA,UNK,1.0,Mbuji Mayi,"Mbuji Mayi, Congo (DRC)",OWNO,////,////,Mbuji Mayi Airport,"Mbuji Mayi, Kasai-Oriental (East Kasai), Democratic Republic of Congo (Zaire)",OK
    MJN,////,////,FMNM,UNK,0.621,Amborovy,"Majunga, Madagascar",OWNO,////,////,Philibert Tsiranana,"Mahajanga, Madagascar",OK
    MJP,////,////,YMJM,UNK,1.0,Manjimup,"Manjimup, Australia",OWNO,////,////,????,"Manjimup, Western Australia, Australia",OK
    MJQ,MJQ,KMJQ,KMJQ,OK,0.76,Jackson,"Jackson (MN), USA",OWNO,JACKSON MUNI,"JACKSON, MN - UNITED STATES",Jackson Municipal Airport,"Jackson, Minnesota, United States",OK
    MJR,////,////,////,UNK,1.0,Miramar,"Miramar, Argentina",OWNO,////,////,????,"Miramar, Buenos Aires, Argentina",OK
    MJT,////,////,LGMT,UNK,1.0,Mytilene,"Mytilene, Greece",OWNO,////,////,????,"Mytilene, Lesbos Island, Voreío Aigaío (Northern Aegean), Greece",OK
    MJU,////,////,WAAJ,UNK,1.0,Mamuju,"Mamuju, Indonesia",OWNO,////,////,????,"Mamuju, Sulawesi Selatan, Indonesia",OK
    MJV,////,////,LELC,UNK,1.0,San Javier,"Murcia, Spain",OWNO,////,////,San Javier,"Murcia, Murcia, Spain",OK
    MJX,MJX,KMJX,KMJX,OK,0.523,Robert J. Miller,"Toms River (NJ), USA",OWNO,OCEAN COUNTY,"TOMS RIVER, NJ - UNITED STATES",Ocean County Airport,"Toms River, New Jersey, United States",OK
    MJZ,////,////,UERR,UNK,0.909,Mirnyj,"Mirnyj, Russia",OWNO,////,////,Mirny Airport,"Mirny, Sakha (Yakutiya), Russian Federation (Russia)",TO DO CHECK
    MKA,////,////,LKMR,UNK,1.0,Marianske Lazne,"Marianske Lazne, Czech Republic",OWNO,////,////,Marianske Lazne Airport,"Marianske Lazne, Karlovy Vary, Czech Republic",OK
    MKB,////,////,FOOE,UNK,1.0,Mekambo,"Mekambo, Gabon",OWNO,////,////,????,"Mekambo, Ogooué-Ivindo, Gabon",OK
    MKC,MKC,KMKC,KMKC,OK,0.627,Downtown,"Kansas City (MO), USA",OWNO,CHARLES B WHEELER DOWNTOWN,"KANSAS CITY, MO - UNITED STATES",Charles B Wheeler Downtown Airport,"Kansas City, Missouri, United States",OK
    MKE,MKE,KMKE,KMKE,OK,0.874,General Mitchell,"Milwaukee (WI), USA",OWNO,GENERAL MITCHELL INTL,"MILWAUKEE, WI - UNITED STATES",General Mitchell International Airport,"Milwaukee, Wisconsin, United States",OK
    MKG,MKG,KMKG,KMKG,OK,0.687,Muskegon,"Muskegon (MI), USA",OWNO,MUSKEGON COUNTY,"MUSKEGON, MI - UNITED STATES",Muskegon County Airport,"Muskegon, Michigan, United States",OK
    MKH,////,////,FXMK,UNK,1.0,Mokhotlong,"Mokhotlong, Lesotho",OWNO,////,////,????,"Mokhotlong, Lesotho",OK
    MKI,////,////,FEGE,UNK,1.0,M'Boki,"M'Boki, Central African Republic",OWNO,////,////,M'Boki,"Obo, Haut-Mbomou (Tö-Mbömü), Central African Republic",OK
    MKJ,////,////,FCOM,UNK,1.0,Makoua,"Makoua, Congo (ROC)",OWNO,////,////,????,"Makoua, Congo (Republic of)",OK
    MKK,MKK,PHMK,PHMK,OK,1.0,Molokai,"Hoolehua (HI), USA",OWNO,MOLOKAI,"KAUNAKAKAI, HI - UNITED STATES",Molokai Airport,"Kaunakakai, Molokai, Hawaii, United States",OK
    MKL,MKL,KMKL,KMKL,OK,0.572,Mckellar,"Jackson (TN), USA",OWNO,MC KELLAR-SIPES RGNL,"JACKSON, TN - UNITED STATES",Mc Kellar-Sipes Regional,"Jackson, Tennessee, United States",OK
    MKM,////,////,WBGK,UNK,1.0,Mukah Airport,"Mukah, Malaysia",OWNO,////,////,????,"Mukah, Sarawak, Malaysia",OK
    MKN,////,////,////,UNK,1.0,Malekolon,"Malekolon, Papua New Guinea",OWNO,////,////,????,"Malekolon, New Ireland, Papua-New Guinea",OK
    MKO,MKO,KMKO,KMKO,OK,1.0,Davis Field,"Muskogee (OK), USA",OWNO,DAVIS FIELD,"MUSKOGEE, OK - UNITED STATES",Davis Field,"Muskogee, Oklahoma, United States",OK
    MKP,////,////,NTGM,UNK,1.0,Makemo,"Makemo, French Polynesia",OWNO,////,////,????,"Makemo, French Polynesia",OK
    MKQ,////,////,WAKK,UNK,1.0,Mopah,"Merauke, Indonesia",OWNO,////,////,Mopah,"Merauke, Papua, Indonesia",OK
    MKR,////,////,YMEK,UNK,1.0,Meekatharra,"Meekatharra, Australia",OWNO,////,////,????,"Meekatharra, Western Australia, Australia",OK
    MKS,////,////,HAMA,UNK,1.0,Mekane Selam,"Mekane Selam, Ethiopia",OWNO,////,////,Mekane Selam Airport,"Mekane Selam, Amara, Ethiopia",OK
    MKT,MKT,KMKT,KMKT,OK,0.748,Municipal,"Mankato (MN), USA",OWNO,MANKATO RGNL,"MANKATO, MN - UNITED STATES",Mankato Regional,"Mankato, Minnesota, United States",OK
    MKU,////,////,FOOK,UNK,1.0,Makokou,"Makokou, Gabon",OWNO,////,////,????,"Makokou, Ogooué-Ivindo, Gabon",OK
    MKV,////,////,YMVG,UNK,1.0,Mt Cavenagh,"Mt Cavenagh, Australia",OWNO,////,////,????,"Mt. Cavenagh, Northern Territory, Australia",OK
    MKW,////,////,WASR,UNK,1.0,Rendani,"Manokwari, Indonesia",OWNO,////,////,Rendani,"Manokwari, Irian Jaya, Papua, Indonesia",OK
    MKY,////,////,YBMK,UNK,1.0,Mackay,"Mackay, Australia",OWNO,////,////,????,"Mackay, Queensland, Australia",OK
    MKZ,////,////,WMKM,UNK,1.0,Batu Berendum,"Malacca, Malaysia",OWNO,////,////,Batu Berendam,"Batu Berendam, Melaka, Malaysia",OK
    MLA,////,////,LMML,UNK,0.842,Luqa,"Malta, Valletta, Malta",OWNO,////,////,Malta International Airport,"Gudja/Luqa, Malta",OK
    MLB,MLB,KMLB,KMLB,OK,1.0,Melbourne International,"Melbourne (FL), USA",OWNO,MELBOURNE INTL,"MELBOURNE, FL - UNITED STATES",Melbourne International Airport,"Melbourne, Florida, United States",OK
    MLC,MLC,KMLC,KMLC,OK,0.781,Mc Alester,"Mc Alester (OK), USA",OWNO,MC ALESTER RGNL,"MC ALESTER, OK - UNITED STATES",Mc Alester Regional,"Mc Alester, Oklahoma, United States",OK

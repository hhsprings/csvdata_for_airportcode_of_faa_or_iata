
List for checking certainty of Great Circle Mapper (JAA - JNA)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    JAA,////,////,OAJL,UNK,1.0,Jalalabad,"Jalalabad, Afghanistan",OWNO,////,////,????,"Jalalabad, Nangarhar, Afghanistan",OK
    JAB,////,////,YJAB,UNK,1.0,Jabiru,"Jabiru, Australia",OWNO,////,////,????,"Jabiru, Northern Territory, Australia",OK
    JAC,JAC,KJAC,KJAC,OK,1.0,Jackson Hole,"Jackson (WY), USA",OWNO,JACKSON HOLE,"JACKSON, WY - UNITED STATES",Jackson Hole Airport,"Jackson, Wyoming, United States",OK
    JAD,////,////,YPJT,UNK,1.0,Jandakot,"Jandakot, Australia",OWNO,////,////,????,"Jandakot, Western Australia, Australia",OK
    JAE,////,////,SPJE,UNK,0.581,Aeropuerto de Shumba,Jaen,IATA,////,////,Shumba Airport,"Jaén, Cajamarca, Perú",MAYBE
    JAF,////,////,VCCJ,UNK,1.0,Kankesanturai,"Jaffna, Sri Lanka",OWNO,////,////,Jaffna Airport,"Kankesanturai, Northern Province, Sri Lanka (Ceylon)",OK
    JAG,////,////,OPJA,UNK,0.783,Jacobabad,"Jacobabad, Pakistan",OWNO,////,////,Shahbaz AB,"Jacobabad, Sindh, Pakistan",OK
    JAI,////,////,VIJP,UNK,1.0,Sanganeer,"Jaipur, India",OWNO,////,////,Sanganeer,"Jaipur, Rajasthan, India",OK
    JAK,////,////,MTJA,UNK,1.0,Jacmel,"Jacmel, Haiti",OWNO,////,////,????,"Jacmel, Haiti",OK
    JAL,////,////,MMJA,UNK,0.526,Jalapa,"Jalapa, Mexico",OWNO,////,////,El Lencero,"Jalapa, Veracruz, México",OK
    JAM,////,////,////,UNK,0.556,Jambol,"Jambol, Bulgaria",OWNO,////,////,Bezmer AB,"Yambol, Burgas, Bulgaria",TO DO CHECK
    JAN,JAN,KJAN,KJAN,OK,0.535,Jackson International,"Jackson (MS), USA",OWNO,JACKSON-MEDGAR WILEY EVERS INTL,"JACKSON, MS - UNITED STATES",Jackson-Evers International Airport,"Jackson, Mississippi, United States",OK
    JAQ,////,////,////,UNK,1.0,Jacquinot Bay,"Jacquinot Bay, Papua New Guinea",OWNO,////,////,????,"Jacquinot Bay, East New Britain, Papua-New Guinea",OK
    JAR,////,////,OISJ,UNK,nan,////,////,////,////,////,????,"Jahrom, Fars, Iran",UNK
    JAS,JAS,KJAS,KJAS,OK,0.64,County,"Jasper (TX), USA",OWNO,JASPER COUNTY-BELL FIELD,"JASPER, TX - UNITED STATES",Jasper County-Bell Field,"Jasper, Texas, United States",OK
    JAT,////,////,////,UNK,1.0,Jabot,"Jabot, Marshall Islands",OWNO,////,////,????,"Jabot, Marshall Islands",OK
    JAU,////,////,SPJJ,UNK,0.385,Jauja,"Jauja, Peru",OWNO,////,////,Francisco Carle Airport,"Jauja, Junín, Perú",OK
    JAV,////,////,BGJN,UNK,0.5,Ilulissat,"Ilulissat, Greenland",OWNO,////,////,????,"Jakobshavn, Qaasuitsup, Greenland",TO DO CHECK
    JAX,JAX,KJAX,KJAX,OK,1.0,International,"Jacksonville (FL), USA",OWNO,JACKSONVILLE INTL,"JACKSONVILLE, FL - UNITED STATES",Jacksonville International Airport,"Jacksonville, Florida, United States",OK
    JBB,////,////,WARE,UNK,1.0,Noto Hadinegoro,Jember,IATA,////,////,Noto Hadinegoro Airport,"Jember, Jawa Timur, Indonesia",MAYBE
    JBQ,////,////,MDJB,UNK,1.0,La Isabela International Airport,"Santo Domingo, Dominican Republic",OWNO,////,////,La Isabela International Airport,"Santo Domingo, Distrito Nacional, Dominican Republic",OK
    JBR,JBR,KJBR,KJBR,OK,0.79,Jonesboro,"Jonesboro (AR), USA",OWNO,JONESBORO MUNI,"JONESBORO, AR - UNITED STATES",Jonesboro Municipal Airport,"Jonesboro, Arkansas, United States",OK
    JBT,Z59,////,////,OK,0.409,City Landing,"Bethel (AK), USA",OWNO,BETHEL,"BETHEL, AK - UNITED STATES",Bethel SPB,"Bethel, Alaska, United States",OK
    JCA,////,////,////,UNK,1.0,Croisette Heliport,"Cannes, France",OWNO,////,////,Croisette Heliport,"Cannes, Provence-Alpes-Côte d'Azur, France",OK
    JCB,////,////,SSJA,UNK,1.0,Joacaba,"Joacaba, Brazil",OWNO,////,////,????,"Joacaba, Santa Catarina, Brazil",OK
    JCH,////,////,BGCH,UNK,1.0,Qasigiannguit,"Qasigiannguit, Greenland",OWNO,////,////,????,"Qasigiannguit, Qaasuitsup, Greenland",OK
    JCI,IXD,KIXD,KIXD,OK,0.382,Johnson Industrial,"Kansas City (MO), USA",OWNO,NEW CENTURY AIRCENTER,"OLATHE, KS - UNITED STATES",New Century Aircenter Airport,"Olathe, Kansas, United States",OK
    JCK,////,////,YJLC,UNK,1.0,Julia Creek,"Julia Creek, Australia",OWNO,////,////,????,"Julia Creek, Queensland, Australia",OK
    JCM,////,////,SNJB,UNK,1.0,Jacobina,"Jacobina, Brazil",OWNO,////,////,????,"Jacobina, Bahia, Brazil",OK
    JCO,////,////,////,UNK,1.0,Heliport,"Comino, Malta",OWNO,////,////,Heliport,"Comino, Malta",OK
    JCR,////,////,SBEK,UNK,1.0,Jacareacanga,"Jacareacanga, Brazil",OWNO,////,////,????,"Jacareacanga, Pará, Brazil",OK
    JCT,JCT,KJCT,KJCT,OK,1.0,Kimble County,"Junction (TX), USA",OWNO,KIMBLE COUNTY,"JUNCTION, TX - UNITED STATES",Kimble County Airport,"Junction, Texas, United States",OK
    JCU,////,////,GECT,UNK,1.0,Ceuta Heliport,"Ceuta, Spain",OWNO,////,////,Ceuta Heliport,"Ceuta, Ceuta, Spain",OK
    JCY,0TE7,////,////,OK,0.402,Johnson,"Johnson (TX), USA",OWNO,LBJ RANCH,"JOHNSON CITY, TX - UNITED STATES",LBJ Ranch Airport,"Johnson City, Texas, United States",OK
    JDA,GCD,KGCD,KGCD,OK,0.504,John Day,"John Day (OR), USA",OWNO,GRANT CO RGNL/OGILVIE FIELD,"JOHN DAY, OR - UNITED STATES",Grant Co. Regional/Ogilvie Field,"John Day, Oregon, United States",OK
    JDF,////,////,SBJF,UNK,1.0,Francisco De Assis,"Juiz De Fora, Brazil",OWNO,////,////,Francisco de Assis,"Juiz de Fora, Minas Gerais, Brazil",OK
    JDG,////,////,RKPD,UNK,nan,////,////,////,////,////,Jeongseok Airport,"Jeongseok, Cheju Do Island, Republic of Korea (South Korea)",UNK
    JDH,////,////,VIJO,UNK,1.0,Jodhpur,"Jodhpur, India",OWNO,////,////,????,"Jodhpur, Rajasthan, India",OK
    JDN,JDN,KJDN,KJDN,OK,1.0,Jordan,"Jordan (MT), USA",OWNO,JORDAN,"JORDAN, MT - UNITED STATES",????,"Jordan, Montana, United States",OK
    JDO,////,////,SBJU,UNK,1.0,Regional Do Cariri,"Juazeiro Do Norte, Brazil",OWNO,////,////,Regional do Cariri,"Juazeiro do Norte, Ceará, Brazil",OK
    JDZ,////,////,ZSJD,UNK,1.0,Jingdezhen,"Jingdezhen, PR China",OWNO,////,////,????,"Jingdezhen, Jiangxi, China",OK
    JED,////,////,OEJN,UNK,1.0,King Abdulaziz International,"Jeddah, Saudi Arabia",OWNO,////,////,King Abdulaziz International Airport,"Jeddah, Saudi Arabia",OK
    JEE,////,////,MTJE,UNK,1.0,Jeremie,"Jeremie, Haiti",OWNO,////,////,????,"Jérémie, Haiti",OK
    JEF,JEF,KJEF,KJEF,OK,1.0,Jefferson Memorial,"Jefferson City (MO), USA",OWNO,JEFFERSON CITY MEMORIAL,"JEFFERSON CITY, MO - UNITED STATES",Jefferson City Memorial Airport,"Jefferson City, Missouri, United States",OK
    JEG,////,////,BGAA,UNK,1.0,Aasiaat,"Aasiaat, Greenland",OWNO,////,////,????,"Aasiaat, Qaasuitsup, Greenland",OK
    JEJ,////,////,////,UNK,1.0,Jeh,"Jeh, Marshall Islands",OWNO,////,////,????,"Jeh, Marshall Islands",OK
    JEK,////,////,FLJK,UNK,0.471,Jeki,Lower Zambezi Nat.Park,IATA,////,////,Jeki Airstrip,"Lower Zambezi National Park, Lusaka, Zambia",MAYBE
    JEQ,////,////,SNJK,UNK,1.0,Jequie,"Jequie, Brazil",OWNO,////,////,????,"Jequie, Bahia, Brazil",OK
    JER,////,////,EGJJ,UNK,0.837,States,"Jersey, United Kingdom",OWNO,////,////,Jersey Airport,"St. Peter, Jersey",OK
    JFK,JFK,KJFK,KJFK,OK,1.0,John F Kennedy International,"New York (NY), USA",OWNO,JOHN F KENNEDY INTL,"NEW YORK, NY - UNITED STATES",John F Kennedy International Airport,"New York, New York, United States",OK
    JFN,HZY,KHZY,KHZY,OK,0.482,Ashtabula,"Jefferson (OH), USA",OWNO,NORTHEAST OHIO RGNL,"ASHTABULA, OH - UNITED STATES",Northeast Ohio Regional,"Ashtabula, Ohio, United States",OK
    JFR,////,////,BGPT,UNK,1.0,Paamiut,"Paamiut, Greenland",OWNO,////,////,Paamiut Airport,"Paamiut, Sermersooq, Greenland",OK
    JGA,////,////,VAJM,UNK,0.75,Govardhanpur,"Jamnagar, India",OWNO,////,////,????,"Jamnagar, Gujarat, India",OK
    JGB,////,////,////,UNK,1.0,Jagdalpur,"Jagdalpur, India",OWNO,////,////,????,"Jagdalpur, Chhattisgarh, India",OK
    JGD,////,////,ZYJD,UNK,1.0,Jiagedaqi,Jiagedaqi,IATA,////,////,Jiagedaqi Airport,"Jiagedaqi, Inner Mongolia, China",MAYBE
    JGO,////,////,BGGN,UNK,0.727,Qeqertarsuaq,"Qeqertarsuaq, Greenland",OWNO,////,////,Qeqertarsuaq Heliport,"Qeqertarsuaq, Qaasuitsup, Greenland",OK
    JGR,////,////,BGGD,UNK,0.533,Heliport,"Groennedal, Greenland",OWNO,////,////,Kangilinnguit Heliport,"Kangilinnguit, Sermersooq, Greenland",TO DO CHECK
    JGS,////,////,ZSJA,UNK,0.71,Ji'an,"Ji'an, PR China",OWNO,////,////,Jinggangshan,"Ji'an, Jiangxi, China",OK
    JHB,////,////,WMKJ,UNK,0.765,Sultan Ismail International,"Johor Bahru, Malaysia",OWNO,////,////,Senai International Airport,"Johor Bahru, Johor, Malaysia",OK
    JHE,////,////,ESHH,UNK,0.788,Heliport,"Angelholm/Helsingborg, Sweden",OWNO,////,////,Hamnen,"Helsingborg, Sweden",OK
    JHG,////,////,ZPJH,UNK,0.789,Gasa,"Jinghong, PR China",OWNO,////,////,Xishuangbanna Gasa International Airport,"Jinghong, Yunnan, China",OK
    JHM,JHM,PHJH,PHJH,OK,1.0,Kapalua,"Kapalua (HI), USA",OWNO,KAPALUA,"LAHAINA, HI - UNITED STATES",Kapalua Airport,"Lahaina, Maui, Hawaii, United States",OK
    JHQ,////,////,YSHR,UNK,1.0,Shute Harbour H/P,"Shute Harbour, Australia",OWNO,////,////,????,"Shute Harbour, Queensland, Australia",OK
    JHS,////,////,BGSS,UNK,1.0,Sisimiut,"Sisimiut, Greenland",OWNO,////,////,????,"Sisimiut, Qeqqata, Greenland",OK
    JHW,JHW,KJHW,KJHW,OK,1.0,Jamestown,"Jamestown (NY), USA",OWNO,CHAUTAUQUA COUNTY/JAMESTOWN,"JAMESTOWN, NY - UNITED STATES",Chautauqua County/Jamestown Airport,"Jamestown, New York, United States",OK
    JIA,////,////,SWJN,UNK,1.0,Juina,"Juina, Brazil",OWNO,////,////,Juína,"Juína, Mato Grosso, Brazil",OK
    JIB,////,////,HDAM,UNK,1.0,Ambouli,"Djibouti, Djibouti",OWNO,////,////,Ambouli,"Djibouti, Djibouti",OK
    JIC,////,////,ZLJC,UNK,nan,////,////,////,////,////,Jinchang Airport,"Jinchang, Gansu, China",UNK
    JIJ,////,////,HAJJ,UNK,0.615,Jigiga,"Jijiga, Ethiopia",OWNO,////,////,Garaad Wiil-Waal Airport,"Jijiga, Somali, Ethiopia",OK
    JIK,////,////,LGIK,UNK,1.0,Ikaria,"Ikaria Island, Greece",OWNO,////,////,????,"Ikaria, Voreío Aigaío (Northern Aegean), Greece",OK
    JIL,////,////,ZYJL,UNK,1.0,Jilin,"Jilin, PR China",OWNO,////,////,????,"Jilin, Jilin, China",OK
    JIM,////,////,HAJM,UNK,0.5,Jimma,"Jimma, Ethiopia",OWNO,////,////,Aba Segud Airport,"Jimma, Oromia, Ethiopia",OK
    JIN,////,////,HUJI,UNK,1.0,Jinja,"Jinja, Uganda",OWNO,////,////,????,"Jinja, Uganda",OK
    JIP,////,////,SEJI,UNK,1.0,Jipijapa,"Jipijapa, Ecuador",OWNO,////,////,????,"Jipijapa, Manabí, Ecuador",OK
    JIQ,////,////,ZUQJ,UNK,0.72,Wulingshan,Qianjiang,IATA,////,////,Qianjiang Wulingshan Airport,"Qianjiang, Chongqing, China",MAYBE
    JIR,////,////,VNJI,UNK,1.0,Jiri,"Jiri, Nepal",OWNO,////,////,????,"Jiri, Nepal",OK
    JIU,////,////,ZSJJ,UNK,1.0,Jiujiang Lushan Airport,"Jiujiang, PR China",OWNO,////,////,Lushan,"Jiujiang, Jiangxi, China",OK
    JIW,////,////,OPJI,UNK,1.0,Jiwani,"Jiwani, Pakistan",OWNO,////,////,????,"Jiwani, Balochistan, Pakistan",OK
    JJA,////,////,////,UNK,nan,////,////,////,////,////,Jajao Airport,"Jajao, Solomon Islands",UNK
    JJG,////,////,SBJA,UNK,nan,////,////,////,////,////,Humberto Ghizzo Bortoluzzi Regional Airport,"Jaguaruna, Santa Catarina, Brazil",UNK
    JJI,////,////,SPJI,UNK,1.0,Juanjui,"Juanjui, Peru",OWNO,////,////,Juanjuí Airport,"Juanjuí, San Martín, Perú",OK
    JJN,////,////,ZSQZ,UNK,0.842,Jinjiang,"Jinjiang, PR China",OWNO,////,////,????,"Quanzhou, Fujian, China",OK
    JJU,////,////,BGJH,UNK,1.0,Heliport,"Qaqortoq, Greenland",OWNO,////,////,Qaqortoq Heliport,"Qaqortoq, Kujalleq, Greenland",OK
    JKG,////,////,ESGJ,UNK,0.783,Axamo,"Jonkoping, Sweden",OWNO,////,////,????,"Jönköping, Jönköpings län, Sweden",OK
    JKH,////,////,LGHI,UNK,1.0,Chios,"Chios, Greece",OWNO,////,////,????,"Chios, Voreío Aigaío (Northern Aegean), Greece",OK
    JKL,////,////,LGKY,UNK,nan,////,////,////,////,////,Kalymnos Island National Airport,"Kalymnos, Kalymnos Island, Notío Aigaío (Southern Aegean), Greece",UNK
    JKR,////,////,VNJP,UNK,1.0,Janakpur,"Janakpur, Nepal",OWNO,////,////,????,"Janakpur, Nepal",OK
    JKT,////,////,////,UNK,1.0,Metropolitan Area,"Jakarta, Indonesia",OWNO,////,////,Metropolitan Area,"Jakarta, Jakarta Raya, Indonesia",OK
    JKV,JSO,KJSO,KJSO,OK,0.494,Jacksonville,"Jacksonville (TX), USA",OWNO,CHEROKEE COUNTY,"JACKSONVILLE, TX - UNITED STATES",Cherokee County Airport,"Jacksonville, Texas, United States",OK
    JLA,JLA,////,////,OK,1.0,Quartz Creek,"Cooper Lodge (AK), USA",OWNO,QUARTZ CREEK,"COOPER LANDING, AK - UNITED STATES",Quartz Creek Airport,"Cooper Landing, Alaska, United States",OK
    JLD,////,////,////,UNK,1.0,Heliport,"Landskrona, Sweden",OWNO,////,////,Heliport,"Landskrona, Skåne län, Sweden",OK
    JLN,JLN,KJLN,KJLN,OK,0.734,Joplin,"Joplin (MO), USA",OWNO,JOPLIN RGNL,"JOPLIN, MO - UNITED STATES",Joplin Regional,"Joplin, Missouri, United States",OK
    JLR,////,////,VAJB,UNK,1.0,Jabalpur,"Jabalpur, India",OWNO,////,////,????,"Jabalpur, Madhya Pradesh, India",OK
    JMB,////,////,////,UNK,1.0,Jamba,"Jamba, Angola",OWNO,////,////,????,"Jamba, Angola",OK
    JMK,////,////,LGMK,UNK,0.571,Mikonos,"Mikonos, Greece",OWNO,////,////,????,"Mykonos Island, Notío Aigaío (Southern Aegean), Greece",MAYBE
    JMO,////,////,VNJS,UNK,1.0,Jomsom,"Jomsom, Nepal",OWNO,////,////,????,"Jomsom, Nepal",OK
    JMS,JMS,KJMS,KJMS,OK,0.81,Jamestown,"Jamestown (ND), USA",OWNO,JAMESTOWN RGNL,"JAMESTOWN, ND - UNITED STATES",Jamestown Regional,"Jamestown, North Dakota, United States",OK
    JMU,////,////,ZYJM,UNK,0.824,Jiamusi,"Jiamusi, PR China",OWNO,////,////,Jiamusi Dongjiao Airport,"Jiamusi, Heilongjiang, China",OK
    JMY,////,////,////,UNK,1.0,Mammy Yoko Heliport,"Freetown, Sierra Leone",OWNO,////,////,Mammy Yoko Heliport,"Freetown, Sierra Leone",OK
    JNA,////,////,SNJN,UNK,1.0,Januaria,"Januaria, Brazil",OWNO,////,////,????,"Januária, Minas Gerais, Brazil",OK

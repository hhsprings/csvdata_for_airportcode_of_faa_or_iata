
List for checking certainty of Great Circle Mapper (TAA - TEO)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    TAA,////,////,////,UNK,1.0,Tarapaina,"Tarapaina, Solomon Islands",OWNO,////,////,????,"Tarapaina, Solomon Islands",OK
    TAB,////,////,TTCP,UNK,0.435,Tobago,"Tobago, Trinidad and Tobago",OWNO,////,////,Crown Point,"Scarborough, Tobago Island, Trinidad and Tobago",MAYBE
    TAC,////,////,RPVA,UNK,0.898,D.Z. Romualdez,"Tacloban, Philippines",OWNO,////,////,Daniel Z. Romualdez,"Tacloban, Philippines",OK
    TAD,TAD,KTAD,KTAD,OK,0.552,Las Animas,"Trinidad (CO), USA",OWNO,PERRY STOKES,"TRINIDAD, CO - UNITED STATES",Perry Stokes Airport,"Trinidad, Colorado, United States",OK
    TAE,////,////,RKTN,UNK,0.667,Daegu,"Daegu, South Korea",OWNO,////,////,Daegu International Airport,"Daegu, Republic of Korea (South Korea)",OK
    TAF,////,////,DAOL,UNK,1.0,Tafaraoui,"Oran, Algeria",OWNO,////,////,Oran Tafaraoui Airport,"Oran, Oran, Algeria",OK
    TAG,////,////,RPVT,UNK,1.0,Tagbilaran,"Tagbilaran, Philippines",OWNO,////,////,????,"Tagbilaran, Philippines",OK
    TAH,////,////,NVVW,UNK,1.0,Tanna,"Tanna, Vanuatu",OWNO,////,////,????,"Tanna, Tanna Island, Taféa, Vanuatu",OK
    TAI,////,////,OYTZ,UNK,0.5,Al Janad,"Taiz, Yemen",OWNO,////,////,Ta'izz International Airport,"Ta'izz, Yemen",OK
    TAJ,////,////,////,UNK,1.0,Tadji,"Aitape, Papua New Guinea",OWNO,////,////,Tadji,"Aitape, Sandaun, Papua-New Guinea",OK
    TAK,////,////,RJOT,UNK,1.0,Takamatsu,"Takamatsu, Japan",OWNO,////,////,Takamatsu Airport,"Takamatsu, Shikoku Island, Kagawa, Japan",OK
    TAL,TAL,PATA,PATA,OK,0.757,Ralph Calhoun,"Tanana (AK), USA",OWNO,RALPH M CALHOUN MEMORIAL,"TANANA, - UNITED STATES",Ralph M Calhoun Memorial Airport,"Tanana, Alaska, United States",OK
    TAM,////,////,MMTM,UNK,0.746,Gen F Javier Mina,"Tampico, Mexico",OWNO,////,////,General Francisco Javier Mina International Airport,"Tampico, Tamaulipas, México",OK
    TAN,////,////,YTGA,UNK,1.0,Tangalooma,"Tangalooma, Australia",OWNO,////,////,????,"Tangalooma, Moreton Island, Queensland, Australia",OK
    TAO,////,////,ZSQD,UNK,0.711,Qingdao,"Qingdao, PR China",OWNO,////,////,Liuting International Airport,"Qingdao, Shandong, China",OK
    TAP,////,////,MMTP,UNK,1.0,International,"Tapachula, Mexico",OWNO,////,////,Tapachula International Airport,"Tapachula, Chiapas, México",OK
    TAQ,////,////,YTAR,UNK,1.0,Tarcoola,"Tarcoola, Australia",OWNO,////,////,????,"Tarcoola, South Australia, Australia",OK
    TAR,////,////,LIBG,UNK,0.933,M. A. Grottag,"Taranto, Italy",OWNO,////,////,M. A. Grottaglie,"Taranto, Apulia, Italy",OK
    TAS,////,////,UTTT,UNK,0.615,Vostochny,"Tashkent, Uzbekistan",OWNO,////,////,Tashkent International Airport,"Tashkent, Toshkent (Tashkent), Uzbekistan",OK
    TAT,////,////,LZTT,UNK,1.0,Poprad/Tatry,"Poprad/Tatry, Slovakia",OWNO,////,////,Poprad-Tatry,"Poprad, Slovakia",OK
    TAU,////,////,SKTA,UNK,0.667,Tauramena,"Tauramena, Colombia",OWNO,////,////,Casanare,"Tauramena, Casanare, Colombia",OK
    TAV,HI36,////,////,OK,1.0,Tau,"Tau, American Samoa",OWNO,TAU,"TAU VILLAGE, AS - UNITED STATES",Tau Airport,"Tau Village, American Samoa",OK
    TAW,////,////,SUTB,UNK,1.0,Tacuarembo,"Tacuarembo, Uruguay",OWNO,////,////,????,"Tacuarembó, Tacuarembó, Uruguay",OK
    TAX,////,////,WAPT,UNK,0.667,Taliabu,"Taliabu, Indonesia",OWNO,////,////,Taliabu Island Airport,"Tikong, Taliabu Island, Maluku Utara, Indonesia",MAYBE
    TAY,////,////,EETU,UNK,0.533,Tartu,"Tartu, Estonia",OWNO,////,////,Ülenurme,"Tartu, Estonia",OK
    TAZ,////,////,UTAT,UNK,1.0,Dashoguz,"Dashoguz, Turkmenistan",OWNO,////,////,Dashoguz Airport,"Dashoguz, Dasoguz, Turkmenistan",OK
    TBA,////,////,////,UNK,1.0,Tabibuga,"Tabibuga, Papua New Guinea",OWNO,////,////,????,"Tabibuga, Western Highlands, Papua-New Guinea",OK
    TBB,////,////,VVTH,UNK,1.0,Tuy Hoa,"Tuy Hoa, Viet Nam",OWNO,////,////,????,"Tuy Hòa, Vietnam",OK
    TBC,T03,////,////,OK,1.0,Tuba City,"Tuba City (AZ), USA",OWNO,TUBA CITY,"TUBA CITY, AZ - UNITED STATES",????,"Tuba City, Arizona, United States",OK
    TBD,////,////,////,UNK,1.0,Timbiqui,"Timbiqui, Colombia",OWNO,////,////,????,"Timbiqui, Cauca, Colombia",OK
    TBE,////,////,////,UNK,1.0,Timbunke,"Timbunke, Papua New Guinea",OWNO,////,////,????,"Timbunke, East Sepik, Papua-New Guinea",OK
    TBF,////,////,NGTE,UNK,1.0,Tabiteuea North,"Tabiteuea North, Kiribati",OWNO,////,////,????,"Tabiteuea North, Kiribati",OK
    TBG,////,////,AYTB,UNK,1.0,Tabubil,"Tabubil, Papua New Guinea",OWNO,////,////,????,"Tabubil, Western, Papua-New Guinea",OK
    TBH,////,////,RPVU,UNK,0.6,Tablas,"Tablas, Philippines",OWNO,////,////,Romblon,"Tablas, Tablas Island, Philippines",OK
    TBI,////,////,////,UNK,1.0,The Bight,"The Bight, Bahamas",OWNO,////,////,????,"The Bight, Bahamas",OK
    TBJ,////,////,DTKA,UNK,0.545,Tabarka,"Tabarka, Tunisia",OWNO,////,////,Aïn Draham International Airport,"Tabarka, Tunisia",OK
    TBK,////,////,YTBR,UNK,1.0,Timber Creek,"Timber Creek, Australia",OWNO,////,////,????,"Timber Creek, Northern Territory, Australia",OK
    TBL,////,////,YTAB,UNK,1.0,Tableland,"Tableland, Australia",OWNO,////,////,????,"Tableland, Western Australia, Australia",OK
    TBM,////,////,////,UNK,1.0,Tumbang Samba,"Tumbang Samba, Indonesia",OWNO,////,////,????,"Tumbang Samba, Kalimantan Tengah (Central Borneo), Indonesia",OK
    TBN,TBN,KTBN,KTBN,OK,0.473,Forney AAF,"Fort Leonard Wood (MO), USA",OWNO,WAYNESVILLE-ST ROBERT RGNL FORNEY FLD,"FORT LEONARD WOOD, MO - UNITED STATES",Waynesville-St. Robert Regional Forney Field,"Fort Leonard Wood, Missouri, United States",OK
    TBO,////,////,HTTB,UNK,1.0,Tabora,"Tabora, Tanzania",OWNO,////,////,Tabora Airport,"Tabora, Tabora, Tanzania",OK
    TBP,////,////,SPME,UNK,0.261,Tumbes,"Tumbes, Peru",OWNO,////,////,Capitán FAP Pedro Canga Rodriguez,"Tumbes, Tumbes, Perú",OK
    TBR,TBR,KTBR,KTBR,OK,0.529,Municipal,"Statesboro (GA), USA",OWNO,STATESBORO-BULLOCH COUNTY,"STATESBORO, GA - UNITED STATES",Statesboro-Bulloch County Airport,"Statesboro, Georgia, United States",OK
    TBS,////,////,UGTB,UNK,0.514,Novo Alexeyevka,"Tbilisi, Georgia",OWNO,////,////,Tbilisi International Airport,"Tbilisi, Tbilisi, Georgia",OK
    TBT,////,////,SBTT,UNK,0.757,Internacional,"Tabatinga, Brazil",OWNO,////,////,Tabatinga International Airport,"Tabatinga, Amazonas, Brazil",OK
    TBU,////,////,NFTF,UNK,0.828,Fua'Amotu Internationa,"Nuku'Alofa, Tonga",OWNO,////,////,Fua'amotu International Airport,"Nuku'alofa, Tonga",OK
    TBV,////,////,////,UNK,1.0,Tabal,"Tabal, Marshall Islands",OWNO,////,////,????,"Tabal, Marshall Islands",OK
    TBW,////,////,UUOT,UNK,0.667,Tambov,"Tambov, Russia",OWNO,////,////,Donskoe Airport,"Tambov, Tambovskaya, Russian Federation (Russia)",OK
    TBY,////,////,FBTS,UNK,1.0,Tsabong,"Tsabong, Botswana",OWNO,////,////,Tshabong Airport,"Tshabong, Kgalagadi, Botswana",OK
    TBZ,////,////,OITT,UNK,0.706,Tabriz,"Tabriz, Iran",OWNO,////,////,Tabriz International Airport,"Tabriz, Azarbayjan-e Sharqi, Iran",OK
    TCA,////,////,YTNK,UNK,1.0,Tennant Creek,"Tennant Creek, Australia",OWNO,////,////,????,"Tennant Creek, Northern Territory, Australia",OK
    TCB,////,MYAT,MYAT,OK,1.0,Treasure Cay,"Treasure Cay, Bahamas",OWNO,TREASURE CAY,"TREASURE CAY, - BAHAMAS",????,"Treasure Cay, Abaco Islands, Bahamas",OK
    TCC,TCC,KTCC,KTCC,OK,0.64,Tucumcari,"Tucumcari (NM), USA",OWNO,TUCUMCARI MUNI,"TUCUMCARI, NM - UNITED STATES",Tucumcari Municipal Airport,"Tucumcari, New Mexico, United States",OK
    TCD,////,////,////,UNK,1.0,Tarapaca,"Tarapaca, Colombia",OWNO,////,////,????,"Tarapaca, Amazonas, Colombia",OK
    TCE,////,////,LRTC,UNK,0.667,Tulcea,"Tulcea, Romania",OWNO,////,////,Cataloi,"Tulcea, Romania",OK
    TCF,////,////,////,UNK,1.0,Tocoa,"Tocoa, Honduras",OWNO,////,////,????,"Tocoa, Colón, Honduras",OK
    TCG,////,////,ZWTC,UNK,1.0,Tacheng,"Tacheng, PR China",OWNO,////,////,????,"Tacheng, Xinjiang, China",OK
    TCH,////,////,FOOT,UNK,1.0,Tchibanga,"Tchibanga, Gabon",OWNO,////,////,Tchibanga Airport,"Tchibanga, Nyanga, Gabon",OK
    TCI,////,////,////,UNK,1.0,Metropolitan Area,"Tenerife, Canary Islands, Spain",OWNO,////,////,Metropolitan Area,"Tenerife, Tenerife Island, Canary Islands, Spain",OK
    TCL,TCL,KTCL,KTCL,OK,0.549,Van De Graaf,"Tuscaloosa (AL), USA",OWNO,TUSCALOOSA RGNL,"TUSCALOOSA, AL - UNITED STATES",Tuscaloosa Regional,"Tuscaloosa, Alabama, United States",OK
    TCM,TCM,KTCM,KTCM,OK,0.672,Mc Chord AFB,"Tacoma (WA), USA",OWNO,MCCHORD FIELD (JOINT BASE LEWIS-MCCHORD),"TACOMA, WA - UNITED STATES",McChord Field,"Tacoma, Washington, United States",OK
    TCN,////,////,MMHC,UNK,1.0,Tehuacan,"Tehuacan, Mexico",OWNO,////,////,????,"Tehuacán, Puebla, México",OK
    TCO,////,////,SKCO,UNK,1.0,La Florida,"Tumaco, Colombia",OWNO,////,////,La Florida Airport,"Tumaco, Nariño, Colombia",OK
    TCP,////,////,HETB,UNK,1.0,Taba International,"Taba, Egypt",OWNO,////,////,Taba International Airport,"Taba, Shamal Sina (North Sinai), Egypt",OK
    TCQ,////,////,SPTN,UNK,0.189,Tacna,"Tacna, Peru",OWNO,////,////,Coronel FAP Carlos Ciriani Santa Rosa International Airport,"Tacna, Tacna, Perú",OK
    TCR,////,////,////,UNK,1.0,Tuticorin,"Tuticorin, India",OWNO,////,////,Tuticorin Airport,"Thoothukudi, Tamil Nadu, India",OK
    TCS,TCS,KTCS,KTCS,OK,1.0,Municipal,"Truth Or Consequences (NM), USA",OWNO,TRUTH OR CONSEQUENCES MUNI,"TRUTH OR CONSEQUENCES, NM - UNITED STATES",Truth Or Consequences Municipal Airport,"Truth Or Consequences, New Mexico, United States",OK
    TCT,TCT,PPCT,PPCT,OK,1.0,Takotna,"Takotna (AK), USA",OWNO,TAKOTNA,"TAKOTNA, AK - UNITED STATES",????,"Takotna, Alaska, United States",OK
    TCU,////,////,FATN,UNK,1.0,Thaba Nchu,"Thaba Nchu, South Africa",OWNO,////,////,????,"Thaba Nchu, Free State, South Africa",OK
    TCW,////,////,YTOC,UNK,1.0,Tocumwal,"Tocumwal, Australia",OWNO,////,////,????,"Tocumwal, New South Wales, Australia",OK
    TCX,////,////,OIMT,UNK,1.0,Tabas,"Tabas, Iran",OWNO,////,////,????,"Tabas, Khorasan-e Janubi, Iran",OK
    TCZ,////,////,ZUTC,UNK,nan,////,////,////,////,////,Tengchong Tuofeng Airport,"Tengchong, Yunnan, China",UNK
    TDA,////,////,SKTD,UNK,1.0,Trinidad,"Trinidad, Colombia",OWNO,////,////,Trinidad Airport,"Trinidad, Casanare, Colombia",OK
    TDB,////,////,////,UNK,1.0,Tetabedi,"Tetabedi, Papua New Guinea",OWNO,////,////,????,"Tetabedi, Northern, Papua-New Guinea",OK
    TDD,////,////,SLTR,UNK,0.421,Trinidad,"Trinidad, Bolivia",OWNO,////,////,Teniente Jorge Henrich Arauz,"Trinidad, Cercado, El Beni, Bolivia",OK
    TDG,////,////,RPMW,UNK,1.0,Tandag,"Tandag, Philippines",OWNO,////,////,????,"Tandag, Mindanao Island, Philippines",OK
    TDJ,////,////,HDTJ,UNK,1.0,Tadjoura,"Tadjoura, Djibouti",OWNO,////,////,????,"Tadjoura, Djibouti",OK
    TDK,////,////,UAAT,UNK,0.545,Taldy-Kurgan,"Taldy-Kurgan, Kazakhstan",OWNO,////,////,Taldykorgan Airport,"Taldykorgan, Almaty, Kazakhstan",OK
    TDL,////,////,SAZT,UNK,1.0,Tandil,"Tandil, Argentina",OWNO,////,////,????,"Tandil, Buenos Aires, Argentina",OK
    TDN,////,////,YTHD,UNK,1.0,Theda Station,"Theda, Australia",OWNO,////,////,Theda Station Airport,"Drysdale River, Western Australia, Australia",OK
    TDO,TDO,KTDO,KTDO,OK,0.201,Winlock,"Toledo (WA), USA",OWNO,ED CARLSON MEMORIAL FIELD - SOUTH LEWIS CO,"TOLEDO, WA - UNITED STATES",Ed Carlson Memorial Field - South Lewis County Airport,"Toledo, Washington, United States",OK
    TDR,////,////,YTDR,UNK,1.0,Theodore,"Theodore, Australia",OWNO,////,////,????,"Theodore, Queensland, Australia",OK
    TDT,////,////,////,UNK,1.0,Tanda Tula,"Tanda Tula, South Africa",OWNO,////,////,????,"Tanda Tula, Limpopo, South Africa",OK
    TDV,////,////,FMSN,UNK,0.643,Tanandava,"Tanandava, Madagascar",OWNO,////,////,Samangoky Airport,"Tanandava, Madagascar",OK
    TDW,TDW,KTDW,KTDW,OK,1.0,Tradewind,"Amarillo (TX), USA",OWNO,TRADEWIND,"AMARILLO, TX - UNITED STATES",Tradewind Airport,"Amarillo, Texas, United States",OK
    TDX,////,////,VTBO,UNK,nan,////,////,////,////,////,????,"Trat, Trat, Thailand",UNK
    TDZ,TDZ,KTDZ,KTDZ,OK,0.473,Toledo,"Toledo (OH), USA",OWNO,TOLEDO EXECUTIVE,"TOLEDO, OH - UNITED STATES",Toledo Executive Airport,"Toledo, Ohio, United States",OK
    TEA,////,////,MHTE,UNK,1.0,Tela,"Tela, Honduras",OWNO,////,////,????,"Tela, Atlántida, Honduras",OK
    TEB,TEB,KTEB,KTEB,OK,1.0,Teterboro,"Teterboro (NJ), USA",OWNO,TETERBORO,"TETERBORO, NJ - UNITED STATES",????,"Teterboro, New Jersey, United States",OK
    TEC,////,////,SBTL,UNK,1.0,Telemaco Borba,"Telemaco Borba, Brazil",OWNO,////,////,????,"Telemaco Borba, Paraná, Brazil",OK
    TED,////,////,EKTS,UNK,1.0,Thisted,"Thisted, Denmark",OWNO,////,////,????,"Thisted, Denmark",OK
    TEE,////,////,DABS,UNK,0.444,Tbessa,"Tbessa, Algeria",OWNO,////,////,Cheikh Larbi Tébessi,"Tbessa, Tébessa, Algeria",OK
    TEF,////,////,YTEF,UNK,1.0,Telfer,"Telfer, Australia",OWNO,////,////,????,"Telfer, Western Australia, Australia",OK
    TEG,////,////,DFET,UNK,1.0,Tenkodogo,"Tenkodogo, Burkina Faso",OWNO,////,////,????,"Tenkodogo, Burkina Faso",OK
    TEI,////,////,////,UNK,1.0,Tezu,"Tezu, India",OWNO,////,////,????,"Tezu, Arunachal Pradesh, India",OK
    TEK,7KA,PAKA,PAKA,OK,1.0,Tatitlek,"Tatitlek (AK), USA",OWNO,TATITLEK,"TATITLEK, AK - UNITED STATES",????,"Tatitlek, Alaska, United States",OK
    TEL,////,////,WBKE,UNK,1.0,Telupid,"Telupid, Malaysia",OWNO,////,////,????,"Telupid, Sabah, Malaysia",OK
    TEM,////,////,YTEM,UNK,1.0,Temora,"Temora, Australia",OWNO,////,////,????,"Temora, New South Wales, Australia",OK
    TEN,////,////,ZUTR,UNK,1.0,Tongren,"Tongren, PR China",OWNO,////,////,????,"Tongren, Guizhou, China",OK
    TEO,////,////,////,UNK,1.0,Terapo,"Terapo, Papua New Guinea",OWNO,////,////,????,"Terapo, Gulf, Papua-New Guinea",OK

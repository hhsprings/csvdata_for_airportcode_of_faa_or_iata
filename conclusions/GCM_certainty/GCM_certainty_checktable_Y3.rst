
List for checking certainty of Great Circle Mapper (YNP - YTZ)
===================================================================================
This table is using following abbreviations:

* GCM: `Great Circle Mapper <http://www.gcmap.com/>`_
* OWNO: `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_

Each columns are:

* IATA code (GCM) - IATA code that Great Circle Mapper claims.
* FAA code - FAA code (relationship between codes is of GCM's claim)
* ICAO code (FAA) - found ICAO code at Aeronautical Information Services - National Flight Data Center (NFDC). (ex. `KBYH <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=KBYH>`_ )
* ICAO code (GCM) - found ICAO code at Great Circle Mapper.
* ICAO code check - if ICAO code is found in both FAA and GCM, checking whether these are the same or not.
* ratio - (Degrees of Coincidence of names) similarlity of names that NFDC, GCM, and OWNO claims (0.0 means those are not the same at all, 1.0 means those are almost the same).
* IATA name - airport name that OWNO or IATA claims.
* IATA loc - airport location that OWNO or IATA claims.
* IATA source - which name and loc are from `IATA list of One World Nation's Online <http://www.nationsonline.org/oneworld/airport_code.htm>`_ or `IATA code search <http://www.iata.org/publications/Pages/code-search.aspx>`_.
* FAA name - airport name that FAA claims.
* FAA loc - airport location that FAA claims.
* GCM name - airport name that GCM claims.
* GCM loc - airport location that GCM claims.
* country check - minimal checking to location. OK is OK, but others are maybe non-good or actually is OK. Plaese check it yourself.

.. csv-table::
    :header: IATA code (GCM), FAA code, ICAO code (FAA), ICAO code (GCM), ICAO code check, ratio, IATA name, IATA loc, IATA source, FAA name, FAA loc, GCM name, GCM loc, country check
    :widths: 2, 2, 2, 2, 2, 2,  2, 3, 2,  2, 3, 2, 3,  2

    YNP,////,////,////,UNK,1.0,Natuashish,Natuashish,IATA,////,////,Natuashish Airport,"Natuashish, Newfoundland and Labrador, Canada",MAYBE
    YNS,////,////,CYHH,UNK,1.0,Nemiscau,"Nemiscau, Canada",OWNO,////,////,????,"Némiscau, Québec, Canada",OK
    YNT,////,////,ZSYT,UNK,0.857,Yantai Laishan International Airport,"Yantai, PR China",OWNO,////,////,Penglai International Airport,"Yantai, Shandong, China",OK
    YNY,////,////,RKNY,UNK,1.0,Yangyang International Airport,"Gangneung, South Korea",OWNO,////,////,Yangyang International Airport,"Yangyang, Republic of Korea (South Korea)",OK
    YNZ,////,////,ZSYN,UNK,1.0,Yancheng,"Yancheng, PR China",OWNO,////,////,????,"Yancheng, Jiangsu, China",OK
    YOA,////,////,CYOA,UNK,1.0,Ekati,"Ekati, Canada",OWNO,////,////,Ekati Airport,"Ekati Diamond Mine, Northwest Territories, Canada",OK
    YOC,////,CYOC,CYOC,OK,1.0,Old Crow,"Old Crow, Canada",OWNO,OLD CROW,"OLD CROW, - CANADA",????,"Old Crow, Yukon, Canada",OK
    YOD,////,////,CYOD,UNK,0.783,Cold Lake,"Cold Lake, Canada",OWNO,////,////,Cold Lake Regional,"Cold Lake, Alberta, Canada",OK
    YOE,////,////,////,UNK,1.0,Falher,"Falher, Canada",OWNO,////,////,????,"Falher, Alberta, Canada",OK
    YOG,////,////,////,UNK,0.667,Ogoki,"Ogoki, Canada",OWNO,////,////,????,"Ogoki Post, Ontario, Canada",MAYBE
    YOH,////,////,CYOH,UNK,1.0,Oxford House,"Oxford House, Canada",OWNO,////,////,????,"Oxford House, Manitoba, Canada",OK
    YOI,////,////,////,UNK,1.0,Opinaca,Opinaca,IATA,////,////,Opinaca Aerodrome,"Opinaca River, Québec, Canada",MAYBE
    YOJ,////,////,CYOJ,UNK,0.688,Footner Lake,"High Level, Canada",OWNO,////,////,????,"High Level, Alberta, Canada",OK
    YOL,////,////,DNYO,UNK,1.0,Yola,"Yola, Nigeria",OWNO,////,////,Yola Airport,"Yola, Adamawa, Nigeria",OK
    YON,////,////,VQTY,UNK,nan,////,////,////,////,////,Yonphulla Airport,"Trashigang, Bhutan",UNK
    YOO,////,CYOO,CYOO,OK,1.0,Oshawa,"Oshawa, Canada",OWNO,OSHAWA,"OSHAWA, - CANADA",????,"Oshawa, Ontario, Canada",OK
    YOP,////,////,CYOP,UNK,1.0,Rainbow Lake,"Rainbow Lake, Canada",OWNO,////,////,????,"Rainbow Lake, Alberta, Canada",OK
    YOS,////,CYOS,CYOS,OK,1.0,Billy Bishop Regional,"Owen Sound, Canada",OWNO,OWEN SOUND BILLY BISHOP RGNL,"OWEN SOUND, - CANADA",Billy Bishop Regional,"Owen Sound, Ontario, Canada",OK
    YOT,////,////,LLYT,UNK,1.0,Yotvata,"Yotvata, Israel",OWNO,////,////,????,"Yotvata, Israel",OK
    YOW,////,CYOW,CYOW,OK,0.388,International,"Ottawa, Canada",OWNO,OTTAWA/MACDONALD-CARTIER INTL,"OTTAWA, - CANADA",Macdonald-Cartier International Airport,"Ottawa, Ontario, Canada",OK
    YPA,////,////,CYPA,UNK,0.759,Prince Albert,"Prince Albert, Canada",OWNO,////,////,Glass Field,"Prince Albert, Saskatchewan, Canada",OK
    YPB,////,////,////,UNK,0.667,Port Alberni,"Port Alberni, Canada",OWNO,////,////,Alberni Valley Regional,"Port Alberni, British Columbia, Canada",OK
    YPC,////,////,CYPC,UNK,0.583,Paulatuk,"Paulatuk, Canada",OWNO,////,////,Nora A. Ruben Airport,"Paulatuk, Northwest Territories, Canada",OK
    YPD,////,////,////,UNK,0.688,Parry Sound,"Parry Sound, Canada",OWNO,////,////,Parry Sound Area Municipal Airport,"Parry Sound, Ontario, Canada",OK
    YPE,////,////,CYPE,UNK,1.0,Peace River,"Peace River, Canada",OWNO,////,////,????,"Peace River, Alberta, Canada",OK
    YPF,////,////,CWPF,UNK,1.0,Esquimalt,"Esquimalt, Canada",OWNO,////,////,????,"Esquimalt, British Columbia, Canada",OK
    YPG,////,CYPG,CYPG,OK,0.413,Portage La Prairie,"Portage La Prairie, Canada",OWNO,PORTAGE LA PRAIRIE,"PORTAGE, - CANADA",Southport Airport,"Portage, Manitoba, Canada",MAYBE
    YPH,////,////,CYPH,UNK,1.0,Inukjuak,"Inukjuak, Canada",OWNO,////,////,????,"Inukjuak, Québec, Canada",OK
    YPI,////,////,////,UNK,1.0,Port Simpson,"Port Simpson, Canada",OWNO,////,////,????,"Port Simpson, British Columbia, Canada",OK
    YPJ,////,////,CYLA,UNK,1.0,Aupaluk,"Aupaluk, Canada",OWNO,////,////,????,"Aupaluk, Québec, Canada",OK
    YPK,////,CYPK,CYPK,OK,1.0,////,////,////,PITT MEADOWS,"PITT MEADOWS, - CANADA",????,"Pitt Meadows, British Columbia, Canada",OK
    YPL,////,////,CYPL,UNK,1.0,Pickle Lake,"Pickle Lake, Canada",OWNO,////,////,????,"Pickle Lake, Ontario, Canada",OK
    YPM,////,////,CYPM,UNK,1.0,Pikangikum,"Pikangikum, Canada",OWNO,////,////,????,"Pikangikum, Ontario, Canada",OK
    YPN,////,////,CYPN,UNK,1.0,Port Menier,"Port Menier, Canada",OWNO,////,////,????,"Port-Menier, Québec, Canada",OK
    YPO,////,////,CYPO,UNK,1.0,Peawanuck,"Peawanuck, Canada",OWNO,////,////,????,"Peawanuck, Ontario, Canada",OK
    YPQ,////,CYPQ,CYPQ,OK,1.0,Peterborough,"Peterborough, Canada",OWNO,PETERBOROUGH,"PETERBOROUGH, - CANADA",????,"Peterborough, Ontario, Canada",OK
    YPR,////,CYPR,CYPR,OK,0.52,Digby Island,"Prince Rupert, Canada",OWNO,PRINCE RUPERT,"PRINCE RUPERT, - CANADA",????,"Prince Rupert, British Columbia, Canada",OK
    YPS,////,////,CYPD,UNK,1.0,Port Hawkesbury,"Port Hawkesbury, Canada",OWNO,////,////,????,"Port Hawkesbury, Nova Scotia, Canada",OK
    YPT,////,////,////,UNK,0.828,Pender Harbor,"Pender Harbor, Canada",OWNO,////,////,Pender Harbour Water Aerodrome,"Pender Harbour, Pender Island, British Columbia, Canada",OK
    YPW,////,CYPW,CYPW,OK,1.0,Powell River,"Powell River, Canada",OWNO,POWELL RIVER,"POWELL RIVER, - CANADA",????,"Powell River, British Columbia, Canada",OK
    YPX,////,////,CYPX,UNK,1.0,Puvirnituq,"Povungnituk, Canada",OWNO,////,////,Puvirnituq,"Povungnituk, Québec, Canada",OK
    YPY,////,////,CYPY,UNK,1.0,Fort Chipewyan,"Fort Chipewyan, Canada",OWNO,////,////,????,"Fort Chipewyan, Alberta, Canada",OK
    YPZ,////,CYPZ,CYPZ,OK,1.0,Burns Lake,"Burns Lake, Canada",OWNO,BURNS LAKE,"BURNS LAKE, - CANADA",????,"Burns Lake, British Columbia, Canada",OK
    YQA,////,CYQA,CYQA,OK,0.87,Muskoka,"Muskoka, Canada",OWNO,MUSUKOKA,- CANADA,????,"Muskoka, Ontario, Canada",OK
    YQB,////,CYQB,CYQB,OK,0.318,Quebec,"Quebec, Canada",OWNO,QUEBEC/JEAN LESAGE INTL,"QUEBEC, - CANADA",Québec City Jean Lesage International Airport,"Québec, Québec, Canada",OK
    YQC,////,////,CYHA,UNK,1.0,Quaqtaq,"Quaqtaq, Canada",OWNO,////,////,????,"Quaqtaq, Québec, Canada",OK
    YQD,////,////,CYQD,UNK,1.0,The Pas,"The Pas, Canada",OWNO,////,////,????,"The Pas, Manitoba, Canada",OK
    YQF,////,////,CYQF,UNK,0.762,Red Deer,"Red Deer, Canada",OWNO,////,////,Regional,"Red Deer, Alberta, Canada",OK
    YQG,////,CYQG,CYQG,OK,1.0,Windsor,"Windsor, Canada",OWNO,WINDSOR,"WINDSOR, - CANADA",????,"Windsor, Ontario, Canada",OK
    YQH,////,CYQH,CYQH,OK,1.0,Watson Lake,"Watson Lake, Canada",OWNO,WATSON LAKE,"WATSON LAKE, - CANADA",????,"Watson Lake, Yukon, Canada",OK
    YQI,////,CYQI,CYQI,OK,1.0,Yarmouth,"Yarmouth, Canada",OWNO,YARMOUTH,"YARMOUTH, - CANADA",????,"Yarmouth, Nova Scotia, Canada",OK
    YQK,////,////,CYQK,UNK,1.0,Kenora,"Kenora, Canada",OWNO,////,////,????,"Kenora, Ontario, Canada",OK
    YQL,////,////,CYQL,UNK,1.0,Lethbridge,"Lethbridge, Canada",OWNO,////,////,????,"Lethbridge, Alberta, Canada",OK
    YQM,////,////,CYQM,UNK,0.611,Metropolitan Area,"Moncton, Canada",OWNO,////,////,????,"Moncton, New Brunswick, Canada",OK
    YQN,////,////,CYQN,UNK,1.0,Nakina,"Nakina, Canada",OWNO,////,////,????,"Nakina, Ontario, Canada",OK
    YQQ,////,CYQQ,CYQQ,OK,1.0,Comox,"Comox, Canada",OWNO,COMOX,"COMOX, - CANADA",????,"Comox, British Columbia, Canada",OK
    YQR,////,////,CYQR,UNK,1.0,Regina,"Regina, Canada",OWNO,////,////,????,"Regina, Saskatchewan, Canada",OK
    YQS,////,CYQS,CYQS,OK,0.339,Pembroke Area Mncpl,"St Thomas, Canada",OWNO,ST THOMAS MUNI,"ST THOMAS, - CANADA",Pembroke Area Municipal Airport,"St. Thomas, Ontario, Canada",OK
    YQT,////,CYQT,CYQT,OK,1.0,Thunder Bay,"Thunder Bay, Canada",OWNO,THUNDER BAY,"THUNDER BAY, - CANADA",????,"Thunder Bay, Ontario, Canada",OK
    YQU,////,////,CYQU,UNK,1.0,Grande Prairie,"Grande Prairie, Canada",OWNO,////,////,Grande Prairie Airport,"Grande Prairie, Alberta, Canada",OK
    YQV,////,////,CYQV,UNK,0.824,Yorkton,"Yorkton, Canada",OWNO,////,////,Municipal,"Yorkton, Saskatchewan, Canada",OK
    YQW,////,////,CYQW,UNK,1.0,North Battleford,"North Battleford, Canada",OWNO,////,////,????,"North Battleford, Saskatchewan, Canada",OK
    YQX,////,////,CYQX,UNK,0.75,Gander,"Gander, Canada",OWNO,////,////,Gander International Airport,"Gander, Newfoundland and Labrador, Canada",OK
    YQY,////,////,CYQY,UNK,0.421,Sydney,"Sydney, Canada",OWNO,////,////,J.A. Douglas McCurdy Airport,"Sydney, Nova Scotia, Canada",OK
    YQZ,////,////,CYQZ,UNK,1.0,Quesnel,"Quesnel, Canada",OWNO,////,////,????,"Quesnel, British Columbia, Canada",OK
    YRA,////,////,CYRA,UNK,1.0,Rae Lakes,"Rae Lakes, Canada",OWNO,////,////,Rae Lakes Airport,"Gamèti, Northwest Territories, Canada",OK
    YRB,////,////,CYRB,UNK,0.875,Resolute,"Resolute, Canada",OWNO,////,////,????,"Resolute Bay, Nunavut, Canada",MAYBE
    YRC,////,////,CYRC,UNK,0.25,SPB,Refuge Cove,IATA,////,////,????,"Chicoutimi, Québec, Canada",TO DO CHECK
    YRD,////,////,////,UNK,1.0,Dean River,"Dean River, Canada",OWNO,////,////,????,"Dean River, British Columbia, Canada",OK
    YRF,////,////,CYCA,UNK,1.0,Cartwright,"Cartwright, Canada",OWNO,////,////,Cartwright Airport,"Cartwright, Newfoundland and Labrador, Canada",OK
    YRG,////,////,////,UNK,1.0,Rigolet,"Rigolet, Canada",OWNO,////,////,????,"Rigolet, Newfoundland and Labrador, Canada",OK
    YRI,////,CYRI,CYRI,OK,1.0,Riviere Du Loup,"Riviere Du Loup, Canada",OWNO,RIVIERE-DU-LOUP,"RIVIERE-DU-LOUP, - CANADA",????,"Rivière-du-Loup, Québec, Canada",OK
    YRJ,////,////,CYRJ,UNK,1.0,Roberval,"Roberval, Canada",OWNO,////,////,????,"Roberval, Québec, Canada",OK
    YRL,////,////,CYRL,UNK,1.0,Red Lake,"Red Lake, Canada",OWNO,////,////,????,"Red Lake, Ontario, Canada",OK
    YRM,////,////,CYRM,UNK,1.0,Rocky Mountain House,"Rocky Mountain House, Canada",OWNO,////,////,????,"Rocky Mountain House, Alberta, Canada",OK
    YRN,////,////,////,UNK,1.0,Rivers Inlet,"Rivers Inlet, Canada",OWNO,////,////,????,"Rivers Inlet, British Columbia, Canada",OK
    YRO,////,CYRO,CYRO,OK,1.0,Rockcliffe,"Ottawa, Canada",OWNO,ROCKCLIFFE,"OTTAWA, - CANADA",Rockcliffe,"Ottawa, Ontario, Canada",OK
    YRQ,////,CYRQ,CYRQ,OK,1.0,Trois-Rivieres,"Trois-Rivieres, Canada",OWNO,TROIS-RIVIERES,"TROIS-RIVIERES, - CANADA",????,"Trois-Rivières, Québec, Canada",OK
    YRR,////,////,////,UNK,1.0,Stuart Island,"Stuart Island, Canada",OWNO,////,////,????,"Stuart Island, British Columbia, Canada",OK
    YRS,////,////,CYRS,UNK,1.0,Red Sucker Lake,"Red Sucker Lake, Canada",OWNO,////,////,????,"Red Sucker Lake, Manitoba, Canada",OK
    YRT,////,////,CYRT,UNK,1.0,Rankin Inlet,"Rankin Inlet, Canada",OWNO,////,////,????,"Rankin Inlet, Northwest Territories, Canada",OK
    YRV,////,////,CYRV,UNK,1.0,Revelstoke,"Revelstoke, Canada",OWNO,////,////,????,"Revelstoke, British Columbia, Canada",OK
    YSA,////,////,////,UNK,1.0,Sable Island,"Sable Island, Canada",OWNO,////,////,Sable Island Aerodrome,"Sable Island, Nova Scotia, Canada",OK
    YSB,////,CYSB,CYSB,OK,1.0,Sudbury,"Sudbury, Canada",OWNO,SUDBURY,"SUDBURY, - CANADA",????,"Sudbury, Ontario, Canada",OK
    YSC,////,CYSC,CYSC,OK,1.0,Sherbrooke,"Sherbrooke, Canada",OWNO,SHERBROOKE,"SHERBROOKE, - CANADA",????,"Sherbrooke, Québec, Canada",OK
    YSD,////,////,CYSD,UNK,1.0,Suffield,"Suffield, Canada",OWNO,////,////,????,"Suffield, Alberta, Canada",OK
    YSE,////,////,CYSE,UNK,1.0,Squamish,"Squamish, Canada",OWNO,////,////,Squamish Airport,"Squamish, British Columbia, Canada",OK
    YSF,////,////,CYSF,UNK,1.0,Stony Rapids,"Stony Rapids, Canada",OWNO,////,////,????,"Stony Rapids, Saskatchewan, Canada",OK
    YSG,////,////,////,UNK,1.0,Lutselke,"Lutselke/Snowdrift, Canada",OWNO,////,////,????,"Lutselke/Snowdrift, Northwest Territories, Canada",OK
    YSH,////,CYSH,CYSH,OK,0.488,Smith Falls,"Smith Falls, Canada",OWNO,MONTAGUE/RUSS BEACH,"SMITHS FALLS-MONTAGUE, - CANADA",Montague,"Smith Falls, Ontario, Canada",MAYBE
    YSI,////,////,////,UNK,0.455,Sans Souci,"Sans Souci, Canada",OWNO,////,////,Frying Pan Island-Sans Souci Water Aerodrome,"Parry Sound, Ontario, Canada",MAYBE
    YSJ,////,CYSJ,CYSJ,OK,1.0,Saint John,"Saint John, Canada",OWNO,SAINT JOHN,"SAINT JOHN, - CANADA",????,"Saint John, New Brunswick, Canada",OK
    YSK,////,////,CYSK,UNK,1.0,Sanikiluaq,"Sanikiluaq, Canada",OWNO,////,////,Sanikiluaq Airport,"Sanikiluaq, Nunavut, Canada",OK
    YSL,////,CYSL,CYSL,OK,0.458,Edmunston,"St Leonard, Canada",OWNO,ST LEONARD,"ST LEONARD, - CANADA",????,"St. Leonard, New Brunswick, Canada",OK
    YSM,////,////,CYSM,UNK,1.0,Fort Smith,"Fort Smith, Canada",OWNO,////,////,????,"Fort Smith, Northwest Territories, Canada",OK
    YSN,////,////,CZAM,UNK,1.0,Salmon Arm,"Salmon Arm, Canada",OWNO,////,////,????,"Salmon Arm, British Columbia, Canada",OK
    YSO,////,////,////,UNK,1.0,Postville,"Postville, Canada",OWNO,////,////,????,"Postville, Newfoundland and Labrador, Canada",OK
    YSP,////,////,CYSP,UNK,1.0,Marathon,"Marathon, Canada",OWNO,////,////,????,"Marathon, Ontario, Canada",OK
    YSR,////,////,CYSR,UNK,1.0,Nanisivik,"Nanisivik, Canada",OWNO,////,////,????,"Nanisivik, Nunavut, Canada",OK
    YST,////,////,CYST,UNK,1.0,Ste Therese Point,"Ste Therese Point, Canada",OWNO,////,////,????,"Ste. Theresa Point, Manitoba, Canada",OK
    YSU,////,CYSU,CYSU,OK,1.0,Summerside,"Summerside, Canada",OWNO,SUMMERSIDE,"SUMMERSIDE, - CANADA",????,"Summerside, Prince Edward Island, Canada",OK
    YSX,////,////,////,UNK,0.769,Shearwater,"Shearwater, Canada",OWNO,////,////,Shearwater Water Aerodrome,"Bella Bella, British Columbia, Canada",MAYBE
    YSY,////,////,CYSY,UNK,1.0,Sachs Harbour,"Sachs Harbour, Canada",OWNO,////,////,????,"Sachs Harbour, Northwest Territories, Canada",OK
    YTA,////,CYTA,CYTA,OK,0.592,Pembroke and Area Airport,"Pembroke, Canada",OWNO,PEMBROKE,"PEMBROKE, - CANADA",????,"Pembroke, Ontario, Canada",OK
    YTB,////,////,////,UNK,0.786,Hartley Bay,"Hartley Bay, Canada",OWNO,////,////,Hartley Bay Water Aerodrome,"Hartley Bay, British Columbia, Canada",OK
    YTD,////,////,CZLQ,UNK,1.0,Thicket Portage,"Thicket Portage, Canada",OWNO,////,////,????,"Thicket Portage, Manitoba, Canada",OK
    YTE,////,////,CYTE,UNK,1.0,Cape Dorset,"Cape Dorset, Canada",OWNO,////,////,????,"Cape Dorset, Northwest Territories, Canada",OK
    YTF,////,////,CYTF,UNK,1.0,Alma,"Alma, Canada",OWNO,////,////,????,"Alma, Québec, Canada",OK
    YTG,////,////,////,UNK,0.8,Sullivan Bay,"Sullivan Bay, Canada",OWNO,////,////,Sullivan Bay Water Aerodrome,"Sullivan Bay, British Columbia, Canada",OK
    YTH,////,////,CYTH,UNK,1.0,Thompson,"Thompson, Canada",OWNO,////,////,????,"Thompson, Manitoba, Canada",OK
    YTL,////,////,CYTL,UNK,1.0,Big Trout,"Big Trout, Canada",OWNO,////,////,????,"Big Trout, Ontario, Canada",OK
    YTM,////,////,CYFJ,UNK,nan,////,////,////,////,////,La Macaza - Mont-Tremblant International Airport,"Mont-Tremblant, Québec, Canada",UNK
    YTO,////,////,////,UNK,1.0,Metropolitan Area,"Toronto, Canada",OWNO,////,////,Metropolitan Area,"Toronto, Ontario, Canada",OK
    YTP,////,////,////,UNK,0.516,Seaplane Base,"Tofino, Canada",OWNO,////,////,Tofino Harbour Water Aerodrome,"Tofino Harbour, British Columbia, Canada",MAYBE
    YTQ,////,////,CYTQ,UNK,1.0,Tasiujuaq,"Tasiujuaq, Canada",OWNO,////,////,????,"Tasiujuaq, Québec, Canada",OK
    YTR,////,CYTR,CYTR,OK,1.0,Trenton,"Trenton, Canada",OWNO,TRENTON,"TRENTON, - CANADA",????,"Trenton, Ontario, Canada",OK
    YTS,////,CYTS,CYTS,OK,0.233,Timmins,"Timmins, Canada",OWNO,TIMMINS,"TIMMINS, - CANADA",Timmins Victor M. Power Airport,"Timmins, Ontario, Canada",OK
    YTT,////,////,////,UNK,1.0,Tisdale,"Tisdale, Canada",OWNO,////,////,????,"Tisdale, Saskatchewan, Canada",OK
    YTY,////,////,ZSYA,UNK,nan,////,////,////,////,////,Taizhou Airport,"Yangzhou, Jiangsu, China",UNK
    YTZ,////,CYTZ,CYTZ,OK,0.471,Billy Bishop Toronto City Airport,"Toronto, Canada",OWNO,TORONTO/CITY CENTRE,"TORONTO, - CANADA",Billy Bishop Toronto City Airport,"Toronto, Ontario, Canada",OK

Found code mismatches (GCMap and NFDC)
======================================
NFDC: Aeronautical Information Services - National Flight Data Center (NFDC)


FAA LIDs only in GCMap
------------------------------

.. csv-table::
    :header: FAA LID (NFDC), FAA LID (GCM), ICAO (NFDC), ICAO (GCM), NAME (NFDC), LOC (NFDC), NAME (GCM), LOC (GCM)

    ////,43U,////,////,////,////,????,"Mount Pleasant, Utah, United States"
    ////,AK04,////,PAZK,////,////,Skelton Airport,"Eureka, Alaska, United States"
    ////,BDX,////,KBDX,////,////,????,"Broadus, Montana, United States"
    ////,HDI,////,KHDI,////,////,Hardwick Field,"Cleveland, Tennessee, United States"
    ////,KCG,////,////,////,////,Chignik Fisheries Airport,"Chignik, Alaska, United States"
    ////,KCN,////,////,////,////,Chernofski Harbor SPB,"Chernofski Harbor, Alaska, United States"
    ////,MHN,////,KMHN,////,////,Hooker County Airport,"Mullen, Nebraska, United States"
    ////,RI12,////,////,////,////,Quonset State Air Reserve National Guard Helipad,"North Kingstown, Rhode Island, United States"
    ////,RZZ,////,KRZZ,////,////,Halifax County Airport,"Roanoke Rapids, North Carolina, United States"
    ////,SKY,////,KSKY,////,////,Griffing Sandusky Airport,"Sandusky, Ohio, United States"
    ////,TKJ,////,PATJ,////,////,????,"Tok, Alaska, United States"
    ////,X95,////,TJFA,////,////,Diego Jiménez Torres Airport,"Fajardo, Puerto Rico, United States"
    ////,XS10,////,////,////,////,Aransas National Wildlife Refuge Airport,"Matagorda Island, Texas, United States"
    

FAA LIDs only in NFDC
------------------------------

.. csv-table::
    :header: FAA LID (NFDC), FAA LID (GCM), ICAO (NFDC), ICAO (GCM), NAME (NFDC), LOC (NFDC), NAME (GCM), LOC (GCM)

    9K2,////,PFKK,PFKK,KOKHANOK,"KOKHANOK, AK - UNITED STATES",????,"Kokhanok, Alaska, United States"
    D76,////,PFNO,PFNO,ROBERT/BOB/CURTIS MEMORIAL,"NOORVIK, AK - UNITED STATES",Robert/Bob/Curtis Memorial Airport,"Noorvik, Alaska, United States"
    MHM,////,PAMH,PAMH,MINCHUMINA,"MINCHUMINA, AK - UNITED STATES",????,"Minchumina, Alaska, United States"
    

ICAO code mismatch
------------------------------

.. csv-table::
    :header: FAA LID (NFDC), FAA LID (GCM), ICAO (NFDC), ICAO (GCM), NAME (NFDC), LOC (NFDC), NAME (GCM), LOC (GCM)

    0AA1,0AA1,////,PACY,YAKATAGA,"YAKATAGA, AK - UNITED STATES",????,"Yakataga, Alaska, United States"
    0AA4,0AA4,////,PAFW,FAREWELL,"FAREWELL, AK - UNITED STATES",????,"Farewell, Alaska, United States"
    1Q9,1Q9,////,MLIP,MILI,"MILI ISLAND, - MARSHALL ISLANDS",????,"Mili Island, Marshall Islands"
    5MS1,5MS1,////,KRFK,ROLLANG FIELD,"ROLLING FORK, MS - UNITED STATES",Rollang Field,"Rolling Fork, Mississippi, United States"
    9K2,9K2,PFKK,////,KOKHANOK,"KOKHANOK, AK - UNITED STATES",????,"Kokhanok, Alaska, United States"
    AK71,AK71,////,PALN,LONELY AS,"LONELY, AK - UNITED STATES",Lonely Air Station,"Lonely, Alaska, United States"
    D76,D76,PFNO,////,ROBERT/BOB/CURTIS MEMORIAL,"NOORVIK, AK - UNITED STATES",Robert (Bob) Curtis Memorial,"Noorvik, Alaska, United States"
    MHM,MHM,PAMH,////,MINCHUMINA,"MINCHUMINA, AK - UNITED STATES",????,"Lake Minchumina, Alaska, United States"
    NV11,NV11,////,KUCC,YUCCA AIRSTRIP,"MERCURY, NV - UNITED STATES",Yucca Airstrip,"Mercury, Nevada, United States"
    NV65,NV65,////,KDRA,DESERT ROCK,"MERCURY, NV - UNITED STATES",Desert Rock Airport,"Mercury, Nevada, United States"
    RRT,RRT,KRRT,KRAD,WARROAD INTL MEMORIAL,"WARROAD, MN - UNITED STATES",Warroad International Memorial,"Warroad, Minnesota, United States"
    TPO,TPO,////,PALJ,PORT ALSWORTH,"PORT ALSWORTH, AK - UNITED STATES",????,"Port Alsworth, Alaska, United States"
    TX26,TX26,////,KATT,MABRY ARNG,"AUSTIN, TX - UNITED STATES",Camp Maybry AHP,"Austin, Texas, United States"
    

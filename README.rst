csvdata to check which the airport code is of FAA or IATA
#########################################################

This is NOT:
============
* Not necessarily include all of the ICAO code.
* Not necessarily include all of the FAA code.
* Not necessarily include all of the IATA code. (IATA code was extracted from IATA code list of `One World - Nations Online <http://www.nationsonline.org/oneworld/airport_code.htmyes>`_).
* These data do not have a guarantee to give an accuracy.
* These data do not show relationship between the IATA code and the ICAO code.
* Maybe include the code that does not exist. (the input codes were extracted from openflights's ``airports.dat`` and wikipedia "List of airports by ICAO code" and "List of airports by IATA code".)

This is:
========
The columns about FAA in these csvdata was extracted from `FAA's airport lookup <https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=BDJ>`_, and the columns about IATA was extracted from `One World - Nations Online <http://www.nationsonline.org/oneworld/airport_code.htmyes>`_. (The latter is slightly old and has a few problems, but mostly is ok.) All data are extracted on Sep, 2016, by my simple Python script.

This is just for quick checking the IATA code or the FAA code.

* To check the ICAO code is not in FAA.

  * If the ICAO code is in ``ICAO_codes_not_in_FAA.csv``, it is likely not of FAA.

* To check the IATA code is not of FAA.

  * If the row of ``codes_in_FAA_or_IATA.csv`` which has the code has no ``found_as_faa_name`` (== ``////``) and has ``found_as_iatatab_name``, the code is likely of IATA.

* To check the FAA code is not of IATA.

  * If the row of ``codes_in_FAA_or_IATA.csv`` which has the code has no ``found_as_iatatab_name`` (== ``////``) and has ``found_as_faa_name``, the code is likely of FAA.

If the row of ``codes_in_FAA_or_IATA.csv`` which has the code has both ``found_as_iatatab_name`` and ``found_as_faa_name``, the code is maybe IATA code or FAA code or both. Please check its all columns. (Note that ``icao_found_in_faa`` column means the ICAO code is corresponding to FAA code, but has no relation to IATA code.)

Because the codes were listed from openflights's ``airports.dat`` and wikipedia "List of airports by ICAO code" and "List of airports by IATA code", so many lost codes were found. Please see ``codes_not_in_FAA_not_in_IATA.csv`` (these are maybe simple mistake, or is obsolete code, or... but I don't know the truth at all).

Why the script what I wrote is not publicized?
==============================================
The script is so-called **scraper** that puts a strain to the servers, so I can't decide to release it, sorry.
